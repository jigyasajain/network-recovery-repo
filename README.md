ATMA
====

# Deploying to Tomcat

Edit $TOMCAT_PATH/conf/tomcat-users.xml make sure there is:

```xml
    <tomcat-users>
        <role rolename="manager-gui"/>
        <role rolename="manager-script"/>
        <user username="admin1" password="admin1" roles="manager-script"/>
    </tomcat-users>
```

Edit #MAVEN_PATH/conf/settings.xml, make sure there is:
```xml
    <servers>
         <server>
            <id>TomcatServer</id>
            <username>admin1</username>
            <password>admin1</password>
        </server>
    </servers>
```

Start tomcat7 instance to manipulate the WAR file on Tomcat7:

 - mvn tomcat7:deploy
 - mvn tomcat7:undeploy
 - mvn tomcat7:redeploy

Navigate to http://localhost:8080/root
