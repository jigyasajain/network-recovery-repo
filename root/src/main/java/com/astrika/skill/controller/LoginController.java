package com.astrika.skill.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.astrika.common.exception.DuplicateEmailException;
import com.astrika.common.exception.DuplicateLoginException;
import com.astrika.common.exception.NoSuchAreaException;
import com.astrika.common.exception.NoSuchCityException;
import com.astrika.common.exception.NoSuchCountryException;
import com.astrika.common.exception.NoSuchUserException;
import com.astrika.common.exception.PasswordException;
import com.astrika.common.model.CompanyApproval;
import com.astrika.common.model.Role;
import com.astrika.common.model.User;
import com.astrika.common.model.location.CityMaster;
import com.astrika.common.model.location.CountryMaster;
import com.astrika.common.model.location.StateMaster;
import com.astrika.common.service.CityService;
import com.astrika.common.service.CountryService;
import com.astrika.common.service.CurrencyService;
import com.astrika.common.service.ForgotPasswordAndLoginService;
import com.astrika.common.service.SendMailService;
import com.astrika.common.service.StateService;
import com.astrika.common.service.UserService;
import com.astrika.common.util.PropsValues;
import com.astrika.companymngt.exception.DuplicateCompanyException;
import com.astrika.educationsector.service.EducationSectorService;
import com.astrika.kernel.exception.BusinessException;
import com.astrika.kernel.exception.CustomException;
import com.astrika.registrationmngt.exception.DuplicateNGOEmailException;
import com.astrika.registrationmngt.model.EnquiryList;
import com.astrika.registrationmngt.service.EnquiryService;
import com.astrika.skill.config.MyWebAuthenticationDetails;
import com.google.gson.Gson;

@Controller
public class LoginController {

	@Autowired
	private SendMailService mailService;

	@Autowired
	private EducationSectorService educationSectorService;

	@Autowired
	private UserService userService;

	@Autowired
	private StateService stateService;

	@Autowired
	private CityService cityService;

	@Autowired
	private CountryService countryService;

	@Autowired
	private ForgotPasswordAndLoginService forgotPasswordAndLoginService;

	@Autowired
	private PropsValues prop;

	@Autowired
	private CurrencyService currencyService;

	@Autowired
	@Qualifier("authManager")
	protected AuthenticationManager authenticationManager;

	private static final Logger LOGGER = Logger.getLogger(LoginController.class);

	@Autowired
	private PropsValues propsValue;

	@Autowired
	private EnquiryService enquiryService;

	private static final String SUCCESS="success";
	private static final String ERROR_MSG="errorMsg";
	private static final String ANDROID="ANDROID";
	private static final String ERROR="error";
	private static final String COUNTRY_LIST="countryList";
	
	@RequestMapping("/Login")
	public String loginview(
			@RequestParam(value = "errorCode", required = false) String errorCode,
			@RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
			Map<String, Object> model) throws NoSuchUserException {

		if (success != null && !success.isEmpty()) {
			model.put(SUCCESS, success);
		}

		User user;
		try {
			user = (User) SecurityContextHolder.getContext()
					.getAuthentication().getPrincipal();
		} catch (Exception e) {
			LOGGER.info(e);
			user = null;
		}
		if (user != null) {
			user = userService.findById(user.getUserId());
			if(user.getRole() == Role.ATMA_ADMIN){
				return "redirect:/SuperAdmin";
			}
			else if(user.getRole() == Role.ATMA_ADMIN){
				return "redirect:/AtmaAdmin";
			}
			else if(user.getRole() == Role.COMPANY_ADMIN){
				return "redirect:/CompanyAdmin";
			}
			else if(user.getRole() == Role.MODULE_USER){
				return "redirect:/ModuleUser";
			}
			else{
				return "visitor/mngt/login";
			}
		}
		model.put("errorCode", errorCode);
		return "visitor/mngt/login";
	}

	@ExceptionHandler(BusinessException.class)
	public String handleBusinessException(HttpServletRequest request,
			BusinessException ex) {
		request.setAttribute(ERROR_MSG, ex.getErrorCode());
		return "common/error";
	}

	@RequestMapping("/Index")
	public String home( Map<String, Object> model)
			throws NoSuchUserException {
		String client = null;
		try {
			client = ((MyWebAuthenticationDetails) SecurityContextHolder
					.getContext().getAuthentication().getDetails()).getClient();
		} catch (Exception e) {
			client = null;
			LOGGER.info(e);

		}

		User user = (User) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		if (client != null && ANDROID.equals(client)) {
			user = (User) SecurityContextHolder.getContext()
					.getAuthentication().getPrincipal();


			userService.update(user);
			user = userService.findById(user.getUserId());
			model.put("user", user);
			return "common/token";
		}

		if (user.getRole().getId() == Role.SUPER_ADMIN.getId()) {
			return "redirect:/SuperAdmin/";
		} else if (user.getRole().getId() == Role.ATMA_ADMIN.getId()) {
			return "redirect:/AtmaAdmin/";
		} else if (user.getRole().getId() == Role.COMPANY_ADMIN.getId()) {
			return "redirect:/CompanyAdmin/";
		} else if (user.getRole().getId() == Role.MODULE_USER.getId()) {
			return "redirect:/ModuleUser/";
		}
		else{
			return "redirect:/ModuleUser/";
		}

	}

	@RequestMapping("/ForgotLogin")
	public String forgotLogin() {

		return "common/login/forgotLogin";
	}

	@RequestMapping(value = "/SaveEnquiry")
	public String saveEnquiry(
			@RequestParam("firstName") String firstName,
			@RequestParam("lastName") String lastName,
			@RequestParam("emailId") String emailId,
			@RequestParam("mobile") String mobile,
			@RequestParam("ngoName") String ngoName,
			@RequestParam("sectorWork")String sectorWork,
			@RequestParam("ngoEmail") String ngoEmail,
			@RequestParam("ngoPhone") String ngoPhone,
			@RequestParam("ngoAddressLine1") String ngoAddress1,
			@RequestParam("ngoAddressLine2") String ngoAddress2,
			@RequestParam("ngoAddressLine3") String ngoAddress3,
			@RequestParam("stateDiv") Long stateId,
			@RequestParam("cityDiv") Long cityId,
			@RequestParam("countryDiv") Long countryId,
			@RequestParam("ngoPincode") String ngoPincode,
			@RequestParam("ngoWebsite") String ngoWebsite,
			@RequestParam("organisationVision") String organisationVision,
			@RequestParam("organisationMission") String organisationMission,
			@RequestParam("organisationDescription") String organisationDescription,
			@RequestParam("assitanceFromOtherAgency")boolean assitanceFromOtherAgency,
			@RequestParam("legalType")String legalType,
			@RequestParam("fcraRegistarion")boolean fcraRegistarion,
			@RequestParam("typeOfEducation")String typeOfEducation,
			Map<String, Object> model
			)throws IOException{


		boolean exceptionFlag=false;
		StateMaster ngoState = null;
		try {
			ngoState = stateService.findByStateId(stateId);
		} catch (NoSuchAreaException e) {
			exceptionFlag = true;
			model.put(ERROR, CustomException.NO_SUCH_STATE.getCode());
			LOGGER.info(e);
		}
		CityMaster ngoCity = null;
		if (!exceptionFlag) {
			try {
				ngoCity = cityService.findById(cityId);
			} catch (NoSuchCityException e) {
				exceptionFlag = true;
				model.put(ERROR, CustomException.NO_SUCH_CITY.getCode());
				LOGGER.info(e);
			}
		}
		CountryMaster ngoCountry = null;
		if (!exceptionFlag) {
			try {
				ngoCountry = countryService.findById(countryId);
			} catch (NoSuchCountryException e) {
				exceptionFlag = true;
				model.put(ERROR, CustomException.NO_SUCH_COUNTRY.getCode());
				LOGGER.info(e);
			}
		}

		EnquiryList enquiry=new EnquiryList(firstName, lastName, emailId,mobile,ngoName,sectorWork,ngoEmail,ngoPhone,ngoAddress1,ngoAddress2,ngoAddress3,ngoCity,ngoState,ngoCountry,ngoPincode,ngoWebsite,
				organisationVision,organisationMission,organisationDescription,assitanceFromOtherAgency,legalType,fcraRegistarion,typeOfEducation,CompanyApproval.PENDING);

		try{
			enquiryService.save(enquiry);
			enquiryService.sendEnquiryMail(enquiry, ngoName, null);
		}
		catch (DuplicateEmailException e) {
			exceptionFlag = true;
			LOGGER.info(e);
			model.put(ERROR, e.getErrorCode());
		}
		catch(DuplicateNGOEmailException e){
			exceptionFlag = true;
			LOGGER.info(e);
			model.put(ERROR, e.getErrorCode());
		}
		catch (DuplicateCompanyException e) {
			exceptionFlag = true;
			LOGGER.info(e);
			model.put(ERROR, e.getErrorCode());
		}
		if (!exceptionFlag) {
			model.put(SUCCESS, "alert.enquiryregistersuccessfully");
			return "redirect:/Login";


		} else {
			List<CityMaster> cityList = cityService.findByActive(true);
			List<CountryMaster> countryList = countryService.findByActive(true);
			List<StateMaster> stateList = stateService.findByActive(true);

			List<Object[]> educationSectionList=educationSectorService.getAllSector();
			List<Map<String, Object>> organisationType = new ArrayList<>(); 
			for(Object[] c:educationSectionList){ 
				Map< String, Object> mmm= new HashMap<>();
				mmm.put("id", c[0]);
				mmm.put("educationtype", c[1]);
				organisationType.add(mmm);
			}

			model.put("organisationType", organisationType);
			model.put("cityList", cityList);
			model.put(COUNTRY_LIST, countryList);
			model.put("stateList", stateList);
			model.put("enquiry", enquiry);

			return "common/login/companyregister";
		}
	}

	@RequestMapping(value = "/SubmitForgotLogin")
	public String submitForgotLogin(
			@RequestParam(value = "emailId", required = true) String emailId,
			@RequestParam(value = "client", required = false) String client,
			Map<String, Object> model) throws DuplicateEmailException {
		boolean error = false;

		try {
			forgotPasswordAndLoginService.sendForgotLoginMail(emailId);
			model.put(SUCCESS, "alert.forgotLoginSent");
		} catch (NoSuchUserException e) {
		    LOGGER.info(e);
		    error = true;
			model.put(ERROR_MSG, "alert.emaildoesnotexists");
			
		}
		if (ANDROID.equalsIgnoreCase(client) && client != null) {
			if (error)
				return "common/error";
			else
				return "common/success";
		}
		return "common/login/forgotLogin";
	}

	@RequestMapping(value = { "/{url1}/{url2}/ResetUniversityAdminPassword" }, method = RequestMethod.POST)
	public String resetUniversityAdminPassword(
			@PathVariable(value = "url1") String url1,
			@PathVariable(value = "url2") String url2,
			@RequestParam("universityId") Long universityId,
			@RequestParam("userId") Long userId,
			@RequestParam("password") String password,
			@RequestParam("confirmPassword") String cPassword,
			Map<String, Object> model) throws 
			PasswordException {
		userService.changePassword(userId, password, cPassword);
		model.put("id", universityId);
		model.put(SUCCESS, "alert.passwordchanged");
		return "redirect:/" + url1 + "/" + url2 + "/EditUniversity";
	}



	@RequestMapping(value = { "/{url1}/{url2}/ResetInstructorPassword" }, method = RequestMethod.POST)
	public String resetInstructorPassword(
			@PathVariable(value = "url1") String url1,
			@PathVariable(value = "url2") String url2,
			@RequestParam("userId") Long userId,
			@RequestParam("password") String password,
			@RequestParam("confirmPassword") String cPassword,
			Map<String, Object> model) {
		try {
			userService.changePassword(userId, password, cPassword);
		} catch (BusinessException e) {
			model.put(SUCCESS, "alert.passwordchangeexception");
			LOGGER.info(e);
			return "redirect:/" + url1 + "/" + url2 + "/EditInstructor";
		}
		model.put("id", userId);
		model.put(SUCCESS, "alert.passwordchanged");
		return "redirect:/" + url1 + "/" + url2 + "/EditInstructor";
	}

	@RequestMapping("/ForgotPassword")
	public String forgotPassword() 
	{
		return "common/login/fpassword";
	}

	@RequestMapping("/Registration")
	public String addRegistration() {

		return "admin/mngt/companymngt/addcompany";
	}


	@RequestMapping(value = "/SubmitForgotPassword")
	public String submitForgotPassword(
			@RequestParam(value = "loginId", required = true) String loginId,
			@RequestParam(value = "client", required = false) String client,
			Map<String, Object> model) {

		String url="http://atmanetwork.in";




		boolean error = false;
		try {
			forgotPasswordAndLoginService.sendForgetPasswordLink(loginId, url);
			model.put(SUCCESS, "alert.forgotPasswordSent");
		} catch (NoSuchUserException e) {
			error = true;
			model.put(ERROR_MSG, "alert.logindoesnotexists");
			LOGGER.info(e);
		}
		if (client != null && ANDROID.equalsIgnoreCase(client)) {
			if (error) {
				return "common/error";
			} else {
				return "common/success";
			}
		}
		return "visitor/mngt/login";
	}

	@RequestMapping(value = "/ResetPassword")
	public String resetPassword(
			@RequestParam(value = "token", required = true) String token,
			Map<String, Object> model) throws NoSuchUserException {
		boolean error = false;
		User user = new User();
		try{
			user = userService.findByPasswordToken(token);
		}
		catch(NoSuchUserException e){
			error = true;
			LOGGER.info(e);
		}
		if(!error){
			user.setPasswordResetKey(null);
			model.put("token", token);
			return "common/login/rpassword";
		}
		else{
			model.put("errorCode", "alert.linkexpired");
			return "visitor/mngt/login";
		}
	}

	@RequestMapping(value = "/SaveMember")
	public String saveMember(
			@RequestParam("firstName") String firstName,
			@RequestParam("lastName") String lastName,
			@RequestParam("emailId") String emailId,
			@RequestParam("mobile") String mobile,
			@RequestParam("loginId") String loginId,
			@RequestParam("password") String password,
			@RequestParam("chosenStateId") Long stateId,
			@RequestParam("chosenCityId") Long cityId,
			@RequestParam("chosenCountryId") Long countryId,
			@RequestParam("memberPincode") String pincode,
			@RequestParam(value = "birthday") String birthday,
			@RequestParam(value = "memberGender") String gen,
			@RequestParam(value = "memberAddressLine1") String addressLine1,
			@RequestParam(value = "memberAddressLine2") String addressLine2,
			HttpServletRequest request,
			Map<String, Object> model) throws IOException {
		User user;

		String gender=gen;
		SimpleDateFormat formatter = new SimpleDateFormat("dd-mm-yyyy");
		Date birthdayDate = null;
		try {
			birthdayDate = formatter.parse(birthday);
		} catch (Exception e) {
			birthdayDate = null;
			LOGGER.info(e);
		}
		String address = addressLine1 + " " + addressLine2 + " " + pincode;
		if ("Select".equals(gender)) {
			gender = "";
		}

		try {
			CityMaster city = new CityMaster();
			StateMaster state = new StateMaster();
			CountryMaster country = new CountryMaster();
			city.setCityId(cityId);
			state.setStateId(stateId);
			country.setCountryId(countryId);

			user = userService.save(firstName, lastName, loginId, password,
					emailId, mobile, cityId, stateId, countryId, pincode,
					address, gender, birthdayDate);
			authenticateUserAndSetSession(user, request);

		}

		catch (DuplicateEmailException | DuplicateLoginException e) {
			model.put(ERROR, e.getErrorCode());

			LOGGER.info(e);

			List<CityMaster> cityList = cityService.findByActive(true);
			List<CountryMaster> countryList = countryService.findByActive(true);
			List<StateMaster> stateList = stateService.findByActive(true);

			model.put("firstName", firstName);
			model.put("lastName", lastName);
			model.put("loginId", loginId);
			model.put("emailId", emailId);
			model.put("mobile", mobile);
			model.put("pincode", pincode);
			model.put("addressLine1", addressLine1);
			model.put("addressLine2", addressLine2);
			model.put("gender", gender);
			model.put("cityList", cityList);
			model.put(COUNTRY_LIST, countryList);
			model.put("stateList", stateList);
			model.put(ERROR, e.getErrorCode());

			return "common/login/memberRegistration";
		}





		model.put("success1", "alert.memberregistersuccessfully");
		model.put("firstTimeLogin", "firstTimeLogin");
		userService.sendRegstrationMail(user, null, null, password);
		return "redirect:/StudentDashboard/";
	}

	@RequestMapping(value = "/Register")
	public String registerMember(@RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
			@RequestParam(value = ERROR, required = false, defaultValue = "") String error,
			Map<String, Object> model) {
		if (success != null && !success.isEmpty()) {
			model.put(SUCCESS, success);
		} else if (error != null && !error.isEmpty()) {
			model.put(ERROR, error);
		}

		List<CountryMaster> countryList = countryService.findByActive(true);
		List<Object[]> educationSectionList=educationSectorService.getAllSector();
		List<Map<String, Object>> organisationType = new ArrayList<>(); 
		for(Object[] c:educationSectionList){ 
			Map< String, Object> mmm= new HashMap<>();
			mmm.put("id", c[0]);
			mmm.put("educationtype", c[1]);
			organisationType.add(mmm);
		}

		model.put("organisationType", organisationType);
		model.put(COUNTRY_LIST, countryList);
		model.put(SUCCESS, "alert.enquiryregistersuccessfully");
		return "common/login/companyregister";
	}

	@RequestMapping(value = "/SubmitResetPassword")
	public String submitResetPassword(
			@RequestParam(value = "token", required = true) String token,
			@RequestParam(value = "password2", required = true) String password2,
			Map<String, Object> model) throws NoSuchUserException {
		forgotPasswordAndLoginService.updatePassword(token, password2);
		model.put(SUCCESS, "alert.resetPasswordSent");
		return "visitor/mngt/login";
	}


	@RequestMapping("/ValidateLogin")
	@ResponseBody
	public String validateLogin(@RequestParam("loginId") String loginId)
	{
		Gson gson = new Gson();
		try {
			 userService.findByLoginId(loginId);
			return gson.toJson("exist");
		} catch (NoSuchUserException e) {
		    LOGGER.info(e);
			return gson.toJson("available");
		}
	}

	@RequestMapping("/ValidateEmailId")
	@ResponseBody
	public String validateEmailId(@RequestParam("emailId") String emailId)
	{
		Gson gson = new Gson();
		try {
		    userService.findByEmailId(emailId);
			return gson.toJson("exist");
		} catch (NoSuchUserException e) {
			LOGGER.info(e);
			return gson.toJson("available");
		}
	}

	@RequestMapping(value = "/Error400")
	public String error400() {
		return "common/errors/error_400";
	}

	@RequestMapping(value = "/Error401")
	public String error401() {
		return "common/errors/error_401";
	}

	@RequestMapping(value = "/Error403")
	public String error403() {
		return "common/errors/error_403";
	}

	@RequestMapping(value = "/Error404")
	public String error404() {
		return "common/errors/error_404";
	}

	@RequestMapping(value = "/Error500")
	public String error500() {
		return "common/errors/error_500";
	}

	@RequestMapping(value = "/Error503")
	public String error503() {
		return "common/errors/error_503";
	}

	private void authenticateUserAndSetSession(User user,
			HttpServletRequest request) {
		UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
				user.getUsername(), user.getPassword());
		request.getSession();

		token.setDetails(new MyWebAuthenticationDetails(request));
		Authentication authenticatedUser = authenticationManager
				.authenticate(token);

		SecurityContextHolder.getContext().setAuthentication(authenticatedUser);
	}

	@RequestMapping("/MemberCaptcha")
	@ResponseBody
	public void memberCaptchaView() {
		/*empty method*/
	}

	@RequestMapping(value = "/Termsofuse")
	public String showTermsofuse() {
		return "common/login/termsofuse";
	}
}
