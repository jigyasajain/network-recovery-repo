package com.astrika.skill.controller.superadmin;

import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.astrika.categorymngt.exception.DuplicateCategoryException;
import com.astrika.categorymngt.model.CategoryMaster;
import com.astrika.categorymngt.service.CategoryService;
import com.astrika.common.exception.DuplicateCityException;
import com.astrika.common.exception.DuplicateEmailException;
import com.astrika.common.exception.DuplicateLanguageException;
import com.astrika.common.exception.DuplicateLoginException;
import com.astrika.common.exception.DuplicateStateException;
import com.astrika.common.exception.NoSuchAreaException;
import com.astrika.common.exception.NoSuchCityException;
import com.astrika.common.exception.NoSuchCountryException;
import com.astrika.common.exception.NoSuchCurrencyException;
import com.astrika.common.exception.NoSuchLanguageException;
import com.astrika.common.exception.NoSuchUserException;
import com.astrika.common.exception.PasswordException;
import com.astrika.common.model.CurrencyMaster;
import com.astrika.common.model.ImageMaster;
import com.astrika.common.model.InviteStatus;
import com.astrika.common.model.Role;
import com.astrika.common.model.User;
import com.astrika.common.model.UserStatus;
import com.astrika.common.model.location.CityMaster;
import com.astrika.common.model.location.CountryMaster;
import com.astrika.common.model.location.StateMaster;
import com.astrika.common.service.AreaService;
import com.astrika.common.service.CityService;
import com.astrika.common.service.CountryService;
import com.astrika.common.service.CurrencyExchangeRateService;
import com.astrika.common.service.CurrencyService;
import com.astrika.common.service.DocumentService;
import com.astrika.common.service.ImageService;
import com.astrika.common.service.LanguageService;
import com.astrika.common.service.StateService;
import com.astrika.common.service.UserService;
import com.astrika.common.util.PropsValues;
import com.astrika.common.util.RandomCodeGenerator;
import com.astrika.companymngt.exception.DuplicateCompanyException;
import com.astrika.companymngt.model.company.CompanyMaster;
import com.astrika.companymngt.model.module.ModuleMaster;
import com.astrika.companymngt.model.personaldetails.PersonalInfo;
import com.astrika.companymngt.service.CompanyService;
import com.astrika.companymngt.service.ModuleService;
import com.astrika.companymngt.service.PersonalInfoService;
import com.astrika.documentmngt.exception.DocumentPresentException;
import com.astrika.documentmngt.exception.DuplicateDocumentException;
import com.astrika.documentmngt.exception.DuplicateLinkException;
import com.astrika.documentmngt.model.AtmaDocumentMaster;
import com.astrika.documentmngt.service.AtmaDocumentService;
import com.astrika.forummngt.exception.DuplicateForumException;
import com.astrika.forummngt.model.Comments;
import com.astrika.forummngt.model.ForumMaster;
import com.astrika.forummngt.model.ForumTag;
import com.astrika.forummngt.service.CommentService;
import com.astrika.forummngt.service.ForumService;
import com.astrika.globalsponsoredmngt.exception.ContactImagePresentException;
import com.astrika.globalsponsoredmngt.model.GlobalSponsoredManagement;
import com.astrika.globalsponsoredmngt.service.GlobalSponsoredService;
import com.astrika.kernel.exception.BusinessException;
import com.astrika.kernel.exception.CustomException;
import com.astrika.modulemngt.exception.DuplicateModuleException;
import com.astrika.moduleusermanagement.model.ModuleUser;
import com.astrika.moduleusermanagement.model.NextSteps;
import com.astrika.moduleusermanagement.service.ModuleUserService;
import com.astrika.moduleusermanagement.service.NextStepsService;
import com.astrika.registrationmngt.service.EnquiryService;
import com.astrika.surveymngt.exception.DuplicateLifeStageException;
import com.astrika.surveymngt.model.LifeStageResultMaster;
import com.astrika.surveymngt.model.LifeStages;
import com.astrika.surveymngt.model.SurveyBuilder;
import com.astrika.surveymngt.model.SurveyManager;
import com.astrika.surveymngt.model.SurveyResult;
import com.astrika.surveymngt.model.SurveyStatus;
import com.astrika.surveymngt.model.VisitorSurveyBuilder;
import com.astrika.surveymngt.service.LifeStageResultService;
import com.astrika.surveymngt.service.SurveyBuilderService;
import com.astrika.surveymngt.service.SurveyManagerService;
import com.astrika.surveymngt.service.SurveyResultService;
import com.astrika.surveymngt.service.VisitorSurveyBuilderService;
import com.astrika.universitymngt.model.Status;
import com.astrika.weightagemngt.model.ProfileWeightage;
import com.astrika.weightagemngt.model.SurveyWeightage;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


@Controller
public class MasterWebController {

    @Autowired
    private AreaService areaService;

    @Autowired
    private PropsValues props;

    @Autowired
    private CityService cityService;

    @Autowired
    private StateService stateService;

    @Autowired
    private CountryService countryService;

    @Autowired
    private CurrencyService currencyService;

    @Autowired
    private LanguageService languageService;

    @Autowired
    private PropsValues propsValue;

    @Autowired
    private UserService userService;

    @Autowired
    private CurrencyExchangeRateService currencyExchangeRateService;

    @Autowired
    private DocumentService documentService;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private ModuleService moduleService;

    @Autowired
    private ImageService imageService;

    @Autowired
    private AtmaDocumentService atmadocumentService;

    @Autowired
    private CategoryService categoryService;

    private static final Logger LOGGER = Logger.getLogger(MasterWebController.class);

    @Autowired
    private ModuleUserService moduleUserService;

    @Autowired
    private EnquiryService enquiryService;

    @Autowired
    private ForumService forumService;

    @Autowired
    private CommentService commentService;

    @Autowired
    private SurveyBuilderService surveyBuilderService;

    @Autowired
    private SurveyManagerService surveyManagerService;

    @Autowired
    private VisitorSurveyBuilderService visitorSurveyBuilderService;

    @Autowired
    private GlobalSponsoredService globalSponsoredService;

    @Autowired
    private NextStepsService nextStepsService;

    @Autowired
    private SurveyResultService surveyResultService;

    @Autowired
    private LifeStageResultService lifeStageResultService;

    @Autowired
    private PersonalInfoService personalInfoService;

    private static final String COUNTRY_LIST="countryList";
    private static final String STATE="state";
    private static final String ERROR="error";
    private static final String SUCCESS="success";
    private static final String REDIRECT="redirect:/";
    private static final String MODULE1="module1";
    private static final String GLOBAL_SPONSOR_IMAGES="GlobalSponsorImages";
    private static final String MODULE_LIST="moduleList";
    private static final String BLUEPRINT_IMAGES="BlueprintImages";
    private static final String DOCUMENT="Document";
    private static final String BLUE_PRINT="BluePrint";
    private static final String DOCUMENT_MASTER="documentMaster";
    private static final String SECTIONS="sectionList";
    private static final String CATEGORY_LIST="categoryList";
    private static final String LIFE_STAGE_LIST="lifeStageList";
    private static final String STATE_LIST="stateList";
    private static final String CITY="/City";
    private static final String ALERT="alert.orgdetails3savedsuccessfully";

    List<String> sectionList = Arrays.asList(BLUE_PRINT,"Toolbox","Examples","Reading");

    @Autowired
    @Qualifier("authManager")
    protected AuthenticationManager authenticationManager;

    private User getLogedInUser() {
        User user = null;
        Object object = SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        if (object instanceof User)
            user = (User) object;

        return user;
    }

    @ExceptionHandler(BusinessException.class)
    public String handleBusinessException(HttpServletRequest request,
            BusinessException ex) {
        request.setAttribute("errorMsg", ex.getErrorCode());
        return "common/error";
    }

    @RequestMapping("/{url1}/{url2}/AddState")
    public String addState( Map<String, Object> model) {
        List<CountryMaster> countryList = countryService.findByActive(true);
        model.put(COUNTRY_LIST, countryList);
        return "admin/superadmin/masters/location/addstate";
    }

    @RequestMapping(value = "/{url}/SaveState")
    public String saveState(@PathVariable("url") String url,
            @RequestParam("stateName") String stateName,
            @RequestParam("chosenCountryId") long countryId,
            Map<String, Object> model) throws NoSuchCountryException {
        CountryMaster country = countryService.findById(countryId);
        try {
            stateService.save(stateName, country.getCountryId());
        } catch (DuplicateStateException e) {
            LOGGER.info(e);
            StateMaster state = new StateMaster();
            state.setStateName(stateName);
            state.setCountry(country);
            List<CountryMaster> countryList = countryService.findByActive(true);
            model.put(STATE, state);
            model.put(COUNTRY_LIST, countryList);
            model.put(ERROR, e.getErrorCode());
            return "admin/superadmin/masters/location/addstate";
        }
        model.put(SUCCESS, "alert.statesaved");
        return REDIRECT + url + "/StateList";
    }

    @RequestMapping("/{url1}/{url2}/EditState")
    public String editState(
            @RequestParam(value = "id") Long id,
            Map<String, Object> model) throws BusinessException {
        StateMaster state = stateService.findByStateId(id);

        List<CountryMaster> countryList = countryService.findByActive(true);
        model.put(STATE, state);
        model.put(COUNTRY_LIST, countryList);
        return "admin/superadmin/masters/location/editstate";
    }

    @RequestMapping("/*/StateList")
    public String showState(
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            Map<String, Object> model) throws NoSuchCityException {


        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        }
        return "admin/superadmin/masters/location/stategrid";
    }

    @RequestMapping("/StateList/{url}")
    @ResponseBody
    public String getStateList(
            @PathVariable("url") String type,
            @RequestParam(value = "sEcho", required = false, defaultValue = "0") Integer sEcho,
            @RequestParam(value = "iDisplayStart", required = false, defaultValue = "0") Integer iDisplayStart,
            @RequestParam(value = "iDisplayLength", required = false, defaultValue = "0") Integer iDisplayLength,
            HttpServletRequest request,
            Map<String, Object> model)  {

        int sortColNo = 0;
        String sSearch = null;
        String sortColDir = "";

        try {
            sortColNo = Integer.parseInt((String) request
                    .getParameter("iSortCol_0"));
            sortColDir = (String) request.getParameter("sSortDir_0");
            sSearch = (String) request.getParameter("sSearch");
            sSearch = sSearch == null ? "" : sSearch;
        } catch (Exception e) {
            sortColDir = "asc";
            sortColNo = 0;
            LOGGER.info(e);
        }
        int pageNo = (iDisplayStart + iDisplayLength) / iDisplayLength;
        int count = (pageNo-1)*iDisplayLength;
        Page<Object[]> statePage = null;
        List<Object[]> stateList1;
        List<Object[]> stateList = new ArrayList<>();

        if("Active".equals(type)){
            statePage= stateService.findAllRequiredParaByActive(
                    true, sortColNo, sortColDir, sSearch,
                    pageNo-1, iDisplayLength);
            stateList1 = statePage.getContent();

            for (Object[] dataArray : stateList1) {
                Object[] dataArray1 = Arrays.copyOf(dataArray, 10);

                String editDelete = "<div style='position: relative; text-align:center'> "+
                        "<a href='" + propsValue.CONTEXT_URL
                        + "ManageState/"
                        + "StateList/EditState" + "?id="
                        + dataArray1[0] +"'"+ " data-toggle='tooltip' title='Edit' class='btn btn-xs btn-default'><i class='fa fa-pencil'></i> </a>"+
                        "<a href='#delete_city_popup' data-toggle='modal' onclick='deleteCity("
                        + dataArray1[0] + ")' title='Delete' class='btn btn-xs btn-danger'><i class='fa fa-times'></i></a>" +               
                        "</div>";

                dataArray1[3] = editDelete;

                if("asc".equals(sortColDir)){
                    count++;
                }
                else{
                    count--;
                }
                dataArray1[0] = count;
                stateList.add(dataArray1);
            }

        }else if("Inactive".equals(type)){


            statePage= stateService.findAllRequiredParaByActive(
                    false, sortColNo, sortColDir, sSearch,
                    pageNo-1, iDisplayLength);
            stateList1 = statePage.getContent();
            for (Object[] dataArray : stateList1) {
                Object[] dataArray1 = Arrays.copyOf(dataArray, 10);

                String restore = "<div style='position: relative; text-align:center'> "+
                        "<a href='#restore_City_popup' data-toggle='modal' "+
                        "       onclick='restoreCity("+dataArray1[0]+")' "+
                        "       title='Restore' class='btn btn-xs btn-success'><i "+
                        "       class='fa fa-plus'></i> </a>"+
                        "</div>";

                dataArray1[3] = restore;
                if("asc".equals(sortColDir)){
                    count++;
                }
                else{
                    count--;
                }
                dataArray1[0] = count;
                stateList.add(dataArray1);
            }
        }

        Long iTotalRecords = (long) statePage.getTotalElements();
        model.put("draw", pageNo);
        model.put("iTotalRecords", iTotalRecords);
        model.put("iTotalDisplayRecords", iTotalRecords);
        model.put("sEcho", sEcho + "");
        model.put("iDisplayLength", "10");
        model.put("aaData", stateList);
        Gson gson = new GsonBuilder().create();
        return gson.toJson(model);
    }

    @RequestMapping(value = "/{url1}/{url2}/UpdateState")
    public String updateState(@PathVariable("url1") String url1,
            @RequestParam("stateId") Long stateId,
            @RequestParam("stateName") String stateName,
            @RequestParam("chosenCountryId") long countryId,
            Map<String, Object> model) throws IOException {


        try {
            stateService.update(stateId, stateName, countryId);
        } catch (DuplicateStateException e) {
            LOGGER.info(e);
            StateMaster state = new StateMaster();
            state.setStateId(stateId);
            state.setStateName(stateName);
            CountryMaster country = countryService.findById(countryId);
            state.setCountry(country);
            model.put(STATE, state);
            List<CountryMaster> countryList = countryService.findByActive(true);
            model.put(COUNTRY_LIST, countryList);
            model.put(ERROR, e.getErrorCode());
            return "admin/superadmin/masters/location/editstate";
        }
        model.put(SUCCESS, "alert.stateupdated");
        return REDIRECT + url1 + "/StateList";
    }

    @RequestMapping(value = "/{url}/RestoreState")
    public String restoreState(@PathVariable("url") String url,
            @RequestParam(value = "id", required = false) Long id,
            Map<String, Object> model) throws NoSuchCityException {
        stateService.restore(id);
        model.put(SUCCESS, "alert.staterestored");
        return REDIRECT + url + "/StateList";
    }

    @RequestMapping(value = "/{url}/DeleteState")
    public String deleteState(@PathVariable("url") String url,
            @RequestParam(value = "id", required = false) Long id,
            Map<String, Object> model) throws IOException
            {

        stateService.delete(id);
        model.put(SUCCESS, "alert.statedeleted");
        return REDIRECT + url + "/StateList";
            }

    @RequestMapping(value = "/*/RestoreCountry")
    public String restoreCountry(
            @RequestParam(value = "id", required = false) Long id,
            Map<String, Object> model) throws NoSuchCountryException {

        countryService.restore(id);
        model.put(SUCCESS, "alert.countryrestored");
        return "redirect:/*/Country";
    }

    @RequestMapping(value = "/*/DeleteCountry")
    public String deleteCountry(
            @RequestParam(value = "id", required = false) Long countryId,
            Map<String, Object> model) throws IOException
            {
        countryService.delete(countryId);
        model.put(SUCCESS, "alert.countrydeleted");
        return "redirect:/*/Country";
            }

    @RequestMapping(value = "/*/SaveCountry")
    public String addCountry(
            @RequestParam(value = "countryId", required = false) Long countryId,
            @RequestParam("countryName") String countryName,
            Map<String, Object> model) throws IOException {


        if (countryId > 0) { // update country
            countryService.update(countryId, countryName);
            model.put(SUCCESS, "alert.countryupdated");
        } else { 

            model.put(SUCCESS, "alert.countrysaved");
        }
        return "redirect:/*/Country";
    }

    @RequestMapping(value = "/*/SaveCurrency")
    public String addCurrency(
            @RequestParam(value = "currencyId", required = false) Long currencyId,
            @RequestParam("currencyName") String currencyName,
            @RequestParam("currencySign") String currencySign,
            @RequestParam("currencyCode") String currencyCode,
            Map<String, Object> model) throws NoSuchCurrencyException
            {
        if (currencyId > 0) {
            currencyService.update(currencyId, currencyName, currencySign,
                    currencyCode);
            model.put(SUCCESS, "alert.currencyupdated");
        } else { // insert currency
            CurrencyMaster currency = currencyService.save(currencyName,
                    currencySign, currencyCode);
            BigDecimal rate = BigDecimal.ZERO;
            if (currency.getCurrencyId() == propsValue.baseCurrency) {
                rate = BigDecimal.ONE;
            }
            currencyExchangeRateService.save(propsValue.baseCurrency,
                    currency.getCurrencyId(), rate);
            model.put(SUCCESS, "alert.currencysaved");
        }
        return "redirect:/*/Currency";
            }

    @RequestMapping(value = "/*/DeleteCurrency")
    public String deleteCurrency(
            @RequestParam(value = "id", required = false) Long id,
            Map<String, Object> model) throws IOException
            {


        currencyExchangeRateService.activeDeactive(id, false);
        currencyService.delete(id);
        model.put(SUCCESS, "alert.currencydeleted");
        return "redirect:/*/Currency";
            }

    @RequestMapping(value = "/*/RestoreCurrency")
    public String restoreCurrency(
            @RequestParam(value = "id", required = false) Long id,
            Map<String, Object> model) throws NoSuchLanguageException
            {
        currencyExchangeRateService.activeDeactive(id, true);
        currencyService.restore(id);
        model.put(SUCCESS, "alert.currencyrestored");
        return "redirect:/*/Currency";
            }


    @RequestMapping(value = "/{url1}/{url2}/SaveSponsoredImage")
    public String saveSponsoredImage(
            @PathVariable("url1") String url1,
            @PathVariable("url2") String url2,
            @RequestParam("moduleId") Long moduleId,
            @RequestParam("link") String link,
            @RequestParam(value = "imageChooser1", required = false) MultipartFile sponsoredimage,
            Map<String, Object> model) throws IOException {

        ModuleMaster module = moduleService.findByModuleId(moduleId);
        if (sponsoredimage != null && sponsoredimage.getSize() > 0) {
            String filename = sponsoredimage.getOriginalFilename();
            String ext = filename.substring(filename.lastIndexOf("."));
            ImageMaster sponsoredImage = imageService.addImage(sponsoredimage.getBytes(),
                    ext, "SponsoredImage" + ext, true, false, module
                    .getModuleName().toString());
            if (module.getSponsoredImage() != null) {
                imageService.delete(module.getSponsoredImage().getImageId());
            }
            module.setSponsoredImage(sponsoredImage);
        }
        try {

            module.setLink(link);
            moduleService.save(module);
        } catch (Exception e) {
            ModuleMaster module1 = new ModuleMaster();
            module1.setLink(module.getLink());
            module1.setSponsoredImage(module.getSponsoredImage());
            model.put(MODULE1, module1);
            LOGGER.info(e);
            return "admin/mngt/sponsoredimagemngt/addsponsoredimage";
        }
        model.put(SUCCESS, "alert.savedsponsoredimage");
        return REDIRECT + url1 + "/" + url2;
    }

    @RequestMapping(value = "/{url1}/{url2}/SavePromotionalImage")
    public String savePromoImage(
            @PathVariable("url1") String url1,
            @PathVariable("url2") String url2,
            @RequestParam("moduleId") Long moduleId,
            @RequestParam(value = "imageChooser1", required = false) MultipartFile promoimage,
            Map<String, Object> model) throws IOException {

        ModuleMaster module = moduleService.findByModuleId(moduleId);

        if (promoimage != null && promoimage.getSize() > 0) {
            String filename = promoimage.getOriginalFilename();
            String ext = filename.substring(filename.lastIndexOf("."));
            ImageMaster promoImage = imageService.addImage(promoimage.getBytes(), ext,
                    "PromotionalImage" + ext, true, false, module
                    .getModuleName().toString());
            if (module.getPromotionalImage() != null) {
                imageService.delete(module.getPromotionalImage().getImageId());
            }
            module.setPromotionalImage(promoImage);
        }
        try {
            moduleService.save(module);
        } catch (Exception e) {
            LOGGER.info(e);
            ModuleMaster module1 = new ModuleMaster();
            module1.setPromotionalImage(module.getPromotionalImage());
            model.put(MODULE1, module1);
            return "admin/mngt/promotionalimagemngt/addpromotionalimage";
        }
        model.put(SUCCESS, "alert.savedpromoimage");
        return REDIRECT + url1 + "/" + url2;
    }

    @RequestMapping(value = "/{url1}/{url2}/SaveGlobalSponsoredImage")
    public String saveGlobalSponsoredImage(
            @PathVariable("url1") String url1,
            @PathVariable("url2") String url2,
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            @RequestParam("imageType") String imageType,
            @RequestParam(value = "imageChooser1", required = false) MultipartFile contactimage,
            HttpServletRequest request,
            Map<String, Object> model) throws IOException {

        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        } else if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }

        boolean exceptionFlag = false;
        HttpSession session = request.getSession(true);
        List<ImageMaster> globalSponsorImage;

        if("Global Sponsor".equals(imageType) && session.getAttribute(GLOBAL_SPONSOR_IMAGES) == null){
            GlobalSponsoredManagement imageManagement = new GlobalSponsoredManagement();
            imageManagement.setImageType(imageType);
            model.put("sponsorMaster", imageManagement);
            model.put(ERROR, "alert.youneedtoselectatleastoneimage");
            return "admin/mngt/globalsponsoredmngt/addglobalsponsoredimage";
        }
        else{
            globalSponsorImage = (List<ImageMaster>) session
                    .getAttribute(GLOBAL_SPONSOR_IMAGES);
        }
        try {
            globalSponsoredService.save(globalSponsorImage, contactimage,
                    imageType);
            session.removeAttribute(GLOBAL_SPONSOR_IMAGES);
        } catch (ContactImagePresentException e) {

            exceptionFlag = true;
            model.put(ERROR, e.getErrorCode());
            LOGGER.info(e);
        }

        if (!exceptionFlag) {
            model.put(SUCCESS, "alert.imagesaved");
            return REDIRECT + url1 + "/" + url2;
        } else {
            GlobalSponsoredManagement imageManagement = new GlobalSponsoredManagement();
            imageManagement.setImageType(imageType);
            model.put("sponsorMaster", imageManagement);
            return "admin/mngt/globalsponsoredmngt/addglobalsponsoredimage";
        }

    }

    @RequestMapping("/*/*/UploadSponsorImage")
    public String uploadSponsorImage(@RequestParam("file") MultipartFile file,
            HttpServletRequest request) throws IOException {
        String filename = file.getOriginalFilename();


        String ext = filename.substring(filename.lastIndexOf("."));
        ImageMaster image = imageService.addImage(file.getBytes(), ext,
                "GlobalSponsorImg" + ext, true, false, GLOBAL_SPONSOR_IMAGES);
        HttpSession session = request.getSession(true);
        List<ImageMaster> globalSponsorImage = (List<ImageMaster>) session
                .getAttribute(GLOBAL_SPONSOR_IMAGES);
        if (globalSponsorImage == null) {
            globalSponsorImage = new ArrayList<>();
        }
        globalSponsorImage.add(image);
        session.setAttribute(GLOBAL_SPONSOR_IMAGES, globalSponsorImage);
        return file.getOriginalFilename();
    }

    @RequestMapping(value = "/ManageSurvey/CreateSurvey/SaveSurvey")
    public String saveSurvey(
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            @RequestParam("moduleId") Long moduleId,
            @RequestParam("1") String one, @RequestParam("2") String two,
            @RequestParam("3") String three, @RequestParam("4") String four,
            @RequestParam("5") String five, @RequestParam("6") String six,
            @RequestParam("7") String seven, @RequestParam("8") String eight,
            @RequestParam("9") String nine, @RequestParam("10") String ten,
            @RequestParam("11") String eleven,
            @RequestParam("12") String twelve,
            @RequestParam("13") String thirteen,
            @RequestParam("14") String forteen,
            @RequestParam("15") String fifteen,Map<String, Object> model) {

        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        } else if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }
        List<ModuleMaster> moduleList = moduleService.findByActive(true);

        ModuleMaster module = moduleService.findByModuleId(moduleId);
        List<SurveyBuilder> surveyBuilders = new ArrayList<>();

        SurveyBuilder surveyBuilder = new SurveyBuilder(1, one, module, true,
                LifeStages.LIFE_STAGE1, Status.PUBLISH);
        surveyBuilders.add(surveyBuilder);

        SurveyBuilder surveyBuilder1 = new SurveyBuilder(2, two, module, true,
                LifeStages.LIFE_STAGE1, Status.PUBLISH);
        surveyBuilders.add(surveyBuilder1);

        SurveyBuilder surveyBuilder2 = new SurveyBuilder(3, three, module,
                true, LifeStages.LIFE_STAGE1, Status.PUBLISH);
        surveyBuilders.add(surveyBuilder2);

        SurveyBuilder surveyBuilder3 = new SurveyBuilder(4, four, module, true,
                LifeStages.LIFE_STAGE2, Status.PUBLISH);
        surveyBuilders.add(surveyBuilder3);

        SurveyBuilder surveyBuilder4 = new SurveyBuilder(5, five, module, true,
                LifeStages.LIFE_STAGE2, Status.PUBLISH);
        surveyBuilders.add(surveyBuilder4);

        SurveyBuilder surveyBuilder5 = new SurveyBuilder(6, six, module, true,
                LifeStages.LIFE_STAGE2, Status.PUBLISH);
        surveyBuilders.add(surveyBuilder5);

        SurveyBuilder surveyBuilder6 = new SurveyBuilder(7, seven, module,
                true, LifeStages.LIFE_STAGE3, Status.PUBLISH);
        surveyBuilders.add(surveyBuilder6);

        SurveyBuilder surveyBuilder7 = new SurveyBuilder(8, eight, module,
                true, LifeStages.LIFE_STAGE3, Status.PUBLISH);
        surveyBuilders.add(surveyBuilder7);

        SurveyBuilder surveyBuilder8 = new SurveyBuilder(9, nine, module, true,
                LifeStages.LIFE_STAGE3, Status.PUBLISH);
        surveyBuilders.add(surveyBuilder8);

        SurveyBuilder surveyBuilder9 = new SurveyBuilder(10, ten, module, true,
                LifeStages.LIFE_STAGE4, Status.PUBLISH);
        surveyBuilders.add(surveyBuilder9);

        SurveyBuilder surveyBuilder10 = new SurveyBuilder(11, eleven, module,
                true, LifeStages.LIFE_STAGE4, Status.PUBLISH);
        surveyBuilders.add(surveyBuilder10);

        SurveyBuilder surveyBuilder11 = new SurveyBuilder(12, twelve, module,
                true, LifeStages.LIFE_STAGE4, Status.PUBLISH);
        surveyBuilders.add(surveyBuilder11);

        SurveyBuilder surveyBuilder12 = new SurveyBuilder(13, thirteen, module,
                true, LifeStages.LIFE_STAGE5, Status.PUBLISH);
        surveyBuilders.add(surveyBuilder12);

        SurveyBuilder surveyBuilder13 = new SurveyBuilder(14, forteen, module,
                true, LifeStages.LIFE_STAGE5, Status.PUBLISH);
        surveyBuilders.add(surveyBuilder13);

        SurveyBuilder surveyBuilder14 = new SurveyBuilder(15, fifteen, module,
                true, LifeStages.LIFE_STAGE5, Status.PUBLISH);
        surveyBuilders.add(surveyBuilder14);

        surveyBuilderService.processSurveyBuilderUpdateAlongWithVistorSurveys(surveyBuilders,module);

        model.put(SUCCESS, "alert.questionsavedsucessfully");
        List<SurveyBuilder> list1 = surveyBuilderService
                .findByLifeStagesAndModuleId(LifeStages.LIFE_STAGE1, moduleId);
        List<SurveyBuilder> list2 = surveyBuilderService
                .findByLifeStagesAndModuleId(LifeStages.LIFE_STAGE2, moduleId);
        List<SurveyBuilder> list3 = surveyBuilderService
                .findByLifeStagesAndModuleId(LifeStages.LIFE_STAGE3, moduleId);
        List<SurveyBuilder> list4 = surveyBuilderService
                .findByLifeStagesAndModuleId(LifeStages.LIFE_STAGE4, moduleId);
        List<SurveyBuilder> list5 = surveyBuilderService
                .findByLifeStagesAndModuleId(LifeStages.LIFE_STAGE5, moduleId);

        for(ModuleMaster mu:moduleList){
            ModuleMaster module1 = moduleService
                    .findByModuleId(mu.getModuleId());
            List<SurveyBuilder> surveyList = surveyBuilderService.findByStatus(
                    Status.PUBLISH, module1.getModuleId());
            if (!surveyList.isEmpty()) {
                mu.setCompleted(true);
            }
        }

        model.put("list1", list1);
        model.put("list2", list2);
        model.put("list3", list3);
        model.put("list4", list4);
        model.put("list5", list5);
        model.put("moduleId", moduleId);
        model.put("mList", moduleList);

        return "admin/surveymngt/editsurvey";

    }

    @RequestMapping(value = "/ManageSurvey/ViewSurvey/SaveVisitorSurvey")
    public String saveVisitorSurvey(
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            @RequestParam("moduleId") Long moduleId,
            @RequestParam("lifeStage") String moduleLifestage,
            @RequestParam("highestTotal") String highestTotal,
            @RequestParam("act") boolean act, @RequestParam("1") String one,
            @RequestParam("2") String two, @RequestParam("3") String three,
            @RequestParam("4") String four, @RequestParam("5") String five,
            @RequestParam("6") String six, @RequestParam("7") String seven,
            @RequestParam("8") String eight, @RequestParam("9") String nine,
            @RequestParam("10") String ten, @RequestParam("11") String eleven,
            @RequestParam("12") String twelve,
            @RequestParam("13") String thirteen,
            @RequestParam("14") String forteen,
            @RequestParam("15") String fifteen,
            @RequestParam("testslider_1") int sliderone,
            @RequestParam("testslider_2") int slidertwo,
            @RequestParam("testslider_3") int sliderthree,
            @RequestParam("testslider_4") int sliderfour,
            @RequestParam("testslider_5") int sliderfive,
            @RequestParam("testslider_6") int slidersix,
            @RequestParam("testslider_7") int sliderseven,
            @RequestParam("testslider_8") int slidereight,
            @RequestParam("testslider_9") int slidernine,
            @RequestParam("testslider_10") int sliderten,
            @RequestParam("testslider_11") int slidereleven,
            @RequestParam("testslider_12") int slidertwelve,
            @RequestParam("testslider_13") int sliderthirteen,
            @RequestParam("testslider_14") int sliderfourteen,
            @RequestParam("testslider_15") int sliderfifteen,
            HttpServletRequest request,
            Map<String, Object> model) throws NoSuchUserException {

        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        } else if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }

        User user;
        user = (User) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();

        ModuleMaster module = moduleService.findByModuleId(moduleId);
        List<ModuleMaster> modules = moduleService.findByActive(true);

        CompanyMaster company = companyService.findByUserId(user.getUserId());
        List<VisitorSurveyBuilder> survey1 = visitorSurveyBuilderService
                .findByModuleIdandUserIdandCompanyId(moduleId,
                        user.getUserId(), company.getCompanyId());
        ModuleUser moduleUser = moduleUserService.findByCompanyIdAndModuleId(company.getCompanyId(), module.getModuleId());
        int totalCompletionRate = moduleUser.getTotalSurveyWeight();

        if(totalCompletionRate == 0){
            for(int i = 1; i<=15; i++){
                totalCompletionRate = totalCompletionRate + SurveyWeightage.QUESTION1.getId();
            }
        }

        List<SurveyBuilder> surveyBuilders = surveyBuilderService.findByModuleIdOrderByQuestionNo(moduleId);
        SurveyManager surveyManager1 = surveyManagerService
                .findByCompanyIdandOngoingSurvey(company.getCompanyId(), true);

        List<ModuleMaster> nextModule = moduleService.findTheNextRow(moduleId);
        long modId = 1;
        for(ModuleMaster mu : nextModule){
            modId = mu.getModuleId();
        }
        /**
         * If there is no next module, be on same
         */
        if(nextModule == null || nextModule.isEmpty()){
            modId = moduleId;
        }

		if (act == false) {//0 = save (can revisit, draft), 1 = submit (all done)

			if (!survey1.isEmpty() && survey1 != null) {
				//cleaning up any previous survey data before saving all
				visitorSurveyBuilderService.deleteAllByModuleIdandUserIdandCompanyId(moduleId, user.getUserId(), company.getCompanyId());
			}

            int surveyCompletionRate = 0;

            if(sliderone!=0){
                surveyCompletionRate = surveyCompletionRate + SurveyWeightage.QUESTION1.getId();
            }
            if(slidertwo!=0){
                surveyCompletionRate = surveyCompletionRate + SurveyWeightage.QUESTION2.getId();
            }
            if(sliderthree!=0){
                surveyCompletionRate = surveyCompletionRate + SurveyWeightage.QUESTION3.getId();
            }
            if(sliderfour!=0){
                surveyCompletionRate = surveyCompletionRate + SurveyWeightage.QUESTION4.getId();
            }
            if(sliderfive!=0){
                surveyCompletionRate = surveyCompletionRate + SurveyWeightage.QUESTION5.getId();
            }
            if(slidersix!=0){
                surveyCompletionRate = surveyCompletionRate + SurveyWeightage.QUESTION6.getId();
            }
            if(sliderseven!=0){
                surveyCompletionRate = surveyCompletionRate + SurveyWeightage.QUESTION7.getId();
            }
            if(slidereight!=0){
                surveyCompletionRate = surveyCompletionRate + SurveyWeightage.QUESTION8.getId();
            }
            if(slidernine!=0){
                surveyCompletionRate = surveyCompletionRate + SurveyWeightage.QUESTION9.getId();
            }
            if(sliderten!=0){
                surveyCompletionRate = surveyCompletionRate + SurveyWeightage.QUESTION10.getId();
            }
            if(slidereleven!=0){
                surveyCompletionRate = surveyCompletionRate + SurveyWeightage.QUESTION11.getId();
            }
            if(slidertwelve!=0){
                surveyCompletionRate = surveyCompletionRate + SurveyWeightage.QUESTION12.getId();
            }
            if(sliderthirteen!=0){
                surveyCompletionRate = surveyCompletionRate + SurveyWeightage.QUESTION13.getId();
            }
            if(sliderfourteen!=0){
                surveyCompletionRate = surveyCompletionRate + SurveyWeightage.QUESTION14.getId();
            }
            if(sliderfifteen!=0){
                surveyCompletionRate = surveyCompletionRate + SurveyWeightage.QUESTION15.getId();
            }

            int surveyCompletionPercent = 100 * surveyCompletionRate/totalCompletionRate;

            moduleUser.setTotalSurveyWeight(totalCompletionRate);
            moduleUser.setSurveyWeight(surveyCompletionRate);
            moduleUser.setSurveyWeightPercent(surveyCompletionPercent);
            moduleUserService.saveSurveyResult(moduleUser);

            VisitorSurveyBuilder visitorSurveyBuilder = new VisitorSurveyBuilder(
                    LifeStages.LIFE_STAGE1, module, 1, one, surveyBuilders.get(0).getQuestionExplanation() , sliderone, true,
                    Status.DRAFT, surveyManager1);
            visitorSurveyBuilderService.save(visitorSurveyBuilder);

            VisitorSurveyBuilder visitorSurveyBuilder1 = new VisitorSurveyBuilder(
                    LifeStages.LIFE_STAGE1, module, 2, two, surveyBuilders.get(1).getQuestionExplanation() ,slidertwo, true,
                    Status.DRAFT, surveyManager1);
            visitorSurveyBuilderService.save(visitorSurveyBuilder1);

            VisitorSurveyBuilder visitorSurveyBuilder2 = new VisitorSurveyBuilder(
                    LifeStages.LIFE_STAGE1, module, 3, three, surveyBuilders.get(2).getQuestionExplanation() ,sliderthree,
                    true, Status.DRAFT, surveyManager1);
            visitorSurveyBuilderService.save(visitorSurveyBuilder2);

            VisitorSurveyBuilder visitorSurveyBuilder3 = new VisitorSurveyBuilder(
                    LifeStages.LIFE_STAGE2, module, 4, four, surveyBuilders.get(3).getQuestionExplanation() ,sliderfour, true,
                    Status.DRAFT, surveyManager1);
            visitorSurveyBuilderService.save(visitorSurveyBuilder3);

            VisitorSurveyBuilder visitorSurveyBuilder4 = new VisitorSurveyBuilder(
                    LifeStages.LIFE_STAGE2, module, 5, five, surveyBuilders.get(4).getQuestionExplanation() ,sliderfive, true,
                    Status.DRAFT, surveyManager1);
            visitorSurveyBuilderService.save(visitorSurveyBuilder4);

            VisitorSurveyBuilder visitorSurveyBuilder5 = new VisitorSurveyBuilder(
                    LifeStages.LIFE_STAGE2, module, 6, six, surveyBuilders.get(5).getQuestionExplanation() ,slidersix, true,
                    Status.DRAFT, surveyManager1);
            visitorSurveyBuilderService.save(visitorSurveyBuilder5);

            VisitorSurveyBuilder visitorSurveyBuilder6 = new VisitorSurveyBuilder(
                    LifeStages.LIFE_STAGE3, module, 7, seven, surveyBuilders.get(6).getQuestionExplanation() ,sliderseven,
                    true, Status.DRAFT, surveyManager1);
            visitorSurveyBuilderService.save(visitorSurveyBuilder6);

            VisitorSurveyBuilder visitorSurveyBuilder7 = new VisitorSurveyBuilder(
                    LifeStages.LIFE_STAGE3, module, 8, eight, surveyBuilders.get(7).getQuestionExplanation() ,slidereight,
                    true, Status.DRAFT, surveyManager1);
            visitorSurveyBuilderService.save(visitorSurveyBuilder7);

            VisitorSurveyBuilder visitorSurveyBuilder8 = new VisitorSurveyBuilder(
                    LifeStages.LIFE_STAGE3, module, 9, nine, surveyBuilders.get(8).getQuestionExplanation() ,slidernine, true,
                    Status.DRAFT, surveyManager1);
            visitorSurveyBuilderService.save(visitorSurveyBuilder8);

            VisitorSurveyBuilder visitorSurveyBuilder9 = new VisitorSurveyBuilder(
                    LifeStages.LIFE_STAGE4, module, 10, ten, surveyBuilders.get(9).getQuestionExplanation() ,sliderten, true,
                    Status.DRAFT, surveyManager1);
            visitorSurveyBuilderService.save(visitorSurveyBuilder9);

            VisitorSurveyBuilder visitorSurveyBuilder10 = new VisitorSurveyBuilder(
                    LifeStages.LIFE_STAGE4, module, 11, eleven, surveyBuilders.get(10).getQuestionExplanation() ,slidereleven,
                    true, Status.DRAFT, surveyManager1);
            visitorSurveyBuilderService.save(visitorSurveyBuilder10);

            VisitorSurveyBuilder visitorSurveyBuilder11 = new VisitorSurveyBuilder(
                    LifeStages.LIFE_STAGE4, module, 12, twelve, surveyBuilders.get(11).getQuestionExplanation() ,slidertwelve,
                    true, Status.DRAFT, surveyManager1);
            visitorSurveyBuilderService.save(visitorSurveyBuilder11);

            VisitorSurveyBuilder visitorSurveyBuilder12 = new VisitorSurveyBuilder(
                    LifeStages.LIFE_STAGE5, module, 13, thirteen,surveyBuilders.get(12).getQuestionExplanation() ,
                    sliderthirteen, true, Status.DRAFT, surveyManager1);
            visitorSurveyBuilderService.save(visitorSurveyBuilder12);

            VisitorSurveyBuilder visitorSurveyBuilder13 = new VisitorSurveyBuilder(
                    LifeStages.LIFE_STAGE5, module, 14, forteen,surveyBuilders.get(13).getQuestionExplanation() ,
                    sliderfourteen, true, Status.DRAFT, surveyManager1);
            visitorSurveyBuilderService.save(visitorSurveyBuilder13);

            VisitorSurveyBuilder visitorSurveyBuilder14 = new VisitorSurveyBuilder(
                    LifeStages.LIFE_STAGE5, module, 15, fifteen, surveyBuilders.get(14).getQuestionExplanation() ,sliderfifteen,
                    true, Status.DRAFT, surveyManager1);
            visitorSurveyBuilderService.save(visitorSurveyBuilder14);

            model.put(SUCCESS, "alert.answersavedsucessfully");
            return "redirect:/ManageSurvey/TakeSurvey/"+modId;
        } else {//submit all done
        	
            int surveyCompletionRate = totalCompletionRate;
            int surveyCompletionPercent = 100 * surveyCompletionRate/totalCompletionRate;

            SurveyManager surveyManager = surveyManagerService
                    .findByCompanyIdandSurveyStatus(company.getCompanyId(),
                            SurveyStatus.PENDING);

            int totalModulesCompleted = surveyManager
                    .getTotalModulesCompleted();

            if (!survey1.isEmpty() && survey1 != null) {

                visitorSurveyBuilderService.deleteAllByModuleIdandUserIdandCompanyId(moduleId, user.getUserId(), company.getCompanyId());
            }

            VisitorSurveyBuilder visitorSurveyBuilder = new VisitorSurveyBuilder(
                    LifeStages.LIFE_STAGE1, module, 1, one, surveyBuilders.get(0).getQuestionExplanation(), sliderone, true,
                    Status.COMPLETED, surveyManager1);

            visitorSurveyBuilderService.save(visitorSurveyBuilder);

            VisitorSurveyBuilder visitorSurveyBuilder1 = new VisitorSurveyBuilder(
                    LifeStages.LIFE_STAGE1, module, 2, two, surveyBuilders.get(1).getQuestionExplanation(),slidertwo, true,
                    Status.COMPLETED, surveyManager1);
            visitorSurveyBuilderService.save(visitorSurveyBuilder1);

            VisitorSurveyBuilder visitorSurveyBuilder2 = new VisitorSurveyBuilder(
                    LifeStages.LIFE_STAGE1, module, 3, three,surveyBuilders.get(2).getQuestionExplanation(), sliderthree,
                    true, Status.COMPLETED, surveyManager1);
            visitorSurveyBuilderService.save(visitorSurveyBuilder2);

            VisitorSurveyBuilder visitorSurveyBuilder3 = new VisitorSurveyBuilder(
                    LifeStages.LIFE_STAGE2, module, 4, four, surveyBuilders.get(3).getQuestionExplanation(),sliderfour, true,
                    Status.COMPLETED, surveyManager1);
            visitorSurveyBuilderService.save(visitorSurveyBuilder3);

            VisitorSurveyBuilder visitorSurveyBuilder4 = new VisitorSurveyBuilder(
                    LifeStages.LIFE_STAGE2, module, 5, five, surveyBuilders.get(4).getQuestionExplanation(),sliderfive, true,
                    Status.COMPLETED, surveyManager1);
            visitorSurveyBuilderService.save(visitorSurveyBuilder4);

            VisitorSurveyBuilder visitorSurveyBuilder5 = new VisitorSurveyBuilder(
                    LifeStages.LIFE_STAGE2, module, 6, six, surveyBuilders.get(5).getQuestionExplanation(),slidersix, true,
                    Status.COMPLETED, surveyManager1);
            visitorSurveyBuilderService.save(visitorSurveyBuilder5);

            VisitorSurveyBuilder visitorSurveyBuilder6 = new VisitorSurveyBuilder(
                    LifeStages.LIFE_STAGE3, module, 7, seven, surveyBuilders.get(6).getQuestionExplanation(),sliderseven,
                    true, Status.COMPLETED, surveyManager1);
            visitorSurveyBuilderService.save(visitorSurveyBuilder6);

            VisitorSurveyBuilder visitorSurveyBuilder7 = new VisitorSurveyBuilder(
                    LifeStages.LIFE_STAGE3, module, 8, eight, surveyBuilders.get(7).getQuestionExplanation(),slidereight,
                    true, Status.COMPLETED, surveyManager1);
            visitorSurveyBuilderService.save(visitorSurveyBuilder7);

            VisitorSurveyBuilder visitorSurveyBuilder8 = new VisitorSurveyBuilder(
                    LifeStages.LIFE_STAGE3, module, 9, nine, surveyBuilders.get(8).getQuestionExplanation(),slidernine, true,
                    Status.COMPLETED, surveyManager1);
            visitorSurveyBuilderService.save(visitorSurveyBuilder8);

            VisitorSurveyBuilder visitorSurveyBuilder9 = new VisitorSurveyBuilder(
                    LifeStages.LIFE_STAGE4, module, 10, ten, surveyBuilders.get(9).getQuestionExplanation(),sliderten, true,
                    Status.COMPLETED, surveyManager1);
            visitorSurveyBuilderService.save(visitorSurveyBuilder9);

            VisitorSurveyBuilder visitorSurveyBuilder10 = new VisitorSurveyBuilder(
                    LifeStages.LIFE_STAGE4, module, 11, eleven, surveyBuilders.get(10).getQuestionExplanation(),slidereleven,
                    true, Status.COMPLETED, surveyManager1);
            visitorSurveyBuilderService.save(visitorSurveyBuilder10);

            VisitorSurveyBuilder visitorSurveyBuilder11 = new VisitorSurveyBuilder(
                    LifeStages.LIFE_STAGE4, module, 12, twelve,surveyBuilders.get(11).getQuestionExplanation(), slidertwelve,
                    true, Status.COMPLETED, surveyManager1);
            visitorSurveyBuilderService.save(visitorSurveyBuilder11);

            VisitorSurveyBuilder visitorSurveyBuilder12 = new VisitorSurveyBuilder(
                    LifeStages.LIFE_STAGE5, module, 13, thirteen,surveyBuilders.get(12).getQuestionExplanation(),
                    sliderthirteen, true, Status.COMPLETED, surveyManager1);
            visitorSurveyBuilderService.save(visitorSurveyBuilder12);

            VisitorSurveyBuilder visitorSurveyBuilder13 = new VisitorSurveyBuilder(
                    LifeStages.LIFE_STAGE5, module, 14, forteen,surveyBuilders.get(13).getQuestionExplanation(),
                    sliderfourteen, true, Status.COMPLETED, surveyManager1);
            visitorSurveyBuilderService.save(visitorSurveyBuilder13);

            VisitorSurveyBuilder visitorSurveyBuilder14 = new VisitorSurveyBuilder(
                    LifeStages.LIFE_STAGE5, module, 15, fifteen, surveyBuilders.get(14).getQuestionExplanation(),sliderfifteen,
                    true, Status.COMPLETED, surveyManager1);
            visitorSurveyBuilderService.save(visitorSurveyBuilder14);

            int lifeStageval = Integer.parseInt(moduleLifestage);
            LifeStages moduleSpecificLifestage = LifeStages
                    .fromInt(lifeStageval + 1);
            int highesttotal = Integer.parseInt(highestTotal);

            moduleUser.setLifeStages(moduleSpecificLifestage);
            moduleUser.setTotalOflifestage(highesttotal);
            moduleUser.setTotalSurveyWeight(totalCompletionRate);
            moduleUser.setSurveyWeight(surveyCompletionRate);
            moduleUser.setSurveyWeightPercent(surveyCompletionPercent);
            try{
                moduleUserService.saveSurveyResult(moduleUser);
            }
            catch(DuplicateModuleException e){

                LOGGER.info(e);
                model.put(ERROR,e.getErrorCode());
                return "redirect:/ManageSurvey/TakeSurvey/" + moduleId;

            }

            SurveyResult surveyResult1 = new SurveyResult();
            surveyResult1.setCompany(company);
            surveyResult1.setModule(module);
            surveyResult1.setLifeStages(moduleSpecificLifestage);
            surveyResult1.setTotalOflifestage(highesttotal);
            surveyResultService.save(surveyResult1);
            surveyManager.setTotalModulesCompleted(++totalModulesCompleted);
            SurveyManager survey = surveyManagerService.save(surveyManager);

            return savesurveys(survey,company,modules,model,request,modId);
        }
    }

    private String savesurveys(SurveyManager survey, CompanyMaster company, List<ModuleMaster> modules, Map<String, Object> model, HttpServletRequest request, long modId) {

        if (survey.getTotalModules() == survey.getTotalModulesCompleted()) {
            survey.setSurveyStatus(SurveyStatus.COMPLETED);
            survey.setOngoingSurvey(false);
            surveyManagerService.save(survey);
            HttpSession session = request.getSession(true);
            session.removeAttribute("surveyId");

            model.put("done", true);

            int totalModules = modules.size();
            SurveyManager surveyManager2 = new SurveyManager();
            surveyManager2.setCompany(company);
            surveyManager2.setSurveyStatus(SurveyStatus.PENDING);
            surveyManager2.setTotalModules(totalModules);
            surveyManager2.setTotalModulesCompleted(0);
            surveyManager2.setOngoingSurvey(true);
            surveyManager2.setSurveySkipOption(true);
            SurveyManager surveyManager3 = surveyManagerService
                    .save(surveyManager2);
            DateTime dateTime = survey.getSurveyPublished();

            if (dateTime.getMonthOfYear() > 3 && dateTime.getMonthOfYear() <= 6) {
                DateTime date = new DateTime().withMonthOfYear(10)
                        .withDayOfMonth(1).withTimeAtStartOfDay();
                DateTime date2 = date.plusMonths(1);
                surveyManager3.setSurveyPublished(date);
                surveyManager3.setLastDateToComplete(date2);
                surveyManagerService.save(surveyManager3);
            } else if (dateTime.getMonthOfYear() > 6
                    && dateTime.getMonthOfYear() <= 9) {
                DateTime date = new DateTime().withMonthOfYear(1)
                        .withDayOfMonth(1).plusYears(1)
                        .withTimeAtStartOfDay();
                DateTime date2 = date.plusMonths(1);
                surveyManager3.setSurveyPublished(date);
                surveyManager3.setLastDateToComplete(date2);
                surveyManagerService.save(surveyManager3);
            } else if (dateTime.getMonthOfYear() > 9
                    && dateTime.getMonthOfYear() <= 12) {
                DateTime date = new DateTime().withMonthOfYear(4)
                        .withDayOfMonth(1).plusYears(1)
                        .withTimeAtStartOfDay();
                DateTime date2 = date.plusMonths(1);
                surveyManager3.setSurveyPublished(date);
                surveyManager3.setLastDateToComplete(date2);
                surveyManagerService.save(surveyManager3);
            } else if (dateTime.getMonthOfYear() <= 3) {
                DateTime date = new DateTime().withMonthOfYear(7)
                        .withDayOfMonth(1).withTimeAtStartOfDay();
                DateTime date2 = date.plusMonths(1);
                surveyManager3.setSurveyPublished(date);
                surveyManager3.setLastDateToComplete(date2);
                surveyManagerService.save(surveyManager3);

            }

            session.setAttribute("companyId", company.getCompanyId());

            return "redirect:/ManageSurvey/ViewSurvey/SurveyResult";
        } else {

            model.put(SUCCESS, "alert.answersavedsucessfully");

            return "redirect:/ManageSurvey/TakeSurvey/"+modId;
        }

    }


    @RequestMapping("/ManageSurvey/ViewSurvey/SurveyResult")
    public String surveyResultPage(
            HttpServletRequest request,
            Map<String, Object> model) throws DuplicateModuleException{
        Long compId = (Long)request.getSession().getAttribute("companyId");
        List<ModuleUser> surveyResultList = moduleUserService.findByCompanyCompanyId(compId);

        List<LifeStageResultMaster> resultSetList = new ArrayList<>();

        for(ModuleUser mu : surveyResultList){
            LifeStageResultMaster stageResult = lifeStageResultService.findByModuleandLifeStage(mu.getModule().getModuleId(), mu.getLifeStages());
            resultSetList.add(stageResult);
            mu.setTotalSurveyWeight(0);
            mu.setSurveyWeight(0);
            mu.setSurveyWeightPercent(0);
            moduleUserService.saveSurveyResult(mu);
        }   

        List<GlobalSponsoredManagement> globalSponsorImageList = globalSponsoredService.findByActiveandImageTypeGlobalSponsor(true, "Global Sponsor");
        GlobalSponsoredManagement atmaContactImage = globalSponsoredService.findByActiveandImageTypeContact(true, "Contact");
        model.put("surveyResultList", surveyResultList);
        model.put("resultSetList", resultSetList);
        model.put("atmaContactImage", atmaContactImage);
        model.put("globalSponsorImageList", globalSponsorImageList);

        return "visitor/mngt/surveymngt/surveyresult";
    }

    @RequestMapping("/ManageNextSteps/CreateNextSteps/SaveNextSteps")
    public String saveNextSteps(
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            @RequestParam("moduleId") Long moduleId,
            @RequestParam(value = "1", required = false) String one,
            @RequestParam(value = "2", required = false) String two,
            @RequestParam(value = "3", required = false) String three,
            @RequestParam(value = "4", required = false) String four,
            @RequestParam(value = "5", required = false) String five,
            @RequestParam(value = "6", required = false) String six,
            @RequestParam(value = "7", required = false) String seven,
            @RequestParam(value = "8", required = false) String eight,
            @RequestParam(value = "9", required = false) String nine,
            @RequestParam(value = "10", required = false) String ten,
            @RequestParam(value = "11", required = false) String eleven,
            @RequestParam(value = "12", required = false) String twelve,
            @RequestParam(value = "13", required = false) String thirteen,
            @RequestParam(value = "14", required = false) String fourteen,
            @RequestParam(value = "15", required = false) String fifteen,
            @RequestParam(value = "16", required = false) String sixteen,
            @RequestParam(value = "17", required = false) String seventeen,
            @RequestParam(value = "18", required = false) String eighteen,
            @RequestParam(value = "19", required = false) String nineteen,
            @RequestParam(value = "20", required = false) String twenty,
            @RequestParam(value = "chosenCategory1", required = false) Long categoryId1,
            @RequestParam(value = "chosenCategory2", required = false) Long categoryId2,
            @RequestParam(value = "chosenCategory3", required = false) Long categoryId3,
            @RequestParam(value = "chosenCategory4", required = false) Long categoryId4,
            @RequestParam(value = "chosenCategory5", required = false) Long categoryId5,
            @RequestParam(value = "chosenCategory6", required = false) Long categoryId6,
            @RequestParam(value = "chosenCategory7", required = false) Long categoryId7,
            @RequestParam(value = "chosenCategory8", required = false) Long categoryId8,
            @RequestParam(value = "chosenCategory9", required = false) Long categoryId9,
            @RequestParam(value = "chosenCategory10", required = false) Long categoryId10,
            @RequestParam(value = "chosenCategory11", required = false) Long categoryId11,
            @RequestParam(value = "chosenCategory12", required = false) Long categoryId12,
            @RequestParam(value = "chosenCategory13", required = false) Long categoryId13,
            @RequestParam(value = "chosenCategory14", required = false) Long categoryId14,
            @RequestParam(value = "chosenCategory15", required = false) Long categoryId15,
            @RequestParam(value = "chosenCategory16", required = false) Long categoryId16,
            @RequestParam(value = "chosenCategory17", required = false) Long categoryId17,
            @RequestParam(value = "chosenCategory18", required = false) Long categoryId18,
            @RequestParam(value = "chosenCategory19", required = false) Long categoryId19,
            @RequestParam(value = "chosenCategory20", required = false) Long categoryId20,

            Map<String, Object> model) {

        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        } else if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }

        List<ModuleMaster> moduleList = moduleService.findByActive(true);
        ModuleMaster module = moduleService.findByModuleId(moduleId);

        List<NextSteps> nextSteps = nextStepsService
                .findByLifeStageandModuleId(LifeStages.LIFE_STAGE1, moduleId);
        if (!nextSteps.isEmpty() && nextSteps != null) {
            nextStepsService.deleteByModuleId(moduleId);
        }

        if (categoryId1 != null) {
            CategoryMaster category1 = categoryService
                    .findByCategoryId(categoryId1);
            NextSteps nextSteps1 = new NextSteps(module, category1, one,
                    LifeStages.LIFE_STAGE1);
            nextStepsService.save(nextSteps1);
        } else {
            NextSteps nextSteps1 = new NextSteps();
            nextSteps1.setLifestages(LifeStages.LIFE_STAGE1);
            nextSteps1.setModule(module);
            nextSteps1.setStep(one);
            nextStepsService.save(nextSteps1);
        }

        if (categoryId2 != null) {
            CategoryMaster category2 = categoryService
                    .findByCategoryId(categoryId2);
            NextSteps nextSteps2 = new NextSteps(module, category2, two,
                    LifeStages.LIFE_STAGE1);
            nextStepsService.save(nextSteps2);
        } else {
            NextSteps nextSteps1 = new NextSteps();
            nextSteps1.setLifestages(LifeStages.LIFE_STAGE1);
            nextSteps1.setModule(module);
            nextSteps1.setStep(two);
            nextStepsService.save(nextSteps1);
        }

        if (categoryId3 != null) {
            CategoryMaster category3 = categoryService
                    .findByCategoryId(categoryId3);
            NextSteps nextSteps3 = new NextSteps(module, category3, three,
                    LifeStages.LIFE_STAGE1);
            nextStepsService.save(nextSteps3);
        } else {
            NextSteps nextSteps1 = new NextSteps();
            nextSteps1.setLifestages(LifeStages.LIFE_STAGE1);
            nextSteps1.setModule(module);
            nextSteps1.setStep(three);
            nextStepsService.save(nextSteps1);
        }

        if (categoryId4 != null) {
            CategoryMaster category4 = categoryService
                    .findByCategoryId(categoryId4);
            NextSteps nextSteps4 = new NextSteps(module, category4, four,
                    LifeStages.LIFE_STAGE1);
            nextStepsService.save(nextSteps4);
        } else {
            NextSteps nextSteps1 = new NextSteps();
            nextSteps1.setLifestages(LifeStages.LIFE_STAGE1);
            nextSteps1.setModule(module);
            nextSteps1.setStep(four);
            nextStepsService.save(nextSteps1);
        }

        if (categoryId5 != null) {
            CategoryMaster category5 = categoryService
                    .findByCategoryId(categoryId5);
            NextSteps nextSteps5 = new NextSteps(module, category5, five,
                    LifeStages.LIFE_STAGE2);
            nextStepsService.save(nextSteps5);
        } else {
            NextSteps nextSteps1 = new NextSteps();
            nextSteps1.setLifestages(LifeStages.LIFE_STAGE2);
            nextSteps1.setModule(module);
            nextSteps1.setStep(five);
            nextStepsService.save(nextSteps1);
        }

        if (categoryId6 != null) {
            CategoryMaster category6 = categoryService
                    .findByCategoryId(categoryId6);
            NextSteps nextSteps6 = new NextSteps(module, category6, six,
                    LifeStages.LIFE_STAGE2);
            nextStepsService.save(nextSteps6);
        } else {
            NextSteps nextSteps1 = new NextSteps();
            nextSteps1.setLifestages(LifeStages.LIFE_STAGE2);
            nextSteps1.setModule(module);
            nextSteps1.setStep(six);
            nextStepsService.save(nextSteps1);
        }

        if (categoryId7 != null) {
            CategoryMaster category7 = categoryService
                    .findByCategoryId(categoryId7);
            NextSteps nextSteps7 = new NextSteps(module, category7, seven,
                    LifeStages.LIFE_STAGE2);
            nextStepsService.save(nextSteps7);
        } else {
            NextSteps nextSteps1 = new NextSteps();
            nextSteps1.setLifestages(LifeStages.LIFE_STAGE2);
            nextSteps1.setModule(module);
            nextSteps1.setStep(seven);
            nextStepsService.save(nextSteps1);
        }

        if (categoryId8 != null) {
            CategoryMaster category8 = categoryService
                    .findByCategoryId(categoryId8);
            NextSteps nextSteps8 = new NextSteps(module, category8, eight,
                    LifeStages.LIFE_STAGE2);
            nextStepsService.save(nextSteps8);
        } else {
            NextSteps nextSteps1 = new NextSteps();
            nextSteps1.setLifestages(LifeStages.LIFE_STAGE2);
            nextSteps1.setModule(module);
            nextSteps1.setStep(eight);
            nextStepsService.save(nextSteps1);
        }

        if (categoryId9 != null) {
            CategoryMaster category9 = categoryService
                    .findByCategoryId(categoryId9);
            NextSteps nextSteps9 = new NextSteps(module, category9, nine,
                    LifeStages.LIFE_STAGE3);
            nextStepsService.save(nextSteps9);
        } else {
            NextSteps nextSteps1 = new NextSteps();
            nextSteps1.setLifestages(LifeStages.LIFE_STAGE3);
            nextSteps1.setModule(module);
            nextSteps1.setStep(nine);
            nextStepsService.save(nextSteps1);
        }

        if (categoryId10 != null) {
            CategoryMaster category10 = categoryService
                    .findByCategoryId(categoryId10);
            NextSteps nextSteps10 = new NextSteps(module, category10, ten,
                    LifeStages.LIFE_STAGE3);
            nextStepsService.save(nextSteps10);
        } else {
            NextSteps nextSteps1 = new NextSteps();
            nextSteps1.setLifestages(LifeStages.LIFE_STAGE3);
            nextSteps1.setModule(module);
            nextSteps1.setStep(ten);
            nextStepsService.save(nextSteps1);
        }

        if (categoryId11 != null) {
            CategoryMaster category11 = categoryService
                    .findByCategoryId(categoryId11);
            NextSteps nextSteps11 = new NextSteps(module, category11, eleven,
                    LifeStages.LIFE_STAGE3);
            nextStepsService.save(nextSteps11);

        } else {
            NextSteps nextSteps1 = new NextSteps();
            nextSteps1.setLifestages(LifeStages.LIFE_STAGE3);
            nextSteps1.setModule(module);
            nextSteps1.setStep(eleven);
            nextStepsService.save(nextSteps1);
        }

        if (categoryId12 != null) {
            CategoryMaster category12 = categoryService
                    .findByCategoryId(categoryId12);
            NextSteps nextSteps12 = new NextSteps(module, category12, twelve,
                    LifeStages.LIFE_STAGE3);
            nextStepsService.save(nextSteps12);

        } else {
            NextSteps nextSteps1 = new NextSteps();
            nextSteps1.setLifestages(LifeStages.LIFE_STAGE3);
            nextSteps1.setModule(module);
            nextSteps1.setStep(twelve);
            nextStepsService.save(nextSteps1);
        }

        if (categoryId13 != null) {
            CategoryMaster category13 = categoryService
                    .findByCategoryId(categoryId13);
            NextSteps nextSteps13 = new NextSteps(module, category13, thirteen,
                    LifeStages.LIFE_STAGE4);
            nextStepsService.save(nextSteps13);
        } else {
            NextSteps nextSteps1 = new NextSteps();
            nextSteps1.setLifestages(LifeStages.LIFE_STAGE4);
            nextSteps1.setModule(module);
            nextSteps1.setStep(thirteen);
            nextStepsService.save(nextSteps1);
        }

        if (categoryId14 != null) {
            CategoryMaster category14 = categoryService
                    .findByCategoryId(categoryId14);
            NextSteps nextSteps14 = new NextSteps(module, category14, fourteen,
                    LifeStages.LIFE_STAGE4);
            nextStepsService.save(nextSteps14);
        } else {
            NextSteps nextSteps1 = new NextSteps();
            nextSteps1.setLifestages(LifeStages.LIFE_STAGE4);
            nextSteps1.setModule(module);
            nextSteps1.setStep(fourteen);
            nextStepsService.save(nextSteps1);
        }

        if (categoryId15 != null) {
            CategoryMaster category15 = categoryService
                    .findByCategoryId(categoryId15);
            NextSteps nextSteps15 = new NextSteps(module, category15, fifteen,
                    LifeStages.LIFE_STAGE4);
            nextStepsService.save(nextSteps15);

        } else {
            NextSteps nextSteps1 = new NextSteps();
            nextSteps1.setLifestages(LifeStages.LIFE_STAGE4);
            nextSteps1.setModule(module);
            nextSteps1.setStep(fifteen);
            nextStepsService.save(nextSteps1);
        }

        if (categoryId16 != null) {
            CategoryMaster category16 = categoryService
                    .findByCategoryId(categoryId16);
            NextSteps nextSteps16 = new NextSteps(module, category16, sixteen,
                    LifeStages.LIFE_STAGE4);
            nextStepsService.save(nextSteps16);
        } else {
            NextSteps nextSteps1 = new NextSteps();
            nextSteps1.setLifestages(LifeStages.LIFE_STAGE4);
            nextSteps1.setModule(module);
            nextSteps1.setStep(sixteen);
            nextStepsService.save(nextSteps1);
        }

        if (categoryId17 != null) {
            CategoryMaster category17 = categoryService
                    .findByCategoryId(categoryId17);
            NextSteps nextSteps17 = new NextSteps(module, category17,
                    seventeen, LifeStages.LIFE_STAGE5);
            nextStepsService.save(nextSteps17);
        } else {
            NextSteps nextSteps1 = new NextSteps();
            nextSteps1.setLifestages(LifeStages.LIFE_STAGE5);
            nextSteps1.setModule(module);
            nextSteps1.setStep(seventeen);
            nextStepsService.save(nextSteps1);
        }

        if (categoryId18 != null) {
            CategoryMaster category18 = categoryService
                    .findByCategoryId(categoryId18);
            NextSteps nextSteps18 = new NextSteps(module, category18, eighteen,
                    LifeStages.LIFE_STAGE5);
            nextStepsService.save(nextSteps18);
        } else {
            NextSteps nextSteps1 = new NextSteps();
            nextSteps1.setLifestages(LifeStages.LIFE_STAGE5);
            nextSteps1.setModule(module);
            nextSteps1.setStep(eighteen);
            nextStepsService.save(nextSteps1);
        }

        if (categoryId19 != null) {
            CategoryMaster category19 = categoryService
                    .findByCategoryId(categoryId19);
            NextSteps nextSteps19 = new NextSteps(module, category19, nineteen,
                    LifeStages.LIFE_STAGE5);
            nextStepsService.save(nextSteps19);

        } else {
            NextSteps nextSteps1 = new NextSteps();
            nextSteps1.setLifestages(LifeStages.LIFE_STAGE5);
            nextSteps1.setModule(module);
            nextSteps1.setStep(nineteen);
            nextStepsService.save(nextSteps1);
        }

        if (categoryId20 != null) {
            CategoryMaster category20 = categoryService
                    .findByCategoryId(categoryId20);
            NextSteps nextSteps20 = new NextSteps(module, category20, twenty,
                    LifeStages.LIFE_STAGE5);
            nextStepsService.save(nextSteps20);

        } else {
            NextSteps nextSteps1 = new NextSteps();
            nextSteps1.setLifestages(LifeStages.LIFE_STAGE5);
            nextSteps1.setModule(module);
            nextSteps1.setStep(twenty);
            nextStepsService.save(nextSteps1);
        }

        model.put("moList", moduleList);
        model.put(SUCCESS, "alert.stepssavedsuccessfully");

        return "redirect:/ManageNextSteps/CreateNextSteps/" + moduleId;
    }

    @RequestMapping(value = "/{url1}/{url2}/SaveAdmin")
    public String saveAdmin(
            @PathVariable("url1") String url1,
            @PathVariable("url2") String url2,
            @RequestParam("firstName") String firstName,
            @RequestParam("lastName") String lastName,
            @RequestParam("emailId") String emailId,
            @RequestParam("designation") String designation,
            @RequestParam("description") String description,
            @RequestParam(value = "approvalRights", required = false) boolean enquiryAccessRights,
            Map<String, Object> model) throws IOException {

        boolean exceptionFlag = false;
        String loginId = emailId;
        String password = RandomCodeGenerator.randomString(6);
        boolean flag=true;

        User user = new User(firstName, lastName, enquiryAccessRights, loginId,
                password, designation, description, Role.ATMA_ADMIN.getId(),
                emailId, InviteStatus.PENDING.getId(),flag);

        try {
            User user1 = userService.saveUser(user);
            userService.sendRegstrationMail(user1, null, null,password);

        } catch (DuplicateEmailException e) {
            exceptionFlag = true;
            model.put(ERROR, e.getErrorCode());
            LOGGER.info(e);
        } catch (DuplicateLoginException e) {
            exceptionFlag = true;
            model.put(ERROR, e.getErrorCode());
            LOGGER.info(e);
        }
        if (!exceptionFlag) {
            model.put(SUCCESS, "alert.adminsaved");
            return REDIRECT + url1 + "/" + url2;
        } else {
            model.put("user", user);
            return "admin/mngt/atmaadminmngt/addadmin";
        }

    }

    @RequestMapping(value = "/{url1}/{url2}/UpdateAdmin")
    public String updateAdmin(
            @PathVariable("url1") String url1,
            @PathVariable("url2") String url2,
            @RequestParam("userId") Long userId,
            @RequestParam("firstName") String firstName,
            @RequestParam("lastName") String lastName,
            @RequestParam("designation") String designation,
            @RequestParam("description") String description,
            @RequestParam(value = "approvalRights", required = false) boolean enquiryAccessRights,
            Map<String, Object> model) throws IOException {

        try {

            userService.updateUser1(userId, firstName, lastName, designation,
                    description, enquiryAccessRights);
        } catch (NoSuchUserException e) {
            LOGGER.info(e);
            User user = new User();
            user = userService.findById(userId);
            user.setUserId(userId);
            user.setFirstName(firstName);
            user.setLastName(lastName);

            user.setDesignation(designation);
            user.setDescription(description);
            user.setEnquiryAccessRights(enquiryAccessRights);

            model.put(ERROR, e.getErrorCode());

            model.put("user", user);
            return "admin/mngt/atmaadminmngt/editadmin";

        }
        model.put(SUCCESS, "alert.adminupdated");
        return REDIRECT + url1 + "/" + url2;
    }

    @RequestMapping(value = "/{url1}/{url2}/SaveModule")
    public String addModule(
            @PathVariable(value = "url1") String url1,
            @PathVariable(value = "url2") String url2,
            @RequestParam(value = "imageChooser1", required = false) MultipartFile profile,
            HttpServletRequest request,
            Map<String, Object> model) throws IOException {
        boolean exceptionFlag = false;

        String moduleName = request.getParameter("moduleName").trim();
        String moduleDescription = request.getParameter("moduleDescription");
        ModuleMaster module = new ModuleMaster();
        module.setModuleName(moduleName);
        module.setModuleDescription(moduleDescription);
        ModuleMaster module1 = new ModuleMaster();
        try {
            module1 = moduleService
                    .save(moduleName, moduleDescription, profile);
        } catch (DuplicateModuleException e) {
            LOGGER.info(e);
            exceptionFlag = true;
            model.put(ERROR, e.getErrorCode());
        }

        if (!exceptionFlag) {
            List<CompanyMaster> companies = companyService.findByActive(true);

            if (!companies.isEmpty()) {
                saveCompanySurvey(companies,module1);
            }

            model.put(SUCCESS, "alert.modulesaved");
            return REDIRECT + url1 + "/" + url2;
        } else {
            model.put("module", module);
            return "admin/mngt/modulemngt/addmodule";
        }

    }

    private void saveCompanySurvey(List<CompanyMaster> companies, ModuleMaster module1) {

        for (CompanyMaster comp : companies) {
            ModuleUser moduleUser = new ModuleUser();
            moduleUser.setCompany(comp);
            moduleUser.setModule(module1);
            moduleUser.setUser(null);
            moduleUser.setLifeStages(null);
            moduleUserService.save(moduleUser);
            SurveyManager survey = surveyManagerService.findByCompanyIdandOngoingSurvey(comp.getCompanyId(), true);
            if(survey!=null){
                int totalmod = survey.getTotalModules();
                survey.setTotalModules(++totalmod);
                surveyManagerService.save(survey);
            }

        }
    }

    @RequestMapping(value = "/{url1}/{url2}/SaveCategory")
    public String addCategory(
            @PathVariable(value = "url1") String url1,
            @PathVariable(value = "url2") String url2,
            @RequestParam(value = "categoryName") String categoryName,
            @RequestParam(value = "categoryDescription", required = false) String categoryDescription,
            @RequestParam(value = "categorySummary", required = false) String categorySummary,
            @RequestParam(value = "chosenModuleId") Long moduleId,
            Map<String, Object> model) throws IOException {
        ModuleMaster module = moduleService.findByModuleId(moduleId);

        try {
            categoryService.save(categoryName, categoryDescription,
                    module.getModuleId(), categorySummary);
        } catch (DuplicateCategoryException e) {
            LOGGER.info(e);
            CategoryMaster category = new CategoryMaster();
            category.setCategoryName(categoryName);
            category.setCategoryDescription(categoryDescription);
            category.setCategorySummary(categorySummary);
            category.setModule(module);

            model.put("category", category);
            List<ModuleMaster> moduleList = moduleService.findByActive(true);
            model.put(MODULE_LIST, moduleList);
            model.put(ERROR, e.getErrorCode());

            return "admin/mngt/categorymngt/addcategory";
        }
        model.put(SUCCESS, "alert.categorysaved");
        return REDIRECT + url1 + "/" + url2;

    }

    @RequestMapping(value = "/{url1}/{url2}/SaveModuleUser")
    public String addModuleUser(
            @PathVariable(value = "url1") String url1,
            @PathVariable(value = "url2") String url2,
            @RequestParam(value = "firstName") String firstName,
            @RequestParam(value = "lastName") String lastName,
            @RequestParam(value = "emailId") String emailId,
            @RequestParam(value = "mobile") String mobile,
            Map<String, Object> model) throws NoSuchUserException {

        boolean exceptionFlag = false;
        Long companyId = getLogedInUser().getModuleId();
        String loginId = emailId;
        String password = RandomCodeGenerator.randomString(6);
        String designation = "Module User";

        int profileCompletionRate = 0;
        profileCompletionRate = profileCompletionRate + ProfileWeightage.FIRST_NAME.getId();
        profileCompletionRate = profileCompletionRate + ProfileWeightage.LAST_NAME.getId();
        profileCompletionRate = profileCompletionRate + ProfileWeightage.MOBILE.getId();
        profileCompletionRate = profileCompletionRate + ProfileWeightage.EMAIL_ID.getId();

        int totalCompletionRate = profileCompletionRate;
        totalCompletionRate = totalCompletionRate + ProfileWeightage.GENDER.getId();
        totalCompletionRate = totalCompletionRate + ProfileWeightage.DESIGNATION.getId();
        totalCompletionRate = totalCompletionRate + ProfileWeightage.EDUCATIONAL_BACKGROUND.getId();
        totalCompletionRate = totalCompletionRate + ProfileWeightage.LINKEDIN.getId();
        totalCompletionRate = totalCompletionRate + ProfileWeightage.WORK_EXPERIENCE.getId();
        totalCompletionRate = totalCompletionRate + ProfileWeightage.FUNCTIONAL_AREAS.getId();
        totalCompletionRate = totalCompletionRate + ProfileWeightage.NETWORK_KNOWLEDGE.getId(); 

        int profileCompletionPercent = 100 * profileCompletionRate/totalCompletionRate;
        boolean flag=true;

        User user = new User(firstName, lastName, designation, loginId,
                emailId, password, Role.MODULE_USER.getId(),
                InviteStatus.PENDING.getId(), mobile, UserStatus.ACTIVE,flag);
        try {

            User user1 = userService.saveUser(user);
            user1.setModuleId(companyId);
            user1.setProfileCompletion(profileCompletionPercent);
            user1.setProfileCompletionRate(profileCompletionRate);
            user1.setTotalProfileCompletion(totalCompletionRate);
            User user2 = userService.update(user1);
            userService.sendRegstrationMail(user2, null, null,password);

        } catch (DuplicateEmailException e) {
            exceptionFlag = true;
            model.put(ERROR, e.getErrorCode());
            LOGGER.info(e);
        }

        if (!exceptionFlag) {
            model.put(SUCCESS, "alert.moduleusersaved");
            return REDIRECT + url1 + "/" + url2;

        } else {
            model.put("user", user);
            List<ModuleMaster> moduleList = moduleService.findByActive(true);
            model.put(MODULE_LIST, moduleList);
            return "admin/mngt/moduleusermngt/addmoduleuser";
        }
    }

    @RequestMapping(value = "/{url1}/{url2}/SaveAssignModule")
    public String saveAssignModule(
            @PathVariable("url1") String url1,
            @PathVariable("url2") String url2,
            @RequestParam(value = "moduleuserId", required = false) Long moduleuserId,
            @RequestParam(value = "moduleId", required = false) String moduleIds,
            HttpServletRequest request,
            Map<String, Object> model) throws DuplicateModuleException {
        Long companyId = getLogedInUser().getModuleId();
        String[] strValues = moduleIds.split(",");
        for (String mod : strValues) {

            String duserId = request.getParameter(mod);
            if (!duserId.isEmpty()) {
                Long userId = Long.parseLong(duserId);
                Long moduleId = Long.parseLong(mod);

                User user1 = userService.findById(userId);
                ModuleUser moduleUser = moduleUserService
                        .findByCompanyIdAndModuleId(companyId, moduleId);
                moduleUser.setAdmin(user1);
                moduleUserService.save(moduleUser);
            }
        }

        if(moduleuserId == null){
            return REDIRECT + url1 + "/" + url2;
        }
        else{
            model.put(SUCCESS, "alert.moduleassigned");
            return REDIRECT + url1 + "/" + url2;
        }

    }

    @RequestMapping(value = "/{url1}/{url2}/UpdateModuleUser")
    public String updateModuleUser(@PathVariable("url1") String url1,
            @PathVariable("url2") String url2,
            @RequestParam("userId") Long userId,
            @RequestParam("firstName") String firstName,
            @RequestParam("lastName") String lastName,
            @RequestParam("emailId") String emailId,
            @RequestParam(value = "mobile") String mobile,
            Map<String, Object> model) throws NoSuchUserException
            {
        boolean exceptionFlag = false;
        String designation = "Module User";
        try {
            User user = userService.findById(userId);
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setMobile(mobile);
            userService.update(user);
        }
        catch (NoSuchUserException e) {

            exceptionFlag = true;
            model.put(ERROR, e.getErrorCode());
            LOGGER.info(e);
        }

        if (!exceptionFlag) {
            model.put(SUCCESS, "alert.moduleuserupdated");
            return REDIRECT + url1 + "/" + url2;
        } else {
            User user;
            user = userService.findById(userId);
            user.setUserId(userId);
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setEmailId(emailId);
            user.setDesignation(designation);
            user.setMobile(mobile);
            model.put("user", user);
            return "admin/mngt/moduleusermngt/editmoduleuser";

        }
            }

    @RequestMapping(value = "/{url1}/{url2}/UpdateSponsoredImage")
    public String updateSponsored(
            @PathVariable(value = "url1") String url1,
            @PathVariable(value = "url2") String url2,
            @RequestParam("moduleId") Long moduleId,
            @RequestParam("link") String link,
            @RequestParam(value = "imageChooserProfile", required = false) MultipartFile sponsoredimage,
            Map<String, Object> model) throws IOException {

        ModuleMaster module = moduleService.findByModuleId(moduleId);
        ImageMaster sponsoredImage = null;
        if (sponsoredimage != null && sponsoredimage.getSize() > 0) {
            String filename = sponsoredimage.getOriginalFilename();
            String ext = filename.substring(filename.lastIndexOf("."));
            sponsoredImage = imageService.addImage(sponsoredimage.getBytes(),
                    ext, "SponsoredImg" + ext, true, false, module
                    .getModuleName().toString());
        }
        try {
            module.setSponsoredImage(sponsoredImage);
            module.setLink(link);
            moduleService.update(module);
        } catch (Exception e) {
            LOGGER.info(e);
            ModuleMaster module1 = new ModuleMaster();
            module1.setSponsoredImage(module.getSponsoredImage());
            module1.setLink(module.getLink());
            model.put(MODULE1, module1);
            return "admin/mngt/sponsoredimagemngt/editsponsoredimage";
        }
        model.put(SUCCESS, "alert.moduleupdated");
        return REDIRECT + url1 + "/" + url2;

    }

    @RequestMapping(value = "/{url1}/{url2}/UpdateGlobalSponsoredImage")
    public String updateGlobalSponsor(
            @PathVariable(value = "url1") String url1,
            @PathVariable(value = "url2") String url2,
            @RequestParam("id") Long contactId,
            @RequestParam(value = "imageChooser1", required = false) MultipartFile contactimage,
            Map<String, Object> model) throws IOException {

        GlobalSponsoredManagement imageManagement = globalSponsoredService
                .findByImageId(contactId);

        if (contactimage != null && contactimage.getSize() > 0) {
            String filename = contactimage.getOriginalFilename();
            String ext = filename.substring(filename.lastIndexOf("."));
            ImageMaster contactImage = imageService.addImage(contactimage.getBytes(), ext,
                    "ContactImage" + ext, true, false, "Contact Image");
            try {
                imageManagement.setContactImage(contactImage);
                globalSponsoredService.update(imageManagement);
            } catch (RuntimeException e) {
                LOGGER.info(e);
                return "admin/mngt/globalsponsoredmngt/editcontactimage";
            }
        }

        model.put(SUCCESS, "alert.imageupdated");
        return REDIRECT + url1 + "/" + url2;
    }

    @RequestMapping(value = "/{url1}/{url2}/UpdateModule")
    public String updateModule(
            @PathVariable(value = "url1") String url1,
            @PathVariable(value = "url2") String url2,
            @RequestParam("moduleId") Long moduleId,
            @RequestParam("moduleName") String moduleName,
            @RequestParam("moduleDescription") String moduleDescription,
            @RequestParam(value = "imageChooser1", required = false) MultipartFile profile,
            Map<String, Object> model) throws IOException {

        try {
            moduleService.update(moduleId, moduleName, moduleDescription,
                    profile);
        } catch (DuplicateModuleException e) {

            LOGGER.info(e);
            ModuleMaster module = new ModuleMaster();
            module.setModuleId(moduleId);
            module.setModuleName(moduleName);
            module.setModuleDescription(moduleDescription);
            model.put("module", module);
            model.put(ERROR, e.getErrorCode());
            return "admin/mngt/modulemngt/editmodule";
        }
        model.put(SUCCESS, "alert.moduleupdated");
        return REDIRECT + url1 + "/" + url2;
    }

    @RequestMapping(value = "/{url1}/{url2}/UpdateCategory")
    public String updateCategory(@PathVariable("url1") String url1,
            @PathVariable("url2") String url2,
            @RequestParam("categoryId") Long categoryId,
            @RequestParam("categoryName") String categoryName,
            @RequestParam("categoryDescription") String categoryDescription,
            @RequestParam(value = "categorySummary") String categorySummary,
            @RequestParam("chosenModuleId") long moduleId,
            Map<String, Object> model) throws IOException {

        try {
            categoryService.update(categoryId, categoryName,
                    categoryDescription, moduleId, categorySummary);
        } catch (DuplicateCategoryException e) {
            CategoryMaster category = new CategoryMaster();
            category.setCategoryId(categoryId);
            category.setCategoryName(categoryName);
            category.setCategoryDescription(categoryDescription);
            category.setCategorySummary(categorySummary);
            ModuleMaster module = moduleService.findByModuleId(moduleId);
            category.setModule(module);
            model.put("category", category);

            List<ModuleMaster> moduleList = moduleService.findByActive(true);
            model.put(MODULE_LIST, moduleList);

            model.put(ERROR, e.getErrorCode());
            LOGGER.info(e);
            return "admin/mngt/categorymngt/editcategory";
        }
        model.put(SUCCESS, "alert.categoryupdated");
        return REDIRECT + url1 + "/" + url2;
    }

    @RequestMapping(value = "/ChangePhoto")
    public String changePhoto(
            @RequestParam(value = "imageChooser", required = false) MultipartFile profile) throws IOException {
        User user = getLogedInUser();
        ImageMaster profileImage = null;
        ImageMaster removedProfileImage = user.getProfileImage();

        if (profile != null && profile.getSize() > 0) {
            String filename = profile.getOriginalFilename();
            String ext = filename.substring(filename.lastIndexOf(".")+1);

            profileImage = imageService.addImage(profile.getBytes(), ext,
                    "ProfileImg." + ext, true, false, user.getFirstName()
                    .replace("#", "").toString());
        }

        user.setProfileImage(profileImage);
        userService.update(user);
        if (removedProfileImage != null) {
            imageService.delete(removedProfileImage.getImageId());
        }
        return "redirect:/Index";
    }

    @RequestMapping(value = "/{url1}/{url2}/SaveDocument")
    public String addDocument(
            @PathVariable("url1") String url1,
            @PathVariable("url2") String url2,
            @RequestParam("chosenModuleId") long moduleId,
            @RequestParam("chosenCategoryId") long categoryId,
            @RequestParam("chosenSectionId") String chosenSection,
            @RequestParam("contentType") String contentType,
            @RequestParam(value = "docTitle1", required = false) String docTitle1,
            @RequestParam(value = "document1", required = false) MultipartFile file1,
            @RequestParam(value = "documentDescription1", required = false) String documentDescription1,
            @RequestParam(value = "chosenLifeStage1", required = false) LifeStages lifeStage1,
            @RequestParam(value = "docTitle2", required = false) String docTitle2,
            @RequestParam(value = "document2", required = false) MultipartFile file2,
            @RequestParam(value = "documentDescription2", required = false) String documentDescription2,
            @RequestParam(value = "chosenLifeStage2", required = false) LifeStages lifeStage2,
            @RequestParam(value = "docTitle3", required = false) String docTitle3,
            @RequestParam(value = "document3", required = false) MultipartFile file3,
            @RequestParam(value = "documentDescription3", required = false) String documentDescription3,
            @RequestParam(value = "chosenLifeStage3", required = false) LifeStages lifeStage3,
            @RequestParam(value = "docTitle4", required = false) String docTitle4,
            @RequestParam(value = "document4", required = false) MultipartFile file4,
            @RequestParam(value = "documentDescription4", required = false) String documentDescription4,
            @RequestParam(value = "chosenLifeStage4", required = false) LifeStages lifeStage4,
            @RequestParam(value = "linkTitle1", required = false) String linkTitle1,
            @RequestParam(value = "linkName1", required = false) String link1,
            @RequestParam(value = "linkTitle2", required = false) String linkTitle2,
            @RequestParam(value = "linkName2", required = false) String link2,
            @RequestParam(value = "linkTitle3", required = false) String linkTitle3,
            @RequestParam(value = "linkName3", required = false) String link3,
            HttpServletRequest request,
            Map<String, Object> model) throws IOException{

        boolean exceptionFlag = false;
        ModuleMaster module = moduleService.findByModuleId(moduleId);
        CategoryMaster category = categoryService.findByCategoryId(categoryId);

        HttpSession session = request.getSession(true);
        List<ImageMaster> bluePrintImages = new ArrayList<>();

        if (session.getAttribute(BLUEPRINT_IMAGES) != null) {
            bluePrintImages = (List<ImageMaster>) session
                    .getAttribute(BLUEPRINT_IMAGES);
        }

        String filename1 = "";
        String agreementFileUlr1 = "";
        Long sizeofFile1 = null;
        String documentName1 = "";
        String extension1 = "";

        String filename2 = "";
        String agreementFileUlr2 = "";
        Long sizeofFile2 = null;
        String documentName2 = "";
        String extension2 = "";

        String filename3 = "";
        String agreementFileUlr3 = "";
        Long sizeofFile3 = null;
        String documentName3 = "";
        String extension3 = "";

        String filename4 = "";
        String agreementFileUlr4 = "";
        Long sizeofFile4 = null;
        String documentName4 = "";
        String extension4 = "";
        if (file1 != null) {
            if (file1.getOriginalFilename() != null
                    && file1.getOriginalFilename().length() > 0) {
                filename1 = file1.getOriginalFilename();
                String ext1 = filename1.substring(filename1.lastIndexOf("."));
                byte[] byteArray1 = file1.getBytes();
                String absolutePath = documentService
                        .getAbsoluteSaveLocationDir(1);

                File uploadedFile = documentService.save(byteArray1, ext1,
                        filename1, absolutePath);
                agreementFileUlr1 = uploadedFile.getAbsolutePath();
                agreementFileUlr1 = agreementFileUlr1
                        .split(propsValue.documentsRoot)[1];
            }
            sizeofFile1 = file1.getSize() / 1024;
            documentName1 = FilenameUtils.getBaseName(filename1);
            extension1 = FilenameUtils.getExtension(filename1);

        }
        if (file2 != null) {
            if (file2.getOriginalFilename() != null
                    && file2.getOriginalFilename().length() > 0) {
                filename2 = file2.getOriginalFilename();
                String ext2 = filename2.substring(filename2.lastIndexOf("."));
                byte[] byteArray2 = file2.getBytes();
                String absolutePath = documentService
                        .getAbsoluteSaveLocationDir(1);

                File uploadedFile = documentService.save(byteArray2, ext2,
                        filename2, absolutePath);
                agreementFileUlr2 = uploadedFile.getAbsolutePath();
                agreementFileUlr2 = agreementFileUlr2
                        .split(propsValue.documentsRoot)[1];
            }
            sizeofFile2 = file2.getSize() / 1024;
            documentName2 = FilenameUtils.getBaseName(filename2);
            extension2 = FilenameUtils.getExtension(filename2);

        }
        if (file3 != null) {
            if (file3.getOriginalFilename() != null
                    && file3.getOriginalFilename().length() > 0) {
                filename3 = file3.getOriginalFilename();
                String ext3 = filename3.substring(filename3.lastIndexOf("."));
                byte[] byteArray3 = file3.getBytes();
                String absolutePath = documentService
                        .getAbsoluteSaveLocationDir(1);

                File uploadedFile = documentService.save(byteArray3, ext3,
                        filename3, absolutePath);
                agreementFileUlr3 = uploadedFile.getAbsolutePath();
                agreementFileUlr3 = agreementFileUlr3
                        .split(propsValue.documentsRoot)[1];
            }
            sizeofFile3 = file3.getSize() / 1024;
            documentName3 = FilenameUtils.getBaseName(filename3);
            extension3 = FilenameUtils.getExtension(filename3);

        }
        if (file4 != null) {
            if (file4.getOriginalFilename() != null
                    && file4.getOriginalFilename().length() > 0) {
                filename4 = file4.getOriginalFilename();
                String ext4 = filename4.substring(filename4.lastIndexOf("."));
                byte[] byteArray4 = file4.getBytes();
                String absolutePath = documentService
                        .getAbsoluteSaveLocationDir(1);

                File uploadedFile = documentService.save(byteArray4, ext4,
                        filename4, absolutePath);
                agreementFileUlr4 = uploadedFile.getAbsolutePath();
                agreementFileUlr4 = agreementFileUlr4
                        .split(propsValue.documentsRoot)[1];
            }
            sizeofFile4 = file4.getSize() / 1024;
            documentName4 = FilenameUtils.getBaseName(filename4);
            extension4 = FilenameUtils.getExtension(filename4);

        }

        try {
            if ((DOCUMENT.equals(contentType))
                    && !(BLUE_PRINT.equals(chosenSection))) {
                atmadocumentService.save(moduleId, categoryId, chosenSection,
                        documentName1, agreementFileUlr1, contentType, link1,
                        extension1, docTitle1, sizeofFile1,
                        documentDescription1, lifeStage1);
                if (documentName2 != "") {
                    atmadocumentService.save(moduleId, categoryId,
                            chosenSection, documentName2, agreementFileUlr2,
                            contentType, link2, extension2, docTitle2,
                            sizeofFile2, documentDescription2, lifeStage2);
                }
                if (documentName3 != "") {
                    atmadocumentService.save(moduleId, categoryId,
                            chosenSection, documentName3, agreementFileUlr3,
                            contentType, link3, extension3, docTitle3,
                            sizeofFile3, documentDescription3, lifeStage3);
                }

            } else if ((DOCUMENT.equals(contentType))
                    && (BLUE_PRINT.equals(chosenSection))) {
                if(session.getAttribute(BLUEPRINT_IMAGES) == null){
                    AtmaDocumentMaster documentMaster = new AtmaDocumentMaster();
                    documentMaster.setModule(module);
                    documentMaster.setCategory(category);
                    documentMaster.setSection(chosenSection);
                    documentMaster.setContentType(contentType);
                    documentMaster.setLifeStages(lifeStage4);
                    documentMaster.setTitle(docTitle4);
                    documentMaster.setDocumentDescription(documentDescription4);

                    List<ModuleMaster> moduleList = moduleService.findByActive(true);
                    List<CategoryMaster> categoryList = categoryService
                            .findByModuleId(documentMaster.getModule()
                                    .getModuleId());
                    List<LifeStages> lifeStageList = new ArrayList<>(
                            Arrays.asList(LifeStages.values()));
                    model.put(DOCUMENT_MASTER, documentMaster);
                    model.put(SECTIONS, sectionList);
                    model.put(MODULE_LIST, moduleList);
                    model.put(CATEGORY_LIST, categoryList);
                    model.put(LIFE_STAGE_LIST, lifeStageList);
                    model.put(ERROR, "alert.youneedtoselectatleastoneimage");
                    return "admin/mngt/documentmngt/adddocument";
                }
                else{
                    AtmaDocumentMaster document = atmadocumentService.save(
                            moduleId, categoryId, chosenSection, documentName4,
                            agreementFileUlr4, contentType, link1, extension4,
                            docTitle4, sizeofFile4, documentDescription4,
                            lifeStage4);
                    atmadocumentService.saveDocument(document, bluePrintImages);
                    session.removeAttribute(BLUEPRINT_IMAGES);
                }   
            } else {
                atmadocumentService.save(moduleId, categoryId, chosenSection,
                        documentName1, agreementFileUlr1, contentType, link1,
                        extension1, linkTitle1, sizeofFile1,
                        documentDescription1, lifeStage1);
                if (!link2.contentEquals("") && link2!=null) {
                    atmadocumentService.save(moduleId, categoryId,
                            chosenSection, documentName2, agreementFileUlr2,
                            contentType, link2, extension2, linkTitle2,
                            sizeofFile2, documentDescription2, lifeStage2);
                }
                if (!link3.contentEquals("") && link2!=null) {
                    atmadocumentService.save(moduleId, categoryId,
                            chosenSection, documentName3, agreementFileUlr3,
                            contentType, link3, extension3, linkTitle3,
                            sizeofFile3, documentDescription3, lifeStage3);
                }

            }

        } catch (DuplicateDocumentException e) {
            exceptionFlag = true;
            model.put(ERROR, e.getErrorCode());
            LOGGER.info(e);
        } catch (DuplicateLinkException e) {
            exceptionFlag = true;
            model.put(ERROR, e.getErrorCode());
            LOGGER.info(e);
        } catch (DocumentPresentException e) {
            exceptionFlag = true;
            model.put(ERROR, e.getErrorCode());
            LOGGER.info(e);
        }

        if (!exceptionFlag) {
            model.put(SUCCESS, "alert.documentsaved");
            return REDIRECT + url1 + "/" + url2;
        } else {
            return REDIRECT + url1 + "/" + url2;
        }

    }

    @RequestMapping("/*/*/UploadBluePrintImage")
    public String uploadBlueprintImage(
            @RequestParam("file") MultipartFile file, HttpServletRequest request)
                    throws IOException {
        String filename = file.getOriginalFilename();

        String ext = filename.substring(filename.lastIndexOf("."));
        ImageMaster image = null;
        try {
        	  image = imageService.addImage(file.getBytes(), ext,
                      "BluePrintImg" + ext, true, false, BLUEPRINT_IMAGES);
		} catch (Exception e) {
			e.printStackTrace();
		}
      
        HttpSession session = request.getSession(true);
        List<ImageMaster> bluePrintImages = (List<ImageMaster>) session
                .getAttribute(BLUEPRINT_IMAGES);
        if (bluePrintImages == null) {
            bluePrintImages = new ArrayList<>();
        }
        bluePrintImages.add(image);
        session.setAttribute(BLUEPRINT_IMAGES, bluePrintImages);
        return file.getOriginalFilename();
    }

    @RequestMapping(value = "/{url1}/{url2}/UpdateDocument")
    public String updateDocument(
            @PathVariable("url1") String url1,
            @PathVariable("url2") String url2,
            @RequestParam("documentId") Long documentId,
            @RequestParam("chosenModuleId") long moduleId,
            @RequestParam("chosenCategoryId") long categoryId,
            @RequestParam("chosenSectionId") String chosenSection,
            @RequestParam("chosenContentId") String contentType,
            @RequestParam("title") String title,
			@RequestParam(value = "document", required = false) MultipartFile file,
            @RequestParam(value = "documentDescription", required = false) String documentDescription,
            @RequestParam(value = "chosenLifeStage", required = false) LifeStages lifeStage,
            @RequestParam(value = "linkName", required = false) String linkName,
            HttpServletRequest request,
            Map<String, Object> model) throws IOException{

        boolean exceptionFlag = false;
       
        ModuleMaster module = moduleService.findByModuleId(moduleId);
        CategoryMaster category = categoryService.findByCategoryId(categoryId);


        HttpSession session = request.getSession(true);
        List<ImageMaster> bluePrintImages = new ArrayList<>();
        if (session.getAttribute(BLUEPRINT_IMAGES) != null) {
            bluePrintImages = (List<ImageMaster>) session
                    .getAttribute(BLUEPRINT_IMAGES);
        }

        String filename = "";
        String agreementFileUlr = "";
		if(contentType.equalsIgnoreCase("Document")){
            if (file == null || file.getSize() == 0) {
                AtmaDocumentMaster document1 = atmadocumentService.update(
                        documentId, moduleId, categoryId, chosenSection,
                        contentType,
                        title, documentDescription,  linkName,
                        lifeStage);
                if (BLUE_PRINT.equals(chosenSection)) {
                    atmadocumentService
                    .saveDocument(document1, bluePrintImages);
                    session.removeAttribute(BLUEPRINT_IMAGES);

                }
                model.put(SUCCESS, "alert.documentupdated");
                return REDIRECT + url1 + "/" + url2;
            }
            else{
                if (file.getOriginalFilename() != null
                        && file.getOriginalFilename().length() > 0) {
                    filename = file.getOriginalFilename();
                    String ext = filename.substring(filename.lastIndexOf("."));
                    byte[] byteArray = file.getBytes();
                    String absolutePath = documentService
                            .getAbsoluteSaveLocationDir(1);
                    File uploadedFile = documentService.save(byteArray, ext,
                            filename, absolutePath);
                    agreementFileUlr = uploadedFile.getAbsolutePath();
                    agreementFileUlr = agreementFileUlr
                            .split(propsValue.documentsRoot)[1];
                }

                Long sizeofFile = file.getSize() / 1024;
                String extension = FilenameUtils.getExtension(filename);
                String documentName = FilenameUtils.getBaseName(filename);
                try {
                    AtmaDocumentMaster document2 = atmadocumentService.update(
                            documentId, moduleId, categoryId, chosenSection,
                            documentName, agreementFileUlr, contentType, linkName,
                            extension, title, sizeofFile, documentDescription,
                            lifeStage);

                    saveDoc(chosenSection,document2,bluePrintImages,session);

                } catch (DuplicateDocumentException e) {
                    exceptionFlag = true;
                    model.put(ERROR, e.getErrorCode());
                    LOGGER.info(e);
                } catch (DuplicateLinkException e) {
                    LOGGER.info(e);
                    exceptionFlag = true;
                    model.put(ERROR, e.getErrorCode());

                }

                if (!exceptionFlag) {
                    model.put(SUCCESS, "alert.documentupdated");
                    return REDIRECT + url1 + "/" + url2;
                } else {
                    AtmaDocumentMaster documentMaster = new AtmaDocumentMaster();
                    documentMaster.setModule(module);
                    documentMaster.setDocumentId(documentId);
                    documentMaster.setSection(chosenSection);
                    documentMaster.setCategory(category);
                    documentMaster.setDocumentName(documentName);
                    documentMaster.setDocumentDescription(documentDescription);
                    documentMaster.setContentType(contentType);
                    documentMaster.setLink(linkName);
                    documentMaster.setTitle(title);
                    documentMaster.setLifeStages(lifeStage);
                    model.put(DOCUMENT_MASTER, documentMaster);
                    List<ModuleMaster> moduleList = moduleService
                            .findByActive(true);

                    List<CategoryMaster> categoryList = categoryService
                            .findByModuleId(documentMaster.getModule()
                                    .getModuleId());
                    List<LifeStages> lifeStageList = new ArrayList<>(
                            Arrays.asList(LifeStages.values()));
                    model.put(MODULE_LIST, moduleList);
                    model.put(CATEGORY_LIST, categoryList);
                    model.put(SECTIONS, sectionList);
                    model.put(LIFE_STAGE_LIST, lifeStageList);
                    return "admin/mngt/documentmngt/editdocument";
                }
            }

        }
        else{
            String documentName = "";
            String extension = "";
            Long sizeofFile = null;
            AtmaDocumentMaster documentMaster1 = atmadocumentService.findByDocumentId(documentId);
            if(documentMaster1.getLink().equalsIgnoreCase(linkName)){
                atmadocumentService.update(
                        documentId, moduleId, categoryId, chosenSection,
                        contentType,
                        title, documentDescription,  linkName,
                        lifeStage);
                model.put(SUCCESS, "alert.linkupdated");
                return REDIRECT + url1 + "/" + url2;
            }
            else{
                try {
                    atmadocumentService.update(
                            documentId, moduleId, categoryId, chosenSection,
                            documentName, agreementFileUlr, contentType, linkName,
                            extension, title, sizeofFile, documentDescription,
                            lifeStage);
                } catch (DuplicateDocumentException e) {
                    exceptionFlag = true;
                    model.put(ERROR, e.getErrorCode());
                    LOGGER.info(e);
                } catch (DuplicateLinkException e) {
                    exceptionFlag = true;
                    model.put(ERROR, e.getErrorCode());
                    LOGGER.info(e);
                }
                if (!exceptionFlag) {
                    model.put(SUCCESS, "alert.linkupdated");
                    return REDIRECT + url1 + "/" + url2;
                } else {
                    AtmaDocumentMaster documentMaster = new AtmaDocumentMaster();
                    documentMaster.setDocumentId(documentId);
                    documentMaster.setModule(module);
                    documentMaster.setCategory(category);
                    documentMaster.setSection(chosenSection);
                    documentMaster.setContentType(contentType);
                    documentMaster.setLink(linkName);
                    documentMaster.setTitle(title);

                    model.put(DOCUMENT_MASTER, documentMaster);
                    List<ModuleMaster> moduleList = moduleService
                            .findByActive(true);

                    List<CategoryMaster> categoryList = categoryService
                            .findByModuleId(documentMaster.getModule()
                                    .getModuleId());
                    List<LifeStages> lifeStageList = new ArrayList<>(
                            Arrays.asList(LifeStages.values()));
                    model.put(MODULE_LIST, moduleList);
                    model.put(CATEGORY_LIST, categoryList);
                    model.put(SECTIONS, sectionList);
                    model.put(LIFE_STAGE_LIST, lifeStageList);
                    return "admin/mngt/documentmngt/editdocument";
                }
            }
        }
    }

    private HttpSession saveDoc(String chosenSection, AtmaDocumentMaster document2, List<ImageMaster> bluePrintImages, HttpSession session) {

        if (BLUE_PRINT.equals(chosenSection)) {
            atmadocumentService.saveDocument(document2, bluePrintImages);
            session.removeAttribute(BLUEPRINT_IMAGES);

        }
        return session;
    }

    @RequestMapping(value = "/SaveForum")
    public String addforum(@RequestParam("moduleId") Long id,
            @RequestParam("title") String title,
            @RequestParam("tagDiv") ForumTag tag, 
            @RequestParam("forumDescription") String forumDescription,
            Map<String, Object> model)
                    throws DuplicateForumException {
        boolean exceptionFlag = false;
        ModuleMaster module = moduleService.findByModuleId(id);

        try {
            ForumMaster forum = new ForumMaster();
            forum.setTitle(title);
            forum.setTag(tag);
            forum.setModule(module);
            forum.setFlag(true);
            forum.setDescription(forumDescription);
            forumService.save(forum);
        } catch (DuplicateForumException e) {
            exceptionFlag = true;
            model.put(ERROR, e.getErrorCode());
            LOGGER.info(e);
        }

        if (!exceptionFlag) {
            return "redirect:/OrganisationalDevelopmentArea?id=" + id;
        } else {
            ForumMaster forum = new ForumMaster();
            forum.setTitle(title);
            forum.setTag(tag);
            forum.setModule(module);
            forum.setFlag(true);
            forum.setDescription(forumDescription);
            model.put("forum", forum);
            List<ForumTag> forumList = Arrays.asList(ForumTag.values());
            model.put("forumList", forumList);
            return "redirect:/OrganisationalDevelopmentArea?id=" + id;
        }
    }

    @RequestMapping("/SaveComment")
    public String saveComment(
            @RequestParam("moduleId") Long moduleId,
            @RequestParam("categoryId") Long categoryId,
            @RequestParam("comments") String comments) {

        Comments comment1 = new Comments();
        comment1.setComments(comments);
        comment1.setFlag(true);
        ModuleMaster module = moduleService.findByModuleId(moduleId);
        CategoryMaster category = categoryService.findByCategoryId(categoryId);
        comment1.setModule(module);
        comment1.setCategory(category);
        try {
            commentService.save(comment1);
        } catch (Exception e) {
            LOGGER.info(e);

        }
        return "redirect:/Projects?id=" + categoryId;
    }

    @RequestMapping("/SaveForumComment")
    public String saveForumComment(
            @RequestParam("forumId") Long forumId,
            @RequestParam("comments") String forumComment) {


        Comments comment1 = new Comments();
        comment1.setActive(true);
        comment1.setFlag(true);
        comment1.setComments(forumComment);
        ForumMaster forum = forumService.findByForumId(forumId);
        comment1.setForum(forum);
        String title = forum.getTitle();
        Long id = forum.getForumId();
        try {
            commentService.save(comment1);
        } catch (Exception e) {
            LOGGER.info(e);
        }
        return "redirect:ForumComments/"+id;
    }

    @RequestMapping("/SavePersonalDetails")
    public String savePersonalDetails(
            @RequestParam("userId") Long userId,
            @RequestParam("firstName") String firstName,
            @RequestParam("lastName") String lastName,
            @RequestParam(value="mobile") String mobile,
            @RequestParam("gender") String gender,
            @RequestParam("designation") String designation,
            @RequestParam(value = "linkedinUrl", required = false) String linkedinUrl,
            @RequestParam("eduBackground") String eduBackground,
            @RequestParam("workExperience") String workExp,
            @RequestParam("functionalArea") String functionalArea,
            @RequestParam(value = "funcAreaOther", required = false) String funcArea,
            @RequestParam("aboutNetwork") String aboutNetwork,
            @RequestParam(value = "networkOther", required=false) String networkOther,
            HttpServletRequest request,
            Map<String, Object> model) throws NoSuchUserException {
        User user = userService.findById(userId);
        PersonalInfo personalInfo = personalInfoService.findById(userId);
        PersonalInfo personalInfo1 = new PersonalInfo();

        int profileCompletionPercent;
        int totalCompletionRate = user.getTotalProfileCompletion();
        int profileCompletionRate = user.getProfileCompletionRate();
        if(user.getGender()==null){
            profileCompletionRate = profileCompletionRate + ProfileWeightage.GENDER.getId();
        }
        if(personalInfo!=null){
            if(!linkedinUrl.isEmpty() && personalInfo.getLinkedin().isEmpty()){
                profileCompletionRate = profileCompletionRate + ProfileWeightage.LINKEDIN.getId();
            }
            profileCompletionPercent = 100 * profileCompletionRate/totalCompletionRate;
        }
        else{
            profileCompletionRate = profileCompletionRate + ProfileWeightage.EDUCATIONAL_BACKGROUND.getId();
            profileCompletionRate = profileCompletionRate + ProfileWeightage.WORK_EXPERIENCE.getId();
            profileCompletionRate = profileCompletionRate + ProfileWeightage.FUNCTIONAL_AREAS.getId();
            profileCompletionRate = profileCompletionRate + ProfileWeightage.NETWORK_KNOWLEDGE.getId();
            profileCompletionRate = profileCompletionRate + ProfileWeightage.DESIGNATION.getId();
            if(!linkedinUrl.isEmpty()){
                profileCompletionRate = profileCompletionRate + ProfileWeightage.LINKEDIN.getId();
            }
            profileCompletionPercent = 100 * profileCompletionRate/totalCompletionRate;
        }

        try{
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setMobile(mobile);
            user.setGender(gender);
            user.setDesignation(designation);
            user.setProfileCompletion(profileCompletionPercent);
            user.setProfileCompletionRate(profileCompletionRate);
            User user1 = userService.update(user);
            updateUserinSession(user1,request);
            if(personalInfo!=null){                             
                personalInfo.setAboutNetwork(aboutNetwork);
                personalInfo.setEducationalBackground(eduBackground);
                personalInfo.setFuncAreaReason(funcArea);
                personalInfo.setFunctionalAreas(functionalArea);
                personalInfo.setLinkedin(linkedinUrl);
                personalInfo.setNetworkReason(networkOther);
                personalInfo.setWorkExperience(workExp);
                personalInfo.setUser(user1);
                personalInfoService.save(personalInfo);
            }
            else{

                personalInfo1.setAboutNetwork(aboutNetwork);
                personalInfo1.setEducationalBackground(eduBackground);
                personalInfo1.setFuncAreaReason(funcArea);
                personalInfo1.setFunctionalAreas(functionalArea);
                personalInfo1.setLinkedin(linkedinUrl);
                personalInfo1.setNetworkReason(networkOther);
                personalInfo1.setWorkExperience(workExp);
                personalInfo1.setUser(user1);
                personalInfoService.save(personalInfo1);

            }
        }catch(Exception e){
            LOGGER.info(e);
        }
        long id = user.getUserId();
        model.put(SUCCESS, "alert.detailssaved");
        if(user.getRole() == Role.COMPANY_ADMIN){
            return "redirect:/EditOrganisationalDetailsStep1/"+id;
        }
        else{
            return "redirect:/ModuleUser";
        }
    }

    public void updateUserinSession(User user, HttpServletRequest request){
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
                user.getUsername(), request.getSession().getAttribute("pass"));

        request.getSession();

        token.setDetails(new WebAuthenticationDetails(request));
        Authentication authenticatedUser = authenticationManager.authenticate(token);
        ((User)authenticatedUser.getPrincipal()).setMacId(user.getMacId());
        SecurityContextHolder.getContext().setAuthentication(authenticatedUser);

    }

    @RequestMapping("/SaveOrganisationalDetailsStep1")
    public String saveOrgDetailsStep1(
            @RequestParam("userId") Long userId,
            @RequestParam("companyId") Long companyId,
            @RequestParam("sectorWork")String sectorWork,
            @RequestParam("ngoEmail") String ngoEmail,
            @RequestParam(value="ngoPhone", required=false) String ngoPhone,
            @RequestParam(value ="ngoAddressLine1", required=false) String ngoAddress1,
            @RequestParam(value = "ngoAddressLine2", required=false) String ngoAddress2,
            @RequestParam(value= "ngoAddressLine3", required=false) String ngoAddress3,
            @RequestParam("stateDiv") Long stateId,
            @RequestParam("cityDiv") Long cityId,
            @RequestParam("countryDiv") Long countryId,
            @RequestParam("ngoPincode") String ngoPincode,
            @RequestParam("ngoWebsite") String ngoWebsite,
            @RequestParam("organisationVision") String organisationVision,
            @RequestParam("organisationMission") String organisationMission,
            @RequestParam("organisationDescription") String organisationDescription,
            @RequestParam("assitanceFromOtherAgency")boolean assitanceFromOtherAgency,
            @RequestParam("legalType")String legalType,
            @RequestParam("fcraRegistration")boolean fcraRegistarion,
            @RequestParam(value="typeOfEducation", required=false)String typeOfEducation,
            HttpServletRequest request,
            Map<String, Object> model
            ) throws IOException{


        boolean exceptionFlag=false;
        StateMaster ngoState = null;
        User user = userService.findById(userId);
        User admin = getLogedInUser();

        try {
            ngoState = stateService.findByStateId(stateId);
        } catch (NoSuchAreaException e) {
            exceptionFlag = true;
            model.put(ERROR, CustomException.NO_SUCH_STATE.getCode());
            LOGGER.info(e);
        }
        CityMaster ngoCity = null;
        if (!exceptionFlag) {
            try {
                ngoCity = cityService.findById(cityId);
            } catch (NoSuchCityException e) {
                exceptionFlag = true;
                model.put(ERROR, CustomException.NO_SUCH_CITY.getCode());
                LOGGER.info(e);
            }
        }
        CountryMaster ngoCountry = null;
        if (!exceptionFlag) {
            try {
                ngoCountry = countryService.findById(countryId);
            } catch (NoSuchCountryException e) {
                exceptionFlag = true;
                model.put(ERROR, CustomException.NO_SUCH_COUNTRY.getCode());
                LOGGER.info(e);
            }
        }   

        int totalCompletionRate = user.getTotalProfileCompletion() == 0 ? 1 : user.getTotalProfileCompletion();
        int profileCompletionRate = user.getProfileCompletionRate();

        try{
            CompanyMaster company = companyService.findByCompanyId(companyId);

            if(!ngoPhone.isEmpty() && company.getCompanyTelephone1().isEmpty()){
                profileCompletionRate = profileCompletionRate + ProfileWeightage.TELEPHONE.getId();
            }
            if(!ngoAddress1.isEmpty() && company.getCompanyAddressLine1().isEmpty()){
                profileCompletionRate = profileCompletionRate + ProfileWeightage.COMPANY_ADDRESSLINE1.getId();
            }
            if(!ngoAddress2.isEmpty() && company.getCompanyAddressLine2().isEmpty()){
                profileCompletionRate = profileCompletionRate + ProfileWeightage.COMPANY_ADDRESSLINE2.getId();
            }
            if(!ngoAddress3.isEmpty() && company.getCompanyAddressLine3().isEmpty()){
                profileCompletionRate = profileCompletionRate + ProfileWeightage.COMPANY_ADDRESSLINE3.getId();
            }
            if(!typeOfEducation.isEmpty() && company.getTypeOfEducation().isEmpty()){
                profileCompletionRate = profileCompletionRate + ProfileWeightage.TYPE_OF_EDUCATION.getId();
            }

            int profileCompletionPercent = 100 * profileCompletionRate/totalCompletionRate;

            company.setCompanyAddressLine1(ngoAddress1);
            company.setAssitanceFromOtherAgency(assitanceFromOtherAgency);
            company.setCompanyAddressLine2(ngoAddress2);
            company.setCompanyAddressLine3(ngoAddress3);
            company.setCity(ngoCity);
            company.setCompanyEmail(ngoEmail);
            company.setCompanyPincode(ngoPincode);
            company.setCompanyTelephone1(ngoPhone);
            company.setCompanyWebsite(ngoWebsite);
            company.setCountry(ngoCountry);
            company.setFcraRegistarion(fcraRegistarion);
            company.setLegalType(legalType);
            company.setOrganisationDescription(organisationDescription);
            company.setOrganisationMission(organisationMission);
            company.setOrganisationVision(organisationVision);
            company.setTypeOfEducation(typeOfEducation);
            company.setSectorWork(sectorWork);
            company.setState(ngoState);
            companyService.update(company);
            user.setProfileCompletion(profileCompletionPercent);
            user.setProfileCompletionRate(profileCompletionRate);
            User user1 = userService.update(user);

            if(!Role.SUPER_ADMIN.equals(admin.getRole()))
                updateUserinSession(user1,request);
        }
        catch (DuplicateEmailException e) {
            exceptionFlag = true;
            LOGGER.info(e);
            model.put(ERROR, e.getErrorCode());
        }
        catch (DuplicateCompanyException e) {
            LOGGER.info(e);
            exceptionFlag = true;
            model.put(ERROR, e.getErrorCode());
        }
        if (!exceptionFlag) {
            model.put(SUCCESS, "alert.orgdetails1savedsuccessfully");
            if(Role.SUPER_ADMIN.equals(admin.getRole())){
                return "redirect:/EditOrganisationalDetailsByAdmin/step/2/company/" + companyId;
            }else {
                return "redirect:/EditOrganisationalDetailsStep2/" + userId;
            }


        } else {
            CompanyMaster company = companyService.findByCompanyId(companyId);
            List<CityMaster> cityList = cityService.findByActive(true);
            List<CountryMaster> countryList = countryService.findByActive(true);
            List<StateMaster> stateList = stateService.findByActive(true);

            List<String> educationList = new ArrayList<>();
            educationList.add("Full Time School");
            educationList.add("Early Childhood Organisation");
            educationList.add("Special Education");
            educationList.add("Remediation And Coaching");
            educationList.add("LifeSkills");
            educationList.add("Girls Education");
            model.put("educationList", educationList);
            model.put("cityList", cityList);
            model.put(COUNTRY_LIST, countryList);
            model.put(STATE_LIST, stateList);
            model.put("company", company);
            if(Role.SUPER_ADMIN.equals(admin.getRole())){
                return "redirect:/EditOrganisationalDetailsByAdmin/step/1/company/" + companyId;
            }else {
                return "redirect:/EditOrganisationalDetailsStep1/" + userId;
            }
        }
    }

    @RequestMapping("/SaveOrganisationalDetailsStep2")
    public String saveOrgDetailsStep2(
            @RequestParam("userId") Long userId,
            @RequestParam("companyId") Long companyId,
            @RequestParam("inclination") boolean inclination,
            @RequestParam(value="inclinationReason", required=false) String inclinationReason,
            @RequestParam("operationArea") String operationArea,
            @RequestParam(value = "otherArea", required=false) String otherArea,
            @RequestParam("registrationNumber") String registrationNumber,
            @RequestParam("panNumber") String panNumber,
            @RequestParam("panNumberFile")  MultipartFile file1,
            @RequestParam("twelveACertificate") String twelveACertificate,
            @RequestParam("twelveAFile")  MultipartFile file2,
            @RequestParam("eightyGNumber") String eightyGNumber,
            @RequestParam("eightyGFile")  MultipartFile file3,
            @RequestParam("fcraregNumber") String fcraregNumber,
            @RequestParam("fcraForm")  MultipartFile file4,
            @RequestParam("noOfTrustees") String noOfTrustees,
            @RequestParam("noOfChildren") String noOfChildren,
            @RequestParam("noOfGirls") String noOfGirls,
            @RequestParam("noOfBoys") String noOfBoys,
            @RequestParam("ageOfStudents") String ageOfStudents,
            @RequestParam("funders") String funders,
            HttpServletRequest request,
            Map<String, Object> model) throws IOException{

        int children = Integer.parseInt(noOfChildren);
        int trustees = Integer.parseInt(noOfTrustees);
        int girls = Integer.parseInt(noOfGirls);
        int boys = Integer.parseInt(noOfBoys);
        CompanyMaster company = companyService.findByCompanyId(companyId);
        User user = userService.findById(userId);
        User admin = getLogedInUser();


        String filename1 = "";
        String agreementFileUlr1 = "";
        String documentName1 = "";
        String filename2 = "";
        String agreementFileUlr2 = "";
        String documentName2 = "";
        String filename3 = "";
        String agreementFileUlr3 = "";
        String documentName3 = "";
        String filename4 = "";
        String agreementFileUlr4 = "";
        String documentName4 = "";

        if (file1 != null) {
            if (file1.getOriginalFilename() != null
                    && file1.getOriginalFilename().length() > 0) {
                filename1 = file1.getOriginalFilename();
                String ext1 = filename1.substring(filename1.lastIndexOf("."));
                byte[] byteArray1 = file1.getBytes();
                String absolutePath = documentService
                        .getAbsoluteSaveLocationDir(company.getCompanyName());
                File uploadedFile =  null;
                try{
                    uploadedFile = documentService.save(byteArray1, ext1,
                            filename1, absolutePath);
                }
                catch (EOFException e) {
                    LOGGER.info(e);
                } catch(IOException e) {
                    LOGGER.info(e);
                }
                agreementFileUlr1 = uploadedFile.getAbsolutePath();
                agreementFileUlr1 = agreementFileUlr1
                        .split(propsValue.documentsRoot)[1];
            }
            documentName1 = FilenameUtils.getBaseName(filename1);

        }
        if (file2 != null) {
            if (file2.getOriginalFilename() != null
                    && file2.getOriginalFilename().length() > 0) {
                filename2 = file2.getOriginalFilename();
                String ext2 = filename2.substring(filename2.lastIndexOf("."));
                byte[] byteArray2 = file2.getBytes();
                String absolutePath = documentService
                        .getAbsoluteSaveLocationDir(company.getCompanyName());
                File uploadedFile =  null;
                try{
                    uploadedFile = documentService.save(byteArray2, ext2,
                            filename2, absolutePath);
                }
                catch (EOFException e) {
                    LOGGER.info(e);
                } catch(IOException e) {
                    LOGGER.info(e);
                }
                agreementFileUlr2 = uploadedFile.getAbsolutePath();
                agreementFileUlr2 = agreementFileUlr2
                        .split(propsValue.documentsRoot)[1];
            }
            documentName2 = FilenameUtils.getBaseName(filename2);
        }
        if (file3 != null) {
            if (file3.getOriginalFilename() != null
                    && file3.getOriginalFilename().length() > 0) {
                filename3 = file3.getOriginalFilename();
                String ext3 = filename3.substring(filename3.lastIndexOf("."));
                byte[] byteArray3 = file3.getBytes();
                String absolutePath = documentService
                        .getAbsoluteSaveLocationDir(company.getCompanyName());
                File uploadedFile =  null;
                try{
                    uploadedFile = documentService.save(byteArray3, ext3,
                            filename3, absolutePath);
                }
                catch (EOFException e) {
                    LOGGER.info(e);
                } catch(IOException e) {
                    LOGGER.info(e);
                }
                agreementFileUlr3 = uploadedFile.getAbsolutePath();
                agreementFileUlr3 = agreementFileUlr3
                        .split(propsValue.documentsRoot)[1];
            }
            documentName3 = FilenameUtils.getBaseName(filename3);
        }
        if (file4 != null) {
            if (file4.getOriginalFilename() != null
                    && file4.getOriginalFilename().length() > 0) {
                filename4 = file4.getOriginalFilename();
                String ext4 = filename4.substring(filename4.lastIndexOf("."));
                byte[] byteArray4 = file4.getBytes();
                String absolutePath = documentService
                        .getAbsoluteSaveLocationDir(company.getCompanyName());
                File uploadedFile =  null;
                try{
                    uploadedFile = documentService.save(byteArray4, ext4,
                            filename4, absolutePath);
                }
                catch (EOFException e) {
                    LOGGER.info(e);
                } catch(IOException e) {
                    LOGGER.info(e); 
                }
                agreementFileUlr4 = uploadedFile.getAbsolutePath();
                agreementFileUlr4 = agreementFileUlr4
                        .split(propsValue.documentsRoot)[1];
            }
            documentName4 = FilenameUtils.getBaseName(filename4);
        }


        int totalCompletionRate = user.getTotalProfileCompletion();
        int profileCompletionRate = user.getProfileCompletionRate();
        profileCompletionRate=getProfileCompletionRates(company,profileCompletionRate,trustees,boys,children,girls);

        int profileCompletionPercent = 100 * profileCompletionRate/totalCompletionRate;

        company.setRegistrationNumber(registrationNumber);
        company.setPanNumber(panNumber);
        company.setReligiousInclination(inclination);
        company.setReasonOfInclination(inclinationReason);
        company.setAreaOfOperations(operationArea);
        company.setReasonForOtherArea(otherArea);
        company.setTwelveACertificate(twelveACertificate);
        company.setEightyGNumber(eightyGNumber);
        company.setFcraRegNumber(fcraregNumber);
        company.setNoOfTrustees(trustees);
        company.setNoOfChildren(children);
        company.setNoOfGirls(girls);
        company.setNoOfBoys(boys);
        company.setAgeOfStudents(ageOfStudents);
        company.setMainSourceOfFunding(funders);
        if(file1.getSize()!=0){
            company.setPanFile(agreementFileUlr1);
            company.setPanDocumentName(documentName1);
        }
        if(file2.getSize()!=0){
            company.setTwelveAFile(agreementFileUlr2);
            company.setTwelveADocumentName(documentName2);
        }
        if(file3.getSize()!=0){
            company.setEightyGFile(agreementFileUlr3);
            company.setEightyGDocumentName(documentName3);
        }
        if(file4.getSize()!=0){
            company.setFcraFile(agreementFileUlr4);
            company.setFcraDocumentName(documentName4);
        }
        companyService.update(company);
        user.setProfileCompletion(profileCompletionPercent);
        user.setProfileCompletionRate(profileCompletionRate);
        User user1 = userService.update(user);

        if(!Role.SUPER_ADMIN.equals(admin.getRole()))
            updateUserinSession(user1,request);
        model.put(SUCCESS, "alert.orgdetails2savedsuccessfully");

        if(Role.SUPER_ADMIN.equals(admin.getRole())){
            return "redirect:/EditOrganisationalDetailsByAdmin/step/3/company/" + companyId;
        }else {
            return "redirect:/EditOrganisationalDetailsStep3/" + userId;
        }
    }

    private int getProfileCompletionRates(CompanyMaster company, int profileCompletionRates, int trustees, int boys, int children, int girls) {

        int profileCompletionRate= profileCompletionRates;
        if(company.getAreaOfOperations() == null){
            profileCompletionRate = profileCompletionRate + ProfileWeightage.AREA_OF_OPERATION.getId();
        }
        if(company.getRegistrationNumber() == null){
            profileCompletionRate = profileCompletionRate + ProfileWeightage.REGISTRATION_NUMBER.getId();
        }
        if(company.getPanNumber() == null){
            profileCompletionRate = profileCompletionRate + ProfileWeightage.PAN_NUMBER.getId();
        }
        if(company.getPanFile() == null){
            profileCompletionRate = profileCompletionRate + ProfileWeightage.PAN_FILE.getId();
        }
        if(company.getTwelveACertificate() == null){
            profileCompletionRate = profileCompletionRate + ProfileWeightage.TWELVE_A_CERTIFICATE.getId();
        }
        if(company.getTwelveAFile() == null){
            profileCompletionRate = profileCompletionRate + ProfileWeightage.TWELVE_A_FILE.getId(); 
        }
        if(company.getEightyGNumber() == null){
            profileCompletionRate = profileCompletionRate + ProfileWeightage.EIGHTY_G_NUMBER.getId();
        }
        if(company.getEightyGFile() == null){
            profileCompletionRate = profileCompletionRate + ProfileWeightage.EIGHTY_G_FILE.getId();
        }
        if(company.getFcraRegNumber() == null){
            profileCompletionRate = profileCompletionRate + ProfileWeightage.FCRA_REG_NUMBER.getId();
        }
        if(company.getFcraFile() == null){
            profileCompletionRate = profileCompletionRate + ProfileWeightage.FCRA_FILE.getId();
        }
        if(company.getNoOfTrustees() == 0 && trustees!=0){
            profileCompletionRate = profileCompletionRate + ProfileWeightage.NO_OF_TRUSTEES.getId();
        }
        if(company.getNoOfBoys() == 0 && boys!=0){
            profileCompletionRate = profileCompletionRate + ProfileWeightage.NO_OF_BOYS.getId();
        }
        if(company.getNoOfChildren() == 0 && children!=0){
            profileCompletionRate = profileCompletionRate + ProfileWeightage.NO_OF_CHILDREN.getId();
        }
        if(company.getNoOfGirls() == 0 && girls!=0){
            profileCompletionRate = profileCompletionRate + ProfileWeightage.NO_OF_GIRLS.getId();
        }
        if(company.getAgeOfStudents() == null){
            profileCompletionRate = profileCompletionRate + ProfileWeightage.AGE_STUDENTS.getId();
        }
        if(company.getMainSourceOfFunding() == null){
            profileCompletionRate = profileCompletionRate + ProfileWeightage.MAIN_SOURCE_FUNDING.getId();
        }   

        return profileCompletionRate;
    }

    @RequestMapping("/SaveOrganisationalDetailsStep3")
    public String saveOrgDetailsStep3(
            @RequestParam("userId") Long userId,
            @RequestParam("companyId") Long companyId,
            @RequestParam("noOfEmployees") String noOfEmployees,
            @RequestParam("listOfTrustees") String listOfTrustees,
            @RequestParam("audit1")  MultipartFile file1,
            @RequestParam("audit2") MultipartFile file2,
            @RequestParam("budgetSize") String budgetSize,
            @RequestParam("existingFunders") String existingFunders,
            @RequestParam(value="existFundOther", required=false) String otherFunders,
            HttpServletRequest request,
            Map<String, Object> model) throws IOException{

        Long budget = Long.parseLong(budgetSize);
        CompanyMaster company = companyService.findByCompanyId(companyId);
        User user = userService.findById(userId);
        User admin = getLogedInUser();
        String filename1 = "";
        String agreementFileUlr1 = "";
        String documentName1 = "";
        Long sizeofFile1 = null;
        String filename2 = "";
        String agreementFileUlr2 = "";
        String documentName2 = "";
        Long sizeofFile2 = null;
        if (file1 != null) {
            if (file1.getOriginalFilename() != null
                    && file1.getOriginalFilename().length() > 0) {
                filename1 = file1.getOriginalFilename();
                String ext1 = filename1.substring(filename1.lastIndexOf("."));
                byte[] byteArray1 = file1.getBytes();
                String absolutePath = documentService
                        .getAbsoluteSaveLocationDir(company.getCompanyName());

                File uploadedFile = documentService.save(byteArray1, ext1,
                        filename1, absolutePath);
                agreementFileUlr1 = uploadedFile.getAbsolutePath();
                agreementFileUlr1 = agreementFileUlr1
                        .split(propsValue.documentsRoot)[1];
            }
            documentName1 = FilenameUtils.getBaseName(filename1);
            sizeofFile1 = file1.getSize() / 1024;

        }

        if (file2 != null) {
            if (file2.getOriginalFilename() != null
                    && file2.getOriginalFilename().length() > 0) {
                filename2 = file2.getOriginalFilename();
                String ext2 = filename2.substring(filename2.lastIndexOf("."));
                byte[] byteArray2 = file2.getBytes();
                String absolutePath = documentService
                        .getAbsoluteSaveLocationDir(company.getCompanyName());

                File uploadedFile = documentService.save(byteArray2, ext2,
                        filename2, absolutePath);
                agreementFileUlr2 = uploadedFile.getAbsolutePath();
                agreementFileUlr2 = agreementFileUlr2
                        .split(propsValue.documentsRoot)[1];
            }
            documentName2 = FilenameUtils.getBaseName(filename2);
            sizeofFile2 = file2.getSize() / 1024;
        }

        int totalCompletionRate = user.getTotalProfileCompletion();
        int profileCompletionRate = user.getProfileCompletionRate();

        if(company.getNoOfEmployees() == null){
            profileCompletionRate = profileCompletionRate + ProfileWeightage.NO_OF_EMPLOYEES.getId();
        }
        if(company.getTrustees() == null){
            profileCompletionRate = profileCompletionRate + ProfileWeightage.TRUSTEES.getId();
        }
        if(company.getAudit1() == null){
            profileCompletionRate = profileCompletionRate + ProfileWeightage.AUDIT1.getId();
        }
        if(company.getAudit2() == null){
            profileCompletionRate = profileCompletionRate + ProfileWeightage.AUDIT2.getId();
        }
        if(company.getBudgetSize() == null){
            profileCompletionRate = profileCompletionRate + ProfileWeightage.BUDGET_SIZE.getId();
        }
        if(company.getExistingFunders() == null){
            profileCompletionRate = profileCompletionRate + ProfileWeightage.EXISTING_FUNDERS.getId();
        }       
        int profileCompletionPercent = 100 * profileCompletionRate/totalCompletionRate;

        company.setTrustees(listOfTrustees);    
        company.setNoOfEmployees(noOfEmployees);
        company.setExistingFunders(existingFunders);
        company.setOtherFunders(otherFunders);
        company.setBudgetSize(budget);
        if(file1.getSize()!=0){
            company.setAudit1(agreementFileUlr1);
            company.setSizeOfaudit1(sizeofFile1);
            company.setAudit1DocumentName(documentName1);
        }
        if(file2.getSize()!=0){
            company.setAudit2(agreementFileUlr2);
            company.setSizeOfaudit2(sizeofFile2);
            company.setAudit2DocumentName(documentName2);
        }
        companyService.update(company);
        user.setProfileCompletion(profileCompletionPercent);
        user.setProfileCompletionRate(profileCompletionRate);
        User user1 = userService.update(user);
        updateUserinSession(user1,request);

        if(Role.SUPER_ADMIN.equals(admin.getRole())){
            model.put(SUCCESS, ALERT);
            return "redirect:/ManageAdmin/OrganizationList";
        }else {
            if (user1.getRole() == Role.COMPANY_ADMIN) {
                model.put(SUCCESS, ALERT);
                return "redirect:/CompanyAdmin";
            } else {
                model.put(SUCCESS, ALERT);
                return "redirect:/ModuleUser";
            }
        }

    }

    @RequestMapping(value = "/{url1}/{url2}/SaveData")
    public String saveSurveyResultData(
            @PathVariable(value = "url1") String url1,
            @PathVariable(value = "url2") String url2,
            @RequestParam(value = "chosenModuleId") Long moduleId,
            @RequestParam(value = "chosenLifeStage1") LifeStages lifeStage1,
            @RequestParam(value = "surveyResultData") String surveyResultData,
            Map<String, Object> model) throws 
            IOException {


        ModuleMaster module = moduleService.findByModuleId(moduleId);
        LifeStageResultMaster resultSet = new LifeStageResultMaster();
        resultSet.setModule(module);
        resultSet.setResultdata(surveyResultData);
        resultSet.setLifeStages(lifeStage1);
        try {
            lifeStageResultService.save(resultSet);
        } catch (DuplicateLifeStageException e) {

            LOGGER.info(e);
            List<LifeStages> lifeStageList = new ArrayList<>(
                    Arrays.asList(LifeStages.values()));
            List<ModuleMaster> moduleList = moduleService.findByActive(true);
            model.put("resultSet", resultSet);
            model.put(MODULE_LIST, moduleList);
            model.put(LIFE_STAGE_LIST, lifeStageList);
            model.put(ERROR, e.getErrorCode());
            return "admin/surveymngt/addsurveyresult";
        }
        model.put(SUCCESS, "alert.resultsetsavedsuccessfully");
        return REDIRECT + url1 + "/" + url2;

    }

    @RequestMapping(value = "/{url1}/{url2}/UpdateData")
    public String updateSurveyResultData(
            @PathVariable(value = "url1") String url1,
            @PathVariable(value = "url2") String url2,
            @RequestParam(value="resultId") Long resultId,
            @RequestParam(value = "surveyResultData") String surveyResultData,
            Map<String, Object> model) throws IOException {
        LifeStageResultMaster resultSet = lifeStageResultService.findByResultId(resultId);
        resultSet.setResultdata(surveyResultData);
        lifeStageResultService.update(resultSet);
        model.put(SUCCESS, "alert.resultsetupdatedsuccessfully");
        return REDIRECT + url1 + "/" + url2;

    }

    @RequestMapping(value = "/*/SaveLanguage")
    public String addLanguage(
            @RequestParam(value = "languageId", required = false) Long languageId,
            @RequestParam("languageName") String languageName,
            @RequestParam("shortName") String shortName,
            Map<String, Object> model) throws DuplicateLanguageException
            {
        if (languageId > 0) { 
            languageService.update(languageId, languageName, shortName);
            model.put(SUCCESS, "alert.languageupdated");
        } else { 
            languageService.save(languageName, shortName);
            model.put(SUCCESS, "alert.languagesaved");
        }
        return "redirect:/*/Language";
            }

    @RequestMapping(value = "/*/DeleteLanguage")
    public String deleteLanguage(
            @RequestParam(value = "id", required = false) Long id,
            Map<String, Object> model) throws NoSuchLanguageException {
        languageService.delete(id);
        model.put(SUCCESS, "alert.languagedeleted");
        return "redirect:/*/Language";
    }

    @RequestMapping(value = "/*/RestoreLanguage")
    public String restoreLanguage(
            @RequestParam(value = "id", required = false) Long id,
            Map<String, Object> model) throws NoSuchLanguageException {
        languageService.restore(id);
        model.put(SUCCESS, "alert.languagerestored");
        return "redirect:/*/Language";
    }

    @RequestMapping(value = "/{url1}/{url2}/SaveUser")
    public String saveUser(@PathVariable("url1") String url1,
            @PathVariable("url2") String url2, HttpServletRequest request,
            Map<String, Object> model)
                    throws IOException {

        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String emailId = request.getParameter("emailId");
        String mobile = request.getParameter("mobile");

        User user = new User(firstName, lastName, true, Role.STUDENT.getId(),
                emailId, mobile, null, "");
        try {
            User user1 = userService.saveUser(user);
            userService.sendRegstrationMail(user1, null, null, "");

        } catch (DuplicateEmailException | DuplicateLoginException e) {
            model.put(ERROR, e.getErrorCode());
            LOGGER.info(e);
            return REDIRECT + url1 + "/" + url2;
        }
        model.put(SUCCESS, "alert.usersaved");
        return REDIRECT + url1 + "/" + url2;
    }

    @RequestMapping(value = "/{url1}/{url2}/UpdateUser")
    public String updateUser(@PathVariable("url1") String url1,
            @PathVariable("url2") String url2,
            @RequestParam("userId") Long userId,
            @RequestParam("firstName") String firstName,
            @RequestParam("lastName") String lastName,
            @RequestParam("emailId") String emailId,
            @RequestParam("mobile") String mobile,
            Map<String, Object> model)
                    throws IOException{

        try {
            userService
            .updateUser(userId, firstName, lastName, emailId, mobile);
        } catch (DuplicateLoginException e) {
            User user = new User();
            user = userService.findById(userId);
            String loginID = user.getLoginId();
            user.setUserId(userId);
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setEmailId(emailId);
            user.setMobile(mobile);
            user.setLoginId(loginID);
            model.put(ERROR, e.getErrorCode());

            model.put("user", user);
            LOGGER.info(e);
            return "admin/mngt/usermngt/edituser";

        }
        model.put(SUCCESS, "alert.userupdated");
        return REDIRECT + url1 + "/" + url2;
    }

    @RequestMapping(value = "{url}/SaveCity")
    public String saveCity(@PathVariable("url") String url,
            @RequestParam("cityName") String cityName,
            @RequestParam("chosenStateId") long stateId,
            @RequestParam("chosenCountryId") long countryId,
            Map<String, Object> model) throws IOException {

        try {
            cityService.save(cityName, stateId, countryId);
        } catch (DuplicateCityException e) {
            CountryMaster country = countryService.findById(countryId);
            StateMaster state = stateService.findByStateId(stateId);
            CityMaster city = new CityMaster();
            city.setCityName(cityName);
            city.setState(state);
            city.setCountry(country);
            List<StateMaster> stateList = stateService.findByActive(true);
            List<CountryMaster> countryList = countryService.findByActive(true);
            model.put("city", city);
            model.put(STATE_LIST, stateList);
            model.put(COUNTRY_LIST, countryList);
            model.put(ERROR, e.getErrorCode());
            LOGGER.info(e);
            return "admin/superadmin/masters/location/addcity";
        }

        model.put(SUCCESS, "alert.citysaved");
        return REDIRECT + url + CITY;
    }


    @RequestMapping(value = "/{url1}/{url2}/UpdateCity")
    public String updateCity(@PathVariable("url1") String url1,
            @RequestParam(value = "cityId", required = false) Long cityId,
            @RequestParam("cityName") String cityName,
            @RequestParam("chosenstateId") long stateId,
            @RequestParam("chosenCountryId") long countryId,
            Map<String, Object> model) throws IOException {

        try {
            cityService.update(cityId, cityName, stateId, countryId);
        } catch (DuplicateCityException e) {
            CountryMaster country = countryService.findById(countryId);
            StateMaster state = stateService.findByStateId(stateId);
            CityMaster city = new CityMaster();
            city.setCityId(cityId);
            city.setCityName(cityName);
            city.setState(state);
            city.setCountry(country);
            List<StateMaster> stateList = stateService.findByActive(true);
            List<CountryMaster> countryList = countryService.findByActive(true);
            model.put(STATE_LIST, stateList);
            model.put(COUNTRY_LIST, countryList);
            model.put("city", city);
            model.put(ERROR, e.getErrorCode());
            LOGGER.info(e);
            return "admin/superadmin/masters/location/editcity";
        }
        model.put(SUCCESS, "alert.cityupdated");
        return REDIRECT + url1 + CITY;
    }

    @RequestMapping("/{url1}/{url2}/AddCity")
    public String addCity( Map<String, Object> model) {

        List<CountryMaster> countryList = countryService.findByActive(true);
        model.put(COUNTRY_LIST, countryList);
        return "admin/superadmin/masters/location/addcity";
    }

    @RequestMapping("/{url1}/{url2}/EditCity")
    public String editCity(
            @RequestParam(value = "id") Long id,
            Map<String, Object> model) throws NoSuchAreaException 
            {

        CityMaster city = cityService.findById(id);
        List<StateMaster> stateList = stateService.findByActive(true);
        List<CountryMaster> countryList = countryService.findByActive(true);
        model.put("city", city);
        model.put(STATE_LIST, stateList);
        model.put(COUNTRY_LIST, countryList);

        return "admin/superadmin/masters/location/editcity";
            }

    @RequestMapping(value = "/{url}/DeleteCity")
    public String deleteCity(@PathVariable("url") String url,
            @RequestParam(value = "id", required = false) Long id,
            Map<String, Object> model) throws IOException
            {

        cityService.delete(id);
        model.put(SUCCESS, "alert.citydeleted");
        return REDIRECT + url + CITY;
            }

    @RequestMapping(value = "/{url}/RestoreCity")
    public String restoreCity(@PathVariable("url") String url,
            @RequestParam(value = "id", required = false) Long id,
            Map<String, Object> model) throws NoSuchAreaException {

        cityService.restore(id);
        model.put(SUCCESS, "alert.cityrestored");
        return REDIRECT + url + CITY;
    }

    @RequestMapping(value = "/{url}/UpdateProfile")
    public String updateStudentProfile(@PathVariable("url") String url,
            @RequestParam("firstName") String firstName,
            @RequestParam("lastName") String lastName,
            @RequestParam("mobile") String mobile,
            Map<String, Object> model) {

        User user = getLogedInUser();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMobile(mobile);
        userService.update(user);
        model.put("user", user);
        model.put(SUCCESS, "alert.profileupdatedsuccessfully");

        return REDIRECT + url + "/EditProfile";
    }


    @RequestMapping(value = { "/{url}/ResetUserPassword" }, method = RequestMethod.POST)
    public String resetUserPassword(@PathVariable("url") String url,
            @RequestParam("userId") Long userId,
            @RequestParam("oldpassword") String oldpassword,
            @RequestParam("password") String password,
            @RequestParam("confirmPassword") String confirmPassword,
            Map<String, Object> model) throws NoSuchUserException 
            {

        try {
            userService.resetMemberPassword(userId, oldpassword, password,
                    confirmPassword);
            model.put(SUCCESS, "alert.passwordchanged");
        } catch (PasswordException e) {
            model.put(ERROR, e.getErrorCode());
            LOGGER.info(e);
        }
        return REDIRECT + url + "/EditProfile";
            }

    @RequestMapping(value = { "/{url1}/{url2}/ResetG7EmpPassword" }, method = RequestMethod.POST)
    public String resetG7EmpPassword(@PathVariable(value = "url1") String url1,
            @PathVariable(value = "url2") String url2,
            @RequestParam("userId") Long userId,
            @RequestParam("password") String password,
            @RequestParam("confirmPassword") String cPassword,
            Map<String, Object> model) throws NoSuchUserException
            {
        userService.changePassword(userId, password, cPassword);
        model.put("id", userId);
        model.put(SUCCESS, "alert.passwordchanged");
        return REDIRECT + url1 + "/" + url2 + "/EditUser";
            }

    @RequestMapping("/{url1}/{url2}/EditUser")
    public String editUser(
            @RequestParam(value = "id") Long id,
            Map<String, Object> model) throws DuplicateEmailException
            {

        User user = userService.findById(id);
        model.put("user", user);

        return "admin/mngt/usermngt/edituser";

            }

    @RequestMapping("/{url}/EditProfile")
    public String editUserProfile(
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            Map<String, Object> model) throws NoSuchUserException {
        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        }
        if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }
        User user = getLogedInUser();
        if (user.getRole().getId() == Role.ATMA_ADMIN.getId()) {
            model.put("user", user);
            return "admin/mngt/profilemngt/edituniversityadminprofile";
        }
        List<CountryMaster> countryList = countryService.findByActive(true);
        List<StateMaster> stateList = stateService.findByActive(true);
        List<CityMaster> cityList = cityService.findByActive(true);
        model.put(COUNTRY_LIST, countryList);
        model.put(STATE_LIST, stateList);
        model.put("cityList", cityList);
        model.put("user", user);
        return "admin/mngt/profilemngt/editAdminProfile";
    }

    @RequestMapping(value = "/{url1}/UpdateUniversityAdminProfile")
    public String updateUniversityAdminProfile(
            @RequestParam("firstName") String firstName,
            @RequestParam("lastName") String lastName,
            @RequestParam("mobile") String mobile, 
            Map<String, Object> model) {
        User user1 = getLogedInUser();
        userService.update(user1);
        user1.setFirstName(firstName);
        user1.setLastName(lastName);
        user1.setMobile(mobile);
        model.put("user", user1);
        model.put(SUCCESS, "alert.profileupdatedsuccessfully");
        return "admin/mngt/profilemngt/edituniversityadminprofile";
    }
}
