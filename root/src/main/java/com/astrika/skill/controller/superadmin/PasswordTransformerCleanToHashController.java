package com.astrika.skill.controller.superadmin;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.astrika.common.service.UserService;

/**
 * Created by gaurav on 28/03/16.
 */
@Controller
public class PasswordTransformerCleanToHashController {

    private static final Logger LOGGER = Logger.getLogger(PasswordTransformerCleanToHashController.class);

    @Autowired private UserService userService;

    @PreAuthorize("hasRole('SUPER_ADMIN')")
    @RequestMapping(value = "/transform",method = RequestMethod.GET)
    public String newMethod(){
        return "admin/mngt/atmaadminmngt/transform";
    }

    @PreAuthorize("hasRole('SUPER_ADMIN')")
    @RequestMapping(value = "/transform",method = RequestMethod.POST)
    public String process(RedirectAttributes redirectAttributes){
        try{
            userService.transformPasswordFromClearToHash();
            redirectAttributes.addFlashAttribute("success","transform.password.success");
        }catch (Exception e){
            LOGGER.error("Problem in transforming passwords",e);
            redirectAttributes.addFlashAttribute("error","transform.password.error");
        }
        return "redirect:/transform";
    }
}
