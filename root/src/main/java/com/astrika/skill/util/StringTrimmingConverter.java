package com.astrika.skill.util;

import org.springframework.core.convert.converter.Converter;

public class StringTrimmingConverter implements Converter<String, String> {

    public String convert(String source) {
    	String src = source.replaceAll("\"", "'");
       return src.trim();
    }

}
