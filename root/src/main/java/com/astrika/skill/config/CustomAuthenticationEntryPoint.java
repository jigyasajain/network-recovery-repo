package com.astrika.skill.config;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Component;

import com.astrika.common.exception.NoSuchUserException;
import com.astrika.common.model.Role;
import com.astrika.common.model.User;
import com.astrika.common.model.UserStatus;
import com.astrika.common.service.UserService;
import com.astrika.kernel.exception.CustomException;

@Component(value="customAuthenticationEntryPoint")
public class CustomAuthenticationEntryPoint implements AuthenticationEntryPoint{

	@Autowired
	private UserService userService;
	
	@Autowired
	@Qualifier("authManager")
	protected AuthenticationManager authenticationManager;
	
	private static final String ERRORCODE="?errorCode=";
	
	private static final Logger LOGGER = Logger.getLogger(CustomAuthenticationEntryPoint.class);
	
	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {
		
		String loginId=request.getParameter("j_username");
		String successUrl="/Index";
		String url = request.getParameter("url");
		String password = request.getParameter("j_password");
		boolean responseCommited = false;
		HttpSession session = request.getSession();
		session.setAttribute("pass", password);
		
		try {
			User user = userService.findByLoginId(loginId);
			user.setPassword(password);
			
			if (user != null && user.getStatus().getId() == UserStatus.INACTIVE.getId()) {
				if (user.getRole().getId() == Role.MODULE_USER.getId()){
					response.sendRedirect(url+ERRORCODE+CustomException.USER_DEACTIVATED.getCode());
					responseCommited = true;
				}
				if (user.getRole().getId() == Role.ATMA_ADMIN.getId()){
					response.sendRedirect(url+ERRORCODE+CustomException.ATMA_ADMIN_DEACTIVATED.getCode());
					responseCommited = true;
				}
				if (user.getRole().getId() == Role.COMPANY_ADMIN.getId()){
					response.sendRedirect(url+ERRORCODE+CustomException.COMPANY_ADMIN_DEACTIVATED.getCode());
					responseCommited = true;
				}
				if (user.getRole().getId() == Role.RESTAURANT_ADMIN.getId()){
					response.sendRedirect(url+ERRORCODE+CustomException.REST_ADMIN_DEACTIVATED.getCode());
					responseCommited = true;
				}
				if (user.getRole().getId() == Role.CORPORATE_ADMIN.getId()){
					response.sendRedirect(url+ERRORCODE+CustomException.CORPORATE_ADMIN_DEACTIVATED.getCode());
					responseCommited = true;
				}
			}
			else{
				authenticateUserAndSetSession(user, request);
			}
		} 
		catch (BadCredentialsException e) {
			response.sendRedirect(url+ERRORCODE+CustomException.AUTHENTICATION_FAILURE.getCode());
			responseCommited = true;
			LOGGER.info(e);
		}
		catch (NoSuchUserException e) {
			response.sendRedirect(url+ERRORCODE+CustomException.AUTHENTICATION_FAILURE.getCode());
			responseCommited = true;
			LOGGER.info(e);
		}catch (IllegalArgumentException e) {
			response.sendRedirect(url+ERRORCODE+CustomException.AUTHENTICATION_FAILURE.getCode());
			responseCommited = true;
			LOGGER.info(e);
		}
		
		if(!responseCommited){
			request.getRequestDispatcher(successUrl).forward(request, response);
		}
			
	}

	 private void authenticateUserAndSetSession(User user,
			 HttpServletRequest request)
			 {
			 UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
			 user.getUsername(), user.getPassword());

			 request.getSession();

			 token.setDetails(new WebAuthenticationDetails(request));
			 Authentication authenticatedUser = authenticationManager.authenticate(token);
			 ((User)authenticatedUser.getPrincipal()).setMacId(user.getMacId());
			 SecurityContextHolder.getContext().setAuthentication(authenticatedUser);
	}
}
