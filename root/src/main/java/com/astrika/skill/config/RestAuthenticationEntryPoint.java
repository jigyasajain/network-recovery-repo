package com.astrika.skill.config;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Component;

import com.astrika.common.exception.NoSuchUserException;
import com.astrika.common.model.User;
import com.astrika.common.service.UserService;

@Component(value="restAuthenticationEntryPoint")
public class RestAuthenticationEntryPoint implements AuthenticationEntryPoint{

	@Autowired
	private UserService userService;
	
	private static final Logger LOGGER = Logger.getLogger(RestAuthenticationEntryPoint.class);
	
	@Autowired
	@Qualifier("authManager")
	protected AuthenticationManager authenticationManager;
	
	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {
		String token="";
		String url="";
		String macId = null;
		boolean isAddMember = false;
		if (ServletFileUpload.isMultipartContent(request)) {
			
			List<FileItem> items = null;
			try {
				items = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
			} catch (FileUploadException e) {
				LOGGER.info(e);
			}
			for (FileItem item : items) {

				if (item.isFormField()) {
					token=getToken(item);
					url=getUrl(item);
					macId=getMacId(item);
					isAddMember=getIsAddMember(item);
				}
			}
			request.setAttribute("parts", items);
		}
		else{
		 token= request.getParameter("token");
		 url= request.getParameter("url");
		 macId= request.getParameter("macId");
		}
		try {
			User user = userService.findByToken(token);
			if(user != null){
				user.setMacId(macId);
				authenticateUserAndSetSession(user, request);
			}else if(user == null && isAddMember){
				/*sonar*/
			}else{
				response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
			}
		} catch (NoSuchUserException e) {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
			LOGGER.info(e);
		}
		request.getRequestDispatcher(url).forward(request, response);
	}

	 private static boolean getIsAddMember(FileItem item) {
	     Boolean isAddMember=false;
	     if("isAddMember".equalsIgnoreCase(item.getFieldName())){
             isAddMember = Boolean.valueOf(item.getString());
         }
	     return isAddMember;
    }

    private static String getMacId(FileItem item) {
	     String macId="";
	     if("macId".equalsIgnoreCase(item.getFieldName()))
         {
             macId=item.getString();
         }
        return macId;
    }

    private static String getUrl(FileItem item) {
	     String url="";
	     if("url".equalsIgnoreCase(item.getFieldName()))
         {
	         url=item.getString();
         }
        return url;
    }

    private static String getToken(FileItem item) {
        
	     String token = "";
	     if("token".equalsIgnoreCase(item.getFieldName()))
         {
             token=item.getString();
         }
        return token;
    }

    private void authenticateUserAndSetSession(User user,
			 HttpServletRequest request)
			 {
			 UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
			 user.getUsername(), user.getPassword());

			 // generate session if one doesn't exist
			 request.getSession();

			 token.setDetails(new WebAuthenticationDetails(request));
			 Authentication authenticatedUser = authenticationManager.authenticate(token);
			 ((User)authenticatedUser.getPrincipal()).setMacId(user.getMacId());
			 SecurityContextHolder.getContext().setAuthentication(authenticatedUser);
	}
}
