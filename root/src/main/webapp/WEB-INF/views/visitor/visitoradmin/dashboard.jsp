<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->




<%@page import="java.util.Date"%>
<%@include file="../inc/config.jsp"%>
<%@include file="../inc/template_start.jsp"%>
<%@include file="../inc/page_head.jsp"%>
<style>
.feedback_profile {
	height: 30px;
	width: 30px !important;
}

.ratinglable {
	float: left;
}

.row {
	margin-bottom: 5px;
}

.col-md-2 {
	width: 100% !important;
}

.ratingwid {
	width: 269px;
}

.rating_value {
	width: 100% !important;
}
</style>
<style type="text/css">
        .db-icon {
            padding: 10px 5px;
        }
        .db-icon img {
            border: thin solid #ccc;
        }
        .db-icon a:hover{
            opacity: .5;
        }
 </style>
            <!-- Page content -->
            <div id="page-content">
                <!-- Statistics Widgets Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>Module User Dashboard</h1>
                    </div>
                </div>
                <!-- END Statistics Widgets Header -->
					<div class="row">
                	<c:forEach var="currentUser" items="${modulemaster}"
						varStatus="count">
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <!-- Widget -->
                        <div class="widget">
                            <div class="widget-extra">
                                <div class="text-center db-icon">
                                    <a href="<%=contexturl %>ListofCategories?id=${currentUser.moduleId}">
                                        <img src="<%=contexturl %>${currentUser.profileImage.imagePath}" width="120" height="120" alt="Module" >
                                    </a>
                                </div>
                            </div>
                            <div class="widget-extra themed-background-spring text-center">
                                <h4 class="widget-content-light"><strong>${currentUser.moduleName}</strong></h4>
                            </div>
                             <p>${currentUser.moduleDescription}</p>
                        </div>
                        <!-- END Widget -->
                    </div>
				</c:forEach>
                </div>
			</div>
            <!-- END Page Content -->

            
        </div>
        <!-- END Main Container -->
    </div>
<!-- END Page Content -->
<%@include file="../inc/page_footer.jsp"%>

<!-- Remember to include excanvas for IE8 chart support -->
<!--[if IE 8]><script src="<%=contexturl%>resources/js/helpers/excanvas.min.js"></script><![endif]-->

<%@include file="../inc/template_scripts.jsp"%>

<!-- Google Maps API + Gmaps Plugin, must be loaded in the page you would like to use maps (Remove 'http:' if you have SSL) -->
<!-- <script src="http://maps.google.com/maps/api/js?sensor=true"></script> -->
<%-- <script src="<%=contexturl%>resources/js/helpers/gmaps.min.js"></script> --%>

<!-- <!-- Load and execute javascript code used only in this page --> 
<%-- <script src="<%=contexturl%>resources/js/pages/index.js"></script> --%>
<!-- <script>$(function(){ Index.init(); });</script> -->



<%@include file="../inc/template_end.jsp"%>