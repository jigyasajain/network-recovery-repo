<%@page import="com.astrika.common.model.Role"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@page import="java.util.HashMap"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@page import="org.springframework.web.servlet.support.RequestContextUtils"%>
<%@page import="com.astrika.common.util.PropsValues"%>
<%@page import="org.springframework.security.core.context.SecurityContextHolder"%>
<%@page import="com.astrika.common.model.User"%>


<%

/**
 * config.jsp
 *
 * Author: astrikainfotech
 *
 * Configuration file. It contains variables used in the template as well as the primary navigation array from which the navigation is created
 *
 */

/* Template variables */

HashMap<String,String> template=new HashMap<String,String>();

ApplicationContext ac = RequestContextUtils.getWebApplicationContext(request);
PropsValues propvalue = (PropsValues) ac.getBean("propsValues");
String contexturl=propvalue.CONTEXT_URL;

template.put("name", "Atma");
template.put("version", "1.1");
template.put("author", "astrikainfotech");
template.put("robots", "noindex, nofollow");
template.put("title", "Atma- Strengthning organisations to impact Society");
template.put("description", "Atma-NGO project is developed by  Astrika Infotech Private Limited.");
    // 'navbar-default'         for a light header
    // 'navbar-inverse'         for a dark header
template.put("header_navbar", "navbar-default");
    // ''                       empty for a static header
    // 'navbar-fixed-top'       for a top fixed header / fixed sidebars
    // 'navbar-fixed-bottom'    for a bottom fixed header / fixed sidebars
template.put("header", "");
    // ''                                               for a full main and alternative sidebar hidden by default (> 991px)
    // 'sidebar-visible-lg'                             for a full main sidebar visible by default (> 991px)
    // 'sidebar-partial'                                for a partial main sidebar which opens on mouse hover, hidden by default (> 991px)
    // 'sidebar-partial sidebar-visible-lg'             for a partial main sidebar which opens on mouse hover, visible by default (> 991px)
    // 'sidebar-alt-visible-lg'                         for a full alternative sidebar visible by default (> 991px)
    // 'sidebar-alt-partial'                            for a partial alternative sidebar which opens on mouse hover, hidden by default (> 991px)
    // 'sidebar-alt-partial sidebar-alt-visible-lg'     for a partial alternative sidebar which opens on mouse hover, visible by default (> 991px)
    // 'sidebar-partial sidebar-alt-partial'            for both sidebars partial which open on mouse hover, hidden by default (> 991px)
    // 'sidebar-no-animations'                          add this as extra for disabling sidebar animations on large screens (> 991px) - Better performance with heavy pages!
template.put("sidebar", "sidebar-partial sidebar-visible-lg sidebar-no-animations");
    // ''                       empty for a static footer
    // 'footer-fixed'           for a fixed footer
template.put("footer", "footer-fixed");
    // ''                       empty for default style
    // 'style-alt'              for an alternative main style (affects main page background as well as blocks style)
template.put("main_style", "");
    // 'night', 'amethyst', 'modern', 'autumn', 'flatie', 'spring', 'fancy', 'fire' or '' leave empty for the Default Blue theme
template.put("theme","");
    // ''                       for default content in header
    // 'horizontal-menu'        for a horizontal menu in header
    // This option is just used for feature demostration and you can remove it if you like. You can keep or alter header's content in page_head.jsp
template.put("header_content", "");
//template.put("active_page,basename($_SERVER['PHP_SELF']));
    		

User user;
Long moduleId;
try {
user = (User) SecurityContextHolder.getContext()
.getAuthentication().getPrincipal();
moduleId = user.getModuleId();
} catch (Exception e) {

user = null;
moduleId = null;
}    	
%>
<!-- This property is used to check whether logged in user is a corporate or an individual in membershipform.jsp  -->
<c:set var="visitor" value="<%= user%>"></c:set>
<c:set var="MEMBER" value="<%=Role.STUDENT.getId() %>"></c:set>
<c:set var="CORPORATE" value="<%=Role.CORPORATE_ADMIN.getId() %>"></c:set>
<%-- <fmt:setTimeZone value="${visitor.city.cityTimeZone}"/> --%>