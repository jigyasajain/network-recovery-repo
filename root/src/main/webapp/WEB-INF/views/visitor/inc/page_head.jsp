<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>

<%@page import="com.astrika.common.model.Role"%>
<%@page import="com.astrika.common.model.User"%>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Atma Network</title>
    <!-- Style -->
    <!-- Bootstrap -->
    <link rel="shortcut icon" href="<%=contexturl %>resources/img/favicon.ico">
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<%--     <link href="<%=contexturl %>resources/css/bootstrap.css" rel="stylesheet" media="screen"> --%>

    <link href="<%=contexturl %>resources/css/fontface.css" rel="stylesheet" media="screen">
    
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" media="screen"> 
<%--     <link href="<%=contexturl %>resources/css/font-awesome.css" rel="stylesheet" media="screen"> --%>

<link href="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.5/jquery.bxslider.min.css" rel="stylesheet" media="screen">
<%--     <link href="<%=contexturl %>resources/css/jquery.bxslider.css" rel="stylesheet" media="screen"> --%>

<link href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.1.4/css/ion.rangeSlider.css" rel="stylesheet" media="screen">
<%--     <link href="<%=contexturl %>resources/css/ion.rangeSlider.css" rel="stylesheet" media="screen"> --%>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.1.4/css/ion.rangeSlider.skinNice.css" rel="stylesheet" media="screen">
<%--     <link href="<%=contexturl %>resources/css/ion.rangeSlider.skinNice.css" rel="stylesheet" media="screen"> --%>
    <!--[if IE 9]>
  <script src="<%=contexturl %>resources/js/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="<%=contexturl %>resources/js/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
    <!--[if IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    <link href="<%=contexturl %>resources/css/style.css" rel="stylesheet" media="screen">
</head>

<body>
    <div class="header-bg">
        <nav class="navbar ">
            <div class="container">
                <div class="navbar-header">
                
                	<security:authorize ifAnyGranted="<%=Role.COMPANY_ADMIN.name()%>">
							<a href="<%=contexturl%>CompanyAdmin"><img src="<%=contexturl %>resources/img/atma.png" /></a>
					</security:authorize>
					<security:authorize ifAnyGranted="<%=Role.MODULE_USER.name()%>">
							<a href="<%=contexturl%>ModuleUser"><img src="<%=contexturl %>resources/img/atma.png" /></a>
					</security:authorize>
                
                
                
<%--                     <a href=""><img src="<%=contexturl %>resources/img/atma.png" /></a> --%>
                </div>
                <div id="navbar" class="mynav">
                    <ul class="nav navbar-nav pull-right">
<!--                         <li class="active"><a href="#"><b>Profile</b></a> -->
<!--                         </li> -->
                        <li class="white"><a href="#logout_popup" data-toggle="modal"><b>Log Out</b></a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        
        
<div id="logout_popup" class="modal fade" tabindex="-1"
	role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<form action="" method="post" class="form-horizontal form-bordered"
					id="fg">
					<div>
						<div class="col-xs-6">You will be logged out of the application</div>
						<div class="col-xs-6 text-right logout-action">
							<button type="button" class="btn btn-sm btn-default"
								data-dismiss="modal">No</button>
							<a class="btn btn-sm btn-green save" href="<c:url value="/j_spring_security_logout"/>"></i>
							Yes</a>
						</div>
						<span class="clearfix"> </span>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
        