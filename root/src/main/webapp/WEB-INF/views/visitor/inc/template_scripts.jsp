<%
/**
 * template_scripts.jsp
 *
 * Author: pixelcave
 *
 * All vital JS scripts are included here
 *
 */
%>

<!-- Include Jquery library from Google's CDN but if something goes wrong get Jquery from local file (Remove 'http:' if you have SSL) -->
<script>!window.jQuery && document.write(unescape('%3Cscript src="<%=contexturl %>resources/js/vendor/jquery-1.11.0.min.js"%3E%3C/script%3E'));</script>

<!-- Bootstrap.js, Jquery plugins and Custom JS code -->
<%-- <script src="<%=contexturl %>resources/js/vendor/jquery-1.11.0.min.js"></script> --%>
<%-- <script src="<%=contexturl %>resources/js/vendor/bootstrap.min.js"></script> --%>
<%-- <script src="<%=contexturl %>resources/js/plugins.js"></script> --%>
<script src="${pageContext.request.contextPath}/webjars/counter-webapp-frontend/0.0.1/vendor_bundle.js"></script>

<%-- <script src="<%=contexturl %>resources/js/app.js"></script> --%>
<script src="${pageContext.request.contextPath}/webjars/counter-webapp-frontend/0.0.1/app_bundle.js"></script>

<script src="<%=contexturl %>resources/js/vendor/jquery.raty.min.js"></script>	
<script src="<%=contexturl %>resources/js/jquery.autoellipsis-1.0.10.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
	
	// Menu Selection
	var pathname = window.location.pathname;
	var path = pathname.substring(pathname.indexOf(<%=contexturl%>));
	try{
		$("a").removeClass("active");$("li").removeClass("active");
		if(path.indexOf("ManageCompany") >= 0){
			$("#manageCompany").addClass("active");
			if(path.indexOf("CompanyList") > 0){
				$("#companyList").addClass("active");
			}
			
		}
		else if(path.indexOf("ManageBrand") >= 0){
			$("#manageBrand").addClass("active");
			if(path.indexOf("BrandList") > 0){
				$("#brandList").addClass("active");
			}
			<security:authorize ifAnyGranted="<%=Role.COMPANY_ADMIN.name()%>">
			$("#manageBrand1").addClass("active");
			if(path.indexOf("ViewBrand") > 0){
				$("#viewBrand").addClass("active");
			}
			</security:authorize>
		}
		else if(path.indexOf("ManageRestaurant") >= 0){
			$("#manageRestaurant").addClass("active");
			if(path.indexOf("RestaurantList") > 0){
				$("#restaurantList").addClass("active");
			}
			<security:authorize ifAnyGranted="<%=Role.RESTAURANT_ADMIN.name()%>">
			$("a").removeClass("active");$("li").removeClass("active");
			$("#manageRestaurant1").addClass("active");
			if(path.indexOf("ViewRestaurant") > 0){
				$("#viewRestaurant").addClass("active");
			}
			else if(path.indexOf("MenuList") > 0){
				$("#menu").addClass("active");
			}
			else if(path.indexOf("OfferList") > 0){
				$("#offerList").addClass("active");
			}
			else if(path.indexOf("PromoDescriptionList") > 0){
				$("#promoList").addClass("active");
			}
			else if(path.indexOf("MembershipModalList") > 0){
				$("#modalList").addClass("active");
			}
			else if(path.indexOf("MealMenuList") > 0){
				$("#mealList").addClass("active");
			}
			else if(path.indexOf("RestaurantRatingAndFeedback") > 0){
				$("#ratingAndFeedback").addClass("active");
			}
			else if(path.indexOf("FollowerList") > 0){
				$("#followerList").addClass("active");
			}
			else if(path.indexOf("Checkins") > 0){
				$("#checkinsList").addClass("active");
			}
		
		</security:authorize>
		}
		else if(path.indexOf("ManageMembers") >= 0){
			$("#manageMember").addClass("active");
			if(path.indexOf("1") > 0){
				$("#freeMember").addClass("active");
			}
			else if(path.indexOf("2") > 0){
				$("#paidMember").addClass("active");
			}
			else if(path.indexOf("3") > 0){
				$("#corporateMember").addClass("active");
			}
		}
		else if(path.indexOf("ManageVoucher") >= 0){
			$("#manageVoucher").addClass("active");
			if(path.indexOf("IndividualVoucher") > 0){
				$("#individualVoucher").addClass("active");
			}
		}
		else if(path.indexOf("ManageEviteTemplate") >= 0){
			$("#manageEviteTemplate").addClass("active");
			if(path.indexOf("EviteTemplateList") > 0){
				$("#eviteTemplateList").addClass("active");
			}
		}
		else if(path.indexOf("ManageUsers") >= 0){
			$("#manageUser").addClass("active");
			if(path.indexOf("UserList") > 0){
				$("#userList").addClass("active");
			}
		}
		else if(path.indexOf("ManageAds") >= 0){
			$("#manageAds").addClass("active");
			if(path.indexOf("AdvertisementList") > 0){
				$("#advertisementList").addClass("active");
			}
		}
		else if(path.indexOf("ManageCorporate") >= 0){
			
			
			
			if(path.indexOf("CorporateList") > 0){
				$("#manageCorporate").addClass("active");
				$("#corporateList").addClass("active");
			}
			if(path.indexOf("ViewCorporate") > 0){
				$("#manageCorporateAdmin").addClass("active");
				$("#viewcoporate").addClass("active");
			}
			if(path.indexOf("Membership") > 0){
				$("#manageCorporateAdmin").addClass("active");
				$("#membership").addClass("active");
			}
			if(path.indexOf("ShowAssignMembers") > 0){
				$("#manageCorporateAdmin").addClass("active");
				$("#assignMembers").addClass("active");
			}
			if(path.indexOf("ShowMemberList") > 0){
				$("#manageCorporateAdmin").addClass("active");
				$("#corporatemembers").addClass("active");
			}
			
		}
		else if(path.indexOf("ManageLocation") >= 0){
			$("#manageLocation").addClass("active");
			if(path.indexOf("Area") > 0){
				$("#area").addClass("active");
			}
			if(path.indexOf("City") > 0){
				$("#city").addClass("active");
			}
			if(path.indexOf("Country") > 0){
				$("#country").addClass("active");
			}
		}
		else if(path.indexOf("ManageTemplate") >= 0){
			$("#manageTemplate").addClass("active");
			$("#emailTemplate").addClass("active");
		}
		else{
			$("#dashboard").addClass("active");
		}
	}
	catch(e){
		
	}
	
	
	
	
	
	
	
	 jQuery.validator.addMethod("filesize", function(value, element, param) {
	    // param = size (en bytes) 
	    // element = element to validate (<input>)
	    // value = value of the element (file name)
	    return this.optional(element) || (element.files[0].size <= param) 
	}, "Size of the image is too large.");
	
	
 
	 jQuery.validator.addMethod("phone", function (phone_number, element) {
	        phone_number = phone_number.replace(/\s+/g, "");
	        return this.optional(element) || phone_number.length >= 8 && phone_number.length < 25 && phone_number.match(/^[0-9\+\-]*$/);
	    }, "Please specify a valid phone number");
	    
	    jQuery.validator.addMethod("mobile", function (mobile_number, element) {
	    	mobile_number = mobile_number.replace(/\s+/g, "");
	        return this.optional(element) || mobile_number.length >= 10 && mobile_number.length < 25 && mobile_number.match(/^[0-9\+\-]*$/);
	    }, "Please specify a valid mobile number");
	    
	    jQuery.validator.addMethod("pincode", function (pincode, element) {
	    	pincode = pincode.replace(/\s+/g, "");
	        return this.optional(element) || pincode.length >=4 && pincode.length <= 10;
	    }, "Please specify a valid pincode");
	    
	    
	    
	    jQuery.validator.addMethod("greaterThan", 
	    		function(value, element, params) {
                          return  (Number(value) > Number($(params).val())); 
	    		},'Must be greater than {0}.');
	    
	    jQuery.validator.addMethod("discount", function (number, element) {
	        return  number <100 ;
	    }, "Please specify a valid discount ");
	    
	    jQuery.validator.addMethod("urlTrim", function (urlTrim, element) {
	    	var urlregex = new RegExp("^(http:\/\/www.|https:\/\/www.|ftp:\/\/www.|www.){1}([0-9A-Za-z]+\.)");
	   
	     return urlregex.test(urlTrim.trim()) || urlTrim.length == 0;
	    }, "Please Invalid url");
	    
	    
	    jQuery.validator.addMethod("maxno", function (number, element) {
	        number =number.replace(/\s+/g, "");
	        return this.optional(element) || number <13 ;
	    }, "Please specify a valid month ");
	    
	    jQuery.validator.addMethod("minno", function (number, element) {
	        number =number.replace(/\s+/g, "");
	        return this.optional(element) || number >13 ;
	    }, "Please specify a valid month ");
	    
	    jQuery.validator.addMethod("domain",function(domains,element)
	    		{
	    	 var regex = /^\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	    	 var flag=false;
	    		  var split = domains.split(',');
	    		  
	    		  for(var i = 0; i < split.length; ++i) {
	    			  flag= split[i] != '' && regex.test(split[i]);
	    		  }
	    		  return flag;
		}, "Email id already assigned");
	    
	    jQuery.validator.addMethod("hash", function (nameTrim, element) {
	     return (nameTrim.indexOf('#') == -1)
	    }, "Please Invalid Login Id");
	    
	    
	    
	$.validator.setDefaults({ ignore: ":hidden:not(select)" })
	
	
// 	$("#area").click(function(){
// 		$("a").removeClass("active");$("li").removeClass("active");
// 		$("#area").addClass("active");
// 		$.ajax({
// 			type: "GET",
// 			async: false,
<%-- 			url: "<%=contexturl %>/Admin/Area",				 --%>
// 			success: function(data, textStatus, jqXHR){
// 				$('#page-content').html(data);
// 			},
// 			dataType: 'html'
// 		});
// 	});
	
	
	
	
	
	
	
	$("#country").click(function(){
		$("a").removeClass("active");$("li").removeClass("active");
		$("#country").addClass("active");
		$.ajax({
			type: "GET",
			async: false,
			url: "<%=contexturl %>/Admin/Country",				
			success: function(data, textStatus, jqXHR){
				$('#page-content').html(data);
			},
			dataType: 'html'
		});
	});
	
	$("#currency").click(function(){
		$("a").removeClass("active");$("li").removeClass("active");
		$("#currency").addClass("active");
		$.ajax({
			type: "GET",
			async: false,
			url: "<%=contexturl %>/Admin/Currency",				
			success: function(data, textStatus, jqXHR){
				$('#page-content').html(data);
			},
			dataType: 'html'
		});
	});
	
	
	$("#brandWiseRate").click(function(){
		$("a").removeClass("active");$("li").removeClass("active");
		$("#brandWiseRate").addClass("active");
		$.ajax({
			type: "GET",
			async: false,
			url: "<%=contexturl %>/Admin/BrandWiseRate",				
			success: function(data, textStatus, jqXHR){
				$('#page-content').html(data);
			},
			dataType: 'html'
		});
	});
	
	$("#cityWiseRate").click(function(){
		$("a").removeClass("active");$("li").removeClass("active");
		$("#cityWiseRate").addClass("active");
		$.ajax({
			type: "GET",
			async: false,
			url: "<%=contexturl %>/Admin/CityWiseRate",				
			success: function(data, textStatus, jqXHR){
				$('#page-content').html(data);
			},
			dataType: 'html'
		});
	});
	
	
	$("#countryWiseRate").click(function(){
		$("a").removeClass("active");$("li").removeClass("active");
		$("#countryWiseRate").addClass("active");
		$.ajax({
			type: "GET",
			async: false,
			url: "<%=contexturl %>/Admin/CountryWiseRate",				
			success: function(data, textStatus, jqXHR){
				$('#page-content').html(data);
			},
			dataType: 'html'
		});
	});
	
	$("#countryDiscountSlab").click(function(){
		$("a").removeClass("active");$("li").removeClass("active");
		$("#countryDiscountSlab").addClass("active");
		$.ajax({
			type: "GET",
			async: false,
			url: "<%=contexturl %>/Admin/CountryDiscountSlab",				
			success: function(data, textStatus, jqXHR){
				$('#page-content').html(data);
			},
			dataType: 'html'
		});
	});
	
	$("#cityDiscountSlab").click(function(){
		$("a").removeClass("active");$("li").removeClass("active");
		$("#cityDiscountSlab").addClass("active");
		$.ajax({
			type: "GET",
			async: false,
			url: "<%=contexturl %>/Admin/CityDiscountSlab",				
			success: function(data, textStatus, jqXHR){
				$('#page-content').html(data);
			},
			dataType: 'html'
		});
	});
	
	$("#tenureDiscountSlab").click(function(){
		$("a").removeClass("active");$("li").removeClass("active");
		$("#tenureDiscountSlab").addClass("active");
		$.ajax({
			type: "GET",
			async: false,
			url: "<%=contexturl %>/Admin/TenureDiscountSlab",				
			success: function(data, textStatus, jqXHR){
				$('#page-content').html(data);
			},
			dataType: 'html'
		});
	});
	
	$("#cuisine").click(function(){
		$("a").removeClass("active");$("li").removeClass("active");
		$("#cuisine").addClass("active");
		$.ajax({
			type: "GET",
			async: false,
			url: "<%=contexturl %>/Admin/Cuisine",				
			success: function(data, textStatus, jqXHR){
				$('#page-content').html(data);
			},
			dataType: 'html'
		});
	});
	
	$("#currencyExchangeRate").click(function(){
		$("a").removeClass("active");$("li").removeClass("active");
		$("#currencyExchangeRate").addClass("active");
		$.ajax({
			type: "GET",
			async: false,
			url: "<%=contexturl %>/Admin/CurrencyExchangeRate",				
			success: function(data, textStatus, jqXHR){
				$('#page-content').html(data);
			},
			dataType: 'html'
		});
	});
	
	$("#language").click(function(){
		$("a").removeClass("active");$("li").removeClass("active");
		$("#language").addClass("active");
		$.ajax({
			type: "GET",
			async: false,
			url: "<%=contexturl %>/Admin/Language",				
			success: function(data, textStatus, jqXHR){
				$('#page-content').html(data);
			},
			dataType: 'html'
		});
	});
	

	$("#corporateVoucher").click(function(){
		$("a").removeClass("active");$("li").removeClass("active");
		$("#corporateVoucher").addClass("active");
		$.ajax({
			type: "GET",
			async: false,
			url: "<%=contexturl %>/Admin/CorporateVoucher",				
			success: function(data, textStatus, jqXHR){
				$('#page-content').html(data);
			},
			dataType: 'html'
		});
	});

	$("#user-settings-reset-password").click(function(){
		
		$("#user-settings-password_update").show();
		$("#user-settings-oldpassword").rules( "add", {
			  required:!0,
			  messages: {
			    required: "Please enter old password"
			  }
			});
		
		$("#user-settings-password").rules( "add", {
			  required:!0,
			  minlength : 6,
			  messages: {
				  required : "Please enter a new password",
				  minlength : "Password must be atleast 6 character"
			  }
			});
		
		
		$("#user-settings-repassword").rules( "add", {
			  required:true,
			  equalTo : "#user-settings-password",
			  messages: {
				  required : "Please enter a new password",
				  equalTo : "Please enter the same password as above"
			  }
			});
		
		

		
	});
	
	
	
	$("#advertisementLocation").click(function(){
		$("a").removeClass("active");$("li").removeClass("active");
		$("#advertisementLocation").addClass("active");
		$.ajax({
			type: "GET",
			async: false,
			url: "<%=contexturl %>/Admin/AdvertisementLocation",				
			success: function(data, textStatus, jqXHR){
				$('#page-content').html(data);
			},
			dataType: 'html'
		});
	});
	
// 	$("#advertisement").click(function(){
// 		$.ajax({
// 			type: "GET",
// 			async: false,
// 			url: "Advertisement",				
// 			success: function(data, textStatus, jqXHR){
// 				$('#page-content').html(data);
// 			},
// 			dataType: 'html'
// 		});
// 	});
	
});
</script>

<script type="text/javascript">
	$(document).ready(function() {
		
		$("#vitalInfo_submit").click(function(){
			$("#vitalInfo_submit").submit();
		});
		
		$("#vitalInfo_form").validate(
				{	errorClass:"help-block animation-slideDown",
					errorElement:"div",
					errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
					highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
					success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
					rules:{ 
						useremail:{
							required:!0,
							email:!0
						}					
					},
					messages:{
						useremail : {
							required : "Please enter an emailId",
							email : "Please enter a valid emailId"
						}
					},
					submitHandler: function() {
						var data = $("#vitalInfo_form").serialize();
						$.ajax({
							type: "POST",
							async: false,
							url: "<%=contexturl%>Admin/SaveAccountInfo",
							data:data,
							success: function(data, textStatus, jqXHR){
								var i = data.trim().indexOf("errorCode");
								if(i == 2){
									var obj = $.parseJSON(data.trim());
									var html = '<div class="alert alert-danger alert-dismissable">	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="fa fa-times-circle"></i> Error</h4> <span >'+obj.errorCode+'</span></div>';
									$("#vitalInfoErrorMsg").html(html).show();
									$("#vitalInfoErrorMsg").html(html).fadeIn();
									$("#vitalInfoErrorMsg").html(html).fadeOut(8000);
								}
								else{
									var obj = $.parseJSON(data.trim());
									var html = '<div class="alert alert-success alert-dismissable">	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="fa fa-times-circle"></i> Success</h4> <span >'+obj.success+'</span></div>';
									$("#vitalInfoSuccessMsg").html(html).show();
									$("#vitalInfoSuccessMsg").html(html).fadeIn();
									$("#vitalInfoSuccessMsg").html(html).fadeOut(8000);
								}
							},
							dataType: 'html'
						});
	                }
				});	
		
	});
	
</script>
<script type="text/javascript">
	function OpenFileDialog(){
		document.getElementById("imageChooser").click();
	}
	
	function changePhoto(){
		if($('#imageChooser').val() != ""){			
			var imageName = document.getElementById("imageChooser").value;
			var ext1 = imageName.substring(imageName.lastIndexOf("."));
			var ext = ext1.toLowerCase();
			if(ext == '.jpg' || ext == '.jpeg' || ext == '.png' || ext == 'gif' || ext == 'bmp' || ext == 'tiff')
				$('#changePhoto').submit();		
			else
				alert("Please select only image file");		
		}
	}
</script>

<script type="text/javascript">
	var unsaved = false;
	$(function(){

		$(".save").click(function(){ //trigers change in all input fields including text type
		     unsaved = false;
		 });
		
		 $(":input").change(function(){ //trigers change in all input fields including text type
		     unsaved = true;
		 });

		 function unloadPage(){ 
		     if(unsaved){
		         return "You have unsaved changes on this page. Do you want to leave this page and discard your changes or stay on this page?";
		     }
		 }

		 window.onbeforeunload = unloadPage;
	});
</script>
