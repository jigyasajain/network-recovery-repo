<!DOCTYPE html>
<html lang="en">

<%@include file="../inc/config.jsp"%>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Atma Network</title>
    <!-- Style -->
    <!-- Bootstrap -->
    <link rel="shortcut icon" href="<%=contexturl %>resources/img/favicon.ico">
<%--     <link href="<%=contexturl %>resources/css/bootstrap.css" rel="stylesheet" media="screen"> --%>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link href="<%=contexturl %>resources/css/fontface.css" rel="stylesheet" media="screen">
    
<%--     <link href="<%=contexturl %>resources/css/font-awesome.css" rel="stylesheet" media="screen"> --%>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" media="screen"> 
    
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.5/jquery.bxslider.min.css" rel="stylesheet" media="screen">
<%--     <link href="<%=contexturl %>resources/css/jquery.bxslider.css" rel="stylesheet" media="screen"> --%>
    <!--[if IE 9]>
  <script src="<%=contexturl %>resources/js/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="<%=contexturl %>resources/js/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
    <!--[if IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    <link href="<%=contexturl %>resources/css/style.css" rel="stylesheet" media="screen">    
</head>

<body>
    <div class="header-bg">
        <nav class="navbar">
            <div class="container">
                <div class="navbar-header">
                    <a href="<%=contexturl%>CompanyAdmin"><img src="<%=contexturl %>resources/img/atma.png" /></a>
                </div>
                <div id="navbar" class="mynav">
                    <ul class="nav navbar-nav pull-right">
                        <li class="white"><a href="<%=contexturl %>webinarVideos"><b>Webinar Videos</b></a>
                        </li>
                        <li class="active"><a href="<%=contexturl %>ManageModuleUser/ModuleUserList"><b>Admin Side</b></a>
                        </li>
                        <li class="white"><a href="#logout_popup" data-toggle="modal"/><b>Log Out</b></a>
                        </li>
                        <c:choose>
							<c:when test="${(surveyDate eq true)}">
								<li class="white"><a class=""
									href="<%=contexturl%>ManageSurvey/SurveyInstructions"><b><span
											class="glowing">SURVEY</span></b></a></li>
								<c:if test="${survey.surveySkipOption eq true}">

									<li class="white"><a class="btn-sm btn btn-default"
										data-toggle="modal" href="#survey_skip_popup"
										onclick="surveySkip(${survey.surveyManagerId})"><b>Skip
												Survey</b></a></li>
								</c:if>
							</c:when>
							<c:otherwise>
								<c:if test="${not empty surveyResultList[0].lifeStages}">
									<li class="white"><a 
									href="<%=contexturl%>YourResults/${company.companyId}"><b>Survey Results</b></a></li>
								</c:if>
							</c:otherwise>
						</c:choose>
                        
                        
<%--                         <c:if test="${(surveyDate eq true)}"> --%>
<%--                         	<li class="white"><a class="" href="<%=contexturl %>ManageSurvey/SurveyInstructions"><b><span class="glowing">SURVEY</span></b></a> --%>
<!--                        	 	</li> -->
<%-- 							<c:if test="${survey.surveySkipOption eq true}"> --%>
								                       
<!-- 								<li class="white"><a class="btn-sm btn btn-default" -->
<!-- 									data-toggle="modal" href="#survey_skip_popup" -->
<%-- 									onclick="surveySkip(${survey.surveyManagerId})"><b>Skip --%>
<!-- 											Survey</b></a></li> -->
<%-- 							</c:if> --%>
<%-- 						</c:if> --%>
                        
                    </ul>
                </div>
            </div>
        </nav>

 		<div class="blue-container">
            <div class="container">
                  <h3 class ="light">Company Admin Dashboard</h3>
                <div class="row blue">
                    <div class="col-md-10">
                        <div class="media">
                        	<c:choose>
                        		<c:when test = "${(user1.profileImage!=null) && (not empty user1.profileImage)}">
                        			<a class="pull-left" href="#"><img class="media-object pro-pic" height = 90px width=75px src="<%=contexturl%>${user1.profileImage.imagePath}" alt="<%=contexturl %>resources/img/User_Thumbs.png" title="Click here to change photo" onclick="OpenFileDialog();">
                            		</a>
                        		</c:when>
                        		<c:otherwise>
                        			<a class="pull-left" href="#"><img class="media-object pro-pic" src="<%=contexturl %>resources/img/User_Thumbs.png" alt="Organisation Name" title="Click here to change photo" onclick="OpenFileDialog();">
                            		</a>
                        		</c:otherwise>
                        	</c:choose>
							<form action="<%=contexturl%>ChangePhoto" id="changePhoto"
								method="post" enctype="multipart/form-data" class="save">
								<input type="file" id="imageChooser" style="display: none;"
									onchange="changePhoto();" name="imageChooser"
									accept="image/jpg,image/jpeg,image/png,image/gif,image/bmp,image/tiff"
									class="save" />
							</form>
							<div class="media-body text-st">
                                <p>${user1.fullName}</p>
                                <h4 class="media-heading">${company.companyName}</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 text-center">
                    	<div>
                            <button id="personalFormId" type="button" class="btn btn-default" onclick="location.href='<%=contexturl %>EditPersonalDetails/'+${user1.userId}">Edit Profile</button>
                        </div> <br>  
                        <span class="text-white">Your profile is ${user1.profileCompletion}% complete.</span>
                        <div class="progress">
                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="${user1.profileCompletion}" aria-valuemin="0" aria-valuemax="100" style="width: ${user1.profileCompletion}%">
                                <span class="sr-only">${user1.profileCompletion}% Complete (warning)</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div id="survey_skip_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="<%=contexturl %>SkipSurvey" method="post" class="form-horizontal form-bordered save"
						id="survey_skip_form">
						<div style="padding: 10px; height: 100px;">
							<label>Do you want to skip this survey?</label><br>
							<label>If you skip this survey, then the next survey that will be conducted after 6 months will be mandatory for you.</label>
							<div class="col-xs-12 text-right logout-action">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal">
									<spring:message code="label.no" />
								</button>
								<div id="survey_Skip" class="btn btn-sm btn-green save">
									<spring:message code="label.yes" />
								</div>
							</div>
							<span class="clearfix"> </span>
						</div>
						<input type="hidden" name="id" id="surveySkipId">

					</form>

				</div>
			</div>
		</div>
	</div>
	
<div id="logout_popup" class="modal fade" tabindex="-1"
	role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<form action="" method="post" class="form-horizontal form-bordered"
					id="fg">
					<div>
						<div class="col-xs-6">You will be logged out of the application</div>
						<div class="col-xs-6 text-right logout-action">
							<button type="button" class="btn btn-sm btn-default"
								data-dismiss="modal">No</button>
							<a class="btn btn-sm btn-green save" href="<c:url value="/j_spring_security_logout"/>"></i>
							Yes</a>
						</div>
						<span class="clearfix"> </span>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

    <div class="container">
		<div class="row">
			<div class="col-md-9">
				<c:if test="${!empty success}">
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert"
							aria-hidden="true">x</button>
						<h4>
							<i class="fa fa-check-circle"></i>
							<spring:message code="label.success" />
						</h4>
						<spring:message code="${success}" />
					</div>
				</c:if>
			</div>
		</div>
		<br>
		<div class="row">
            <div class="col-md-9">
                <div class="table-bordered">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Modules</th>
                            <th>Life Stage of Module User</th>
                            <th>User</th>
                            <th>Last Login</th>
                        </tr>
                    </thead>
                    <tbody>
                    	<c:forEach var = "moduleUserlist" items = "${moduleUserList}" varStatus = "counter">
								<c:choose>
									<c:when test="${not empty moduleUserlist.user}">
										<tr>
											<td><a href="<%=contexturl %>OrganisationalDevelopmentArea?id=${moduleUserlist.module.moduleId}"><strong>${moduleUserlist.module.moduleName}</strong></a></td>
											<td class="text-blue">${moduleUserlist.lifeStages.name}</td>
											<td class="text-blue">${moduleUserlist.user.fullName}</td>
											<c:choose>
												<c:when test = "${empty moduleUserlist.user.lastLoginDate}">
													<td></td>
												</c:when>
											<c:otherwise>
												<td><fmt:formatDate type="date" value="${moduleUserlist.user.lastLoginDate}"/></td>
											</c:otherwise>	
											</c:choose>
										</tr>
									</c:when>
									<c:otherwise>
										<tr>
											<td><a href="<%=contexturl %>OrganisationalDevelopmentArea?id=${moduleUserlist.module.moduleId}"><strong>${moduleUserlist.module.moduleName}</strong></a></td>
											<td class="text-blue">${moduleUserlist.lifeStages.name}</td>
											<td class="text-blue">${user1.fullName}</td>
											<td><fmt:formatDate type="date" value="${user1.lastLoginDate}" /></td>
										</tr>
									</c:otherwise>
								</c:choose>
							</c:forEach>
                    </tbody>
                </table>
                    </div>
            </div>


           <div class="col-md-3 col-sm-9 hidden-xs">
            	<img class="img-responsive" src="<%=contexturl %>resources/img/AtmaNetwork.jpg" />
            	<br></br>
            	<c:choose>
					<c:when test = "${empty atmaContactImage.contactImage}">
						 <div class="need-help"><img class="img-responsive" src="<%=contexturl %>resources/img/need help-3.jpg" /><br><br></div>
					</c:when>
					<c:otherwise>
						 <div class="need-help"><img class="img-responsive" src="<%=contexturl%>${atmaContactImage.contactImage.imagePath}" /><br><br></div>
					</c:otherwise>
				</c:choose>
           	 <br></br>
       	 </div>
        </div>
    </div>
    
     <div class="container">
        <div class="row">
        </div>
    </div>
<%-- <script src="<%=contexturl%>resources/js/vendor/jquery-1.11.0.min.js"></script> --%>
<%-- <script src="<%=contexturl%>resources/js/vendor/bootstrap.min.js"></script> --%>
<script src="${pageContext.request.contextPath}/webjars/counter-webapp-frontend/0.0.1/vendor_bundle.js"></script>

<script type="text/javascript">
	var selectedId = 0;
	
	$(document).ready(function() {
		
		
	     var blink = function() {
	           $('span.glowing').animate({
	               opacity: "0.5",
	           }, function(){
	               $(this).animate({
	                   opacity: "1"
	               }, blink);
	           });
	       }
	       blink();
		
		
			$("#personalFormId").click(function(){
				$("personalForm").submit();
			});
	});
	
	
	function OpenFileDialog(){
		document.getElementById("imageChooser").click();
	}
	
	function changePhoto(){
		if($('#imageChooser').val() != ""){			
			var imageName = document.getElementById("imageChooser").value;
			var ext1 = imageName.substring(imageName.lastIndexOf("."));
			var ext = ext1.toLowerCase();
			if(ext == '.jpg' || ext == '.jpeg' || ext == '.png' || ext == 'gif' || ext == 'bmp' || ext == 'tiff')
				$('#changePhoto').submit();		
			else
				alert("Please select only image file");		
		}
	}
	
	$("#survey_Skip").click(function(){
		$("#surveySkipId").val(selectedId);
		$("#survey_skip_form").submit();
	});
	
	function surveySkip(id){
		selectedId = id;
	}
	
</script>

<%@include file="../inc/page_footer.jsp"%>
    