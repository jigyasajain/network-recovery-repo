<!DOCTYPE html>
<html lang="en">

<%@include file="../inc/config.jsp"%>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Atma Network</title>
    <!-- Style -->
    <!-- Bootstrap -->
    <link rel="shortcut icon" href="<%=contexturl %>resources/img/favicon.ico">
    <link href="<%=contexturl %>resources/css/bootstrap.css" rel="stylesheet" media="screen">
    <link href="<%=contexturl %>resources/css/fontface.css" rel="stylesheet" media="screen">
    <link href="<%=contexturl %>resources/css/font-awesome.css" rel="stylesheet" media="screen">
    <link href="<%=contexturl %>resources/css/jquery.bxslider.css" rel="stylesheet" media="screen">
    <!--[if IE 9]>
  <script src="<%=contexturl %>resources/js/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="<%=contexturl %>resources/js/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
    <!--[if IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    <link href="<%=contexturl %>resources/css/style.css" rel="stylesheet" media="screen">    
</head>

<body>
    <div class="header-bg" style="min-height:50px">
        <nav class="navbar">
            <div class="container">
                <div class="navbar-header">
                    <a href="<%=contexturl%>CompanyAdmin"><img src="<%=contexturl %>resources/img/atma.png" /></a>
                </div>
                <div id="navbar" class="mynav">
                    <ul class="nav navbar-nav pull-right">
                        <li class="white"><a href="<%=contexturl %>webinarVideos"><b>Webinar Videos</b></a>
                        </li>
                        <li class="active"><a href="<%=contexturl %>ManageModuleUser/ModuleUserList"><b>Admin Side</b></a>
                        </li>
                        <li class="white"><a href="#logout_popup" data-toggle="modal"/><b>Log Out</b></a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
	</div>
    
    
    <div class="container" style="height:700px">
			<div class="page-header" style="margin-top:0px">
				<h3>
				Webinar Videos
				</h3>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div>
						<iframe width="560" height="315" src="https://www.youtube.com/embed/Sa7lfJ258sI" frameborder="0" allowfullscreen>
						</iframe>
					</div>
				</div>
				<div class="col-sm-6">
					<h4> Strategy Planning Part 1</strong></h4>
				</div>
			</div>
			
		</div>
   
<script src="<%=contexturl%>resources/js/vendor/jquery-1.11.0.min.js"></script>
<script src="<%=contexturl%>resources/js/vendor/bootstrap.min.js"></script>
<script type="text/javascript">
	
</script>

<%@include file="../inc/page_footer.jsp"%>
    