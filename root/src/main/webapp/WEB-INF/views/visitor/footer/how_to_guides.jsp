<!DOCTYPE html>
<html lang="en">
<%@include file="/WEB-INF/views/admin/inc/config.jsp"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Atma Network</title>
<!-- Style -->
<!-- Bootstrap -->
<link rel="shortcut icon" href="<%=contexturl %>resources/img/favicon.ico">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" media="screen"> 
<%-- <link href="<%=contexturl%>resources/css/bootstrap.css" rel="stylesheet" media="screen"> --%>
<%-- <link href="<%=contexturl%>resources/css/font-awesome.css" rel="stylesheet" media="screen"> --%>
<link href="<%=contexturl%>resources/css/fontface.css" rel="stylesheet" media="screen">

<!--[if IE 9]>
  <script src="<%=contexturl%>resources/js/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="<%=contexturl%>resources/js/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<!--[if IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

<link href="<%=contexturl%>resources/css/style.css" rel="stylesheet"
	media="screen">
</head>


<body>
	<div class="header-bg">
		<nav class="navbar">
			<div class="container">
				<div class="navbar-header">
					<img src="<%=contexturl%>resources/img/atma.png" />
				</div>
				<div id="navbar" class="mynav">
					<ul class="nav navbar-nav pull-right">
						<security:authorize access="isAuthenticated()">
							<li class="active"><a href="<%=contexturl%>"><b>Back to Home Page</b></a></li>
						</security:authorize>
						<security:authorize access="isAnonymous()">
							<li class="active"><a href="<%=contexturl%>Login"><b>Back to Login Page</b></a></li>
						</security:authorize>
					</ul>
				</div>
			</div>
		</nav>
		<div class="blue-container">
			<div class="container"></div>
		</div>
	</div>
	<br />
	<div class="container">
		<div class="row">
		  <h4><p class="text-center"><strong>How to guides</strong></p></h4>
		  <br />

		  <blockquote>
			  <ul>
				<li><h5><a href="http://www.slideshare.net/jigyasajain1001/how-to-register-56692303" target="_blank">How to Register</a></h5></li>
				<li><h5><a href="http://www.slideshare.net/jigyasajain1001/how-to-complete-your-profile" target="_blank">How to Complete Profile</a></h5></li>
				<li><h5><a href="http://www.slideshare.net/jigyasajain1001/how-to-add-module-user/1" target="_blank">How to add Module User</a></h5></li>
				<li><h5><a href="http://www.slideshare.net/jigyasajain1001/how-to-take-lss" target="_blank">How to take Life Stage Survey</a></h5></li>
				<li><h5><a href="http://www.slideshare.net/jigyasajain1001/how-to-register-as-module-user" target="_blank">How to Register as Module User</a></h5></li>
              </ul>
		  </blockquote>
		</div>
   	</div>
   	<div class="container" id="footer">
		<%@include file="../../visitor/inc/footer_items.jsp"%>
	</div>
</body>
<script src="<%=contexturl%>resources/js/vendor/jquery-1.11.0.min.js"></script>
<script src="<%=contexturl%>resources/js/vendor/bootstrap.min.js"></script>



</html>
