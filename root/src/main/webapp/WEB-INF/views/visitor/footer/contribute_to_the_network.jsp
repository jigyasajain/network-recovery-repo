<!DOCTYPE html>
<html lang="en">
<%@include file="/WEB-INF/views/admin/inc/config.jsp"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Atma Network</title>
<!-- Style -->
<!-- Bootstrap -->
<link rel="shortcut icon" href="<%=contexturl %>resources/img/favicon.ico">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" media="screen"> 
<%-- <link href="<%=contexturl%>resources/css/bootstrap.css" rel="stylesheet" media="screen"> --%>
<%-- <link href="<%=contexturl%>resources/css/font-awesome.css" rel="stylesheet" media="screen"> --%>
<link href="<%=contexturl%>resources/css/fontface.css" rel="stylesheet" media="screen">

<!--[if IE 9]>
  <script src="<%=contexturl%>resources/js/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="<%=contexturl%>resources/js/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<!--[if IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

<link href="<%=contexturl%>resources/css/style.css" rel="stylesheet"
	media="screen">
</head>


<body>
	<div class="header-bg">
		<nav class="navbar">
			<div class="container">
				<div class="navbar-header">
					<img src="<%=contexturl%>resources/img/atma.png" />
				</div>
				<div id="navbar" class="mynav">
					<ul class="nav navbar-nav pull-right">
						<security:authorize access="isAuthenticated()">
							<li class="active"><a href="<%=contexturl%>"><b>Back to Home Page</b></a></li>
						</security:authorize>
						<security:authorize access="isAnonymous()">
							<li class="active"><a href="<%=contexturl%>Login"><b>Back to Login Page</b></a></li>
						</security:authorize>
					</ul>
				</div>
			</div>
		</nav>
		<div class="blue-container">
			<div class="container"></div>
		</div>
	</div>
	<br />
	<div class="container">
		<div class="row">
		  <h4><p class="text-center"><strong>Contribute To The Network</strong></p></h4>
		  <br />
		  <blockquote>
			  <p>We hope that the Network has been able to guide you in multiple areas of development in your organisation. This platform has been developed to share the best practices and guides for implementing new processes and systems at your organisation. We hope that you can share your experiences and practices to help other young organisations. You can contribute to the Network!</p>
		  </blockquote>
		  <p><strong><u><h4>Why should you contribute?</h4></u></strong></p>
		  <blockquote>
			  <p>Atma has worked with various nonprofit organisations and social enterprises for more than 8 years. With our expertise we have been able to build a comprehensive library which can help NGOs which are beyond our reach. You can also be a part of this drive by just sharing the resources that you think can help other NGOs become better and more sustainable. You can also share your experiences that can set as an example for other young organisations. This way you can contribute to us and help multiple organisations around the world</p>
		  </blockquote>
		  <p><strong><u><h4>What can you contribute?</h4></u></strong></p>
		  <blockquote>
			  <p>Atma has been able to build a huge collection of user-friendly and evidence-based assessments, how- to guides, templates, readings on best practices, and real- life examples. You can share documents that can be classified under any of the following categorie</p>
			  <p>
			  	<ul>
			  		<li><h5>Blueprints (step by step guides)</h5></li>
			  		<li><h5>Tools (Templates, Checklists, Worksheets, Surveys and Assessments</h5></li>
			  		<li><h5>Examples</h5></li>
			  		<li><h5>Readings (Content compiled by expert sources</h5></li>
			  		<li><h5>Videos</h5></li>
			  	</ul>
			  </p>
			  <p>You can share any document that you think can help us build the Network to be an even more powerful tool.</p>
		  </blockquote>
		  <p><strong><u><h4>How can you contribute?</h4></u></strong></p>
		  <blockquote>
			  <p>You can share documents and materials with Atma network.  All you need to do is, email it to us at <a href="mailto:network@atma.org.in">network@atma.org.in</a>  with a signed letter of authorization attached below</p>
			  <br />
			  <p><strong><a href="<%=contexturl%>resource/open/loa">Download Letter of Authorization</a></strong></p>
		  </blockquote>
		</div>
   	</div>

   	<br/>
   	<br/>
   	<br/>
   	<br/>
   	<div class="container" id="footer">
		<%@include file="../../visitor/inc/footer_items.jsp"%>
	</div>
</body>
<%-- <script src="<%=contexturl%>resources/js/vendor/jquery-1.11.0.min.js"></script> --%>
<%-- <script src="<%=contexturl%>resources/js/vendor/bootstrap.min.js"></script> --%>
<script src="${pageContext.request.contextPath}/webjars/counter-webapp-frontend/0.0.1/vendor_bundle.js"></script>


</html>
