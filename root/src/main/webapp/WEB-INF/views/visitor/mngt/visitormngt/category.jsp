<%@page import="com.astrika.common.model.Role"%>
<%@page import="com.astrika.common.model.User"%>

<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/page_head.jsp"%>
<%-- <link href="<%=contexturl%>resources/css/lightbox.css" rel="stylesheet"/> --%>
<link href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/css/lightbox.css" rel="stylesheet"/>

<div class="blue-container">
	<div class="container">
		<h3 class="atma-breadcrumbs">
			<security:authorize ifAnyGranted="<%=Role.COMPANY_ADMIN.name()%>">
				<a class="light a-breadcrumb" href="<%=contexturl%>CompanyAdmin">Dashboard</a> &gt;<a
					class="light a-breadcrumb"
					href="<%=contexturl %>OrganisationalDevelopmentArea?id=${module.moduleId}">${module.moduleName}</a> &gt;<a
					class="light a-breadcrumb" href="">${category.categoryName}</a>
			</security:authorize>
			<security:authorize ifAnyGranted="<%=Role.MODULE_USER.name()%>">
				<a class="light a-breadcrumb" href="<%=contexturl%>ModuleUser">Dashboard</a> &gt;<a
					class="light a-breadcrumb"
					href="<%=contexturl %>OrganisationalDevelopmentArea?id=${module.moduleId}">${module.moduleName}</a> &gt;<a
					class="light a-breadcrumb" href="">${category.categoryName}</a>
			</security:authorize>
		</h3>
		<div class="row blue">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<p>${category.categoryDescription}</p>
				</div>

			</div>
			<hr style="margin: 8px;">
			<div class="row">
				<div class="col-md-2 col-sm-4">
					<!-- /.modal-content -->
					<!-- 				<div class="col-md-3 col-sm-5"> -->
					<div class="text-center">
						<img src="<%=contexturl%>resources/img/blue-print.jpg"
							alt="View Blue Print">

					</div>
					<p class="text-yellow text-center" style="margin: 0px;">
						<a href="javascript:void(0)" class="clickMe"><strong>VIEW</strong></a>
						&nbsp;&nbsp;|&nbsp;<a
							href="<%=contexturl.replace('\\', '/') %>Files${fn:replace(listOfBlueprint.documenturlpath,'\\','/')}"
							download="${listOfBlueprint.documentName}.${listOfBlueprint.doctype}"
							target="_blank"
							onclick="location.href='<%=contexturl %>Download/'+${listOfBlueprint.documentId}"><strong>DOWNLOAD</strong></a>
					</p>
					<ul id="lightbox-img" style="margin: 0px; padding-top: 3px;">
						<c:forEach var="bluePrintImage"
							items="${listOfBlueprint.blueprintImages}">
							<li><a
								href="<%=contexturl%>${bluePrintImage.image.imagePath}"
								data-lightbox="roadtrip"><strong>VIEW</strong></a></li>
						</c:forEach>
					</ul>

				</div>
				<div class="col-md-9  col-sm-7">
					<div class="media">
						<div class="contain">
							<p>
								<strong>BLUEPRINT DESCRIPTION</strong>
							</p>
							<p>${listOfBlueprint.documentDescription}</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<br>
<div class="container">
	<div class="row">
		<div class="col-md-9 col-sm-9">
			<div class="row">
				<div class="col-md-8">
					<div class="block full">
						<div class="block-title">
							<h4 class="">Tool Box</h4>
						</div>
						<div class="tool-container">
							<div class="toolbox-scroll">
								<div class="inside-toolbox-contant">
									<c:forEach var="listofToolBox" items="${listOfToolBox}"
										varStatus="counter">
										<div class="media media-devider">
											<c:if test="${listofToolBox.doctype == 'pdf'}">
												<div class="text-center pull-left">
													<a class="" href="#"> <i
														class="fa fa-file-pdf-o doc-icon"></i>
													</a>
												</div>
											</c:if>
											<c:if
												test="${listofToolBox.doctype == 'doc' || listofToolBox.doctype == 'docx' || listofToolBox.doctype == 'txt'}">
												<div class="text-center pull-left">
													<a class="" href="#"> <i
														class="fa fa-file-word-o doc-icon"></i>
													</a>
												</div>
											</c:if>
											<c:if
												test="${listofToolBox.doctype == 'xlsx' || listofToolBox.doctype == 'xls'}">
												<div class="text-center pull-left">
													<a class="" href="#"> <i
														class="fa fa-file-excel-o doc-icon"></i>
													</a>
												</div>
											</c:if>
											<c:if test="${listofToolBox.doctype == 'link'}">
												<div class="text-center pull-left">
													<a class="" href="#"> <i class="fa fa-link doc-icon"></i>
													</a>
												</div>
											</c:if>
											<div class="media-body">
												<c:choose>
													<c:when test="${empty listofToolBox.documentName}">
														<h5 class="media-heading">
															<a href="${listofToolBox.link}" target="_blank"
																onclick="location.href='<%=contexturl %>Download/'+${listofToolBox.documentId}"><strong>${listofToolBox.title}</strong></a>
														</h5>.
													</c:when>
													<c:otherwise>
														<h5 class="media-heading">
															<a
																href="<%=contexturl.replace('\\', '/') %>Files${fn:replace(listofToolBox.documenturlpath,'\\','/')}"
																download="${listofToolBox.documentName}.${listofToolBox.doctype}"
																target="_blank"
																onclick="location.href='<%=contexturl %>Download/'+${listofToolBox.documentId}"><strong>${listofToolBox.title}</strong></a>
														</h5>${listofToolBox.documentDescription}.
	                                            			<p>
															<small>Applicable if you are at</small> <strong
																class="text-rating"> <c:if
																	test="${listofToolBox.lifeStages.id eq 1}">
																	<span class="circle-highlight numbers"> 1 </span>
																	<span class="circle-highlight numbers"> 2 </span>
																	<span class="circle-highlight numbers"> 3 </span>
																	<span class="circle-highlight numbers"> 4 </span>
																	<span class="circle-highlight numbers"> 5 </span>
																</c:if> <c:if test="${listofToolBox.lifeStages.id eq 2}">
																	<span class="circle-highlight numbers"> 1 </span>
																	<span class="numbers"> 2 </span>
																	<span class="numbers"> 3 </span>
																	<span class="numbers"> 4 </span>
																	<span class="numbers"> 5 </span>
																</c:if> <c:if test="${listofToolBox.lifeStages.id eq 3}">
																	<span class="numbers"> 1 </span>
																	<span class="circle-highlight numbers"> 2 </span>
																	<span class="numbers"> 3 </span>
																	<span class="numbers"> 4 </span>
																	<span class="numbers"> 5 </span>
																</c:if> <c:if test="${listofToolBox.lifeStages.id eq 4}">
																	<span class="numbers"> 1 </span>
																	<span class="numbers"> 2 </span>
																	<span class="circle-highlight numbers"> 3 </span>
																	<span class="numbers"> 4 </span>
																	<span class="numbers"> 5 </span>
																</c:if> <c:if test="${listofToolBox.lifeStages.id eq 5}">
																	<span class="numbers"> 1 </span>
																	<span class="numbers"> 2 </span>
																	<span class="numbers"> 3 </span>
																	<span class="circle-highlight numbers"> 4 </span>
																	<span class="numbers"> 5 </span>
																</c:if> <c:if test="${listofToolBox.lifeStages.id eq 6}">
																	<span class="numbers"> 1 </span>
																	<span class="numbers"> 2 </span>
																	<span class="numbers"> 3 </span>
																	<span class="numbers"> 4 </span>
																	<span class="circle-highlight numbers"> 5 </span>
																</c:if> <c:if test="${listofToolBox.lifeStages.id eq 7}">
																	<span class="numbers"> 1 </span>
																	<span class="numbers"> 2 </span>
																	<span class="numbers"> 3 </span>
																	<span class="numbers"> 4 </span>
																	<span class="numbers"> 5 </span>
																</c:if>
															</strong>
														</p>
													</c:otherwise>
												</c:choose>
											</div>
										</div>
									</c:forEach>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="block full">
						<div class="block-title">
							<h4 class="">Examples</h4>
						</div>
						<div class="media-scroll">
							<div class="inside-media-contant">

								<c:forEach var="listofExamples" items="${listOfExamples}"
									varStatus="counter">
									<c:choose>
										<c:when test="${empty listofExamples.documentName}">
											<p class="wordWrap">
												<i class="fa fa-link"></i>
												<a id = "example_${counter.count}" class="example" data-toggle="popover" data-placement="top" 
													data-content="${listofExamples.documentDescription}"
													href="${listofExamples.link}" target="_blank"
													onclick="location.href='<%=contexturl %>Download/'+${listofExamples.documentId}">
												<small>${listofExamples.title}</small></a>
											</p>
										</c:when>
										<c:otherwise>
											<p>
												<c:if test="${listofExamples.doctype == 'pdf'}">
													<i class="fa fa-file-pdf-o"></i>
												</c:if>
												<c:if
													test="${listofExamples.doctype == 'doc' || listofExamples.doctype == 'docx' || listofExamples.doctype == 'txt'}">
													<i class="fa fa-file-word-o"></i>
												</c:if>
												<c:if test="${listofExamples.doctype == 'xlsx'}">
													<i class="fafa-file-excel-o"></i>
												</c:if>
												<a id = "example_${counter.count}" class="example" data-toggle="popover" data-placement="top"
													data-content="${listofExamples.documentDescription}"
													href="<%=contexturl.replace('\\', '/') %>Files${fn:replace(listofExamples.documenturlpath,'\\','/')}"
													download="${listofExamples.documentName}.${listofExamples.doctype}"
													target="_blank" 
													onclick="location.href='<%=contexturl %>Download/'+${listofExamples.documentId}">
												<small>${listofExamples.title}</small></a>
											</p>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</div>
						</div>
					</div>
					<div class="block full">
						<div class="block-title">
							<h4 class="">Reading</h4>
						</div>
						<div class="Reading-scroll">
							<div class="inside-reading-contant">
								<c:forEach var="listofReading" items="${listOfReading}"
									varStatus="counter">
									<c:choose>
										<c:when test="${empty listofReading.documentName}">
											<p>
												<i class="fa fa-link"></i><a href="${listofReading.link}" id = "reading_${counter.count}" class="reading" data-toggle="popover" data-placement="top"
													data-content="${listofReading.documentDescription}"
													target="_blank" 
													onclick="location.href='<%=contexturl %>Download/'+${listofReading.documentId}">&nbsp;${listofReading.title}</a>
											</p>
										</c:when>
										<c:otherwise>
											<p>
												<c:if test="${listofReading.doctype == 'pdf'}">
													<i class="fa fa-file-pdf-o"></i>
												</c:if>
												<c:if
													test="${listofReading.doctype == 'doc' || listofReading.doctype == 'docx' || listofReading.doctype == 'txt'}">
													<i class="fa fa-file-word-o"></i>
												</c:if>
												<c:if test="${listofReading.doctype == 'xlsx'}">
													<i class="fafa-file-excel-o"></i>
												</c:if>
												<a	id = "reading_${counter.count}" class="reading" data-toggle="popover" data-placement="top"
													data-content="${listofReading.documentDescription}"
													href="<%=contexturl.replace('\\', '/') %>Files${fn:replace(listofReading.documenturlpath,'\\','/')}"
													download="${listofReading.documentName}.${listofReading.doctype}"
													target="_blank"
													onclick="location.href='<%=contexturl %>Download/'+${listofReading.documentId}"><small>${listofReading.title}</small></a>
											</p>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</div>
						</div>
					</div>
				</div>

			</div>
			<div class="block full">
				<div class="comment-area">
					<div class="comment-box">
						<c:forEach var="comment" items="${comments}" varStatus="counter">
							<div class="media topic">
								<c:choose>
									<c:when test="${not empty comment.createdBy.profileImage}">
										<a class="pull-left" href="#"> <img class="media-object"
											src="<%=contexturl %><%=contexturl.replace('\\', '/') %>Files${fn:replace(commentTest.createdBy.profileImage.imagePath,'\\','/')}"
											alt="...">
										</a>
									</c:when>
									<c:otherwise>
										<a class="pull-left" href="#"> <img class="media-object"
											src="<%=contexturl %>resources/img/User-icon.png" alt="...">
										</a>
									</c:otherwise>
								</c:choose>
								<div class="media-body wrap-word">
									<h4 class="media-heading">${comment.createdBy.fullName}</h4>
									${comment.comments}
								</div>
							</div>
						</c:forEach>
						<form action="<%=contexturl%>SaveComment" method="post"
							class="form-horizontal ui-formwizard" id="Comment_form"
							enctype="multipart/form-data">
							<input name="moduleId" id="moduleId" type="hidden"
								value="${module.moduleId}" /> <input name="categoryId"
								id="categoryId" type="hidden" value="${category.categoryId}" />
							<div class="row">
								<div class="col-sm-12">
									<br>
									<div class="col-sm-12 form-group">
										<div>
											<textarea id="commentBox" name="comments"
												class="form-control" rows="2"
												style="margin: 0px -0.5px 0px 0px; height: 40px;"
												placeholder="Add a comment here">${comment.comments}</textarea>
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<button id="comment_submit" type="submit"
										class="pull-right btn btn-sm btn-black">Submit
										Comment</button>
									<span class="clearfix"></span>
								</div>
							</div>
						</form>
					</div>
				</div>
				<!--div class="form-group">
                        <textarea class="form-control" rows="3" style="margin: 0px -0.5px 0px 0px;  height: 80px;"></textarea>
                    </div>
                    <button type="button" class="btn btn-black pull-right">Submit </button><span class="clearfix"></span!-->
			</div>
		</div>

		<%@include file="../../inc/images_sidebar.jsp"%>
	</div>
</div>


<script src="${pageContext.request.contextPath}/webjars/counter-webapp-frontend/0.0.1/vendor_bundle.js"></script>
<%-- <script src="<%=contexturl%>resources/js/vendor/jquery-1.11.0.min.js"></script> --%>
<%-- <script src="<%=contexturl%>resources/js/vendor/bootstrap.min.js"></script> --%>
<%-- <script src="<%=contexturl %>resources/js/vendor/validate.js"></script> --%>

<script src="${pageContext.request.contextPath}/webjars/counter-webapp-frontend/0.0.1/app_bundle.js"></script>
<%-- <script src="<%=contexturl %>resources/js/additional-methods.js"></script> --%>


<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.map"></script>
<%-- <script src="<%=contexturl %>resources/js/lightbox.min.js"></script> --%>
<%-- <script src="<%=contexturl %>resources/js/lightbox.min.map"></script> --%>

  <script type="text/javascript">
	$(document).ready( function(){
			// HOVER FUNCTIONAL 
			
				$(".example").hover(function() {
								// this is the mouseenter event handler
								var boom = $(this).attr('id');
								//$(this).addClass("my_hover");
								//alert(boom);
								$(this).popover('show');
							}, function() {
								// this is the mouseleave event handler
								$(this).popover('hide');

							}),

							$(".reading").hover(function() {
								// this is the mouseenter event handler
								var boom = $(this).attr('id');
								//$(this).addClass("my_hover");
								//alert(boom);
								$(this).popover('show');
							}, function() {
								// this is the mouseleave event handler
								$(this).popover('hide');

							});

								// fuction for manage scroll
								//media-scroll
								if ($('.media-scroll').height() < $(
										'.inside-media-contant').height()) {
									$('.media-scroll').css('overflow-y',
											'scroll');
								}
								//Reading-scroll
								if ($('.Reading-scroll').height() < $(
										'.inside-reading-contant').height()) {
									$('.Reading-scroll').css('overflow-y',
											'scroll');
								}
								//toolbox-scroll
								if ($('.toolbox-scroll').height() < $(
										'.inside-toolbox-contant').height()) {
									$('.toolbox-scroll').css('overflow-y',
											'scroll');
								}

			

								$(".clickMe").on("click",function() {
											$("#lightbox-img li:eq(0) a").trigger("click");
										});

								$("#Comment_form").validate(
												{
													errorClass:"help-block animation-slideDown",
													errorElement:"div",
													errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
													highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
													success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
													rules : {
														comments : {
															required : !0,
															maxlength : 255,
														}
													},
													messages : {
														comments : {
															required : 'You cannot enter a blank comment',
															maxlength : 'Comment should not be more than 255 characters',
														}
													},
												});

								$("#comment_submit").click(function() {
									unsaved = false;
									$("#Comment_form").submit();
								});

							});
		</script>


    <%@include file="../../inc/page_footer.jsp"%>