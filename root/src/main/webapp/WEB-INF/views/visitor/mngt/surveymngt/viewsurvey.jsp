<%@page import="com.astrika.common.model.Role"%>
<%@page import="com.astrika.common.model.User"%>


<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/page_head.jsp"%>


  <div class="blue-container">
            <div class="container">
                <h3>
                	<security:authorize ifAnyGranted="<%=Role.COMPANY_ADMIN.name()%>">
						<a class="light a-breadcrumb" href="<%=contexturl %>CompanyAdmin">Dashboard</a> &gt;<a class="light a-breadcrumb" href="<%=contexturl %>ManageSurvey/SurveyInstructions">Instructions</a>&gt;<a class="light a-breadcrumb">Survey</a> 
					</security:authorize>
					<security:authorize ifAnyGranted="<%=Role.MODULE_USER.name()%>">
						<a class="light a-breadcrumb" href="<%=contexturl %>ModuleUser">Dashboard</a> &gt;<a class="light a-breadcrumb" href="<%=contexturl %>ManageSurvey/SurveyInstructions">Instructions</a>&gt;<a class="light a-breadcrumb">Survey</a> 
					</security:authorize>
                
                </h3>
                <div class="row blue">
                    <div class="col-md-12  col-sm-12">
                        <div class="media">
                            <div class="contain">
                               <p class="text-white">Below you will find a series of statements. Please read each statement carefully and move the slider beneath each one to select how accurately it describes your organisation right now. You may select any number from 1 to 5.</p>
                               <div class="row">
                               	<div class="col-md-2 col-md-offset-1 text-center"><strong>1. Completely disagree-this does not sound like us at all.</strong></div>
                               	<div class="col-md-2 text-center"><strong>2. Disagree-This doesn't sound much like us.</strong></div>
                               	<div class="col-md-2 text-center"><strong>3. Neutral-This is not the opposite of us but not the same as us either</strong></div>
                               	<div class="col-md-2 text-center"><strong>4. Agree-This sounds a little like us.</strong></div>
                               	<div class="col-md-2 text-center"><strong>5. Completely agree-this sounds a lot like us.</strong></div>
                               </div>
                               <p >There are no right or wrong answers! Just select whichever number is most true for you.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
  </div>
</div>

<div class="container">
	<div class="row">
	</div>
	<div class="row">
		<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i>
						<spring:message code="label.success" />
					</h4>
					<spring:message code="${success}" />
				</div>
		</c:if>
			
		<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
					</h4>
					<spring:message code="${error}" />
				</div>
		</c:if>
		<div class="col-md-9 col-sm-9">
			<div class="row">
				<div class="col-md-4">
					<div class="block full">
						<div class="block-title">
							<h4 class="">Organisational Development Areas</h4>
						</div>
						<div class="">
							<c:forEach var="module" items="${modules}" varStatus="counter">
								<div class="media">
									<a class="pull-left"
										href="<%=contexturl%>ManageSurvey/TakeSurvey/${module.moduleId}">
										<img class="media-object"
										src="<%=contexturl%>${module.profileImage.imagePath}"
										alt="..." height="50px" width="50px">
									</a>
									<div class="media-body">
										<h4 class="media-heading"><a class="text-darkgray"
										href="<%=contexturl%>ManageSurvey/TakeSurvey/${module.moduleId}">${module.moduleName}</a></h4>
										<div class="progress">
											<div class="progress-bar progress-bar-success"
												role="progressbar" aria-valuenow="${moduleUserList[counter.index].surveyWeightPercent}" aria-valuemin="0"
												aria-valuemax="100" style="width: ${moduleUserList[counter.index].surveyWeightPercent}%">
												<span class="sr-only">${moduleUserList[counter.index].surveyWeightPercent}% Complete (success)</span>
											</div>
										</div>
										<small>${moduleUserList[counter.index].surveyWeightPercent}% Completed</small>
									</div>
								</div>
							</c:forEach>
						</div>
					</div>
				</div>
				<div class="col-md-8">
					<div class="block full">
						<div class="block-title">
							<h4 class="">${module.moduleName}</h4>
						</div>
						<form action="<%=contexturl%>ManageSurvey/ViewSurvey/SaveVisitorSurvey" method="post" class="form-horizontal ui-formwizard"
							id="Survey_form" enctype="multipart/form-data">
							<input name="moduleId" id="moduleId" type="hidden" value="${module.moduleId}" />
							<input id="act" type="hidden" name="act">
							<div class="panel-group" id="accordion">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a class="accordion-toggle" data-toggle="collapse"
												data-parent="#accordion" href="#collapseOne"> Section 1 </a>
											
										</h4>
									</div>
									<div id="collapseOne" class="panel-collapse collapse in">
										<div class="panel-body">
											<div class="questions-container">
												<c:forEach var="list1" items="${list1}" varStatus="counter">
													<div class="question">
														<label for = "question_no" class="question_label" data-toggle="popover" data-placement="top" data-content="${list1.questionExplanation}">Q-${counter.count} ${list1.question}</label> <input type="text"
															id="testslider_${counter.count}" name="testslider_${counter.count}"/>
														<input name="${counter.count}" id="${counter.count}" type="hidden" value="${list1.question}" />
													</div>
													<!-- END first 5 Q. -->
												</c:forEach>
											</div>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle" data-toggle="collapse"
                                                data-parent="#accordion" href="#collapseTwo"> Section 2 </a>

                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <div class="questions-container">
                                                <c:forEach var="list2" items="${list2}" varStatus="counter">
                                                    <div class="question">
                                                        <label for = "question_no" class="question_label" data-toggle="popover" data-placement="top" data-content="${list2.questionExplanation}">Q-${counter.count+3} ${list2.question}</label> <input type="text"
                                                            id="testslider_${counter.count+3}" name="testslider_${counter.count+3}"/>
                                                        <input name="${counter.count+3}" id="${counter.count+3}" type="hidden" value="${list2.question}" />
                                                    </div>
                                                    <!-- END first 5 Q. -->
                                                </c:forEach>
                                            </div>
                                        </div>
                                    </div>
                                </div>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a class="accordion-toggle" data-toggle="collapse"
												data-parent="#accordion" href="#collapseThree">
												Section 3</a>
										</h4>
									</div>
									<div id="collapseThree" class="panel-collapse collapse in">
										<div class="panel-body">
											<div class="questions-container">
												<c:forEach var="list3" items="${list3}" varStatus="counter">
													<div class="question">
														<label for = "question_no" class="question_label" data-toggle="popover" data-placement="top" data-content="${list3.questionExplanation}">Q-${counter.count+6} ${list3.question}</label> <input type="text"
															id="testslider_${counter.count+6}" name="testslider_${counter.count+6}"/>
														<input name="${counter.count+6}" id="${counter.count+6}" type="hidden" value="${list3.question}" />
													</div>
													<!-- END first 5 Q. -->
												</c:forEach>
											</div>
										</div>
									</div>
								</div>
								<!-- END collapse-2-->
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a class="accordion-toggle" data-toggle="collapse"
												data-parent="#accordion" href="#collapseFour">
												Section 4 </a>
										</h4>
									</div>
									<div id="collapseFour" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <div class="questions-container">
                                                <c:forEach var="list4" items="${list4}" varStatus="counter">
                                                    <div class="question">
                                                        <label for = "question_no" class="question_label" data-toggle="popover" data-placement="top" data-content="${list4.questionExplanation}">Q-${counter.count+9} ${list4.question}</label> <input type="text"
                                                            id="testslider_${counter.count+9}" name="testslider_${counter.count+9}"/>
                                                        <input name="${counter.count+9}" id="${counter.count+9}" type="hidden" value="${list4.question}" />
                                                    </div>
                                                <!-- END first 5 Q. -->
                                                </c:forEach>
                                            </div>
                                        </div>
									</div>
								</div>
								<div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle" data-toggle="collapse"
                                                data-parent="#accordion" href="#collapseFive">
                                                Section 5 </a>
                                        </h4>
                                    </div>
                                    <div id="collapseFive" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <div class="questions-container">
                                                <c:forEach var="list5" items="${list5}" varStatus="counter">
                                                    <div class="question">
                                                        <label for = "question_no" class="question_label" data-toggle="popover" data-placement="top" data-content="${list5.questionExplanation}">Q-${counter.count+12} ${list5.question}</label> <input type="text"
                                                            id="testslider_${counter.count+12}" name="testslider_${counter.count+12}"/>
                                                        <input name="${counter.count+12}" id="${counter.count+12}" type="hidden" value="${list5.question}" />
                                                    </div>
                                                <!-- END first 5 Q. -->
                                                </c:forEach>
                                            </div>
                                        </div>
                                    </div>
                                </div>
							</div>
							<br>
						</form>
						
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-3 hidden-xs">
			<div class="adv">
				<img class="img-responsive"
					src="<%=contexturl %>resources/img/AtmaNetwork.jpg" />
			</div>
			<c:choose>
					<c:when test = "${empty atmaContactImage.contactImage}">
						 <div class="need-help"><img class="img-responsive" src="<%=contexturl %>resources/img/need help-3.jpg" /><br><br></div>
					</c:when>
					<c:otherwise>
						 <div class="need-help"><img class="img-responsive" src="<%=contexturl%>${atmaContactImage.contactImage.imagePath}" /><br><br></div>
					</c:otherwise>
			</c:choose>
		</div>
	</div>
</div>

<div class="container">
        <div class="row">
        </div>
    </div>
  
<!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> -->
<!--  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script> -->
<script src="${pageContext.request.contextPath}/webjars/counter-webapp-frontend/0.0.1/vendor_bundle.js"></script> 
 
 <script src="<%=contexturl%>resources/js/jquery.bxslider.js"></script>
 <script src="<%=contexturl%>resources/js/ion-rangeSlider/ion.rangeSlider.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
    	
//     	<c:if test = "${empty list2}">$("#survey_save").hide(),
// 		$("#survey_submit").hide(),
// 		</c:if>

        $(".question_label").hover(function() {
            $(this).popover('show');
        }, function() {
            $(this).popover('hide');
        });

        <c:forEach var="list1" items="${list1}" varStatus="counter">
        $("#testslider_${counter.count}").ionRangeSlider({
            grid : true,
            min : 0,
            max : 5,
            from : "${list1.answer}",
            disable: true,
            grid_num : 5
        });
        </c:forEach>


        <c:forEach var="list2" items="${list2}" varStatus="counter">
        $("#testslider_${counter.count+3}").ionRangeSlider({
        	grid : true,
			min : 0,
			max : 5,
			from : "${list2.answer}",
			disable: true,
			grid_num : 5
        });
        </c:forEach>
        
        <c:forEach var="list3" items="${list3}" varStatus="counter">
        $("#testslider_${counter.count+6}").ionRangeSlider({
        	grid : true,
			min : 0,
			max : 5,
			from :"${list3.answer}",
			disable: true,
			grid_num : 5
        });
        </c:forEach>
        
        <c:forEach var="list4" items="${list4}" varStatus="counter">
        $("#testslider_${counter.count+9}").ionRangeSlider({
        	grid : true,
			min : 0,
			max : 5,
			from :"${list4.answer}",
			disable: true,
			grid_num : 5
        });
        </c:forEach>

        <c:forEach var="list5" items="${list5}" varStatus="counter">
        $("#testslider_${counter.count+12}").ionRangeSlider({
            grid : true,
            min : 0,
            max : 5,
            from :"${list5.answer}",
            disable: true,
            grid_num : 5
        });
        </c:forEach>
        
        $("#survey_save").click(function(){
			unsaved=false;
			$("#act").val("0");
			 $("#Survey_form").submit();
		 });
		
		$("#survey_submit").click(function(){
			unsaved=false;
			$("#act").val("1");
			 return check(); 
      	  $("#Survey_form").submit();
        });

    });
</script>

 <%@include file="../../inc/page_footer.jsp"%>
