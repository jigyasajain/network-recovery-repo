<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>


<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>

			<h1>
				<spring:message code="heading.forum" />
				<br> <small><spring:message
						code="heading.newforum" /></small>
			</h1>
			<span id="errorMsg"></span>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="menu.manageforum" /></li>
	</ul>
	<form action="<%=contexturl %>SaveForum" method="post" class="form-horizontal ui-formwizard"
		id="Forum_form" enctype="multipart/form-data">
		<input name="id" value="${module.moduleId}" type="hidden">
		<div class="row">
  		
			<div class="">
				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.foruminfo" /></strong>
						</h2>
						 
					</div>
					<div class="form-group"> 
					
						<label class="col-md-4 control-label" for="title"><spring:message
								code="label.forumtitle" /> <span class="text-danger">*</span>
						</label>
						<div class="col-md-6">
                         
							<input id="title" name="title" class="form-control"
								placeholder="<spring:message code='label.forumtitle'/>.."
								type="text" value="${forum.title}">
						</div>
					</div>

					
					<div class="form-group" id="moduleDiv">
						<label class="col-md-4 control-label" for="chosenTagId"><spring:message
 								code="label.tag" /><span class="text-danger">*</span></label> 
						<div class="col-md-6">
							<select class="tag_chosen" style="width: 200px;"
								id="chosenTagId"
								data-placeholder="<spring:message code='label.tag' />"
								name="chosenTagId">
								<option value=""></option>
								<c:forEach items="${forumList}" var="tag">
											<option value="${tag}" <c:if test="${tag eq forum.tag}">selected="selected"</c:if> >${tag.name}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					


				</div>



	</div>

			
</div>

	<div style="text-align: center; margin-bottom: 10px">
		<div class="form-group form-actions">
<!-- 				<div class="col-md-9 col-md-offset-3"> -->
					<button id="company_submit" type="submit"
						class="btn btn-sm btn-primary save">
						<i class="fa fa-angle-right"></i> <spring:message code="button.save" />
					</button>
					<button type="reset" class="btn btn-sm btn-warning save">
						<i class="fa fa-repeat"></i>
						<spring:message code="button.reset" />
					</button>
<%-- 						<a class="btn btn-sm btn-primary save" href="<%=contexturl %>Login"> --%>
<%-- 							<spring:message code="button.cancel" /> </a> --%>
		</div>
	</div>
</form>
</div>
<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>

<%-- <script src="<%=contexturl %>resources/js/custom/featurecontrol.js"></script> --%>
<script src="${pageContext.request.contextPath}/webjars/counter-webapp-frontend/0.0.1/app_bundle.js"></script>

<%@include file="../../inc/chosen_scripts.jsp"%>
<script type="text/javascript">
	$(document)
			.ready(
					function() {
						
					
						$(".tag_chosen").data("placeholder","Select Tag From...").chosen();	
 						
						$("#Forum_form").validate(
						{	errorClass:"help-block animation-slideDown",
							errorElement:"div",
							errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
							highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
							success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
										rules : {
											title : {
												required : !0,maxlength : 75
											},
											chosenTagId: {
												required : !0
											}
											
										},
										messages : {
											title : {
												required :'<spring:message code="validation.pleaseenteratitle"/>',
												maxlength :'<spring:message code="validation.title75character"/>'
											},
											chosenTagId: {
												required : '<spring:message code="validation.selecttag"/>'
											}
											
										},
								});

						$("#company_submit").click(function() {
							unsaved=false;
							$("#Forum_form").submit();
						});
						
						$("button[type=reset]").click(function(evt) {
							evt.preventDefault(); // stop default reset behavior (or the rest of this code will run before the reset actually happens)
							form = $(this).parents("form").first(); // get the reset buttons form
							form[0].reset(); // actually reset the form
							 // tell all Chosen fields in the form that the values may have changed
							form.find(".country_chosen").trigger("chosen:updated");
							
							var myOptions = "<option value></option>";
							$(".city_chosen").html(myOptions).chosen().trigger("chosen:updated");
							
							$(".state_chosen").html(myOptions).chosen().trigger("chosen:updated");
							});

						
					});
	

</script>

<style>
.chosen-container {
	width: 250px !important;
}
#map-canvas {
	height: 300px
}
</style>
<%@include file="../../inc/template_end.jsp"%>