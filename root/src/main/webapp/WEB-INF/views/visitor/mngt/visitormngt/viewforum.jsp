<%@page import="com.astrika.common.model.Role"%>
<%@page import="com.astrika.common.model.User"%>

<%@include file="/WEB-INF/views/admin/inc/config.jsp"%>
<%-- <%@include file="../../inc/config.jsp"%> --%>
<%@include file="../../inc/page_head.jsp"%>


<div class="blue-container">
            <div class="container">
                <h3>
                <security:authorize ifAnyGranted="<%=Role.COMPANY_ADMIN.name()%>">
						<a class="light a-breadcrumb" href="<%=contexturl %>CompanyAdmin">Dashboard</a> &gt;<a class="light a-breadcrumb" href="<%=contexturl %>OrganisationalDevelopmentArea?id=${module.moduleId}">${module.moduleName}</a> &gt;<a class="light a-breadcrumb">Forums</a> 
					</security:authorize>
					<security:authorize ifAnyGranted="<%=Role.MODULE_USER.name()%>">
						<a class="light a-breadcrumb" href="<%=contexturl %>ModuleUser">Dashboard</a> &gt;<a class="light a-breadcrumb" href="<%=contexturl %>OrganisationalDevelopmentArea?id=${module.moduleId}">${module.moduleName}</a> &gt;<a class="light a-breadcrumb">Forums</a>
					</security:authorize>
                </h3>
                <div class="row blue">
                    <div class="col-md-12  col-sm-12">
                        <div class="media">
                            <div class="contain">
                                <p>${module.moduleDescription}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<br>
<div class="container">
	<div class="row">
		<div class="col-md-9 col-sm-9">
			<div class="row">
				<div class="col-md-12">
					<c:forEach var="forumList" items="${forumList}" varStatus="counter">
						<div class="topic-box">
							<div class="Topic-title">
								<a href="<%=contexturl %>ForumComments/${forumList.forumId}">
									<h4 class="text-blue">${forumList.title}<small class="forum-label pull-right">${forumList.tag.name}</small></h4>
								</a>
								
							</div>
							<div>
								<p>${forumList.description}</p>
							</div>
							<div class="fourm-comment">
								<div>
									<div style="margin-bottom: 10px;">
										
									</div>
								</div>
							</div>
						</div>
					</c:forEach>
					<!-- END Topic -->
				</div>
			</div>
		</div>
		<%@include file="../../inc/images_sidebar.jsp"%>
	</div>
</div>

<%-- <script src="<%=contexturl%>resources/js/vendor/jquery-1.11.0.min.js"></script> --%>
<%-- <script src="<%=contexturl%>resources/js/vendor/bootstrap.min.js"></script> --%>
<%-- <script src="<%=contexturl %>resources/js/vendor/validate.js"></script> --%>
<script src="${pageContext.request.contextPath}/webjars/counter-webapp-frontend/0.0.1/vendor_bundle.js"></script>

<%-- <script src="<%=contexturl %>resources/js/additional-methods.js"></script> --%>
<script src="${pageContext.request.contextPath}/webjars/counter-webapp-frontend/0.0.1/app_bundle.js"></script>

 <script type="text/javascript">
	$(document)
			.ready(
					function() {

						$("#Comment_form").validate(
								{
									errorClass:"help-block animation-slideDown",
									errorElement:"div",
									errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
									highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
									success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
																	rules : {
																		comments : {
																			required : !0,
																			maxlength : 255
																		}
																	},
																	messages : {
																		comments : {
																			required :'You cannot enter a blank comment',
																			maxlength :'Comment should not be more than 255 characters'
																		}		
																	},
															});
						
						
						$("#comment_submit").click(function() {
							unsaved=false;
							$("#Comment_form").submit();
						});
						
						
						
					});
	

</script>
<script>

function showCommentsDiv(){
	$("#forumCommentDiv").show();
}

</script>
 
 <%@include file="../../inc/page_footer.jsp"%>