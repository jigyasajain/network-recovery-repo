<%@page import="com.astrika.common.model.Role"%>
<%@page import="com.astrika.common.model.User"%>


<%@include file="/WEB-INF/views/admin/inc/config.jsp"%>
<%-- <%@include file="../../inc/config.jsp"%> --%>
<%@include file="../../inc/page_head.jsp"%>
<style>
.comment-box span {
  right: 10px;
  position: absolute;
  top: 10px;
}
</style>

<div class="blue-container">
            <div class="container">
            	<h3 class="atma-breadcrumbs">
            		<security:authorize ifAnyGranted="<%=Role.ATMA_ADMIN.name()%>">
						<a class="light a-breadcrumb" href="<%=contexturl%>ManageForums/ForumList?id=${forum.module.moduleId}">Back</a>
					</security:authorize>
					<security:authorize ifAnyGranted="<%=Role.COMPANY_ADMIN.name()%>">
						<a class="light a-breadcrumb" href="<%=contexturl%>Forums/${forum.module.moduleId}">Back to Forums</a>
					</security:authorize>
					<security:authorize ifAnyGranted="<%=Role.MODULE_USER.name()%>">
						<a class="light a-breadcrumb" href="<%=contexturl%>Forums/${forum.module.moduleId}">Back to Forums</a>
					</security:authorize>
					
				</h3>
<%--                 <h3>
					<a class="light a-breadcrumb" href="<%=contexturl%>Forums/${forum.module.moduleId}">Back to Forums</a>
				</h3> --%>
                <div class="row blue">
                    <div class="col-md-12  col-sm-12">
                        <div class="media">
                            <div class="contain">
<%--                                 <p>${module.moduleDescription}</p> --%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<br>
<div class="container">
	<div class="row">
		<div class="col-md-9 col-sm-9">
			<div class="block full">
				<div class="row">
					<div class="col-md-12">
						<!-- 						<div class="topic-box"> -->
						<div class="Topic-title">
							<h4 class="text-blue">${forum.title}<small
									class="forum-label pull-right">${forum.tag.name}</small>
							</h4>
						</div>
						<div>
							<p class="wrap-word">${forum.description}</p>
						</div>
						<div class="comment-area">
							<div class="comment-box">
								<c:forEach var="commentTest" items="${commentList}"
									varStatus="counter">

										<div class="media topic">
											<c:choose>
												<c:when
													test="${not empty commentTest.createdBy.profileImage}">
													<a class="pull-left" href="#"> <img
														class="media-object"
														src="<%=contexturl.replace('\\', '/') %>${fn:replace(commentTest.createdBy.profileImage.imagePath,'\\','/')}"
														alt="...">
													</a>
												</c:when>
												<c:otherwise>
													<a class="pull-left" href="#"> <img
														class="media-object"
														src="<%=contexturl%>resources/img/User-icon.png" alt="...">
													</a>
												</c:otherwise>
											</c:choose>
											<div class="media-body wrap-word">
												<h4 class="media-heading">${commentTest.createdBy.firstName}</h4>
												${commentTest.comments}
												
											</div>
											<c:if test="${atmaAdminUser eq true}">
												<span class=""><a href="#delete_comment"
													data-toggle="modal"
													onclick="deleteComment(${commentTest.commentId})"
													title="Delete Comment"><span
														class="glyphicon glyphicon-remove text-danger"></span></a></span>
											</c:if>
										</div>

								</c:forEach>
								<form action="<%=contexturl%>SaveForumComment" method="post"
									class="form-horizontal ui-formwizard" id="Comment_form"
									enctype="multipart/form-data">
									<input name="forumId" id="forumId" type="hidden"
										value="${forum.forumId}" />
									<div class="row">
										<div class="col-sm-12">
											<br>
											<div class="col-sm-12 form-group">
												<div>
													<textarea id="commentBox" name="comments"
														class="form-control" rows="2"
														style="margin: 0px -0.5px 0px 0px; height: 40px;"
														placeholder="Add a comment here">${comment.comments}</textarea>
												</div>
											</div>

										</div>
										<div class="col-sm-12">
											<button id="comment_submit" type="submit"
												class="pull-right btn btn-sm btn-black">Submit</button>
											<span class="clearfix"></span>
										</div>
									</div>
								</form>
							</div>
						</div>
						<!-- 						</div> -->
						<!-- END Topic -->
					</div>
				</div>
			</div>
		</div>
		<%@include file="../../inc/images_sidebar.jsp"%>
	</div>
	<div id="delete_comment" class="modal fade" tabindex="-1" role="dialog"
		aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="<%=contexturl %>DeleteComment" method="post" class="form-horizontal form-bordered"
						id="comment_deletion_form">
						<input name="forumId" id="forumId" type="hidden" value="${forum.forumId}" />
						<div class="row">
							<div class="col-xs-6"><spring:message
									code="validation.doyouwanttodeletethiscomment" /></div>
							<div  class="col-xs-6 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal">
									<spring:message code="label.no" />
								</button>
								<div id="delete_forumComment" class="btn btn-sm btn-green">
									<spring:message code="label.yes" />
								</div>
							</div>
						</div>
						<input type="hidden" name="id" id="deleteCommentId">
					</form>
				</div>
			</div>
		</div>
	</div>
</div>


<script src="${pageContext.request.contextPath}/webjars/counter-webapp-frontend/0.0.1/vendor_bundle.js"></script>
<%-- <script src="<%=contexturl%>resources/js/vendor/jquery-1.11.0.min.js"></script> --%>
<%-- <script src="<%=contexturl%>resources/js/vendor/bootstrap.min.js"></script> --%>
<%-- <script src="<%=contexturl %>resources/js/vendor/validate.js"></script> --%>

<script src="${pageContext.request.contextPath}/webjars/counter-webapp-frontend/0.0.1/app_bundle.js"></script>
<%-- <script src="<%=contexturl %>resources/js/additional-methods.js"></script> --%>

 <script type="text/javascript">
	$(document)
			.ready(
					function() {

						$("#Comment_form").validate(
								{
									errorClass:"help-block animation-slideDown",
									errorElement:"div",
									errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
									highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
									success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
																	rules : {
																		comments : {
																			required : !0,
																			maxlength : 255
																		}
																	},
																	messages : {
																		comments : {
																			required :'You cannot enter a blank comment',
																			maxlength :'Comment should not be more than 255 characters'
																		}		
																	},
															});
						
						
						$("#comment_submit").click(function() {
							unsaved=false;
							$("#Comment_form").submit();
						});
						
						$("#delete_forumComment").click(function(){
							$("#deleteCommentId").val(selectedId);
							$("#comment_deletion_form").submit();
						});
						
					});
	

</script>
<script>

function showCommentsDiv(){
	$("#forumCommentDiv").show();
}


<c:forEach items="${commentList}">
	function deleteComment(id){
		selectedId = id;
	}
</c:forEach>
</script>
 
 <%@include file="../../inc/page_footer.jsp"%>