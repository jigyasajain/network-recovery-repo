<%@page import="org.springframework.web.servlet.support.RequestContextUtils"%>
<%@page import="com.astrika.common.util.PropsValues"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@page import="com.astrika.kernel.exception.CustomException"%>
<%@page import="org.springframework.web.util.HtmlUtils"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%
	ApplicationContext ac = RequestContextUtils
			.getWebApplicationContext(request);
	PropsValues propvalue = (PropsValues) ac.getBean("propsValues");
	String contexturl = propvalue.CONTEXT_URL;
%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Atma Network</title>
<!-- Style -->
<!-- Bootstrap -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" media="screen"> 
<link href="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.5/jquery.bxslider.min.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/counter-webapp-frontend/0.0.1/style_bundle.css">

<link rel="shortcut icon" href="<%=contexturl %>resources/img/favicon.ico"> 
<%-- <link href="<%=contexturl %>resources/css/fontface.css" rel="stylesheet" media="screen"> --%>
<%-- <link href="<%=contexturl %>resources/css/login.css" rel="stylesheet" media="screen"> --%>

<%-- <!--[if lt IE 9]> --%>
<%--   <script src="js/html5shiv/3.7.0/html5shiv.js"></script> --%>
<%--   <script src="js/respond.js/1.4.2/respond.min.js"></script> --%>
<%--     <![endif]--> --%>
<!-- [if IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif] -->
<!-- SLIDER -->
</head>

<body>
	<div class="container">
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<div class="bg-white">
					<div class="text-center">

						<img src="<%=contexturl%>resources/img/atma.jpg" />
						<p>An Accelerator for Education</p>
					</div>
					<hr>
					<c:if test="${!empty success}">
						<div class="alert alert-success alert-dismissable">
							<button type="button" class="close" data-dismiss="alert"
								aria-hidden="true">x</button>
							<h4>
								<i class="fa fa-check-circle"></i> Success
							</h4>
							<spring:message code="${success}" />
						</div>
					</c:if>
					<c:if test="${ !empty errorCode}">
						<div class="alert alert-danger alert-dismissable">
							<button type="button" class="close" data-dismiss="alert"
								aria-hidden="true">x</button>
							<h4>
								<i class="fa fa-times-circle"></i> Error
							</h4>
							<spring:message code='${errorCode}' />
						</div>
					</c:if>
					<form action="<%=contexturl%>customLogin" method="post"
						id="candidate_login" class="mt10" autocomplete="off">
						<input type="hidden" name="client" value="WEB">
						<input type="hidden" name="url" value="<%=contexturl %>Login">
						<div class="form-group">
							<div>
								<label>Email </label> <input type="email" class="form-control"
									id="j_username" name="j_username">
							</div>
						</div>
						<div class="form-group">
							<div>
								<label>Password </label> <input type="password"
									class="form-control" id="j_password" name="j_password">
							</div>
						</div>
						<div class="form-group">
							<p class="pull-left">
								<a href="ForgotPassword">Forgot password ?</a>
							</p>
							<button type="submit" id="loginSubmit" class="btn pull-right">Login</button>
							<span class="clearfix"></span>
						</div>
						<div class="blue-box">
							<div class="text-center">
								Not a member? <a href="Register"><strong>Signup now</strong></a>
							</div>
						</div>
					</form>
				</div>
			</div>

			<div class="col-md-4"></div>
		</div>
	</div>
</body>

<%-- <script src="<%=contexturl %>resources/js/vendor/jquery-1.11.0.min.js"></script> --%>
<%-- <script src="<%=contexturl %>resources/js/vendor/bootstrap.min.js"></script> --%>
<%-- <script src="<%=contexturl %>resources/js/vendor/validate.js"></script> --%>
<script src="${pageContext.request.contextPath}/webjars/counter-webapp-frontend/0.0.1/vendor_bundle.js"></script>
<script src="${pageContext.request.contextPath}/webjars/counter-webapp-frontend/0.0.1/app_bundle.js"></script>
<!-- <script type="text/javascript"> 
// 	$(document)
// 			.ready(
// 					function() {
// 						$("#candidate_login").validate(
// 										{
// 											errorClass:"help-block animation-slideDown",
// 											errorElement:"div",
// 											errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
// 											highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
// 											success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
// 											rules : {
// 												j_username : {
// 													required : !0
// 												},
// 												j_password : {
// 													required : !0
// 												}
// 											},
// 											messages : {
// 												j_username : {
// 													required : "Please enter your login Id"
// 												},
// 												j_password : {
// 													required : "Please enter a password"
// 												}
// 											}
// 										});
// 					});
</script> -->

	<div class="container" id="footer">
		<%@include file="../inc/footer_items.jsp"%>
	</div>
</html>
