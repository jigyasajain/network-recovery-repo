<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>


<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i>
						<spring:message code="label.error" />
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>

			<h1 class="text-center">
				<strong>${forum.title}</strong><br> <small>${forum.tag}</small>
			</h1>

			<span id="errorMsg"></span>
		</div>
	</div>
	<form action="<%=contexturl%>SaveForumComment" method="post"
		class="form-horizontal ui-formwizard" id="Forum_form"
		enctype="multipart/form-data">
		<input name="id" value="${forum.forumId}" type="hidden"> <input
			name="forumtitle" value="${forum.title}" type="hidden">
		<div class="row">

			<div class="">
				<div class="block">
					<input  id="act" type="hidden" name="act"> 
					<input id = "commentId" name="commentId" type="hidden">
					<div class="form-group">

						<div class="col-md-6">
							<textarea id="forumComment" name="forumComment"
								class="form-control" placeholder="Comment.."></textarea>
						</div>
						<button id="add_comment" type="submit"
							class="btn btn-sm btn-primary save">
							<i class="fa fa-angle-right"></i>
							<spring:message code="button.addcomment" />
						</button>
					</div>

					<c:forEach var="listOfComments" items="${commentList}"
						varStatus="counter">
					
						<div class="row">
							<div class="form-group">
								<div class="col-md-1">
									<img width="45" height="45"
										src="<%=contexturl%>resources/img/application/default_profile.png"
										class="avatar" title="leo">
								</div>


								<div class="col-md-6">
									<span class="username">${listOfComments.createdBy.fullName}</span>

									<p>${listOfComments.comments}</p>
									<c:choose>
										<c:when test="${!empty listOfComments.comment}">
											<c:out value="ns"></c:out>
										</c:when>
										<c:otherwise>
											<div class="col-md-1">
												<button id="replyButton_${counter.count}" type="button"
													class="btn btn-sm btn-primary save">
													<i class="fa fa-angle-right"></i> Reply
												</button>
											</div>
										</c:otherwise>
									</c:choose>
								</div>

								<div class="col-md-6" id="reply_${counter.count}"
									style="display: none">

									<input id="reply_${counter.count}" name="reply"
										class="form-control" placeholder="Reply.." type="text">
								</div>
								<div class="col-md-9" id="commentreplyButton_${counter.count}"
									style="display: none">
									<button id="add_reply_${counter.count}" type="submit"
										class="btn btn-sm btn-primary save">
										<i class="fa fa-angle-right"></i>
										<spring:message code="button.addreply" />
									</button>
								</div>
							</div>
						</div>
					</c:forEach>


				</div>
			</div>
		</div>
	</form>
</div>
<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>

<%-- <script src="<%=contexturl%>resources/js/custom/featurecontrol.js"></script> --%>
<script src="${pageContext.request.contextPath}/webjars/counter-webapp-frontend/0.0.1/app_bundle.js"></script>

<%@include file="../../inc/chosen_scripts.jsp"%>
<script type="text/javascript">
	
	$(document).ready(function() {

		$("#add_comment").click(function() {
			unsaved = false;
			$("#act").val("0");
			$("#Forum_form").submit();
		});

		<c:forEach var="listOfComments" items="${commentList}" varStatus = "counter">
			$("#add_reply_${counter.count}").click(function() {
				unsaved = false;
				$("#act").val("1");
				$("#commentId").val("${listOfComments.commentId}");
				$("#Forum_form").submit();
			});
		</c:forEach>

	});
</script>
<script>
<c:forEach var="listOfComments" items="${commentList}" varStatus = "counter">
		$("#replyButton_${counter.count}").click(function(){
			$('#reply_${counter.count}').css("display", "block");
			$('#commentreplyButton_${counter.count}').css("display", "block");
		});
</c:forEach>
</script>

<style>
.chosen-container {
	width: 250px !important;
}

#map-canvas {
	height: 300px
}
</style>
<%@include file="../../inc/template_end.jsp"%>