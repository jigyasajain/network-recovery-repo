<%@page import="com.astrika.common.model.Role"%>
<%@page import="com.astrika.common.model.User"%>



<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/page_head.jsp"%>

  <div class="blue-container">
            <div class="container">
             <h3 class="atma-breadcrumbs">
			<security:authorize ifAnyGranted="<%=Role.COMPANY_ADMIN.name()%>">
				<a class="light a-breadcrumb" href="<%=contexturl%>CompanyAdmin">Dashboard</a>
			</security:authorize>
			<security:authorize ifAnyGranted="<%=Role.MODULE_USER.name()%>">
				<a class="light a-breadcrumb" href="<%=contexturl%>ModuleUser">Dashboard</a>
			</security:authorize>
			</h3>
            
                <div class="row blue">
                    <div class="col-md-10">
                        <div class="media">
                            <c:choose>
                        		<c:when test = "${(user.profileImage!=null) && (not empty user.profileImage)}">
                        			<a class="pull-left" href="#"><img class="media-object pro-pic" height = 90px width=75px src="<%=contexturl %>${user.profileImage.imagePath}" alt="<%=contexturl %>resources/img/User_Thumbs.png" title="Click here to change photo" onclick="OpenFileDialog();">
                            		</a>
                        		</c:when>
                        		<c:otherwise>
                        			<a class="pull-left" href="#"><img class="media-object pro-pic" src="<%=contexturl %>resources/img/User_Thumbs.png" alt="Organisation Name" title="Click here to change photo" onclick="OpenFileDialog();">
                            		</a>
                        		</c:otherwise>
                        	</c:choose>
                            <div class="media-body text-st">
                                <p>Edit Organisational Details Here</p>
                                <h4 class="media-heading">Organisational Details</h4>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
<br>
<div class="container">
	<div class="row">
		<c:if test="${!empty success}">
			<div class="alert alert-success alert-dismissable">
				<button type="button" class="close" data-dismiss="alert"
					aria-hidden="true">x</button>
				<h4>
					<i class="fa fa-check-circle"></i>
					<spring:message code="label.success" />
				</h4>
				<spring:message code="${success}" />
			</div>
		</c:if>
		<div class="alert alert-danger alert-dismissable"
			style="display: none" id="error">
			<button type="button" class="close" onclick="closeDiv();"
				aria-hidden="true">x</button>
			<h4>
				<i class="fa fa-times-circle"></i>
				<spring:message code="label.error" />
			</h4>
			<span class="errorMsg"></span>
		</div>
	</div>
	<br>
	<div class="row">
	<c:if test="${not adminEdit}">
		<div class="col-md-2"></div>
		<div class="col-md-4">
			<button class="btn btn-default btn-sm btn-block"
				onclick="location.href='<%=contexturl %>EditPersonalDetails/'+${user.userId}">Personal
				Details</button>
		</div>
		<div class="col-md-4">
			<button class="btn btn-sm btn-block">Organisation Details</button>
		</div>
		<div class="col-md-2"></div>
	</c:if>
	<c:if test="${adminEdit}">
    		<div>
    			<button class="btn btn-sm btn-block">Organisation Details</button>
    		</div>
    </c:if>
		<br> <br> <br>
	</div>
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="fuelux">
				<div class="wizard" id="MyWizard">
					<ul class="steps">
						<li style="width: 30%" class="active" data-target="#step1"><span
							class="badge badge-info">1</span>Step<span class="chevron"></span>
						</li>
						<li style="width: 40%" data-target="#step2"><span
							class="badge">2</span>Step<span class="chevron"></span></li>
						<li style="width: 30%" data-target="#step3"><span
							class="badge">3</span> Step</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-md-2"></div>
	</div>
	<div class="row">
		<form action="<%=contexturl%>SaveOrganisationalDetailsStep1"
			method="post" class="form-horizontal " id="step1_form"
			enctype="multipart/form-data">
			<input name="userId" value="${user.userId}" type="hidden"> <input
				name="companyId" value="${company.companyId}" type="hidden">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<div class="block full">
					<div class="block-title">
						<h4 class="">Company Details</h4>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">NGO Name <span
							class="mandatory">*</span>
						</label>
						<div class="col-sm-8">
							<input id="ngo_Name" name="ngoName" type="text"
								class="form-control" placeholder="NGO Name *"
								value="${company.companyName}" disabled="disabled">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Field/Sector or Work
							<span class="mandatory">*</span>
						</label>
						<div class="col-sm-8">
							<input id="sector_work" name="sectorWork" type="text"
								class="form-control" placeholder="Field/Sector or Work*"
								value="${company.sectorWork}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Address Line1</label>
						<div class="col-sm-8">
							<input id="ngoAddressLine1" name="ngoAddressLine1" type="text"
								class="form-control" placeholder="Address Line1"
								value="${company.companyAddressLine1}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Address Line2</label>
						<div class="col-sm-8">
							<input id="ngoAddressLine2" name="ngoAddressLine2" type="text"
								class="form-control" placeholder="Address Line2"
								value="${company.companyAddressLine2}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Address Line3</label>
						<div class="col-sm-8">
							<input id="ngoAddressLine3" name="ngoAddressLine3" type="text"
								class="form-control" placeholder="Address Line3"
								value="${company.companyAddressLine3}">
						</div>
					</div>
					<div class="form-group" id="countryDiv">
						<label class="col-sm-3 control-label">Country Name<span
							class="mandatory">*</span>
						</label>

						<div class="col-sm-8">
							<select class=" form-control" id="countrydiv" name="countryDiv">
								<option value=""></option>
								<c:forEach items="${countryList}" var="country">
									<option value="${country.countryId}"
										<c:if test="${country.countryId eq company.country.countryId}">selected="selected"</c:if>>${country.countryName}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="form-group" id="stateDiv">
						<label class="col-sm-3 control-label">State Name<span
							class="mandatory">*</span>
						</label>
						<div class="col-sm-8">
							<select class=" form-control" id="statediv" name="stateDiv">
								<option value=""></option>
								<c:forEach items="${stateList}" var="state">
									<option value="${state.stateId}"
										<c:if test="${state.stateId eq company.state.stateId}">selected="selected"</c:if>>${state.stateName}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="form-group" id="cityDiv">
						<label class="col-sm-3 control-label">City Name<span
							class="mandatory">*</span>
						</label>
						<div class="col-sm-8">
							<select class=" form-control" id="citydiv" name="cityDiv">
								<option value=""></option>
								<c:forEach items="${cityList}" var="city">
									<option value="${city.cityId}"
										<c:if test="${city.cityId eq company.city.cityId}">selected="selected"</c:if>>${city.cityName}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Pincode<span
							class="mandatory">*</span>
						</label>
						<div class="col-sm-8">
							<input id="ngoPincode" name="ngoPincode" type="text"
								class="form-control" placeholder="Pincode"
								value="${company.companyPincode}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Telephone</label>
						<div class="col-sm-8">
							<input id="ngoPhone" name="ngoPhone" type="text"
								class="form-control" placeholder="Telephone"
								value="${company.companyTelephone1}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">NGO Email<span
							class="mandatory">*</span>
						</label>
						<div class="col-sm-8">
							<input id="ngo_email" name="ngoEmail" type="email"
								class="form-control" placeholder="NGO Email"
								value="${company.companyEmail}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">NGO Website<span
							class="mandatory">*</span>
						</label>
						<div class="col-sm-8">
							<input id="ngo_website" name="ngoWebsite" type="text"
								class="form-control" placeholder="e.g. http://www.google.com"
								value="${company.companyWebsite}">
							<span class="help-block">The website url should start with http</span>
						</div>
					</div>
				</div>
				<!-- </div>
                <div class="col-md-5">-->
				<div class="block full">
					<div class="block-title">
						<h4 class="">About Organisation</h4>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label">Organisation Vision<span
							class="mandatory">*</span>
						</label>
						<div class="col-sm-8">
							<textarea id="organisationVision" name="organisationVision"
								class="form-control" rows="5" placeholder="Organisation Vision">${company.organisationVision}</textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label">Organisation Mission<span
							class="mandatory">*</span>
						</label>
						<div class="col-sm-8">
							<textarea id="organisationMission" name="organisationMission"
								class="form-control" rows="5" placeholder="Organisation Mission">${company.organisationMission}</textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label">3 sentences
							describing what your organisation does.<span class="mandatory">*</span>
						</label>
						<div class="col-sm-8">
							<textarea id="organisationDescription"
								name="organisationDescription" class="form-control" rows="5"
								placeholder="Orginasation Discription">${company.organisationDescription}</textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-8 control-label">Is your organisation
							presently or has it in the past recieved organisational
							development assistance from any other agency?<span
							class="mandatory">*</span>
						</label>
						<div class="col-sm-4">
							<c:choose>
								<c:when test="${not empty company.assitanceFromOtherAgency}">
									<label class="radio"> <input
										id="assistance_otheragency" name="assitanceFromOtherAgency"
										type="radio" value="1"
										<c:if test="${company.assitanceFromOtherAgency eq true}"> checked = "checked"</c:if>><span
										class="input-text">Yes</span>
									</label>
									<label class="radio"> <input
										id="assistance_otheragency" name="assitanceFromOtherAgency"
										type="radio" value="0"
										<c:if test="${company.assitanceFromOtherAgency eq false}"> checked = "checked"</c:if>><span
										class="input-text">No</span>
									</label>
								</c:when>
								<c:otherwise>
									<label class="radio"> <input
										id="assistance_otheragency" type="radio"
										name="assitanceFromOtherAgency" value="1" /> <span
										class="input-text">Yes</span>
									</label>
									<label class="radio"> <input
										id="assistance_otheragency" type="radio"
										name="assitanceFromOtherAgency" value="0" checked="checked" />
										<span class="input-text">No</span>
									</label>
								</c:otherwise>
							</c:choose>

						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-8 control-label">Please Indicate the
							Legal Type of Registration<span class="mandatory">*</span>
						</label>
						<div class="col-sm-4">
							<c:choose>
								<c:when test="${not empty company.legalType}">
									<label class="radio"> <input id="legalType"
										name="legalType" type="radio" value="Societies Reg.Act 1860"
										<c:if test="${company.legalType == 'Societies Reg.Act 1860'}"> checked = "checked"</c:if> />
										<span class="input-text">Societies Reg. Act 1860</span>
									</label>
									<label class="radio"> <input id="legalType"
										name="legalType" type="radio" value="Public Trust Act"
										<c:if test="${company.legalType == 'Public Trust Act'}"> checked = "checked"</c:if> />
										<span class="input-text">Public Trust Act</span>
									</label>
									<label class="radio"> <input id="legalType"
										name="legalType" type="radio" value="Section 25 Company"
										<c:if test="${company.legalType == 'Section 25 Company'}"> checked = "checked"</c:if> />
										<span class="input-text">Section 25 Company</span>
									</label>
									<label class="radio"> <input id="legalType"
										name="legalType" type="radio" value="Private Limited Company"
										<c:if test="${company.legalType == 'Private Limited Company'}"> checked = "checked"</c:if> />
										<span class="input-text">Private Limited Company</span>
									</label>
								</c:when>
								<c:otherwise>
									<label class="radio"> <input id="legalType"
										name="legalType" type="radio" value="Societies Reg.Act 1860" />
										<span class="input-text">Societies Reg. Act 1860</span>
									</label>
									<label class="radio"> <input id="legalType"
										name="legalType" type="radio" value="Public Trust Act" /> <span
										class="input-text">Public Trust Act</span>
									</label>
									<label class="radio"> <input id="legalType"
										name="legalType" type="radio" value="Section 25 Company"
										/> <span class="input-text">Section
											25 Company</span>
									</label>
									<label class="radio"> <input id="legalType"
										name="legalType" type="radio" value="Private Limited Company"
										checked="checked" /> <span class="input-text">Private Limited Company</span>
									</label>
								</c:otherwise>
							</c:choose>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-8 control-label">FCRA Registration<span
							class="mandatory">*</span>
						</label>
						<div class="col-sm-4">
							<c:choose>
								<c:when test="${not empty company.fcraRegistarion}">
									<label class="radio"> <input id="fcra_registration"
										name="fcraRegistration" type="radio" value="1"
										<c:if test="${company.fcraRegistarion eq true}"> checked = "checked"</c:if>><span
										class="input-text">Yes</span>
									</label>
									<label class="radio"> <input id="fcra_registration"
										name="fcraRegistration" type="radio" value="0"
										<c:if test="${company.fcraRegistarion eq false}"> checked = "checked"</c:if>><span
										class="input-text">No</span>
									</label>
								</c:when>
								<c:otherwise>
									<label class="radio"> <input id="fcra_registration"
										name="fcraRegistration" type="radio" value="1" /> <span
										class="input-text">Yes</span>
									</label>
									<label class="radio"> <input id="fcra_registration"
										name="fcraRegistration" type="radio" value="0"
										checked="checked" /> <span class="input-text">No</span>
									</label>
								</c:otherwise>
							</c:choose>
						</div>
					</div>
				</div>
				<div class="block full">
					<div class="block-title">
						<h4 class="">Organisation Details</h4>
					</div>
					<div class="form-group" id="educationTypeDiv">
						<label class="col-sm-4 control-label">Type of Organisation<span
								class="mandatory">*</span>
							
						</label>
						<div class="col-sm-8" id="subFunctionDiv4">
							<select class=" form-control subfunctionChosen"
								id="typeOfEducation" name="typeOfEducation">
								<option value=""></option>
								<c:forEach items="${result3}" var="EType">
									<option value="${EType.educationtype}"
										 <c:if test="${EType.educationtype eq company.typeOfEducation}">selected="selected"</c:if>>${EType.educationtype}
								</option>
								</c:forEach>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-2"></div>
		</form>
	</div>
</div>
<div class="container">
        <div class="row">
            <div class="text-center">
            	<c:if test="${not adminEdit}">
            		<button type="button" class="btn btn-primary" onclick="location.href='<%=contexturl %>EditPersonalDetails/'+${user.userId}">Back </button>
            	</c:if>
                <button id = "step1" type="submit" class="btn btn-warning">Submit this page</button>
                <c:if test="${not adminEdit}">
                	<button type="button" class="btn btn-primary" onclick="location.href='<%=contexturl %>EditOrganisationalDetailsStep2/'+${user.userId}">Next </button>
                </c:if>
                <c:if test="${adminEdit}">
                	<button type="button" class="btn btn-primary" onclick="location.href='<%=contexturl %>EditOrganisationalDetailsByAdmin/step/2/company/${company.companyId}'">Next </button>
                </c:if>
                <c:if test="${adminEdit}">
               		<button type="button" class="btn btn-info" onclick="location.href='<%=contexturl %>ManageAdmin/OrganizationList'">Back To Organisation List </button>
                </c:if>
            </div>
            <br>
            <br>
            <br>
        </div>
    </div>

<%-- <script src="<%=contexturl%>resources/js/vendor/jquery-1.11.0.min.js"></script> --%>
<%-- <script src="<%=contexturl%>resources/js/vendor/bootstrap.min.js"></script> --%>
<%-- <script src="<%=contexturl%>resources/js/vendor/validate.js"></script> --%>
<script src="${pageContext.request.contextPath}/webjars/counter-webapp-frontend/0.0.1/vendor_bundle.js"></script>

<%-- <script src="<%=contexturl%>resources/js/additional-methods.js"></script> --%>
<script src="${pageContext.request.contextPath}/webjars/counter-webapp-frontend/0.0.1/app_bundle.js"></script>

 <script type="text/javascript">
	$(document)
			.ready(
					function() {
						
						$('select[name="countryDiv"]').change( function() {
							var countryId = ($(this).val());
							if(countryId == ""){
								$("#statediv").html("");
								$("#citydiv").html("");
							}
							else{
								$.ajax({
									type: "GET",
									async: false,
									url: "<%=contexturl%>state/statebycountryid/"+countryId,
									success: function(data, textStatus, jqXHR){
										var obj = jQuery.parseJSON(data);
										var len=obj.length;
										var myOptions = "<option value></option>";
										var myCityOptions = "<option value></option>";
										for(var i=0; i<len; i++){
										myOptions += '<option value="'+obj[i].stateId+'">'+obj[i].stateName+'</option>';
										}
										$("#statediv").html(myOptions);
										$("#citydiv").html(myCityOptions);
									},
									dataType: 'html'
								});
							}
							
						});
						
						
						$('select[name="stateDiv"]').change( function() {
							var stateId = ($(this).val());
							$.ajax({
								type: "GET",
								async: false,
								url: "<%=contexturl %>city/citybystateid/"+stateId,
								success: function(data, textStatus, jqXHR){
									var obj = jQuery.parseJSON(data);
									var len=obj.length;
									var myOptions = "<option value></option>";
									for(var i=0; i<len; i++){
									myOptions += '<option value="'+obj[i].cityId+'">'+obj[i].cityName+'</option>';
									}
									$("#citydiv").html(myOptions);
								},
								dataType: 'html'
							});
							
							
						});	
						
						$("#step1_form").validate(
								{
									errorClass:"help-block animation-slideDown",
									errorElement:"div",
									errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
									highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
									success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
									rules : {
										ngoName : {
											required : !0,
											maxlength : 75
										},
										sectorWork : {
											required : !0,
											maxlength : 75
										},
										ngoAddressLine1 : {
											maxlength : 75
										},
										ngoAddressLine2 : {
											maxlength : 75
										},
										ngoAddressLine3 : {
											maxlength : 75
										},

										cityDiv : {
											required : !0
										},
										stateDiv : {
											required : !0
										},

										countryDiv : {
											required : !0
										},
										typeOfEducation : {
											required : !0
										},
										ngoPincode : {
											required : !0,
											maxlength : 10,
											digits : !0,
											pincode : !0
										},
										ngoPhone : {
											phone : !0
										},
										ngoEmail : {
											required : !0,
											maxlength : 75,
											email : !0
										},
										ngoWebsite : {
											required : !0,
											url : true,
											maxlength : 500
										},
										
										organisationVision : {
											maxlength : 500,
											required : !0
										},
										organisationMission : {
											maxlength : 500,
											required : !0
										},
										organisationDescription : {
											maxlength : 255,
											required : !0
										},
										legalType : {
											required : !0
										},
									},
									messages : {
										ngoName : {
											required : 'Please enter a NGO Name',
											maxlength : 'NGO name should not be more than 75 characters'
										},
										sectorWork : {
											required : 'Please enter a Field/Sector of Work',
											maxlength : 'Field/Sector of Work should not be more than 75 characters'
										},

										ngoAddressLine1 : {

											maxlength : '<spring:message code="validation.addressline1"/>'
										},
										ngoAddressLine2 : {

											maxlength : '<spring:message code="validation.addressline1"/>'
										},
										ngoAddressLine3 : {

											maxlength : '<spring:message code="validation.addressline1"/>'
										},
										cityDiv : {
											required : '<spring:message code="validation.selectcity"/>'
										},
										stateDiv : {
											required : '<spring:message code="validation.selectstate"/>'
										},
										countryDiv : {
											required : '<spring:message code="validation.selectcountry"/>'
										},
										typeOfEducation : {
											required : '<spring:message code="validation.typeOfEducation"/>'
										},
										ngoPincode : {
											required : '<spring:message code="validation.pleaseenterpincode"/>',
											maxlength : '<spring:message code="validation.pincode10character"/>',
											digits : '<spring:message code="validation.digits"/>'
										},
										
										ngoPhone : {

											phone : '<spring:message code="validation.phone"/>'
										},
										ngoEmail : {
											required : '<spring:message code="validation.pleaseenteremailid"/>',
											email : '<spring:message code="validation.pleaseenteravalidemailid"/>',
											maxlength : '<spring:message code="validation.email75character"/>'
										},
										ngoWebsite : {
											required : 'Please enter a company website',
											url : '<spring:message code="validation.pleaseenteravalidurl"/>',
											maxlength : 'Link should not be more than 500 characters'
										},
										
										organisationVision : {
											maxlength : 'Vision should not be more than 500 characters',
											required : 'Please enter Organisation Vision'
										},
										organisationMission : {
											maxlength : 'Mission should not be more than 500 characters',
											required : 'Please enter Organisation Mission'
										},
										organisationDescription : {
											maxlength : 'Description should not be more than 255 characters',
											required : 'Please enter Organisation Description'
										},
										legalType : {
											required : 'Please select a Legal Type',
										},
									},
								});
						
						jQuery.validator.addMethod("phone", function (phone_number, element) {
					         //phone_number = phone_number.replace(/\s+/g, "");
					        // return this.optional(element) || phone_number.length >= 8 && phone_number.length < 25 && phone_number.match(/^[0-9\+\-]*$/);
					         return this.optional(element) || phone_number.length >= 6 && phone_number.length < 12 && phone_number.match(/^[0-9][0-9]{5,10}$/);

					     }, "Please specify a valid phone number");
					    
					 jQuery.validator.addMethod("mobile", function (mobile_number, element) {
					      mobile_number = mobile_number.replace(/\s+/g, "");
					       if (mobile_number.match(/^[7-9][0-9]{9}$/)) return true;
					         //return this.optional(element) || mobile_number.length >= 10 && mobile_number.length < 25 && mobile_number.match(/^[0-9\+\-]*$/);
					     }, "Please specify a valid mobile number");
					    
					    jQuery.validator.addMethod("pincode", function (pincode, element) {
					    	pincode = pincode.replace(/\s+/g, "");
					        return this.optional(element) || pincode.length >=4 && pincode.length <= 10;
					    }, "Please specify a valid pincode");
											
						
						$("#step1").click(function() {
							unsaved=false;
							$("#step1_form").submit();
						});
						
						
						
					});
	

</script>

 <%@include file="../../inc/page_footer.jsp"%>
