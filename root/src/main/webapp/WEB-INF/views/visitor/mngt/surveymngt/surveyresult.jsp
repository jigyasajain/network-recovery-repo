
<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/page_head.jsp"%>

  <div class="blue-container">
            <div class="container">
                <h3 class="atma-breadcrumbs">
                	<security:authorize ifAnyGranted="<%=Role.COMPANY_ADMIN.name()%>">
						<a class="light a-breadcrumb" href="<%=contexturl %>CompanyAdmin">Dashboard</a> &gt; <a class="light a-breadcrumb" href="">Survey Result</a>
					</security:authorize>
					<security:authorize ifAnyGranted="<%=Role.MODULE_USER.name()%>">
						<a class="light a-breadcrumb" href="<%=contexturl %>ModuleUser">Dashboard</a> &gt; <a class="light a-breadcrumb" href="">Survey Result</a>
					</security:authorize>
                
                </h3>
                <div class="row blue">
                    <div class="col-md-12  col-sm-12">
                        <div class="media">
                            <div class="contain">
                               <p class="text-white">An organisation's level of growth, strengths, and challenges can be generally defined into <strong>5 Life Stages</strong>.<br>
                               	Using this framework you can think of your organisation as a person. At each stage of life we have different health needs, different abilities, and different areas of difficulty. For example a young child is very curious and energetic, but is not autonomous and may not yet know how to play by the rules. Organisations grow in a similar way: 
                               <div class="row">
                               	<div class="col-md-2 text-center"><strong>Stage 1: Idea phase- Conception and Gestation.</strong></div>
                               	<div class="col-md-2 text-center"><strong>Stage 2: Start-up phase- Birth and Childhood.</strong></div>
                               	<div class="col-md-2 text-center"><strong>Stage 3: Organising and growth phase- Adolescence and Young Adulthood.</strong></div>
                               	<div class="col-md-2 text-center"><strong>Stage 4: Production and Momentum phase- Young to Middle Adulthood.</strong></div>
                               	<div class="col-md-2 text-center"><strong>Stage 5: Review and Change phase- Middle Age to Seniority.</strong></div>
                               </div>
                               </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
  </div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-9 col-sm-9">
			<div class="row">
				<h4>Understanding Your Results</h4>
				<p>For each Organisational Area you will be given a profile
					including:</p>
				<div class="row">
					<ul>
						<li><strong>Your Life Stage Number</strong> in each area</li>
						<li><strong>Where You Are Now:</strong> A broad description
							of your present situation, your strengths and areas to grow</li>
						<li><strong>Path Ahead:</strong> The recommended steps to
							take and areas to focus on to help your organisation develop in
							this area</li>
						<li><strong>Network Resources:</strong> Specific sections and
							modules in the Network for you to refer to for targeted guidance
							in each area. These are based on best practices and Atma's
							experience and methods. Resources include:
							<ul>
								<li><strong>Blueprints</strong> (step by step guides)</li>
								<li><strong>Tools</strong> (Templates, Checklists, Surveys
									and Assessments)</li>
								<li><strong>Examples</strong> from Atma Partner
									organisations</li>
								<li><strong>Readings</strong> compiled from expert sources</li>
							</ul></li>
					</ul>
				</div>

				<h4 class="text-center">Your Results</h4>
				<ul>
					<c:forEach items="${surveyResultList}" var="surveyResult" varStatus="counter">
						<li><strong>${surveyResult.module.moduleName}: Organisational ${surveyResult.lifeStages.name}</strong>
							<ul>
								<li>${resultSetList[counter.index].resultdata}</li>
							</ul>
						</li>
					</c:forEach>
				</ul>
			</div>
		</div>

		<div class="col-md-3 col-sm-3 hidden-xs">
			<div class="adv"><img class="img-responsive" src="<%=contexturl %>resources/img/AtmaNetwork.jpg" /></div>
			<c:choose>
					<c:when test = "${empty atmaContactImage.contactImage}">
						 <div class="need-help"><img class="img-responsive" src="<%=contexturl %>resources/img/need help-3.jpg" /><br><br></div>
					</c:when>
					<c:otherwise>
						 <div class="need-help"><img class="img-responsive" src="<%=contexturl %>${atmaContactImage.contactImage.imagePath}" /><br><br></div>
					</c:otherwise>
			</c:choose>
		</div>
	</div>
</div>

<div class="container">
        <div class="row">
        </div>
</div>
<%-- <script src="<%=contexturl%>resources/js/vendor/jquery-1.11.0.min.js"></script> --%>
<%-- <script src="<%=contexturl%>resources/js/vendor/bootstrap.min.js"></script> --%>
<script src="${pageContext.request.contextPath}/webjars/counter-webapp-frontend/0.0.1/vendor_bundle.js"></script>

 <%@include file="../../inc/page_footer.jsp"%>

