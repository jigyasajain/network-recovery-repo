<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<%@include file="../inc/config.jsp"%>
<%@include file="../inc/template_start.jsp"%>
<%@include file="../inc/page_head.jsp"%>
<style type="text/css">
        .db-icon {
            padding: 10px 5px;
        }
        .db-icon img {
            border: thin solid #ccc;
        }
        .db-icon a:hover{
            opacity: .5;
        }
 </style>
 
 <style type="text/css">
	#h1 {
		text-align:center;
	}
</style>

<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>

			<h1 class = "text-center">
				<strong>${module.moduleName}</strong>
				
			</h1>
			<a href="<%=contexturl %>ModuleUser">Back</a>
			
			<span id="errorMsg"></span>
		</div>
	</div>

                <!-- END Statistics Widgets Header -->
		<div class="row">
                	<c:forEach var="currentcategoryUser" items="${moduleSpecificList}"
						varStatus="count">
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <!-- Widget -->
                        <div class="widget">
                            <div class="widget-extra">

                            </div>
                            <div class="widget-extra themed-background-spring text-center">
                             <a href="<%=contexturl %>AddComment?id=${currentcategoryUser.categoryId}">
<%--                                <input type="hidden" id="categoryId" value="${currentcategoryUser.categoryId}"/> --%>
                                <h4 class="widget-content-light"><strong>${currentcategoryUser.categoryName}</strong></h4></a>
                            </div>
                             <p>${currentcategoryUser.categoryDescription}</p>
                        </div>
                        <!-- END Widget -->
                    </div>
				</c:forEach>
        </div>

	<div class="row">
	
		<dl>
			<c:forEach var="currentforum" items="${forums}">
				  <a href="<%=contexturl %>Forums/${currentforum.title}?id=${currentforum.forumId}"><dd>${currentforum.title}</dd></a>
			</c:forEach>
		</dl>

	</div>



	<div class="row">
        		<a class="btn btn-sm btn-primary save" href="<%=contexturl %>AddForum?id=${module.moduleId}"><i class="gi gi-remove"></i>
						Add forum</a>
        </div>
        
</div>


<%@include file="../inc/page_footer.jsp"%>

<!-- Remember to include excanvas for IE8 chart support -->
<!--[if IE 8]><script src="<%=contexturl%>resources/js/helpers/excanvas.min.js"></script><![endif]-->

<%@include file="../inc/template_scripts.jsp"%>

<!-- Google Maps API + Gmaps Plugin, must be loaded in the page you would like to use maps (Remove 'http:' if you have SSL) -->
<!-- <script src="http://maps.google.com/maps/api/js?sensor=true"></script> -->
<%-- <script src="<%=contexturl%>resources/js/helpers/gmaps.min.js"></script> --%>

<!-- <!-- Load and execute javascript code used only in this page --> 
<%-- <script src="<%=contexturl%>resources/js/pages/index.js"></script> --%>
<!-- <script>$(function(){ Index.init(); });</script> -->



<%@include file="../inc/template_end.jsp"%>