<%@page import="com.astrika.common.model.Role"%>
<%@page import="com.astrika.common.model.User"%>

<%@include file="/WEB-INF/views/admin/inc/config.jsp"%>
<%-- <%@include file="../../inc/config.jsp"%> --%>
<%@include file="../../inc/page_head.jsp"%>

<div class="blue-container">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="atma-breadcrumbs">
					<security:authorize ifAnyGranted="<%=Role.COMPANY_ADMIN.name()%>">
						<a class="light a-breadcrumb" href="<%=contexturl %>CompanyAdmin">Dashboard</a> &gt;<a class="light a-breadcrumb">${module.moduleName}</a> 
					</security:authorize>
					<security:authorize ifAnyGranted="<%=Role.MODULE_USER.name()%>">
						<a class="light a-breadcrumb" href="<%=contexturl %>ModuleUser">Dashboard</a> &gt;<a class="light a-breadcrumb">${module.moduleName}</a> 
					</security:authorize>
					
				</h3>
				<div class="blue">
					<div class="row">
						<c:if test="${myModuleLifeStage != 0}">
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-6 col-sm-6 col-xs-6">
									<div id="progress"
										style="width: 75px; height: 75px; float: left;"></div>
									<div class="lifestage-status pull-left">
										My Module <br>Lifestage
									</div>
									<span class="clearfix"></span>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-6">
									<div id="progress2"
										style="width: 75px; height: 75px; float: left;"></div>
									<div class="lifestage-status pull-left">
										Average <br>Lifestage
									</div>
									<span class="clearfix"></span>
								</div>
							</div>
						</div>
						</c:if>
						<c:if test="${not empty nextSteps}">
						<div class="col-md-6">
							<div class="lifestage-status">Next Steps</div>
							<div class="steps">
								<div id="slider2" class="steps-slider">
								<c:forEach var = "nextStep" items = "${nextSteps}" varStatus = "counter">
									<c:if test="${not empty nextStep.step}">
										<c:choose>
											<c:when test="${not empty nextStep.category}">
												<div class="slide">
													<a href="<%=contexturl %>Projects?id=${nextStep.category.categoryId}">
														<p>${nextStep.step}</p>
													</a>
												</div>
											</c:when>
											<c:otherwise>
												<div class="slide">													
														<p>${nextStep.step}</p>													
												</div>
											</c:otherwise>
										</c:choose>												
									</c:if>
								</c:forEach>
								</div>
							</div>
						</div>
						</c:if>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
</div>
<br>
   <div class="container">
	<div class="row">
		<div class="col-md-9">
			<c:if test="${!empty error}">
					<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert"
							aria-hidden="true">x</button>
						<h4>
							<i class="fa fa-times-circle"></i>
							Error
						</h4>
						<spring:message code="${error}" />
					</div>
				</c:if>
		</div>
	</div>
	<br>
        <div class="row">
            <div class="col-md-9 col-sm-9">
				<c:forEach var="moduleSpecificCategoryList"
					items="${moduleSpecificList}" varStatus="count">
					<div class="row">
						<div class="col-md-12">
							<a href="<%=contexturl %>Projects?id=${moduleSpecificCategoryList.categoryId}">
								<h3 class="light text-blue">${moduleSpecificCategoryList.categoryName}</h3>
							</a>
							<p>${moduleSpecificCategoryList.categorySummary}</p>
						</div>
					</div>
				</c:forEach>
			<div class="row">
				<div class="block full">
					<div class="block-title forum">
						<div class="forum">
							Forum <a href="#" class="pull-right btn btn-xs btn-default" onclick = "showForumDiv();">Start
								New Forum</a>
						</div>
					</div>
					<form action="<%=contexturl%>SaveForum" method="post" class="form-horizontal ui-formwizard" id="Forum_form"
						enctype="multipart/form-data">
						<input name="moduleId" id="moduleId" type="hidden" value="${module.moduleId}" />
						<div class="row" id="forumDiv" style="display: none">
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label" for="title"><spring:message
										code="label.forumtitle" /> <span
									class="mandatory">*</span>
								</label>
								<div class="col-sm-10">
									<input id="title" name="title" class="form-control"
										style="margin: 0px -0.5px 0px 0px; height: 40px;"
										placeholder="Start a forum" type="text" value="${forum.title}">
								</div>
							</div>
					
							<div class="col-sm-12 form-group">
								 <label class="col-sm-2 control-label">Tag <span
									class="mandatory">*</span>
								</label>
								<div class="col-sm-10">
									<select class=" form-control" id="tagDiv" name="tagDiv">
										<option value=""></option>
										<c:forEach items="${tagList}" var="tag">
											<option value="${tag}"
												<c:if test="${tag eq forum.tag}">selected="selected"</c:if>>${tag.name}</option>
										</c:forEach>
									</select>
								</div>
							</div>
							
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label" for="forumDescription">Describe the topic in short <span
									class="mandatory">*</span>
								</label>
								<div class="col-sm-10">
									<textarea id="forumDescription" name="forumDescription" class="form-control"
										style="margin: 0px -0.5px 0px 0px; height: 40px;"
										placeholder="Describe in short">${forum.description}</textarea>
								</div>
							</div>
							
							<div class="col-sm-12">
								<p class="col-sm-12">
								<button id="forum_submit" type="submit"
									class="pull-right btn btn-sm btn-black">Submit Forum</button></p>
								<span class="clearfix"></span>
							</div>
							<br>
						</div>
					</form>
					<div class="topic-container">
						<c:forEach var="forum" items="${forumList}" varStatus="counter">
							<div class="topic">
								<h5>
									<a href="<%=contexturl %>ForumComments/${forum.forumId}"><strong>${forum.title}</strong></a><small class="forum-label pull-right">${forum.tag.name}</small>
								</h5>
								<p class="wrap-word">${forum.description}</p>		
							</div>
						</c:forEach>
						<c:if test="${not empty forumList}">
							<div>
								<a href="<%=contexturl%>Forums/${module.moduleId}"
									class="pull-right btn btn-sm btn-blue">See all the Forums </a>
								<span class="clearfix"></span>
							</div>
						</c:if>

					</div>

				</div>
			</div>

		</div>
      <%@include file="../../inc/images_sidebar.jsp"%>
    </div>
    </div>


     
     
<%-- <script src="<%=contexturl%>resources/js/vendor/jquery-1.11.0.min.js"></script> --%>
<%-- <script src="<%=contexturl%>resources/js/vendor/bootstrap.min.js"></script> --%>
<%-- <script src="<%=contexturl %>resources/js/vendor/validate.js"></script> --%>
<script src="${pageContext.request.contextPath}/webjars/counter-webapp-frontend/0.0.1/vendor_bundle.js"></script>


<%-- <script src="<%=contexturl %>resources/js/additional-methods.js"></script> --%>
<script src="${pageContext.request.contextPath}/webjars/counter-webapp-frontend/0.0.1/app_bundle.js"></script>

<script src="<%=contexturl %>resources/js/jquery.bxslider.js"></script>
<script src="<%=contexturl %>resources/js/ion-rangeSlider/ion.rangeSlider.min.js"></script>

<link rel="stylesheet" type="text/css" href="<%=contexturl %>resources/css/shieldui.css" />
<script type="text/javascript" src="<%=contexturl %>resources/js/progress/shieldui-all.min.js"></script>
<script type="text/javascript">

$(document).ready(function() {			
	$("#Forum_form").validate(
			{
				errorClass : "help-block animation-slideDown",
				errorElement : "div",
				errorPlacement : function(e, a) {
					a.parents(".form-group > div")
							.append(e)
				},
				highlight : function(e) {
					$(e)
							.closest(".form-group")
							.removeClass(
									"has-success has-error")
							.addClass("has-error")
				},
				success : function(e) {
					e
							.closest(".form-group")
							.removeClass(
									"has-success has-error")
				},
												rules : {
													title : {
														required : !0,maxlength : 75,
														noURL : !0
													},
													tagDiv: {
														required : !0
													},
													forumDescription: {
														required: !0,
														maxlength: 255
													}
													
												},
												messages : {
													title : {
														required :'Please enter a Title',
														maxlength :'Title should not be more than 75 characters',
														noURL : 'Please enter a valid title',
													},
													tagDiv: {
														required : 'Select a Tag'
													},
													forumDescription: {
														required: 'Please describe the topic in short',
														maxlength: 'Description should not be be more than 255 characters'
													}
													
												},
										});
	
						jQuery.validator.addMethod("noURL", function (title, element) {
							var re = /^(?=[a-zA-Z0-9~@#$^*()_+=[\]{}|\\,.?: -]*$)(?!.*[<>'"\/;`%])/;
	     					 title = title.replace(/\s+/g, "");
	      						 if (title.match(re)) return true;
	         					
	    	 				}, "Please specify a valid title");
							

						$("#forum_submit").click(function() {
// 							unsaved=false;
							$("#Forum_form").submit();
						});
						
						
});		
					
</script>
<script>

function showForumDiv(){
	$("#forumDiv").show();
}

</script>
<script>
<c:if test="${myModuleLifeStage != 0}">
var progress = $("#progress").shieldProgressBar({
    min: 0,
    max: 100,
    value: "${myModuleLifeStagePercent}",
    layout: "circular",
    layoutOptions: {
        circular: {
            borderColor: "#007335",
            width: 5,
            borderWidth: 1,
            color: "#49f297",
            backgroundColor: "transparent"
        }
    },
    text: {
        enabled: true,
        template: '<span style="font-size:20px; text-align:center; font-waight:bold; color:#ffffff">${myModuleLifeStage}</span>'
    },
}).swidget();

var progress = $("#progress2").shieldProgressBar({
    min: 0,
    max: 100,
    value: "${averageModuleLifeStage}",
    layout: "circular",
    layoutOptions: {
        circular: {
            borderColor: "#007335",
            width: 5,
            borderWidth: 1,
            color: "#49f297",
            backgroundColor: "transparent"
        }
    },
    text: {
        enabled: true,
        template: '<span style="font-size:20px; text-align:center; font-waight:bold; color: #ffffff">{0:n1}%</span>'
    },
}).swidget();
</c:if>
</script>

<%@include file="../../inc/page_footer.jsp"%>