<%@page import="com.astrika.common.model.Role"%>
<%@page import="com.astrika.common.model.User"%>


<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/page_head.jsp"%>


  <div class="blue-container">
            <div class="container">
             <h3 class="atma-breadcrumbs">
			<security:authorize ifAnyGranted="<%=Role.COMPANY_ADMIN.name()%>">
				<a class="light a-breadcrumb" href="<%=contexturl%>CompanyAdmin">Dashboard</a>
			</security:authorize>
			<security:authorize ifAnyGranted="<%=Role.MODULE_USER.name()%>">
				<a class="light a-breadcrumb" href="<%=contexturl%>ModuleUser">Dashboard</a>
			</security:authorize>
			</h3>
                <div class="row blue">
                    <div class="col-md-10">
                        <div class="media">
                           <c:choose>
                        		<c:when test = "${(user.profileImage!=null) && (not empty user.profileImage)}">
                        			<a class="pull-left" href="#"><img class="media-object pro-pic" height = 90px width=75px src="<%=contexturl %>${user.profileImage.imagePath}" alt="<%=contexturl %>resources/img/User_Thumbs.png" title="Click here to change photo" onclick="OpenFileDialog();">
                            		</a>
                        		</c:when>
                        		<c:otherwise>
                        			<a class="pull-left" href="#"><img class="media-object pro-pic" src="<%=contexturl %>resources/img/User_Thumbs.png" alt="Organisation Name" title="Click here to change photo" onclick="OpenFileDialog();">
                            		</a>
                        		</c:otherwise>
                        	</c:choose>
                            <div class="media-body text-st">
                                <p>Edit Organisational Details Here</p>
                                <h4 class="media-heading">Organisational Details</h4>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
<br>
<div class="container">
	
	<div class="row">
		<c:if test="${!empty success}">
			<div class="alert alert-success alert-dismissable">
				<button type="button" class="close" data-dismiss="alert"
					aria-hidden="true">x</button>
				<h4>
					<i class="fa fa-check-circle"></i>
					<spring:message code="label.success" />
				</h4>
				<spring:message code="${success}" />
			</div>
		</c:if>
		<div class="alert alert-danger alert-dismissable"
			style="display: none" id="error">
			<button type="button" class="close" onclick="closeDiv();"
				aria-hidden="true">x</button>
			<h4>
				<i class="fa fa-times-circle"></i>
				<spring:message code="label.error" />
			</h4>
			<span class="errorMsg"></span>
		</div>
	</div>
	<br>
	<div class="row">
		<c:if test="${not adminEdit}">
			<div class="col-md-2"></div>
			<div class="col-md-4">
				<button class="btn btn-default btn-sm btn-block"
					onclick="location.href='<%=contexturl %>EditPersonalDetails/'+${user.userId}">Personal
					Details</button>
			</div>
			<div class="col-md-4">
				<button class="btn btn-sm btn-block">Organisation Details</button>
			</div>
			<div class="col-md-2"></div>
		</c:if>
		<c:if test="${adminEdit}">
				<div>
					<button class="btn btn-sm btn-block">Organisation Details</button>
				</div>
		</c:if>
		<br> <br> <br>
	</div>
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="fuelux">
				<div class="wizard" id="MyWizard">
					<ul class="steps">
						<li style="width: 30%" data-target="#step1"><span
							class="badge badge-info">1</span>Step<span class="chevron"></span>
						</li>
						<li style="width: 40%" class="active" data-target="#step2"><span
							class="badge">2</span>Step<span class="chevron"></span></li>
						<li style="width: 30%" data-target="#step3"><span
							class="badge">3</span> Step</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-md-2"></div>
	</div>
	<div class="row">
		<form action="<%=contexturl%>SaveOrganisationalDetailsStep2" method="post"
			class="form-horizontal " id="step2_form"
			enctype="multipart/form-data">
			<input name="userId" value="${user.userId}" type="hidden"> <input
				name="companyId" value="${company.companyId}" type="hidden">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<div class="block full">
					<div class="block-title">
						<h4 class="">Organisation Details</h4>
					</div>
					<div class="form-group">
						<label class="col-sm-5 control-label">Religious/political
							inclination<span class="mandatory">*</span>
						</label>
						<div class="col-sm-7">
							<c:choose>
								<c:when test="${company.religiousInclination}">
									<label class="radio"> <input id="religiousinclination"
										type="radio" name="inclination" value="1"
										<c:if test="${company.religiousInclination eq true}"> checked = "checked"</c:if> />
										<span class="input-text">Yes</span>
									</label>
									<label class="radio"> <input id="politicalinclination"
										type="radio" name="inclination" value="0"
										<c:if test="${company.religiousInclination eq false}"> checked = "checked"</c:if> />
										<span class="input-text">No</span>
									</label>
								</c:when>
								<c:otherwise>
									<label class="radio"> <input id = "religiousinclination" type="radio" name="inclination"
										value="1" /> <span class="input-text">Yes</span>
									</label>
									<label class="radio"> <input id="politicalinclination" type="radio" name="inclination"
										value="0" checked="checked" /> <span class="input-text">No</span>
									</label>
								</c:otherwise>
							</c:choose>
						</div>
					</div>
					<c:if test="${not empty company.reasonOfInclination}">
						<div class="form-group" id="religiousReason">
							<label class="col-sm-5 control-label">If, yes please
								explain. </label>
							<div class="col-sm-7">
								<textarea id="inclinationReason" name="inclinationReason"
									class="form-control" rows="2" placeholder="Please explain">${company.reasonOfInclination}</textarea>
							</div>
						</div>

					</c:if>
					<div class="" id="selectedDiv2"></div>
					
					
					<div class="form-group">
						<label class="col-sm-5 control-label">Area or Areas of
							program operations<span class="mandatory">*</span>
						</label>
						<div class="col-sm-7">
							<c:choose>
								<c:when test="${not empty areaPrograms}">
									<c:forEach items="${areasOfOperations}" var="areaOfOperations">
										<c:forEach items="${areaPrograms}" var="areaPrograms"
											varStatus="count">
											<c:if test="${areaPrograms eq areaOfOperations}">
												<c:set var="checkarea" value="true"></c:set>
											</c:if>
										</c:forEach>
										
										<label class="checkbox"> <c:choose>
												<c:when test="${checkarea eq 'true'}">
													<c:choose>
														<c:when test="${areaOfOperations eq 'Others'}">
															<input id="opArea_${counter.count}" type="checkbox"
																name="operationArea" value="${areaOfOperations}"
																class="Others" checked="checked" />
															<span class="input-text">${areaOfOperations}</span>
														</c:when>
														<c:otherwise>
															<input id="opArea_${counter.count}" type="checkbox"
																name="operationArea" value="${areaOfOperations}"
																checked="checked" />
															<span class="input-text">${areaOfOperations}</span>
														</c:otherwise>
													</c:choose>
												</c:when>
												<c:otherwise>			
													<c:choose>
														<c:when test="${areaOfOperations eq 'Others'}">
															<input id="opArea_${counter.count}" type="checkbox"
																name="operationArea" value="${areaOfOperations}" class="Others" />
															<span class="input-text">${areaOfOperations}</span>
														</c:when>
														<c:otherwise>
															<input id="opArea_${counter.count}" type="checkbox"
																name=operationArea value="${areaOfOperations}" />
															<span class="input-text">${areaOfOperations}</span>
														</c:otherwise>
													</c:choose>											
												</c:otherwise>
											</c:choose>
										</label>
										<c:set var="checkarea" value="false"></c:set>
										
									</c:forEach>
								</c:when>
								<c:otherwise>
									<c:forEach items="${areasOfOperations}" var="areaOfOperations"
										varStatus="counter">
										<label class="checkbox"> <c:choose>
												<c:when test="${areaOfOperations eq 'Others'}">
													<input id="opArea_${counter.count}" type="checkbox"
														name="operationArea" value="${areaOfOperations}" class="Others" />
													<span class="input-text">${areaOfOperations}</span>
												</c:when>
												<c:otherwise>
													<input id="opArea_${counter.count}" type="checkbox"
														name="operationArea" value="${areaOfOperations}" />
													<span class="input-text">${areaOfOperations}</span>
												</c:otherwise>
											</c:choose>

										</label>
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</div>
					</div>
					
					<c:if test="${not empty company.reasonForOtherArea}">
						<div class="form-group" id="otherareaDiv">
							<label class="col-sm-3 control-label">Mention the other
								Areas of Operations<span class="mandatory">*</span>
							</label>
							<div class="col-sm-8">
								<input id="otherArea" name="otherArea" type="text"
									class="form-control" placeholder="Other"
									value="${company.reasonForOtherArea}">
							</div>
						</div>
					</c:if>
					<div class="" id="selectedDiv1"></div>
					
				</div>
				<!--</div>
                        <div class="col-md-6">-->
				<div class="block full">
					<div class="block-title">
						<h4 class="">Legal Organisation</h4>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Registration number
						</label>
						<div class="col-sm-8">
							<input id="registrationNumber" name="registrationNumber"
								type="text" class="form-control" placeholder="e.g. AC097609" value="${company.registrationNumber}">
						</div>
					</div>
<!-- 					<div class="form-group"> -->
<!-- 						<label class="col-sm-3 control-label">Date of registration<span -->
<!-- 							class="mandatory">*</span> -->
<!-- 						</label> -->
<!-- 						<div class="col-sm-8"> -->
<!-- 							<input type="text" class="form-control" placeholder="NGO Website"> -->
<!-- 						</div> -->
<!-- 					</div> -->
					<div class="row">
						<label class="col-sm-3 control-label">PAN Number 
						</label>
						<div class="form-group col-sm-5">
						<div class="col-sm-12">
							<input id="panNumber" name="panNumber" type="text"
								class="form-control" placeholder="e.g. ABCDE1234F" value="${company.panNumber}">
						</div>
						</div>
						<div class="form-group col-sm-4">
						<div class="col-sm-12">
							<input type="file" id="panNumberFile" name="panNumberFile"><a class="wrap-word"
								target="_blank" href="<%=contexturl%>Files/${company.panFile}">${company.panDocumentName}</a>
						</div>
						</div>
					</div>
					<div class="row">
						<label class="col-sm-3 control-label">12 A Certificate 
						</label>
						<div class="form-group col-sm-5">
							<div class="col-sm-12">
								<input id="12ACertificate" name="twelveACertificate" type="text"
									class="form-control" placeholder="Number"
									value="${company.twelveACertificate}">
							</div>
						</div>
						<div class="form-group col-sm-4">
							<div class="col-sm-12">
								<input type="file" id="twelveAFile" name="twelveAFile"><a
									class="wrap-word" target="_blank"
									href="<%=contexturl%>Files/${company.twelveAFile}">${company.twelveADocumentName}</a>
							</div>
						</div>
					</div>
					<div class="row">
						<label class="col-sm-3 control-label">80 G Number 
						</label>
						<div class="form-group col-sm-5">
							<div class="col-sm-12">
								<input id="80GNumber" name="eightyGNumber" type="text"
									class="form-control" placeholder="Number"
									value="${company.eightyGNumber}">
							</div>
						</div>
						<div class="form-group col-sm-4">
							<div class="col-sm-12">
								<input type="file" id="eightyGFile" name="eightyGFile"><a
									class="wrap-word" target="_blank"
									href="<%=contexturl%>Files/${company.eightyGFile}">${company.eightyGDocumentName}</a>
							</div>
						</div>
					</div>
					<div class="row">
						<label class="col-sm-3 control-label">FCRA Registration
							Number 
						</label>
						<div class="form-group col-sm-5">
							<div class="col-sm-12">
								<input id="fcraregNumber" name="fcraregNumber" type="text"
									class="form-control" placeholder="Number"
									value="${company.fcraRegNumber}">
							</div>
						</div>
						<div class="form-group col-sm-4">
							<div class="col-sm-12">
								<input type="file" id="fcraForm" name="fcraForm"><a
									class="wrap-word" target="_blank"
									href="<%=contexturl%>Files/${company.fcraFile}">${company.fcraDocumentName}</a>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Number of trustees
						</label>
						<div class="col-sm-4">
							<input id="noOfTrustees" name="noOfTrustees" type="text"
								class="form-control" placeholder="e.g. 10" value="${company.noOfTrustees}">
						</div>
					</div>
				</div>
				<div class="block full">
					<div class="block-title">
						<h4 class="">Education Details</h4>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label">Number of Children
							in your programs<span class="mandatory">*</span>
						</label>
						<div class="col-sm-8">
							<input id="noOfChildren" name="noOfChildren" type="text"
								class="form-control" placeholder="e.g. 12" value="${company.noOfChildren}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label">Number of Girls<span
							class="mandatory">*</span>
						</label>
						<div class="col-sm-8">
							<input id="noOfGirls" name="noOfGirls" type="text"
								class="form-control" placeholder="e.g. 15" value="${company.noOfGirls}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label">Number of Boys<span
							class="mandatory">*</span>
						</label>
						<div class="col-sm-8">
							<input id="noOfBoys" name="noOfBoys" type="text"
								class="form-control" placeholder="e.g. 15" value="${company.noOfBoys}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label">Age of students
							served<span class="mandatory">*</span>
						</label>
						<div class="col-sm-8">
							<select class=" form-control" id="ageOfStudents"
								name="ageOfStudents">
								<option value=""></option>
								<c:forEach items="${ageList}" var="age">
									<option value="${age}"
										<c:if test="${age eq company.ageOfStudents}">selected="selected"</c:if>>${age}</option>
								</c:forEach>
							</select>
						</div>
					</div>
				</div>
				<div class="block full">
					<div class="block-title">
						<h4 class="">Funding</h4>
					</div>
					<div class="form-group">
						<label class="col-sm-5 control-label">What is your main
							sources of funding?<span class="mandatory">*</span>
						</label>
						<div class="col-sm-7">
							<c:choose>
								<c:when test="${not empty company.mainSourceOfFunding}">
									<label class="radio"> <input id="individualFunders"
										type="radio" name="funders" value="Individuals"
										<c:if test="${company.mainSourceOfFunding eq 'Individuals'}"> checked = "checked"</c:if> />
										<span class="input-text">Individuals</span>
									</label>
									<label class="radio"> <input id="foundations"
										type="radio" name="funders" value="Foundations"
										<c:if test="${company.mainSourceOfFunding eq 'Foundations'}"> checked = "checked"</c:if> />
										<span class="input-text">Foundations</span>
									</label>
									<label class="radio"> <input id="corporateFunders"
										type="radio" name="funders" value="Corporate Funders"
										<c:if test="${company.mainSourceOfFunding eq 'Corporate Funders'}"> checked = "checked"</c:if> />
										<span class="input-text">Corporate Funders</span>
									</label>
								</c:when>
								<c:otherwise>
									<label class="radio"> <input id="individualFunders"
										type="radio" name="funders" value="Individuals"/>
										<span class="input-text">Individuals</span>
									</label>
									<label class="radio"> <input id="foundations"
										type="radio" name="funders" value="Foundations"/>
										<span class="input-text">Foundations</span>
									</label>
									<label class="radio"> <input id="corporateFunders"
										type="radio" name="funders" value="Corporate Funders" checked="checked"/>
										<span class="input-text">Corporate Funders</span>
									</label>
								</c:otherwise>
							</c:choose>
						</div>
					</div>
				</div>

			</div>
		</form>
		<div class="col-md-2"></div>
	</div>
</div>

<div id="tempOtherAreaDiv" style="display: none">
	<div class="form-group" id="otherareaDiv">
		<label class="col-sm-3 control-label">Mention the other Areas
			of Operations<span class="mandatory">*</span>
		</label>
		<div class="col-sm-8">
			<input id="otherArea" name="otherArea" type="text"
				class="form-control" placeholder="Other"
				value="${company.reasonForOtherArea}">
		</div>
	</div>
</div>

<div id="tempReligiousReasonDiv" style="display: none">
	<div class="form-group" id="religiousReason">
		<label class="col-sm-5 control-label">If, yes please explain.
		</label>
		<div class="col-sm-7">
			<textarea id="inclinationReason" name="inclinationReason"
				class="form-control" rows="2" placeholder="Please explain">${company.reasonOfInclination}</textarea>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="text-center">
			<c:if test="${not adminEdit}">
				<button type="button" class="btn btn-primary" onclick="location.href='<%=contexturl %>EditOrganisationalDetailsStep1/'+${user.userId}">Back</button>
			</c:if>

			<c:if test="${adminEdit}">
            	<button type="button" class="btn btn-primary" onclick="location.href='<%=contexturl %>EditOrganisationalDetailsByAdmin/step/1/company/${company.companyId}'">Back</button>
            </c:if>


			<button id="step2" type="submit" class="btn btn-warning">Submit this page</button>

			 <c:if test="${not adminEdit}">
			 	<button type="button" class="btn btn-primary" onclick="location.href='<%=contexturl %>EditOrganisationalDetailsStep3/'+${user.userId}">Next</button>
             </c:if>

             <c:if test="${adminEdit}">
           		<button type="button" class="btn btn-primary" onclick="location.href='<%=contexturl %>EditOrganisationalDetailsByAdmin/step/3/company/${company.companyId}'">Next </button>
           	 </c:if>

           	  <c:if test="${adminEdit}">
					<button type="button" class="btn btn-info" onclick="location.href='<%=contexturl %>ManageAdmin/OrganizationList'">Back To Organisation List </button>
			 </c:if>


		</div>
		<br> <br> <br>
	</div>
</div>


<%-- <script src="<%=contexturl%>resources/js/vendor/jquery-1.11.0.min.js"></script> --%>
<%-- <script src="<%=contexturl%>resources/js/vendor/bootstrap.min.js"></script> --%>
<%-- <script src="<%=contexturl%>resources/js/vendor/validate.js"></script> --%>
<script src="${pageContext.request.contextPath}/webjars/counter-webapp-frontend/0.0.1/vendor_bundle.js"></script>

<%-- <script src="<%=contexturl%>resources/js/additional-methods.js"></script> --%>
<script src="${pageContext.request.contextPath}/webjars/counter-webapp-frontend/0.0.1/app_bundle.js"></script>

 <script type="text/javascript">
		$(document).ready(
				function() {
					

					 var otherAreaDiv= $("#tempOtherAreaDiv").contents();
					 
					 var religiousreason = $("#tempReligiousReasonDiv").contents();
					 
					 $("input[name='operationArea']").change(function(){
						 if($("input[class='Others']:checked").val() == "Others"){
							 $('#selectedDiv1').empty();	
							 $('#selectedDiv1').append(otherAreaDiv);
						 }
						 else{
							 $('#otherareaDiv').empty();
							 $('#selectedDiv1').empty();
						 }
					 });
					 
					 $("input[name='inclination']").change(function(){
						 if($("input[name='inclination']:checked").val() == "1"){
							 $('#selectedDiv2').empty();	
							 $('#selectedDiv2').append(religiousreason);
						 }
						 else{
							 $('#religiousReason').empty();
							 $('#selectedDiv2').empty();
						 }
					 });
					 

					$("#step2_form").validate(
							{
								errorClass : "help-block animation-slideDown",
								errorElement : "div",
								errorPlacement : function(e, a) {
									a.parents(".form-group > div").append(e)
								},
								highlight : function(e) {
									$(e).closest(".form-group").removeClass(
											"has-success has-error").addClass(
											"has-error")
								},
								success : function(e) {
									e.closest(".form-group").removeClass(
											"has-success has-error")
								},
								rules : {
									
									inclinationReason : {
										required : !0,
										maxlength : 100,
									},
									
									operationArea : {
										required : !0,
									},
									otherArea : {
										required : !0,
										maxlength : 100,
									},
									registrationNumber : {
										lettersandNumbers : true,
									},
									panNumber : {
										
									},
									<c:if test="${empty company.panFile}">
										panNumberFile : {
										
											extension : "pdf",
										},
									</c:if>
									<c:if test="${not empty company.panFile}">
										panNumberFile : {
											extension : "pdf",
										},
									</c:if>
									twelveACertificate : {
										lettersandNumbers : true,
										maxlength : 10,
									},
									<c:if test="${empty company.twelveAFile}">
										twelveAFile : {
											extension : "pdf",
										},
									</c:if>
									<c:if test="${not empty company.twelveAFile}">
										twelveAFile : {
											extension : "pdf",
										},
									</c:if>
									eightyGNumber : {
										lettersandNumbers : true,
									},
									<c:if test="${empty company.eightyGFile}">
										eightyGFile : {
											extension : "pdf",
										},
									</c:if>
									<c:if test="${not empty company.eightyGFile}">
										eightyGFile : {
											extension : "pdf",
										},
									</c:if>
									fcraregNumber : {
										lettersandNumbers : true,
									},
									<c:if test="${empty company.fcraFile}">
										fcraForm : {
											extension : "pdf",
										},
									</c:if>
									<c:if test="${not empty company.fcraFile}">
										fcraForm : {
											extension : "pdf",
										},
									</c:if>
									noOfTrustees : {
									},
									noOfChildren : {
										required : !0,
										numeric : true,	
										maxlength : 10,
									},
									noOfGirls : {
										required : !0,
										numeric : true,	
										maxlength : 10,
									},
									noOfBoys : {
										required : !0,
										numeric : true,
										maxlength : 10,
									},
									ageOfStudents : {
										required : !0,
									}
								},
								messages : {
									
									inclinationReason : {
										required : 'Please enter a reason',
										maxlength : 'Reason cannot be more than 100 characters',
									},
									operationArea : {
										required : 'Please select atleast one area of operation',
									},
									otherArea : {
										required : 'Please mention the other areas',
										maxlength : 'Area of operation should not be more than 100 characters',
									},
									registrationNumber : {
										required : 'Please enter the registration number',
										lettersandNumbers : 'Please enter a valid registration number',
										maxlength : 'Registration Number cannot be more than 10 characters',
									},
									panNumber : {
										required : 'Please enter the pan number',
										pannumber : 'Please enter a valid pan number',
										maxlength : 'Pan number cannot be more than 10 characters',
									},	
									<c:if test="${empty company.panFile}">
										panNumberFile : {
											required : 'Please select a file',
											extension : 'Please upload a .pdf file only',
										},
									</c:if>
									<c:if test="${not empty company.panFile}">
										panNumberFile : {
											extension : 'Please upload a .pdf file only',
										},
									</c:if>
									twelveACertificate : {
										required : 'Please enter the 12 A Certificate number',
										lettersandNumbers : 'Please enter a valid 12 A Certificate number',
										maxlength : '12 A Certificate Number cannot be more than 10 characters',
									},
									
									<c:if test="${empty company.twelveAFile}">
										twelveAFile : {
											required : 'Please select a file',
											extension : 'Please upload a .pdf file only',
										},
									</c:if>
									<c:if test="${not empty company.twelveAFile}">
										twelveAFile : {
											extension : 'Please upload a .pdf file only',
										},
									</c:if>
									eightyGNumber : {
										required : 'Please enter the 80 G Certificate number',
										lettersandNumbers : 'Please enter a valid 80 G Certificate number',
										maxlength : '80 G Number cannot be more than 10 characters',
									},
									<c:if test="${empty company.eightyGFile}">
										eightyGFile : {
											required : 'Please select a file',
											extension : 'Please upload a .pdf file only',
										},
									</c:if>
									<c:if test="${not empty company.eightyGFile}">
										eightyGFile : {
											extension : 'Please upload a .pdf file only',
										},
									</c:if>
									fcraregNumber : {
										required : 'Please enter the FCRA Registration number',
										lettersandNumbers : 'Please enter a valid FCRA Registration number',
										maxlength : 'FCRA Number cannot be more than 10 characters',
									},
									<c:if test="${empty company.fcraFile}">
										fcraForm : {
											required : 'Please select a file',
											extension : 'Please upload a .pdf file only',
										},
									</c:if>
									<c:if test="${not empty company.fcraFile}">
										fcraForm : {
											extension : 'Please upload a .pdf file only',
										},
									</c:if>
									noOfTrustees : {
										required : 'Please enter the number of trustees',		
										numeric : 'Please enter a valid number',
										maxlength : 'Only upto two digits allowed',
									},
									noOfChildren : {
										required : 'Please enter the number of children',
										numeric : 'Please enter a valid number',
										maxlength : 'Only upto ten digits allowed',
									},
									noOfGirls : {
										required : 'Please enter the number of girls',
										numeric : 'Please enter a valid number',
										maxlength : 'Only upto ten digits allowed',
									},
									noOfBoys : {
										required : 'Please enter the number of boys',
										numeric : 'Please enter a valid number',
										maxlength : 'Only upto ten digits allowed',
									},
									ageOfStudents : {
										required : 'Please select the age range of students',
									}
								},
							});
					
					
					jQuery.validator.addMethod("lettersandNumbers", function( value, element ) {
				        var regex = new RegExp("^[a-zA-Z0-9]*$");
				        var key = value;

				        if (!regex.test(key)) {
				           return false;
				        }
				        return true;
				    }, "please use only alphabetic characters");
					
					jQuery.validator.addMethod("numeric", function( value, element ) {
						var regex = /^[0-9]\d*(((,\d{3}){1})?(\.\d{0,2})?)$/;
				        var key = value;

				        if (!regex.test(key)) {
				           return false;
				        }
				        return true;
				    }, "Incorrect value");
					
					//At least one (+) digit (\d), followed by an optional group containing a hyphen-minus (-), followed by at least one digit again.	
					jQuery.validator.addMethod("employeeno", function( value, element ) {
						var regex = /^\d+(-\d+)?$/;
				        var key = value;

				        if (!regex.test(key)) {
				           return false;
				        }
				        return true;
				    }, "Incorrect value");
					
					jQuery.validator.addMethod("pannumber", function( value, element ) {
						var regex = /[A-Z]{5}\d{4}[A-Z]{1}/;
				        var key = value;

				        if (!regex.test(key)) {
				           return false;
				        }
				        return true;
				    }, "Incorrect value");
									
					
					$("#step2").click(function() {
						unsaved = false;
						$("#step2_form").submit();
					});

				});
	</script>

 <%@include file="../../inc/page_footer.jsp"%>
