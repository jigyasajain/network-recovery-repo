<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>


<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>

			<h1>
<%-- 				<spring:message code="heading.comment" /> --%>
                Comments
<%-- 				<br> <small><spring:message --%>
<%-- 						code="heading." /></small> --%>
			</h1>
			<span id="errorMsg"></span>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
<%-- 		<li><spring:message code="menu.manageforum" /></li> --%>
<li>Manage Comments</li>
	</ul>
	<form action="<%=contexturl %>SaveComment" method="post" class="form-horizontal ui-formwizard"
		id="Comments_form" enctype="multipart/form-data">
		<div class="row">
<%--   <input name="id" id="id" type="hidden" value="${ } --%>
  <input name="moduleId" id="moduleId" type="hidden" value="${module.moduleId}"/>
  <input name="categoryId" id="categoryId" type="hidden" value="${category.categoryId}"/>
			<div class="">
			
				<div class="block">
					<div class="block-title">
						<h2>
							<strong>
<%-- 							<spring:message code="heading.foruminfo" /> --%>
                             List Of Comments
							</strong>
						</h2>
						
					</div>
					<div class="form-group">
					<div class="col-md-6">
<%-- 					 <c:forEach var="listOfComments" items="${commentList}"> --%>
<%-- 						<label class="col-md-4 control-label" for="comment_List">${listOfComments.comments}<br>  --%>
<!--                        </label> -->
<%-- 						 </c:forEach> --%>
                                   <dl>
                                   <c:forEach var="listOfComments" items="${commentList}">
                                   <dt>${listOfComments.createdBy.fullName}</dt>
                                   <dd>${listOfComments.comments}</dd>
                                   </c:forEach>
                                   </dl>
						 </div>
					</div>
					<div class="form-group">
					<div class="col-md-6">
					
                         
							<textarea id="title" name="comments" class="form-control"
								placeholder="<spring:message code='label.forumtitle'/>..">${comment.comments}</textarea>
						</div>
						<button id="add_comment" type="submit"
							class="btn btn-sm btn-primary save">
							<i class="fa fa-angle-right"></i>
							<spring:message code="button.addcomment" />
						</button>
					</div>
					</div>
					</div>
					</div>


</form>
</div>
<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>

<%-- <script src="<%=contexturl %>resources/js/custom/featurecontrol.js"></script> --%>
<script src="${pageContext.request.contextPath}/webjars/counter-webapp-frontend/0.0.1/app_bundle.js"></script>

<%@include file="../../inc/chosen_scripts.jsp"%>
<script type="text/javascript">
	$(document)
			.ready(
					function() {
						
					
						$(".tag_chosen").data("placeholder","Select Tag From...").chosen();	
 						
						$("#Forum_form").validate(
						{	errorClass:"help-block animation-slideDown",
							errorElement:"div",
							errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
							highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
							success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
										rules : {
											title : {
												required : !0,maxlength : 75
											},
											chosenTagId: {
												required : !0
											}
											
										},
										messages : {
											title : {
												required :'<spring:message code="validation.pleaseenteratitle"/>',
												maxlength :'<spring:message code="validation.title75character"/>'
											},
											chosenTagId: {
												required : '<spring:message code="validation.selecttag"/>'
											}
											
										},
								});

						$("#company_submit").click(function() {
							unsaved=false;
							$("#Comment_form").submit();
						});
						
						$("button[type=reset]").click(function(evt) {
							evt.preventDefault(); // stop default reset behavior (or the rest of this code will run before the reset actually happens)
							form = $(this).parents("form").first(); // get the reset buttons form
							form[0].reset(); // actually reset the form
							 // tell all Chosen fields in the form that the values may have changed
							form.find(".country_chosen").trigger("chosen:updated");
							
							var myOptions = "<option value></option>";
							$(".city_chosen").html(myOptions).chosen().trigger("chosen:updated");
							
							$(".state_chosen").html(myOptions).chosen().trigger("chosen:updated");
							});

						
					});
	

</script>

<style>
.chosen-container {
	width: 250px !important;
}
#map-canvas {
	height: 300px
}
</style>

<%@include file="../../inc/template_end.jsp"%>