<%@include file="/WEB-INF/views/admin/inc/config.jsp"%>
<%@include file="/WEB-INF/views/admin/inc/template_start.jsp"%>
<%@include file="/WEB-INF/views/admin/inc/template_scripts.jsp"%>
<style>
.img-list1 {
list-style-type: none;
margin: 0;
padding: 0;
text-align: center;
}
 
.img-list2{
display: inline-block;
height: 150px;
margin: 0 1em 1em 0;
position: relative;
width: 150px;
}

span.text-content {
background: rgba(0,0,0,0.5);
color: white;
cursor: pointer;

height: 150px;
left: 0;
position: absolute;
top: 0;
width: 150px;
}
 
span.text-content span {

text-align: center;
vertical-align: middle;
}

span.text-content {
background: rgba(0,0,0,0.5);
color: white;
cursor: pointer;
border-radius: 50%;
margin-left: 15px;
height: 120px;
left: 0;
position: absolute;
top: 0;
width: 120px;
opacity: 0;
}
 
.img-list1 div:hover span.text-content {
opacity: 1;
}
</style>
<div id="page-container"
	class="sidebar-partial sidebar-visible-lg sidebar-no-animations style-alt footer-fixed">
	<!-- START Header -->
	<header class="navbar navbar-inverse">
		<!-- Navbar Header -->
		<div class="navbar-header">
			<!-- Main Sidebar Toggle Button -->
			<ul class="nav navbar-nav-custom">

				<!-- Brand -->
				<a href="Index" class="sidebar-brand-logo"> <img
					src="<%=contexturl%>resources/img/logo.png">
				</a>
				<!-- END Brand -->
			</ul>
			<!--                         <ul> -->
			<!--                           <a class="btn btn-sm btn-primary" href="Login"> -->
			<!-- 								  <i class="gi gi-remove"></i>Back to Login Page  -->
			<!-- 				             </a> -->
			<!--                         </ul> -->
		</div>
</div>
<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i>
						<spring:message code="label.success" />
					</h4>
					<spring:message code="${success}" />
					<br> <a class="btn btn-sm btn-primary" href="Login"> <i
						class="gi gi-remove"></i> <spring:message
							code="heading.backtologinpage" />
					</a>
				</div>
			</c:if>
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i>
						<spring:message code="label.error" />
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>
			<h1>
				<i class="fa fa-map-marker"></i>
				<spring:message code="heading.studentregistration" />
				<br>
				<small></small>
			</h1>
			<span id="errorMsg"></span>
		</div>
	</div>

	<%-- <div class="row">
		<div class="col-md-12">
			<div class="block">
				<div class="block-title">
					<h2>
						<strong><spring:message code="heading.corporatemember" />
						</strong>
					</h2>
				</div>

				<div class="form-group" style="height: 45px;">
					<label class="col-md-2 control-label" for="corporateMember"><spring:message
							code="heading.corporatemember" /> </label>
					<div class="col-md-2">
						<label class="switch switch-primary"> <input
							id="corporateMember" name="corporateMember" type="checkbox"
							<c:if test="${!empty corporatecode}">checked="checked"  disabled="disabled"</c:if> />
							<span></span> </label>
					</div>
					<form action="" method="post" class="form-horizontal " <c:if test="${empty corporatecode}">style="display: none"</c:if>
						id="corporate_form">
						
						<label class="col-md-2 control-label" for="corporate_code"><spring:message
								code="heading.corporatecode" /><span class="text-danger">*</span>
						</label>
						<div class="form-group">
						<div class="col-md-2">
							<input id="corporate_code" name="corporateCode"
								class="form-control" placeholder="Corporate Code.."
								type="hidden"
								<c:if test="${!empty corporatecode}">value="${corporatecode}"</c:if>/>

							<input id="corporate_code1" name="corporateCode1"
								class="form-control" placeholder="Corporate Code.." type="text"
								<c:if test="${!empty corporatecode}">value="${corporatecode}"  disabled="disabled"</c:if>/>
						</div>
						
						
						
						<div class="col-md-2" style="float: right;">
							<div id="corporate_submit" type="submit"
								class="btn btn-sm btn-primary save">
								<i class="fa fa-angle-right"></i>
								<spring:message code="button.save" />
							</div>
						</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div> --%>

	<form action="SaveMember" method="post" class="form-horizontal "
		id="member_form" enctype="multipart/form-data">
		
		<div class="row">

			<div class="col-md-6">
				<div class="block">
					<div class="block-title">
						<h2>
							<%-- 							<strong><spring:message code="heading.admininfo"/></strong> --%>
							<strong><spring:message code="heading.studentinfo" /></strong>
						</h2>
					</div>
<%-- 					<input id="corpCode" name="corpCode" type="hidden"
						value="${corporatecode}"> <input id="corporateId"
						name="corporateId" type="hidden" value="${corporateId}"> <input
						id="emailId" name="emailId" type="hidden" value="${emailId}"  > --%>
					<div class="form-group">
						<label class="col-md-4 control-label" for="first_Name"><spring:message
								code="label.firstname" /><span class="text-danger">*</span> </label>
						<div class="col-md-6">
							<input id="first_Name" name="firstName" class="form-control"
								placeholder="<spring:message code='label.firstname'/>.."
								type="text" value="${member.user.firstName}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="last_Name"><spring:message
								code="label.lastname" /><span class="text-danger">*</span> </label>
						<div class="col-md-6">
							<input id="last_Name" name="lastName" class="form-control"
								placeholder="<spring:message code="label.lastname"/>.."
								type="text" value="${member.user.lastName}">
						</div>
					</div>
					
					
<!-- 					<div class="form-group"> -->
<%-- 						<label class="col-md-4 control-label" for="example-daterange1"><spring:message --%>
<%-- 								code="label.profile" /> </label> --%>
<!-- 						<div class="col-md-6"> -->
<!-- 							<input type="file" name="profile" accept="image/*" /> -->
<!-- 						</div> -->
<!-- 					</div> -->


					<div class="form-group img-list1" id="profilepic">
						<label class="col-md-4 control-label" for="offer_validtill"><spring:message code="label.profilepic" /></label>
						<div class="col-md-6 img-list2" id="imagediv">
						
							<img border="0"
								src="<%=contexturl%>resources/img/application/default_profile.png"
								width="120" height="120" title="Click here to change photo"
								onclick="OpenFileDialog();" class="img-circle pic" id="profileimage" style="cursor: pointer;" />
								
								<span class="text-content" onclick="OpenFileDialog();" title="Click here to change photo"><br>
								<br><span>Upload Profile pic</span></span>
						</div>
						<input type="file" name="profile" id="imageChooser" style="display: none;"
							name="imageChooser"
							accept="image/jpg,image/jpeg,image/png,image/gif,image/bmp,image/tiff" />

					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="email"><spring:message
								code="label.emailid" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<input name="emailId" class="form-control" id="email"
								placeholder="test@example.com" type="text" value="${emailId}"
								<c:if test="${!empty corporatecode}"> disabled="disabled"  </c:if>>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="mobile"><spring:message
								code="label.mobile" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<input id="mobile" name="mobile" class="form-control"
								placeholder='<spring:message code="label.mobile"></spring:message>'
								type="text" value="${member.user.mobile}">
						</div>
					</div>



					<div class="form-group">
						<label class="col-md-4 control-label" for="login"><spring:message
								code="label.loginid" /><span class="text-danger">*</span> </label>
						<div class="col-md-6">
							<input id="login" name="loginId" class="form-control" placeholder='<spring:message code='label.loginid'/>' type="text" value="${member.user.loginId}">
							<span class="label label-success" id="availability" style="cursor: pointer;"><i class="fa fa-check"></i> 
							<spring:message code="label.checkavailability" /></span>
							<span class="text-success" id="available" style="display: none">
								<spring:message code="alert.loginavailable" />
							</span>
							 <span class="text-danger" id="notavailable" style="display: none">
							  <spring:message code="alert.loginnotavailable" />
							</span>
							<span id="loading" style="display: none"><i class="fa fa-spinner fa-spin"></i>
						   </span>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="password"><spring:message
								code="label.password" /><span class="text-danger">*</span> </label>
						<div class="col-md-6">
							<input id="password" name="password" class="form-control"
								placeholder="Choose a crazy one.." type="password" value="">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="confirm_password"><spring:message
								code="label.confirmpassword" /><span class="text-danger">*</span>
						</label>
						<div class="col-md-6">
							<input id="confirm_password" name="confirmPassword"
								class="form-control" placeholder="..and confirm it!"
								type="password" value="">
						</div>
					</div>
				</div>
				


			</div>



			<div class="col-md-6">



				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.studentcontactinfo" /></strong>
						</h2>
					</div>



					<div class="form-group">
						<label class="col-md-4 control-label" for="addressline1"><spring:message
								code="label.addressline1" /><span class="text-danger">*</span>
						</label>
						<div class="col-md-6">

							<input id="memberAddressline1" name="memberAddressLine1"
								class="form-control"
								placeholder="<spring:message code="label.addressline1"/>.."
								type="text" value="${member.addressLine1}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="addressline2"><spring:message
								code="label.addressline2" /></label>
						<div class="col-md-6">

							<input id="memberAddressline2" name="memberAddressLine2"
								class="form-control"
								placeholder="<spring:message code="label.addressline2"/>.."
								type="text" value="${member.addressLine2}">
						</div>
					</div>
					
					<div class="form-group" id="countryDiv">
						<label class="col-md-4 control-label" for="country"><spring:message
								code="label.countryname" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<select class="country_chosen" style="width: 200px;"
								id="chosenCountryId"
								data-placeholder="<spring:message code='label.choosecountry' />"
								name="chosenCountryId">
								<option value=""></option>
								<c:forEach items="${countryList}" var="country">
									<option value="${country.countryId}"
										<c:if test="${country.countryId eq member.user.country.countryId}">selected="selected"</c:if>>${country.countryName}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					
					
					<div class="form-group" id="stateDiv">
						<label class="col-md-4 control-label" for="country"><spring:message
								code="label.statename" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<select class="state_chosen" style="width: 200px;"
								id="chosenStateId"
								data-placeholder="<spring:message code='label.choosestate' />"
								name="chosenStateId">
								<option value=""></option>
								<c:forEach items="${stateList}" var="state">
									<option value="${state.stateId}"
										<c:if test="${state.stateId eq member.user.state.stateId}">selected="selected"</c:if>>${state.stateName}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					
					<div class="form-group" id="cityDiv">
						<label class="col-md-4 control-label" for="city"><spring:message
								code="label.cityname" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<select class="city_chosen" style="width: 200px;"
								id="chosenCityId"
								data-placeholder="<spring:message code='label.choosecity' />"
								name="chosenCityId">
								<c:forEach items="${cityList}" var="city">
									<option value="${city.cityId}"  <c:if test="${city.cityId eq member.user.city.cityId}">selected="selected"</c:if> >${city.cityName}</option>
								</c:forEach>
							</select>
						</div>
					</div>

<!-- 					<div class="form-group" id="areaDiv"> -->
<%-- 						<label class="col-md-4 control-label" for="area"><spring:message --%>
<%-- 								code="label.statename" /><span class="text-danger">*</span></label> --%>
<!-- 						<div class="col-md-6"> -->
<!-- 							<select class="area_chosen" style="width: 200px;" -->
<!-- 								id="memberAreaId" -->
<%-- 								data-placeholder="<spring:message code='label.choosestate' />" --%>
<!-- 								name="chosenAreaId"> -->
<%-- 									<c:forEach items="${stateList}" var="state"> --%>
<%-- 												<option value="${state.stateId}"  <c:if test="${state.stateId eq member.state.stateId}">selected="selected"</c:if> >${state.stateName}</option> --%>
<%-- 									</c:forEach> --%>
<!-- 							</select> -->
<!-- 						</div> -->
<!-- 					</div> -->

					<div class="form-group">
						<label class="col-md-4 control-label" for="memberPincode"><spring:message
								code="label.pincode" /><span class="text-danger">*</span></label>
						<div class="col-md-6">

							<input id="memberPincode" name="memberPincode"
								class="form-control"
								placeholder="<spring:message code="label.pincode"/>.."
								type="text" value="${member.pincode}">
						</div>
					</div>




				</div>
				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.personaldetails" /></strong>
						</h2>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="birthday"><spring:message
								code="label.birthday" /></label>
						<div class="col-md-6">
							<input type="text" id="birthdayDate_datepicker" name="birthday"
								style="width: 200px;" class="form-control input-datepicker"
								data-date-format="<spring:message code="label.dateformat" />"
								placeholder="<spring:message code="label.dateformat" />"
								<c:if test="${! empty birthday}">value="${birthday}"</c:if>>
						</div>
					</div>
					
					
					<div class="form-group">
						<label class="col-md-4 control-label" for="gender"><spring:message
								code="label.gender" /></label>
						<div class="col-md-6">
							<select class="gender_chosen" style="width: 200px;"
								id="memberGender" data-placeholder="select gender.."
								name="memberGender">
								<option value="Select" >Select..</option>
								<option value="Male"  <c:if test="${gender eq 'Male'}">selected="selected" </c:if>>
									<spring:message code="label.male" />
								</option>
								<option value="Female"  <c:if test="${gender eq 'Female'}"> selected="selected" </c:if>>
									<spring:message code="label.female" />
								</option>
							</select>

						</div>
					</div>
					<%-- <div class="form-group">
						<label class="col-md-4 control-label" for="maritalStatus"><spring:message
								code="label.maritalstatus" /> </label>
						<div class="col-md-6">
							<select class="marital_status_chosen" style="width: 200px;"
								id="membermaritalStatus"
								data-placeholder="select Marital Status .."
								name="memberMaritalStatus">
								<option value="Select" >Select..</option>
								<option value="Single" <c:if test="${maritalStatus eq 'Single'}"> selected="selected" </c:if>>
									<spring:message code="label.single" />
								</option>
								<option value="Married" <c:if test="${maritalStatus eq 'Married'}">selected="selected" </c:if>>
									<spring:message code="label.married" />
								</option>
							</select>

						</div>
					</div> --%>
<%-- 					<div class="form-group" id="aniversary_div"  <c:if test="${(anniversary eq 'Single') || (empty anniversary) }">style="display: none"</c:if>>
						<label class="col-md-4 control-label" for="anniversary"><spring:message
								code="label.anniversary" /></label>
						<div class="col-md-6">
							<input type="text" id="anniversaryDate_datepicker" name="anniversary"
								style="width: 200px;" class="form-control input-datepicker"
								data-date-format="<spring:message code="label.dateformat" />"
								placeholder="<spring:message code="label.dateformat" />"
								<c:if test="${! empty anniversary}">value="${anniversary}"</c:if>>
						</div>
					</div> --%>
					

				</div>
				
<!-- 				<div class="block"> -->
<!-- 					<div class="block-title"> -->
<!-- 						<h2> -->
<!-- 							<strong>User Preference</strong> -->
<!-- 						</h2> -->
<!-- 					</div> -->
<!-- 					<div class="form-group"> -->
<%-- 						<label class="col-md-4 control-label" for="cuisine"><spring:message --%>
<%-- 								code="label.cuisine" /></label> --%>
<!-- 						<div class="col-md-6"> -->
<!-- 							<select class="cuisine_chosen" -->
<%-- 								data-placeholder="<spring:message code='label.choosecuisine' />" --%>
<!-- 								multiple="" class="chosen" style="width: 200px;" -->
<!-- 								id="memberCuisine" name="memberCuisine"> -->
<%-- 								<c:forEach items="${cuisineList}" var="cuisine"> --%>
<%-- 									<c:set var="present" value="false" /> --%>
<%-- 									<c:forEach items="${cuisines}" var="restCuisine"> --%>
<%-- 										<c:if test="${cuisine.name eq restCuisine}"> --%>
<%-- 											<c:set var="present" value="true" /> --%>
<%-- 										</c:if> --%>
<%-- 									</c:forEach> --%>
<%-- 									<option value="${cuisine.name}" --%>
<%-- 										<c:if test="${present}">	selected="selected" </c:if>> --%>
<%-- 										${cuisine.name}</option> --%>

<%-- 								</c:forEach> --%>
<!-- 							</select> -->
<!-- 						</div> -->
<!-- 					</div> -->


<!-- 					<div class="form-group"> -->


<!-- 						<div class="col-sm-4"> -->
<%-- 							<label class="col-md-6 control-label" for="creditCard"><spring:message --%>
<%-- 									code="label.smoking" /></label> --%>
<!-- 							<div class="col-md-4"> -->
<%-- 								<label class="switch switch-primary"> <input <c:if test="${member.smoking}">checked</c:if> --%>
<!-- 									id="smoking" name="smoking" type="checkbox" /> <span></span></label> -->
<!-- 							</div> -->
<!-- 						</div> -->
<!-- 						<div class="col-sm-4"> -->
<%-- 							<label class="col-md-6 control-label" for="alcohol"><spring:message --%>
<%-- 									code="label.alcohol" /></label> --%>
<!-- 							<div class="col-md-4"> -->
<%-- 								<label class="switch switch-primary"> <input <c:if test="${member.preferAlcohol}">checked</c:if> --%>
<!-- 									id="alcohol" name="alcohol" type="checkbox" /> <span></span></label> -->
<!-- 							</div> -->
<!-- 						</div> -->
<!-- 						<div class="col-sm-4"> -->
<%-- 							<label class="col-md-6 control-label" for="nonVeg"><spring:message --%>
<%-- 									code="label.nonvegitarian" /> </label> --%>
<!-- 							<div class="col-md-4"> -->
<%-- 								<label class="switch switch-primary"> <input <c:if test="${member.nonvegeterian}">checked</c:if> --%>
<!-- 									id="nonVeg" name="nonVeg" type="checkbox" /> <span></span></label> -->
<!-- 							</div> -->
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 					</div> -->
			
			</div>

		</div>
		<div class="row" style="margin-left: 0px; margin-right: 0px;">
			<div class="block" style="min-height: 150px">
				<div class="form-group col-sm-6">
					<div>
						<%@ include file="/WEB-INF/views/common/captcha/page.jsp"%>
					</div>
					
				</div>
				<div class="form-group col-sm-6">
					<div class="col-md-2">
						<label class="switch switch-primary">
						 <input	id="termsofuse" name="termsofuse" type="checkbox" /> <span></span>
						</label>
					</div>
					<div class="col-md-6">
					<label class="control-label" for="termsofuse"> I
						agree to the SkillMe <a class="link" href="Termsofuse"
						target="_blank">Terms of Use</a>
					</label>
					</div>
					<div class="col-md-9 col-md-offset-3">
						<button id="member_submit" type="submit"
							class="btn btn-sm btn-primary save">
							<i class="fa fa-angle-right"></i>
							<spring:message code="button.save" />
						</button>
						<button type="reset" class="btn btn-sm btn-warning">
							<i class="fa fa-repeat"></i>
							<spring:message code="button.reset" />
						</button>
						<a class="btn btn-sm btn-primary" href="Login"><i
							class="gi gi-remove"></i> <spring:message code="button.cancel" />
						</a>
					</div>
					
				</div>

				<div class="form-group">
					
				</div>
			</div>
		</div>
	</form>
</div>
	<footer class="clearfix" style="left:0; background-color: #4f4f4f; color:#fec408; padding: 0px; position: fixed; bottom: 0px; width: 100%;">
			<div class="row" style="padding: 0px 15px;">
				<div class="col-md-3 col-xs-4" style="padding: 9px 10px;background-color: #333333;">
					<div class="pull-left" style="background-color: #333333;">
						<span id="year-copy">2014</span> &copy; <a href="http://www.overtheedgefoodsolution.com" target="_blank">OTEFS</a>
					</div>
				</div>
				<div class="col-md-3 col-xs-8 col-md-offset-6" style="padding: 9px 10px;">
					<div class="pull-right">
					   Technology Partner : <a href="http://www.mindnerves.com" target="_blank"> MindNerves Technology Services <span class="hidden-xs">Pvt. Ltd.</span></a>
					</div>
				</div>			
			</div>
		</footer>
<%-- <%@include file="/WEB-INF/views/admin/inc/page_footer.jsp"%> --%>
<%@include file="/WEB-INF/views/admin/inc/template_scripts.jsp"%>
<%@include file="/WEB-INF/views/admin/inc/chosen_scripts.jsp"%>



<script type="text/javascript">
	$(document).ready(function() {
						$(".state_chosen").chosen();
						$(".city_chosen").chosen();
						$(".country_chosen").chosen();
						$(".gender_chosen").chosen();
						$('#birthdayDate_datepicker').datepicker('setEndDate', new Date());
					
						$('select[name="chosenCountryId"]').chosen().change( function() {
							var countryId = ($(this).val());
							$.ajax({
								type: "GET",
								async: false,
								url: "<%=contexturl %>state/statebycountryid/"+countryId,
								success: function(data, textStatus, jqXHR){
									var obj = jQuery.parseJSON(data);
									var len=obj.length;
									var myOptions = "<option value></option>";
									var myCityOptions = "<option value></option>";
									for(var i=0; i<len; i++){
									myOptions += '<option value="'+obj[i].stateId+'">'+obj[i].stateName+'</option>';
									}
									$(".state_chosen").html(myOptions).chosen().trigger("chosen:updated");
									$(".city_chosen").html(myCityOptions).chosen().trigger("chosen:updated");
								},
								dataType: 'html'
							});
							
						});
						
						
						
						$('select[name="chosenStateId"]').chosen().change( function() {
							var stateId = ($(this).val());
							$.ajax({
								type: "GET",
								async: false,
								url: "<%=contexturl %>city/citybystateid/"+stateId,
								success: function(data, textStatus, jqXHR){
									var obj = jQuery.parseJSON(data);
									var len=obj.length;
									var myOptions = "<option value></option>";
									for(var i=0; i<len; i++){
									myOptions += '<option value="'+obj[i].cityId+'">'+obj[i].cityName+'</option>';
									}
									$(".city_chosen").html(myOptions).chosen().trigger("chosen:updated");
								},
								dataType: 'html'
							});
							
							
						});	

						
						
          
						$("#availability").click(
								function() {
									var data = $("#login").val();
									$("#notavailable").css("display", "none");
									$("#loading").css("display", "inline");
									$("#available").css("display", "none");
									if (data != "") {
										$.getJSON("..<%=contexturl%>ValidateLogin",
																{
																	loginId : data
																},
																function(data) {
																	if (data == "exist") {
																		$("#notavailable").css("display","inline");
																		$("#loading").css("display", "none");
																	} else if (data == "available") {
																		$("#available").css("display","inline");
																		$("#loading").css("display", "none");
																		
																	}
																});
											}
										});

						$("#member_form")
								.validate(
										{
											errorClass : "help-block animation-slideDown",
											errorElement : "div",
											errorPlacement : function(e, a) {
												a.parents(".form-group > div")
														.append(e);
											},
											highlight : function(e) {
												$(e)
														.closest(".form-group")
														.removeClass(
																"has-success has-error")
														.addClass("has-error")
											},
											success : function(e) {
												e
														.closest(".form-group")
														.removeClass(
																"has-success has-error")
											},
											rules : {

												firstName : {
													required : !0,
													maxlength : 75
												},
												lastName : {
													required : !0,
													maxlength : 75
												},
												emailId : {
													required : !0,
													maxlength : 75,
													email : !0
												},
												mobile : {
													required : !0,
													maxlength : 75,
													phone : !0
												},
												loginId : {
													required : !0,
													minlength : 6,
													maxlength : 75,
													hash:!0
												},
												password : {
													required : !0,
													minlength : 6,
													maxlength : 75
												},
												confirmPassword : {
													required : !0,
													equalTo : "#password"
												},
												memberAddressLine1 : {
													required : !0,
													maxlength : 75
												},
												memberAddressLine2 : {
													maxlength : 75
												},
												memberPincode : {
													required : !0,
													maxlength : 10
												},

												chosenCityId : {
													required : !0
												},
												chosenCountryId : {
													required : !0
												},
												chosenStateId : {
													required : !0
												},
												termsofuse : {
													required : !0
												},
												captchaText:{
													required : !0
												}
											},
											messages : {

												firstName : {
													required : '<spring:message code="validation.pleaseenterfirstname"/>',
													maxlength : '<spring:message code="validation.firstname75character"/>'
												},
												lastName : {
													required : '<spring:message code="validation.pleaseenterlastname"/>',
													maxlength : '<spring:message code="validation.lastname75character"/>'
												},
												emailId : {
													required : '<spring:message code="validation.pleaseenteremailid"/>',
													maxlength : '<spring:message code="validation.email75character"/>',
														email : '<spring:message code="validation.pleaseenteravalidemailid"/>'
												},
												mobile : {
													required : '<spring:message code="validation.pleaseenteramobilenumber"/>',
													phone : '<spring:message code="validation.phone"/>',
													maxlength : '<spring:message code="validation.mobile75character"/>'
												},
												loginId : {
													required : '<spring:message code="validation.pleaseenteraloginid"/>',
													minlength : '<spring:message code="validation.loginidmustbeatleast6character"/>',
													maxlength : '<spring:message code="validation.loginid75character"/>',
													hash :'<spring:message code="validation.pleaseentervalidloginid"/>',
												},
												password : {
													required : '<spring:message code="validation.pleaseenterapassword"/>',
													minlength : '<spring:message code="validation.passwordmustbeatleast6character"/>',
													maxlength : '<spring:message code="validation.passworddname75character"/>'
												},
												confirmPassword : {
													required : '<spring:message code="validation.pleaseconfirmpassword"/>',
													equalTo : '<spring:message code="validation.pleaseenterthesamepasswordasabove"/>'
												},

												memberAddressLine1 : {
													required : '<spring:message code="validation.pleaseenteraddressline1"/>',
													maxlength : '<spring:message code="validation.address"/>'
												},
												memberAddressLine2 : {
													maxlength : '<spring:message code="validation.address"/>'
												},
												memberPincode : {
													required : '<spring:message code="validation.pleaseenterpincode"/>',
													digits : '<spring:message code="validation.digits"/>',
													maxlength : '<spring:message code="validation.pincode75character"/>'
												},
												chosenCityId : {
													required : '<spring:message code="validation.selectcity"/>'
												},
												chosenCountryId : {
													required : '<spring:message code="validation.selectcountry"/>'
												},
												chosenStateId : {
													required : '<spring:message code="validation.selectstate"/>'
												},
												termsofuse : {
													required : 'Select terms and condition.'
												},
												captchaText:{
													required : 'Please enter Verification code'
												}
											},

										});


					});
</script>
<style>
.chosen-container {
	width: 250px !important;
}


.massage1{
	float:left;
	width:600px;
	height:auto;
	padding-left: 25px;
	margin-top: 15px;
}

.massage1_check{
	float:left;
	width:20px;
	height:40px;
	margin-top: 10px;
}

.massage1_text{
	float:left;
	width:460px;
	height:auto;
	background-color: #ECECEC;
	border: 1px solid #cccccc;
	padding: 10px;
	margin-left: 13px;
	line-height: 18px;
	font: 14px/18px MuseoSans500;
	color: #ABABAB;
}
</style>
<script type="text/javascript">
	function OpenFileDialog(){
		document.getElementById("imageChooser").click();
	}
	
	function changePhoto(){
		if($('#imageChooser').val() != ""){			
			var imageName = document.getElementById("imageChooser").value;
			var ext1 = imageName.substring(imageName.lastIndexOf("."));
			var ext = ext1.toLowerCase();
			if(ext == '.jpg' || ext == '.jpeg' || ext == '.png' || ext == 'gif' || ext == 'bmp' || ext == 'tiff')
				$('#changePhoto').submit();		
			else
				alert("Please select only image file");		
		}
	}
</script>

<script src="Scripts/jquery-1.10.2.js"></script>
<script type="text/javascript">
    function showimages(files) {
        for (var i = 0, f; f = files[i]; i++) {
           
                       
            var reader = new FileReader();
            reader.onload = function (evt) {
            	$( "#image" ).remove();
            	$( "#imagediv" ).remove();
            	
//                 var img = '<div class="col-md-6 id="imagediv"><img border="0" src="' + evt.target.result + '" width="120" height="120" title="Click here to change photo" style="cursor: pointer;"  onclick="OpenFileDialog();" class="img-circle pic" id="image"/></div>';
                  var img ='<div class="col-md-6 img-list2" id="imagediv"><img border="0" src="' + evt.target.result + '" width="120" height="120" title="Click here to change photo" onclick="OpenFileDialog();" class="img-circle pic" id="image" style="cursor: pointer;" /><span class="text-content" onclick="OpenFileDialog();" title="Click here to change photo"><br><br><span>Change Profile pic</span></span></div>'
                 $('#profilepic').append(img);
            }
            reader.readAsDataURL(f);
        }
    }

    $('#imageChooser').change(function (evt) {
        showimages(evt.target.files);
    });
</script>
<%@include file="/WEB-INF/views/admin/inc/template_end.jsp"%>