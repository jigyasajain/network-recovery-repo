<%@page
    import="org.springframework.web.servlet.support.RequestContextUtils"%>
<%@page import="com.astrika.common.util.PropsValues"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@page import="com.astrika.kernel.exception.CustomException"%>
<%@page import="org.springframework.web.util.HtmlUtils"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%
ApplicationContext ac = RequestContextUtils.getWebApplicationContext(request);
PropsValues propvalue = (PropsValues) ac.getBean("propsValues");
String contexturl=propvalue.CONTEXT_URL;
%>
<!DOCTYPE html>
<html class="no-js">
<head>
<meta charset="utf-8">

<title>Gourmet7 - An Annual Premium Paid Membership Program</title>

<meta name="description"
    content="Gourmet7 is an Annual Premium Paid Membership Program developed by  Astrika Infotech Private Limited.">
<meta name="author" content="astrikainfotech">
<meta name="robots" content="noindex, nofollow">

<meta name="viewport"
    content="width=device-width,initial-scale=1,maximum-scale=1.0">
<style type="text/css">
.jqstooltip {
    width: auto !important;
    height: auto !important;
    position: absolute;
    left: 0px;
    top: 0px;
    visibility: hidden;
    background: #000000;
    color: white;
    font-size: 11px;
    text-align: left;
    white-space: nowrap;
    padding: 5px;
    z-index: 10000;
}

.jqsfield {
    color: white;
    font: 10px arial, san serif;
    text-align: left;
}

#login-background {
    height: 100% !important;
}

body {
    height: 900px;
}

.input-group {
margin-bottom: 10px !important;
}

p{
 margin: 0 !important;
}
</style>

<link rel="stylesheet"
    href="<%=contexturl %>resources/css/bootstrap.min.css">

<link rel="stylesheet" href="<%=contexturl %>resources/css/plugins.css">

<link rel="stylesheet" href="<%=contexturl %>resources/css/main.css">

<link rel="stylesheet" href="<%=contexturl %>resources/css/themes.css">

<script
    src="<%=contexturl %>resources/js/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
</head>
<body>
    <div id="login-background" style="height: 900px; overflow: hidden;">
        <!-- For best results use an image with a resolution of 2560x400 pixels (prefer a blurred image for smaller file size) -->
        <img
            src="<%=contexturl %>resources/img/placeholders/photos/photo7@2x.jpg"
            alt="Login Background" class="animation-pulseSlow">
    </div>
    <!-- END Login Background -->

    <!-- Login Container -->
    <div id="login-container" class="animation-fadeIn">
        <!-- Login Title -->
        <div class="login-title text-center">
            <h1>
                <img src="<%=contexturl %>resources/img/logo.png"> <br>
                <small>Please <strong>Login</strong></small>
            </h1>
        </div>
        <!-- END Login Title -->
        <div class="block remove-margin" style="min-height: 280px;height: 100%;position: relative;float: left;width: 100%;">
            <c:if test="${ !empty errorCode}">
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert"
                    aria-hidden="true">x</button>
                <h4>
                    <i class="fa fa-times-circle"></i> Error
                </h4>
                <spring:message code='${errorCode}' />
            </div>
            </c:if>
                <form action="<%=contexturl %>customLogin"
                    method="post" id="candidate_login" class="mt10">
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="gi gi-user"></i>
                                </span> <input type="text" id="email" name="j_username"
                                    class="form-control input-lg" placeholder="Login Id">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="gi gi-asterisk"></i>
                                </span> <input type="password" id="pass" name="j_password"
                                    class="form-control input-lg" placeholder="Password">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-actions" style="padding-bottom:20px;">                  
                        <div class="col-xs-4" style="width: 50%;">
                            <p>
                                <input type="hidden" name="client" value="WEB">
                                <input type="hidden" name="url" value="<%=contexturl %>CorporateLogin">
                                <a href="ForgotLogin"><small>Forgot Login Id ?</small> </a>
                            </p>
                        </div>
                        
                        <div class="col-xs-4" style="text-align: right;width: 50%;">
                            <p>
                                <a href="ForgotPassword"><small>Forgot Password ?</small> </a>
                            </p>
                        </div>
                        <div class="col-xs-8 text-right" style="width: 100%;  text-align: center;margin-top: 20px">
                            <button type="submit" class="btn btn-block btn-primary" style="width: 100%">
                                <i class="fa fa-angle-right"></i> 
                                Login
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="login-title text-center" style="padding: 1px">
                    <h5>
                        <small style="color: #fff">Not a member yet? <a href="Register">Sign Up</a></small> 
                    </h5>
                </div>
        </div>
        
    </div>
<script src="<%=contexturl %>resources/js/vendor/jquery-1.11.0.min.js"></script>
<!-- Bootstrap.js, Jquery plugins and Custom JS code -->
<script src="<%=contexturl %>resources/js/vendor/bootstrap.min.js"></script>
<script src="<%=contexturl %>resources/js/plugins.js"></script>
<script src="<%=contexturl %>resources/js/app.js"></script>
        <!-- Load and execute javascript code used only in this page -->
        <script src="<%=contexturl %>resources/js/pages/login.js"></script>
        <script>$(function(){ Login.init(); });</script>
        <script type="text/javascript">
    $(document).ready(function() {
    $("#candidate_login").validate(
                {   errorClass:"help-block animation-slideDown",
                    errorElement:"div",
                    errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
                    highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
                    success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
                    rules:{ j_username:{required:!0},   j_password:{required:!0}                                
                    },
                    messages:{
                        j_username : {
                            required : "<spring:message code='validation.pleaseenteraloginid'/>"
                        }   ,
                        j_password : {
                            required : "<spring:message code='validation.pleaseenterapassword'/>"
                        }   
                    }
                });     
    });
    
</script>
</body>
</html>