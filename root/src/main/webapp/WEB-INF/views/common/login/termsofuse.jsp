<%@include file="/WEB-INF/views/admin/inc/config.jsp"%>
<%@include file="/WEB-INF/views/admin/inc/template_start.jsp"%>
<div id="page-container"
	class="sidebar-partial sidebar-visible-lg sidebar-no-animations style-alt footer-fixed">
	<!-- START Header -->
	<header class="navbar navbar-inverse">
		<!-- Navbar Header -->
		<div class="navbar-header">
			<!-- Main Sidebar Toggle Button -->
			<ul class="nav navbar-nav-custom">

				<!-- Brand -->
				<a href="Index" class="sidebar-brand-logo"> <img
					src="<%=contexturl%>resources/img/logo.png">
				</a>
				<!-- END Brand -->
			</ul>
			<!--                         <ul> -->
			<!--                           <a class="btn btn-sm btn-primary" href="Login"> -->
			<!-- 								  <i class="gi gi-remove"></i>Back to Login Page  -->
			<!-- 				             </a> -->
			<!--                         </ul> -->
		</div>
</div>
<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				<i class="fa fa-map-marker"></i> Terms of Use <br> <small></small>
			</h1>
			<span id="errorMsg"></span>
		</div>
	</div>
	<div class="terms_text">
		<ol>
			<li><strong>Eligibility:</strong> <br /> Usage of services
				offered by Gourmet7.com is subject to the terms of a legally
				binding agreement between the "User" and "Over the Edge Food
				Solutions Pvt. Ltd.". The "User" is the individual who wishes to
				browse the contents on the website or avail and kind of services
				through it. "Over the Edge Food Solutions Pvt. Ltd." is the
				proprietor of www.Gourmet7.com and throughout this agreement
				shall be referred as "Gourmet7.com". To enter in to the
				agreement, the user must be at least 18 years of age (Sorry for the
				ones below it. See you when you are 18) and must have read and
				understood (more importantly) the terms and conditions completely.
				If you do not agree to the terms specified or are less than 18 years
				of age, we would be sad, but still request you to not use the
				services of this website.</li>
			<li><strong>Privacy Policy:</strong> <br /> To avail any of our
				services, it is essential for a User to be registered with us. For
				the same, we would require certain personal information such as full
				name, contact number(s) and email address, as part of the
				registration process. User must ensure that any registration
				information given to Gourmet7.com should always be accurate,
				correct and up to date. At Gourmet7.com, we understand the value
				of hard earned money and hence, even when using third party gateway
				for payments, we have taken our best measures to ensure the user
				feels secured to use it. At the same time, we also respect
				everyone's privacy and hence, feel absolutely assured, the data
				provided will remain strictly confidential and will not be misused
				or distributed in anyways.</li>
			<li><strong>Intellectual Property Rights:</strong> <br /> User
				agrees not to use data or any information, pictures, etc., which is
				owned by or licensed to Gourmet7.com, for any commercial or
				personal use. Unless, agreed to by Gourmet7.com in writing, User
				shall not be allowed to reproduce, duplicate, copy, sell, trade or
				resell the Services rendered by Gourmet7.com for any purpose.</li>
			<li><strong>Guarantee &amp; Liability:</strong> <br />
				Although we would make our best efforts to provide the users with up
				to date and accurate information along with impeccable services from
				our end, under no circumstances, shall we be responsible for any
				flawed information/services on the part of the third party clients.
			</li>
			<li><strong>Report Abuse</strong> <br /> Being an interactive
				web site, we encourage our users to share contents with us. While
				doing this, we take absolute care while listing the same on our
				website. The content goes through enhanced scanning by our
				webmasters and only after clearance from them, the content is
				listed. Incase the user finds any content on the web site to be
				objectionable, the user is requested to immediately intimate us by
				sending an email on admin@Gourmet7.com.</li>
			<li><strong>Advertising</strong> <br /> In order to assist to
				serve you even better, Gourmet7.com is ably supported by
				advertising revenue and may display third-party advertisements and
				promotions. These advertisements are strictly displayed as per the
				terms and conditions agreed upon with the advertiser. The content
				and display duration of advertisements displayed on Gourmet7.com
				are subject to change without specific prior notice to User. By
				advertising, HungWungry.com merely spreads the awareness about third
				party products or services and does not necessarily offer or sell
				these until and unless clearly specified. Under no circumstances,
				Gourmet7.com shall be held responsible for any disputes arising
				between the User and the advertisers.</li>
			<li><strong>Third Party Content</strong> <br /> Some contents
				displayed on Gourmet7.com may include hyperlinks to other web
				sites or contents. In such cases, Gourmet7.com may have
				absolutely no control over any web sites or resources which are
				provided by entities other than Gourmet7.com Please be reminded
				that until and unless clearly specified, we do not endorse any
				advertising, products or other materials on or available from such
				web sites or resources. User acknowledges and agrees that
				Gourmet7.com is not liable for any loss or damage which may be
				incurred by User as a result.</li>
		</ol>
	</div>
</div>
<%@include file="/WEB-INF/views/admin/inc/template_end.jsp"%>