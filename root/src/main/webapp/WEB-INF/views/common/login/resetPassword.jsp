<%@include file="../../admin/inc/config.jsp"%>
<%@include file="../../admin/inc/template_start.jsp"%>

<style type="text/css">
.jqstooltip {
	width: auto !important;
	height: auto !important;
	position: absolute;
	left: 0px;
	top: 0px;
	visibility: hidden;
	background: #000000;
	color: white;
	font-size: 11px;
	text-align: left;
	white-space: nowrap;
	padding: 5px;
	z-index: 10000;
}

.jqsfield {
	color: white;
	font: 10px arial, san serif;
	text-align: left;
}

#login-background{
	height: 100% !important;
}
body{
height:900px;
}
</style>

<!-- Login Background -->
<div id="login-background" style="height: 900px">
    <!-- For best results use an image with a resolution of 2560x400 pixels (prefer a blurred image for smaller file size) -->
    <img src="<%=contexturl %>resources/img/placeholders/photos/photo7@2x.jpg" alt="Login Background" class="animation-pulseSlow">
</div>
<!-- END Login Background -->

	<div id="login-container" class="animation-fadeIn">
		<div class="login-title text-center">
			<h1>
				<strong>Atma</strong><br>
			</h1>
		</div>
		<div class="block remove-margin" style="height: 275px;">
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i> Success
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>
			
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i> Error
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>
			<form action="SubmitResetPassword"
				method="post" id="candidate_login" class="mt10">
				<div class="form-group">
					<div class="col-xs-12">
						<div class="input-group">
							<span class="input-group-addon"><i class="gi gi-asterisk"></i>
							</span> <input type="password" id="pass" name="password1"
								class="form-control input-lg" placeholder="Password">
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-12">
						<div class="input-group">
							<span class="input-group-addon"><i class="gi gi-asterisk"></i>
							</span> <input type="password" id="confirmPass" name="password2"
								class="form-control input-lg" placeholder="Confirm Password">
								<input type="hidden" name="token" value="${token}" />
						</div>
					</div>
				</div>
				<div class="form-group form-actions">
					<div class="col-xs-4">
						<p>
							<a href="Login" id="link-login" class="save"><small>Go back to Login Page</small> </a>
						</p>
					</div>
					<div class="col-xs-8 text-right">
						<button type="submit" class="btn btn-sm btn-primary save ">
							<i class="fa fa-angle-right"></i> Submit
						</button>
					</div>
				</div><span class="clearfix"></span>
			</form>
		</div>
	</div>


<%@include file="../../admin/inc/template_scripts.jsp"%>

<!-- Load and execute javascript code used only in this page -->
<script src="resources/js/pages/login.js"></script>
<script>$(function(){ Login.init(); });</script>
<script type="text/javascript">
	$(document).ready(function() {
	$("#candidate_login").validate(
				{	errorClass:"help-block animation-slideDown",
					errorElement:"div",
					errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
					highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
					success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
					rules:{ password1:{required:!0,minlength : 6},	password2	:{required:!0,equalTo : "#pass"}					
					},
					messages:{
						password1 : {
							required : "Please enter a password",
							 minlength : "Password must be atleast 6 character"
						}	,
						password2 : {
							required : "Please confirm password",
							equalTo : "Please enter the same password as above"
						}	
					}
				});		
	});
	
</script>

<%@include file="../../admin/inc/template_end.jsp"%>