<%@page import="org.springframework.web.servlet.support.RequestContextUtils"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@ include file="/WEB-INF/views/common/captcha/init.jsp" %>

<%
boolean captchaEnabled = false;

%>

<c:if test="<%= captchaEnabled %>">
	<div class="taglib-captcha">
	 	<img alt="Text to identify" class="captcha" src="<c:url value="/MemberCaptcha" />">
		<a class="captcha-reload" onclick="reloadcaptcha();" style="cursor: pointer">
		<img src="<%=contexturl %>resources/img/application/refresh.gif" alt="Reload-Capcha" height="30px" width="35px" style="padding: 0 5px;" />
		<label style="width: 200px;text-decoration: none;color: #666;">Get New Captcha</label>
		</a>
	</div>

	<label class="col-md-4 control-label" for="login">	Text-Verification :<span class="text-danger">*</span></label><input  name="captchaText" size="10" type="text" value="" />



	<script type="text/javascript">
					function reloadcaptcha() {
						  jQuery(".captcha").attr("src", jQuery(".captcha").attr("src")+"?force=" + new Date().getMilliseconds());
						  return false;
						}

					</script>

</c:if>
