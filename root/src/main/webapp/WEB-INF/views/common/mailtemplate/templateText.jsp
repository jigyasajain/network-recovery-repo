<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" type="text/css" href="resources/css/jquery.cleditor.css" />
<script src="js/jquery-1.9.1.js" type="text/javascript"></script>
<script type="text/javascript" src="js/jquery.cleditor.js"></script>
<style>

.cleditorToolbar {background: url('resources/images/toolbar.gif') repeat}
.cleditorButton {float:left; width:24px; height:24px; margin:1px 0 1px 0; background: url('resources/images/buttons.gif')}

</style>
<script type="text/javascript">
	$(document).ready(function() {
		$("#input").cleditor(); 
	});
</script>
<textarea id="input" name="templateText">${templateText}</textarea>
<input type="hidden" name="templateName" value="${templateName}"/>
