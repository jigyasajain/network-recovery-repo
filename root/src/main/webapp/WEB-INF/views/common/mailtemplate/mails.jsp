<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.springframework.web.context.WebApplicationContext"%>
<%@page import="com.astrika.common.util.PropsValues"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
	<%
	WebApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(application);
	PropsValues props=(PropsValues)context.getBean("propsValues");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Gourmet7 | Application Emails</title>

<script src="js/jquery-1.9.1.js" type="text/javascript"></script>

<script type="text/javascript">
	$(document).ready(function() {
	
		$("#authentication").click(function(){
			$.ajax({
				type: "GET",
				async: false,
				url: "EditTemplate",
 				data: { template:"<%= props.memberAuthentication %>" }, 
				success: function(data, textStatus, jqXHR){
					$('#editTemplate').html(data);
				},
				dataType: 'html'
			});
		});
	});
</script>
</head>

<body>

	<div style="width: 90%">
		<div style="width: 70%"><%= props.memberAuthentication %></div>
		<div style="width: 30%" id="authentication">EDIT</div>
	</div>
	
	<form action="/SaveEmailTemplate" method="post">
		<div id="editTemplate" style="width: 600px">
		</div>
		<input type="submit" value="Save">
	</form>
</body>
</html>