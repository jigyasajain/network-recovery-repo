<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->



<%-- <%@page import="com.astrika.outletmngt.model.OutletFeedback"%> --%>
<%@page import="java.util.Date"%>
<%@include file="../inc/config.jsp"%>
<%@include file="../inc/template_start.jsp"%>
<%@include file="../inc/page_head.jsp"%>
<style>
.feedback_profile {
    height: 30px;
    width: 30px !important;
}

.ratinglable {
    float: left;
}

.row {
    margin-bottom: 5px;
}

.col-md-2 {
    width: 100% !important;
}

.ratingwid {
    width: 269px;
}

.rating_value {
    width: 100% !important;
}
</style>

<!-- Page content -->
<div id="page-content">
    <!-- Dashboard Header -->
    <!-- For an image header add the class 'content-header-media' and an image as in the following example -->
    <div class="content-header content-header-media" style="height: 450px">
        <div class="header-section">
            <div class="row">
                <!-- Main Title (hidden on small devices for the statistics to fit) -->
<!--                <div class="col-md-4 col-lg-6 hidden-xs hidden-sm"> -->
<!--                    <h1> -->
<%--                        <strong>${restaurant.name}</strong><br> <small>${restaurant.brand.company.companyName} --%>
<%--                            / ${restaurant.brand.brandName}</small> --%>
<!--                    </h1> -->
<!--                </div> -->
                <!-- END Main Title -->

                <!-- Top Stats -->
<!--                <div class="col-md-8 col-lg-6"> -->
<!--                    <div class="row text-center"> -->
<!--                        <div class="col-xs-4 col-sm-3"> -->
<!--                            <h2 class="animation-hatch"> -->
<%--                                <strong>${followedCount}</strong><br> <small><spring:message code="label.follower"/></small> --%>
<!--                            </h2> -->
<!--                        </div> -->
<!--                        <div class="col-xs-4 col-sm-3"> -->
<!--                            <h2 class="animation-hatch"> -->
<%--                                <strong>${feedBackCount}</strong><br> <small> --%>
<%--                                    <spring:message code="label.feedback"/></small> --%>
<!--                            </h2> -->
<!--                        </div> -->
<!--                        <div class="col-xs-4 col-sm-3"> -->
<!--                            <h2 class="animation-hatch"> -->
<%--                                <strong>${restaurant.ratingsCount}</strong><br> <small><spring:message code="label.rating"/></small> --%>
<!--                            </h2> -->
<!--                        </div> -->
<!--                        We hide the last stat to fit the other 3 on small devices -->
<!--                        <div class="col-sm-3 hidden-xs"> -->
<!--                            <h2 class="animation-hatch"> -->
<%--                                <strong>${promocodeUsed}</strong><br> <small><spring:message code="label.promocodeused"/></small> --%>
<!--                            </h2> -->
<!--                        </div> -->
<!--                    </div> -->
<!--                </div> -->
                <!-- END Top Stats -->
            </div>
        </div>
        <!-- For best results use an image with a resolution of 2560x248 pixels (You can also use a blurred image with ratio 10:1 - eg: 1000x100 pixels - it will adjust and look great!) -->
        <c:choose>
            <c:when test="${!empty restaurant.profileImage.imagePath}">

                <img src="<%=contexturl%>${restaurant.profileImage.imagePath}"
                    alt="header image" class="animation-pulseSlow" style="height: 100% !important;">
            </c:when>
            <c:otherwise>
                <img
                    src="<%=contexturl%>resources/img/placeholders/headers/dashboard_header.jpg"
                    alt="header image" class="animation-pulseSlow" style="height: 100% !important;">
            </c:otherwise>
        </c:choose>
    </div>
    <!-- END Dashboard Header -->

    <!-- Mini Top Stats Row -->
    <div class="row">
        <!--        <div class="col-sm-6 col-lg-3"> -->
        <!--            <!-- Widget -->
        <!--            <div class="widget"> -->
        <!--                <div class="widget-simple"> -->
        <!--                    <a href="page_comp_charts.php" -->
        <!--                        class="widget-icon pull-left themed-background-autumn animation-fadeIn"> -->
        <!--                        <i class="gi gi-user"></i> -->
        <!--                    </a> -->
        <!--                    <h3 class="widget-content text-right animation-pullDown"> -->
        <%--                        <strong>Un Followed</strong><br>This month <small>${unfollowedCount}</small> --%>
        <%--                        <br>Total:<small>${totalUnfollowedCount}</small> --%>
        <!--                    </h3> -->
        <!--                </div> -->
        <!--            </div> -->
        <!--            <!-- END Widget -->
        <!--        </div> -->



        <div class="col-sm-6">
            <!-- Widget -->
            <div class="widget">
                <div class="widget-simple">
                    <a href="page_comp_charts.php"
                        class="widget-icon pull-left themed-background animation-fadeIn">
                        <i class="gi gi-user"></i>
                    </a>
                    <div class="pull-right">
                        Total: <strong>${totalFollowedCount}</strong>
                    </div>
                    <h3 class="widget-content animation-pullDown">
                        <strong>Followed</strong><br>
                        <small>This month </small><strong>${followedCount}</strong>
                    </h3>
                </div>
            </div>
            <!-- END Widget -->
        </div>


        <div class="col-sm-6">
            <!-- Widget -->
            <div class="widget">
                <div class="widget-simple">
                    <a href="page_comp_charts.php"
                        class="widget-icon pull-left themed-background animation-fadeIn">
                        <i class="gi gi-user"></i>
                    </a>
                    <div class="pull-right">
                        Total:<strong>${totalUnfollowedCount}</strong>
                    </div>
                    <h3 class="widget-content animation-pullDown">
                        <strong>Un Followed</strong><br> <small>This month</small><strong>${unfollowedCount}</strong>
                    </h3>
                </div>
            </div>
            <!-- END Widget -->
        </div>





        <!--        <div class="col-sm-6 col-lg-3"> -->
        <!--            <!-- Widget -->
        <!--            <div class="widget"> -->
        <!--                <div class="widget-simple"> -->
        <!--                    <a href="#" -->
        <!--                        class="widget-icon pull-left  themed-background-spring animation-fadeIn"> -->
        <!--                        <i class="gi gi-user"></i> -->
        <!--                    </a> -->
        <!--                    <h3 class="widget-content text-right animation-pullDown"> -->
        <%--                        <strong>Followed</strong><br>This month <small>${followedCount}</small> --%>
        <%--                        <br>Total: <small>${totalFollowedCount}</small> --%>
        <!--                    </h3> -->
        <!--                </div> -->
        <!--            </div> -->
        <!--            <!-- END Widget -->
        <!--        </div> -->

        <div class="col-sm-6 col-lg-3">
            <!-- Widget -->
            <div class="widget">
                <div class="widget-simple">
                    <a href="#"
                        class="widget-icon pull-left themed-background-fire animation-fadeIn">
                        <i class="gi gi-envelope"></i>
                    </a>
                    <h3 class="widget-content text-right animation-pullDown">
                        5 <strong>Messages</strong> <small>Support Center</small>
                    </h3>
                </div>
            </div>
            <!-- END Widget -->
        </div>
        <div class="col-sm-6 col-lg-3">
            <!-- Widget -->
            <div class="widget">
                <div class="widget-simple">
                    <a href="page_comp_gallery.php"
                        class="widget-icon pull-left themed-background-amethyst animation-fadeIn">
                        <i class="gi gi-picture"></i>
                    </a>
                    <h3 class="widget-content text-right animation-pullDown">
                        +30 <strong>Photos</strong> <small>Gallery</small>
                    </h3>
                </div>
            </div>
            <!-- END Widget -->
        </div>
        <div class="col-sm-6">
            <!-- Widget -->
            <div class="widget">
                <div class="widget-simple">
                    <a href="page_comp_charts.php"
                        class="widget-icon pull-left themed-background animation-fadeIn">
                        <i class="gi gi-wallet"></i>
                    </a>
                    <div class="pull-right">
                        <!-- Jquery Sparkline (initialized in js/pages/index.js), for more examples you can check out http://omnipotent.net/jquery.sparkline/#s-about -->
                        <span id="mini-chart-sales"></span>
                    </div>
                    <h3 class="widget-content animation-pullDown visible-lg">
                        Latest <strong>Sales</strong> <small>Per hour</small>
                    </h3>
                </div>
            </div>
            <!-- END Widget -->
        </div>
<!--        <div class="col-sm-6"> -->
<!--            <!-- Widget --> 
<!--            <div class="widget"> -->
<!--                <div class="widget-simple"> -->
<!--                    <a href="page_widgets_stats.php" -->
<!--                        class="widget-icon pull-left themed-background animation-fadeIn"> -->
<!--                        <i class="gi gi-crown"></i> -->
<!--                    </a> -->
<!--                    <div class="pull-right"> -->
<!--                        Jquery Sparkline (initialized in js/pages/index.js), for more examples you can check out http://omnipotent.net/jquery.sparkline/#s-about -->
<!--                        <span id="mini-chart-brand"></span> -->
<!--                    </div> -->
<!--                    <h3 class="widget-content animation-pullDown visible-lg"> -->
<!--                        Our <strong>Brand</strong> <small>Popularity over time</small> -->
<!--                    </h3> -->
<!--                </div> -->
<!--            </div> -->
<!--        </div> -->
    </div>
    <!-- END Mini Top Stats Row -->

    <!-- Widgets Row -->
    <div class="row">
        <div class="col-md-6">
            <!-- Timeline Widget -->
            <div class="widget">
                <div class="widget-extra themed-background-dark">
                    <div class="widget-options">
                        <div class="btn-group btn-group-xs">
                            <a href="javascript:void(0)" class="btn btn-default"
                                data-toggle="tooltip" title="Edit Widget"><i
                                class="fa fa-pencil"></i></a> <a href="javascript:void(0)"
                                class="btn btn-default" data-toggle="tooltip"
                                title="Quick Settings"><i class="fa fa-cog"></i></a>
                        </div>
                    </div>
                    <h3 class="widget-content-light">
                        Latest <strong>Feedbacks</strong> <small><a
                            href="<%=contexturl %>RestaurantAdmin/Restaurant/RestaurantRatingAndFeedback?outletId=<%=user.getModuleId()%>"><strong>View
                                    all</strong></a></small>
                    </h3>
                </div>



                <div class="widget-extra">
                    <!-- Timeline Content -->
                    <div class="timeline">
                        <ul class="timeline-list">
                            <c:forEach var="currentOutletFeedback"
                                items="${outletFeedbackList}" varStatus="count">
                                <li class="active">
                                    <div class="timeline-icon">
                                        <c:choose>
                                            <c:when
                                                test="${! empty currentOutletFeedback.user.profileImage and !empty currentOutletFeedback.user.profileImage.imagePath}">
                                                <img
                                                    src="<%=contexturl%>${currentOutletFeedback.user.profileImage.imagePath}"
                                                    alt="avatar" class="pull-left img-circle feedback_profile">

                                            </c:when>
                                            <c:otherwise>
                                                <img
                                                    src="<%=contexturl%>resources/img/application/default_profile.png"
                                                    alt="avatar" class="pull-left img-circle feedback_profile">

                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                    <div class="timeline-time">
                                        <small><fmt:formatDate
                                                pattern="<%=propvalue.simpleDateFormat %>"
                                                value="${currentOutletFeedback.createdDate}" /></small>
                                    </div>
                                    <div class="timeline-content">
                                        <p class="push-bit">
                                            <a href=""><strong>${currentOutletFeedback.user.firstName}:</strong></a>
                                        </p>
                                        <p class="push-bit">${currentOutletFeedback.feedback}</p>
                                        <p class="push-bit">
                                            <a
                                                href="<%=contexturl %>RestaurantAdmin/Restaurant/RestaurantRatingAndFeedback?outletId=<%=user.getModuleId()%>"
                                                class="btn btn-xs btn-primary"><i class="fa fa-file"></i>
                                                Reply</a>
                                        </p>

                                    </div>
                                </li>
                            </c:forEach>
                        </ul>
                    </div>
                    <!-- END Timeline Content -->
                </div>
                <!--                 <div class="widget-extra"> -->
                <!--                     Timeline Content -->
                <!--                     <div class="timeline"> -->
                <!--                         <ul class="timeline-list"> -->
                <!--                             <li class="active"> -->
                <!--                                 <div class="timeline-icon"><i class="gi gi-airplane"></i></div> -->
                <!--                                 <div class="timeline-time"><small>just now</small></div> -->
                <!--                                 <div class="timeline-content"> -->
                <!--                                     <p class="push-bit"><a href="page_ready_user_profile.php"><strong>Jordan Carter</strong></a></p> -->
                <!--                                     <p class="push-bit">The trip was an amazing and a life changing experience!!</p> -->
                <!--                                     <p class="push-bit"><a href="page_ready_article.php" class="btn btn-xs btn-primary"><i class="fa fa-file"></i> Read the article</a></p> -->
                <!--                                     <div class="row push"> -->
                <!--                                         <div class="col-sm-6 col-md-4"> -->
                <%--                                             <a href="<%=contexturl %>resources/img/placeholders/photos/photo1.jpg" data-toggle="lightbox-image"> --%>
                <%--                                                 <img src="<%=contexturl %>resources/img/placeholders/photos/photo1.jpg" alt="image"> --%>
                <!--                                             </a> -->
                <!--                                         </div> -->
                <!--                                         <div class="col-sm-6 col-md-4"> -->
                <%--                                             <a href="<%=contexturl %>resources/img/placeholders/photos/photo22.jpg" data-toggle="lightbox-image"> --%>
                <%--                                                 <img src="<%=contexturl %>resources/img/placeholders/photos/photo22.jpg" alt="image"> --%>
                <!--                                             </a> -->
                <!--                                         </div> -->
                <!--                                     </div> -->
                <!--                                 </div> -->
                <!--                             </li> -->
                <!--                             <li class="active"> -->
                <!--                                 <div class="timeline-icon themed-background-fire themed-border-fire"><i class="fa fa-file-text"></i></div> -->
                <!--                                 <div class="timeline-time"><small>5 min ago</small></div> -->
                <!--                                 <div class="timeline-content"> -->
                <!--                                     <p class="push-bit"><a href="page_ready_user_profile.php"><strong>Administrator</strong></a></p> -->
                <!--                                     <strong>Free courses</strong> for all our customers at A1 Conference Room - 9:00 <strong>am</strong> tomorrow! -->
                <!--                                 </div> -->
                <!--                             </li> -->
                <!--                             <li class="active"> -->
                <!--                                 <div class="timeline-icon"><i class="gi gi-drink"></i></div> -->
                <!--                                 <div class="timeline-time"><small>3 hours ago</small></div> -->
                <!--                                 <div class="timeline-content"> -->
                <!--                                     <p class="push-bit"><a href="page_ready_user_profile.php"><strong>Ella Winter</strong></a></p> -->
                <!--                                     <p class="push-bit"><strong>Happy Hour!</strong> Free drinks at <a href="javascript:void(0)">Cafe-Bar</a> all day long!</p> -->
                <!--                                     <div id="gmap-timeline" class="gmap"></div> -->
                <!--                                 </div> -->
                <!--                             </li> -->
                <!--                             <li class="active"> -->
                <!--                                 <div class="timeline-icon"><i class="fa fa-cutlery"></i></div> -->
                <!--                                 <div class="timeline-time"><small>yesterday</small></div> -->
                <!--                                 <div class="timeline-content"> -->
                <!--                                     <p class="push-bit"><a href="page_ready_user_profile.php"><strong>Patricia Woods</strong></a></p> -->
                <!--                                     <p class="push-bit">Today I had the lunch of my life! It was delicious!</p> -->
                <!--                                     <div class="row push"> -->
                <!--                                         <div class="col-sm-6 col-md-4"> -->
                <%--                                             <a href="<%=contexturl %>resources/img/placeholders/photos/photo23.jpg" data-toggle="lightbox-image"> --%>
                <%--                                                 <img src="<%=contexturl %>resources/img/placeholders/photos/photo23.jpg" alt="image"> --%>
                <!--                                             </a> -->
                <!--                                         </div> -->
                <!--                                     </div> -->
                <!--                                 </div> -->
                <!--                             </li> -->
                <!--                             <li class="active"> -->
                <!--                                 <div class="timeline-icon themed-background-fire themed-border-fire"><i class="fa fa-smile-o"></i></div> -->
                <!--                                 <div class="timeline-time"><small>2 days ago</small></div> -->
                <!--                                 <div class="timeline-content"> -->
                <!--                                     <p class="push-bit"><a href="page_ready_user_profile.php"><strong>Administrator</strong></a></p> -->
                <!--                                     To thank you all for your support we would like to let you know that you will receive free feature updates for life! You are awesome! -->
                <!--                                 </div> -->
                <!--                             </li> -->
                <!--                             <li class="active"> -->
                <!--                                 <div class="timeline-icon"><i class="fa fa-pencil"></i></div> -->
                <!--                                 <div class="timeline-time"><small>1 week ago</small></div> -->
                <!--                                 <div class="timeline-content"> -->
                <!--                                     <p class="push-bit"><a href="page_ready_user_profile.php"><strong>Nicole Ward</strong></a></p> -->
                <!--                                     <p class="push-bit">Consectetur adipiscing elit. Maecenas ultrices, justo vel imperdiet gravida, urna ligula hendrerit nibh, ac cursus nibh sapien in purus. Mauris tincidunt tincidunt turpis in porta. Integer fermentum tincidunt auctor. Vestibulum ullamcorper, odio sed rhoncus imperdiet, enim elit sollicitudin orci, eget dictum leo mi nec lectus. Nam commodo turpis id lectus scelerisque vulputate.</p> -->
                <!--                                     Integer sed dolor erat. Fusce erat ipsum, varius vel euismod sed, tristique et lectus? Etiam egestas fringilla enim, id convallis lectus laoreet at. Fusce purus nisi, gravida sed consectetur ut, interdum quis nisi. Quisque egestas nisl id lectus facilisis scelerisque? Proin rhoncus dui at ligula vestibulum ut facilisis ante sodales! Suspendisse potenti. Aliquam tincidunt sollicitudin sem nec ultrices. Sed at mi velit. Ut egestas tempor est, in cursus enim venenatis eget! Nulla quis ligula ipsum. -->
                <!--                                 </div> -->
                <!--                             </li> -->
                <!--                             <li class="text-center"> -->
                <!--                                 <a href="javascript:void(0)" class="btn btn-xs btn-default">View more..</a> -->
                <!--                             </li> -->
                <!--                         </ul> -->
                <!--                     </div> -->
                <!--                     END Timeline Content -->
                <!--                 </div> -->
            </div>
            <!-- END Timeline Widget -->

            <div class="widget">
                <div class="widget-extra themed-background-dark">
                    <div class="widget-options">
                        <div class="btn-group btn-group-xs">
                            <a href="javascript:void(0)" class="btn btn-default"
                                data-toggle="tooltip" title="Edit Widget"><i
                                class="fa fa-pencil"></i></a> <a href="javascript:void(0)"
                                class="btn btn-default" data-toggle="tooltip"
                                title="Quick Settings"><i class="fa fa-cog"></i></a>
                        </div>
                    </div>
                    <h3 class="widget-content-light">
                        Latest <strong>Rating</strong> <small><a
                            href="<%=contexturl %>RestaurantAdmin/Restaurant/RestaurantRatingAndFeedback?outletId=<%=user.getModuleId()%>"><strong>View
                                    all</strong></a></small>
                    </h3>
                </div>

                <div class="widget-extra">
                    <!-- Block 1 -->
                    <div class="row">
                        <div class="col-xs-6 block">
                            <div class="block-title">
                                <h4>Pervious Month</h4>
                            </div>
                            <div id="pervious_details_avg_rating_contatiner" class="row">
                                <span class="col-md-2">Average:</span>
                                <div id="pervious_rating" class="col-md-2"
                                    style="cursor: pointer;"></div>
                            </div>
                            <div id="my_rating_div" class="block" style="border: none;">
    
                                <div id="pervious_details_usr_food_rating_contatiner" class="row">
                                    <span class="col-md-2">Food:</span>
                                    <div id="pervious_rating1" class="col-md-2"
                                        style="cursor: pointer;"></div>
                                </div>
                                <div id="pervious_details_usr_ambiance_rating_contatiner"
                                    class="row">
                                    <span class="col-md-2">Ambiance:</span>
                                    <div id="pervious_rating2" class="col-md-2"
                                        style="cursor: pointer;"></div>
                                </div>
                                <div id="pervious_details_usr_service_rating_contatiner"
                                    class="row">
                                    <span class="col-md-2">Service:</span>
                                    <div id="pervious_rating3" class="col-md-2"
                                        style="cursor: pointer;"></div>
                                </div>
                                <div id="pervious_details_usr_valueForMoney_rating_contatiner"
                                    class="row">
                                    <span class="col-md-2">Value For Money:</span>
                                    <div id="pervious_rating4" class="col-md-2"
                                        style="cursor: pointer;"></div>
                                </div>
                                <div id="pervious_details_usr_hygiene_rating_contatiner"
                                    class="row">
                                    <span class="col-md-2">Hygiene:</span>
                                    <div id="pervious_rating5" class="col-md-2"
                                        style="cursor: pointer;"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6  block">
                        <div class="block-title">
                            <h4>Current Month</h4>
                        </div>
                        <div id="b_details_avg_rating_contatiner" class="row">
                            <span class="col-md-2">Average:</span>
                            <div id="current_rating" class="col-md-2"
                                style="cursor: pointer;"></div>
                        </div>
                        <div id="my_rating_div" class="block" style="border: none;">
                            <div id="b_details_usr_food_rating_contatiner" class="row">
                                <span class="col-md-2">Food:</span>
                                <div id="current_rating1" class="col-md-2"
                                    style="cursor: pointer;"></div>
                            </div>
                            <div id="b_details_usr_ambiance_rating_contatiner" class="row">
                                <span class="col-md-2">Ambiance:</span>
                                <div id="current_rating2" class="col-md-2"
                                    style="cursor: pointer;"></div>
                            </div>
                            <div id="b_details_usr_service_rating_contatiner" class="row">
                                <span class="col-md-2">Service:</span>
                                <div id="current_rating3" class="col-md-2"
                                    style="cursor: pointer;"></div>
                            </div>
                            <div id="b_details_usr_valueForMoney_rating_contatiner"
                                class="row">
                                <span class="col-md-2">Value For Money:</span>
                                <div id="current_rating4" class="col-md-2"
                                    style="cursor: pointer;"></div>
                            </div>
                            <div id="b_details_usr_hygiene_rating_contatiner" class="row">
                                <span class="col-md-2">Hygiene:</span>
                                <div id="current_rating5" class="col-md-2"
                                    style="cursor: pointer;"></div>
                            </div>
                        </div>
                    </div>
                    </div>
                    
                    <!-- END Block 1 -->
                    
                </div>



            </div>


            <!--Start of classsic chart -->

            <div class="widget-extra">
                <!-- Classic Chart Block -->
                <div class="block full">
                    <!-- Classic Chart Title -->
                    <div class="block-title">
                        <h2>
                            <strong>Promocodes</strong>
                        </h2>
                    </div>
                    <!-- END Classic Chart Title -->

                    <!-- Classic Chart Content -->
                    <!-- Flot Charts (initialized in js/pages/compCharts.js), for more examples you can check out http://www.flotcharts.org/ -->
                    <div id="chart-classic" class="chart"></div>
                    <!-- END Classic Chart Content -->
                </div>
                <!-- END Classic Chart Block -->
            </div>
            <!--End of classic chart  -->
            <!-- Advanced Gallery Widget -->
            <div class="widget">
                <div class="widget-advanced">
                    <!-- Widget Header -->
                    <div class="widget-header text-center themed-background-dark">
                        <h3 class="widget-content-light clearfix">
                            Restaurant <strong>Gallery</strong>
                        </h3>
                    </div>
                    <!-- END Widget Header -->

                    <!-- Widget Main -->
                    <div class="widget-main">
                        <a href="#" class="widget-image-container">
                            <span class="widget-icon themed-background"><i
                                class="gi gi-picture"></i></span>
                        </a>
                        <div class="gallery gallery-widget" data-toggle="lightbox-gallery">
                            <div class="row">
                                <c:forEach items="${restaurant.restaurantImage}" var="restImage">
                                    <div class="col-xs-6 col-sm-3">
                                        <a
                                            href="<%=contexturl %>${restImage.image.imagePath }"
                                            class="gallery-link" title="Image Info"> <img
                                            src="<%=contexturl %>${restImage.image.thumbImagePath }"
                                            alt="image">
                                        </a>
                                    </div>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                    <!-- END Widget Main -->
                </div>
            </div>
            <!-- END Advanced Gallery Widget -->


        </div>
        <div class="col-md-6">
            <!-- Your Plan Widget -->
            <div class="widget">
                
                    
                <div class="widget-extra themed-background-dark">
                        <h3 class="widget-content-light">
                         <strong>Call for Services</strong> <small>used</small>
                         </h3>
                </div>
                <div class="widget-extra-full">
                    <div>
                        <ul class="nav nav-tabs" data-toggle="tabs">
                             <li class="active"><a href="#allService">All Services</a></li>
                             <li><a href="#forService" id="call_for_service">Call For Service</a></li>
                             <li><a href="#forManager" id="call_for_manager">Call For Manager</a></li>
                             <li><a href="#forBill" id="call_for_bill">Call For Bill</a></li>
                        </ul>
                    </div>
                    <div class="row tab-content">
                        <div class="tab-pane active"  id="allService" style="margin-left: 15px;margin-right: 10px">
                            <div id="chart-allService" class="chart" ></div>
                        </div>
                        <div class="tab-pane"  id="forService" style="margin-left: 15px;margin-right: 10px">
                            <div id="chart-callService" class="chart"></div>
                        </div>
                        <div class="tab-pane"  id="forManager" style="margin-left: 15px;margin-right: 10px">
                            <div id="chart-callManager" class="chart"></div>
                        </div>
                        <div class="tab-pane"  id="forBill" style="margin-left: 15px;margin-right: 10px">
                            <div id="chart-callBill" class="chart"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Your Plan Widget -->

            <!-- Charts Widget -->
            <div class="widget">
                <div class="widget-extra themed-background-dark">
                        <h3 class="widget-content-light">
                         <strong>Checkin</strong> <small>count</small>
                         </h3>
                </div>
                <div class="widget-extra-full">
                    <div class="widget-content animation-pullDown visible-lg">
                        Currently CheckedIn : ${checkedInCount}
                        <div class="row tab-content">
                            <div class="table-responsive">
                                <table class="table table-striped table-vcenter" id="general-table">
                                    <thead>
                                        <tr>
                                            <th class="text-center" style="width: 150px;"><i class="gi gi-user"></i></th>
                                            <th>User Name</th>
                                            <th>Email</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach var="customer" items="${checkedInMember}">
                                            <tr>
                                                <td class="text-center"><img class="img-circle" style="width: 75px;" alt="avatar" src="${customer.member.user.profileImage.imagePath}"></td>
                                                <td>${customer.member.user.fullName}</td>
                                                <td>${customer.member.user.emailId}</td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                            <div style="margin-left: 15px;margin-right: 10px">
                                <div>In current year.</div>
                                <div id="chart-checkin" class="chart"></div>
                            </div>
                     
                        </div>
                    </div>
                    
                </div>
            </div>
            
            
            <div class="widget">
                <div class="widget-extra themed-background-dark">
                        <h3 class="widget-content-light">
                         <strong>Offer</strong> <small><a href="<%=contexturl %>ManageRestaurant/RestaurantList/OfferList?outletId=<%=user.getModuleId()%>" id="offerList">View All</a></small>
                         </h3>
                </div>
                <div class="widget-extra-full">
                    <div class="widget-content animation-pullDown visible-lg">
                        
                        <div class="row tab-content">
                            <div class="table-responsive">
                                <table class="table table-striped table-vcenter" id="general-table">
                                    <thead>
                                        <tr>
                                            
                                            <th>Title</th>
                                            <th>Validity</th>
                                            <th>ExpiryDate</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach var="currentOffer" items="${offerList}" varStatus="count">
                                            <tr>
                                            <c:if test="${count.count le 5}">
                                                <td><c:out
                                                        value="${currentOffer.title}" /></td>
                                                <td ><label
                                                    class="switch switch-primary"><input
                                                        type="checkbox"
                                                        <c:if test="${currentOffer.offerValidity}"><spring:message code="label.checked"/></c:if> disabled="disabled"/><span></span>
                                                </label></td>
                                                <td>
                                                    <%--                            <c:out  value="${currentOffer.expiryDate}" />  --%>
                                                    <fmt:formatDate
                                                        pattern="<%=propvalue.simpleDatetimeFormat %>"
                                                        value="${currentOffer.expiryDate}" />
                                                </td>
                                                </c:if>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                    
                </div>
            </div>
            
            
            
        </div>
    </div>
    <!-- END Widgets Row -->
</div>
<!-- END Page Content -->
<%@include file="../inc/page_footer.jsp"%>

<!-- Remember to include excanvas for IE8 chart support -->
<!--[if IE 8]><script src="<%=contexturl%>resources/js/helpers/excanvas.min.js"></script><![endif]-->

<%@include file="../inc/template_scripts.jsp"%>
<script type="text/javascript">
    $(document).ready(function() {
//      <c:forEach var="currentUserOutletRating" items="${userOutletRatingList}" varStatus="count">
//          $("#avgrating${currentUserOutletRating.user.userId}").raty({
//              readOnly    : true,
//              hints       : ['', '', '', '', ''],
//              half        : true,
<%--                path        : "<%=contexturl%>resources/img/raty/", --%>
//              scoreName   : "avgRating${currentUserOutletRating.user.userId}",    
//              width       : 125,
//              score       : ${currentUserOutletRating.avgrating}
//          });
//          </c:forEach>
            
            
            $('#current_rating').raty({
                readOnly    : true,
                hints       : ['', '', '', '', ''],
                half        : true,
                path        : '<%=contexturl%>resources/img/raty/',
                scoreName   : "avgRating",  
                width       : 125,
                score       :<c:choose>
            <c:when test="${!empty arrCurrentAvg[0]}">
            ${arrCurrentAvg[0]}
          </c:when>
          <c:otherwise>
              0 
         </c:otherwise>
        </c:choose>
            });
        
            $('#current_rating1').raty({
                readOnly    : true,
                hints       : ['', '', '', '', ''],
                half        : true,
                path        : '<%=contexturl%>resources/img/raty/',
                scoreName   : "rating1",    
                width       : 125,
                cancel      : true,
                cancelHint  : "Remove this rating",
                score       :<c:choose>
            <c:when test="${!empty arrCurrentAvg[1]}">
            ${arrCurrentAvg[1]}
          </c:when>
          <c:otherwise>
              0 
         </c:otherwise>
        </c:choose>
                
            });
            $('#current_rating2').raty({
                readOnly    : true,
                hints       : ['', '', '', '', ''],
                half        : true,
                path        : '<%=contexturl%>resources/img/raty/',
                scoreName   : "rating2",    
                width       : 125,
                cancel      : true,
                cancelHint  : "Remove this rating",
                score       :<c:choose>
            <c:when test="${!empty arrCurrentAvg[2]}">
            ${arrCurrentAvg[2]}
          </c:when>
          <c:otherwise>
              0 
         </c:otherwise>
        </c:choose>
                
            });
           
            $('#current_rating3').raty({
                readOnly    : true,
                hints       : ['', '', '', '', ''],
                half        : true,
                path        : '<%=contexturl%>resources/img/raty/',
                scoreName   : "rating3",    
                width       : 125,
                cancel      : true,
                cancelHint  : "Remove this rating",
                score       : <c:choose>
            <c:when test="${!empty arrCurrentAvg[3]}">
            ${arrCurrentAvg[3]}
          </c:when>
          <c:otherwise>
              0 
         </c:otherwise>
        </c:choose>
                              
                
            });
            $('#current_rating4').raty({
                readOnly    : true,
                hints       : ['', '', '', '', ''],
                half        : true,
                path        : '<%=contexturl%>resources/img/raty/',
                scoreName   : "rating4",    
                width       : 125,
                cancel      : true,
                cancelHint  : "Remove this rating",
                score       :<c:choose>
                              <c:when test="${!empty arrCurrentAvg[4]}">
                              ${arrCurrentAvg[4]}
                            </c:when>
                            <c:otherwise>
                                0   
                           </c:otherwise>
                          </c:choose>
                
            });
            $('#current_rating5').raty({
                readOnly    : true,
                hints       : ['', '', '', '', ''],
                half        : true,
                path        : '<%=contexturl%>resources/img/raty/',
                scoreName   : "rating5",    
                width       : 125,
                cancel      : true,
                cancelHint  : "Remove this rating",
                score       :<c:choose>
                                     <c:when test="${!empty arrCurrentAvg[5]}">
                                     ${arrCurrentAvg[5]}
                                     </c:when>
                                     <c:otherwise>
                                         0  
                                     </c:otherwise>
                             </c:choose>
                
            });
            
            
            $('#pervious_rating').raty({
                readOnly    : true,
                hints       : ['', '', '', '', ''],
                half        : true,
                path        : '<%=contexturl%>resources/img/raty/',
                scoreName   : "avgRating",  
                width       : 125,
                score       :<c:choose>
            <c:when test="${!empty arrPerviousAvg[0]}">
            ${arrPerviousAvg[0]}
          </c:when>
          <c:otherwise>
              0 
         </c:otherwise>
        </c:choose>
            });
        
            $('#pervious_rating1').raty({
                readOnly    : true,
                hints       : ['', '', '', '', ''],
                half        : true,
                path        : '<%=contexturl%>resources/img/raty/',
                scoreName   : "rating1",    
                width       : 125,
                cancel      : true,
                cancelHint  : "Remove this rating",
                score       :<c:choose>
            <c:when test="${!empty arrPerviousAvg[1]}">
            ${arrPerviousAvg[1]}
          </c:when>
          <c:otherwise>
              0 
         </c:otherwise>
        </c:choose>
                
            });
            $('#pervious_rating2').raty({
                readOnly    : true,
                hints       : ['', '', '', '', ''],
                half        : true,
                path        : '<%=contexturl%>resources/img/raty/',
                scoreName   : "rating2",    
                width       : 125,
                cancel      : true,
                cancelHint  : "Remove this rating",
                score       :<c:choose>
            <c:when test="${!empty arrPerviousAvg[2]}">
            ${arrPerviousAvg[2]}
          </c:when>
          <c:otherwise>
              0 
         </c:otherwise>
        </c:choose>
                
            });
           
            $('#pervious_rating3').raty({
                readOnly    : true,
                hints       : ['', '', '', '', ''],
                half        : true,
                path        : '<%=contexturl%>resources/img/raty/',
                scoreName   : "rating3",    
                width       : 125,
                cancel      : true,
                cancelHint  : "Remove this rating",
                score       : <c:choose>
            <c:when test="${!empty arrPerviousAvg[3]}">
            ${arrPerviousAvg[3]}
          </c:when>
          <c:otherwise>
              0 
         </c:otherwise>
        </c:choose>
                              
                
            });
            $('#pervious_rating4').raty({
                readOnly    : true,
                hints       : ['', '', '', '', ''],
                half        : true,
                path        : '<%=contexturl%>resources/img/raty/',
                scoreName   : "rating4",    
                width       : 125,
                cancel      : true,
                cancelHint  : "Remove this rating",
                score       :<c:choose>
                              <c:when test="${!empty arrPerviousAvg[4]}">
                              ${arrPerviousAvg[4]}
                            </c:when>
                            <c:otherwise>
                                0   
                           </c:otherwise>
                          </c:choose>
                
            });
            $('#pervious_rating5').raty({
                readOnly    : true,
                hints       : ['', '', '', '', ''],
                half        : true,
                path        : '<%=contexturl%>resources/img/raty/',
                scoreName   : "rating5",    
                width       : 125,
                cancel      : true,
                cancelHint  : "Remove this rating",
                score       :<c:choose>
                                     <c:when test="${!empty arrPerviousAvg[5]}">
                                     ${ arrPerviousAvg[5]}
                                     </c:when>
                                     <c:otherwise>
                                         0  
                                     </c:otherwise>
                             </c:choose>
                
            });
            
            
        });
</script>
<script type="text/javascript">
    $(document).ready(function() {

         var chartAllService = $('#chart-allService');
         var chartCallService = $('#chart-callService');
         var chartCallManager = $('#chart-callManager');
         var chartCallBill = $('#chart-callBill');
         
         var allServiceData= ${allService};
         var callServiceData= ${callService};
         var callManagerData= ${callManager};
         var callBillData= ${callBill};
         
         var chartCheckin = $('#chart-checkin');
         var checkinData =${checkinData};
         
          var chartClassic = $('#chart-classic');
          var totalData = ${totalData};
          var assignedData = ${assignedData};
          var usedData = ${usedData};
          var chartPromo = ${chartData};
          
//           var dataSales = [[1, 500], [2, 420], [3, 480], [4, 350], [5, 600], [6, 850], [7, 1100], [8, 950], [9, 1220], [10, 1300], [11, 1500], [12, 1700]];
//           var dataTest = [[1, 800], [2, 900], [3, 600], [4, 350], [5, 600], [6, 850], [7, 1100], [8, 950], [9, 1220], [10, 1300], [11, 1500], [12, 1700]];
         // Array with month labels used in Classic and Stacked chart
          var chartMonths = [[1, 'Jan'], [2, 'Feb'], [3, 'Mar'], [4, 'Apr'], [5, 'May'], [6, 'Jun'], [7, 'Jul'], [8, 'Aug'], [9, 'Sep'], [10, 'Oct'], [11, 'Nov'], [12, 'Dec']];

          // Classic Chart
          $.plot(chartClassic,
              [
                  {
                      label: 'Total',
                      data: totalData,
                      lines: {show: true, fill: true, fillColor: {colors: [{opacity: 0.25}, {opacity: 0.25}]}},
                      points: {show: true, radius: 6}
                  },
                  {
                      label: 'Assigned',
                      data: assignedData,
                      lines: {show: true, fill: true, fillColor: {colors: [{opacity: 0.15}, {opacity: 0.15}]}},
                      points: {show: true, radius: 6}
                  },
                  {
                      label: 'Used',
                      data: usedData,
                      lines: {show: true, fill: true, fillColor: {colors: [{opacity: 0.15}, {opacity: 0.15}]}},
                      points: {show: true, radius: 6}
                  }
              ],
              {
                  colors: ['#3498db', '#333333','#aaa'],
                  legend: {show: true, position: 'nw', margin: [15, 10]},
                  grid: {borderWidth: 0, hoverable: true, clickable: true},
                  yaxis: {ticks: 4, tickColor: '#eeeeee'},
                  xaxis: {ticks: chartPromo, tickColor: '#ffffff'}
              }
          );

          // Creating and attaching a tooltip to the classic chart
          var previousPoint = null, ttlabel = null;
          chartClassic.bind('plothover', function(event, pos, item) {

              if (item) {
                  if (previousPoint !== item.dataIndex) {
                      previousPoint = item.dataIndex;

                      $('#chart-tooltip').remove();
                      var x = item.datapoint[0], y = item.datapoint[1];

                      if (item.seriesIndex === 2) {
                          ttlabel = '<strong>' + y + '</strong> used';
                      } 
                      else if (item.seriesIndex === 1) {
                          ttlabel = '<strong>' + y + '</strong> assigned';
                      } else {
                          ttlabel = '<strong>' + y + '</strong> promo';
                      }

                      $('<div id="chart-tooltip" class="chart-tooltip">' + ttlabel + '</div>')
                          .css({top: item.pageY - 45, left: item.pageX + 5}).appendTo("body").show();
                  }
              }
              else {
                  $('#chart-tooltip').remove();
                  previousPoint = null;
              }
          });
          
          
       // Classic Chart for Checkins
           $.plot(chartCheckin,
              [
                  {
                      label: 'Total',
                      data: checkinData,
                      lines: {show: true, fill: true, fillColor: {colors: [{opacity: 0.25}, {opacity: 0.25}]}},
                      points: {show: true, radius: 6}
                  }
                  
              ],
              {
                  colors: ['#3498db', '#333333','#aaa'],
                  legend: {show: true, position: 'nw', margin: [15, 10]},
                  grid: {borderWidth: 0, hoverable: true, clickable: true},
                  yaxis: {ticks: 4, tickColor: '#eeeeee'},
                  xaxis: {ticks: chartMonths, tickColor: '#ffffff'}
              }
          );

          // Creating and attaching a tooltip to the classic chart
          var previousPoint5 = null, ttlabel5 = null;
          chartCheckin.bind('plothover', function(event, pos, item) {

              if (item) {
                  if (previousPoint5 !== item.dataIndex) {
                      previousPoint5 = item.dataIndex;

                      $('#chart-tooltip5').remove();
                      var x = item.datapoint[0], y = item.datapoint[1];

                      ttlabel1 = '<strong>' + y + '</strong> used';
                      

                      $('<div id="chart-tooltip5" class="chart-tooltip">' + ttlabel1 + '</div>')
                          .css({top: item.pageY - 45, left: item.pageX + 5}).appendTo("body").show();
                  }
              }
              else {
                  $('#chart-tooltip5').remove();
                  previousPoint5 = null;
              }
          });
          
          
          // Classic Chart For AllService
          $.plot(chartAllService,
              [
                  {
                      label: 'Total',
                      data: allServiceData,
                      lines: {show: true, fill: true, fillColor: {colors: [{opacity: 0.25}, {opacity: 0.25}]}},
                      points: {show: true, radius: 6}
                  }
                  
              ],
              {
                  colors: ['#3498db', '#333333','#aaa'],
                  legend: {show: true, position: 'nw', margin: [15, 10]},
                  grid: {borderWidth: 0, hoverable: true, clickable: true},
                  yaxis: {ticks: 4, tickColor: '#eeeeee'},
                  xaxis: {ticks: chartMonths, tickColor: '#ffffff'}
              }
          );

          // Creating and attaching a tooltip to the classic chart
          var previousPoint1 = null, ttlabel1 = null;
          chartAllService.bind('plothover', function(event, pos, item) {

              if (item) {
                  if (previousPoint1 !== item.dataIndex) {
                      previousPoint1 = item.dataIndex;

                      $('#chart-tooltip1').remove();
                      var x = item.datapoint[0], y = item.datapoint[1];

                      ttlabel1 = '<strong>' + y + '</strong> used';
                      

                      $('<div id="chart-tooltip1" class="chart-tooltip">' + ttlabel1 + '</div>')
                          .css({top: item.pageY - 45, left: item.pageX + 5}).appendTo("body").show();
                  }
              }
              else {
                  $('#chart-tooltip1').remove();
                  previousPoint1 = null;
              }
          });
          
       // Classic Chart For CallForService
        $("#call_for_service").click(function(){
             $.plot(chartCallService,
                     [
                         {
                             label: 'Total',
                             data: callServiceData,
                             lines: {show: true, fill: true, fillColor: {colors: [{opacity: 0.25}, {opacity: 0.25}]}},
                             points: {show: true, radius: 6}
                         }
                         
                     ],
                     {
                         colors: ['#3498db', '#333333','#aaa'],
                         legend: {show: true, position: 'nw', margin: [15, 10]},
                         grid: {borderWidth: 0, hoverable: true, clickable: true},
                         yaxis: {ticks: 4, tickColor: '#eeeeee'},
                         xaxis: {ticks: chartMonths, tickColor: '#ffffff'}
                     }
                 );
    
                 // Creating and attaching a tooltip to the classic chart
                 var previousPoint2 = null, ttlabel2 = null;
                 chartCallService.bind('plothover', function(event, pos, item) {
    
                     if (item) {
                         if (previousPoint2 !== item.dataIndex) {
                             previousPoint2 = item.dataIndex;
    
                             $('#chart-tooltip2').remove();
                             var x = item.datapoint[0], y = item.datapoint[1];
    
                             ttlabel2 = '<strong>' + y + '</strong> used';
                             
    
                             $('<div id="chart-tooltip2" class="chart-tooltip">' + ttlabel2 + '</div>')
                                 .css({top: item.pageY - 45, left: item.pageX + 5}).appendTo("body").show();
                         }
                     }
                     else {
                         $('#chart-tooltip2').remove();
                         previousPoint2 = null;
                     }
                 });
             
        });
       
       
         
       // Classic Chart For CallForManager
        $("#call_for_manager").click(function(){
          $.plot(chartCallManager,
              [
                  {
                      label: 'Total',
                      data: callManagerData,
                      lines: {show: true, fill: true, fillColor: {colors: [{opacity: 0.25}, {opacity: 0.25}]}},
                      points: {show: true, radius: 6}
                  }
                  
              ],
              {
                  colors: ['#3498db', '#333333','#aaa'],
                  legend: {show: true, position: 'nw', margin: [15, 10]},
                  grid: {borderWidth: 0, hoverable: true, clickable: true},
                  yaxis: {ticks: 4, tickColor: '#eeeeee'},
                  xaxis: {ticks: chartMonths, tickColor: '#ffffff'}
              }
          );

          // Creating and attaching a tooltip to the classic chart
          var previousPoint3 = null, ttlabel3 = null;
          chartCallManager.bind('plothover', function(event, pos, item) {

              if (item) {
                  if (previousPoint3 !== item.dataIndex) {
                      previousPoint3 = item.dataIndex;

                      $('#chart-tooltip1').remove();
                      var x = item.datapoint[0], y = item.datapoint[1];

                      ttlabel3 = '<strong>' + y + '</strong> used';
                      

                      $('<div id="chart-tooltip3" class="chart-tooltip">' + ttlabel3 + '</div>')
                          .css({top: item.pageY - 45, left: item.pageX + 5}).appendTo("body").show();
                  }
              }
              else {
                  $('#chart-tooltip3').remove();
                  previousPoint3 = null;
              }
          });
        }); 
          
       // Classic Chart For CallForBill
       $("#call_for_bill").click(function(){
          $.plot(chartCallBill,
              [
                  {
                      label: 'Total',
                      data: callBillData,
                      lines: {show: true, fill: true, fillColor: {colors: [{opacity: 0.25}, {opacity: 0.25}]}},
                      points: {show: true, radius: 6}
                  }
                  
              ],
              {
                  colors: ['#3498db', '#333333','#aaa'],
                  legend: {show: true, position: 'nw', margin: [15, 10]},
                  grid: {borderWidth: 0, hoverable: true, clickable: true},
                  yaxis: {ticks: 4, tickColor: '#eeeeee'},
                  xaxis: {ticks: chartMonths, tickColor: '#ffffff'}
              }
          );

          // Creating and attaching a tooltip to the classic chart
          var previousPoint4 = null, ttlabel4 = null;
          chartCallBill.bind('plothover', function(event, pos, item) {

              if (item) {
                  if (previousPoint4 !== item.dataIndex) {
                      previousPoint4 = item.dataIndex;

                      $('#chart-tooltip4').remove();
                      var x = item.datapoint[0], y = item.datapoint[1];

                      ttlabel4 = '<strong>' + y + '</strong> used';
                      

                      $('<div id="chart-tooltip4" class="chart-tooltip">' + ttlabel4 + '</div>')
                          .css({top: item.pageY - 45, left: item.pageX + 5}).appendTo("body").show();
                  }
              }
              else {
                  $('#chart-tooltip4').remove();
                  previousPoint4 = null;
              }
          });
       });
        });
</script>


<!-- Google Maps API + Gmaps Plugin, must be loaded in the page you would like to use maps (Remove 'http:' if you have SSL) -->
<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script src="<%=contexturl%>resources/js/helpers/gmaps.min.js"></script>

<!-- Load and execute javascript code used only in this page -->
<script src="<%=contexturl%>resources/js/pages/index.js"></script>
<script>$(function(){ Index.init(); });</script>



<%@include file="../inc/template_end.jsp"%>