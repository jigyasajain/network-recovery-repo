
<%@page import="com.astrika.common.model.Role"%>
<%@page import="com.astrika.common.model.User"%>
<%-- <%@include file="../inc/config.jsp"%> --%>
<%
/**
 * page_head.jsp
 *
 * Author: pixelcave
 *
 * Header and Sidebar of each page
 *
 */
%>

<style>
.els{
	overflow: hidden;
	text-overflow: ellipsis;
	-o-text-overflow: ellipsis;
	white-space: nowrap;
	width: 100%;
}
</style>

<!-- Page Container -->
<!-- In the PHP version you can set the following options from inc/config.jsp file -->
<!--
    Available #page-container classes:

    '' (None)                                       for a full main and alternative sidebar hidden by default (> 991px)

    'sidebar-visible-lg'                            for a full main sidebar visible by default (> 991px)
    'sidebar-partial'                               for a partial main sidebar which opens on mouse hover, hidden by default (> 991px)
    'sidebar-partial sidebar-visible-lg'            for a partial main sidebar which opens on mouse hover, visible by default (> 991px)

    'sidebar-alt-visible-lg'                        for a full alternative sidebar visible by default (> 991px)
    'sidebar-alt-partial'                           for a partial alternative sidebar which opens on mouse hover, hidden by default (> 991px)
    'sidebar-alt-partial sidebar-alt-visible-lg'    for a partial alternative sidebar which opens on mouse hover, visible by default (> 991px)

    'sidebar-partial sidebar-alt-partial'           for both sidebars partial which open on mouse hover, hidden by default (> 991px)

    'sidebar-no-animations'                         add this as extra for disabling sidebar animations on large screens (> 991px) - Better performance with heavy pages!

    'style-alt'                                     for an alternative main style (without it: the default style)
    'footer-fixed'                                  for a fixed footer (without it: a static footer)

    'header-fixed-top'                              has to be added only if the class 'navbar-fixed-top' was added on header.navbar
    'header-fixed-bottom'                           has to be added only if the class 'navbar-fixed-bottom' was added on header.navbar
-->
<%
    String page_classes = null;

    if (template.get("header").equals( "navbar-fixed-top")) {
        page_classes = "header-fixed-top";
    } else if (template.get("header").equals("navbar-fixed-bottom")) {
        page_classes = "header-fixed-bottom";
    }

    if (template.containsKey("sidebar")) {
        page_classes += ((page_classes == "") ? "" : " ") + template.get("sidebar");
    }

    if (template.get("main_style").equals("style-alt"))  {
        page_classes += ((page_classes == "") ? "" : " ") + "style-alt";
    }

    if (template.get("footer").equals("footer-fixed"))  {
        page_classes += ((page_classes == "") ? "" : " ") + "footer-fixed";
    }
    
    
%>
<div id="page-container"<% if (page_classes!=null) { %>  class="<%= page_classes %>" <% } %> >
    <!-- Alternative Sidebar -->
    <div id="sidebar-alt">
        <!-- Wrapper for scrolling functionality -->
        <div class="sidebar-scroll">
            <!-- Sidebar Content -->
            <div class="sidebar-content">
                <!-- Chat -->
                <!-- Chat demo functionality initialized in js/app.js -> chatUi() -->
                <a href="javascript:void(0)" class="sidebar-title">
                    <i class="gi gi-comments pull-right"></i> <strong>Chat</strong>UI
                </a>

                <!-- Chat Talk -->
                <div class="chat-talk display-none">
                    <!-- Chat Info -->
                    <div class="chat-talk-info sidebar-section">
                        <img src="<%=contexturl %>resources/img/placeholders/avatars/avatar5.jpg" alt="avatar" class="img-circle pull-left save">
                        <strong>John</strong> Doe
                        <button id="chat-talk-close-btn" class="btn btn-xs btn-default pull-right">
                            <i class="fa fa-times"></i>
                        </button>
                    </div>
                    <!-- END Chat Info -->

                    <!-- Chat Messages -->
                    <ul class="chat-talk-messages">
                        <li class="text-center"><small>Yesterday, 18:35</small></li>
                        <li class="chat-talk-msg animation-slideRight">Hey admin?</li>
                        <li class="chat-talk-msg animation-slideRight">How are you?</li>
                        <li class="text-center"><small>Today, 7:10</small></li>
                        <li class="chat-talk-msg chat-talk-msg-highlight themed-border animation-slideLeft">I'm fine, thanks!</li>
                    </ul>
                    <!-- END Chat Messages -->

                    <!-- Chat Input -->
                    <form action="index.jsp" method="post" id="sidebar-chat-form" class="chat-form">
                        <input type="text" id="sidebar-chat-message" name="sidebar-chat-message" class="form-control form-control-borderless" placeholder="Type a message..">
                    </form>
                    <!-- END Chat Input -->
                </div>
                <!--  END Chat Talk -->
                <!-- END Chat -->

                <!-- Activity -->
                <a href="javascript:void(0)" class="sidebar-title">
                    <i class="fa fa-globe pull-right"></i> <strong>Activity</strong>UI
                </a>
                <div class="sidebar-section">
                    <div class="alert alert-danger alert-alt">
                        <small>just now</small><br>
                        <i class="fa fa-thumbs-up fa-fw"></i> Upgraded to Pro plan
                    </div>
                    <div class="alert alert-info alert-alt">
                        <small>2 hours ago</small><br>
                        <i class="gi gi-coins fa-fw"></i> You had a new sale!
                    </div>
                    <div class="alert alert-success alert-alt">
                        <small>3 hours ago</small><br>
                        <i class="fa fa-plus fa-fw"></i> <a href="page_ready_user_profile.jsp"><strong>John Doe</strong></a> would like to become friends!<br>
                        <a href="javascript:void(0)" class="btn btn-xs btn-primary"><i class="fa fa-check"></i> Accept</a>
                        <a href="javascript:void(0)" class="btn btn-xs btn-default"><i class="fa fa-times"></i> Ignore</a>
                    </div>
                    <div class="alert alert-warning alert-alt">
                        <small>2 days ago</small><br>
                        Running low on space<br><strong>18GB in use</strong> 2GB left<br>
                        <a href="page_ready_pricing_tables.jsp" class="btn btn-xs btn-primary"><i class="fa fa-arrow-up"></i> Upgrade Plan</a>
                    </div>
                </div>
                <!-- END Activity -->

                <!-- Messages -->
                <a href="page_ready_inbox.jsp" class="sidebar-title">
                    <i class="fa fa-envelope pull-right"></i> <strong>Messages</strong>UI (5)
                </a>
                <div class="sidebar-section">
                    <div class="alert alert-alt">
                        Debra Stanley<small class="pull-right">just now</small><br>
                        <a href="page_ready_inbox_message.jsp"><strong>New Follower</strong></a>
                    </div>
                    <div class="alert alert-alt">
                        Sarah Cole<small class="pull-right">2 min ago</small><br>
                        <a href="page_ready_inbox_message.jsp"><strong>Your subscription was updated</strong></a>
                    </div>
                    <div class="alert alert-alt">
                        Bryan Porter<small class="pull-right">10 min ago</small><br>
                        <a href="page_ready_inbox_message.jsp"><strong>A great opportunity</strong></a>
                    </div>
                    <div class="alert alert-alt">
                        Jose Duncan<small class="pull-right">30 min ago</small><br>
                        <a href="page_ready_inbox_message.jsp"><strong>Account Activation</strong></a>
                    </div>
                    <div class="alert alert-alt">
                        Henry Ellis<small class="pull-right">40 min ago</small><br>
                        <a href="page_ready_inbox_message.jsp"><strong>You reached 10.000 Followers!</strong></a>
                    </div>
                </div>
                <!-- END Messages -->
            </div>
            <!-- END Sidebar Content -->
        </div>
        <!-- END Wrapper for scrolling functionality -->
    </div>
    <!-- END Alternative Sidebar -->

    <!-- Main Sidebar -->
    <div id="sidebar">
        <!-- Wrapper for scrolling functionality -->
        <div class="sidebar-scroll">
            <!-- Sidebar Content -->
            <div class="sidebar-content">
                <!-- Brand -->
                <a href="<%=contexturl %>Index" class="sidebar-brand save">
<%--                 	<img src="<%=contexturl %>resources/img/logo2.png"> --%>
						<strong>Atma</strong>
                </a>
                <!-- END Brand -->

                <!-- User Info -->
                <div class="sidebar-section sidebar-user clearfix">
                     <div class="sidebar-user-avatar">
                    <%
                    	if(user.getProfileImage() != null && !user.getProfileImage().getImagePath().isEmpty()){
                    		
                    %>

						<img src="<%=contexturl+user.getProfileImage().getImagePath() %>" alt="avatar"
							title="Click here to change photo" onclick="OpenFileDialog();">

						<%
							} else {
						%>
                      
                            <img src="<%=contexturl %>resources/img/application/default_profile.png" alt="avatar" 
                            title="Click here to change photo"
                            onclick="OpenFileDialog();">
                       
                     <%} %>
                     
                    </div>
					<form action="<%=contexturl %>ChangePhoto" id="changePhoto"
						method="post" enctype="multipart/form-data" class="save">
						<input type="file" id="imageChooser" style="display: none;"
							onchange="changePhoto();" name="imageChooser"
							accept="image/jpg,image/jpeg,image/png,image/gif,image/bmp,image/tiff" class="save"/>
					</form>
                    <div class="sidebar-user-name els"><%=user.getFullName() %></div>
                    <div class="sidebar-user-links">
                        <a href="<%=contexturl %>User/EditProfile" data-toggle="tooltip" data-placement="bottom" title="Profile" class="save"><i class="gi gi-user save"></i></a>
                      	<a href="#logout_popup" data-toggle="modal" data-placement="bottom" class="enable-tooltip save" title="Logout"><i class="gi gi-exit"></i></a>
                    </div>
                </div>
                <!-- END User Info -->

			<!-- START Sidebar Navigation -->
               <ul class="sidebar-nav">
				<li><a href="<%=contexturl %>Index" class=" active" id="dashboard" class="save"><i
						class="gi gi-stopwatch sidebar-nav-icon"></i> <spring:message code="menu.dashboard"/></a></li>
				
				<security:authorize ifAnyGranted="<%=Role.SUPER_ADMIN.name() %>">
				<li class="sidebar-header"><span
					class="sidebar-header-options clearfix"></span> <span
					class="sidebar-header-title"><spring:message code="label.adminmanagement"/></span></li>
				
					<li id="manageAdmin"><a  href="#" class="sidebar-nav-menu"><i
							class="fa fa-angle-left sidebar-nav-indicator"></i><i
							class="gi gi-crown sidebar-nav-icon"></i> <spring:message code="menu.manageadmin"/></a>
						<ul>
							<li><a href="<%=contexturl %>ManageAdmin/AdminList" id="adminList" class="save">Admin List</a></li>
						</ul> 
					</li> 
				
<%-- 						<li><a href="<%=contexturl %>ManageDepartment/DepartmentList" id="departmentList">Department List</a></li>  --%> 
<%-- 						<li><a href="<%=contexturl %>ManageInstructor/InstructorList" id="brandList" title="You can add, remove or edit your brands here.">Instructor List</a></li>			 --%> 
<%--  							<li><a href="<%=contexturl %>ManageCourse/CourseCategoryList" id="viewCourseCategoryList">Course Category List</a></li>  --%> 
<%--   							<li><a href="<%=contexturl %>ManageCourse/CourseList" id="viewCourse">Course List</a></li> --%> 
<%-- 							<li><a href="<%=contexturl %>ManageDesignation/DesignationList" id="DesignationList">Designation List</a></li>  --%> 
<%-- 							<li><a href="<%=contexturl %>ManageCourseType/CourseTypeList" id="CourseTypeList">Course-Type List</a></li>  --%> 
<!-- 						</ul>  -->
<!-- 					</li>  -->
					
						<li class="sidebar-header"><span
					class="sidebar-header-options clearfix"></span> <span
					class="sidebar-header-title"><spring:message code="label.Modulemanagement" /></span></li>
				
					<li id="manageModule"><a  href="#" class="sidebar-nav-menu save"><i
							class="fa fa-angle-left sidebar-nav-indicator"></i><i
							class="gi gi-crown sidebar-nav-icon"></i> <spring:message code="menu.managemodule" /></a>
						<ul>
							<li><a href="<%=contexturl %>ManageModule/ModuleList" id="moduleList" class="save">Module List</a></li>
						</ul> 
					</li> 
					
					
				</security:authorize>
			
 				<security:authorize ifAnyGranted="<%=Role.ATMA_ADMIN.name()  %>"> 
				<li class="sidebar-header"><span
					class="sidebar-header-options clearfix"></span> <span
					class="sidebar-header-title"><spring:message code="label.categorymanagement"/></span></li>


					<li id="manageCategory"><a  href="#" class="sidebar-nav-menu save"><i
							class="fa fa-angle-left sidebar-nav-indicator"></i><i
							class="gi gi-crown sidebar-nav-icon"></i> <spring:message code="menu.managecategory"/></a>
						<ul>
							<li><a href="<%=contexturl %>ManageCategory/CategoryList" id="categoryList" class="save">Category List</a></li>
						</ul> 
					</li>
					

				<li class="sidebar-header"><span
					class="sidebar-header-options clearfix"></span> <span
					class="sidebar-header-title"><spring:message code="label.SurveyBuilder" /></span></li>
					
					<li id="surveyBuilder"><a href="#"
							class="sidebar-nav-menu save" onclick="getModule();"> <i
								class="fa fa-angle-left sidebar-nav-indicator"></i> <i
								class="gi gi-crown sidebar-nav-icon"></i> <spring:message code="menu.manageSurvey" /></a>
								
							<ul id="moduleList">
								<c:forEach var="module" items="${myList}" varStatus="count">
											<li><a href="<%=contexturl%>ManageSurvey/CreateSurvey" id="surveyList" class="save"> ${module.moduleName}</a></li>
								</c:forEach>
							</ul>
					</li>

				<li class="sidebar-header"><span
					class="sidebar-header-options clearfix"></span> <span
					class="sidebar-header-title"><spring:message code="label.DocumentManager"/></span></li>
					
					<li id="manageDocument"><a  href="#" class="sidebar-nav-menu save"><i
							class="fa fa-angle-left sidebar-nav-indicator"></i><i
							class="gi gi-crown sidebar-nav-icon"></i> <spring:message code="menu.managedocument"/></a>
						<ul>
							<li><a href="<%=contexturl %>ManageDocument/DocumentList" id="documentList" class="save">Document List</a></li>
						</ul>
					</li>
				
				
				<li class="sidebar-header"><span
					class="sidebar-header-options clearfix"></span> <span
					class="sidebar-header-title"><spring:message code="label.SponsoredImage"/></span></li>
					
					<li id="manageSponsoredImage"><a  href="#" class="sidebar-nav-menu save"><i
							class="fa fa-angle-left sidebar-nav-indicator"></i><i
							class="gi gi-crown sidebar-nav-icon"></i> <spring:message code="menu.managesponsoredimage"/></a>
						<ul>
							<li><a href="<%=contexturl %>ManageSponsoredImage/SponsoredImageList" id="sponsoredImageList" class="save">SponsoredImage List</a></li>
						</ul>
					</li>
					
					
 			<c:if test="${visitor.enquiryAccessRights==true}"> 
				<li class="sidebar-header"><span
					class="sidebar-header-options clearfix"></span> <span
					class="sidebar-header-title"><spring:message code="label.manageenquiry"/></span></li>
					
					<li id="manageEnquiry"><a  href="#" class="sidebar-nav-menu save"><i
							class="fa fa-angle-left sidebar-nav-indicator"></i><i
							class="gi gi-crown sidebar-nav-icon"></i> <spring:message code="menu.manageenquiry"/></a>
						<ul>
							<li><a href="<%=contexturl %>ManageEnquiry/ListEnquiry" id="enquiryList" class="save">Enquiry List</a></li>
						</ul>
					</li>
			</c:if>
			
				<li class="sidebar-header"><span
					class="sidebar-header-options clearfix"></span> <span
					class="sidebar-header-title"><spring:message code="label.nextsteps"/></span></li>
					
					<li id="manageNextSteps"><a href="#"
							class="sidebar-nav-menu save" onclick="getModuleforNextSteps();"> <i
								class="fa fa-angle-left sidebar-nav-indicator"></i> <i
								class="gi gi-crown sidebar-nav-icon"></i> <spring:message code="menu.managenextsteps" /></a>
						<ul id="List">
								<c:forEach var="module" items="${myModuleList}" varStatus="count">
											<li><a href="<%=contexturl%>ManageNextSteps/CreateNextSteps" id="nextStepsList" class="save"> ${module.moduleName}</a></li>
								</c:forEach>
						</ul>
					</li>
		</security:authorize>	 
					
					
					
 		<security:authorize ifAnyGranted="<%=Role.COMPANY_ADMIN.name()%>"> 
				<li class="sidebar-header"><span
					class="sidebar-header-options clearfix"></span> <span
					class="sidebar-header-title"><spring:message code="label.moduleusermanagement"/></span></li>


					<li id="manageModuleUser"><a  href="#" class="sidebar-nav-menu save"><i
							class="fa fa-angle-left sidebar-nav-indicator"></i><i
							class="gi gi-crown sidebar-nav-icon"></i> <spring:message code="menu.managemoduleuser"/></a>
						<ul>
							<li><a href="<%=contexturl %>ManageModuleUser/ModuleUserList" id="moduleUserList" class="save">Module User List</a></li>
						</ul> 
					</li>
					
			</security:authorize>			
 				 
 				 
		
				<security:authorize ifAnyGranted="<%=Role.SUPER_ADMIN.name()  %>"> 
				
<!--  				<li class="sidebar-header"><span  -->
<!--   					class="sidebar-header-options clearfix"></span> <span  -->
<!--   					class="sidebar-header-title"><spring:message code="menu.studentmanagement"/></span></li>  -->


<!--   				<li id="manageUser"><a href="#" class="sidebar-nav-menu"><i  -->
<!--   						class="fa fa-angle-left sidebar-nav-indicator"></i><i  -->
<!--   						class="gi gi-user sidebar-nav-icon"></i> <spring:message code="menu.managestudent"/></a> -->
<!-- 					<ul>  -->
<%--  						<li><a href="<%=contexturl %>ManageUsers/UserList" id="userList">Student List</a></li>  --%>
<!-- 					</ul></li> - -->


				<li class="sidebar-header"><span
					class="sidebar-header-options clearfix"></span> <span
					class="sidebar-header-title"><spring:message code="menu.masters"/></span></li>


				<li id="manageLocation"><a href="#" class="sidebar-nav-menu save"><i
						class="fa fa-angle-left sidebar-nav-indicator"></i><i
						class="fa fa-map-marker sidebar-nav-icon"></i> <spring:message code="menu.managelocations"/></a>
					<ul>

						<li><a href="#" id="country" class="save">Country</a></li>
						<li><a href="<%=contexturl %>Manage/StateList" id="state" class="save">State</a></li> 
						<li><a href="<%=contexturl %>ManageCity/City" id="city" class="save">City</a></li>
 						
					</ul>
				</li>


<!-- 						<li><a href="#" class="sidebar-nav-menu"><i -->
<!-- 								class="fa fa-angle-left sidebar-nav-indicator"></i><i -->
<!-- 								class="gi gi-usd sidebar-nav-icon"></i> <spring:message code="menu.managecurrency" /></a> -->
<!-- 							<ul> -->
<!-- 								<li><a href="#" id="currency">Currency Master</a></li> -->
<!-- 								<li><a href="#" id="currencyExchangeRate">Exchange Rate</a></li> -->
<!-- 							</ul> -->
<!-- 						</li> -->

<!-- 						<li><a href="#" class="sidebar-nav-menu"><i -->
<!-- 						class="fa fa-angle-left sidebar-nav-indicator"></i><i -->
<!-- 						class="gi gi-music sidebar-nav-icon"></i>Manage Language</a> -->
<!-- 						<ul> -->
<!-- 						<li><a href="#" id="language">Language</a></li> -->
<!-- 						</ul> -->
<!-- 						</li> -->
				
					
				
				</security:authorize>
				<security:authorize ifAnyGranted="<%=Role.MODULE_USER.name()%>">	
<!-- 					<li class="sidebar-header"><span -->
<!-- 					class="sidebar-header-options clearfix"></span> <span -->
<!-- 					class="sidebar-header-title"><spring:message code="label.moduleusermanagement"/></span></li> -->


<!-- 					<li id="manageVisitorsPage"><a  href="#" class="sidebar-nav-menu"><i -->
<!-- 							class="fa fa-angle-left sidebar-nav-indicator"></i><i -->
<!-- 							class="gi gi-crown sidebar-nav-icon"></i> <spring:message code="menu.modules"/></a> -->
<!-- 						<ul> -->
<%-- 							<li><a href="<%=contexturl %>ManageVisitorsPage/ListofModules" id="listofModules">List of Modules</a></li> --%>
<!-- 						</ul>  -->
<!-- 					</li> -->
					
				</security:authorize>
				
				
				
				
				
			</ul>
			  <!-- END Sidebar Navigation -->

                <!-- Sidebar Notifications -->
<!--                 <div class="sidebar-header"> -->
<!--                     <span class="sidebar-header-options clearfix"> -->
<!--                         <a href="javascript:void(0)" data-toggle="tooltip" title="Refresh"><i class="gi gi-refresh"></i></a> -->
<!--                     </span> -->
<!--                     <span class="sidebar-header-title">Activity</span> -->
<!--                 </div> -->
<!--                 <div class="sidebar-section"> -->
<!--                     <div class="alert alert-success alert-alt"> -->
<!--                         <small>5 min ago</small><br> -->
<!--                         <i class="fa fa-thumbs-up fa-fw"></i> You had a new sale ($10) -->
<!--                     </div> -->
<!--                     <div class="alert alert-info alert-alt"> -->
<!--                         <small>10 min ago</small><br> -->
<!--                         <i class="fa fa-arrow-up fa-fw"></i> Upgraded to Pro plan -->
<!--                     </div> -->
<!--                     <div class="alert alert-warning alert-alt"> -->
<!--                         <small>3 hours ago</small><br> -->
<!--                         <i class="fa fa-exclamation fa-fw"></i> Running low on space<br><strong>18GB in use</strong> 2GB left -->
<!--                     </div> -->
<!--                     <div class="alert alert-danger alert-alt"> -->
<!--                         <small>Yesterday</small><br> -->
<!--                         <i class="fa fa-bug fa-fw"></i> <a href="javascript:void(0)"><strong>New bug submitted</strong></a> -->
<!--                     </div> -->
<!--                 </div> -->
                <!-- END Sidebar Notifications -->
            </div>
            <!-- END Sidebar Content -->
        </div>
        <!-- END Wrapper for scrolling functionality -->
    </div>
    <!-- END Main Sidebar -->

    <!-- Main Container -->
    <div id="main-container">
        <!-- Header -->
        <!-- In the PHP version you can set the following options from inc/config.jsp file -->
        <!--
            Available header.navbar classes:

            'navbar-default'            for the default light header
            'navbar-inverse'            for an alternative dark header

            'navbar-fixed-top'          for a top fixed header (fixed sidebars with scroll will be auto initialized, functionality can be found in js/app.js - handleSidebar())
                'header-fixed-top'      has to be added on #page-container only if the class 'navbar-fixed-top' was added

            'navbar-fixed-bottom'       for a bottom fixed header (fixed sidebars with scroll will be auto initialized, functionality can be found in js/app.js - handleSidebar()))
                'header-fixed-bottom'   has to be added on #page-container only if the class 'navbar-fixed-bottom' was added
        -->
        <header class="navbar<% if (template.containsKey("header_navbar")) { %> <%= template.get("header_navbar") %><% } %>
							 <% if (template.containsKey("header")) { %> <%= template.get("header") %><%  } %>">
            <% if ( template.get("header_content").equals("horizontal-menu" )) { // Horizontal Menu Header Content %>
            <!-- Navbar Header -->
            <div class="navbar-header">
                <!-- Horizontal Menu Toggle + Alternative Sidebar Toggle Button, Visible only in small screens (< 768px) -->
                <ul class="nav navbar-nav-custom pull-right visible-xs">
                    <li>
                        <a href="CommingSoon" data-toggle="collapse" data-target="#horizontal-menu-collapse">Menu</a>
                    </li>
                    <li>
                        <a href="CommingSoon" onclick="App.sidebar('toggle-sidebar-alt');">
                            <i class="gi gi-share_alt"></i>
                            <span class="label label-primary label-indicator animation-floating">4</span>
                        </a>
                    </li>
                </ul>
                <!-- END Horizontal Menu Toggle + Alternative Sidebar Toggle Button -->

                <!-- Main Sidebar Toggle Button -->
                <ul class="nav navbar-nav-custom">
                    <li>
                        <a href="CommingSoon" onclick="App.sidebar('toggle-sidebar');">
                            <i class="fa fa-bars fa-fw"></i>
                        </a>
                    </li>
                </ul>
                <!-- END Main Sidebar Toggle Button -->
            </div>
            <!-- END Navbar Header -->

            <!-- Alternative Sidebar Toggle Button, Visible only in large screens (> 767px) -->
            <ul class="nav navbar-nav-custom pull-right hidden-xs">
                <li>
                    <!-- If you do not want the main sidebar to open when the alternative sidebar is closed, just remove the second parameter: App.sidebar('toggle-sidebar-alt'); -->
                    <a href="CommingSoon" onclick="App.sidebar('toggle-sidebar-alt', 'toggle-other');">
                        <i class="gi gi-share_alt"></i>
                        <span class="label label-primary label-indicator animation-floating">4</span>
                    </a>
                </li>
            </ul>
            <!-- END Alternative Sidebar Toggle Button -->

            <!-- Horizontal Menu + Search -->
            <div id="horizontal-menu-collapse" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="CommingSoon">Home</a>
                    </li>
                    <li>
                        <a href="CommingSoon">Profile</a>
                    </li>
                    <li class="dropdown">
                        <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">Settings <i class="fa fa-angle-down"></i></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0)"><i class="fa fa-asterisk fa-fw pull-right"></i> General</a></li>
                            <li><a href="javascript:void(0)"><i class="fa fa-lock fa-fw pull-right"></i> Security</a></li>
                            <li><a href="javascript:void(0)"><i class="fa fa-user fa-fw pull-right"></i> Account</a></li>
                            <li><a href="javascript:void(0)"><i class="fa fa-magnet fa-fw pull-right"></i> Subscription</a></li>
                            <li class="divider"></li>
                            <li class="dropdown-submenu">
                                <a href="javascript:void(0)" tabindex="-1"><i class="fa fa-chevron-right fa-fw pull-right"></i> More Settings</a>
                                <ul class="dropdown-menu">
                                    <li><a href="javascript:void(0)" tabindex="-1">Second level</a></li>
                                    <li><a href="javascript:void(0)">Second level</a></li>
                                    <li><a href="javascript:void(0)">Second level</a></li>
                                    <li class="divider"></li>
                                    <li class="dropdown-submenu">
                                        <a href="javascript:void(0)" tabindex="-1"><i class="fa fa-chevron-right fa-fw pull-right"></i> More Settings</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="javascript:void(0)">Third level</a></li>
                                            <li><a href="javascript:void(0)">Third level</a></li>
                                            <li><a href="javascript:void(0)">Third level</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">Contact <i class="fa fa-angle-down"></i></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0)"><i class="fa fa-envelope-o fa-fw pull-right"></i> By Email</a></li>
                            <li><a href="javascript:void(0)"><i class="fa fa-phone fa-fw pull-right"></i> By Phone</a></li>
                        </ul>
                    </li>
                </ul>
<!--                 <form action="page_ready_search_results.jsp" class="navbar-form navbar-left" role="search"> -->
<!--                     <div class="form-group"> -->
<!--                         <input type="text" class="form-control" placeholder="Search.."> -->
<!--                     </div> -->
<!--                 </form> -->
            </div>
            <!-- END Horizontal Menu + Search -->
            <% } else { // Default Header Content  %>
            <!-- Left Header Navigation -->
            <ul class="nav navbar-nav-custom">
                <!-- Main Sidebar Toggle Button -->
                <li>
                    <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');">
                        <i class="fa fa-bars fa-fw"></i>
                    </a>
                </li>
                <!-- END Main Sidebar Toggle Button -->

                <!-- Template Options -->
                <!-- Change Options functionality can be found in js/app.js - templateOptions() -->
<!--                 <li class="dropdown"> -->
<!--                     <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"> -->
<!--                          -->
<!--                     </a> -->
<!--                     <ul class="dropdown-menu dropdown-custom dropdown-options"> -->
<!--                         <li class="dropdown-header text-center">Header Style</li> -->
<!--                         <li> -->
<!--                             <div class="btn-group btn-group-justified btn-group-sm"> -->
<!--                                 <a href="javascript:void(0)" class="btn btn-primary" id="options-header-default">Light</a> -->
<!--                                 <a href="javascript:void(0)" class="btn btn-primary" id="options-header-inverse">Dark</a> -->
<!--                             </div> -->
<!--                         </li> -->
<!--                         <li class="dropdown-header text-center">Page Style</li> -->
<!--                         <li> -->
<!--                             <div class="btn-group btn-group-justified btn-group-sm"> -->
<!--                                 <a href="javascript:void(0)" class="btn btn-primary" id="options-main-style">Default</a> -->
<!--                                 <a href="javascript:void(0)" class="btn btn-primary" id="options-main-style-alt">Alternative</a> -->
<!--                             </div> -->
<!--                         </li> -->
<!--                         <li class="dropdown-header text-center">Main Layout</li> -->
<!--                         <li> -->
<!--                             <button class="btn btn-sm btn-block btn-primary" id="options-header-top">Fixed Side/Header (Top)</button> -->
<!--                             <button class="btn btn-sm btn-block btn-primary" id="options-header-bottom">Fixed Side/Header (Bottom)</button> -->
<!--                         </li> -->
<!--                         <li class="dropdown-header text-center">Footer</li> -->
<!--                         <li> -->
<!--                             <div class="btn-group btn-group-justified btn-group-sm"> -->
<!--                                 <a href="javascript:void(0)" class="btn btn-primary" id="options-footer-static">Default</a> -->
<!--                                 <a href="javascript:void(0)" class="btn btn-primary" id="options-footer-fixed">Fixed</a> -->
<!--                             </div> -->
<!--                         </li> -->
<!--                     </ul> -->
<!--                 </li> -->
                <!-- END Template Options -->
            </ul>
            <!-- END Left Header Navigation -->

            <!-- Search Form -->
<!--             <form action="page_ready_search_results.jsp" method="post" class="navbar-form-custom" role="search"> -->
<!--                 <div class="form-group"> -->
<!--                     <input type="text" id="top-search" name="top-search" class="form-control" placeholder="Search.."> -->
<!--                 </div> -->
<!--             </form> -->
            <!-- END Search Form -->

            <!-- Right Header Navigation -->
            <ul class="nav navbar-nav-custom pull-right">
                <!-- Alternative Sidebar Toggle Button -->
                <li class="hidden">
                    <!-- If you do not want the main sidebar to open when the alternative sidebar is closed, just remove the second parameter: App.sidebar('toggle-sidebar-alt'); -->
                    <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar-alt', 'toggle-other');">
                        <i class="gi gi-share_alt"></i>
                        <span class="label label-primary label-indicator animation-floating">4</span>
                    </a>
                </li>
                <!-- END Alternative Sidebar Toggle Button -->

                <!-- User Dropdown -->
                <li class="dropdown">
                   
                    
                    	<%
							if (user.getProfileImage() != null
									&& !user.getProfileImage().getImagePath().isEmpty()) {
						%>
							<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
									<img src="<%=contexturl+user.getProfileImage().getImagePath() %>" alt="avatar"> <i class="fa fa-angle-down"></i>
							</a>
						<%
							} else {
						%>
						<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
									<img src="<%=contexturl %>resources/img/application/default_profile.png" alt="avatar"> <i class="fa fa-angle-down"></i>
								</a>

						<%
							}
						%>
                    
                    
                    <ul
						class="dropdown-menu dropdown-custom dropdown-menu-right">
						<li class="dropdown-header text-center">Account</li>
<%-- 						<li><a href="<%=contexturl %>User/EditProfile"> <i --%>
<!-- 								class="fa fa-user fa-fw pull-right"></i> Profile </a></li> -->
						<li><a href="#logout_popup" data-toggle="modal"><i
								class="gi gi-exit pull-right"></i> Logout</a></li>
					</ul>
				</li>
                <!-- END User Dropdown -->
            </ul>
            <!-- END Right Header Navigation -->
            <% } %>
        </header>
        
        <script src="<%=contexturl %>resources/js/vendor/jquery-1.11.0.min.js"></script>
        <script type="text/javascript">
        
        	


        
        
        </script>
        <!-- END Header -->
