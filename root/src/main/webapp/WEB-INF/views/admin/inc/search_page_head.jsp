<%@page import="com.astrika.common.model.location.CityMaster"%>
<%@page import="com.astrika.common.model.User"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page import="com.astrika.common.model.Role"%>
<%
/**
 * page_head.jsp
 *
 * Author: pixelcave
 *
 * Header and Sidebar of each page
 *
 */
%>
	<style>
		.sidebar-brand-logo {
			height: 50px;
			line-height: 50px;
			padding: 0 10px;
			margin: 0;
			font-weight: 300;
			font-size: 18px;
			display: block;
			color: #ffffff;
		}

		a.sidebar-brand-logo:hover, a.sidebar-brand-logo:focus{
			background-color: #1bbae1;
			color: #ffffff;
			text-decoration: none;
		}
		.header-section{
			padding:5px 10px;
		}
		h1, .h1, h2, .h2, h3, .h3 {
			margin-top: 10px;
			margin-bottom: 10px;
		}
	
		.navbar.navbar-inverse{
			background-color:#394263;
		}
		.navbar-inverse .navbar-collapse, .navbar-inverse .navbar-form{
			border:none;
		}
		.navbar-nav {
			margin: 0 -15px;
		}
	</style>
	
<style>
.chosen-container {
	width: 180px !important;
}
</style>

<!-- Page Container -->
<!-- In the PHP version you can set the following options from inc/config.jsp file -->
<!--
    Available #page-container classes:

    '' (None)                                       for a full main and alternative sidebar hidden by default (> 991px)

    'sidebar-visible-lg'                            for a full main sidebar visible by default (> 991px)
    'sidebar-partial'                               for a partial main sidebar which opens on mouse hover, hidden by default (> 991px)
    'sidebar-partial sidebar-visible-lg'            for a partial main sidebar which opens on mouse hover, visible by default (> 991px)

    'sidebar-alt-visible-lg'                        for a full alternative sidebar visible by default (> 991px)
    'sidebar-alt-partial'                           for a partial alternative sidebar which opens on mouse hover, hidden by default (> 991px)
    'sidebar-alt-partial sidebar-alt-visible-lg'    for a partial alternative sidebar which opens on mouse hover, visible by default (> 991px)

    'sidebar-partial sidebar-alt-partial'           for both sidebars partial which open on mouse hover, hidden by default (> 991px)

    'sidebar-no-animations'                         add this as extra for disabling sidebar animations on large screens (> 991px) - Better performance with heavy pages!

    'style-alt'                                     for an alternative main style (without it: the default style)
    'footer-fixed'                                  for a fixed footer (without it: a static footer)

    'header-fixed-top'                              has to be added only if the class 'navbar-fixed-top' was added on header.navbar
    'header-fixed-bottom'                           has to be added only if the class 'navbar-fixed-bottom' was added on header.navbar
-->

 <div id="page-container" class="sidebar-partial sidebar-visible-lg sidebar-no-animations footer-fixed">
 			<!-- START Header -->	
			<header class="navbar navbar-inverse">
	          <!-- Navbar Header -->
                    <div class="navbar-header">
							<ul class="nav navbar-nav-custom  visible-xs  visible-sm ">
				                    <li>
					                    <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');">
					                        <i class="fa fa-bars fa-fw"></i>
					                    </a>
					                </li>
				                </ul>
							
						<!-- Main Sidebar Toggle Button -->
                        <ul class="nav navbar-nav-custom">
                           
                                 <!-- Brand -->
								<a href="<%=contexturl %>Index" class="sidebar-brand-logo">
									<img src="<%=contexturl %>resources/img/logo.png">
								</a>
								<!-- END Brand -->
                           
                        </ul>
                        <!-- END Main Sidebar Toggle Button -->
						
                        <!-- Horizontal Menu Toggle + Alternative Sidebar Toggle Button, Visible only in small screens (< 768px) -->
                        <ul class="nav navbar-nav-custom pull-right visible-xs">
                            <li>
                                <a href="javascript:void(0)" data-toggle="collapse" data-target="#horizontal-menu-collapse">Menu</a>
                            </li>
                          <!--  <li>
                                <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar-alt');">
                                    <i class="gi gi-share_alt"></i>
                                    <span class="label label-primary label-indicator animation-floating">4</span>
                                </a>
                            </li> -->
				<li class="dropdown">
					<%
							if (user.getProfileImage() != null
									&& !user.getProfileImage().getImagePath().isEmpty()) {
						%> <a href="javascript:void(0)" class="dropdown-toggle"
					data-toggle="dropdown"> <img
						src="<%=contexturl+user.getProfileImage().getImagePath() %>"
						alt="avatar"> <i class="fa fa-angle-down"></i>
				</a> <%
							} else {
						%> <a href="javascript:void(0)" class="dropdown-toggle"
					data-toggle="dropdown"> <img
						src="<%=contexturl %>resources/img/application/default_profile.png"
						alt="avatar"> <i class="fa fa-angle-down"></i>
				</a> <%
							}
						%>


					<ul class="dropdown-menu dropdown-custom dropdown-menu-right" style="z-index: 10000;">
						<li class="dropdown-header text-center">Account</li>
						<li>
							<!-- 										<a href="page_ready_timeline.html"> --> <!-- 											<i class="fa fa-clock-o fa-fw pull-right"></i> -->
							<!-- 											<span class="badge pull-right">10</span> --> <!-- 											Updates -->
							<!-- 										</a> --> <!-- 										<a href="page_ready_inbox.html"> -->
							<!-- 											<i class="fa fa-envelope-o fa-fw pull-right"></i> -->
							<!-- 											<span class="badge pull-right">5</span> --> <!-- 											Messages -->
							<!-- 										</a> --> <!-- 										<a href="page_ready_pricing_tables.html"><i class="fa fa-magnet fa-fw pull-right"></i> -->
							<!-- 											<span class="badge pull-right">3</span> --> <!-- 											Subscriptions -->
							<!-- 										</a> --> <!-- 										<a href="CommingSoon"><i class="fa fa-question fa-fw pull-right"></i> -->
							<!-- 											<span class="badge pull-right">11</span> --> <!-- 											FAQ -->
							<!-- 										</a> -->
						</li>
						<li class="divider"></li>
						<li><a href="<%=contexturl %>Member/Dashboard"> <i
								class="fa fa-user fa-fw pull-right"></i> Profile
						</a> <!-- Opens the user settings modal that can be found at the bottom of each page (page_footer.html in PHP version) -->
							<a href="#modal-user-settings" data-toggle="modal"> <i
								class="fa fa-cog fa-fw pull-right"></i> Settings
						</a></li>
						<li class="divider"></li>
						<li><a href="#logout_popup"><i
								class="fa fa-ban fa-fw pull-right"></i> Logout</a></li>

					</ul>
				</li>
			</ul>
						   <!-- END Horizontal Menu Toggle + Alternative Sidebar Toggle Button -->
					
					
                    </div>
                    <!-- END Navbar Header -->
                    
					<!-- User Dropdown -->
						 <ul class="nav navbar-nav-custom pull-right hidden-xs">							
							<li class="dropdown">
								<%
							if (user.getProfileImage() != null
									&& !user.getProfileImage().getImagePath().isEmpty()) {
						%>
							<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
									<img src="<%=contexturl+user.getProfileImage().getImagePath() %>" alt="avatar"> <i class="fa fa-angle-down"></i>
							</a>
						<%
							} else {
						%>
						<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
									<img src="<%=contexturl %>resources/img/application/default_profile.png" alt="avatar"> <i class="fa fa-angle-down"></i>
								</a>

						<%
							}
						%>
								<ul class="dropdown-menu dropdown-custom dropdown-menu-right" style="z-index: 10000;">
									<li class="dropdown-header text-center">Account</li>
									<li>
									<a href="<%=contexturl %>MyProfile" id="editProfile"><i
									class="fa fa-user fa-fw pull-right"></i>Profile</a></li>
									<li class="divider"></li>
									<li><a href="#logout_popup"  data-toggle="modal">
									<i class="gi gi-exit pull-right"></i> Logout</a></li>
			
								</ul>
							</li>                      
                     	  </ul>
					<!-- END User Dropdown --> 
					
					
                    <!-- Alternative Sidebar Toggle Button, Visible only in large screens (> 767px) -->
<!--                     <ul class="nav navbar-nav-custom pull-right hidden-xs"> -->
<!--                         <li> -->
<!--                             If you do not want the main sidebar to open when the alternative sidebar is closed, just remove the second parameter: App.sidebar('toggle-sidebar-alt'); -->
<!--                             <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar-alt', 'toggle-other');"> -->
<!--                                 <i class="gi gi-share_alt"></i> -->
<!--                                 <span class="label label-primary label-indicator animation-floating">4</span> -->
<!--                             </a> -->
<!--                         </li> -->
<!--                     </ul> -->
                    <!-- END Alternative Sidebar Toggle Button -->

					
						   
                    <!-- Horizontal Menu + Search -->
                    <div id="horizontal-menu-collapse" class="collapse navbar-collapse">
						<form action="<%=contexturl %>QuickSearch" class="navbar-form navbar-left" role="search" id="QuickSearchForm" method="post">
                            <div class="form-group">
								<input type="text" id="top-search" name="keyword" class="form-control" placeholder="<spring:message code="placeholder.keyword"/>" value="${keyword}">
								<input type="hidden" name="start" id="paginationStart" value="0">
								<input type="hidden" name="end" id="paginationEnd" value="0">
								<input type="hidden" name="pageno" id="paginationPageno" value="1">
                            </div>
                        </form>
                        <ul class="nav navbar-nav">
                            <li>
                                <a href="<%=contexturl %>StudentDashboard">Home</a>
                            </li>
                            <li>
                                <a href="<%=contexturl %>SearchResult">Courses</a>
                            </li>
                            <!-- 
                            <li class="dropdown">
                                <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">Settings <i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu">
                                    <li><a href="javascript:void(0)"><i class="fa fa-asterisk fa-fw pull-right"></i> General</a></li>
                                    <li><a href="javascript:void(0)"><i class="fa fa-lock fa-fw pull-right"></i> Security</a></li>
                                    <li><a href="javascript:void(0)"><i class="fa fa-user fa-fw pull-right"></i> Account</a></li>
                                    <li><a href="javascript:void(0)"><i class="fa fa-magnet fa-fw pull-right"></i> Subscription</a></li>                              
                                </ul>
                            </li> 
                             -->
<!--                             <li> -->
<%--                             	<a href="#modal-change-city" data-toggle="modal" title="Change City" onclick="populateCityPopup();"><%=((CityMaster)session.getAttribute("City")).getCityName()%> --%>
<!--                             		<i class="fa fa-angle-down" style="padding-left: 5px;"></i></a> -->
<!--                             </li>                        -->
                        </ul>
                     
                    </div>
                    <!-- END Horizontal Menu + Search -->
                </header>
            <!-- END Header -->	
      
			
			<!-- Alternative Sidebar -->
            <div id="sidebar-alt">
                <!-- Wrapper for scrolling functionality -->
                <div class="sidebar-scroll">
                    <!-- Sidebar Content -->
                    <div class="sidebar-content">
                        <!-- Chat -->
                        <!-- Chat demo functionality initialized in js/app.js -> chatUi() -->
                        <a href="javascript:void(0)" class="sidebar-title">
                            <i class="gi gi-comments pull-right"></i> <strong>Chat</strong>UI
                        </a>
                        <!-- Chat Users -->
                        <ul class="chat-users clearfix">
                            <li>
                                <a href="javascript:void(0)" class="chat-user-online">
                                    <span></span>
                                    <img src="<%=contexturl %>resources/img/placeholders/avatars/avatar12.jpg" alt="avatar" class="img-circle">
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="chat-user-online">
                                    <span></span>
                                    <img src="<%=contexturl %>resources/img/placeholders/avatars/avatar15.jpg" alt="avatar" class="img-circle">
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="chat-user-online">
                                    <span></span>
                                    <img src="<%=contexturl %>resources/img/placeholders/avatars/avatar10.jpg" alt="avatar" class="img-circle">
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="chat-user-online">
                                    <span></span>
                                    <img src="<%=contexturl %>resources/img/placeholders/avatars/avatar4.jpg" alt="avatar" class="img-circle">
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="chat-user-away">
                                    <span></span>
                                    <img src="<%=contexturl %>resources/img/placeholders/avatars/avatar7.jpg" alt="avatar" class="img-circle">
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="chat-user-away">
                                    <span></span>
                                    <img src="<%=contexturl %>resources/img/placeholders/avatars/avatar9.jpg" alt="avatar" class="img-circle">
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="chat-user-busy">
                                    <span></span>
                                    <img src="<%=contexturl %>resources/img/placeholders/avatars/avatar16.jpg" alt="avatar" class="img-circle">
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <span></span>
                                    <img src="<%=contexturl %>resources/img/placeholders/avatars/avatar1.jpg" alt="avatar" class="img-circle">
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <span></span>
                                    <img src="<%=contexturl %>resources/img/placeholders/avatars/avatar4.jpg" alt="avatar" class="img-circle">
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <span></span>
                                    <img src="<%=contexturl %>resources/img/placeholders/avatars/avatar3.jpg" alt="avatar" class="img-circle">
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <span></span>
                                    <img src="<%=contexturl %>resources/img/placeholders/avatars/avatar13.jpg" alt="avatar" class="img-circle">
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <span></span>
                                    <img src="<%=contexturl %>resources/img/placeholders/avatars/avatar5.jpg" alt="avatar" class="img-circle">
                                </a>
                            </li>
                        </ul>
                        <!-- END Chat Users -->

                        <!-- Chat Talk -->
                        <div class="chat-talk display-none">
                            <!-- Chat Info -->
                            <div class="chat-talk-info sidebar-section">
                                <img src="<%=contexturl %>resources/img/placeholders/avatars/avatar5.jpg" alt="avatar" class="img-circle pull-left">
                                <strong>John</strong> Doe
                                <button id="chat-talk-close-btn" class="btn btn-xs btn-default pull-right">
                                    <i class="fa fa-times"></i>
                                </button>
                            </div>
                            <!-- END Chat Info -->

                            <!-- Chat Messages -->
                            <ul class="chat-talk-messages">
                                <li class="text-center"><small>Yesterday, 18:35</small></li>
                                <li class="chat-talk-msg animation-slideRight">Hey admin?</li>
                                <li class="chat-talk-msg animation-slideRight">How are you?</li>
                                <li class="text-center"><small>Today, 7:10</small></li>
                                <li class="chat-talk-msg chat-talk-msg-highlight themed-border animation-slideLeft">I'm fine, thanks!</li>
                            </ul>
                            <!-- END Chat Messages -->

                            <!-- Chat Input -->
                            <form action="index.html" method="post" id="sidebar-chat-form" class="chat-form">
                                <input type="text" id="sidebar-chat-message" name="sidebar-chat-message" class="form-control form-control-borderless" placeholder="Type a message..">
                            </form>
                            <!-- END Chat Input -->
                        </div>
                        <!--  END Chat Talk -->
                        <!-- END Chat -->

                        <!-- Activity -->
                        <a href="javascript:void(0)" class="sidebar-title">
                            <i class="fa fa-globe pull-right"></i> <strong>Activity</strong>UI
                        </a>
                        <div class="sidebar-section">
                            <div class="alert alert-danger alert-alt">
                                <small>just now</small><br>
                                <i class="fa fa-thumbs-up fa-fw"></i> Upgraded to Pro plan
                            </div>
                            <div class="alert alert-info alert-alt">
                                <small>2 hours ago</small><br>
                                <i class="gi gi-coins fa-fw"></i> You had a new sale!
                            </div>
                            <div class="alert alert-success alert-alt">
                                <small>3 hours ago</small><br>
                                <i class="fa fa-plus fa-fw"></i> <a href="page_ready_user_profile.html"><strong>John Doe</strong></a> would like to become friends!<br>
                                <a href="javascript:void(0)" class="btn btn-xs btn-primary"><i class="fa fa-check"></i> Accept</a>
                                <a href="javascript:void(0)" class="btn btn-xs btn-default"><i class="fa fa-times"></i> Ignore</a>
                            </div>
                            <div class="alert alert-warning alert-alt">
                                <small>2 days ago</small><br>
                                Running low on space<br><strong>18GB in use</strong> 2GB left<br>
                                <a href="page_ready_pricing_tables.html" class="btn btn-xs btn-primary"><i class="fa fa-arrow-up"></i> Upgrade Plan</a>
                            </div>
                        </div>
                        <!-- END Activity -->

                        <!-- Messages -->
                        <a href="page_ready_inbox.html" class="sidebar-title">
                            <i class="fa fa-envelope pull-right"></i> <strong>Messages</strong>UI (5)
                        </a>
                        <div class="sidebar-section">
                            <div class="alert alert-alt">
                                Debra Stanley<small class="pull-right">just now</small><br>
                                <a href="page_ready_inbox_message.html"><strong>New Follower</strong></a>
                            </div>
                            <div class="alert alert-alt">
                                Sarah Cole<small class="pull-right">2 min ago</small><br>
                                <a href="page_ready_inbox_message.html"><strong>Your subscription was updated</strong></a>
                            </div>
                            <div class="alert alert-alt">
                                Bryan Porter<small class="pull-right">10 min ago</small><br>
                                <a href="page_ready_inbox_message.html"><strong>A great opportunity</strong></a>
                            </div>
                            <div class="alert alert-alt">
                                Jose Duncan<small class="pull-right">30 min ago</small><br>
                                <a href="page_ready_inbox_message.html"><strong>Account Activation</strong></a>
                            </div>
                            <div class="alert alert-alt">
                                Henry Ellis<small class="pull-right">40 min ago</small><br>
                                <a href="page_ready_inbox_message.html"><strong>You reached 10.000 Followers!</strong></a>
                            </div>
                        </div>
                        <!-- END Messages -->
                    </div>
                    <!-- END Sidebar Content -->
                </div>
                <!-- END Wrapper for scrolling functionality -->
            </div>
            <!-- END Alternative Sidebar -->
         
         