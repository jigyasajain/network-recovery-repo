
<%@page import="com.astrika.common.model.MemberType"%>
<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>

<style>
.els{
overflow: hidden;
text-overflow: ellipsis;
-o-text-overflow: ellipsis;
white-space: nowrap;
width: 100%;
}
.chosen-container {
	width: 250px !important;
}
</style>

<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				<i class="fa fa-map-marker"></i><spring:message code="heading.memberprofile"/><br><small></small>
			</h1>
			<span id="errorMsg"></span>
		</div>
	</div>
	
	<form action="<%=contexturl %>UpdateMember" method="post" class="form-horizontal "
		id="member_form" >
<!-- 		enctype="multipart/form-data"> -->
		<input type="hidden" name="memberType" value="<%=" "%>">
		<input type="hidden" name="client" value="WEB" />
		<div class="row">
                  
			<div class="col-md-6">
				<div class="block">
					<div class="block-title">
						<h2>
							<%-- 							<strong><spring:message code="heading.admininfo"/></strong> --%>
							<strong><spring:message code="heading.userinfo"/></strong>
						</h2>
					</div>
					   
					
					
					
					<div class="form-group">
						<label class="col-md-4 control-label" for="first_Name"><spring:message
								code="label.firstname" /></label>
						<div class="col-md-6">
							<input id="first_Name" name="firstName" class="form-control"
								placeholder="<spring:message code='label.firstname'/>.."
								type="text" value="${member.user.firstName}" disabled="disabled">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="last_Name"><spring:message
								code="label.lastname" /> </label>
						<div class="col-md-6">

							<input id="last_Name" name="lastName" class="form-control"
								placeholder="<spring:message code="label.lastname"/>.."
								type="text" value="${member.user.lastName}" disabled="disabled">
						</div>
					</div>
					
<!-- 					<div class="form-group"> -->
<%-- 							<label class="col-md-4 control-label" for="example-daterange1"><spring:message code="label.profile"/> --%>
<!-- 								</label> -->
<!-- 							<div class="col-md-6"> -->
<!-- 								<input type="file" name="profile" accept="image/*"/> -->
<!-- 							</div> -->
<!-- 					</div> -->
 

					<div class="form-group">
						<label class="col-md-4 control-label" for="email"><spring:message
								code="label.emailid" /></label>
						<div class="col-md-6">
							<input id="emailId1" name="emailId1" class="form-control"
								placeholder="test@example.com" type="text" value="${member.user.emailId}" disabled="disabled">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="mobile"><spring:message
								code="label.mobile" /></label>
						<div class="col-md-6">
							<input id="mobile" name="mobile" class="form-control"
								placeholder='<spring:message code="label.mobile"></spring:message>'
								type="text" value="${member.user.mobile}" disabled="disabled">
						</div>
					</div>
					
					<div class="form-group">
                        <label class="col-md-4 control-label"><spring:message code="label.userlogin"/></label>
                        <div class="col-md-8">
                            <p class="form-control-static">${member.user.loginId}</p>
                        </div>
                    </div>
					

				</div>

				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.personaldetails"/></strong>
						</h2>
					</div>


					<div class="form-group">
						<label class="col-md-4 control-label" for="birthday"><spring:message code="label.birthday"/></label>
						<div class="col-md-6">
						<input id="birthday" name="birthday" class="form-control"
								placeholder='<spring:message code="label.birthday"></spring:message>'
								type="text" value="${birthday}" disabled="disabled">
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label" for="gender"><spring:message code="label.gender"/></label>
						<div class="col-md-6">
						
						<input id="memberGender" name="memberGender" class="form-control"
								placeholder='<spring:message code="label.gender"></spring:message>'
								type="text" value="${ member.gender}" disabled="disabled">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="maritalStatus"><spring:message code="label.maritalstatus"/>
							</label>
						<div class="col-md-6">
						<input id="membermaritalStatus" name="membermaritalStatus" class="form-control"
								placeholder='<spring:message code="label.maritalstatus"></spring:message>'
								type="text" value="${  member.maritalStatus}" disabled="disabled">
						</div>
					</div>
					<c:choose>
					<c:when test="${'Married' eq member.maritalStatus}">
					<div class="form-group" id="aniversary_div">
						<label class="col-md-4 control-label" for="anniversary"><spring:message code="label.anniversary"/></label>
						<div class="col-md-6">
						<input id="anniversary" name="anniversary" class="form-control"
								placeholder='<spring:message code="label.anniversary"></spring:message>'
								type="text" value="${anniversary}" disabled="disabled">
							
						</div>
					</div>
					</c:when>
					
				</c:choose>
              <c:if test="${! empty member.favoriteCuisine}">
				<div class="form-group">
							<label class="col-md-4 control-label" for="company_name"><spring:message
									code="label.cuisine" /></label>
							<div class="col-md-6">
							  
							<label class="col-md-6 control-label" for="anniversary" title="${fn:replace(member.favoriteCuisine, ',', ', ')}">${fn:replace(member.favoriteCuisine, ',', ', ')}</label>
							</div>	
								
							
				</div>
				</c:if>
					<div class="form-group">


						<div class="col-sm-4">
							<label class="col-md-6 control-label" for="creditCard"><spring:message code="label.smoking"/></label>
							<div class="col-md-4">
								<label class="switch switch-primary"> <input 
									id="smoking" name="smoking" type="checkbox" disabled="disabled"<c:if test="${member.smoking}"> checked </c:if> /> <span></span></label>
							</div>
						</div>
						<div class="col-sm-4">
							<label class="col-md-6 control-label" for="alcohol"><spring:message code="label.alcohol"/></label>
							<div class="col-md-4">
								<label class="switch switch-primary"> <input 
									id="alcohol" name="alcohol" type="checkbox" disabled="disabled" <c:if test="${member.preferAlcohol}"> checked </c:if> /> <span></span></label>
							</div>
						</div>
						<div class="col-sm-4">
							<label class="col-md-6 control-label" for="nonVeg"><spring:message code="label.nonvegitarian"/>
								</label>
							<div class="col-md-4">
								<label class="switch switch-primary"> <input
									id="nonVeg" name="nonVeg" type="checkbox" disabled="disabled"<c:if test="${member.nonvegeterian}"> checked </c:if>/> <span></span></label>
							</div>
						</div>
					</div>

				</div>
			</div>



			<div class="col-md-6">



				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.contactinfo" /></strong>
						</h2>
					</div>

					


					<div class="form-group">
						<label class="col-md-4 control-label" for="addressline1"><spring:message
								code="label.addressline1" />
						</label>
						<div class="col-md-6">

							<input id="memberAddressline1" name="memberAddressLine1"
								class="form-control"
								placeholder="<spring:message code="label.addressline1"/>.."
								type="text" value="${member.addressLine1}" disabled="disabled">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="addressline2"><spring:message
								code="label.addressline2" /></label>
						<div class="col-md-6">

							<input id="memberAddressline2" name="memberAddressLine2"
								class="form-control"
								placeholder="<spring:message code="label.addressline2"/>.."
								type="text" value="${member.addressLine2}" disabled="disabled">
						</div>
					</div>
					
					
					
					
					<div class="form-group">
						<label class="col-md-4 control-label" for="country"><spring:message
								code="label.countryname" /></label>
						<div class="col-md-6">

							<input id="member_CountryId" name="memberCountryId"
								class="form-control"
								placeholder="<spring:message code="label.countryname"/>.."
								type="text" value="${member.user.country.countryName}" disabled="disabled">
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label" for="city"><spring:message
								code="label.cityname" /></label>
						<div class="col-md-6">
							<input id="member_CityId" name="memberCityId"
								class="form-control"
								placeholder="<spring:message code="label.cityname"/>.."
								type="text" value="${member.user.city.cityName}" disabled="disabled">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="area"><spring:message
								code="label.areaname" /></label>
						<div class="col-md-6">
							
							<input id="member_AreaId" name="memberAreaId"
								class="form-control"
								placeholder="<spring:message code="label.areaname"/>.."
								type="text" value="${member.area.areaName}" disabled="disabled">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="memberPincode"><spring:message
								code="label.pincode" /></label>
						<div class="col-md-6">

							<input id="memberPincode" name="memberPincode"
								class="form-control"
								placeholder="<spring:message code="label.pincode"/>.."
								type="text" value="${member.pincode}" disabled="disabled">
						</div>
					</div>




				</div>

				

			</div>

		</div>



		<div class="form-group form-actions">
			<div class="col-md-9 col-md-offset-3">
				
				
				<a class="btn btn-sm btn-primary" href="<%=contexturl%>ManageMembers/ShowMemberList/${member.user.memberType.id}"><i
					class="gi gi-remove"></i> <spring:message code="button.back" />
				</a>
			</div>
		</div>



	</form>
	
	
	
	
</div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>





<%@include file="../../inc/template_end.jsp"%>