<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>
<div id="page-content">
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="fa fa-map-marker"></i>
                <spring:message code="heading.coursebatch" />
                <br> <small><spring:message
                        code="heading.coursebatchdetails" /></small>
            </h1>
            <span id="errorMsg"></span>
            <c:if test="${!empty success}">
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert"
                        aria-hidden="true">x</button>
                    <h4>
                        <i class="fa fa-check-circle"></i> <spring:message code="label.success"/>
                    </h4>
                    <spring:message code="${success}" />
                </div>
            </c:if>

            <c:if test="${!empty error}">
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert"
                        aria-hidden="true">�</button>
                    <h4>
                        <i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
                    </h4>
                    <spring:message code="${error}" />
                </div>
            </c:if>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><spring:message code="heading.coursebatch" /></li>
    </ul>
    <div class = "row">
    <div class="col-md-6">
        
            <div class="block full" >
                    <div class="block-title">
                        <h2>
                            <strong><spring:message code="heading.coursebatchinfo" /></strong>
                        </h2>
                    </div>
                    <form action="<%=contexturl %>ManageCourseBatch/CourseBatchList/UpdateCourseBatch" method="post" class="form-horizontal "
                        id="Course_Batch_Form" >
                            <input name="courseBatchId" value="${courseBatch.courseBatchId}" type="hidden">
                            <input id="confirmation" name="confirmation" type="hidden" value="false">
                            <input id="publish" name="publish" type="hidden" value="false">
                            
                    <div class="form-group" id="CourseBatchDiv">
                    <label class="col-md-3 control-label" for="CourseBatch_Name"><spring:message
                            code="label.course" /><span class="text-danger">*</span></label>
                    <div class="col-md-7">
                        <select class="CourseBatch_chosen" style="width: 400px;" id="fromCourseBatchId"
                            name="chosenCourseId"
                            data-placeholder="<spring:message code='label.choosecoursebatch' />" disabled="disabled">
                            <option value=""></option>
                            <c:forEach items="${courseList}" var="course">
                                <option value="${course.courseId}" <c:if test="${courseBatch.course.courseId eq course.courseId}">selected="selected"</c:if>>${course.courseName}</option>
                            </c:forEach>
                        </select>
                    </div>

                </div>
                <div class="form-group" id="InstructorDiv">
                    <label class="col-md-3 control-label" for="countryFrom_Name"><spring:message
                            code="heading.instructor" /><span class="text-danger">*</span></label>
                    <div class="col-md-7">
                        <select multiple class="Instructor_chosen" style="width: 400px;" id="fromInstructorId"
                            name="chosenInstructorId"
                            data-placeholder="<spring:message code='label.chooseinstructor' />" disabled="disabled">
                            <option value=""></option>
                            <c:forEach items="${instructorList}" var="instructor">
                            <c:set var="present" value="false" />
                                        <c:forEach items="${instructors}" var="instructors">
                                            <c:if test="${instructor.user.userId eq instructors.user.userId}">
                                            <c:set var="present" value="true" />    
                                            </c:if>
                                            
                                        </c:forEach>
                                        <c:if test="${instructor.user.userId eq instructorMain.user.userId}">
                                                <c:set var="present" value="true" />
                                            </c:if>
                                        <option value="${instructor.user.userId}"
                                            <c:if test="${present}">    selected="selected" </c:if>>
                                            ${instructor.user.firstName} ${ instructor.user.lastName}</option>
                                        
                            </c:forEach>
                                
                        </select>
                    </div>

                </div>
                
                <div class="form-group">
                        <label class="col-md-3 control-label" for="example-daterange1"><spring:message code="label.batchperiod"/>
                            </label>
                        <div class="col-md-7">
                            <div class="input-group input-daterange"
                                data-date-format="mm/dd/yyyy">
                                <input disabled="disabled" type="text" id="startDate"
                                    name="startDate" class="form-control text-center" data-date-format="MM/dd/yyyy"
                                    placeholder="From" value="<fmt:formatDate pattern="<%=propvalue.dateFormat %>"  value="${courseBatch.startDate}"/>"> <span class="input-group-addon"><i
                                    class="fa fa-angle-right"></i></span> <input disabled="disabled" type="text"
                                    id="endDate" name="endDate"
                                    class="form-control text-center" placeholder="To" data-date-format="MM/dd/yyyy" value="<fmt:formatDate pattern="<%=propvalue.dateFormat %>"  value="${courseBatch.startDate}"/>">
                            </div>
                        </div>
                    </div>
                
<!--                <div class="form-group" id="startDate"> -->
<%--                <label class="col-md-3 control-label" for="batch_start"><spring:message --%>
<%--                        code='label.startdate' /><span class="text-danger">*</span></label> --%>
<!--                <div class="col-md-9"> -->
<!--                    <input type="text" id="startDate" name="startDate" style="width: 200px;" -->
<!--                        class="form-control input-datepicker"  data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" -->
<%--                        placeholder="<spring:message code='label.dateformat' />" value="<fmt:formatDate pattern="<%=propvalue.SIMPLE_DATE_FORMAT %>"  value="${courseBatch.startDate}"/>"> --%>
<!--                </div> -->
<!--                </div> -->
                
<!--                <div class="form-group" id="endDate"> -->
<%--                <label class="col-md-3 control-label" for="batch_end"><spring:message --%>
<%--                        code='label.enddate' /><span class="text-danger">*</span></label> --%>
<!--                <div class="col-md-9"> -->
<!--                    <input type="text" id="endDate" name="endDate" style="width: 200px;" -->
<!--                        class="form-control input-datepicker"  data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" -->
<%--                        placeholder="<spring:message code='label.dateformat' />" value="<fmt:formatDate pattern="<%=propvalue.SIMPLE_DATE_FORMAT %>"  value="${courseBatch.endDate}"/>"> --%>
<!--                </div> -->
<!--                </div> -->
                
                    
                    
                    
                    <div class="block">
            <div style="text-align: center; margin-bottom: 10px">
                    
                    <a href="<%=contexturl %>ManageCourseBatch/CourseBatchList/" class="btn btn-sm btn-primary"
                    style="text-decoration: none;"> <spring:message
                        code="button.back" /></a>
            </div>
        </div>
    </form>
</div>

</div>
</div>
    <div class="col-md-6">
        
            <div class="block full">
                <div class="block-title">
                    <h2>
                        <strong><spring:message code="heading.coursebatchinviteresult" /></strong>
                    </h2>
                </div>
                
        
        
        <div class="table-responsive">
            <table id="active-datatable"
                class="table table-vcenter table-condensed table-bordered">
                <thead>
                    <tr>
                        <th class="text-center"><spring:message code="label.id" /></th>
                        <th class="text-center"><spring:message code="label.instructorname" /></th>
                        <th class="text-center"><spring:message code="label.instructorstatus" /></th>
<%--                        <th class="text-center"><spring:message code="label.actions" /></th> --%>
                    </tr>
                </thead>
                <tbody id="tbody">
                    <c:forEach var="currentBatchInstructor" items="${courseBatchInstructorList}"
                        varStatus="count">

                        <tr>
                            <td class="text-center">${count.count}</td>
                            <td class="text-left"><c:out
                                    value="${currentBatchInstructor.instructor.user.firstName}  ${currentBatchInstructor.instructor.user.lastName}" /></td>
                            <td class="text-right"><c:out
                                    value="${currentBatchInstructor.status}" /></td>
<!--                            <td class="text-center"> -->
                                
<%--                                <a class="btn btn-sm btn-primary" href="<%=contexturl %>ManageCourseBatch/CourseBatchList/"> --%>
<%--                               <spring:message code="button.delete" /> </a> --%>
                                
<!--                            </td> -->
                        </tr>
                    </c:forEach>

                </tbody>
            </table>
            <div class="form-group form-actions">
                <div class="col-md-9 col-md-offset-4">
                            <c:choose>
                                <c:when test="${courseBatch.status eq status}">
                                </c:when>
                                <c:otherwise>
                                    <a href="#publish_batch_pop" data-toggle="modal"
                                         class="btn btn-sm btn-primary"><spring:message
                                            code="button.publish" /> </a>
                                </c:otherwise>
                            </c:choose>
                            <a class="btn btn-sm btn-primary " href="<%=contexturl %>ManageCourseBatch/CourseBatchList/">
                            <spring:message code="button.back" /> </a>
                    
                </div>
            </div>
        </div>
            
    
            </div>
        
    </div>
    <div id="publish_batch_pop" class="modal fade" tabindex="-1"
        role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <form action="<%=contexturl %>ManageCourseBatch/CourseBatchList/ViewCourseBatch" method="post" class="form-horizontal form-bordered" id="publish_batch_Form">
                        
                        <div style="padding: 10px; height: 110px;">
                            <label><spring:message
                                    code="validation.doyouwantopublishthiscoursebatch" /></label>
                            <div class="col-xs-12 text-right">
                                <button type="button" class="btn btn-sm btn-default"
                                    data-dismiss="modal"><spring:message code="label.no" /></button>
                                <div id="publish_batch" class="btn btn-sm btn-primary"> <spring:message code="label.yes" /></div>
                            </div>
                        </div>
                        <input type="hidden" name="id" id="publishbatchId" value = "${id}">
                        <input type="hidden" name="publish"  value = "true">
                    </form>
                </div>
            </div>
        </div>
    </div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<script
    src="<%=contexturl%>resources/js/pages/activeTablesDatatables.js"></script>
<script
    src="<%=contexturl%>resources/js/pages/inactiveTablesDatatables.js"></script>
<script>$(function(){ ActiveTablesDatatables.init(2); });</script>
<script>$(function(){ InActiveTablesDatatables.init(2); });</script>


<script type="text/javascript">
$(document).ready(function() {
    $(".CourseBatch_chosen").data("placeholder","Select CourseBatch From...").chosen();
    $(".Instructor_chosen").data("placeholder","Select Instructor From...").chosen();
    
    $("#publish_batch").click(function(){   
        $("#publish_batch_Form").submit();
    });
    


});
</script>