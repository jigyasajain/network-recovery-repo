<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>

<style>
.chosen-container {
    width: 250px !important;
}
</style>

<div id="page-content">
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="fa fa-map-marker"></i>Membership Details <br>
                <c:if test="${membership==null}">
                    <small><a href="#currency_popup" data-toggle="modal" onclick="setURL('AddMembership')" class="btn btn-sm btn-primary">Add Membership</a></small>
                </c:if>
                <c:if test="${!empty membership}">
                    <small><a href="UpgradeMembership" class="btn btn-sm btn-primary">Upgrade Membership</a></small>
                </c:if>
                <c:if test="${!empty chnagemembership}">
                    <small><a href="ChangeMembership" class="btn btn-sm btn-primary">Change Membership</a></small>
                </c:if>
            </h1>
            <span id="errorMsg"></span>
        </div>
    </div>

    <form action="" method="post" class="form-horizontal">
        <div class="row">
            <div class="col-md-4">
                <div class="block">
                    <div class="block-title">
                        <h2>
                            <strong>Membership</strong>
                        </h2>
                    </div>
                    <div class="form-group">
                        <label class="col-md-6 control-label">Activation Date</label>
                        <div class="col-md-6">
                            <p class="form-control-static"><fmt:formatDate pattern="<%=propvalue.simpleDatetimeFormat %>"  value="${membership.activationDate}" /></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-6 control-label">Expiry Date</label>
                        <div class="col-md-6">
                            <p class="form-control-static"><fmt:formatDate pattern="<%=propvalue.simpleDatetimeFormat %>"  value="${membership.expiryDate}" /></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-6 control-label">Number Of Users</label>
                        <div class="col-md-6">
                            <p class="form-control-static">${membership.numberOfUsers}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-6 control-label">Tenure In Month</label>
                        <div class="col-md-6">
                            <p class="form-control-static">${membership.tenureInMonth}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-6 control-label">Discount Value</label>
                        <div class="col-md-6">
                            <p class="form-control-static">${membership.paymentCurrency.currencySign} ${membership.discountValue}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-6 control-label">Total Amount Paid</label>
                        <div class="col-md-6">
                            <p class="form-control-static">${membership.paymentCurrency.currencySign} ${membership.totalAmountPaid}</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="block">
                    <div class="block-title">
                        <h2>
                            <strong>Selected Location</strong>
                        </h2>
                    </div>
                    <c:forEach var="country" items="${membership.countryDetails}"
                        varStatus="count">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="row ">
                                    <div class="col-xs-3">
                                        <label class="switch switch-primary"> <input
                                            name="country" type="checkbox"
                                            <c:if test="${country.key.selected}"> checked </c:if> disabled="disabled"/> <span></span>
                                        </label>
                                    </div>
                                    <div class="col-xs-8">
                                        <h4 class="panel-title">
                                            <i class="fa fa-angle-right"></i> <a class="accordion-toggle"
                                                data-toggle="collapse" data-parent="#country" href="#country_${country.key.name}">
                                                ${country.key.name} </a>
                                        </h4>
                                    </div>
                                </div>
                            </div>
                            <div id="country_${country.key.name}" class="panel-collapse collapse in"
                                style="height: auto;">
                                <c:forEach var="city" items="${country.value}"
                                    varStatus="count2">
                                    <div class="panel-body">
                                        <div class="row ">
                                            <div class="col-xs-3">
                                                <label class="switch switch-primary"> <input
                                                    name="city" type="checkbox"
                                                    disabled="disabled"
                                                    <c:choose>
                                                        <c:when test="${country.key.selected}">checked </c:when>
                                                        <c:when test="${city.selected}"> checked </c:when>
                                                        <c:otherwise></c:otherwise>
                                                    </c:choose> 
                                                    /><span></span>
                                                </label>
                                            </div>
                                            
                                            <div class="col-xs-8">
                                                <p>${city.name}</p>
                                            </div>
                                        </div>
                                    </div>
                                </c:forEach>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="block">
                    <div class="block-title">
                        <h2>
                            <strong>Selected Brands</strong>
                        </h2>
                    </div>
                    <c:forEach var="brand" items="${membership.brandDetails}" varStatus="count">
                        <div class="row ">
                            <div class="col-xs-3">
                                <label class="switch switch-primary"> <input
                                    <c:if test="${brand.selected}"> checked </c:if> name="brand"
                                    type="checkbox" disabled="disabled" /> <span></span> </label>
                            </div>
                            <div class="col-xs-8">
                                <p>${brand.name}</p>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </div>
    </form>
</div>

<div id="currency_popup" class="modal fade" tabindex="-1" role="dialog"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">
                    <i class="fa fa-pencil"></i>
                    <spring:message code="label.settings" />
                </h2>
            </div>
            <div class="modal-body">
                <form action="SaveCurrencyInSession" method="post"
                    id="currency_form" class="form-horizontal form-bordered">
                    <input type="hidden" id="toURL" name="toURL"/> 
                    <fieldset>
                        <legend>Select Currency</legend>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="currency"><spring:message
                                    code="label.currency" /><span class="text-danger">*</span> </label>
                            <div class="col-md-6">
                                <select class="currency_chosen" style="width: 200px;"
                                    id="currency" name="currencyId">
                                    <c:forEach items="${currencyList}" var="currency">
                                        <c:choose>
                                            <c:when test="${currency.active == true}">
                                                <option value="${currency.currencyId}">${currency.currencyName}</option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="placeholder">..</option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                    </fieldset>
                    <div class="form-group form-actions">
                        <div class="col-xs-12 text-right">
                            <button type="button" class="btn btn-sm btn-default"
                                data-dismiss="modal" onClick="window.location.reload()">
                                <spring:message code="label.close" />
                            </button>
                            <div id="currency_submit" class="save btn btn-sm btn-primary">
                                <spring:message code="label.savechanges" />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>

<script type="text/javascript">
    $(document).ready(function() {
        $(".currency_chosen").chosen();
    
        $("#currency_submit").click(function() {
            $("#currency_form").submit();
        });
    });
    
    function setURL(url){
        $("#toURL").val(url);
    }
</script>

<%@include file="../../inc/template_end.jsp"%>