<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>


<%-- <%@include file="../../inc/template_scripts.jsp"%> --%>

<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				<i class="fa fa-map-marker"></i>
				<spring:message code="heading.companymaster" />
				<br> <small><spring:message
						code="heading.editcompany" /></small>
			</h1>
			<span id="errorMsg"></span>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i> <spring:message code="label.success"/>
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>

			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">�</button>
					<h4>
						<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="menu.company" /></li>
	</ul>
	<form action="<%=contexturl %>ManageCompany/CompanyList/UpdateCompany" method="post" class="form-horizontal "
		id="Company_form" enctype="multipart/form-data">
		<div class="row">
			<input name="companyId" value="${company.companyId}" type="hidden">
			<input name="userId" value="${company.admin.userId}" type="hidden">
			<div class="col-md-6">
				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.companyinfo" /></strong>
<!-- 							<small style="float:left; margin-top: 4px;">This info is non editable and for view only purpose.</small> -->
						</h2>
					</div>
						
					<div class="form-group">
                        <label class="col-md-4 control-label"><spring:message code="label.companyname"/></label>
                        <div class="col-md-8">
                            <input id="company_Name" name="companyName" class="form-control"
        					placeholder="<spring:message code='label.companyname'/>.."
        					type="text" value="${company.companyName}">
                        </div>
                    </div>
				</div>
				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.contactinfo" /></strong> <br>
							<small style="float:left; margin-top: 4px;">All your Atma related communication will be done on these details. * These details can be same as your admin details.</small>
						</h2>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="telephone1"><spring:message
								code="label.telephone1" /></label>
						<div class="col-md-6">

							<input id="companyTelephone1" name="companyTelephone1"
								class="form-control"
								placeholder="<spring:message code='label.telephone1'/>.."
								type="text" value="${company.companyTelephone1}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="telephone2"><spring:message
								code="label.telephone2" /></label>
						<div class="col-md-6">

							<input id="companyTelephone2" name="companyTelephone2"
								class="form-control"
								placeholder="<spring:message code='label.telephone2'/>.."
								type="text" value="${company.companyTelephone2}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="rest_email"><spring:message
								code="label.companyEmail" /><span class="text-danger">*</span></label>
						<div class="col-md-6">

							<input id="company_email" name="companyEmail"
								class="form-control"
								placeholder="<spring:message code="label.companyEmail"/>.."
								type="text" value="${company.companyEmail}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="mobile"><spring:message
								code="label.mobile" /></label>
						<div class="col-md-6">

							<input id="companyMobile" name="companyMobile"
								class="form-control"
								placeholder="<spring:message code="label.mobile"/>.."
								type="text" value="${company.companyMobile}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="addressline1"><spring:message
								code="label.addressline1" />
						</label>
						<div class="col-md-6">

							<input id="companyAddressLine1" name="companyAddressLine1"
								class="form-control"
								placeholder="<spring:message code="label.addressline1"/>.."
								type="text" value="${company.companyAddressLine1}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="addressline2"><spring:message
								code="label.addressline2" /></label>
						<div class="col-md-6">

							<input id="companyAddressLine2" name="companyAddressLine2"
								class="form-control"
								placeholder="<spring:message code="label.addressline2"/>.."
								type="text" value="${company.companyAddressLine2}">
						</div>
					</div>
					
					<div class="form-group" id="countryDiv">
						<label class="col-md-4 control-label" for="country"><spring:message
								code="label.countryname" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<select class="country_chosen" style="width: 200px;"
								id="chosenCountryId"
								data-placeholder="<spring:message code='label.choosecountry' />"
								name="chosenCountryId">
								<c:forEach items="${countryList}" var="country">
									<option value="${country.countryId}"
										<c:if test="${country.countryId eq company.country.countryId}">selected="selected"</c:if>>${country.countryName}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					
					<div class="form-group" id="stateDiv">
						<label class="col-md-4 control-label" for="area"><spring:message
								code="label.statename" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<select class="state_chosen" style="width: 200px;"
								id="chosenStateId"
								data-placeholder="<spring:message code='label.choosestate' />"
								name="chosenStateId">
							</select>
						</div>
					</div>
					
					
					<div class="form-group" id="cityDiv">
						<label class="col-md-4 control-label" for="city"><spring:message
								code="label.cityname" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<select class="city_chosen" style="width: 200px;"
								id="chosenCityId"
								data-placeholder="<spring:message code='label.choosecity' />"
								name="chosenCityId">
							</select>
						</div>
					</div>

					
					<div class="form-group">
						<label class="col-md-4 control-label" for="companyPincode"><spring:message
								code="label.pincode" /><span class="text-danger">*</span></label>
						<div class="col-md-6">

							<input id="companyPincode" name="companyPincode"
								class="form-control"
								placeholder="<spring:message code="label.pincode"/>.."
								type="text" value="${company.companyPincode}">
						</div>
					</div>
 				</div> 
 			</div>		
 				<div class="col-md-6">
					<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.admininfo" /></strong> <br>
							<small style="float:left; margin-top: 4px;">This info will be used for login. Please make sure you add the exact details and make sure it is not misused.</small>
						</h2>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="first_Name"><spring:message
								code="label.firstname" /><span class="text-danger">*</span> </label>
						<div class="col-md-6">

							<input id="first_Name" name="firstName" class="form-control"
								placeholder="<spring:message code='label.firstname'/>.."
								type="text" value="${company.admin.firstName}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="last_Name"><spring:message
								code="label.lastname" /><span class="text-danger">*</span> </label>
						<div class="col-md-6">

							<input id="last_Name" name="lastName" class="form-control"
								placeholder="<spring:message code="label.lastname"/>.."
								type="text" value="${company.admin.lastName}">
						</div>
					</div>


<!-- 					<div class="form-group"> -->
<%-- 						<label class="col-md-4 control-label" for="email"><spring:message --%>
<%-- 								code="label.emailid" /><span class="text-danger">*</span></label>  --%>
<!-- 						<div class="col-md-6">  -->
<!-- 						<input id="email" name="emailId" class="form-control"  -->
<!-- 						placeholder="test@example.com" type="text" -->
<%-- 						value="${company.admin.emailId}">					 --%>
<!-- 						</div>  -->
<!-- 			</div> -->

					<div class="form-group">
						<label class="col-md-4 control-label" for="mobile"><spring:message
								code="label.mobile" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<input id="mobile" name="mobile" class="form-control"
								placeholder='<spring:message code="label.mobile"></spring:message>'
								type="text" value="${company.admin.mobile}">
						</div>
					</div>
					
				</div>
			</div>
	</div>			
	
			<div style="text-align: center; margin-bottom: 10px">
				<button id="company_submit" class="btn btn-sm btn-primary save"
					type="submit">
					<i class="fa fa-angle-right"></i>
					<spring:message code="button.save" />
				</button>
				<security:authorize ifAnyGranted="<%=Role.SUPER_ADMIN.name() %>">
					<a href="<%=contexturl %>ManageCompany/CompanyList"  class="btn btn-sm btn-primary save" style="text-decoration: none;"><i class="gi gi-remove"></i> <spring:message code="button.cancel"/></a>
				</security:authorize>
				
				<security:authorize ifAnyGranted="<%=Role.ATMA_ADMIN.name() %>">
						 <a href="<%=contexturl %>Index"  class="btn btn-sm btn-primary save" style="text-decoration: none;"><i class="gi gi-remove"></i> <spring:message code="button.cancel"/></a>
<!-- 					 Redirect to view company page. -->
<%-- 					 <a href="<%=contexturl %>ManageCompany/CompanyList/ViewCompany?id=${company.universityId}"  class="btn btn-sm btn-primary" style="text-decoration: none;"><i class="gi gi-remove"></i> <spring:message code="button.cancel"/></a> --%>
				</security:authorize>
			</div>
	
	</form>

</div>
		
		

	<div id="reset-user-settings" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header text-center">
					<h2 class="modal-title">
						<i class="fa fa-pencil"></i> <spring:message code="label.resetpassword"/>
					</h2>
				</div>
				<span id="passwordErrorMsg"></span>
				<div class="modal-body">
					<form action="<%=contexturl %>ManageUniversity/UniversityList/ResetUniversityAdminPassword" method="post"
						class="form-horizontal form-bordered" id="ResetPassword_Form">
						<input name="companyId" value="${company.companyId}" type="hidden">
						<input name="userId" value="${company.admin.userId}" type="hidden">
						<fieldset>
							<div class="form-group">
								<label class="col-md-4 control-label"
									for="user-settings-password"><spring:message code="label.newpassword"/></label>
								<div class="col-md-8">
									<input type="password" id="password" name="password"
										class="form-control"
										placeholder="Please choose a complex one..">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label"
									for="user-settings-repassword"><spring:message code="label.confirmnewpassword"/></label>
								<div class="col-md-8">
									<input type="password" id="user-settings-repassword"
										name="confirmPassword" class="form-control"
										placeholder="..and confirm it!">
								</div>
							</div>
						</fieldset>
						<div class="form-group form-actions">
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal"><spring:message code="label.close"/></button>
								<button id="submit_password" class="btn btn-sm btn-primary save"
									type="submit"><spring:message code="label.savechanges"/></button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>

<script type="text/javascript"
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCheQF_yjKacwuVL4_59EO-3BThy85gbu4&sensor=false&region=GB">
	
</script>


<script type="text/javascript">
	$(document)
			.ready(
					function() {
						$("#manageCompany").addClass("active");
						$(".state_chosen").chosen();
						$(".city_chosen").chosen();
						$(".country_chosen").chosen();
						var cityId1 = ${company.city.cityId};
						var stateId1 = ${company.state.stateId};

						$('select[name="chosenCountryId"]').chosen().change( function() {
							countryId = ($(this).val());
							$.ajax({
								type: "GET",
								async: false,
								url: "<%=contexturl %>state/statebycountryid/"+countryId,
								success: function(data, textStatus, jqXHR){
									var obj = jQuery.parseJSON(data);
									var len=obj.length;
									var myOptions = "<option value></option>";
									var myCityOptions = "<option value></option>";
									for(var i=0; i<len; i++){
									myOptions += '<option value="'+obj[i].stateId+'">'+obj[i].stateName+'</option>';
									}
									$(".state_chosen").html(myOptions).chosen().trigger("chosen:updated");
									$(".city_chosen").html(myCityOptions).chosen().trigger("chosen:updated");
								},
								dataType: 'html'
							});
						});
						
						$('select[name="chosenStateId"]').chosen().change( function() {
							var stateId = ($(this).val());
							$.ajax({
								type: "GET",
								async: false,
								url: "<%=contexturl %>city/citybystateid/"+stateId,
								success: function(data, textStatus, jqXHR){
									var obj = jQuery.parseJSON(data);
									var len=obj.length;
									var myOptions = "<option value></option>";
									for(var i=0; i<len; i++){
									myOptions += '<option value="'+obj[i].cityId+'">'+obj[i].cityName+'</option>';
									}
									$(".city_chosen").html(myOptions).chosen().trigger("chosen:updated");
								},
								dataType: 'html'
							});
						});
						
						changeCountry($("#chosenCountryId").val());
						
						function changeCountry(countryId){
							$.ajax({
								type: "GET",
								async: false,
								url: "<%=contexturl %>state/statebycountryid/"+countryId,
								success: function(data, textStatus, jqXHR){
									var obj = jQuery.parseJSON(data);
									var len=obj.length;
									var myOptions = "<option value></option>";
									for(var i=0; i<len; i++){
										if( stateId1!= obj[i].stateId){
											myOptions += '<option value="'+obj[i].stateId+'">'+obj[i].stateName+'</option>';
										}
										else{
											myOptions += '<option value="'+obj[i].stateId+'" selected="selected">'+obj[i].stateName+'</option>';
										}
									}
									$(".state_chosen").html(myOptions).chosen().trigger("chosen:updated");
									changeState($("#chosenStateId").val());
								},
								dataType: 'html'
							});
						}
						
						function changeState(stateId){
							$.ajax({
								type: "GET",
								async: false,
								url: "<%=contexturl %>city/citybystateid/"+stateId,
								success: function(data, textStatus, jqXHR){
									var obj = jQuery.parseJSON(data);
									var len=obj.length;
									var myOptions = "<option value></option>";
									for(var i=0; i<len; i++){
										if( cityId1!= obj[i].cityId){
											myOptions += '<option value="'+obj[i].cityId+'">'+obj[i].cityName+'</option>';
										}
										else{
											myOptions += '<option value="'+obj[i].cityId+'" selected="selected">'+obj[i].cityName+'</option>';
										}
									}
									$(".city_chosen").html(myOptions).chosen().trigger("chosen:updated");
								},
								dataType: 'html'
							});
						}

						$("#Company_form").validate(
								{	errorClass:"help-block animation-slideDown",
									errorElement:"div",
									errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
									highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
									success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
												rules : {
													companyName : {
														required : !0,maxlength : 75
													},
													firstName : {
														required : !0,maxlength : 75
													},
													lastName : {
														required : !0,maxlength : 75
													},

													mobile : {
														required : !0, maxlength: 15,
														mobile : !0,
													},
													chosenCityId: {
														required : !0
													},
													chosenCountryId: {
														required : !0
													},
													chosenStateId: {
														required : !0
													},

													companyTelephone1 : {
														
														phone : !0
													},
													companyTelephone2 : {
														phone : !0
													},
													companyEmail : {
														required : !0,maxlength : 75,
														email : !0
													},
													companyMobile : {
														
														required: !0,maxlength : 15,
														mobile : !0
													},

													companyAddressLine1 : {
														maxlength : 75
													},
													companyAddressLine2 : {
														maxlength : 75
													},
													companyPincode : {
														required : !0,maxlength : 10,
														digits : !0,
														pincode: !0
													}
												},
												messages : {
													companyName : {
														required :'Please enter a Company Name',
														maxlength :'Company name should not be more than 75 characters'
													},
													firstName : {
														required :'<spring:message code="validation.pleaseenterfirstname"/>',
														maxlength :'<spring:message code="validation.firstname75character"/>'
													},
													lastName : {
														required :'<spring:message code="validation.pleaseenterlastname"/>',
														maxlength :'<spring:message code="validation.lastname75character"/>'
													},

													chosenCityId: {
														required : '<spring:message code="validation.selectcity"/>'
													},
													chosenCountryId: {
														required : '<spring:message code="validation.selectcountry"/>'
													},
													chosenStateId: {
														required : '<spring:message code="validation.selectstate"/>'
													},
													mobile : {
														required :'<spring:message code="validation.pleaseenteramobilenumber"/>',
														mobile :'<spring:message code="validation.mobile"/>',
														maxlength :'<spring:message code="validation.mobile10character"/>'
													},

													companyTelephone1 : {
														
														phone :'<spring:message code="validation.phone"/>'
															
													},
													companyTelephone2 : {

														phone :'<spring:message code="validation.phone"/>'
													},
													companyEmail : {
														required :'<spring:message code="validation.pleaseenteremailid"/>',
														email :'<spring:message code="validation.pleaseenteravalidemailid"/>',
															maxlength :'<spring:message code="validation.email75character"/>'
													},
													companyMobile : {
														
														mobile :'<spring:message code="validation.mobile"/>',
														maxlength :'<spring:message code="validation.mobile10character"/>'
													},
													companyAddressLine1 : {
														
															maxlength :'<spring:message code="validation.addressline1"/>'
													},
													companyAddressLine2 : {
															maxlength :'<spring:message code="validation.addressline1"/>'
													},
													companyPincode : {
														required :'<spring:message code="validation.pleaseenterpincode"/>',
														maxlength :'<spring:message code="validation.pincode10character"/>',
														digits :'<spring:message code="validation.digits"/>'
													}
												},
										});

						$("#ResetPassword_Form")
								.validate(
										{
											errorClass : "help-block animation-slideDown",
											errorElement : "div",
											errorPlacement : function(e, a) {
												a.parents(".form-group > div")
														.append(e)
											},
											highlight : function(e) {
												$(e)
														.closest(".form-group")
														.removeClass(
																"has-success has-error")
														.addClass("has-error")
											},
											success : function(e) {
												e
														.closest(".form-group")
														.removeClass(
																"has-success has-error")
											},
											rules : {
												password : {
													required : !0,
													minlength : 6
												},
												confirmPassword : {
													required : !0,
													equalTo : "#password"
												}
											},
											messages : {
												password : {
													required : "Please enter a password",
													minlength : "Password must be atleast 6 charecter"
												},
												confirmPassword : {
													required : "Please confirm password",
													equalTo : "Please enter the same password as above"
												},
											}
										});

					});
	$("#company_submit").click(function() {
		unsaved=false;
	});
</script>
<script
	src="<%=contexturl%>resources/js/pages/activeTablesDatatables.js"></script>
<script
	src="<%=contexturl%>resources/js/pages/tablesDatatables.js"></script>
<script
	src="<%=contexturl%>resources/js/pages/inactiveTablesDatatables.js"></script>

<!-- <script>$(function(){ TablesDatatables.init(3); });</script> -->

<!-- <script>$(function(){ ActiveTablesDatatables.init(3); });</script> -->
<!-- <script>$(function(){ InActiveTablesDatatables.init(3); });</script> -->

<style>
.chosen-container {
	width: 250px !important;
}
#map-canvas {
	height: 300px
}
</style>