<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->

<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>

<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				<i class="fa fa-map-marker"></i> <spring:message code="heading.checkins"/><br> <small><spring:message code="heading.checkinspecification"/>
					</small>
			</h1>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i> <spring:message code="label.success"/>
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="heading.managerestaurant"/></li>
		<li><a href=""><spring:message code="heading.checkins"/></a></li>
	</ul>
	<div class="block full" id="gridView">
		<div class="block-title">
			<h2>
				<strong><spring:message code="heading.checkins"/></strong>
			</h2>
		</div>
		<div class="table-responsive">
			<table id="active-datatable"
				class="table table-vcenter table-condensed table-bordered">
				<thead>
					<tr>
						<th class="text-center"><spring:message code="label.id" /></th>
<%-- 					    <th class="text-center"><spring:message code="label.profile" /></th> --%>
						<th class="text-center"><spring:message code="label.username" /></th>
<%-- 						<th class="text-center"><spring:message code="label.loginid" /></th> --%>
						<th class="text-center"><spring:message code="label.billamount" /></th>
						<th class="text-center"><spring:message code="label.gpin" /></th>
						<th class="text-center"><spring:message code="label.tableno" /></th>
						<th class="text-center"><spring:message code="label.actions" /></th>
					</tr>
				</thead>
				<tbody id="tbody">
					<c:forEach var="customer" items="${checkedInMember}" varStatus="count">
						<tr>
							<td class="text-center">${count.count}</td>
<%--                             <td class="text-center"><img class="img-circle" alt="avatar" style="width: 100px;height: 95px;" src="${customer.member.user.profileImage.imagePath}"></td> --%>
                            <td><a href="<%=contexturl%>ManageRestaurant/ViewProfile/${customer.member.user.userId}">${customer.member.user.fullName}</a></td>
<%--                             <td>${customer.member.user.loginId}</td> --%>

                            <td><c:if test="${!empty customer.billAmount}">${customer.outlet.baseCurrency.currencySign}  ${customer.billAmount}</c:if> </td>
                             <td>${customer.uniqueIdGenerated}</td>
                             <td>${customer.tableAlloted}</td>
                             <td>
                            	<c:choose>
                            		<c:when test="${customer.customerConfirmed}">
                            			<a  href="#add_bill_popup" data-toggle="modal" class="btn btn-primary save" onclick="addBill(${customer.requestId},'${customer.member.user.fullName}','${customer.member.user.loginId}','${customer.uniqueIdGenerated}',  ${customer.billAmount})"><spring:message code="label.addBill" /></a>
                            			<a class="btn btn-primary save" href="<%=contexturl%>ManageRestaurant/ViewProfile/${customer.member.user.userId}"><spring:message code="label.viewprofile" /></a>
                            			<a class="btn btn-primary save" href="<%=contexturl%>ManageRestaurant/Checkout/${customer.requestId}/${customer.member.user.userId}"><spring:message code="label.checkout" /></a>
                            			<a class="btn btn-primary save" href="#add_tableNo_popup" data-toggle="modal" onclick="addTable(${customer.requestId},<c:choose><c:when test='${! empty customer.tableAlloted}'>${customer.tableAlloted}</c:when>	<c:otherwise>''</c:otherwise></c:choose>)"><spring:message code="label.updatetable" /></a>
                            		</c:when>
                            		<c:otherwise>
                            			<a class="btn btn-primary save" href="#add_tableNo_popup" data-toggle="modal" onclick="addTable(${customer.requestId},'')"><spring:message code="label.confirmrequest" /></a>
                            		</c:otherwise>
                            	</c:choose>
                            </td>
                        </tr>
                   	</c:forEach>

				</tbody>
			</table>
		</div>
	</div>
</div>

<div id="add_bill_popup" class="modal fade" tabindex="-1"
	role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<form action="<%=contexturl%>ManageRestaurant/AddBill" method="post" class="form-horizontal form-bordered" id="addBill_form">
					<div class="form-group">
						<label class="col-md-3 control-label" for="fullName"><spring:message
								code="label.username" /></label>
						<div class="col-md-9">
							<div id="User_Name"></div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label" for="loginId"><spring:message
								code="label.loginid" /></label>
						<div class="col-md-9">
							<div id="login_Id" ></div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label" for="gPin"><spring:message
								code="label.gpin" /></label>
						<div class="col-md-9">
							<div id="g_pin"  ></div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label" for="bill_amount"><spring:message
								code="label.billamount" />  ${outletCurrency.currencySign}</label>
						<div class="col-md-9">
							<input id="bill_amount" name="billAmount" class="form-control"	type="text">
						</div>
					</div>
					<div style="padding: 10px; height: 110px;">
						<div class="col-xs-12 text-right">
							<button type="button" class="btn btn-sm btn-default"
								data-dismiss="modal"><spring:message code="label.no"/></button>
							<button type="submit" class="btn btn-sm btn-primary save"><spring:message code="button.save"/></button>
						</div>
					</div>
					<input type="hidden"  name="requestId" id="requestId">
				</form>
			</div>
		</div>
	</div>
</div>

<div id="add_tableNo_popup" class="modal fade" tabindex="-1"
	role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<form action="<%=contexturl%>ManageRestaurant/ConfirmRequest" method="post" class="form-horizontal form-bordered" id="addBill_form">
					<div class="form-group">
						<label class="col-md-3 control-label" for="bill_amount"><spring:message
								code="label.tableno" /> </label>
						<div class="col-md-9">
							<input id="tableNo" name="tableNo" class="form-control"	type="text">
						</div>
					</div>
					<div style="padding: 10px; height: 110px;">
						<div class="col-xs-12 text-right">
							<button type="button" class="btn btn-sm btn-default"
								data-dismiss="modal"><spring:message code="button.cancel"/></button>
							<button type="submit" class="btn btn-sm btn-primary save"><spring:message code="button.save"/></button>
						</div>
					</div>
					<input type="hidden"  name="requestId" id="table_requestId">
				</form>
			</div>
		</div>
	</div>
</div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>

<script src="<%=contexturl %>resources/js/pages/activeTablesDatatables.js"></script>
<script>$(function(){ ActiveTablesDatatables.init(4); });</script>

<script type="text/javascript">
	$(document).ready(function() {

		$("#addBill_form").validate(
				{	errorClass:"help-block animation-slideDown",
					errorElement:"div",
					errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
					highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
					success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
					rules:{ billAmount:{required:!0,digits : !0}
					},
					messages:{areaName:{required:'<spring:message code="validation.billamount"/>',
						digits : '<spring:message code="validation.validamount"/>'}
					},
				
				});
	});
	
	function addBill(requestId,fullName,loginId,gpin,billAmount){
		$("#requestId").val(requestId);
		$("#User_Name").html(fullName);
		$("#login_Id").html(loginId);
		$("#g_pin").html(gpin);
		$("#bill_amount").val(billAmount);
		
		
	}
	
	function addTable(requestId,tableNo){
		$("#table_requestId").val(requestId);
		$("#tableNo").val(tableNo);
	}
</script>
<%@include file="../../inc/template_end.jsp"%>