<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				Last Added Organisation
			</h1>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i>
						<spring:message code="label.success" />
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>
			<div class="alert alert-danger alert-dismissable"
				style="display: none" id="error">
				<button type="button" class="close" data-dismiss="alert"
					aria-hidden="true">x</button>
				<h4>
					<i class="fa fa-times-circle"> </i> <spring:message code="label.error"/>
				</h4>
				<span id="errorMsg"></span>
			</div>
		</div>
	</div>
	
	<div class="block full  gridView">
		<a href="<%=contexturl %>AtmaAdmin" id="atmaDashboardBack" class="btn btn-sm btn-primary save">Back to dashboard</a>
		<div class="table-responsive">
			<table id="active-datatable"
				class="table table-vcenter table-condensed table-bordered">
				<thead>
					<tr>
						<th class="text-center">Id</th>
						<th class="text-center">Company Name</th>
						<th class="text-center">Register By</th>
						<th class="text-center">Register Date</th>
						<th class="text-center">Profile Type</th>
						<th class="text-center">Status</th>
					</tr>
				</thead>
				<tbody id="tbody">
					 <c:forEach var="org" items="${results}" varStatus="count">
						<tr id="${org.id}">
							<td class="text-center">${org.id}</td>
							<td class="text-center">${org.cName}</td>
							<td class="text-center">${org.byFirstName} ${byLastName}</td>
							<td class="text-center">${org.date}</td>
							<td class="text-center">${org.type}</td>
							<td class="text-center"> <c:if test="${org.flag eq 'true'}"> New</c:if><c:if test="${org.flag eq 'false'}"> Old</c:if></td>
							<script>
								var flagVal=${org.flag};
								if(flagVal == true){
									$('#'+${org.id}).css("background-color", "#f48484");
								}
							</script>
							
							<%-- <td class="text-center">
								<div class="btn-group">
									<p class="form-control-static">
										<a
											href="<%=contexturl%>DownloadXlSheet/${org.companyId}"><spring:message
												code="label.download" /> </a>
									</p>
								</div>
							</td> --%>
						</tr>
					</c:forEach> 
				</tbody>
			</table>
		</div>
	</div>


</div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>

<script
	src="<%=contexturl%>resources/js/pages/activeTablesDatatables.js"></script>
<script
	src="<%=contexturl%>resources/js/pages/inactiveTablesDatatables.js"></script>
<script>$(function(){ ActiveTablesDatatables.init(3); });</script>
<script>$(function(){ InActiveTablesDatatables.init(3); });</script>

<script>
$(document).ready(
		function() {

			$.ajax({
						type: "GET",
						async: false,
						url: "<%=contexturl %>inactiveFlagLastOrganisation",
						success: function(data, textStatus, jqXHR){
						/*  	var obj = data; 
							var re1=[];
							var re2=[];
							re1=data.one;
							re2=data.two; */
							
							
							
						},
					});
			
		});

</script>

<%@include file="../../inc/template_end.jsp"%>