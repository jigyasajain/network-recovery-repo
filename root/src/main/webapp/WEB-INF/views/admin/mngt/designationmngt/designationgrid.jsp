<%@page import="com.astrika.common.model.location.CityMaster"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>

<style>
.chosen-container {
	width: 250px !important;
}

#map-canvas {
	height: 300px;
}
</style>
<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				<i class="fa fa-map-marker"></i>
				<spring:message code="heading.designationmaster" />
				<br> <small><spring:message
						code="heading.specifylistofdesignation" /> </small>
			</h1>
			<span id="errorMsg"></span>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" id="success_close" class="close"
						data-dismiss="alert" aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i>
						<spring:message code="label.success" />
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i>
						<spring:message code="label.error" />
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="menu.managedesignation" /></li>
		<li><a href="#"><spring:message code="label.designationname" /></a></li>
	</ul>
	<div class="block full gridView">
		<div class="block-title">
			<h2>
				<strong><spring:message code="heading.activedesignation" /></strong>
			</h2>
		</div>
		<a class="btn btn-sm btn-primary"
			href="<%=contexturl%>ManageDesignation/DesignationList/AddDesignation">
			<i class="fa fa-angle-right"></i> <spring:message
				code="label.adddesignation" />
		</a>
		<div class="table-responsive">
			<table id="active-datatable"
				class="table table-vcenter table-condensed table-bordered">
				<thead>
					<tr>
						<th class="text-center"><spring:message code="label.id" /></th>
						<th class="text-center"><spring:message
								code="label.designationname" /></th>
						<th class="text-center"><spring:message code="label.actions" /></th>
					</tr>
				</thead>
				<tbody id="tbody">
					<c:forEach var="currentDesg" items="${designationList}"
						varStatus="count">
						<tr>
							<td class="text-center">${count.count}</td>
							<td class="text-left"><c:out
									value="${currentDesg.designationName}" /></td>
							<td class="text-center">
								<div class="btn-group">
									<a
										href="<%=contexturl %>ManageDesignation/Designation/EditDesignation?id=${currentDesg.designationId}"
										data-toggle="tooltip" title="Edit"
										class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
									<a href="#delete_department_popup" data-toggle="modal"
										onclick="deleteDepartment(${currentDesg.designationId})"
										title="Delete" class="btn btn-xs btn-danger"><i
										class="fa fa-times"></i> </a>
								</div>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	<div class="block full gridView">
		<div class="block-title">
			<h2>
				<strong><spring:message code="heading.inactivedesignation" /></strong>
			</h2>
		</div>
		<div class="table-responsive">
			<table id="inactive-datatable"
				class="table table-vcenter table-condensed table-bordered">
				<thead>
					<tr>
						<th class="text-center"><spring:message code="label.id" /></th>
						<th class="text-center"><spring:message
								code="label.designationname" /></th>
						<th class="text-center"><spring:message code="label.actions" /></th>
					</tr>
				</thead>
				<tbody id="tablebody">
					<c:forEach var="currentDesg" items="${inActiveDesignationList}"
						varStatus="count">
						<tr>
							<td class="text-center">${count.count}</td>
							<td class="text-left"><c:out
									value="${currentDesg.designationName}" /></td>
							<td class="text-center">
								<div class="btn-group">
									<a href="#restore_designation_popup" data-toggle="modal"
										onclick="restoreDepartment(${currentDesg.designationId})"
										title="Restore" class="btn btn-xs btn-danger"><i
										class="fa fa-times"></i> </a>
								</div>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>

	</div>

	<div id="delete_department_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="DeleteDesignation" method="post"
						class="form-horizontal form-bordered" id="delete_designation_form">
						<div style="padding: 10px; height: 110px;">
							<label><spring:message
									code="validation.doyouwanttodeletethisdesignation" /></label>
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal">
									<spring:message code="label.no" />
								</button>
								<div id="delete_department" class="btn btn-sm btn-primary">
									<spring:message code="label.yes" />
								</div>
							</div>
						</div>
						<input type="hidden" name="designationId" id="designationId">
					</form>
				</div>
			</div>
		</div>
	</div>

	<div id="restore_designation_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="RestoreDesignation" method="post"
						class="form-horizontal form-bordered" id="restore_designation_form">
						<div style="padding: 10px; height: 110px;">
							<label><spring:message
									code="validation.doyouwanttorestorethisdesignation" /></label>
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal">
									<spring:message code="label.no" />
								</button>
								<div id="restore_department" class="btn btn-sm btn-primary">
									<spring:message code="label.yes" />
								</div>
							</div>
						</div>
						<input type="hidden" name="designation_id" id="designation_id">
					</form>
				</div>
			</div>
		</div>
	</div>

</div>
<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>


<script
	src="<%=contexturl%>resources/js/pages/activeTablesDatatables.js"></script>
<script
	src="<%=contexturl%>resources/js/pages/inactiveTablesDatatables.js"></script>
<script>$(function(){ ActiveTablesDatatables.init(2); });</script>
<script>$(function(){ InActiveTablesDatatables.init(2); });</script>
<script type="text/javascript">

	var selectedId = 0;
	var restoreId = 0;
		
	$(document).ready(function() {
          		
		  $("#restore_department").click(function(){
				
				$("#designation_id").val(restoreId);
				$("#restore_designation_form").submit();
			});
		
		$("#delete_department").click(function(){
			$("#designationId").val(selectedId);
			$("#delete_designation_form").submit();
		});
		
	});
	
	function deleteDepartment(id){
		selectedId = id;
	}
	function restoreDepartment(id){
		restoreId = id;
	}
	

</script>
<%@include file="../../inc/template_end.jsp"%>
