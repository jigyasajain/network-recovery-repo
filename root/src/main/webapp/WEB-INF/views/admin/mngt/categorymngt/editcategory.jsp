<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>
<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
<!-- 				<i class="fa fa-map-marker"></i> -->
				<spring:message code="heading.category" />
				<br> <small><spring:message
						code="heading.categorydetails" /></small>
			</h1>
			<span id="errorMsg"></span>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i> <spring:message code="label.success"/>
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>

			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">�</button>
					<h4>
						<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="heading.category" /></li>
	</ul>
			
			<div class="block full" >
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.categoryinfo" /></strong>
						</h2>
					</div>
					<form action="<%=contexturl %>ManageCategory/CategoryList/UpdateCategory" method="post" class="form-horizontal" id="User_form" enctype="multipart/form-data" >
							<input name="categoryId" value="${category.categoryId}"
								type="hidden">
					<div class="form-group">
						<label class="col-md-4 control-label" for="category_Name"><spring:message
								code="label.categoryname" /><span class="text-danger">*</span> </label>
						<div class="col-md-6">

							<input id="category_Name" name="categoryName" class="form-control"
								placeholder="<spring:message code='label.categoryname'/>.."
								type="text" value="${category.categoryName}">
						</div>
					</div>


					
					<div class="form-group">
							<label class="col-md-4 control-label" for="categoryDescription"><spring:message
								code="label.categoryDescription" /></label>
							<div class="col-md-6">
								<textarea id="categoryDescription" name="categoryDescription" class="form-control"
									   placeholder="Description.."  >${category.categoryDescription}</textarea>
							</div>
					</div>
					
					<div class="form-group">
							<label class="col-md-4 control-label" for="categorySummary"><spring:message
								code="label.categorySummary" /><span class="text-danger">*</span></label>
							<div class="col-md-6">
								<textarea id="categorySummary" name="categorySummary" class="form-control"
									   placeholder="Summary.."  >${category.categorySummary}</textarea>
							</div>
					</div>
				
				
					<div class="form-group" id="moduleDiv">
						<label class="col-md-4 control-label" for="chosenModuleId"><spring:message
								code="label.modulename" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<select class="module_chosen" style="width: 200px;"
								id="chosenModuleId"
								data-placeholder="<spring:message code='label.choosemodule' />"
								name="chosenModuleId">
								<option value=""></option>
								<c:forEach items="${moduleList}" var="module">
											<option value="${module.moduleId}" 
											<c:if test="${module.moduleId eq category.module.moduleId}">selected="selected"</c:if>>${module.moduleName}</option>
								</c:forEach>
							</select>
						</div>
					</div>

			<div style="text-align: center; margin-bottom: 10px">
				<button id="user_submit" class="btn btn-sm btn-primary save"
					type="submit">
					<i class="fa fa-angle-right"></i>
					<spring:message code="button.save" />
				</button>
				<a href="<%=contexturl %>ManageCategory/CategoryList/" class="btn btn-sm btn-primary save"
					style="text-decoration: none;"><i class="gi gi-remove"></i> <spring:message
						code="button.cancel" /></a>
			</div>

	</form>
</div>


</div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>

<script src="../resources/js/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
	$("#manageCategory").addClass("active");

	$(".module_chosen").data("placeholder","Select Module From...").chosen();
$("#User_form").validate(
		{	errorClass:"help-block animation-slideDown",
			errorElement:"div",
			errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
			highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
			success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
			rules:{ categoryName : {required : !0,maxlength : 75},
				categoryDescription : {maxlength : 500},
				categorySummary : {
					required : !0,
					maxlength : 255
				},
				chosenModuleId: {
					required : !0
				}
			},
			messages:{
				categoryName : {
					required :'<spring:message code="validation.pleaseentercategoryName"/>',
					maxlength :'<spring:message code="validation.categoryName75character"/>'
				},
				categoryDescription : {
					maxlength :'<spring:message code="validation.description500character"/>'
				},
				categorySummary : {
					required : '<spring:message code="validation.pleaseentercategorySummary"/>',
					maxlength : '<spring:message code="validation.categorySummary255character"/>'
				},

				chosenModuleId: {
					required : '<spring:message code="validation.selectmodule"/>'
				}
			},
		});

	 $("#user_submit").click(function(){
		 $("#User_form").submit();
	 });

});
</script>




<%@include file="../../inc/template_end.jsp"%>