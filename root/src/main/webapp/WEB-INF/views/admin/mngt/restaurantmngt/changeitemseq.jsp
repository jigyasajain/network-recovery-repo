<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@include file="../../inc/config.jsp"%>

<form id="changeMenuItemSequnceForm" action="<%=contexturl %>ManageRestaurant/RestaurantList/SaveMenuItemSequence">
	<div id="savemenuitemsequence" class="btn btn-sm btn-primary"
		style="margin-bottom: 35px;  float: left;" type="submit">
		<spring:message code="label.savesequense" />
	</div>
	<div class="btn btn-sm btn-primary"><a href="MenuList?id=${outletId}&menuType=ItemMenu"><spring:message code="label.goBack" /></a></div>
	<input type="hidden" name="id" value="${outletId}" />
	<input type="hidden" name="categoryId" value="${categoryId}" />
	<c:choose>
		<c:when test="${!empty menuItems}">
			<ul class="sortable">
				<c:forEach items="${menuItems}" var="menuItem">
					<li class="menu_item_sequence ">
						${menuItem.name } <input type="hidden"
						value="${menuItem.itemId}" class="menuItemIds">
					</li>

				</c:forEach>
			</ul>
		</c:when>
	</c:choose>
	<input type="hidden" id="menuItemSequence" name="itemIds" />
</form>

<script type="text/javascript">
$(".sortable").sortable();
$(".sortable").disableSelection();
$("#savemenuitemsequence").click(function() {
	var exp = "";
	$(".menuItemIds").each(function(index) {
		exp = exp + "," + $(this).val();
	});
	$("#menuItemSequence").val(exp);
	$("#changeMenuItemSequnceForm").submit();
});

</script>