<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>


<%-- <%@include file="../../inc/template_scripts.jsp"%> --%>

<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				<i class="fa fa-map-marker"></i>
				<spring:message code="heading.institutionmaster" />
				<br> <small><spring:message
						code="heading.editinstitution" /></small>
			</h1>
			<span id="errorMsg"></span>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i> <spring:message code="label.success"/>
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>

			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">�</button>
					<h4>
						<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="menu.institution" /></li>
	</ul>
	<form action="<%=contexturl %>ManageInstitution/InstitutionList/UpdateInstitution" method="post" class="form-horizontal "
		id="Company_form" enctype="multipart/form-data">
		<div class="row">
			<input name="universityId" value="${university.universityId}" type="hidden">
			<input name="userId" value="${university.admin.userId}" type="hidden">
			<div class="col-md-6">
				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.institutioninfo" /></strong>
							<small style="float:left; margin-top: 4px;">This info is non editable and for view only purpose.</small>
						</h2>
					</div>
						
					<div class="form-group">
                        <label class="col-md-4 control-label"><spring:message code="label.institutionname"/></label>
                        <div class="col-md-8">
                            <p class="form-control-static">${university.universityName}</p>
                             <input name="universityName" value="${university.universityName}" type="hidden">
                        </div>
                    </div>

<!-- 					<div class="form-group"> -->
<%-- 						<label class="col-md-4 control-label" for="example-daterange1"><spring:message --%>
<%-- 								code="label.selectdaterange" /> </label> --%>
<!-- 						<div class="col-md-8"> -->

<!-- 							<label class="col-md-4 control-label" for="example-daterange1"> -->
<%-- 								${ university.agreementStart} </label> <label --%>
<!-- 								class="col-md-4 control-label" for="example-daterange1"> -->
<%-- 								${ university.agreementEnd} </label> --%>
<!-- 						</div> -->
<!-- 					</div> -->
					
					<div class="form-group">
						<label class="col-md-4 control-label" for="example-daterange1"><spring:message code="label.agreementdate"/>
							</label>
						<div class="col-md-8">
							<div class="input-group input-daterange"
								data-date-format="mm/dd/yyyy">
								<input type="text" id="universityAgreementStart"
									name="universityAgreementStart" class="form-control text-center"
									placeholder="From" value="${university.agreementStart}"> <span class="input-group-addon"><i
									class="fa fa-angle-right"></i></span> <input type="text"
									id="universityAgreementEnd" name="universityAgreementEnd"
									class="form-control text-center" placeholder="To" value="${university.agreementEnd}">
							</div>
						</div>
				</div>
				
						<c:choose>
							<c:when test="${university.logo!=null}">
								<div class="form-group">
									<label class="col-md-4 control-label" for="example-daterange1"><spring:message code="label.logo"/>
										</label>
									<div class="col-md-6">
										<img src="<%=contexturl %>${university.logo.imagePath}" width="150px"/>
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-md-4 control-label" for="example-daterange1"><spring:message code="label.changelogo"/>
										</label>
									<div class="col-md-6">
										<input type="file" name="logo" accept="image/*"/>
									</div>
								</div>
							</c:when>
							<c:otherwise >
								<div class="form-group">
									<label class="col-md-4 control-label" for="example-daterange1"><spring:message code="label.logo"/>
										</label>
									<div class="col-md-6">
										<input type="file" name="logo" accept="image/*"/>
									</div>
								</div>	
							</c:otherwise>
						</c:choose>
						
						<c:choose>
							<c:when test="${university.previewImaage!=null}">
								<div class="form-group">
									<label class="col-md-4 control-label" for="example-daterange1"><spring:message code="label.previewimage"/>
										</label>
									<div class="col-md-6">
										<img src="<%=contexturl %>${university.previewImaage.imagePath}" width="150px"/>
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-md-4 control-label" for="example-daterange1"><spring:message code="label.previewimage"/>
										</label>
									<div class="col-md-6">
										<input type="file" name="previewImage" accept="image/*"/>
									</div>
								</div>
							</c:when>
							<c:otherwise >
								<div class="form-group">
									<label class="col-md-4 control-label" for="example-daterange1"><spring:message code="label.previewimage"/>
										</label>
									<div class="col-md-6">
										<input type="file" name="previewImage" accept="image/*"/>
									</div>
								</div>	
							</c:otherwise>
						</c:choose>
				</div>
				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.contactinfo" /></strong>
							<small style="float:left; margin-top: 4px;">All your SkillMe related communication will be done on these details. * These details can be same as your admin details.</small>
						</h2>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="telephone1"><spring:message
								code="label.telephone1" /></label>
						<div class="col-md-6">

							<input id="universityTelephone1" name="universityTelephone1"
								class="form-control"
								placeholder="<spring:message code='label.telephone1'/>.."
								type="text" value="${university.universityTelephone1}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="telephone2"><spring:message
								code="label.telephone2" /></label>
						<div class="col-md-6">

							<input id="universityTelephone2" name="universityTelephone2"
								class="form-control"
								placeholder="<spring:message code='label.telephone2'/>.."
								type="text" value="${university.universityTelephone2}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="rest_email"><spring:message
								code="label.institutionEmail" /><span class="text-danger">*</span></label>
						<div class="col-md-6">

							<input id="university_email" name="universityEmail"
								class="form-control"
								placeholder="<spring:message code="label.institutionEmail"/>.."
								type="text" value="${university.universityEmail}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="mobile1"><spring:message
								code="label.mobile1" /></label>
						<div class="col-md-6">

							<input id="universityMobile1" name="universityMobile1"
								class="form-control"
								placeholder="<spring:message code="label.mobile1"/>.."
								type="text" value="${university.universityMobile1}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="mobile2"><spring:message
								code="label.mobile2" /></label>
						<div class="col-md-6">

							<input id="universityMobile2" name="universityMobile2"
								class="form-control"
								placeholder="<spring:message code="label.mobile2"/>.."
								type="text" value="${university.universityMobile2}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="addressline1"><spring:message
								code="label.addressline1" />
						</label>
						<div class="col-md-6">

							<input id="universityAddressLine1" name="universityAddressLine1"
								class="form-control"
								placeholder="<spring:message code="label.addressline1"/>.."
								type="text" value="${university.universityAddressLine1}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="addressline2"><spring:message
								code="label.addressline2" /></label>
						<div class="col-md-6">

							<input id="universityAddressLine2" name="universityAddressLine2"
								class="form-control"
								placeholder="<spring:message code="label.addressline2"/>.."
								type="text" value="${university.universityAddressLine2}">
						</div>
					</div>
					
					<div class="form-group" id="countryDiv">
						<label class="col-md-4 control-label" for="country"><spring:message
								code="label.countryname" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<select class="country_chosen" style="width: 200px;"
								id="chosenCountryId"
								data-placeholder="<spring:message code='label.choosecountry' />"
								name="chosenCountryId">
								<c:forEach items="${countryList}" var="country">
									<option value="${country.countryId}"
										<c:if test="${country.countryId eq university.country.countryId}">selected="selected"</c:if>>${country.countryName}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					
					<div class="form-group" id="stateDiv">
						<label class="col-md-4 control-label" for="area"><spring:message
								code="label.statename" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<select class="state_chosen" style="width: 200px;"
								id="chosenStateId"
								data-placeholder="<spring:message code='label.choosestate' />"
								name="chosenStateId">
							</select>
						</div>
					</div>
					
					
					<div class="form-group" id="cityDiv">
						<label class="col-md-4 control-label" for="city"><spring:message
								code="label.cityname" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<select class="city_chosen" style="width: 200px;"
								id="chosenCityId"
								data-placeholder="<spring:message code='label.choosecity' />"
								name="chosenCityId">
							</select>
						</div>
					</div>

					
					<div class="form-group">
						<label class="col-md-4 control-label" for="universityPincode"><spring:message
								code="label.pincode" /><span class="text-danger">*</span></label>
						<div class="col-md-6">

							<input id="universityPincode" name="universityPincode"
								class="form-control"
								placeholder="<spring:message code="label.pincode"/>.."
								type="text" value="${university.universityPincode}">
						</div>
					</div>
					
					<div class="form-group">
							<label class="col-md-4 control-label" for="latitude"><spring:message
 									code="label.latitude" /></label> 
							<div class="col-md-6">

 								<input id="latitude" name="latitude" class="form-control"  
									value="${university.latitude}"
									placeholder="<spring:message code="label.latitude"/>.."
									type="text" >
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="longitude"><spring:message
 									code="label.longitude" /></label> 
							<div class="col-md-6">

								<input id="longitude" name="longitude" class="form-control" 
									value="${university.longitude}"
									placeholder="<spring:message code="label.longitude"/>.."
									type="text" >
							</div>
						</div>
				</div>
				
				<div class="block">
						<div class="block-title">
							<h2>
								<strong>Map</strong>
							</h2>
							Please Drag Pointer to Locate Institution
						</div>

						<div id="map-canvas"></div>
				</div>
				
				<div class="block">
						<div class="block-title">
							<h2>
								<strong><spring:message code="heading.socialinfo" /></strong>
								<small style="float:left; margin-top: 4px;">Please give your social site details here</small>
							</h2>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="facebook_link"><spring:message
									code="label.facebook" /> </label>
							<div class="col-md-6">
								<input id="facebook_link" name="facebookLink"
									class="form-control" value="${university.facebookUrl}"
									placeholder="<spring:message code='label.facebook'/>.."
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="twitter_link"><spring:message
									code="label.twitter" /> </label>
							<div class="col-md-6">
								<input id="twitter_link" name="twitterLink" class="form-control"
									value="${university.twitterUrl}"
									placeholder="<spring:message code='label.twitter'/>.."
									type="text">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label" for="youtube_link"><spring:message
									code="label.youtube" /> </label>
							<div class="col-md-6">
								<input id="youtube_link" name="youtubeLink"
									class="form-control" value="${university.youtubeUrl}"
									placeholder="<spring:message code='label.youtube'/>.."
									type="text">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label" for="linkedIn_link"><spring:message
									code="label.linkedin" /> </label>
							<div class="col-md-6">
								<input id="linkedIn_link" name="linkedinLink"
									class="form-control" value="${university.linkedInUrl}"
									placeholder="<spring:message code='label.linkedin'/>.."
									type="text">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label" for="website_link"><spring:message
									code="label.website" /> </label>
							<div class="col-md-6">
								<input id="website_link" name="websiteLink"
									class="form-control" value="${university.website}"
									placeholder="<spring:message code='label.website'/>.."
									type="text">
							</div>
						</div>

					</div>
					<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.admininfo" /></strong>
							<small style="float:left; margin-top: 4px;">This info will be used for login. Please make sure you add the exact details and make sure it is not misused.</small>
						</h2>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="first_Name"><spring:message
								code="label.firstname" /><span class="text-danger">*</span> </label>
						<div class="col-md-6">

							<input id="first_Name" name="firstName" class="form-control"
								placeholder="<spring:message code='label.firstname'/>.."
								type="text" value="${university.admin.firstName}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="last_Name"><spring:message
								code="label.lastname" /><span class="text-danger">*</span> </label>
						<div class="col-md-6">

							<input id="last_Name" name="lastName" class="form-control"
								placeholder="<spring:message code="label.lastname"/>.."
								type="text" value="${university.admin.lastName}">
						</div>
					</div>


					<div class="form-group">
						<label class="col-md-4 control-label" for="email"><spring:message
								code="label.emailid" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<input id="email" name="emailId" class="form-control"
								placeholder="test@example.com" type="text"
								value="${university.admin.emailId}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="mobile"><spring:message
								code="label.mobile" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<input id="mobile" name="mobile" class="form-control"
								placeholder='<spring:message code="label.mobile"></spring:message>'
								type="text" value="${university.admin.mobile}">
						</div>
					</div>
					<div class="form-group">
                        <label class="col-md-4 control-label"><spring:message code="label.userlogin"/></label>
                        <div class="col-md-8">
                            <p class="form-control-static">${university.admin.loginId}</p>
                        </div>
                    </div>
					<div class="form-group">
						<a href="#reset-user-settings" data-toggle="modal"
							class="col-md-4 control-label"><spring:message code="label.resetpassword"/></a>
					</div>
				</div>
				
				<div class="block">
			<div style="text-align: center; margin-bottom: 10px">
				<button id="company_submit" class="btn btn-sm btn-primary save"
					type="submit">
					<i class="fa fa-angle-right"></i>
					<spring:message code="button.save" />
				</button>
				<security:authorize ifAnyGranted="<%=Role.SKILLME_ADMIN.name() %>">
					<a href="<%=contexturl %>ManageInstitution/InstitutionList"  class="btn btn-sm btn-primary" style="text-decoration: none;"><i class="gi gi-remove"></i> <spring:message code="button.cancel"/></a>
				</security:authorize>
				
				<security:authorize ifAnyGranted="<%=Role.UNIVERSITY_ADMIN.name() %>">
						 <a href="<%=contexturl %>Index"  class="btn btn-sm btn-primary" style="text-decoration: none;"><i class="gi gi-remove"></i> <spring:message code="button.cancel"/></a>
<!-- 					 Redirect to view company page. -->
<%-- 					 <a href="<%=contexturl %>ManageCompany/CompanyList/ViewCompany?id=${company.universityId}"  class="btn btn-sm btn-primary" style="text-decoration: none;"><i class="gi gi-remove"></i> <spring:message code="button.cancel"/></a> --%>
				</security:authorize>
			</div>
		</div>
	</form>

			</div>
			<div class="col-md-6">
				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.instructorlist" /></strong>
							
						</h2>
					</div>
					<div class="block full  gridView">
		<div class="block-title">
			<h2>
				<strong><spring:message code="heading.activeinstructor" /></strong>
			</h2>
		</div>
		
		<div class="table-responsive">
			<table id="active-datatable"
				class="table table-vcenter table-condensed table-bordered">
				<thead>
					<tr>
						<th class="text-center"><spring:message code="label.id" /></th>
						<th class="text-center"><spring:message code="label.name" /></th>
						<th class="text-center"><spring:message code="label.email" /></th>
						<th class="text-center"><spring:message code="label.department" /></th>
					</tr>
				</thead>
				<tbody id="tbody">
					<c:forEach var="currentInstructor" items="${activeUsers}"
						varStatus="count">

						<tr>
							<td class="text-center">${count.count}</td>
							<td class="text-left"><c:out
									value="${currentInstructor.user.fullName}" /></td>
							<td class="text-left"><c:out
									value="${currentInstructor.user.emailId}" /></td>
							<td class="text-left"><c:out
 									value="${currentInstructor.department.departmentName}" /></td>
						</tr>
					</c:forEach>

				</tbody>
			</table>
		</div>
	</div>
	<div class="block full  gridView">
		<div class="block-title">
			<h2>
				<strong><spring:message code="heading.inactiveinstructor" /></strong>
			</h2>
		</div>
		<div class="table-responsive">
			<table id="inactive-datatable"
				class="table table-vcenter table-condensed table-bordered ">
				<thead>
					<tr>
						<th class="text-center"><spring:message code="label.id" /></th>
						<th class="text-center"><spring:message code="label.name" /></th>
						<th class="text-center"><spring:message code="label.email" /></th>
						<th class="text-center"><spring:message code="label.department" /></th>
						
					</tr>
				</thead>
				<tbody id="tablebody">
					<c:forEach var="currentInstructor" items="${inactiveUsers}"
						varStatus="count">
						<tr>
							<td class="text-center">${count.count}</td>
							<td class="text-left"><c:out
									value="${currentInstructor.user.fullName}" /></td>
							<td class="text-left"><c:out
									value="${currentInstructor.user.emailId}" /></td>
							<td class="text-left"><c:out
 									value="${currentInstructor.department.departmentName}" /></td> 
							
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</div>
</div>
			
			
			<div class="col-md-6">
				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.courselist" /></strong>
							
						</h2>
					</div>
					<div class="block full  gridView">
					<div class="block-title">
			<h2>
				<strong><spring:message code="heading.activecourse" /></strong>
			</h2>
		</div>
		
		<div class="table-responsive">
			<table 
				class="table table-vcenter table-condensed table-bordered datatable">
				<thead>
					<tr>
						<th class="text-center"><spring:message code="label.id" /></th>
						<th class="text-center"><spring:message code="label.coursename" /></th>
						<th class="text-center"><spring:message code="label.status" /></th>
						<th class="text-center"><spring:message code="label.createdby" /></th>
					</tr>
				</thead>
				<tbody id="tbody">
					<c:forEach var="currentCourse" items="${activeCourse}"
						varStatus="count">

						<tr>
							<td class="text-center">${count.count}</td>
							<td class="text-left"><c:out
									value="${currentCourse.courseName}" /></td>
							<td class="text-left"><c:out
									value="${currentCourse.status}" /></td>
							<td class="text-left"><c:out
									value="${currentCourse.createdBy.firstName} ${currentCourse.createdBy.lastName}" /></td>
							
						</tr>
					</c:forEach>

				</tbody>
			</table>
		</div>
					</div>
					<div class="block full  gridView">
					
					<div class="block-title">
			<h2>
				<strong><spring:message code="heading.inactivecourse" /></strong> 
			</h2>
		</div>
		<div class="table-responsive">
			<table 
				class="table table-vcenter table-condensed table-bordered datatable">
				<thead>
					<tr>
						<th class="text-center"><spring:message code="label.id" /></th>
						<th class="text-center"><spring:message code="label.coursename" /></th>
						<th class="text-center"><spring:message code="label.status" /></th>						
						<th class="text-center"><spring:message code="label.createdby" /></th>
					</tr>
				</thead>
				<tbody id="tablebody">
					<c:forEach var="currentCourse" items="${inActiveCourse}"
						varStatus="count">
						<tr>
							<td class="text-center">${count.count}</td>
							<td class="text-left"><c:out
									value="${currentCourse.courseName}" /></td>
							<td class="text-left"><c:out
									value="${currentCourse.status}" /></td>
							<td class="text-left"><c:out
									value="${currentCourse.createdBy.firstName} ${currentCourse.createdBy.lastName}" /></td>
							
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
					</div>
				</div>

			</div>
		</div>
		

	<div id="reset-user-settings" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header text-center">
					<h2 class="modal-title">
						<i class="fa fa-pencil"></i> <spring:message code="label.resetpassword"/>
					</h2>
				</div>
				<span id="passwordErrorMsg"></span>
				<div class="modal-body">
					<form action="<%=contexturl %>ManageUniversity/UniversityList/ResetUniversityAdminPassword" method="post"
						class="form-horizontal form-bordered" id="ResetPassword_Form">
						<input name="universityId" value="${university.universityId}" type="hidden">
						<input name="userId" value="${university.admin.userId}" type="hidden">
						<fieldset>
							<div class="form-group">
								<label class="col-md-4 control-label"
									for="user-settings-password"><spring:message code="label.newpassword"/></label>
								<div class="col-md-8">
									<input type="password" id="password" name="password"
										class="form-control"
										placeholder="Please choose a complex one..">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label"
									for="user-settings-repassword"><spring:message code="label.confirmnewpassword"/></label>
								<div class="col-md-8">
									<input type="password" id="user-settings-repassword"
										name="confirmPassword" class="form-control"
										placeholder="..and confirm it!">
								</div>
							</div>
						</fieldset>
						<div class="form-group form-actions">
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal"><spring:message code="label.close"/></button>
								<button id="submit_password" class="btn btn-sm btn-primary save"
									type="submit"><spring:message code="label.savechanges"/></button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>

<script type="text/javascript"
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCheQF_yjKacwuVL4_59EO-3BThy85gbu4&sensor=false&region=GB">
	
</script>
<script type="text/javascript">
	function initialize() {
		var mapOptions = {
			center : new google.maps.LatLng($('#latitude').val(), $(
					'#longitude').val()),
			zoom : 13
		};
		var map = new google.maps.Map(document.getElementById("map-canvas"),
				mapOptions);

		var marker = new google.maps.Marker({
			position : map.getCenter(),
			map : map,
			title : 'Click to zoom',
			draggable : true
		});

		google.maps.event.addListener(map, 'center_changed', function() {
			// 3 seconds after the center of the map has changed, pan back to the
			// marker.

			window.setTimeout(function() {
				$('#latitude').val(marker.getPosition().k);
				$('#longitude').val(marker.getPosition().B);
				map.panTo(marker.getPosition());
			}, 3000);
		});

		google.maps.event.addListener(marker, 'click', function() {
			map.setZoom(13);
			map.setCenter(marker.getPosition());
		});
	}

	google.maps.event.addDomListener(window, 'load', initialize);
</script>

<script type="text/javascript">
	$(document)
			.ready(
					function() {
						$(".state_chosen").chosen();
						$(".city_chosen").chosen();
						$(".country_chosen").chosen();
						var cityId1 = ${university.city.cityId};
						var stateId1 = ${university.state.stateId};

						$('select[name="chosenCountryId"]').chosen().change( function() {
							countryId = ($(this).val());
							$.ajax({
								type: "GET",
								async: false,
								url: "<%=contexturl %>state/statebycountryid/"+countryId,
								success: function(data, textStatus, jqXHR){
									var obj = jQuery.parseJSON(data);
									var len=obj.length;
									var myOptions = "<option value></option>";
									var myCityOptions = "<option value></option>";
									for(var i=0; i<len; i++){
									myOptions += '<option value="'+obj[i].stateId+'">'+obj[i].stateName+'</option>';
									}
									$(".state_chosen").html(myOptions).chosen().trigger("chosen:updated");
									$(".city_chosen").html(myCityOptions).chosen().trigger("chosen:updated");
								},
								dataType: 'html'
							});
						});
						
						$('select[name="chosenStateId"]').chosen().change( function() {
							var stateId = ($(this).val());
							$.ajax({
								type: "GET",
								async: false,
								url: "<%=contexturl %>city/citybystateid/"+stateId,
								success: function(data, textStatus, jqXHR){
									var obj = jQuery.parseJSON(data);
									var len=obj.length;
									var myOptions = "<option value></option>";
									for(var i=0; i<len; i++){
									myOptions += '<option value="'+obj[i].cityId+'">'+obj[i].cityName+'</option>';
									}
									$(".city_chosen").html(myOptions).chosen().trigger("chosen:updated");
								},
								dataType: 'html'
							});
						});
						
						changeCountry($("#chosenCountryId").val());
						
						function changeCountry(countryId){
							$.ajax({
								type: "GET",
								async: false,
								url: "<%=contexturl %>state/statebycountryid/"+countryId,
								success: function(data, textStatus, jqXHR){
									var obj = jQuery.parseJSON(data);
									var len=obj.length;
									var myOptions = "<option value></option>";
									for(var i=0; i<len; i++){
										if( stateId1!= obj[i].stateId){
											myOptions += '<option value="'+obj[i].stateId+'">'+obj[i].stateName+'</option>';
										}
										else{
											myOptions += '<option value="'+obj[i].stateId+'" selected="selected">'+obj[i].stateName+'</option>';
										}
									}
									$(".state_chosen").html(myOptions).chosen().trigger("chosen:updated");
									changeState($("#chosenStateId").val());
								},
								dataType: 'html'
							});
						}
						
						function changeState(stateId){
							$.ajax({
								type: "GET",
								async: false,
								url: "<%=contexturl %>city/citybystateid/"+stateId,
								success: function(data, textStatus, jqXHR){
									var obj = jQuery.parseJSON(data);
									var len=obj.length;
									var myOptions = "<option value></option>";
									for(var i=0; i<len; i++){
										if( cityId1!= obj[i].cityId){
											myOptions += '<option value="'+obj[i].cityId+'">'+obj[i].cityName+'</option>';
										}
										else{
											myOptions += '<option value="'+obj[i].cityId+'" selected="selected">'+obj[i].cityName+'</option>';
										}
									}
									$(".city_chosen").html(myOptions).chosen().trigger("chosen:updated");
								},
								dataType: 'html'
							});
						}

						$("#Company_form").validate(
								{	errorClass:"help-block animation-slideDown",
									errorElement:"div",
									errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
									highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
									success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
												rules : {
													universityName : {
														required : !0,maxlength : 75
													},
													firstName : {
														required : !0,maxlength : 75
													},
													lastName : {
														required : !0,maxlength : 75
													},
													emailId : {
														required : !0,maxlength : 75,
														email : !0
													},
													mobile : {
														required : !0,
														mobile : !0,
													},
													chosenCityId: {
														required : !0
													},
													chosenCountryId: {
														required : !0
													},
													chosenStateId: {
														required : !0
													},
													loginId : {
														required : !0,maxlength : 75,
														minlength : 6
													},
													password : {
														required : !0,maxlength : 75,
														minlength : 6
													},
													confirmPassword : {
														required : !0,
														equalTo : "#password"
													},
													universityTelephone1 : {
														
														phone : !0
													},
													universityTelephone2 : {
														phone : !0
													},
													universityEmail : {
														required : !0,maxlength : 75,
														email : !0
													},
													universityMobile1 : {
														
														mobile : !0
													},
													universityMobile2 : {
														mobile : !0
													},
													universityAddressLine1 : {
														maxlength : 75
													},
													universityAddressLine2 : {
														maxlength : 75
													},
													universityPincode : {
														required : !0,maxlength : 10,
														digits : !0,
														pincode: !0
													}
												},
												messages : {
													universityName : {
														required :'<spring:message code="validation.pleaseenterinstitutionName"/>',
														maxlength :'<spring:message code="validation.institutionName75character"/>'
													},
													firstName : {
														required :'<spring:message code="validation.pleaseenterfirstname"/>',
														maxlength :'<spring:message code="validation.firstname75character"/>'
													},
													lastName : {
														required :'<spring:message code="validation.pleaseenterlastname"/>',
														maxlength :'<spring:message code="validation.lastname75character"/>'
													},
													emailId : {
														required :'<spring:message code="validation.pleaseenteremailid"/>',
														email :'<spring:message code="validation.pleaseenteravalidemailid"/>',
														maxlength :'<spring:message code="validation.email75character"/>'
													},
													chosenCityId: {
														required : '<spring:message code="validation.selectcity"/>'
													},
													chosenCountryId: {
														required : '<spring:message code="validation.selectcountry"/>'
													},
													chosenStateId: {
														required : '<spring:message code="validation.selectstate"/>'
													},
													mobile : {
														required :'<spring:message code="validation.pleaseenteramobilenumber"/>',
														phone :'<spring:message code="validation.mobile"/>'
													},
													loginId : {
														required :'<spring:message code="validation.pleaseenteraloginid"/>',
														minlength :'<spring:message code="validation.loginidmustbeatleast6character"/>',
														maxlength :'<spring:message code="validation.loginid75character"/>'
													},
													password : {
														required :'<spring:message code="validation.pleaseenterapassword"/>',
														minlength :'<spring:message code="validation.passwordmustbeatleast6character"/>',
														maxlength :'<spring:message code="validation.passworddname75character"/>'
													},
													confirmPassword : {
														required :'<spring:message code="validation.pleaseconfirmpassword"/>',
														equalTo :'<spring:message code="validation.pleaseenterthesamepasswordasabove"/>'
													},
													universityTelephone1 : {
														
														phone :'<spring:message code="validation.phone"/>'
															
													},
													universityTelephone2 : {

														phone :'<spring:message code="validation.phone"/>'
													},
													universityEmail : {
														required :'<spring:message code="validation.pleaseenteremailid"/>',
														email :'<spring:message code="validation.pleaseenteravalidemailid"/>',
															maxlength :'<spring:message code="validation.email75character"/>'
													},
													universityMobile1 : {
														
														mobile :'<spring:message code="validation.mobile"/>'
													},
													universityMobile2 : {

														mobile :'<spring:message code="validation.mobile"/>'
													},
													universityAddressLine1 : {
														
															maxlength :'<spring:message code="validation.addressline1"/>'
													},
													universityAddressLine2 : {
															maxlength :'<spring:message code="validation.addressline1"/>'
													},
													universityPincode : {
														required :'<spring:message code="validation.pleaseenterpincode"/>',
														maxlength :'<spring:message code="validation.pincode10character"/>',
														digits :'<spring:message code="validation.digits"/>'
													}
												},
										});

						$("#ResetPassword_Form")
								.validate(
										{
											errorClass : "help-block animation-slideDown",
											errorElement : "div",
											errorPlacement : function(e, a) {
												a.parents(".form-group > div")
														.append(e)
											},
											highlight : function(e) {
												$(e)
														.closest(".form-group")
														.removeClass(
																"has-success has-error")
														.addClass("has-error")
											},
											success : function(e) {
												e
														.closest(".form-group")
														.removeClass(
																"has-success has-error")
											},
											rules : {
												password : {
													required : !0,
													minlength : 6
												},
												confirmPassword : {
													required : !0,
													equalTo : "#password"
												}
											},
											messages : {
												password : {
													required : "Please enter a password",
													minlength : "Password must be atleast 6 charecter"
												},
												confirmPassword : {
													required : "Please confirm password",
													equalTo : "Please enter the same password as above"
												},
											}
										});

					});
	$("#company_submit").click(function() {
		unsaved=false;
	});
</script>
<script
	src="<%=contexturl%>resources/js/pages/activeTablesDatatables.js"></script>
<script
	src="<%=contexturl%>resources/js/pages/tablesDatatables.js"></script>
<script
	src="<%=contexturl%>resources/js/pages/inactiveTablesDatatables.js"></script>

<script>$(function(){ TablesDatatables.init(3); });</script>

<script>$(function(){ ActiveTablesDatatables.init(3); });</script>
<script>$(function(){ InActiveTablesDatatables.init(3); });</script>

<style>
.chosen-container {
	width: 250px !important;
}
#map-canvas {
	height: 300px
}
</style>