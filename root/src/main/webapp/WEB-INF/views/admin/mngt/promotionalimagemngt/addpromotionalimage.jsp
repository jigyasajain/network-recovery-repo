<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>


<%-- <%@include file="../../inc/template_scripts.jsp"%> --%>

<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				<spring:message code="heading.promotionalimagemaster" />
				<br> <small><spring:message
						code="heading.addimage" /></small>
			</h1>
			<span id="errorMsg"></span>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i> <spring:message code="label.success"/>
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>

			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">�</button>
					<h4>
						<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="menu.promotionalimage" /></li>
	</ul>
	<form action="<%=contexturl %>ManagePromotionalImage/PromotionalImageList/SavePromotionalImage" method="post" class="form-horizontal "
		id="promotional_form" enctype="multipart/form-data">
		<div class="row">
			<input name="moduleId" value="${module.moduleId}" type="hidden">
			<div class="">

				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.promotionalimageinfo" /></strong>
						</h2>
					</div>	

                    <div class="form-group">
						<label class="col-md-4 control-label" for="moduleName"><spring:message
								code="label.modulename" /><span class="text-danger">*</span> </label>
						<div class="col-md-6">
                        <label class="col-md-1 control-label" for="moduleName">${module.moduleName}</label>

						</div>
					</div>

					<div class="form-group row" id="profilepic">
						<label class="col-md-4 control-label" for="offer_validtill"><spring:message
								code="label.promotionalimage" /><span class="text-danger">*</span></label>
						<div class="col-md-8">
							<div class="img-list2" id="imagediv">
								<c:choose>
									<c:when test="${! empty module.promotionalImage}">
										<img border="0"
											src="<%=contexturl%>${module.promotionalImage.imagePath}"
											width="120" height="120" title="Click here to change photo"
											onclick="OpenFileDialog();"
											id="profileimage" style="cursor: pointer;" />

										<span class="text-content" onclick="OpenFileDialog();"
											title="Click here to change photo"><br> <br>
										<span>Change Promotional Image</span></span>
									</c:when>
									<c:otherwise>
										<img border="0"
											src="<%=contexturl%>resources/img/application/default_profile.png"
											width="120" height="120" title="Click here to change photo"
											onclick="OpenFileDialog();"
											id="profileimage" style="cursor: pointer;" />

										<span class="text-content" onclick="OpenFileDialog();"
											title="Click here to change photo"><br> <br>
										<span>Upload Promotional Image</span></span>
									</c:otherwise>
								</c:choose>
							</div>
							<div>
							<input type="file" name="imageChooser1" id="imageChooser1"
								style="visibility: hidden; height: 0px" />
							</div>
						</div>
					</div>

					<!-- 					</table> -->
            </div>
           </div>
			<div class="col-md-9 col-md-offset-3">
				<button id="promotional_submit" class="btn btn-sm btn-primary save"
					type="submit" value="Validate!">
					<i class="fa fa-angle-right"></i>
					<spring:message code="button.save" />
				</button>
				<a href="<%=contexturl %>ManagePromotionalImage/PromotionalImageList/" class="btn btn-sm btn-primary save"
					style="text-decoration: none;"><i class="gi gi-remove"></i> <spring:message
						code="button.cancel" /></a>
			</div>
		</div>
	</form>
	


</div>



<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<%@include file="../../inc/chosen_scripts.jsp"%>
<script src="<%=contexturl %>resources/js/custom/featurecontrol.js"></script>
<script src="<%=contexturl %>resources/js/additional-methods.js"></script>

<script type="text/javascript"
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCheQF_yjKacwuVL4_59EO-3BThy85gbu4&sensor=false&region=GB">
	
</script>


<script type="text/javascript">
	$(document)
			.ready(
					function(){
						$("#managePromotionalImage").addClass("active");
						$(".module_chosen").data("placeholder","Select Module From...").chosen();
						
						
						$("#promotional_form").validate(
								{	errorClass:"help-block animation-slideDown",
									errorElement:"div",
									errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
									highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
									success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
												rules : {
													
													imageChooser1 :{
														<c:if test = "${empty module.promotionalImage}">
													       required: true,
														</c:if>
														     extension: "jpg|jpeg",
														     filesize: 900000,
														     maxlength: 1000
													  }
												},
													
												messages : {
															imageChooser1 : {
																<c:if test = "${empty module.promotionalImage}">
																 	required : 'Please select image',
																</c:if>
											                      extension : 'Please select image of (.jpg,.jpeg) extention only.',
											                      maxlength :'The Image is too long. Please select another image'
											           }
													},
													
										});
										
								$("#promotional_submit").click(function() {
// 									return checkpixelsize();
									$("#promotional_form").submit();
								});
					
	
	});
</script>

<script type="text/javascript">
	function OpenFileDialog(){
		document.getElementById("imageChooser1").click();
	}
	
	function changePhoto(){
		if($('#imageChooser1').val() != ""){			
			var imageName = document.getElementById("imageChooser1").value;
			var ext1 = imageName.substring(imageName.lastIndexOf("."));
			var ext = ext1.toLowerCase();
			if(ext == '.jpg' || ext == '.jpeg')
				$('#changePhoto').submit();		
			else
				alert("Please select only image file");		
		}
	}
	
</script>

<script type="text/javascript">
    function showimages(files) {
        for (var i = 0, f; f = files[i]; i++) {
           
                       
            var reader = new FileReader();
            reader.onload = function (evt) {
            	$( "#image" ).remove();
            	$( "#imagediv" ).html("");
            	
//                 var img = '<div class="col-md-6 id="imagediv"><img border="0" src="' + evt.target.result + '" width="120" height="120" title="Click here to change photo" style="cursor: pointer;"  onclick="OpenFileDialog();" class="img-circle pic" id="image"/></div>';
                  var img ='<div><img border="0" src="' + evt.target.result + '" width="120" height="120" title="Click here to change photo" onclick="OpenFileDialog();" id="image" style="cursor: pointer;" /><span class="text-content" onclick="OpenFileDialog();" title="Click here to change photo"><br><br><span>Change Promotional Image</span></span></div>';
                 $('#imagediv').html(img);
            }
            reader.readAsDataURL(f);
        }
    }

    $('#imageChooser1').change(function (evt) {
        showimages(evt.target.files);
    });
</script>

<script>
// function checkpixelsize(){
// 	var imgwidth = $("#imageChooser1").width();
// 	var imgheight = $("#imageChooser1").height();
	
// 	if(imgheight > 300 || imgwidth >300){
// 		alert("Image pixel size is too big");
// 		return false;
// 	}
// 	else{
// 		return true;
// 	}
// }
</script>

<style>
.chosen-container {
	width: 250px !important;
}
#map-canvas {
	height: 300px
}
</style>


<%@include file="../../inc/template_end.jsp"%>