<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->

<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>

<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				<i class="fa fa-map-marker"></i> <spring:message code="heading.followers"/><br> <small><spring:message code="heading.specifylistoffollower"/>
					</small>
			</h1>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="heading.managerestaurant"/></li>
		<li><a href=""><spring:message code="heading.followers"/></a></li>
	</ul>
	<div class="block full">
                <div class="block-title">
                    <h2><strong>${followerCount} Followers</strong></h2>
                </div>
                <div class="gallery" data-toggle="lightbox-gallery">
                    <div class="row">
                    	<c:forEach var="currentFollower" items="${followerList}" varStatus="count">
                        <div class="col-sm-2" style="margin-bottom: 0px; width: 120px;">
                            <c:if test="${currentFollower.profileImage!=null}">
								<img src="<%=contexturl %>${currentFollower.profileImage.imagePath}" alt="avatar" title="${currentFollower.firstName} ${currentFollower.lastName}" width="90" height="100">
							</c:if>
							<c:if test="${currentFollower.profileImage==null}">
								<img src="<%=contexturl %>resources/img/placeholders/avatars/avatar2.jpg" alt="avatar" title="${currentFollower.firstName} ${currentFollower.lastName}" width="90" height="100">
							</c:if>
                        </div>
                        </c:forEach>
                    </div>
                </div>
            </div>
        </div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>

<script type="text/javascript">
	$(document).ready(function() {
		
	});
</script>
<%@include file="../../inc/template_end.jsp"%>