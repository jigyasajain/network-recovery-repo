<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>


<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>

			<h1>
				<i class="fa fa-map-marker"></i>
				<spring:message code="heading.institutionmaster" />
				<br> <small><spring:message
						code="heading.createinstitution" /></small>
			</h1>
			<span id="errorMsg"></span>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="menu.institution" /></li>
	</ul>
	<form action="<%=contexturl %>ManageInstitution/InstitutionList/SaveInstitution" method="post" class="form-horizontal ui-formwizard"
		id="Institution_form" enctype="multipart/form-data">
		<div class="row">

			<div class="col-md-6">
				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.institutioninfo" /></strong>
						</h2>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="institution_Name"><spring:message
								code="label.institutionname" /> <span class="text-danger">*</span>
						</label>
						<div class="col-md-6">

							<input id="university_Name" name="universityName" class="form-control"
								placeholder="<spring:message code='label.institutionname'/>.."
								type="text" value="${university.universityName}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="example-daterange1"><spring:message code="label.agreementdate"/>
							</label>
						<div class="col-md-8">
							<div class="input-group input-daterange"
								data-date-format="mm/dd/yyyy">
								<input type="text" id="universityAgreementStart"
									name="universityAgreementStart" class="form-control text-center"
									placeholder="From" value="${university.agreementStart}"> <span class="input-group-addon"><i
									class="fa fa-angle-right"></i></span> <input type="text"
									id="universityAgreementEnd" name="universityAgreementEnd"
									class="form-control text-center" placeholder="To" value="${university.agreementEnd}">
							</div>
						</div>
					</div>
					<div class="form-group">
							<label class="col-md-4 control-label" for="example-daterange1"><spring:message
									code="label.logo" /> </label>
							<div class="col-md-6">
								<input type="file" name="logo" accept="image/*" />
							</div>
					</div>
				
					<div class="form-group">
							<label class="col-md-4 control-label" for="example-daterange1"><spring:message
									code="label.previewimage" /> </label>
							<div class="col-md-6">
								<input type="file" name="previewImage" accept="image/*" />
							</div>
					</div>
					
<!-- 					<div class="form-group"> -->
<%-- 						<label class="col-md-4 control-label" for="example-daterange1"><spring:message code="label.selectagreement"/> --%>
<!-- 							</label> -->
<!-- 						<div class="col-md-6"> -->
<!-- 							<input type="file" name="file" accept="application/pdf" size="50" /> -->
<!-- 						</div> -->


<!-- 					</div> -->



				</div>


				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.contactinfo" /></strong>
							<small style="float:left; margin-top: 4px;">All your SkillMe related communication will be done on these details. * These details can be same as your admin details.</small>
						</h2>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="telephone1"><spring:message
								code="label.telephone1" /></label>
						<div class="col-md-6">

							<input id="universityTelephone1" name="universityTelephone1"
								class="form-control"
								placeholder="<spring:message code='label.telephone1'/>.."
								type="text" value="${university.universityTelephone1}" >
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="telephone2"><spring:message
								code="label.telephone2" /></label>
						<div class="col-md-6">

							<input id="universityTelephone2" name="universityTelephone2"
								class="form-control"
								placeholder="<spring:message code='label.telephone2'/>.."
								type="text" value="${university.universityTelephone2}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="rest_email"><spring:message
								code="label.institutionEmail" /><span class="text-danger">*</span></label>
						<div class="col-md-6">

							<input id="company_email" name="universityEmail"
								class="form-control"
								placeholder="<spring:message code="label.institutionEmail"/>.."
								type="text" value="${university.universityEmail}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="mobile1"><spring:message
								code="label.mobile1" /></label>
						<div class="col-md-6">

							<input id="universityMobile1" name="universityMobile1"
								class="form-control"
								placeholder="<spring:message code="label.mobile1"/>.."
								type="text" value="${university.universityMobile1}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="mobile2"><spring:message
								code="label.mobile2" /></label>
						<div class="col-md-6">

							<input id="universityMobile2" name="universityMobile2"
								class="form-control"
								placeholder="<spring:message code="label.mobile2"/>.."
								type="text" value="${university.universityMobile2}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="addressline1"><spring:message
								code="label.addressline1" />
						</label>
						<div class="col-md-6">

							<input id="universityAddressLine1" name="universityAddressLine1"
								class="form-control"
								placeholder="<spring:message code="label.addressline1"/>.."
								type="text" value="${university.universityAddressLine1}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="addressline2"><spring:message
								code="label.addressline2" /></label>
						<div class="col-md-6">

							<input id="universityAddressLine2" name="universityAddressLine2"
								class="form-control"
								placeholder="<spring:message code="label.addressline2"/>.."
								type="text" value="${university.universityAddressLine2}">
						</div>
					</div>
					
					<div class="form-group" id="countryDiv">
						<label class="col-md-4 control-label" for="chosenCountryId"><spring:message
								code="label.countryname" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<select class="country_chosen" style="width: 200px;"
								id="chosenCountryId"
								data-placeholder="<spring:message code='label.choosecountry' />"
								name="chosenCountryId">
								<option value=""></option>
								<c:forEach items="${countryList}" var="country">
											<option value="${country.countryId}" 
											<c:if test="${country.countryId eq university.country.countryId}">selected="selected"</c:if>>${country.countryName}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					
					<div class="form-group" id="stateDiv">
						<label class="col-md-4 control-label" for="chosenStateId"><spring:message
								code="label.statename" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<select class="state_chosen" style="width: 200px;"
								id="chosenStateId"
								data-placeholder="<spring:message code='label.choosestate' />"
								name="chosenStateId">
								<c:forEach items="${stateList}" var="state">
												<option value="${state.stateId}"  <c:if test="${state.stateId eq university.state.stateId}">selected="selected"</c:if> >${state.stateName}</option>
									</c:forEach>
							</select>
						</div>
					</div>
					
					<div class="form-group" id="cityDiv">
						<label class="col-md-4 control-label" for="chosenCityId"><spring:message
								code="label.cityname" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<select class="city_chosen" style="width: 200px;"
								id="chosenCityId"
								data-placeholder="<spring:message code='label.choosecity' />"
								name="chosenCityId">
								<c:forEach items="${cityList}" var="city">
									<option value="${city.cityId}"  <c:if test="${city.cityId eq university.city.cityId}">selected="selected"</c:if> >${city.cityName}</option>
								</c:forEach>
							</select>
						</div>
					</div>

					

					<div class="form-group">
						<label class="col-md-4 control-label" for="universityPincode"><spring:message
								code="label.pincode" /><span class="text-danger">*</span></label>
						<div class="col-md-6">

							<input id="universityPincode" name="universityPincode"
								class="form-control"
								placeholder="<spring:message code="label.pincode"/>.."
								type="text" value="${university.universityPincode}">
						</div>
					</div>
					
					<div class="form-group">
							<label class="col-md-4 control-label" for="latitude"><spring:message
									code="label.latitude" /></label>
							<div class="col-md-6">

								<input id="latitude" name="latitude" class="form-control" 
									value="${university.latitude}"
									placeholder="<spring:message code="label.latitude"/>.."
									type="text" >
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="longitude"><spring:message
									code="label.longitude" /></label>
							<div class="col-md-6">

								<input id="longitude" name="longitude" class="form-control" 
									value="${university.longitude}"
									placeholder="<spring:message code="label.longitude"/>.."
									type="text" >
							</div>
						</div>
						
				</div>
				
				<div class="block">
						<div class="block-title">
							<h2>
								<strong>Map</strong>
							</h2>
							Please Drag Pointer to Locate Institution
						</div>

						<div id="map-canvas"></div>
				</div>
				
				<div class="block">
						<div class="block-title">
							<h2>
								<strong><spring:message code="heading.socialinfo" /></strong>
								<small style="float:left; margin-top: 4px;">Please give your social site details here</small>
							</h2>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="facebook_link"><spring:message
									code="label.facebook" /> </label>
							<div class="col-md-6">
								<input id="facebook_link" name="facebookLink"
									class="form-control" value="${university.facebookUrl}"
									placeholder="<spring:message code='label.facebook'/>.."
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="twitter_link"><spring:message
									code="label.twitter" /> </label>
							<div class="col-md-6">
								<input id="twitter_link" name="twitterLink" class="form-control"
									value="${university.twitterUrl}"
									placeholder="<spring:message code='label.twitter'/>.."
									type="text">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label" for="youtube_link"><spring:message
									code="label.youtube" /> </label>
							<div class="col-md-6">
								<input id="youtube_link" name="youtubeLink"
									class="form-control" value="${university.youtubeUrl}"
									placeholder="<spring:message code='label.youtube'/>.."
									type="text">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label" for="linkedIn_link"><spring:message
									code="label.linkedin" /> </label>
							<div class="col-md-6">
								<input id="linkedIn_link" name="linkedinLink"
									class="form-control" value="${university.linkedInUrl}"
									placeholder="<spring:message code='label.linkedin'/>.."
									type="text">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label" for="website_link"><spring:message
									code="label.website" /> </label>
							<div class="col-md-6">
								<input id="website_link" name="websiteLink"
									class="form-control" value="${university.website}"
									placeholder="<spring:message code='label.website'/>.."
									type="text">
							</div>
						</div>

					</div>
				
			</div>

			<div class="col-md-6">
				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.admininfo" /></strong>
							<small style="float:left; margin-top: 4px;">This info will be used for login. Please make sure you add the exact details and make sure it is not misused.</small>
						</h2>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="first_Name"><spring:message
								code="label.firstname" /><span class="text-danger">*</span> </label>
						<div class="col-md-6">

							<input id="first_Name" name="firstName" class="form-control"
								placeholder="<spring:message code='label.firstname'/>.."
								type="text" value="${user.firstName}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="last_Name"><spring:message
								code="label.lastname" /><span class="text-danger">*</span> </label>
						<div class="col-md-6">

							<input id="last_Name" name="lastName" class="form-control"
								placeholder="<spring:message code="label.lastname"/>.."
								type="text" value="${user.lastName}">
						</div>
					</div>

					
					<div class="form-group">
							<label class="col-md-4 control-label" for="email"><spring:message
									code="label.emailid" /><span class="text-danger">*</span></label>
							<div class="col-md-6">
								<input id="email" name="emailId" class="form-control"
									placeholder="test@example.com" type="text" 
									value="${restaurant.admin.emailId}">
							</div>
						</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="mobile"><spring:message
								code="label.mobile" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<input id="mobile" name="mobile" class="form-control"
								placeholder='<spring:message code="label.mobile"></spring:message>'
								type="text" value="${user.mobile}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="login"><spring:message
								code="label.loginid" /> <span class="text-danger">*</span> </label>
						<div class="col-md-6">
							<div >
								
								<input id="login" name="loginId" class="form-control col-md-4 "
									placeholder='<spring:message code='label.loginid'/>' type="text" value="${user.loginId}">
							</div>
							<span class="label label-success" id="availability"
								style="cursor: pointer;"><i class="fa fa-check"></i>
								<spring:message code="label.checkavailability"/></span><span class="text-success" id="available"
								style="display: none"> <spring:message
									code="alert.loginavailable" />
							</span> <span class="text-danger" id="notavailable"
								style="display: none"> <spring:message
									code="alert.loginnotavailable" />
							</span>
							<span id="loding" style="display: none"><i class="fa fa-spinner fa-spin"></i>
						   </span>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="password"><spring:message
								code="label.password" /> <span class="text-danger">*</span> </label>
						<div class="col-md-6">
							<input id="password" name="password" class="form-control"
								placeholder="Choose a crazy one.." type="password" value="${user.password}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="confirm_password"><spring:message
								code="label.confirmpassword" /><span class="text-danger">*</span>
						</label>
						<div class="col-md-6">
							<input id="confirm_password" name="confirmPassword"
								class="form-control" placeholder="..and confirm it!"
								type="password" value="${user.password}">
						</div>
					</div>
				</div>
			</div>
			
<!-- 			<div class="col-md-6"> -->
<!-- 				<div class="block" id="featurecontrol"> -->
<%-- 					<%@include file="../featurecontrol/featurecontrol.jsp"%> --%>
<!-- 				</div> -->
<!-- 			</div> -->
		</div>


		<div class="form-group form-actions">
				<div class="col-md-9 col-md-offset-3">
					<button id="institution_submit" type="submit"
						class="btn btn-sm btn-primary save">
						<i class="fa fa-angle-right"></i> <spring:message code="button.save" />
					</button>
					<button type="reset" class="btn btn-sm btn-warning">
						<i class="fa fa-repeat"></i>
						<spring:message code="button.reset" />
					</button>
						<a class="btn btn-sm btn-primary" href="<%=contexturl %>ManageInstitution/InstitutionList"></i>
							<spring:message code="button.cancel" /> </a>
				</div>
			</div>
	</form>
</div>
<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<%@include file="../../inc/chosen_scripts.jsp"%>
<script src="<%=contexturl %>resources/js/custom/featurecontrol.js"></script>
<script type="text/javascript"
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCheQF_yjKacwuVL4_59EO-3BThy85gbu4&sensor=false&region=GB">
	
</script>
<script type="text/javascript">
	function initialize() {
		var mapOptions = {
			center : new google.maps.LatLng($('#latitude').val(), $(
					'#longitude').val()),
			zoom : 13
		};
		var map = new google.maps.Map(document.getElementById("map-canvas"),
				mapOptions);

		var marker = new google.maps.Marker({
			position : map.getCenter(),
			map : map,
			title : 'Click to zoom',
			draggable : true
		});

		google.maps.event.addListener(map, 'center_changed', function() {
			// 3 seconds after the center of the map has changed, pan back to the
			// marker.

			window.setTimeout(function() {
				$('#latitude').val(marker.getPosition().k);
				$('#longitude').val(marker.getPosition().B);
				map.panTo(marker.getPosition());
			}, 3000);
		});

		google.maps.event.addListener(marker, 'click', function() {
			map.setZoom(13);
			map.setCenter(marker.getPosition());
		});
	}

	google.maps.event.addDomListener(window, 'load', initialize);
</script>
<script type="text/javascript">
	$(document)
			.ready(
					function() {
						
// 						$('#universityAgreementStart').datepicker('setStartDate', new Date());
// 						$('#universityAgreementEnd').datepicker('setStartDate', new Date());

						$(".input-datepicker, .input-daterange").datepicker({
							weekStart : 1
						});
						
						$(".input-timepicker24").timepicker({
							minuteStep : 1,
							showSeconds : !0,
							showMeridian : !1
						});

						$(".state_chosen").chosen();
						$(".city_chosen").chosen();
						$(".country_chosen").chosen();
						
						$('select[name="chosenCountryId"]').chosen().change( function() {
							var countryId = ($(this).val());
							$.ajax({
								type: "GET",
								async: false,
								url: "<%=contexturl %>state/statebycountryid/"+countryId,
								success: function(data, textStatus, jqXHR){
									var obj = jQuery.parseJSON(data);
									var len=obj.length;
									var myOptions = "<option value></option>";
									var myCityOptions = "<option value></option>";
									for(var i=0; i<len; i++){
									myOptions += '<option value="'+obj[i].stateId+'">'+obj[i].stateName+'</option>';
									}
									$(".state_chosen").html(myOptions).chosen().trigger("chosen:updated");
									$(".city_chosen").html(myCityOptions).chosen().trigger("chosen:updated");
								},
								dataType: 'html'
							});
							
						});
						
						$('select[name="chosenStateId"]').chosen().change( function() {
							var stateId = ($(this).val());
							$.ajax({
								type: "GET",
								async: false,
								url: "<%=contexturl %>city/citybystateid/"+stateId,
								success: function(data, textStatus, jqXHR){
									var obj = jQuery.parseJSON(data);
									var len=obj.length;
									var myOptions = "<option value></option>";
									for(var i=0; i<len; i++){
									myOptions += '<option value="'+obj[i].cityId+'">'+obj[i].cityName+'</option>';
									}
									$(".city_chosen").html(myOptions).chosen().trigger("chosen:updated");
								},
								dataType: 'html'
							});
							
							
						});
						
						

						$("#availability").click(
								function() {
									var data = $("#login").val();
									data = '<%=propvalue.companyLoginPrefix%>'+data;
									$("#notavailable").css("display", "none");
									$("#loding").css("display", "inline");
									$("#available").css("display", "none");
									if (data != "") {
										$.getJSON("<%=contexturl%>ValidateLogin", {
											loginId : data
										}, function(data) {
											if (data == "exist") {
												$("#notavailable").css("display", "inline");
												$("#loding").css("display", "none");		
											} else if (data == "available") {
												$("#available").css("display","inline");
												$("#loding").css("display", "none");
											}
										});
									}
								});
								
						$("#Institution_form").validate(
						{	errorClass:"help-block animation-slideDown",
							errorElement:"div",
							errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
							highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
							success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
										rules : {
											universityName : {
												required : !0,maxlength : 75
											},
											firstName : {
												required : !0,maxlength : 75
											},
											lastName : {
												required : !0,maxlength : 75
											},
											emailId : {
												required : !0,maxlength : 75,
												email : !0
											},
											mobile : {
												required : !0,
												mobile : !0,
											},
											chosenCityId: {
												required : !0
											},
											chosenCountryId: {
												required : !0
											},
											chosenStateId: {
												required : !0
											},
											loginId : {
												required : !0,maxlength : 75,
												minlength : 6
											},
											password : {
												required : !0,maxlength : 75,
												minlength : 6
											},
											confirmPassword : {
												required : !0,
												equalTo : "#password"
											},
											universityTelephone1 : {
												phone : !0
											},
											universityTelephone2 : {
												phone : !0
											},
											universityEmail : {
												required : !0,maxlength : 75,
												email : !0
											},
											universityMobile1 : {
												mobile : !0
											},
											universityMobile2 : {
												mobile : !0
											},
											universityAddressLine1 : {
												maxlength : 75
											},
											universityAddressLine2 : {
												maxlength : 75
											},
											universityPincode : {
												required : !0,maxlength : 10,
												digits : !0,
												pincode: !0
											}
										},
										messages : {
											universityName : {
												required :'<spring:message code="validation.pleaseenterinstitutionName"/>',
													maxlength :'<spring:message code="validation.institutionName75character"/>'
											},
											firstName : {
												required :'<spring:message code="validation.pleaseenterfirstname"/>',
												maxlength :'<spring:message code="validation.firstname75character"/>'
											},
											lastName : {
												required :'<spring:message code="validation.pleaseenterlastname"/>',
												maxlength :'<spring:message code="validation.lastname75character"/>'
											},
											emailId : {
												required :'<spring:message code="validation.pleaseenteremailid"/>',
												email :'<spring:message code="validation.pleaseenteravalidemailid"/>',
												maxlength :'<spring:message code="validation.email75character"/>'
											},
											chosenCityId: {
												required : '<spring:message code="validation.selectcity"/>'
											},
											chosenCountryId: {
												required : '<spring:message code="validation.selectcountry"/>'
											},
											chosenStateId: {
												required : '<spring:message code="validation.selectstate"/>'
											},
											mobile : {
												required :'<spring:message code="validation.pleaseenteramobilenumber"/>',
												phone :'<spring:message code="validation.mobile"/>'
											},
											loginId : {
												required :'<spring:message code="validation.pleaseenteraloginid"/>',
												minlength :'<spring:message code="validation.loginidmustbeatleast6character"/>',
												maxlength :'<spring:message code="validation.loginid75character"/>'
											},
											password : {
												required :'<spring:message code="validation.pleaseenterapassword"/>',
												minlength :'<spring:message code="validation.passwordmustbeatleast6character"/>',
												maxlength :'<spring:message code="validation.passworddname75character"/>'
											},
											confirmPassword : {
												required :'<spring:message code="validation.pleaseconfirmpassword"/>',
												equalTo :'<spring:message code="validation.pleaseenterthesamepasswordasabove"/>'
											},
											universityTelephone1 : {
												
												phone :'<spring:message code="validation.phone"/>'
													
											},
											universityTelephone2 : {

												phone :'<spring:message code="validation.phone"/>'
											},
											universityEmail : {
												required :'<spring:message code="validation.pleaseenteremailid"/>',
												email :'<spring:message code="validation.pleaseenteravalidemailid"/>',
													maxlength :'<spring:message code="validation.email75character"/>'
											},
											universityMobile1 : {
												
												mobile :'<spring:message code="validation.mobile"/>'
											},
											universityMobile2 : {

												mobile :'<spring:message code="validation.mobile"/>'
											},
											universityAddressLine1 : {
												
													maxlength :'<spring:message code="validation.addressline1"/>'
											},
											universityAddressLine2 : {
													maxlength :'<spring:message code="validation.addressline1"/>'
											},
											universityPincode : {
												required :'<spring:message code="validation.pleaseenterpincode"/>',
												maxlength :'<spring:message code="validation.pincode10character"/>',
												digits :'<spring:message code="validation.digits"/>'
											}
										},
								});

						$("#institution_submit").click(function() {
							unsaved=false;
							$("#Institution_form").submit();
						});
						
						$("button[type=reset]").click(function(evt) {
							evt.preventDefault(); // stop default reset behavior (or the rest of this code will run before the reset actually happens)
							form = $(this).parents("form").first(); // get the reset buttons form
							form[0].reset(); // actually reset the form
							 // tell all Chosen fields in the form that the values may have changed
							form.find(".country_chosen").trigger("chosen:updated");
							
							var myOptions = "<option value></option>";
							$(".city_chosen").html(myOptions).chosen().trigger("chosen:updated");
							
							$(".state_chosen").html(myOptions).chosen().trigger("chosen:updated");
							});

						
					});
	
// 	function emailVerification(){
// 		var data = $("#email").val();
// 		$("#emailnotavailable").css("display","none");
// 		$("#emailloading").css("display", "inline");
// 		$("#emailavailable").css("display","none");
// 		if( data != ""){
<%-- 			$.getJSON("<%=contexturl%>ValidateEmailId",{emailId: data}, function(data){ --%>
// 				if(data == "exist"){
// 					$("#emailnotavailable").css("display","inline");
// 					$("#emailloading").css("display", "none");
// 				}
// 				else if(data == "available"){
// 					$("#emailavailable").css("display","inline");
// 					$("#emailloading").css("display", "none");
// 				}
// 			});
// 		}
// 	}
</script>
</script>
<style>
.chosen-container {
	width: 250px !important;
}
#map-canvas {
	height: 300px
}
</style>
<%@include file="../../inc/template_end.jsp"%>