<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>


<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>

			<h1>
<!-- 				<i class="fa fa-map-marker"></i> -->
				<spring:message code="heading.companymaster" />
				<br> <small><spring:message
						code="heading.companynewcompany" /></small>
			</h1>
			<span id="errorMsg"></span>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="menu.company" /></li>
	</ul>
	<form action="<%=contexturl %>ManageCompany/CompanyList/SaveCompany" method="post" class="form-horizontal ui-formwizard"
		id="Company_form" enctype="multipart/form-data">
		<div class="row">

			<div class="col-md-6">
				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.companyinfo" /></strong>
						</h2>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="company_Name"><spring:message
								code="label.companyname" /> <span class="text-danger">*</span>
						</label>
						<div class="col-md-6">

							<input id="company_Name" name="companyName" class="form-control"
								placeholder="<spring:message code='label.companyname'/>.."
								type="text" value="${company.companyName}">
						</div>
					</div>


				</div>


				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.contactinfo" /></strong>
							<small style="float:left; margin-top: 4px;">All your Atma related communication will be done on these details. * These details can be same as your admin details.</small>
						</h2>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="telephone1"><spring:message
								code="label.telephone1" /></label>
						<div class="col-md-6">

							<input id="companyTelephone1" name="companyTelephone1"
								class="form-control"
								placeholder="<spring:message code='label.telephone1'/>.."
								type="text" value="${company.companyTelephone1}" >
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="telephone2"><spring:message
								code="label.telephone2" /></label>
						<div class="col-md-6">

							<input id="companyTelephone2" name="companyTelephone2"
								class="form-control"
								placeholder="<spring:message code='label.telephone2'/>.."
								type="text" value="${company.companyTelephone2}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="rest_email"><spring:message
								code="label.companyEmail" /><span class="text-danger">*</span></label>
						<div class="col-md-6">

							<input id="company_email" name="companyEmail"
								class="form-control"
								placeholder="<spring:message code="label.companyEmail"/>.."
								type="text" value="${company.companyEmail}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="mobile"><spring:message
								code="label.mobile" /><span class="text-danger">*</span></label>
						<div class="col-md-6">

							<input id="companyMobile" name="companyMobile"
								class="form-control"
								placeholder="<spring:message code="label.mobile"/>.."
								type="text" value="${company.companyMobile}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="addressline1"><spring:message
								code="label.addressline1" />
						</label>
						<div class="col-md-6">

							<input id="companyAddressLine1" name="companyAddressLine1"
								class="form-control"
								placeholder="<spring:message code="label.addressline1"/>.."
								type="text" value="${company.companyAddressLine1}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="addressline2"><spring:message
								code="label.addressline2" /></label>
						<div class="col-md-6">

							<input id="companyAddressLine2" name="companyAddressLine2"
								class="form-control"
								placeholder="<spring:message code="label.addressline2"/>.."
								type="text" value="${company.companyAddressLine2}">
						</div>
					</div>
					
					<div class="form-group" id="countryDiv">
						<label class="col-md-4 control-label" for="chosenCountryId"><spring:message
								code="label.countryname" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<select class="country_chosen" style="width: 200px;"
								id="chosenCountryId"
								data-placeholder="<spring:message code='label.choosecountry' />"
								name="chosenCountryId">
								<option value=""></option>
								<c:forEach items="${countryList}" var="country">
											<option value="${country.countryId}" 
											<c:if test="${country.countryId eq company.country.countryId}">selected="selected"</c:if>>${country.countryName}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					
					<div class="form-group" id="stateDiv">
						<label class="col-md-4 control-label" for="chosenStateId"><spring:message
								code="label.statename" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<select class="state_chosen" style="width: 200px;"
								id="chosenStateId"
								data-placeholder="<spring:message code='label.choosestate' />"
								name="chosenStateId">
								<c:forEach items="${stateList}" var="state">
												<option value="${state.stateId}"  <c:if test="${state.stateId eq company.state.stateId}">selected="selected"</c:if> >${state.stateName}</option>
									</c:forEach>
							</select>
						</div>
					</div>
					
					<div class="form-group" id="cityDiv">
						<label class="col-md-4 control-label" for="chosenCityId"><spring:message
								code="label.cityname" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<select class="city_chosen" style="width: 200px;"
								id="chosenCityId"
								data-placeholder="<spring:message code='label.choosecity' />"
								name="chosenCityId">
								<c:forEach items="${cityList}" var="city">
									<option value="${city.cityId}"  <c:if test="${city.cityId eq company.city.cityId}">selected="selected"</c:if> >${city.cityName}</option>
								</c:forEach>
							</select>
						</div>
					</div>

					

					<div class="form-group">
						<label class="col-md-4 control-label" for="companyPincode"><spring:message
								code="label.pincode" /><span class="text-danger">*</span></label>
						<div class="col-md-6">

							<input id="companyPincode" name="companyPincode"
								class="form-control"
								placeholder="<spring:message code="label.pincode"/>.."
								type="text" value="${company.companyPincode}">
						</div>
					</div>
					
				
			</div>
	</div>
			<div class="col-md-6">
				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.admininfo" /></strong>
							<small style="float:left; margin-top: 4px;">This info will be used for login. Please make sure you add the exact details and make sure it is not misused.</small>
						</h2>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="first_Name"><spring:message
								code="label.firstname" /><span class="text-danger">*</span> </label>
						<div class="col-md-6">

							<input id="first_Name" name="firstName" class="form-control"
								placeholder="<spring:message code='label.firstname'/>.."
								type="text" value="${user.firstName}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="last_Name"><spring:message
								code="label.lastname" /><span class="text-danger">*</span> </label>
						<div class="col-md-6">

							<input id="last_Name" name="lastName" class="form-control"
								placeholder="<spring:message code="label.lastname"/>.."
								type="text" value="${user.lastName}">
						</div>
					</div>

					
					<div class="form-group">
							<label class="col-md-4 control-label" for="email"><spring:message
									code="label.emailid" /><span class="text-danger">*</span></label>
							<div class="col-md-6">
								<input id="email" name="emailId" class="form-control"
									placeholder="test@example.com" type="text" 
									value="${user.emailId}" >
							</div>
						</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="mobile"><spring:message
								code="label.mobile" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<input id="mobile" name="mobile" class="form-control"
								placeholder='<spring:message code="label.mobile"></spring:message>'
								type="text" value="${user.mobile}">
						</div>
					</div>
				</div>
			</div>
			
		</div>

	<div style="text-align: center; margin-bottom: 10px">
		<div class="form-group form-actions">
<!-- 				<div class="col-md-9 col-md-offset-3"> -->
					<button id="company_submit" type="submit"
						class="btn btn-sm btn-primary save">
						<i class="fa fa-angle-right"></i> <spring:message code="button.save" />
					</button>
					<button type="reset" class="btn btn-sm btn-warning">
						<i class="fa fa-repeat"></i>
						<spring:message code="button.reset" />
					</button>
						<a class="btn btn-sm btn-primary save" href="<%=contexturl %>ManageCompany/CompanyList"></i>
							<spring:message code="button.cancel" /> </a>
		</div>
	</div>
</form>
</div>
<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<%@include file="../../inc/chosen_scripts.jsp"%>
<script src="<%=contexturl %>resources/js/custom/featurecontrol.js"></script>
<script type="text/javascript"
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCheQF_yjKacwuVL4_59EO-3BThy85gbu4&sensor=false&region=GB">
	
</script>
<script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>

<script type="text/javascript">
	$(document)
			.ready(
					function() {
						
						$("#manageCompany").addClass("active");

						$(".state_chosen").chosen();
						$(".city_chosen").chosen();
						$(".country_chosen").chosen();
						
						$('select[name="chosenCountryId"]').chosen().change( function() {
							var countryId = ($(this).val());
							$.ajax({
								type: "GET",
								async: false,
								url: "<%=contexturl %>state/statebycountryid/"+countryId,
								success: function(data, textStatus, jqXHR){
									var obj = jQuery.parseJSON(data);
									var len=obj.length;
									var myOptions = "<option value></option>";
									var myCityOptions = "<option value></option>";
									for(var i=0; i<len; i++){
									myOptions += '<option value="'+obj[i].stateId+'">'+obj[i].stateName+'</option>';
									}
									$(".state_chosen").html(myOptions).chosen().trigger("chosen:updated");
									$(".city_chosen").html(myCityOptions).chosen().trigger("chosen:updated");
								},
								dataType: 'html'
							});
							
						});
						
						$('select[name="chosenStateId"]').chosen().change( function() {
							var stateId = ($(this).val());
							$.ajax({
								type: "GET",
								async: false,
								url: "<%=contexturl %>city/citybystateid/"+stateId,
								success: function(data, textStatus, jqXHR){
									var obj = jQuery.parseJSON(data);
									var len=obj.length;
									var myOptions = "<option value></option>";
									for(var i=0; i<len; i++){
									myOptions += '<option value="'+obj[i].cityId+'">'+obj[i].cityName+'</option>';
									}
									$(".city_chosen").html(myOptions).chosen().trigger("chosen:updated");
								},
								dataType: 'html'
							});
							
							
						});

								
						$("#Company_form").validate(
						{	errorClass:"help-block animation-slideDown",
							errorElement:"div",
							errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
							highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
							success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
										rules : {
											companyName : {
												required : !0,maxlength : 75
											},
											firstName : {
												required : !0,maxlength : 75
											},
											lastName : {
												required : !0,maxlength : 75
											},
											emailId : {
												required : !0,maxlength : 75,
												email : !0
											},
											mobile : {
												required : !0,
												phoneUS : !0,
											},
											chosenCityId: {
												required : !0
											},
											chosenStateId: {
												required : !0
											},
											
											chosenCountryId: {
												required : !0
											},


											companyTelephone1 : {
												phone : !0
											},
											companyTelephone2 : {
												phone : !0
											},
											companyEmail : {
												required : !0,maxlength : 75,
												email : !0
											},
											companyMobile : {
												required : !0, 
												phoneUS : !0
											},

											companyAddressLine1 : {
												maxlength : 75
											},
											companyAddressLine2 : {
												maxlength : 75
											},
											companyPincode : {
												required : !0,maxlength : 10,
												digits : !0,
												pincode: !0
											}
										},
										messages : {
											companyName : {
												required :'Please enter a Company Name',
													maxlength :'Company name should not be more than 75 characters'
											},
											firstName : {
												required :'<spring:message code="validation.pleaseenterfirstname"/>',
												maxlength :'<spring:message code="validation.firstname75character"/>'
											},
											lastName : {
												required :'<spring:message code="validation.pleaseenterlastname"/>',
												maxlength :'<spring:message code="validation.lastname75character"/>'
											},
											emailId : {
												required :'<spring:message code="validation.pleaseenteremailid"/>',
												email :'<spring:message code="validation.pleaseenteravalidemailid"/>',
												maxlength :'<spring:message code="validation.email75character"/>'
											},
											chosenCityId: {
												required : '<spring:message code="validation.selectcity"/>'
											},
											chosenStateId: {
												required : '<spring:message code="validation.selectstate"/>'
											},
											chosenCountryId: {
												required : '<spring:message code="validation.selectcountry"/>'
											},
											mobile : {
												required :'<spring:message code="validation.pleaseenteramobilenumber"/>',
												phoneUS :'<spring:message code="validation.mobile"/>',
												maxlength :'<spring:message code="validation.mobile10character"/>'
											},

											companyTelephone1 : {
												
												phone :'<spring:message code="validation.phone"/>'
													
											},
											companyTelephone2 : {

												phone :'<spring:message code="validation.phone"/>'
											},
											companyEmail : {
												required :'<spring:message code="validation.pleaseenteremailid"/>',
												email :'<spring:message code="validation.pleaseenteravalidemailid"/>',
													maxlength :'<spring:message code="validation.email75character"/>'
											},
											companyMobile : {
												required : '<spring:message code="validation.pleaseenteramobilenumber"/>',
												maxlength :'<spring:message code="validation.mobile10character"/>',
												mobile :'<spring:message code="validation.mobile"/>'
											},

											companyAddressLine1 : {
												
													maxlength :'<spring:message code="validation.addressline1"/>'
											},
											companyAddressLine2 : {
													maxlength :'<spring:message code="validation.addressline1"/>'
											},
											companyPincode : {
												required :'<spring:message code="validation.pleaseenterpincode"/>',
												maxlength :'<spring:message code="validation.pincode10character"/>',
												digits :'<spring:message code="validation.digits"/>'
											}
										},
								});

						$("#company_submit").click(function() {
							unsaved=false;
							$("#Company_form").submit();
						});
						
						$("button[type=reset]").click(function(evt) {
							evt.preventDefault(); // stop default reset behavior (or the rest of this code will run before the reset actually happens)
							form = $(this).parents("form").first(); // get the reset buttons form
							form[0].reset(); // actually reset the form
							 // tell all Chosen fields in the form that the values may have changed
							form.find(".country_chosen").trigger("chosen:updated");
							
							var myOptions = "<option value></option>";
							$(".city_chosen").html(myOptions).chosen().trigger("chosen:updated");
							
							$(".state_chosen").html(myOptions).chosen().trigger("chosen:updated");
							});

						
					});
	
// 	function emailVerification(){
// 		var data = $("#email").val();
// 		$("#emailnotavailable").css("display","none");
// 		$("#emailloading").css("display", "inline");
// 		$("#emailavailable").css("display","none");
// 		if( data != ""){
<%-- 			$.getJSON("<%=contexturl%>ValidateEmailId",{emailId: data}, function(data){ --%>
// 				if(data == "exist"){
// 					$("#emailnotavailable").css("display","inline");
// 					$("#emailloading").css("display", "none");
// 				}
// 				else if(data == "available"){
// 					$("#emailavailable").css("display","inline");
// 					$("#emailloading").css("display", "none");
// 				}
// 			});
// 		}
// 	}
</script>

<style>
.chosen-container {
	width: 250px !important;
}
#map-canvas {
	height: 300px
}
</style>
<%@include file="../../inc/template_end.jsp"%>