

<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>

<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>

			<h1>
				<i class="fa fa-map-marker"></i>
				<spring:message code="heading.coursecategorymaster" />
				<br> <small><spring:message
						code="heading.coursecategorymasterdetails" /></small>
			</h1>
			<span id="errorMsg"></span>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="heading.coursecategorymaster" /></li>
	</ul>
	<form action="<%=contexturl %>ManageCourse/CourseCategoryList/SaveCourseCategory" method="post" class="form-horizontal ui-formwizard"
		id="Instructor_form" >
		
		<div class="col-md-12">
				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.coursecategoryinfo" /></strong>
						</h2>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="coursecategory_Name"><spring:message
								code="label.coursecategoryname" /><span class="text-danger">*</span> </label>
						<div class="col-md-6">

							<input id="coursecategory_Name" name="courseCategoryName" class="form-control"
								placeholder="<spring:message code='label.coursecategoryname'/>.."
								type="text" value="${cousreCategory.categoryName}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="coursecategory_Short_Name"><spring:message
								code="label.coursecategoryshortname" /><span class="text-danger">*</span> </label>
						<div class="col-md-6">

							<input id="coursecategory_Short_Name" name="courseCategoryShortName" class="form-control"
								placeholder="<spring:message code="label.coursecategoryshortname"/>.."
								type="text" value="${cousreCategory.shortName}">
						</div>
					</div>
					
				</div>
			</div>
			<div class="form-group form-actions">
				<div class="col-md-9 col-md-offset-3">
					<button id="user_submit" type="submit"
						class="btn btn-sm btn-primary save">
						<i class="fa fa-angle-right"></i> <spring:message code="button.save" />
					</button>
					<button type="reset" class="btn btn-sm btn-warning">
						<i class="fa fa-repeat"></i>
						<spring:message code="button.reset" />
					</button>
					<a class="btn btn-sm btn-primary" href="<%=contexturl %>ManageCourse/CourseCategoryList/">
							<spring:message code="button.cancel" /> </a>
				</div>
			</div>
	</form>
</div>
<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>

<script type="text/javascript">
$(document).ready(function() {
	
	
	
	$("#Instructor_form").validate(
			{	errorClass:"help-block animation-slideDown",
				errorElement:"div",
				errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
				highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
				success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
				rules:{ courseCategoryName : {required : !0,maxlength : 75},
					courseCategoryShortName : {required : !0,maxlength : 75}
				},
				messages:{
					courseCategoryName : {
						required :'<spring:message code="validation.pleaseentercoursecategoryname"/>',
						maxlength :'<spring:message code="validation.coursecategoryname75character"/>'
					},
					courseCategoryShortName : {
						required :'<spring:message code="validation.pleaseentercoursecategoryshortname"/>',
						maxlength :'<spring:message code="validation.coursecategoryshortname75character"/>'
					},
				},
			});
	
	 $("#user_submit").click(function(){
		 $("#Instructor_form").submit();
	 });
	 
	 $("button[type=reset]").click(function(evt) {
			evt.preventDefault(); 
			form = $(this).parents("form").first(); 
			form[0].reset(); 
// 			form.find(".chosen").trigger("chosen:updated");
// 			var myOptions = "<option value></option>";
// 			$(".state_chosen").html(myOptions).chosen().trigger("chosen:updated");
		});
	
});
</script>

<%@include file="../../inc/template_end.jsp"%>