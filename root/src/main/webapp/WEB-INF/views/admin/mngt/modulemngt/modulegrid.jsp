<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				
				<spring:message code="heading.modulemaster" />
				<br> <small><spring:message
						code="heading.listofmodule" /> </small>
			</h1>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i>
						<spring:message code="label.success" />
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>
			<div class="alert alert-danger alert-dismissable"
				style="display: none" id="error">
				<button type="button" class="close" data-dismiss="alert"
					aria-hidden="true">x</button>
				<h4>
					<i class="fa fa-times-circle"> </i> <spring:message code="label.error"/>
				</h4>
				<span id="errorMsg"></span>
			</div>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="heading.managemodule" /></li>
		<li><a href="#"><spring:message code="label.module" /></a></li>
	</ul>
	<div class="block full  gridView">
		<div class="block-title">
			<h2>
				<strong><spring:message code="heading.activemodule" /></strong>
			</h2>
		</div>
		<a href="<%=contexturl %>ManageModule/ModuleList/AddModule" id="addUniversity" class="btn btn-sm btn-primary save"><spring:message
				code="label.addmodule" /></a>
		<!--<p><a href="https://datatables.net/" target="_blank">DataTables</a> is a plug-in for the Jquery Javascript library. It is a highly flexible tool, based upon the foundations of progressive enhancement, which will add advanced interaction controls to any HTML table. It is integrated with template's design and it offers many features such as on-the-fly filtering and variable length pagination.</p> -->
		<div class="table-responsive">
			<table id="active-datatable"
				class="table table-vcenter table-condensed table-bordered">
				<thead>
					<tr>
					<th class="text-center"><spring:message code="label.id" /></th>
						<th class="text-center"><spring:message code="label.modulename" /></th> 
						<th class="text-center"><spring:message code="label.createdOn" /></th>
				        <th class="text-center"><spring:message code="label.createdBy" /></th> 
				        <th class="text-center"><spring:message code="label.categorycount" /></th>
				        <th class="text-center"><spring:message code="label.action" /></th>
					</tr>
				</thead>
				<tbody id="tbody">
					<c:forEach var="currentModule" items="${moduleList}"
						varStatus="count">

						<tr>
							<td class="text-center">${count.count}</td>
							<td class="text-left"><c:out value="${currentModule.moduleName}" /></td>
							 <td class="text-left"> <fmt:formatDate type="date" value="${currentModule.createdDate}" /></td>
<%--  						    <td class="text-left"><c:out value="${currentModule.createdDate}" /></td>  --%>
						    <td class="text-left"><c:out value="${currentModule.createdBy.fullName}" /></td>  
						    <td class="text-left"><c:out value="${currentModule.categoryCount}" /></td>

							<td class="text-center">
								<div class="btn-group">
									<a href="<%=contexturl %>ManageModule/ModuleList/EditModule?id=${currentModule.moduleId}"
										data-toggle="tooltip" title="Edit"
										class="btn btn-xs btn-default save"><i class="fa fa-pencil"></i></a>
									<a href="#delete_company_popup" data-toggle="modal"
										onclick="deleteCompany(${currentModule.moduleId})"
										title="delete Module" class="btn btn-xs btn-danger save"><i
										class="fa fa-times"></i></a>
								</div>
							</td>
						</tr>
					</c:forEach>

				</tbody>
			</table>
		</div>
	</div>


	<div class="block full  gridView">
		<div class="block-title">
			<h2>
				<strong><spring:message code="heading.inactivemodule" /></strong>
			</h2>
		</div>
		<div class="table-responsive">
			<table id="inactive-datatable"
				class="table table-vcenter table-condensed table-bordered ">
				<thead>
					<tr>
					<th class="text-center"><spring:message code="label.id" /></th>
						<th class="text-center"><spring:message code="label.modulename" /></th>
                        <th class="text-center"><spring:message code="label.createdOn" /></th>
						<th class="text-center"><spring:message code="label.createdBy" /></th>
						<th class="text-center"><spring:message code="label.categorycount" /></th> 
						<th class="text-center"><spring:message code="label.action" /></th>
					</tr>
				</thead>
				<tbody id="tablebody">
					<c:forEach var="currentModule" items="${inactiveModuleList}"
						varStatus="count">
						<tr>
							<td class="text-center">${count.count}</td>
							<td class="text-left"><c:out
									value="${currentModule.moduleName}" /></td>
<%-- 							 <td class="text-left"> <fmt:formatDate type="date" value="${currentModule.createdOn.toDate()}" /></td> --%>
<%-- 							<td class="text-left"><c:out value="${currentModule.createdDate}" /></td>  --%>
							 <td class="text-left"> <fmt:formatDate type="date" value="${currentModule.createdDate}" /></td>
                             <td class="text-left"><c:out value="${currentModule.createdBy.fullName}" /></td> 
							<td class="text-left"><c:out value="${currentModule.categoryCount}" /></td>
							<td class="text-center">
								<div class="btn-group">
									<a href="#restore_Company_popup" data-toggle="modal"
										onclick="restoreCompany(${currentModule.moduleId})"
										title="Restore Module" class="btn btn-xs btn-success save"><i
										class="fa fa-plus"></i> </a>
								</div>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	<div id="delete_company_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="<%=contexturl %>ManageModule/ModuleList/DeleteModule" method="post" class="form-horizontal form-bordered save"
						id="delete_company_Form">
						<div style="padding: 10px; height: 110px;">
							<label><spring:message
									code="validation.doyouwanttodeletethismodule" /></label><br>
							<label><spring:message
									code="validation.deletingmodulewilldeletealldetailsofmodule" /></label>
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal">
									<spring:message code="label.no" />
								</button>
								<div id="delete_company" class="btn btn-sm btn-primary">
									<spring:message code="label.yes" />
								</div>
							</div>
						</div>
						<input type="hidden" name="id" id="deletecompanyId">

					</form>

				</div>
			</div>
		</div>
	</div>


	<div id="restore_Company_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="<%=contexturl %>ManageModule/ModuleList/RestoreModule" method="post" class="form-horizontal form-bordered save" id="restore_company_Form">
						
						<div style="padding: 10px; height: 110px;">
							<label><spring:message
									code="validation.doyouwanttorestorethismodule" /></label><br>
							<label><spring:message
									code="validation.restoremodulewillrestoreallperviouslydeleteddetails" /></label>
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal"><spring:message code="label.no" /></button>
								<div id="restore_Company" class="btn btn-sm btn-primary">	<spring:message code="label.yes" /></div>
							</div>
						</div>
						<input type="hidden" name="id" id="restorecompanyId">
					</form>
				</div>
			</div>
		</div>
	</div>

</div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<script
	src="<%=contexturl%>resources/js/pages/activeTablesDatatables.js"></script>
<script
	src="<%=contexturl%>resources/js/pages/inactiveTablesDatatables.js"></script>
<script>$(function(){ ActiveTablesDatatables.init(5); });</script>
<script>$(function(){ InActiveTablesDatatables.init(5); });</script>
<script type="text/javascript">
	var selectedId = 0;
	var restoreId=0;
	$(document).ready(function() {
		
		
		
		$("#manageModule").addClass("active");
		
        $("#restore_Company").click(function(){
			$("#restorecompanyId").val(restoreId);
			$("#restore_company_Form").submit();
		});
		
		
		$("#delete_company").click(function(){
			$("#deletecompanyId").val(selectedId);
			$("#delete_company_Form").submit();
		});
	});
	
	function deleteCompany(id){
		selectedId = id;
	}
	function restoreCompany(id){
		restoreId = id;
	}
</script>
<%@include file="../../inc/template_end.jsp"%>