<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>


<%-- <%@include file="../../inc/template_scripts.jsp"%> --%>

<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				<spring:message code="heading.sponsoredimagemaster" />
				<br> <small><spring:message
						code="heading.editimage" /></small>
			</h1>
			<span id="errorMsg"></span>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i> <spring:message code="label.success"/>
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>

			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">�</button>
					<h4>
						<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="menu.sponsoredimage" /></li>
	</ul>
	<form action="<%=contexturl %>ManageSponsoredImage/SponsoredImageList/UpdateSponsoredImage" method="post" class="form-horizontal "
		id="Module_form" enctype="multipart/form-data">
		<div class="row">
			<input name="moduleId" value="${module.moduleId}" type="hidden">
			<div class="">
				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.sponsoredimageinfo" /></strong>
						</h2>
					</div>	
					<div class="form-group" id="moduleDiv">
						<label class="col-md-4 control-label" for="chosenModuleId"><spring:message
								code="label.modulename" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<select class="module_chosen" style="width: 200px;"
								id="chosenModuleId"
								data-placeholder="<spring:message code='label.choosemodule' />"
								name="chosenModuleId">
								<option value=""></option>
								<c:forEach items="${moduleList}" var="module">
											<option value="${module.moduleId}" 
											<c:if test="${module.moduleId eq moduleId}">selected="selected"</c:if>>${module.moduleName}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label" for="link_Name"><spring:message
								code="label.linkname" /> <span class="text-danger">*</span> </label>
						<div class="col-md-6">

							<input id="link_Name" name="link" class="form-control"
								placeholder="<spring:message code='label.linkname'/>.."
								type="text" value="${module.link}">
						</div>
					</div>
           			
           			<div class="form-group img-list1" id="profilepic">
						<label class="col-md-4 control-label" for="offer_validtill"><spring:message code="label.moduleimage" /></label>
						<div class="col-md-6 img-list2" id="imagediv">
						<c:choose >
					      <c:when test="${! empty module.sponsoredImage}">
							<img border="0"
								src="<%=contexturl%>${module.sponsoredImage.imagePath}"
								width="120" height="120" title="Click here to change photo"
								onclick="OpenFileDialogInstructor();"  id="profileimage" style="cursor: pointer;" />
								
								<span class="text-content" onclick="OpenFileDialogInstructor();" title="Click here to change photo"><br>
								<br><span>Upload Sponsored Image</span></span>
						  </c:when>
						  <c:otherwise>
						  <img border="0"
								src="<%=contexturl%>resources/img/application/default_profile.png"
								width="120" height="120" title="Click here to change photo"
								onclick="OpenFileDialogInstructor();"  id="profileimage" style="cursor: pointer;" />
								
								<span class="text-content" onclick="OpenFileDialogInstructor();" title="Click here to change photo"><br>
								<br><span>Change Sponsored Image</span></span>
						  </c:otherwise>
						  </c:choose>
						</div>
						<input type="file" id="imageChooserProfile" style="display: none;"
							name="imageChooserProfile"
							accept="image/jpg,image/jpeg,image/png,image/gif,image/bmp,image/tiff" />

					</div>

				</div>
            </div>
           </div>
			<div style="text-align: center; margin-bottom: 10px">
				<button id="user_submit" class="btn btn-sm btn-primary save"
					type="submit">
					<i class="fa fa-angle-right"></i>
					<spring:message code="button.save" />
				</button>
				<a href="<%=contexturl %>ManageSponsoredImage/SponsoredImageList/" class="btn btn-sm btn-primary"
					style="text-decoration: none;"><i class="gi gi-remove"></i> <spring:message
						code="button.cancel" /></a>
			</div>
		
	</form>
	


</div>



<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>

<script type="text/javascript"
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCheQF_yjKacwuVL4_59EO-3BThy85gbu4&sensor=false&region=GB">
	
</script>

<script type="text/javascript">
	$(document)
			.ready(
					function(){
						$("#manageSponsoredImage").addClass("active");
						$(".module_chosen").data("placeholder","Select Module From...").chosen();
						$("#Module_form").validate(
								{	errorClass:"help-block animation-slideDown",
									errorElement:"div",
									errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
									highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
									success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
												rules : {
													chosenModuleId: {
														required : !0
													},
													linkName: {
														required : !0,
														urlTrim :!0
													}
													
												},
												messages : {
													chosenModuleId: {
														required : '<spring:message code="validation.selectmodule"/>'
													},
													linkName : {
														required : '<spring:message code="validation.selectalink"/>',
														urlTrim : '<spring:message code="validation.pleaseenteravalidurl"/>'
													}
												},
										});
										
								$("#module_submit").click(function() {
									$("#Module_form").submit();
								});
					
	
	});
</script>

<script type="text/javascript">
	function OpenFileDialogInstructor(){
		document.getElementById("imageChooserProfile").click();
	}
	
	function changePhoto(){
		if($('#imageChooserProfile').val() != ""){			
			var imageName = document.getElementById("imageChooserProfile").value;
			var ext1 = imageName.substring(imageName.lastIndexOf("."));
			var ext = ext1.toLowerCase();
			if(ext == '.jpg' || ext == '.jpeg')
				$('#changePhoto').submit();		
			else
				alert("Please select only image file");		
		}
	}
</script>


<script type="text/javascript">
    function showimages(files) {
        for (var i = 0, f; f = files[i]; i++) {
           
                       
            var reader = new FileReader();
            reader.onload = function (evt) {
            	$( "#image" ).remove();
            	$( "#imagediv" ).remove();
            	
//                 var img = '<div class="col-md-6 id="imagediv"><img border="0" src="' + evt.target.result + '" width="120" height="120" title="Click here to change photo" style="cursor: pointer;"  onclick="OpenFileDialogInstructor();" class="img-circle pic" id="image"/></div>';
                  var img ='<div class="col-md-6 img-list2" id="imagediv"><img border="0" src="' + evt.target.result + '" width="120" height="120" title="Click here to change photo" onclick="OpenFileDialogInstructor();"  id="image" style="cursor: pointer;" /><span class="text-content" onclick="OpenFileDialogInstructor();" title="Click here to change photo"><br><br><span>Change Sponsored Image</span></span></div>';
                 $('#profilepic').append(img);
            };
            reader.readAsDataURL(f);
        }
    }

    $('#imageChooserProfile').change(function (evt) {
        showimages(evt.target.files);
    });
</script>


<script
	src="<%=contexturl%>resources/js/pages/activeTablesDatatables.js"></script>
<script
	src="<%=contexturl%>resources/js/pages/tablesDatatables.js"></script>


<style>
.chosen-container {
	width: 250px !important;
}
#map-canvas {
	height: 300px
}
</style>


<%@include file="../../inc/template_end.jsp"%>