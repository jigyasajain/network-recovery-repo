<%@include file="../../inc/config.jsp"%>
<div class="menucategory" style="width: 100% !important;">
	<div id="asd" style="margin-left: 16px;" class="btn btn-sm btn-primary"><a href="MenuList?id=${outletId}&menuType=ItemMenu" style=" text-decoration: none;color:white; !important "><spring:message code="label.goBack"/></a></div>
	<div class="form-group" style="margin-top: 15px; margin-bottom: 50px;">
		<label class="col-md-3 control-label themed-color-autumn" for="category"><strong><spring:message code="label.menucategory"/></strong></label>
		<div class="col-md-9">
			<label class="col-md-3 control-label themed-color-autumn" for="category"><strong>${menuCategory.menuCategoryName}</strong></label>
		</div>
	</div>
	<div class="form-group" style="width: 100% !important;">
		<ul id="itemView_${menuCategory.menuCategoryId}" style="width: 100% !important;"></ul>
		<form action="" id="menuItemForm_${menuCategory.menuCategoryId}">
			<input type="hidden" name="id" value="${outletId}" />
			<input type="hidden" name="category" value="${menuCategory.menuCategoryId}">
			<div class="form-group">
				<div class="col-md-2">
					<input id="menuItem_${menuCategory.menuCategoryId}" name="menuItem_${menuCategory.menuCategoryId}" class="form-control" placeholder="<spring:message code="label.menuitem"/>..."	type="text">
				</div>
			</div>
			<div class="form-group"> 
				<div class="col-md-2">
					<input id="description_${menuCategory.menuCategoryId}" name="description_${menuCategory.menuCategoryId}" class="form-control" placeholder="<spring:message code="label.description"/>..."	type="text">
				</div>
			</div>
			<div class="form-group"> 
				<div class="col-md-1" style="margin-top: 7px; width: 0px !important; margin-right: -10px;">
					${currency.currencySign} 
				</div>
				<div class="col-md-2">
					<input id="rate_${menuCategory.menuCategoryId}" name="rate_${menuCategory.menuCategoryId}" class="form-control" placeholder="<spring:message code="label.rate"/>.."	type="text">
				</div>
			</div>
			<div class="col-md-2">
				<div id="addMenu_${menuCategory.menuCategoryId}" class="btn btn-sm btn-primary"><spring:message code="label.addmenu"/></div>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript">
	var menuCategoryCount = 1;
	var menuItemCount = 1;
	$(document).ready(function() {
		$("itemView_${menuCategory.menuCategoryId}").sortable();
		$("itemView_${menuCategory.menuCategoryId}").disableSelection();
		
		$("#menuItemForm_${menuCategory.menuCategoryId}").validate(
				{	errorClass:"help-block animation-slideDown",
					errorElement:"div",
					errorPlacement:function(e,a){a.parents(".form-group > div ").append(e)},
					highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
					success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
					rules:{ 
						menuItem_${menuCategory.menuCategoryId}:{required:!0,maxlength : 75},
						description_${menuCategory.menuCategoryId}:{maxlength : 75},
						rate_${menuCategory.menuCategoryId}:{required:!0,maxlength : 10,digits:!0}
					},
					messages:{
						menuItem_${menuCategory.menuCategoryId}:{required:'<spring:message code="validation.pleaseentermenuitem"/>',
						                                         maxlength:'<spring:message code="validation.menuitem75character"/>'},
						description_${menuCategory.menuCategoryId}:{maxlength:'<spring:message code="validation.description75character"/>'},
						rate_${menuCategory.menuCategoryId}:{required:'<spring:message code="validation.pleaseenterrateofitem"/>',
							                                 maxlength : '<spring:message code="validation.cost10character"/>',
							                                 digits:'<spring:message code="validation.pleaseenterrateindigit"/>'}
					},
					submitHandler: function() {
						var data = $("#menuItemForm_${menuCategory.menuCategoryId}").serialize();
						$.ajax({
							type: "POST",
							async: false,
							url: "<%=contexturl %>ManageRestaurant/RestaurantList/SaveMenuItem",
							data:data,
							success: function(data, textStatus, jqXHR){
								var i = data.trim().indexOf("errorCode");
								if(i == 2){
									var obj = $.parseJSON(data.trim());
									var html = '<div class="alert alert-danger alert-dismissable">	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="fa fa-times-circle"></i> Error</h4> <span >'+obj.errorCode+'</span></div>';
									$("#errorMsg").html(html);
								}
								else{
									$("#menuItem_${menuCategory.menuCategoryId}").val("");
									$("#description_${menuCategory.menuCategoryId}").val("");
									$("#rate_${menuCategory.menuCategoryId}").val("");
									$('#itemView_${menuCategory.menuCategoryId}').append(data);
								}
							},
							dataType: 'html'
						});
	                }
				});
		
		 $("#addMenu_${menuCategory.menuCategoryId}").click(function(){
			 $("#menuItemForm_${menuCategory.menuCategoryId}").submit();
		 });
		
	});
</script>