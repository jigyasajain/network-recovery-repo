
<div id="page-content">
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-coins"></i> <spring:message code="heading.mypayments"/><br> <small><spring:message code="heading.specifylistofpayments"/>
                    </small>
            </h1>
            <c:if test="${!empty success}">
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert"
                        aria-hidden="true">x</button>
                    <h4>
                        <i class="fa fa-check-circle"></i> <spring:message code="label.success"/>
                    </h4>
                    <spring:message code="${success}" />
                </div>
            </c:if>
            <c:if test="${!empty error}">
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert"
                        aria-hidden="true">x</button>
                    <h4>
                        <i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
                    </h4>
                    <spring:message code="${error}" />
                </div>
            </c:if>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><spring:message code="heading.mypayments"/></li>
    </ul>
    <div class="block full" id="gridView">
        <div class="block-title">
            <h2>
                <strong><spring:message code="heading.payments"/></strong>
            </h2>
        </div>
    <div class="table-responsive">
            <table id="active-datatable"
                class="table table-vcenter table-condensed table-bordered">
                <thead>
                    <tr>
                        <th class="text-center"><spring:message code="label.id"/></th>
                        <th class="text-center"><spring:message code="label.date"/></th>
                        <th class="text-center"><spring:message code="label.currency"/></th>
                        <th class="text-center"><spring:message code="label.amount"/></th>
                        <th class="text-center"><spring:message code="label.paidamount"/></th>
                        <th class="text-center"><spring:message code="label.discountamount"/></th>
                        <th class="text-center"><spring:message code="label.tax"/></th>
                        <c:if test="${visitor.role.id eq CORPORATE}"><th class="text-center"><spring:message code="label.noofusers"/></th></c:if>
                  </tr>
                </thead>
                <tbody id="tbody">
                    <c:forEach var="currentMembershipPayment" items="${membershipPaymentsList}"
                        varStatus="count">

                        <tr>
                            <td class="text-center">${count.count}</td>
                            <td class="text-right"><fmt:formatDate pattern="<%=propvalue.simpleDatetimeFormat %>"  value="${currentMembershipPayment.transactionDate}" /></td>
                            <td class="text-left"><c:out  value="${currentMembershipPayment.paymentCurrency.currencyName}" /> </td>
                            <td class="text-right"><c:out  value="${currentMembershipPayment.paymentCurrency.currencySign}" /><c:out  value="${currentMembershipPayment.totalAmount}" /> </td>
                            <td class="text-right"><c:out  value="${currentMembershipPayment.paymentCurrency.currencySign}" /><c:out  value="${currentMembershipPayment.transactionAmount}" /> </td>
                            <td class="text-right"><c:out  value="${currentMembershipPayment.paymentCurrency.currencySign}" /><c:out  value="${currentMembershipPayment.totalDiscount}" /> </td>
                            <td class="text-right"><c:out  value="${currentMembershipPayment.paymentCurrency.currencySign}" /><c:out  value="${currentMembershipPayment.taxAmount}" /> </td>
                        <c:if test="${visitor.role.id eq CORPORATE}">   <td class="text-center"><c:out  value="${currentMembershipPayment.numberOfUsers}" /> </td></c:if>
                            
                        </tr>
                    </c:forEach>

                </tbody>
            </table>
        </div>
    </div>
</div>

