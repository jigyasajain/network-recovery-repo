<div class="form-group">
		<label class="col-md-3 control-label" for="category">${menuItem.name }</label>
		<label class="col-md-3 control-label" for="category">${menuItem.description }</label>
		<label class="col-md-3 control-label" for="category">${currency.currencySign} ${menuItem.rate } </label>
		<div class="col-md-2">
			<a href="#delete_menuItem_popup" data-toggle="modal" 
				onclick="deleteMenuItem(${menuItem.itemId},1)" title="Delete Menu Item"
				class="btn btn-xs btn-danger"><i class="fa fa-times"></i>
			</a>
		</div>
</div>
