<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>
<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				<i class="fa fa-map-marker"></i>
				<spring:message code="heading.category" />
				<br> <small><spring:message
						code="heading.categorydetails" /></small>
			</h1>
			<span id="errorMsg"></span>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i> <spring:message code="label.success"/>
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>

			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">�</button>
					<h4>
						<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li>You can change your details here</li>
		<li><a href="<%=contexturl %>ManageCategory/CategoryList/EditCategory?id=${category.categoryId}" id="brand">Edit Category</a></li>
	</ul>
	<form action="<%=contexturl %>ManageCategory/CategoryList/UpdateCategory" method="post" class="form-horizontal "
		id="Company_form" enctype="multipart/form-data">
		<div class="row">
			
			<div class="col-md-6">
				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.categoryinfo" /></strong>
						</h2>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="category_Name"><spring:message
								code="label.categoryname" /> <span class="text-danger">*</span>
						</label>
						<div class="col-md-6">

							<input id="category_Name" name="categoryName" class="form-control"
								placeholder="<spring:message code='label.categoryname'/>.."
								type="text" value="${category.categoryName}" disabled="disabled">
						</div>
					</div>

					<div class="form-group">
							<label class="col-md-4 control-label" for="description"><spring:message
								code="label.description" /></label>
							<div class="col-md-6">
								<textarea id="description" name="description" class="form-control"
									   placeholder="Description.."  disabled="disabled">${category.description}</textarea>
							</div>
					</div>

					<div class="form-group" id="categoryDiv">
						<label class="col-md-4 control-label" for="module"><spring:message
								code="label.modulename" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<select class="module_chosen" style="width: 200px;"
								id="chosenModuleId"
								data-placeholder="<spring:message code='label.choosemodule' />"
								name="chosenModuleId" disabled="disabled">
								<c:forEach items="${moduleList}" var="country">
									<option value="${module.moduleId}"
										<c:if test="${module.moduleId eq category.module.moduleId}">selected="selected"</c:if>>${module.moduleName}</option>
								</c:forEach>
							</select>
						</div>
					</div>


				</div>

			</div>

					
				</div>
			
			</div>
		</div>
	
	</form>

	
</div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<script src="<%=contexturl %>resources/js/custom/featurecontrol.js"></script>
<script
	src="../resources/js/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>

