<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>

<style>
.chosen-container {
    width: 250px !important;
}
</style>

<div id="page-content">
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-cup"></i>Membership Details <br>
            </h1>
            <span id="errorMsg"></span>
            <c:if test="${!empty success}">
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert"
                        aria-hidden="true">x</button>
                    <h4>
                        <i class="fa fa-check-circle"></i> <spring:message code="label.success"/>
                    </h4>
                    <spring:message code="${success}" />
                </div>
            </c:if>
            <c:if test="${!empty error}">
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert"
                        aria-hidden="true">x</button>
                    <h4>
                        <i class="fa fa-times-circle"></i>
                        <spring:message code="label.error" />
                    </h4>
                    <spring:message code="${error}" />
                </div>
            </c:if>
        </div>
    </div>
    <c:if test="${membership==null}">
    <c:if test="${!empty firstTimeLogin}">
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert"
                aria-hidden="true">x</button>
            <h4>
                 Congratulations Foodie !!!!
            </h4>
            You are now a free member of Gourmet7. Click on Add Membership to purchase a plan.
        </div>
    </c:if> 
    <div style="margin-top: 7%; margin-left: 15%; position: relative;">
        <a href="#currency_popup" style="font-size: 30px;" data-toggle="modal" class="btn btn-sm btn-primary">Add Membership Plan</a> 
        OR <a href="#corporate_popup" style="font-size: 30px;" data-toggle="modal" class="btn btn-sm btn-primary" >Upgrade to Corporate Plan</a>
    </div>
    </c:if>
    <c:if test="${!empty membership}">
    <form action="" method="post" class="form-horizontal">
        <div class="row">
            <div class="col-md-4">
                <div class="block">
                    <div class="block-title">
                        <h2>
                            <strong>Membership</strong>
                        </h2>
                    </div>
                    <div class="form-group">
                        <label class="col-md-6 control-label">Activation Date</label>
                        <div class="col-md-6">
                            <p class="form-control-static"><fmt:formatDate pattern="<%=propvalue.specialDateFormat %>"  value="${membership.activationDate}" /></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-6 control-label">Expiry Date</label>
                        <div class="col-md-6">
                            <p class="form-control-static"><fmt:formatDate pattern="<%=propvalue.specialDateFormat %>"  value="${membership.expiryDate}" /></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-6 control-label">Tenure In Month</label>
                        <div class="col-md-6">
                            <p class="form-control-static">${membership.tenureInMonth}</p>
                        </div>
                    </div>
                    <c:if test="${membership.membershipType.id eq  TYPE_INDIVIDUAL}">
                        <div class="form-group">
                        
                            <label class="col-md-6 control-label">Number Of Users</label>
                            <div class="col-md-6">
                                <p class="form-control-static">${membership.numberOfUsers}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-6 control-label">Discount Value</label>
                            <div class="col-md-6">
                                <p class="form-control-static">${membership.paymentCurrency.currencySign} ${membership.discountValue}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-6 control-label">Total Amount Paid</label>
                            <div class="col-md-6">
                                <p class="form-control-static">${membership.paymentCurrency.currencySign} ${membership.totalAmountPaid}</p>
                            </div>
                        </div>
                    </c:if>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="block">
                    <div class="block-title">
                        <h2>
                            <strong>Selected Location</strong>
                        </h2>
                    </div>
                    <c:forEach var="country" items="${membership.countryDetails}"
                        varStatus="count">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="row ">
                                    <div class="col-xs-3">
<!--                                        <label class="switch switch-primary"> <input -->
<!--                                            name="country" type="checkbox" -->
<%--                                            <c:if test="${country.key.selected}"> checked </c:if> disabled="disabled"/> <span></span> --%>
<!--                                        </label> -->
                                <c:choose> <c:when test="${country.key.selected}"><label><span class="text-success hi hi-ok"></span></label> </c:when> <c:otherwise><span class="text-danger hi hi-remove"></span> </c:otherwise></c:choose>


                                    </div>
                                    <div class="col-xs-8">
                                        <h4 class="panel-title">
                                            <i class="fa fa-angle-right"></i> <a class="accordion-toggle"
                                                data-toggle="collapse" data-parent="#country" href="#country_${country.key.name}">
                                                ${country.key.name} </a>
                                        </h4>
                                    </div>
                                </div>
                            </div>
                            <div id="country_${country.key.name}" class="panel-collapse collapse in"
                                style="height: auto;">
                                <c:forEach var="city" items="${country.value}"
                                    varStatus="count2">
                                    <div class="panel-body">
                                        <div class="row ">
                                            <div class="col-xs-3">
                                            
                                            <c:choose> <c:when test="${country.key.selected}"><label><span class="text-success hi hi-ok"></span></label> </c:when>
                                         <c:when test="${city.selected}"><label><span class="text-success hi hi-ok"></span></label> </c:when>
                                         <c:otherwise><span class="text-danger hi hi-remove"></span> </c:otherwise></c:choose>
                                         <span></span>
                                                
                                            </div>
                                            
                                            <div class="col-xs-8">
                                                <p>${city.name}</p>
                                            </div>
                                        </div>
                                    </div>
                                </c:forEach>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="block">
                    <div class="block-title">
                        <h2>
                            <strong>Selected Brands</strong>
                        </h2>
                    </div>
                    <c:forEach var="brand" items="${membership.brandDetails}" varStatus="count">
                        <div class="row ">
                            <div class="col-xs-3">


                                <c:choose> <c:when test="${brand.selected}"><label><span class="text-success hi hi-ok"></span></label> </c:when> <c:otherwise><span class="text-danger hi hi-remove"></span> </c:otherwise></c:choose>

                            </div>
                            <div class="col-xs-8">
                                <p>${brand.name}</p>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </div>
        <div class="form-group form-actions">
            <div class="col-md-9 col-md-offset-3">
                
                
                <a class="btn btn-sm btn-primary" href="<%=contexturl%>ManageMembers/ShowMemberList/${member.user.memberType.id}"><i
                    class="gi gi-remove"></i> <spring:message code="button.back" />
                </a>
            </div>
        </div>
    </form>
    </c:if>
    
<div id="currency_popup" class="modal fade" tabindex="-1" role="dialog"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">
                    Select Currency
                </h2>
            </div>
            <div class="modal-body">
                <form action="SaveCurrencyInSession" method="post"
                    id="currency_form" class="form-horizontal form-bordered">
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="currency"><spring:message
                                    code="label.currency" /><span class="text-danger">*</span> </label>
                            <div class="col-md-6">
                                <select class="currency_chosen" style="width: 200px;"
                                    id="currency" name="currencyId" data-placeholder="<spring:message code='label.choosecurrency' />">
                                    <option value=""></option>
                                    <c:forEach items="${currencyList}" var="currency">
                                        <c:choose>
                                            <c:when test="${currency.active == true}">
                                                <option value="${currency.currencyId}">${currency.currencyName}</option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="placeholder">..</option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                    </fieldset>
                    <div class="form-group form-actions">
                        <div class="col-xs-12 text-right">
                            <button type="button" class="btn btn-sm btn-default"
                                data-dismiss="modal" onClick="window.location.reload()">
                                <spring:message code="label.close" />
                            </button>
                            <div id="currency_submit" class="save btn btn-sm btn-primary">
                                <spring:message code="button.submit" />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>
    
<div id="corporate_popup" class="modal fade" tabindex="-1" role="dialog"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">
                    <i class="fa fa-pencil"></i>
                    Upgrade Membership
                </h2>
            </div>
            <div class="modal-body">
                <form action="<%=contexturl %>UpgradeToCorporate" method="post"
                    id="corporate_form" class="form-horizontal form-bordered">
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="currency">Corporate Code <span class="text-danger">*</span> </label>
                            <div class="col-md-4">
                                <input type="text" name="code" id="corporateCode" class="form-control">
                            </div>
                        </div>
                    <div class="form-group form-actions">
                        <div class="col-xs-12 text-right">
                            <button type="button" class="btn btn-sm btn-default"
                                data-dismiss="modal">
                                <spring:message code="button.cancel" />
                            </button>
                            <button type="submit" id="code_submit" class="btn btn-sm btn-primary">
                                <spring:message code="button.submit" />
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>


<div id="corporate_confirm_popup" class="modal fade" tabindex="-1" role="dialog"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                    <div class="alert alert-warning alert-dismissable">
                        <i class="fa fa-exclamation-circle"></i> <spring:message code="alert.updateconfirmation" />
                    </div>
                    <div>Do you want to continue?</div>
                    <div class="form-group form-actions">
                        <div class="col-xs-12 text-right">
                            <button type="button" class="btn btn-sm btn-default"
                                data-dismiss="modal">
                                <spring:message code="label.no" />
                            </button>
                            <div id="upgrade_to_corporate" class="btn btn-sm btn-primary" data-dismiss="modal">
                                <spring:message code="label.yes" />
                            </div>
                        </div>
                    </div>
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>
</div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>

<script type="text/javascript">
    $(document).ready(function() {
        $(".currency_chosen").chosen();

        $("#currency_submit").click(function() {
            $("#currency_form").submit();
        });
        
        $("#upgrade_to_corporate").click(function(){
            $("#Upgrade_Corporate" ).trigger( "click" );
        });
        
        $("#corporate_form").validate(
                {   errorClass:"help-block animation-slideDown",
                    errorElement:"div",
                    errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
                    highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
                    success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
                    rules:{ code:{required:!0}
                    },
                    messages:{code:{required:'<spring:message code="validation.corporatecode"/>'}
                    },
                
                });
        
        $("#currency_form").validate(
                {   errorClass:"help-block animation-slideDown",
                    errorElement:"div",
                    errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
                    highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
                    success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
                    rules:{ currencyId:{required:!0}
                    },
                    messages:{
                        currencyId:{required:'<spring:message code="validation.currency"/>'}
                    },
                    
                });
        
      $("#currency_submit").click(function(){
         $("#currency_form").submit();
     });  
      
    });
</script>

<%@include file="../../inc/template_end.jsp"%>