<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>
<link rel="stylesheet" type="text/css" href="<%=contexturl %>resources/css/jquery.cleditor.css" />
<style>
.cleditorToolbar {
	background: url('<%=contexturl %>resources/img/cleditor/toolbar.gif') repeat
}

.cleditorButton {
	float: left;
	width: 24px;
	height: 24px;
	margin: 1px 0 1px 0;
	background: url('<%=contexturl %>resources/img/cleditor/buttons.gif')
}

.cleditorMain {
	height: 250px !important
}
</style>
<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				<i class="fa fa-map-marker"></i>
				<spring:message code="heading.coursecategorymaster" />
				<br> <small><spring:message
						code="heading.coursecategorymasterdetails" /></small>
			</h1>
			<span id="errorMsg"></span>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i> <spring:message code="label.success"/>
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>

			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">�</button>
					<h4>
						<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="heading.course" /></li>
	</ul>
	
		<div class="col-md-12">
				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.courseinfo" /></strong>
						</h2>
					</div>
		<form action="<%=contexturl %>ManageCourse/CourseList/UpdateCourse" method="post" class="form-horizontal ui-formwizard"
		id="Course_form" enctype="multipart/form-data">
		
					<input id="courseId" name="courseId" type="hidden" value="${course.courseId}">
					<input id="confirmation" name="confirmation" type="hidden" value="false">
					<input id="publish" name="publish" type="hidden" value="false">
					<div class="form-group">
						<label class="col-md-3 control-label" for="course_Name"><spring:message
								code="label.coursename" /><span class="text-danger">*</span> </label>
						<div class="col-md-7">

							<input id="courseName" name="courseName" class="form-control"
								placeholder="<spring:message code='label.coursename'/>.."
								type="text" value="${course.courseName}">
						</div>
					</div>
					
					<c:choose>
							<c:when test="${course.previewImage!=null}">
								<div class="form-group">
									<label class="col-md-3 control-label" for="example-daterange1"><spring:message code="label.previewimage"/>
										</label>
									<div class="col-md-7">
										<img src="<%=contexturl %>${course.previewImage.imagePath}" width="150px"/>
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-md-3 control-label" for="example-daterange1"><spring:message code="label.previewimage"/>
										</label>
									<div class="col-md-7">
										<input type="file" name="previewImage" accept="image/*"/>
									</div>
								</div>
							</c:when>
							<c:otherwise >
								<div class="form-group">
									<label class="col-md-3 control-label" for="example-daterange1"><spring:message code="label.previewimage"/>
										</label>
									<div class="col-md-7">
										<input type="file" name="previewImage" accept="image/*"/>
									</div>
								</div>	
							</c:otherwise>
						</c:choose>
					<div class="form-group">
					<label class="col-md-3 control-label" for="course_duration"><spring:message
							code="label.introductoryvideo" /><span class="text-danger">*</span>
					</label>
					<div class="col-md-7">

						<input id="introductoryVideo" name="introductoryVideo"
							class="form-control"
							placeholder="<spring:message code='label.introductoryvideo'/>.."
							type="text" value="${course.introductoryVideo}">
					</div>
				</div>
					

				<div class="form-group" id="LanguageDiv">
					<label class="col-md-3 control-label" for="countryFrom_Name"><spring:message
							code="label.languagename" /><span class="text-danger">*</span></label>
					<div class="col-md-7">
						<select multiple class="Language_chosen" style="width: 400px;" id="fromLanguageId"
							name="chosenLanguageId"
							data-placeholder="<spring:message code='label.chooseLanguage' />">
							<option value=""></option>
							<c:forEach items="${languageList}" var="language">
										<c:set var="present" value="false" />
										<c:forEach items="${languages}" var="lang">
											<c:if test="${language.languageId eq lang.languageId}">
												<c:set var="present" value="true" />
											</c:if>
										</c:forEach>
										<option value="${language.languageId}"
											<c:if test="${present}">	selected="selected" </c:if>>
											${language.languageName}</option>
								
							</c:forEach>
						</select>
					</div>

				</div>
				
				<div class="form-group" id="CategoryDiv">
					<label class="col-md-3 control-label" for="countryFrom_Name"><spring:message
							code="label.coursecategoryname" /><span class="text-danger">*</span></label>
					<div class="col-md-7">
						<select multiple class="Category_chosen" style="width: 400px;" id="fromCategoryId"
							name="chosenCategoryId"
							data-placeholder="<spring:message code='label.choosecoursecategory' />">
							<option value=""></option>
							<c:forEach items="${categoryList}" var="category">
										<c:set var="present" value="false" />
										<c:forEach items="${categories}" var="categories">
											<c:if test="${category.categoryId eq categories.categoryId}">
												<c:set var="present" value="true" />
											</c:if>
										</c:forEach>
										<option value="${category.categoryId}"
											<c:if test="${present}">	selected="selected" </c:if>>
											${category.categoryName}</option>
							</c:forEach>
						</select>
					</div>

				</div>
				<div class="form-group" id="InstructorDiv">
					<label class="col-md-3 control-label" for="countryFrom_Name"><spring:message
							code="heading.instructor" /></label>
					<div class="col-md-7">
						<select multiple class="Instructor_chosen" style="width: 400px;" id="chosenInstructorId"
							name="chosenInstructorId"
							data-placeholder="<spring:message code='label.chooseinstructor' />">
							<option value=""></option>
							<c:forEach items="${instructorList}" var="instructor">
							<c:set var="present" value="false" />
										<c:forEach items="${instructors}" var="instructors">
											<c:if test="${instructor.user.userId eq instructors.user.userId}">
												<c:set var="present" value="true" />
											</c:if>
											
										</c:forEach>
										<option value="${instructor.user.userId}"
											<c:if test="${present}">	selected="selected" </c:if>>
											${instructor.user.firstName} ${ instructor.user.lastName}</option>
							</c:forEach>
						</select>
					</div>

				</div>
				
				<div class="form-group">
				<label class="col-md-3 control-label" for="courseType"><spring:message
						code="label.coursetype" /><span class="text-danger">*</span></label>
				<div class="col-md-7">
					<input id="courseType" name="courseType" class="form-control"
						placeholder="<spring:message code="label.coursetype"/>.." value="${course.courseType}" type="text">
				</div>
			</div>
				
					
					<div class="form-group">
					<label class="col-md-3 control-label" for="offer_termsCondition"><spring:message
							code="label.aboutcourse" /><span class="text-danger">*</span></label>
					<div class="col-md-9">
						<textarea id="aboutCourse" name="aboutCourse">${course.aboutCourse}</textarea>
						<label style="color: #e74c3c;" id="termsError"></label>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-md-3 control-label" for="offer_termsCondition"><spring:message
							code="label.objective" /><span class="text-danger">*</span></label>
					<div class="col-md-9">
						<textarea id="objective" name="objective">${course.objective}</textarea>
						<label style="color: #e74c3c;" id="termsError"></label>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-md-3 control-label" for="offer_termsCondition"><spring:message
							code="label.whythis" /><span class="text-danger">*</span></label>
					<div class="col-md-9">
						<textarea id="whythis" name="whythis">${course.whyThis}</textarea>
						<label style="color: #e74c3c;" id="termsError"></label>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-md-3 control-label" for="offer_termsCondition"><spring:message
							code="label.syllabus" /><span class="text-danger">*</span></label>
					<div class="col-md-9">
						<textarea id="syllabus" name="syllabus">${course.syllabus}</textarea>
						<label style="color: #e74c3c;" id="termsError"></label>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-3 control-label" for="course_duration"><spring:message
							code="label.courseduration" /><span class="text-danger">*</span>
					</label>
					<div class="col-md-7">

						<input id="courseDuration" name="courseDuration"
							class="form-control"
							placeholder="<spring:message code='label.entercoursedurationinnumber'/>.."
							type="text" value="${course.courseDuration}">
					</div>
				</div>
				
			<div class="form-group form-actions">
			
			
				<div class="col-md-9 col-md-offset-4">
						<c:choose>
							<c:when test="${course.status eq status}">
							<button id="course_submit" type="submit"
									class="btn btn-sm btn-primary save">
									<i class="fa fa-angle-right"></i>
									<spring:message code="button.save" />
								</button>
								<button id="course_save_submit" type="submit" style="display: none;"
									class="btn btn-sm btn-primary save">
									<i class="fa fa-angle-right"></i>
									<spring:message code="button.saveandsendConfirmation" />
								</button>

								<button id="publish_course" type="submit"
									class="btn btn-sm btn-primary save">
									<i class="fa fa-angle-right"></i>
									<spring:message code="button.publish" />
								</button>
							</c:when>
							<c:otherwise>
								
							</c:otherwise>
						</c:choose>
						<a class="btn btn-sm btn-primary" href="<%=contexturl %>ManageCourse/CourseList/"><i ></i>
						<spring:message code="button.back" /> </a>
				</div>
			</div>
			
	</form>
	</div>
	</div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<script type="text/javascript" src="<%=contexturl %>resources/js/jquery.cleditor.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$(".Language_chosen").data("placeholder","Select Language From...").chosen();
	$(".Category_chosen").data("placeholder","Select Category From...").chosen();
	$(".Instructor_chosen").data("placeholder","Select Instructor From...").chosen();
	$(".chosenTime").data("placeholder","Select Time Zone From...").chosen();
	$("#aboutCourse").cleditor(); 
	$("#objective").cleditor();
	$("#whythis").cleditor();
	$("#syllabus").cleditor();
	
	$("#Course_form").validate(
			{	errorClass:"help-block animation-slideDown",
				errorElement:"div",
				errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
				highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
				success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
				rules:{ courseName : {required : !0,maxlength : 75},
					introductoryVideo : {required : !0,maxlength : 100},
					courseType : {required : !0,maxlength : 75},
					aboutCourse : {maxlength : 5000},
					objective : {required : !0,maxlength : 5000},
					whythis : {maxlength : 5000},
					syllabus : {required : !0,maxlength : 5000},
					courseDuration :{required : !0,maxlength : 5,number :!0},
					chosenLanguageId : {required : !0},
					chosenCategoryId : {required : !0},
					
				},
				
			});
	
	
	 $("#course_submit").click(function(){
		 $("#Course_form").submit();
	 });
	 $("#course_save_submit").click(function(){
		$("#confirmation").val(true);
		$("#Course_form").submit(); 
	 });
	 
	 $("#publish_course").click(function(){
	     	
	     	$("#publish").val(true);
				$("#Course_form").submit();
		});
	 var value = $("#chosenInstructorId").val();
	 $("#chosenInstructorId" )
		.change(function() {
		  var value = $( this ).val();
		  if(value!=null){
			  $("#course_save_submit").show();
			  $("#publish_course").hide();
			  
			  
		  }else{
			  $("#course_save_submit").hide();
			  $("#publish_course").show();
			 
		  }
		});
	 if(value!=null){
		  $("#course_save_submit").show();
		  $("#publish_course").hide();
		  
		  
	  }else{
		  $("#course_save_submit").hide();
		  $("#publish_course").show();
		 
	  }
	
});
</script>

<%@include file="../../inc/template_end.jsp"%>