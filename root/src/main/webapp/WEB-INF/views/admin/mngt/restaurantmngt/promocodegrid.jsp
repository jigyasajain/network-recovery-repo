<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->

<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>

<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				<i class="fa fa-map-marker"></i> <spring:message code="heading.promocodeDescription"/><br> <small><spring:message code="heading.specifylistofpromoDescription"/>
					</small>
			</h1>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i> <spring:message code="label.success"/>
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="heading.managerestaurant"/></li>
		<li><a href=""><spring:message code="heading.promocodeDescription"/></a></li>
	</ul>
	<div class="block full" id="gridView">
		<div class="block-title">
			<h2>
				<strong><spring:message code="heading.promocodeDescription"/></strong>
			</h2>
		</div>
		<a href="AddPromoDescription?outletId=${outletId}" id="addPromoDescription" class="btn btn-sm btn-primary"><spring:message code="label.addPromoDescription"/></a>
		<div class="table-responsive">
			<table id="active-datatable"
				class="table table-vcenter table-condensed table-bordered">
				<thead>
					<tr>
						<th class="text-center"><spring:message code="label.id"/></th>
						<th class="text-center"><spring:message code="label.title"/></th>
						<th class="text-center"><spring:message code="label.validity"/></th>
						<th class="text-center"><spring:message code="label.expirydate"/></th>
						<th class="text-center"><spring:message code="label.actions"/></th>
					</tr>
				</thead>
				<tbody id="tbody">
					<c:forEach var="currentPromoDescription" items="${promoDescriptionList}"
						varStatus="count">

						<tr>
							<td class="text-center">${count.count}</td>
							<td class="text-left"><c:out value="${currentPromoDescription.title}" />
							</td>
							<td class="text-center">
<!-- 							<label class="switch switch-primary"><input -->
<!-- 									type="checkbox" -->
<%-- 									<c:if test="${currentPromoDescription.promocodeValidity}"> checked  </c:if> disabled="disabled" /><span></span> --%>
<!-- 							</label> -->
                                 <c:choose> <c:when test="${currentPromoDescription.promocodeValidity}"><label><span class="text-success hi hi-ok"></span></label> </c:when> <c:otherwise><span class="text-danger hi hi-remove"></span> </c:otherwise></c:choose>
							
							</td>
							<td class="text-right">
							<fmt:formatDate pattern="<%=propvalue.simpleDateFormat %>"  value="${currentPromoDescription.expiryDate}"/></td>
							<td class="text-center">
								<div class="btn-group">
									<a
										href="EditPromoDescription?id=${currentPromoDescription.promoDescriptionId}&outletId=${outletId}"
										data-toggle="tooltip" title="Edit"
										class="btn btn-xs btn-default"><i class="fa fa-pencil"></i>
									</a> <a href="#delete_promoDescription_popup" data-toggle="modal"
										onclick="deletePromoDescription(${currentPromoDescription.promoDescriptionId})" title="Delete"
										class="btn btn-xs btn-danger"><i class="fa fa-times"></i>
									</a>
								</div>
							</td>
						</tr>
					</c:forEach>

				</tbody>
			</table>
		</div>
	</div>
	
	<div class="block full" id="gridView">
		<div class="block-title">
			<h2>
				<strong><spring:message code="heading.expirepromocode"/></strong>
			</h2>
		</div>
		<div class="table-responsive">
			<table id="inactive-datatable"
				class="table table-vcenter table-condensed table-bordered">
				<thead>
					<tr>
						<th class="text-center"><spring:message code="label.id"/></th>
						<th class="text-center"><spring:message code="label.title"/></th>
						<th class="text-center"><spring:message code="label.validity"/></th>
						<th class="text-center"><spring:message code="label.expirydate"/></th>
						<th class="text-center"><spring:message code="label.actions"/></th>
					</tr>
				</thead>
				<tbody id="tbody">
					<c:forEach var="currentPromoDescription" items="${expiredDescriptionList}"
						varStatus="count">

						<tr>
							<td class="text-center">${count.count}</td>
							<td class="text-left"><c:out value="${currentPromoDescription.title}" />
							</td>
							<td class="text-center">
					 <c:choose> <c:when test="${currentPromoDescription.promocodeValidity}"><label><span class="text-success hi hi-ok"></span></label> </c:when> <c:otherwise><span class="text-danger hi hi-remove"></span> </c:otherwise></c:choose>

							</td>
							<td class="text-right">
							<fmt:formatDate pattern="<%=propvalue.simpleDateFormat %>"  value="${currentPromoDescription.expiryDate}" /></td>
							<td class="text-center">
								<div class="btn-group">
									<a
										href="EditPromoDescription?id=${currentPromoDescription.promoDescriptionId}&outletId=${outletId}"
										data-toggle="tooltip" title="Edit"
										class="btn btn-xs btn-default"><i class="fa fa-pencil"></i>
									</a> <a href="#delete_promoDescription_popup" data-toggle="modal"
										onclick="deletePromoDescription(${currentPromoDescription.promoDescriptionId})" title="Delete"
										class="btn btn-xs btn-danger"><i class="fa fa-times"></i>
									</a>
								</div>
							</td>
						</tr>
					</c:forEach>

				</tbody>
			</table>
		</div>
	</div>
	
	
	<div id="delete_promoDescription_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="DeletePromoDescription" method="post" class="form-horizontal form-bordered"
						id="delete_promoDescription_Form">
						<div style="padding: 10px; height: 110px;">
							<label><spring:message code="validation.doyouwanttodeletepromocode"/></label>
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal"><spring:message code="label.no"/></button>
								<div id="delete_promoDescriptionYes" class="btn btn-sm btn-primary"><spring:message code="label.yes"/></div>
								<input type="hidden" name="outletId" id="outletId">
								<input type="hidden" name="id" id="promoDescriptionId">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>

<script src="<%=contexturl %>resources/js/pages/activeTablesDatatables.js"></script>
<script src="<%=contexturl %>resources/js/pages/inactiveTablesDatatables.js"></script>
<script>$(function(){ ActiveTablesDatatables.init(4); });</script>
<script>$(function(){ InActiveTablesDatatables.init(4); });</script>
<script type="text/javascript">
	var selectedId = 0;
	$(document).ready(function() {
		
		$("#delete_promoDescriptionYes").click(function(){
			$("#outletId").val(${outletId});
			$("#promoDescriptionId").val(selectedId);
			$("#delete_promoDescription_Form").submit();
		});
		
	});
	
	function deletePromoDescription(id){
		selectedId = id;
	}
</script>
<%@include file="../../inc/template_end.jsp"%>