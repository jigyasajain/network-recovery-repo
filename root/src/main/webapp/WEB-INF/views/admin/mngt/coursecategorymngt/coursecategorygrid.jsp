<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>



<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				<i class="fa fa-map-marker"></i>
				<spring:message code="heading.coursecategorymaster" />
				<br> <small><spring:message
						code="heading.listofcoursecategory" /> </small>
			</h1>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i>
						<spring:message code="label.success" />
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>
			<div class="alert alert-danger alert-dismissable"
				style="display: none" id="error">
				<button type="button" class="close" data-dismiss="alert"
					aria-hidden="true">x</button>
				<h4>
					<i class="fa fa-times-circle"> </i> <spring:message code="label.error"/>
				</h4>
				<span id="errorMsg"></span>
			</div>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="heading.managecoursecategory" /></li>
		<li><a href="#"><spring:message code="label.coursecategory" /></a></li>
	</ul>
	<div class="block full  gridView">
		<div class="block-title">
			<h2>
				<strong><spring:message code="heading.activecoursecategory" /></strong>
			</h2>
		</div>
		<a href="<%=contexturl %>ManageCourse/CourseCategoryList/AddCourseCategory" id="addCompany" class="btn btn-sm btn-primary"><spring:message
				code="label.addcoursecategory" /></a>
		<!--<p><a href="https://datatables.net/" target="_blank">DataTables</a> is a plug-in for the Jquery Javascript library. It is a highly flexible tool, based upon the foundations of progressive enhancement, which will add advanced interaction controls to any HTML table. It is integrated with template's design and it offers many features such as on-the-fly filtering and variable length pagination.</p> -->
		<div class="table-responsive">
			<table id="active-datatable"
				class="table table-vcenter table-condensed table-bordered">
				<thead>
					<tr>
						<th class="text-center"><spring:message code="label.id" /></th>
						<th class="text-center"><spring:message code="label.coursecategoryname" /></th>
						<th class="text-center"><spring:message code="label.coursecategoryshortname" /></th>
<%-- 						<th class="text-center"><spring:message code="label.admin" /></th> --%>
<%-- 						<th class="text-center"><spring:message code="label.country" /></th> --%>
						<th class="text-center"><spring:message code="label.actions" /></th>
					</tr>
				</thead>
				<tbody id="tbody">
					<c:forEach var="CurrentCourseCategory" items="${activeList}"
						varStatus="count">

						<tr>
							<td class="text-center">${count.count}</td>
							<td class="text-left"><c:out
									value="${CurrentCourseCategory.categoryName}" /></td>
							<td class="text-left"><c:out
									value="${CurrentCourseCategory.shortName}" /></td>
<%-- 							<td class="text-left"><c:out --%>
<%-- 									value="${currentCompany.companyAdmin.fullName}" /></td> --%>
<%-- 							<td class="text-left"><c:out --%>
<%-- 									value="${currentUniversity.country}" /></td> --%>
							<td class="text-center">
								<div class="btn-group">
									<a href="<%=contexturl %>ManageCourse/CourseCategoryList/EditCourseCategory?id=${CurrentCourseCategory.categoryId}"
										data-toggle="tooltip" title="Edit"
										class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
									<a href="#delete_course_category_popup" data-toggle="modal"
										onclick="deleteCategory(${CurrentCourseCategory.categoryId})"
										title="Delete" class="btn btn-xs btn-danger"><i
										class="fa fa-times"></i></a>
								</div>
							</td>
						</tr>
					</c:forEach>

				</tbody>
			</table>
		</div>
	</div>


	<div class="block full  gridView">
		<div class="block-title">
			<h2>
				<strong><spring:message code="heading.activecoursecategory" /></strong>
			</h2>
		</div>
		<div class="table-responsive">
			<table id="inactive-datatable"
				class="table table-vcenter table-condensed table-bordered ">
				<thead>
					<tr>
						<th class="text-center"><spring:message code="label.id" /></th>
						<th class="text-center"><spring:message code="label.coursecategoryname" /></th>
						<th class="text-center"><spring:message code="label.coursecategoryshortname" /></th>
<%-- 						<th class="text-center"><spring:message code="label.admin" /></th> --%>
<%-- 						<th class="text-center"><spring:message code="label.country" /></th> --%>
						<th class="text-center"><spring:message code="label.actions" /></th>
					</tr>
				</thead>
				<tbody id="tablebody">
					<c:forEach var="CurrentCourseCategory" items="${inActiveList}"
						varStatus="count">
						<tr>
							<td class="text-center">${count.count}</td>
							<td class="text-left"><c:out
									value="${CurrentCourseCategory.categoryName}" /></td>
							<td class="text-left"><c:out
									value="${CurrentCourseCategory.shortName}" /></td>
<%-- 							<td class="text-left"><c:out --%>
<%-- 									value="${currentCompany.companyAdmin.fullName}" /></td> --%>
<%-- 							<td class="text-left"><c:out --%>
<%-- 									value="${currentUniversity.country}" /></td> --%>
							<td class="text-center">
								<div class="btn-group">
									<a href="#restore_Company_popup" data-toggle="modal"
										onclick="restoreCategory(${CurrentCourseCategory.categoryId})"
										title="Restore" class="btn btn-xs btn-success"><i
										class="fa fa-plus"></i> </a>
								</div>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	<div id="delete_course_category_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="<%=contexturl %>ManageCourse/CourseCategoryList/DeleteCourseCategory" method="post" class="form-horizontal form-bordered"
						id="delete_category_Form">
						<div style="padding: 10px; height: 110px;">
							<label><spring:message
									code="validation.doyouwanttodeletethiscoursecategory" /></label><br>
							
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal">
									<spring:message code="label.no" />
								</button>
								<div id="delete_category" class="btn btn-sm btn-primary">
									<spring:message code="label.yes" />
								</div>
							</div>
						</div>
						<input type="hidden" name="id" id="deletecategoryId">

					</form>

				</div>
			</div>
		</div>
	</div>


	<div id="restore_Company_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="<%=contexturl %>ManageCourse/CourseCategoryList/RestoreCourseCategory" method="post" class="form-horizontal form-bordered" id="restore_category_Form">
						
						<div style="padding: 10px; height: 110px;">
							<label><spring:message
									code="validation.doyouwanttorestorethiscoursecategory" /></label><br>
							
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal"><spring:message code="label.no" /></button>
								<div id="restore_Category" class="btn btn-sm btn-primary">	<spring:message code="label.yes" /></div>
							</div>
						</div>
						<input type="hidden" name="id" id="restorecategoryId">
					</form>
				</div>
			</div>
		</div>
	</div>

</div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<script
	src="<%=contexturl%>resources/js/pages/activeTablesDatatables.js"></script>
<script
	src="<%=contexturl%>resources/js/pages/inactiveTablesDatatables.js"></script>
<script>$(function(){ ActiveTablesDatatables.init(2); });</script>
<script>$(function(){ InActiveTablesDatatables.init(2); });</script>
<script type="text/javascript">
	var selectedId=0;
	var restoreId=0;
	$(document).ready(function() {
		
		
		
	
		
        $("#restore_Category").click(function(){
			$("#restorecategoryId").val(restoreId);
			$("#restore_category_Form").submit();
		});
		
		
		$("#delete_category").click(function(){
			$("#deletecategoryId").val(selectedId);
			$("#delete_category_Form").submit();
		});
	});
	
	function deleteCategory(id){
		selectedId = id;
	}
	function restoreCategory(id){
		restoreId = id;
	}
</script>
<%@include file="../../inc/template_end.jsp"%>