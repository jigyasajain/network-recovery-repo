<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>
<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				<i class="fa fa-map-marker"></i>
				<spring:message code="heading.module" />
				<br> <small><spring:message
						code="heading.moduledetails" /></small>
			</h1>
			<span id="errorMsg"></span>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i> <spring:message code="label.success"/>
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>

			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">�</button>
					<h4>
						<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li>You can change your details here</li>
		<li><a href="<%=contexturl %>ManageModule/ModuleList/EditModule?id=${module.moduleId}" id="brand">Edit Module</a></li>
	</ul>
	<form action="<%=contexturl %>ManageModule/ModuleList/UpdateModule" method="post" class="form-horizontal "
		id="Module_form" enctype="multipart/form-data">
		<div class="row">
			
			<div class="col-md-6">
				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.moduleinfo" /></strong>
						</h2>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="module_Name"><spring:message
								code="label.modulename" /> <span class="text-danger">*</span>
						</label>
						<div class="col-md-6">

							<input id="module_Name" name="moduleName" class="form-control"
								placeholder="<spring:message code='label.modulename'/>.."
								type="text" value="${module.moduleName}" disabled="disabled">
						</div>
					</div>
<div class="block">
					
					<div class="form-group">
						<label class="col-md-4 control-label" for="moduleDescription"><spring:message
								code="label.description" /></label>
						<div class="col-md-6">

							<input id="moduleDescription" name="moduleDescription"
								class="form-control"
								placeholder="<spring:message code='label.moduledescription'/>.."
								type="text" value="${module.moduleDescription}">
						</div>
					</div>
					
					
<!-- 					<div class="form-group"> -->
<%-- 						<label class="col-md-4 control-label" for="example-daterange1"><spring:message --%>
<%-- 								code="label.selectdaterange" /> </label> --%>
<!-- 						<div class="col-md-8"> -->

<!-- 							<label class="col-md-4 control-label" for="example-daterange1"> -->
<%-- 								${ company.compAgreementStart} </label> <label --%>
<!-- 								class="col-md-4 control-label" for="example-daterange1"> -->
<%-- 								${ company.compAgreementEnd} </label> --%>
<!-- 						</div> -->
<!-- 					</div> -->


<!-- 					<div class="form-group"> -->
<%-- 							<label class="col-md-4 control-label" for="example-daterange1"><spring:message --%>
<%-- 									code="label.agreementdoc" /> </label> --%>
<!-- 							<div class="col-md-8"> -->
<%-- 	                            <p class="form-control-static">${company.agreementFileName} --%>
<!-- 	                            </p> -->
<!-- 	                        </div> -->
<!-- 					</div> -->
					
<!-- 				</div> -->
<!-- 				<div class="block"> -->
<!-- 					<div class="block-title"> -->
<!-- 						<h2> -->
<%-- 							<strong><spring:message code="heading.contactinfo" /></strong> --%>
<!-- 						</h2> -->
<!-- 					</div> -->
<!-- 					<div class="form-group"> -->
<%-- 						<label class="col-md-4 control-label" for="telephone1"><spring:message --%>
<%-- 								code="label.telephone1" /><span class="text-danger">*</span></label> --%>
<!-- 						<div class="col-md-6"> -->

<!-- 							<input id="companyTelephone1" name="companyTelephone1" -->
<!-- 								class="form-control" -->
<%-- 								placeholder="<spring:message code='label.telephone1'/>.." --%>
<%-- 								type="text" value="${company.companyTelephone1}" disabled="disabled"> --%>
<!-- 						</div> -->
<!-- 					</div> -->

<!-- 					<div class="form-group"> -->
<%-- 						<label class="col-md-4 control-label" for="telephone2"><spring:message --%>
<%-- 								code="label.telephone2" /></label> --%>
<!-- 						<div class="col-md-6"> -->

<!-- 							<input id="companyTelephone2" name="companyTelephone2" -->
<!-- 								class="form-control" -->
<%-- 								placeholder="<spring:message code='label.telephone2'/>.." --%>
<%-- 								type="text" value="${company.companyTelephone2}" disabled="disabled"> --%>
<!-- 						</div> -->
<!-- 					</div> -->

<!-- 					<div class="form-group"> -->
<%-- 						<label class="col-md-4 control-label" for="rest_email"><spring:message --%>
<%-- 								code="label.companyEmail" /><span class="text-danger">*</span></label> --%>
<!-- 						<div class="col-md-6"> -->

<!-- 							<input id="company_email" name="companyEmail" -->
<!-- 								class="form-control" -->
<%-- 								placeholder="<spring:message code="label.companyEmail"/>.." --%>
<%-- 								type="text" value="${company.companyEmail}" disabled="disabled"> --%>
<!-- 						</div> -->
<!-- 					</div> -->

<!-- 					<div class="form-group"> -->
<%-- 						<label class="col-md-4 control-label" for="mobile"><spring:message --%>
<%-- 								code="label.mobile" /><span class="text-danger">*</span></label> --%>
<!-- 						<div class="col-md-6"> -->

<!-- 							<input id="companyMobile" name="companyMobile" -->
<!-- 								class="form-control" -->
<%-- 								placeholder="<spring:message code="label.mobile"/>.." --%>
<%-- 								type="text" value="${company.companyMobile}" disabled="disabled"> --%>
<!-- 						</div> -->
<!-- 					</div> -->

<!-- <!-- 					<div class="form-group"> --> 
<%-- <%-- 						<label class="col-md-4 control-label" for="mobile2"><spring:message --%> 
<%-- <%-- 								code="label.mobile2" /></label> --%> 
<!-- <!-- 						<div class="col-md-6"> --> 

<!-- <!-- 							<input id="companyMobile2" name="companyMobile2" --> 
<!-- <!-- 								class="form-control" --> 
<%-- <%-- 								placeholder="<spring:message code="label.mobile2"/>.." --%> 
<%-- <%-- 								type="text" value="${company.companyMobile2}" disabled="disabled"> --%> 
<!-- <!-- 						</div> --> 
<!-- <!-- 					</div> --> 

<!-- 					<div class="form-group"> -->
<%-- 						<label class="col-md-4 control-label" for="addressline1"><spring:message --%>
<%-- 								code="label.addressline1" /><span class="text-danger">*</span> --%>
<!-- 						</label> -->
<!-- 						<div class="col-md-6"> -->

<!-- 							<input id="companyAddressline1" name="companyAddressLine1" -->
<!-- 								class="form-control" -->
<%-- 								placeholder="<spring:message code="label.addressline1"/>.." --%>
<%-- 								type="text" value="${company.companyAddressLine1}" disabled="disabled"> --%>
<!-- 						</div> -->
<!-- 					</div> -->

<!-- 					<div class="form-group"> -->
<%-- 						<label class="col-md-4 control-label" for="addressline2"><spring:message --%>
<%-- 								code="label.addressline2" /></label> --%>
<!-- 						<div class="col-md-6"> -->

<!-- 							<input id="companyAddressline2" name="companyAddressLine2" -->
<!-- 								class="form-control" -->
<%-- 								placeholder="<spring:message code="label.addressline2"/>.." --%>
<%-- 								type="text" value="${company.companyAddressLine2}" disabled="disabled"> --%>
<!-- 						</div> -->
<!-- 					</div> -->
					
					
					
						
<!-- 					<div class="form-group"> -->
<%-- 							<label class="col-md-4 control-label" for="country"><spring:message --%>
<%-- 									code="label.countryname" /><span class="text-danger">*</span></label> --%>
<!-- 							<div class="col-md-6"> -->
<!-- 								<input id="country" name="country" disabled="disabled" -->
<%-- 									class="form-control" value="${company.companyCountry.countryName}" --%>
<!-- 								type="text"> -->
<!-- 							</div> -->
<!-- 						</div> -->
					
<!-- 					<div class="form-group"> -->
<%-- 							<label class="col-md-4 control-label" for="city"><spring:message --%>
<%-- 									code="label.countryname" /><span class="text-danger">*</span></label> --%>
<!-- 							<div class="col-md-6"> -->
<!-- 								<input id="city" name="city" disabled="disabled" -->
<%-- 									class="form-control" value="${company.companyCity.cityName}" --%>
									
<!-- 									type="text"> -->
<!-- 							</div> -->
<!-- 						</div> -->

<!-- 					<div class="form-group"> -->
<%-- 							<label class="col-md-4 control-label" for="area"><spring:message --%>
<%-- 									code="label.countryname" /><span class="text-danger">*</span></label> --%>
<!-- 							<div class="col-md-6"> -->
<!-- 								<input id="area" name="area" disabled="disabled" -->
<%-- 									class="form-control" value="${company.companyArea.areaName}" --%>
									
<!-- 									type="text"> -->
<!-- 							</div> -->
<!-- 						</div> -->
					
					
<!-- 					<div class="form-group"> -->
<%-- 						<label class="col-md-4 control-label" for="companyPincode"><spring:message --%>
<%-- 								code="label.pincode" /><span class="text-danger">*</span></label> --%>
<!-- 						<div class="col-md-6"> -->

<!-- 							<input id="companyPincode" name="companyPincode" -->
<!-- 								class="form-control" -->
<%-- 								placeholder="<spring:message code="label.pincode"/>.." --%>
<%-- 								type="text" value="${company.companyPincode}" disabled="disabled"> --%>
<!-- 						</div> -->
<!-- 					</div> -->




<!-- 				</div> -->

<!-- 			</div> -->
<!-- 			<div class="col-md-6"> -->
<!-- 				<div class="block"> -->
<!-- 					<div class="block-title"> -->
<!-- 						<h2> -->
<%-- 							<strong><spring:message code="heading.admininfo" /></strong> --%>
<!-- 						</h2> -->
<!-- 					</div> -->
<!-- 					<div class="form-group"> -->
<%-- 						<label class="col-md-4 control-label" for="first_Name"><spring:message --%>
<%-- 								code="label.firstname" /><span class="text-danger">*</span> </label> --%>
<!-- 						<div class="col-md-6"> -->

<!-- 							<input id="first_Name" name="firstName" class="form-control" -->
<%-- 								placeholder="<spring:message code='label.firstname'/>.." --%>
<%-- 								type="text" value="${company.companyAdmin.firstName}" disabled="disabled"> --%>
<!-- 						</div> -->
<!-- 					</div> -->

<!-- 					<div class="form-group"> -->
<%-- 						<label class="col-md-4 control-label" for="last_Name"><spring:message --%>
<%-- 								code="label.lastname" /><span class="text-danger">*</span> </label> --%>
<!-- 						<div class="col-md-6"> -->

<!-- 							<input id="last_Name" name="lastName" class="form-control" -->
<%-- 								placeholder="<spring:message code="label.lastname"/>.." --%>
<%-- 								type="text" value="${company.companyAdmin.lastName}" disabled="disabled"> --%>
<!-- 						</div> -->
<!-- 					</div> -->


<!-- 					<div class="form-group"> -->
<%-- 						<label class="col-md-4 control-label" for="email"><spring:message --%>
<%-- 								code="label.emailid" /><span class="text-danger">*</span></label> --%>
<!-- 						<div class="col-md-6"> -->
<!-- 							<input id="email" name="emailId" class="form-control" -->
<!-- 								placeholder="test@example.com" type="text" -->
<%-- 								value="${company.companyAdmin.emailId}" disabled="disabled"> --%>
<!-- 						</div> -->
<!-- 					</div> -->

<!-- 					<div class="form-group"> -->
<%-- 						<label class="col-md-4 control-label" for="mobile"><spring:message --%>
<%-- 								code="label.mobile" /><span class="text-danger">*</span></label> --%>
<!-- 						<div class="col-md-6"> -->
<!-- 							<input id="mobile" name="mobile" class="form-control" -->
<%-- 								placeholder='<spring:message code="label.mobile"></spring:message>' --%>
<%-- 								type="text" value="${company.companyAdmin.mobile}" disabled="disabled"> --%>
<!-- 						</div> -->
					</div>
<!-- 					<div class="form-group"> -->
<%--                         <label class="col-md-4 control-label"><spring:message code="label.userlogin"/></label> --%>
<!--                         <div class="col-md-8"> -->
<%--                             <p class="form-control-static">${company.companyAdmin.loginId}</p> --%>
<!--                         </div> -->
<!--                     </div> -->
					
				</div>

			</div>
		</div>
	
	</form>

	
</div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<script src="<%=contexturl %>resources/js/custom/featurecontrol.js"></script>
<script
	src="../resources/js/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>

s