<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>
<%@page import="com.astrika.common.model.Role"%>


<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				<i class="fa fa-map-marker"></i>
				<spring:message code="heading.courseMaster" />
				<br> <small><spring:message
						code="heading.specifylistofcourses" /> </small>
			</h1>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i>
						<spring:message code="label.success" />
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>
			<div class="alert alert-danger alert-dismissable"
				style="display: none" id="error">
				<button type="button" class="close" data-dismiss="alert"
					aria-hidden="true">x</button>
				<h4>
					<i class="fa fa-times-circle"> </i> <spring:message code="label.error"/>
				</h4>
				<span id="errorMsg"></span>
			</div>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="heading.managecourse" /></li>
		<li><a href="#"><spring:message code="heading.courseMaster" /></a></li>
	</ul>
	
	                    
	<div class="block full  gridView " >
		<div class="block-title">
			<h2>
				<strong><spring:message code="heading.activecourse" /></strong>
			</h2>
		</div>
		<security:authorize ifAnyGranted="<%=Role.INSTRUCTOR.name() %>">
		<a href="<%=contexturl %>ManageCourse/CourseList/AddCourse" id="addCourse" class="btn btn-sm btn-primary"><spring:message
				code="label.addcourse" /></a>
		</security:authorize>
		<div class="table-responsive">
			<table id="active-datatable"
				class="table table-vcenter table-condensed table-bordered">
				<thead>
					<tr>
						<th class="text-center"><spring:message code="label.id" /></th>
						<th class="text-center"><spring:message code="label.coursename" /></th>
						<security:authorize ifAnyGranted="<%=Role.SKILLME_ADMIN.name() %>">
						<th class="text-center"><spring:message code="label.institutionname" /></th>
						</security:authorize>
						<th class="text-center"><spring:message code="label.createdby" /></th>
						<th class="text-center"><spring:message code="label.courseStatus" /></th>
						<security:authorize ifAnyGranted="<%=Role.INSTRUCTOR.name() %>">
						<th class="text-center"><spring:message code="label.yourstatus" /></th>
						</security:authorize>
						<th class="text-center"><spring:message code="label.actions" /></th>
					</tr>
				</thead>
				<tbody id="tbody">
					<c:forEach var="currentCourse" items="${activeCourse}"
						varStatus="count">

						<tr>
							<security:authorize ifAnyGranted="<%=Role.SKILLME_ADMIN.name() %>">
							<td class="text-center">${count.count}</td>
							<td class="text-left"><c:out
									value="${currentCourse.courseName}" /></td>
							<td class="text-left"><c:out
									value="${currentCourse.university.universityName}" /></td>
							<td class="text-left"><c:out
									value="${currentCourse.createdBy.firstName} ${currentCourse.createdBy.lastName}" /></td>
							<td class="text-left"><c:out
									value="${currentCourse.status}" /></td>
							<td class="text-center">
								<div class="btn-group">
							<security:authorize ifAnyGranted="<%=Role.INSTRUCTOR.name() %>">
								<c:if test = "${currentCourse.course.status eq status}" >
								<a href="<%=contexturl %>ManageCourse/CourseList/EditCourse?id=${currentCourse.course.courseId}"
										data-toggle="tooltip" title="Edit"
										class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
								
								</c:if>
								<c:if test = "${(currentCourse.course.status eq status2) or (currentCourse.course.status eq status1)}" >
								<a href="<%=contexturl %>ManageCourse/CourseList/ViewCourse?id=${currentCourse.course.courseId}"
										data-toggle="tooltip" title="View"
										class="btn btn-xs btn-default"><i class="fa fa-file-text-o fa-fw"></i></a>
								
								</c:if>
							</security:authorize>
								
									
									<a href="#delete_course_popup" data-toggle="modal"
										onclick="deleteCourse(${currentCourse.courseId})"
										title="Delete Course" class="btn btn-xs btn-danger"><i
										class="fa fa-times"></i></a>
							
								</div>
								
							</td>
							</security:authorize>
							
							
							<security:authorize ifAnyGranted="<%=Role.UNIVERSITY_ADMIN.name() %>">
							<td class="text-center">${count.count}</td>
							<td class="text-left"><c:out
									value="${currentCourse.courseName}" /></td>
							<td class="text-left"><c:out
									value="${currentCourse.createdBy.firstName} ${currentCourse.createdBy.lastName}" /></td>
							<td class="text-left"><c:out
									value="${currentCourse.status}" /></td>
							<td class="text-center">
								<div class="btn-group">
							<security:authorize ifAnyGranted="<%=Role.INSTRUCTOR.name() %>">
								<c:if test = "${currentCourse.course.status eq status}" >
								<a href="<%=contexturl %>ManageCourse/CourseList/EditCourse?id=${currentCourse.course.courseId}"
										data-toggle="tooltip" title="Edit"
										class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
								
								</c:if>
								<c:if test = "${(currentCourse.course.status eq status2) or (currentCourse.course.status eq status1)}" >
								<a href="<%=contexturl %>ManageCourse/CourseList/ViewCourse?id=${currentCourse.course.courseId}"
										data-toggle="tooltip" title="View"
										class="btn btn-xs btn-default"><i class="fa fa-file-text-o fa-fw"></i></a>
								
								</c:if>
							</security:authorize>
								
									
									<a href="#delete_course_popup" data-toggle="modal"
										onclick="deleteCourse(${currentCourse.courseId})"
										title="Delete Course" class="btn btn-xs btn-danger"><i
										class="fa fa-times"></i></a>
							
								</div>
								
							</td>
							</security:authorize>
							
							
							<security:authorize ifAnyGranted="<%=Role.INSTRUCTOR.name() %>">
							<td class="text-center">${count.count}</td>
							<td class="text-left"><c:out
									value="${currentCourse.course.courseName}" /></td>
							<td class="text-left"><c:out
									value="${currentCourse.course.createdBy.firstName} ${currentCourse.course.createdBy.lastName}" /></td>
							<td class="text-left"><c:out
									value="${currentCourse.course.status}" /></td>
							
							<td class="text-left"><c:out
									value="${currentCourse.status}" /></td>
							
							<td class="text-center">
								<div class="btn-group">
							
								<c:if test = "${currentCourse.course.status eq status}" >
								<a href="<%=contexturl %>ManageCourse/CourseList/EditCourse?id=${currentCourse.course.courseId}"
										data-toggle="tooltip" title="Edit"
										class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
								
								</c:if>
								<c:if test = "${(currentCourse.course.status eq status2) or (currentCourse.course.status eq status1)}" >
								<a href="<%=contexturl %>ManageCourse/CourseList/ViewCourse?id=${currentCourse.course.courseId}"
										data-toggle="tooltip" title="View"
										class="btn btn-xs btn-default"><i class="fa fa-file-text-o fa-fw"></i></a>
								
								</c:if>
							
								
									
									<a href="#delete_course_popup" data-toggle="modal"
										onclick="deleteCourse(${currentCourse.course.courseId})"
										title="Delete Course" class="btn btn-xs btn-danger"><i
										class="fa fa-times"></i></a>
							
								
							
								</div>
								
							</td>
							</security:authorize>
						</tr>
					</c:forEach>

				</tbody>
			</table>
		</div>
	
</div>

	<div class="block full  gridView">
		<div class="block-title">
			<h2>
				<strong><spring:message code="heading.inactivecourse" /></strong> 
			</h2>
		</div>
		<div class="table-responsive">
			<table id="inactive-datatable"
				class="table table-vcenter table-condensed table-bordered ">
				<thead>
					<tr>
						<th class="text-center"><spring:message code="label.id" /></th>
						<th class="text-center"><spring:message code="label.coursename" /></th>
						<security:authorize ifAnyGranted="<%=Role.SKILLME_ADMIN.name() %>">
						<th class="text-center"><spring:message code="label.institutionname" /></th>
						</security:authorize>
						<th class="text-center"><spring:message code="label.createdby" /></th>
						<th class="text-center"><spring:message code="label.courseStatus" /></th>
						<security:authorize ifAnyGranted="<%=Role.INSTRUCTOR.name() %>">
						<th class="text-center"><spring:message code="label.yourstatus" /></th>
						</security:authorize>
						<th class="text-center"><spring:message code="label.actions" /></th>
					</tr>
				</thead>
				<tbody id="tablebody">
					<c:forEach var="currentCourse" items="${inActiveCourse}"
						varStatus="count">
						<tr>
							<security:authorize ifAnyGranted="<%=Role.SKILLME_ADMIN.name() %>">
							<td class="text-center">${count.count}</td>
							<td class="text-left"><c:out
									value="${currentCourse.courseName}" /></td>
							
							<td class="text-left"><c:out
									value="${currentCourse.university.universityName}" /></td>
							
							<td class="text-left"><c:out
									value="${currentCourse.createdBy.firstName} ${currentCourse.createdBy.lastName}" /></td>
							<td class="text-left"><c:out
									value="${currentCourse.status}" /></td>
							
							<td class="text-center">
								<div class="btn-group">
									<a href="#restore_Course_popup" data-toggle="modal"
										onclick="restoreCourse(${currentCourse.courseId})"
										title="Restore Course" class="btn btn-xs btn-danger"><i
										class="fa fa-times"></i> </a>
								</div>
							</td>
							</security:authorize>
							
							
							<security:authorize ifAnyGranted="<%=Role.UNIVERSITY_ADMIN.name() %>">
							<td class="text-center">${count.count}</td>
							<td class="text-left"><c:out
									value="${currentCourse.courseName}" /></td>
							
							<td class="text-left"><c:out
									value="${currentCourse.createdBy.firstName} ${currentCourse.createdBy.lastName}" /></td>
							<td class="text-left"><c:out
									value="${currentCourse.status}" /></td>
							
							<td class="text-center">
								<div class="btn-group">
									<a href="#restore_Course_popup" data-toggle="modal"
										onclick="restoreCourse(${currentCourse.courseId})"
										title="Restore Course" class="btn btn-xs btn-danger"><i
										class="fa fa-times"></i> </a>
								</div>
							</td>
							</security:authorize>
							
							
							<security:authorize ifAnyGranted="<%=Role.INSTRUCTOR.name() %>">
							<td class="text-center">${count.count}</td>
							<td class="text-left"><c:out
									value="${currentCourse.course.courseName}" /></td>
							
							<td class="text-left"><c:out
									value="${currentCourse.course.createdBy.firstName} ${currentCourse.course.createdBy.lastName}" /></td>
							<td class="text-left"><c:out
									value="${currentCourse.course.status}" /></td>
							
							<td class="text-left"><c:out
									value="${currentCourse.status}" /></td>
							
							<td class="text-center">
								<div class="btn-group">
									<a href="#restore_Course_popup" data-toggle="modal"
										onclick="restoreCourse(${currentCourse.course.courseId})"
										title="Restore Course" class="btn btn-xs btn-danger"><i
										class="fa fa-times"></i> </a>
								</div>
							</td>
							</security:authorize>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	
	
	

</div>
<div id="delete_course_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="<%=contexturl %>ManageCourse/CourseList/DeleteCourse" method="post" class="form-horizontal form-bordered"
						id="delete_course_Form">
						<div style="padding: 10px; height: 110px;">
							<label><spring:message
									code="validation.deletecourse" /></label>
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal">
									<spring:message code="label.no" />
								</button>
								<div id="delete_course" class="btn btn-sm btn-primary">
									<spring:message code="label.yes" />
								</div>
							</div>
						</div>
						<input type="hidden" name="id" id="deletecourseId">

					</form>

				</div>
			</div>
		</div>
	</div>


	<div id="restore_Course_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="<%=contexturl %>ManageCourse/CourseList/RestoreCourse" method="post" class="form-horizontal form-bordered" id="restore_course_Form">
						
						<div style="padding: 10px; height: 110px;">
							<label><spring:message
									code="validation.restorecourse" /></label>
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal"><spring:message code="label.no" /></button>
								<div id="restore_Course" class="btn btn-sm btn-primary">	<spring:message code="label.yes" /></div>
							</div>
						</div>
						<input type="hidden" name="id" id="restorecourseId">
					</form>
				</div>
			</div>
		</div>
	</div>
	

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<script
	src="<%=contexturl%>resources/js/pages/activeTablesDatatables.js"></script>
<script
	src="<%=contexturl%>resources/js/pages/inactiveTablesDatatables.js"></script>
<security:authorize ifAnyGranted="<%=Role.SKILLME_ADMIN.name() %>">
<script>$(function(){ ActiveTablesDatatables.init(5); });</script>
<script>$(function(){ InActiveTablesDatatables.init(5); });</script>
</security:authorize>
<security:authorize ifAnyGranted="<%=Role.INSTRUCTOR.name() %>">
<script>$(function(){ ActiveTablesDatatables.init(5); });</script>
<script>$(function(){ InActiveTablesDatatables.init(5); });</script>
</security:authorize>

<security:authorize ifAnyGranted="<%=Role.UNIVERSITY_ADMIN.name() %>">
<script>$(function(){ ActiveTablesDatatables.init(4); });</script>
<script>$(function(){ InActiveTablesDatatables.init(4); });</script>
</security:authorize>
<script type="text/javascript">

	var selectedId = 0;
	var restoreId=0;
	$(document).ready(function() {
		
        $("#restore_Course").click(function(){
			
			$("#restorecourseId").val(restoreId);
			$("#restore_course_Form").submit();
		});
		
		
		$("#delete_course").click(function(){
			
			$("#deletecourseId").val(selectedId);
			$("#delete_course_Form").submit();
		});
	});
	
	function deleteCourse(id){
		selectedId = id;
	}
	function restoreCourse(id){
		restoreId = id;
	}
</script>
<%@include file="../../inc/template_end.jsp"%>