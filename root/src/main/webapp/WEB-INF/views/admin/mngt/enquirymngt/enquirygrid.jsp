<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				
				<spring:message code="heading.enquirymaster" />
				<br> <small><spring:message
						code="heading.listofenquiredcompany" /> </small>
			</h1>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i>
						<spring:message code="label.success" />
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>
			<div class="alert alert-danger alert-dismissable"
				style="display: none" id="error">
				<button type="button" class="close" data-dismiss="alert"
					aria-hidden="true">x</button>
				<h4>
					<i class="fa fa-times-circle"> </i> <spring:message code="label.error"/>
				</h4>
				<span id="errorMsg"></span>
			</div>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="heading.manageenquiry" /></li>
		<li><a href="#"><spring:message code="label.enquiry" /></a></li>
	</ul>
	<div class="block full  gridView">
		
<%-- 		<a href="<%=contexturl %>ManageModule/ModuleList/AddModule" id="addUniversity" class="btn btn-sm btn-primary save"><spring:message --%>
<%-- 				code="label.addmodule" /></a> --%>
		<!--<p><a href="https://datatables.net/" target="_blank">DataTables</a> is a plug-in for the Jquery Javascript library. It is a highly flexible tool, based upon the foundations of progressive enhancement, which will add advanced interaction controls to any HTML table. It is integrated with template's design and it offers many features such as on-the-fly filtering and variable length pagination.</p> -->
	<div class="table-responsive">
		<div class="block-title">
			<h2>
				<strong>Pending Requests</strong>
			</h2>
		</div>
			<table id="active-datatable"
				class="table table-vcenter table-condensed table-bordered">
				<thead>
					<tr>
					<th class="text-center"><spring:message code="label.id" /></th>
						<th class="text-center"><spring:message code="label.companyname" /></th> 
						<th class="text-center"><spring:message code="label.createdOn" /></th>
				        <th class="text-center"><spring:message code="label.action" /></th>
					</tr>
				</thead>
				<tbody id="tbody">
					<c:forEach var="enquiry" items="${listOfEnquiry}"
						varStatus="count">

						<tr>
							<td class="text-center">${count.count}</td>
							<td class="text-left"><c:out value="${enquiry.ngoName}" /></td>
							<td class="text-left"> <fmt:formatDate type="date" value="${enquiry.createdDate}" /></td>
							
                             <td class="text-center">
								<div class="btn-group">
									<a href="<%=contexturl %>ManageEnquiry/ListEnquiry/ViewEnquiry?id=${enquiry.enquiryId}"
										data-toggle="tooltip" title=""
										class="btn btn-sm btn-primary save"><strong>Edit/Approve</strong></a>
								</div>
							</td>
						</tr>
					</c:forEach>

				</tbody>
			</table>
		</div>
	</div>
	
	<div class="block full  gridView">
		<div class="block-title">
			<h2>
				<strong>Rejected Requests</strong>
			</h2>
		</div>
		<div class="table-responsive">
			<table id="inactive-datatable"
				class="table table-vcenter table-condensed table-bordered ">
				<thead>
					<tr>
						<th class="text-center"><spring:message code="label.id" /></th>
						<th class="text-center"><spring:message code="label.companyname" /></th> 
						<th class="text-center"><spring:message code="label.rejectedOn" /></th>
					</tr>
				</thead>
				<tbody id="tablebody">
					<c:forEach var="rejectedenquiry" items="${listOfRejected}"
						varStatus="count">
						<tr>
							<td class="text-center">${count.count}</td>
							<td class="text-left"><c:out value="${rejectedenquiry.ngoName}" /></td>
							<td class="text-left"> <fmt:formatDate type="date" value="${rejectedenquiry.rejectedDate}" /></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<script
	src="<%=contexturl%>resources/js/pages/activeTablesDatatables.js"></script>
<script
	src="<%=contexturl%>resources/js/pages/inactiveTablesDatatables.js"></script>
<script>$(function(){ ActiveTablesDatatables.init(3); });</script>
<script>$(function(){ InActiveTablesDatatables.init(2); });</script>

<script type="text/javascript">
	var selectedId = 0;
	var restoreId=0;
	$(document).ready(function() {
		
		$("#manageEnquiry").addClass("active");		
		
	});
</script>
<%@include file="../../inc/template_end.jsp"%>