<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>


<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i>
						<spring:message code="label.error" />
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>

			<h1>
<!-- 				<i class="fa fa-map-marker"></i> -->
				<spring:message code="heading.document" />
				<br> <small><spring:message code="heading.newdocument" /></small>
			</h1>
			<span id="errorMsg"></span>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="menu.managedocument" /></li>
	</ul>
	<form action="<%=contexturl%>ManageDocument/DocumentList/SaveDocument"
		method="post" class="form-horizontal ui-formwizard" id="Document_form"
		enctype="multipart/form-data">
		<div class="row">

			<div class="">
				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.documentinfo" /></strong>
						</h2>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="contentType"><spring:message
								code="label.contentname" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<c:choose>
								<c:when test="${not empty documentMaster.contentType}">
									<input id="contentTypeDoc" name="contentType"
										class="contentType" type="radio" value="Document"
										<c:if test="${documentMaster.contentType eq 'Document'}"> checked = "checked"</c:if>>Document <br>
									<input id="contentTypeLink" name="contentType"
										class="contentType" type="radio" value="Link"
										<c:if test="${documentMaster.contentType eq 'Link'}">checked = "checked"</c:if>>Link
								</c:when>
								<c:otherwise>
									<input id="contentTypeDoc" name="contentType"
										class="contentType" type="radio" value="Document">Document <br>
									<input id="contentTypeLink" name="contentType"
										class="contentType" type="radio" value="Link"
										checked="checked">Link
								</c:otherwise>
							</c:choose>
						</div>
					</div>

					<div class="form-group" id="moduleDiv">
						<label class="col-md-4 control-label" for="chosenModuleId"><spring:message
								code="label.modulename" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<select class="module_chosen" style="width: 200px;"
								id="chosenModuleId"
								data-placeholder="<spring:message code='label.choosemodule' />"
								name="chosenModuleId">
								<option value=""></option>
								<c:forEach items="${moduleList}" var="module">
									<option value="${module.moduleId}"
										<c:if test="${module.moduleId eq documentMaster.module.moduleId}">selected="selected"</c:if>>${module.moduleName}</option>
								</c:forEach>
							</select>
						</div>
					</div>

					<div class="form-group" id="categoryDiv">
						<label class="col-md-4 control-label" for="chosenCategoryId"><spring:message
								code="label.categoryname" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<select class="category_chosen" style="width: 200px;"
								id="chosenCategoryId"
								data-placeholder="<spring:message code='label.choosecategory' />"
								name="chosenCategoryId">
								<option value=""></option>
								<c:forEach items="${categoryList}" var="category">
									<option value="${category.categoryId}"
										<c:if test="${category.categoryId eq documentMaster.category.categoryId}">selected="selected"</c:if>>${category.categoryName}</option>
								</c:forEach>
							</select>
						</div>
					</div>

					<div class="form-group" id="sectionDiv">
						<label class="col-md-4 control-label" for="chosenSectionId"><spring:message
								code="label.sectionname" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<select class="section_chosen" style="width: 200px;"
								id="chosenSectionId"
								data-placeholder="<spring:message code='label.choosesection' />"
								name="chosenSectionId">
								<option value=""></option>
								<c:forEach items="${sectionList}" var="section">
									<option value="${section}"
										<c:if test="${section eq documentMaster.section}">selected="selected"</c:if>>${section}</option>
								</c:forEach>
							</select>
						</div>
					</div>

					<c:if test = "${(documentMaster.contentType eq 'Document') && not(documentMaster.section eq 'BluePrint')}">
					<div id="DocumentDiv1">
						<div class="block">
							<div class="block-title">
								<h2>
									<strong>Form 1</strong>
								</h2>
							</div>
							<div class="form-group" id="doctitle1">
								<label class="col-md-4 control-label" for="docTitle1"><spring:message
										code="label.title" /> <span class="text-danger">*</span> </label>
								<div class="col-md-6">

									<input id="docTitle1" name="docTitle1" class="form-control"
										placeholder="<spring:message code='label.title'/>.."
										type="text" value="${documentMaster.title}">
								</div>
							</div>
							<div class="form-group" id="docDescription1">
								<label class="col-md-4 control-label" for="documentDescription1"><spring:message
										code="label.documentdescription" /><span class="text-danger">*</span></label>
								<div class="col-md-6">
									<textarea id="documentDescription1" name="documentDescription1"
										class="form-control"
										placeholder="<spring:message code='label.description'/>..">${documentMaster.documentDescription}</textarea>
								</div>
							</div>

							<div class="form-group" id="lifestageDiv1">
								<label class="col-md-4 control-label" for="chosenLifeStage1"><spring:message
 										code="label.lifestage" /><span class="text-danger">*</span></label>
								<div class="col-md-6">
 									<select class="lifestage_chosen1" style="width: 200px;" 
 										id="chosenLifeStage1" 
 										data-placeholder="<spring:message code='label.chooselifestage' />" 
 										name="chosenLifeStage1"> 
 										<option value=""></option> 
 										<c:forEach items="${lifeStageList}" var="lifeStages"> 
 											<option value="${lifeStages}"
 												<c:if test="${lifeStages eq documentMaster.lifeStages}">selected="selected"</c:if>>${lifeStages.name}</option>
								</c:forEach>
 									</select>
								</div> 
 							</div> 

								<div class="form-group" id="doc1">
								<label class="col-md-4 control-label" for="document1"><spring:message
										code="label.document" /> <span class="text-danger">*</span> </label>
								<div class="col-md-6">

									<input id="document1" name="document1" class=""
										placeholder="<spring:message code='label.uploaddocument'/>.."
										type="file">
								</div>
							</div>
						</div>
					</div>

					<div id="DocumentDiv2">
						<div class="block">
							<div class="block-title">
								<h2>
									<strong>Form 2</strong>
								</h2>
							</div>
							<div class="form-group" id="doctitle2">
								<label class="col-md-4 control-label" for="docTitle2"><spring:message
										code="label.title" /> </label>
								<div class="col-md-6">

									<input id="docTitle2" name="docTitle2" class="form-control"
										placeholder="<spring:message code='label.title'/>.."
										type="text" value="${documentMaster1.title}">
								</div>
							</div>
							<div class="form-group" id="docDescription2">
								<label class="col-md-4 control-label" for="documentDescription2"><spring:message
										code="label.documentdescription" /></label>
								<div class="col-md-6">
									<textarea id="documentDescription2" name="documentDescription2"
										class="form-control"
										placeholder="<spring:message code='label.description'/>..">${documentMaster1.documentDescription}</textarea>
								</div>
							</div>

								<div class="form-group" id="lifestageDiv2">
									<label class="col-md-4 control-label" for="chosenLifeStage2"><spring:message
											code="label.lifestage" /></label>
									<div class="col-md-6">
										<select class="lifestage_chosen2" style="width: 200px;"
											id="chosenLifeStage2"
											data-placeholder="<spring:message code='label.chooselifestage' />"
											name="chosenLifeStage2">
											<option value=""></option>
											<c:forEach items="${lifeStageList}" var="lifeStages">
												<option value="${lifeStages}"
													<c:if test="${lifeStages eq documentMaster1.lifeStages}">selected="selected"</c:if>>${lifeStages.name}</option>
											</c:forEach>
										</select>
									</div>
								</div>

								<div class="form-group" id="doc2">
								<label class="col-md-4 control-label" for="document2"><spring:message
										code="label.document" /></label>
								<div class="col-md-6">

									<input id="document2" name="document2" class=""
										placeholder="<spring:message code='label.uploaddocument'/>.."
										type="file">
								</div>
							</div>
						</div>
					</div>

					<div id="DocumentDiv3">
						<div class="block">
							<div class="block-title">
								<h2>
									<strong>Form 3</strong>
								</h2>
							</div>
							<div class="form-group" id="doctitle3">
								<label class="col-md-4 control-label" for="docTitle3"><spring:message
										code="label.title" /></label>
								<div class="col-md-6">

									<input id="docTitle3" name="docTitle3" class="form-control"
										placeholder="<spring:message code='label.title'/>.."
										type="text" value="${documentMaster2.title}">
								</div>
							</div>
							<div class="form-group" id="docDescription3">
								<label class="col-md-4 control-label" for="documentDescription3"><spring:message
										code="label.documentdescription" /></label>
								<div class="col-md-6">
									<textarea id="documentDescription3" name="documentDescription3"
										class="form-control"
										placeholder="<spring:message code='label.description'/>..">${documentMaster2.documentDescription}</textarea>
								</div>
							</div>

								<div class="form-group" id="lifestageDiv3">
									<label class="col-md-4 control-label" for="chosenLifeStage3"><spring:message
											code="label.lifestage" /></label> 
									<div class="col-md-6">
										<select class="lifestage_chosen3" style="width: 200px;"
											id="chosenLifeStage3"
											data-placeholder="<spring:message code='label.chooselifestage' />"
											name="chosenLifeStage3">
											<option value=""></option>
											<c:forEach items="${lifeStageList}" var="lifeStages">
												<option value="${lifeStages}"
													<c:if test="${lifeStages eq documentMaster2.lifeStages}">selected="selected"</c:if>>${lifeStages.name}</option>
											</c:forEach>
										</select>
									</div>
								</div>

								<div class="form-group" id="doc3">
								<label class="col-md-4 control-label" for="document3"><spring:message
										code="label.document" /></label>
								<div class="col-md-6">

									<input id="document3" name="document3" class=""
										placeholder="<spring:message code='label.uploaddocument'/>.."
										type="file">
								</div>
							</div>
						</div>
					</div>
					</c:if>

					<c:if test="${(documentMaster.contentType eq 'Document') && (documentMaster.section eq 'BluePrint')}">
						<div id="DocumentDiv4">
							<div class="block">
								<div class="block-title">
									<h2>
										<strong>Form 3</strong>
									</h2>
								</div>
								<div class="form-group" id="doctitle4">
									<label class="col-md-4 control-label" for="docTitle4"><spring:message
											code="label.title" /> <span class="text-danger">*</span> </label>
									<div class="col-md-6">

										<input id="docTitle4" name="docTitle4" class="form-control"
											placeholder="<spring:message code='label.title'/>.."
											type="text" value="${documentMaster.title}">
									</div>
								</div>
								<div class="form-group" id="docDescription4">
									<label class="col-md-4 control-label"
										for="documentDescription4"><spring:message
											code="label.documentdescription" /><span class="text-danger">*</span></label>
									<div class="col-md-6">
										<textarea id="documentDescription4"
											name="documentDescription4" class="form-control"
											placeholder="<spring:message code='label.description'/>..">${documentMaster.documentDescription}</textarea>
									</div>
								</div>

								<div class="form-group" id="lifestageDiv4">
									<label class="col-md-4 control-label" for="chosenLifeStage4"><spring:message
											code="label.lifestage" /><span class="text-danger">*</span></label>
									<div class="col-md-6">
										<select class="lifestage_chosen4" style="width: 200px;"
											id="chosenLifeStage4"
											data-placeholder="<spring:message code='label.chooselifestage' />"
											name="chosenLifeStage4">
											<option value=""></option>
											<c:forEach items="${lifeStageList}" var="lifeStages">
												<option value="${lifeStages}"
													<c:if test="${lifeStages eq documentMaster.lifeStages}">selected="selected"</c:if>>${lifeStages.name}</option>
											</c:forEach>
										</select>
									</div>
								</div>

								<div class="form-group" id="doc4">
									<label class="col-md-4 control-label" for="document4"><spring:message
											code="label.document" /> <span class="text-danger">*</span>
									</label>
									<div class="col-md-6">

										<input id="document4" name="document4" class=""
											placeholder="<spring:message code='label.uploaddocument'/>.."
											type="file">
									</div>
								</div>

								<div class="form-group" id="bpImages">
									<label class="col-md-4 control-label" for="document4">BluePrint
										Images <span class="text-danger">*</span>
									</label>
									<div id="myId" class="dropzone dz-clickable col-md-6"
										enctype="multipart/form-data">
										<div class="dz-default dz-message">
											<span>Upload Images here</span>
										</div>
									</div>
								</div>


							</div>
						</div>
					</c:if>

					<c:if test = "${(documentMaster.contentType eq 'Link') && not(documentMaster.section eq 'BluePrint')}">
					<div id="linkDiv1">
						<div class="block">
							<div class="block-title">
								<h2>
									<strong>Form 1</strong>
								</h2>
							</div>
							<div class="form-group" id="linkTitle1">
								<label class="col-md-4 control-label" for="linkTitle1"><spring:message
										code="label.title" /> <span class="text-danger">*</span> </label>
								<div class="col-md-6">

									<input id="linkTitle1" name="linkTitle1" class="form-control"
										placeholder="<spring:message code='label.title'/>.."
										type="text" value="${documentMaster.title}">
								</div>
							</div>

							<div class="form-group" id="lname1">
								<label class="col-md-4 control-label" for="link_Name1"><spring:message
										code="label.linkname" /> <span class="text-danger">*</span> </label>
								<div class="col-md-6">
									<input id="link_Name1" name="linkName1" class="form-control"
										placeholder="<spring:message code='label.linkname'/>.."
										type="text" value="${documentMaster.link}">
								</div>
							</div>
						</div>
					</div>

					<div id="linkDiv2">
						<div class="block">
							<div class="block-title">
								<h2>
									<strong>Form 2</strong>
								</h2>
							</div>
							<div class="form-group" id="linkTitle2">
								<label class="col-md-4 control-label" for="linkTitle2"><spring:message
										code="label.title" /></label>
								<div class="col-md-6">

									<input id="linkTitle2" name="linkTitle2" class="form-control"
										placeholder="<spring:message code='label.title'/>.."
										type="text" value="${documentMaster1.title}">
								</div>
							</div>

							<div class="form-group" id="lname2">
								<label class="col-md-4 control-label" for="link_Name2"><spring:message
										code="label.linkname" /></label>
								<div class="col-md-6">
									<input id="link_Name2" name="linkName2" class="form-control"
										placeholder="<spring:message code='label.linkname'/>.."
										type="text" value="${documentMaster1.link}">
								</div>
							</div>
						</div>
					</div>

					<div id="linkDiv3">
						<div class="block">
							<div class="block-title">
								<h2>
									<strong>Form 3</strong>
								</h2>
							</div>
							<div class="form-group" id="linkTitle3">
								<label class="col-md-4 control-label" for="linkTitle3"><spring:message
										code="label.title" /></label>
								<div class="col-md-6">

									<input id="linkTitle3" name="linkTitle3" class="form-control"
										placeholder="<spring:message code='label.title'/>.."
										type="text" value="${documentMaster2.title}">
								</div>
							</div>

							<div class="form-group" id="lname3">
								<label class="col-md-4 control-label" for="link_Name3"><spring:message
										code="label.linkname" /></label>
								<div class="col-md-6">
									<input id="link_Name3" name="linkName3" class="form-control"
										placeholder="<spring:message code='label.linkname'/>.."
										type="text" value="${documentMaster2.link}">
								</div>
							</div>
						</div>
					</div>
					</c:if>

					<div class="" id="selectedDiv1"></div>
					<div class="" id="selectedDiv2"></div>
					<div class="" id="selectedDiv3"></div>

					


				</div>
			</div>
		</div>
		<div style="text-align: center; margin-bottom: 10px">
			<div class="form-group form-actions">
				<!-- 				<div class="col-md-9 col-md-offset-3"> -->
				<button id="document_submit" type="submit"
					class="btn btn-sm btn-primary save">
					<i class="fa fa-angle-right"></i>
					<spring:message code="button.save" />
				</button>
				<a class="btn btn-sm btn-primary save"
					href="<%=contexturl %>ManageDocument/DocumentList"></i> <spring:message
						code="button.cancel" /> </a>
			</div>
		</div>
	</form>

	<div id="temporaryDocumentDiv1" style="display: none">
		<div class="block">
			<div class="block-title">
				<h2>
					<strong>Form 1</strong>
				</h2>
			</div>
			<div class="form-group" id="doctitle1">
				<label class="col-md-4 control-label" for="docTitle1"><spring:message
						code="label.title" /> <span class="text-danger">*</span> </label>
				<div class="col-md-6">

					<input id="docTitle1" name="docTitle1" class="form-control"
						placeholder="<spring:message code='label.title'/>.." type="text"
						value="${documentMaster.title}">
				</div>
			</div>
			<div class="form-group" id="docDescription1">
				<label class="col-md-4 control-label" for="documentDescription1"><spring:message
						code="label.documentdescription" /><span class="text-danger">*</span></label>
				<div class="col-md-6">
					<textarea id="documentDescription1" name="documentDescription1"
						class="form-control"
						placeholder="<spring:message code='label.description'/>..">${documentMaster.documentDescription}</textarea>
				</div>
			</div>

			<div class="form-group" id="lifestageDiv1">
				<label class="col-md-4 control-label" for="chosenLifeStage1"><spring:message
						code="label.lifestage" /><span class="text-danger">*</span></label>
				<div class="col-md-6">
					<select class="lifestage_chosen1" style="width: 200px;"
						id="chosenLifeStage1"
						data-placeholder="<spring:message code='label.chooselifestage' />"
						name="chosenLifeStage1">
						<option value=""></option>
						<c:forEach items="${lifeStageList}" var="lifeStages">
							<option value="${lifeStages}"
								<c:if test="${lifeStages eq documentMaster.lifeStages}">selected="selected"</c:if>>${lifeStages.name}</option>
						</c:forEach>
					</select>
				</div>
			</div>


			<div class="form-group" id="doc1">
				<label class="col-md-4 control-label" for="document1"><spring:message
						code="label.document" /> <span class="text-danger">*</span> </label>
				<div class="col-md-6">

					<input id="document1" name="document1" class=""
						placeholder="<spring:message code='label.uploaddocument'/>.."
						type="file">
				</div>
			</div>
		</div>
	</div>

	<div id="temporaryDocumentDiv2" style="display: none">
		<div class="block">
			<div class="block-title">
				<h2>
					<strong>Form 2</strong>
				</h2>
			</div>
			<div class="form-group" id="doctitle2">
				<label class="col-md-4 control-label" for="docTitle2"><spring:message
						code="label.title" /> </label>
				<div class="col-md-6">

					<input id="docTitle2" name="docTitle2" class="form-control"
						placeholder="<spring:message code='label.title'/>.." type="text"
						value="${documentMaster.title}">
				</div>
			</div>
			<div class="form-group" id="docDescription2">
				<label class="col-md-4 control-label" for="documentDescription2"><spring:message
						code="label.documentdescription" /></label>
				<div class="col-md-6">
					<textarea id="documentDescription2" name="documentDescription2"
						class="form-control"
						placeholder="<spring:message code='label.description'/>..">${documentMaster.documentDescription}</textarea>
				</div>
			</div>

			<div class="form-group" id="lifestageDiv2">
				<label class="col-md-4 control-label" for="chosenLifeStage2"><spring:message
						code="label.lifestage" /></label> 
				<div class="col-md-6">
					<select class="lifestage_chosen2" style="width: 200px;"
						id="chosenLifeStage2"
						data-placeholder="<spring:message code='label.chooselifestage' />"
						name="chosenLifeStage2">
						<option value=""></option>
						<c:forEach items="${lifeStageList}" var="lifeStages">
							<option value="${lifeStages}"
								<c:if test="${lifeStages eq documentMaster.lifeStages}">selected="selected"</c:if>>${lifeStages.name}</option>
						</c:forEach>
					</select>
				</div>
			</div>


			<div class="form-group" id="doc2">
				<label class="col-md-4 control-label" for="document2"><spring:message
						code="label.document" /></label>
				<div class="col-md-6">

					<input id="document2" name="document2" class=""
						placeholder="<spring:message code='label.uploaddocument'/>.."
						type="file">
				</div>
			</div>
		</div>
	</div>

	<div id="temporaryDocumentDiv3" style="display: none">
		<div class="block">
			<div class="block-title">
				<h2>
					<strong>Form 3</strong>
				</h2>
			</div>
			<div class="form-group" id="doctitle3">
				<label class="col-md-4 control-label" for="docTitle3"><spring:message
						code="label.title" /> </label>
				<div class="col-md-6">

					<input id="docTitle3" name="docTitle3" class="form-control"
						placeholder="<spring:message code='label.title'/>.." type="text"
						value="${documentMaster.title}">
				</div>
			</div>
			<div class="form-group" id="docDescription3">
				<label class="col-md-4 control-label" for="documentDescription3"><spring:message
						code="label.documentdescription" /></label>
				<div class="col-md-6">
					<textarea id="documentDescription3" name="documentDescription3"
						class="form-control"
						placeholder="<spring:message code='label.description'/>..">${documentMaster.documentDescription}</textarea>
				</div>
			</div>

			<div class="form-group" id="lifestageDiv3">
				<label class="col-md-4 control-label" for="chosenLifeStage3"><spring:message
					code="label.lifestage" /></label>
				<div class="col-md-6">
					<select class="lifestage_chosen3" style="width: 200px;"
						id="chosenLifeStage3"
						data-placeholder="<spring:message code='label.chooselifestage' />"
						name="chosenLifeStage3">
						<option value=""></option>
						<c:forEach items="${lifeStageList}" var="lifeStages">
							<option value="${lifeStages}"
								<c:if test="${lifeStages eq documentMaster.lifeStages}">selected="selected"</c:if>>${lifeStages.name}</option>
						</c:forEach>
					</select>
				</div>
			</div>


			<div class="form-group" id="doc3">
				<label class="col-md-4 control-label" for="document3"><spring:message
						code="label.document" /></label>
				<div class="col-md-6">

					<input id="document3" name="document3" class=""
						placeholder="<spring:message code='label.uploaddocument'/>.."
						type="file">
				</div>
			</div>
		</div>
	</div>
	
	
	<div id="temporaryDocumentDiv4" style="display: none">
		<div class="block">
			<div class="block-title">
				<h2>
					<strong>Form 1</strong>
				</h2>
			</div>
			<div class="form-group" id="doctitle4">
				<label class="col-md-4 control-label" for="docTitle4"><spring:message
						code="label.title" /> <span class="text-danger">*</span> </label>
				<div class="col-md-6">

					<input id="docTitle4" name="docTitle4" class="form-control"
						placeholder="<spring:message code='label.title'/>.." type="text"
						value="${documentMaster.title}">
				</div>
			</div>
			<div class="form-group" id="docDescription4">
				<label class="col-md-4 control-label" for="documentDescription4"><spring:message
						code="label.documentdescription" /><span class="text-danger">*</span></label>
				<div class="col-md-6">
					<textarea id="documentDescription4" name="documentDescription4"
						class="form-control"
						placeholder="<spring:message code='label.description'/>..">${documentMaster.documentDescription}</textarea>
				</div>
			</div>

			<div class="form-group" id="lifestageDiv4">
				<label class="col-md-4 control-label" for="chosenLifeStage4"><spring:message
					code="label.lifestage" /><span class="text-danger">*</span></label>
				<div class="col-md-6">
					<select class="lifestage_chosen4" style="width: 200px;"
						id="chosenLifeStage4"
						data-placeholder="<spring:message code='label.chooselifestage' />"
						name="chosenLifeStage4">
						<option value=""></option>
						<c:forEach items="${lifeStageList}" var="lifeStages">
							<option value="${lifeStages}"
								<c:if test="${lifeStages eq documentMaster.lifeStages}">selected="selected"</c:if>>${lifeStages.name}</option>
						</c:forEach>
					</select>
				</div>
			</div>


			<div class="form-group" id="doc4">
				<label class="col-md-4 control-label" for="document4"><spring:message
						code="label.document" /> <span class="text-danger">*</span> </label>
				<div class="col-md-6">

					<input id="document4" name="document4" class=""
						placeholder="<spring:message code='label.uploaddocument'/>.."
						type="file">
				</div>
			</div>

			<div class="form-group" id="bpImages">
				<label class="col-md-4 control-label" for="document4">BluePrint Images <span class="text-danger">*</span> </label>
				<div id="myId" class="dropzone dz-clickable col-md-6"
					enctype="multipart/form-data">
					<div class="dz-default dz-message">
						<span>Upload Images here</span>
					</div>
				</div>
			</div>

		</div>
	</div>

	<div  id="temporarylinkDiv1" style="display: none">
		<div class="block">
		<div class="block-title">
			<h2>
				<strong>Form 1</strong>
			</h2>
		</div>
		<div class="form-group" id="linkTitle1">
			<label class="col-md-4 control-label" for="linkTitle1"><spring:message
					code="label.title" /> <span class="text-danger">*</span> </label>
			<div class="col-md-6">

				<input id="linkTitle1" name="linkTitle1" class="form-control"
					placeholder="<spring:message code='label.title'/>.." type="text"
					value="${documentMaster.title}">
			</div>
		</div>
		
		<div class="form-group" id="lname1">
			<label class="col-md-4 control-label" for="link_Name1"><spring:message
					code="label.linkname" /> <span class="text-danger">*</span> </label>
			<div class="col-md-6">
				<input id="link_Name1" name="linkName1" class="form-control"
					placeholder="<spring:message code='label.linkname'/>.." type="text"
					value="${documentMaster.link}">
			</div>
		</div>
	</div>
	</div>
	
	<div  id="temporarylinkDiv2" style="display: none">
		<div class="block">
		<div class="block-title">
			<h2>
				<strong>Form 2</strong>
			</h2>
		</div>
		<div class="form-group" id="linkTitle2">
			<label class="col-md-4 control-label" for="linkTitle2"><spring:message
					code="label.title" /></label>
			<div class="col-md-6">

				<input id="linkTitle2" name="linkTitle2" class="form-control"
					placeholder="<spring:message code='label.title'/>.." type="text"
					value="${documentMaster.title}">
			</div>
		</div>
		
		<div class="form-group" id="lname2">
			<label class="col-md-4 control-label" for="link_Name2"><spring:message
					code="label.linkname" /></label>
			<div class="col-md-6">
				<input id="link_Name2" name="linkName2" class="form-control"
					placeholder="<spring:message code='label.linkname'/>.." type="text"
					value="${documentMaster.link}">
			</div>
		</div>
	</div>
	</div>
	
	<div  id="temporarylinkDiv3" style="display: none">
		<div class="block">
		<div class="block-title">
			<h2>
				<strong>Form 3</strong>
			</h2>
		</div>
		<div class="form-group" id="linkTitle3">
			<label class="col-md-4 control-label" for="linkTitle3"><spring:message
					code="label.title" /></label>
			<div class="col-md-6">

				<input id="linkTitle3" name="linkTitle3" class="form-control"
					placeholder="<spring:message code='label.title'/>.." type="text"
					value="${documentMaster.title}">
			</div>
		</div>
		
		<div class="form-group" id="lname3">
			<label class="col-md-4 control-label" for="link_Name3"><spring:message
					code="label.linkname" /></label>
			<div class="col-md-6">
				<input id="link_Name3" name="linkName3" class="form-control"
					placeholder="<spring:message code='label.linkname'/>.." type="text"
					value="${documentMaster.link}">
			</div>
		</div>
	</div>
	</div>

</div>
<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<%@include file="../../inc/chosen_scripts.jsp"%>
<script src="<%=contexturl %>resources/js/custom/featurecontrol.js"></script>
<script type="text/javascript"
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCheQF_yjKacwuVL4_59EO-3BThy85gbu4&sensor=false&region=GB">
	
</script>

<script type="text/javascript">
	$(document)
			.ready(
					function() {
						$("#manageDocument").addClass("active");
						
						Dropzone.autoDiscover = false;
						var i=0;
						$("div#myId")
								.dropzone(
										{
											acceptedFiles: "image/*",
											url : "<%=contexturl %>ManageDocument/DocumentList/UploadBluePrintImage"
										});

						var document1 = $("#temporaryDocumentDiv1").contents();
						var document2 = $("#temporaryDocumentDiv2").contents();
						var document3 = $("#temporaryDocumentDiv3").contents();
						var document4 = $("#temporaryDocumentDiv4").contents();
						var link1 = $("#temporarylinkDiv1").contents();
						var link2 = $("#temporarylinkDiv2").contents();
						var link3 = $("#temporarylinkDiv3").contents();
						
						$(".section_chosen").data("placeholder","Select Section From...").chosen();
						$(".category_chosen").chosen();
						$(".lifestage_chosen1").data("placeholder","Select LifeStage From...").chosen();
						$(".lifestage_chosen2").data("placeholder","Select LifeStage From...").chosen();
						$(".lifestage_chosen3").data("placeholder","Select LifeStage From...").chosen();
						$(".lifestage_chosen4").data("placeholder","Select LifeStage From...").chosen();
 						$(".module_chosen").data("placeholder","Select Module From...").chosen();
						
						$('select[name="chosenModuleId"]').chosen().change( function() {
							var moduleId = ($(this).val());
							$.ajax({
								type: "GET",
								async: false,
								url: "<%=contexturl %>category/categorybymoduleid/"+moduleId,
								success: function(data, textStatus, jqXHR){
									var obj = jQuery.parseJSON(data);
									var len=obj.length;
									var myOptions = "<option value></option>";
// 									var myCityOptions = "<option value></option>";
									for(var i=0; i<len; i++){
									myOptions += '<option value="'+obj[i].categoryId+'">'+obj[i].categoryName+'</option>';
									}
									$(".category_chosen").html(myOptions).chosen().trigger("chosen:updated");
// 									$(".city_chosen").html(myCityOptions).chosen().trigger("chosen:updated");
								},
								dataType: 'html'
							});
							
						});
	
						
						
						$('#chosenSectionId').change(function() {
						       if ($(this).val() == "BluePrint" && $('input[name=contentType]:checked').val() == "Document") {
						    	   $('#selectedDiv1').empty();	
						    	   $('#selectedDiv2').empty();	
						    	   $('#selectedDiv3').empty();	
						    	   $('#selectedDiv1').append(document4);
	    						}
						       else if(!($(this).val() == "BluePrint") && $("input[name=contentType]:checked").val() == "Document"){
						    	   $('#selectedDiv1').empty();	
						    	   $('#selectedDiv2').empty();	
						    	   $('#selectedDiv3').empty();	
						    	   $('#selectedDiv1').append(document1);
	      							$('#selectedDiv2').append(document2);
	      							$('#selectedDiv3').append(document3);
						       }
						       else if(!($(this).val() == "BluePrint") && $('input[name=contentType]:checked').val() == "Link"){
						    	   $('#selectedDiv1').empty();	
							    	 $('#selectedDiv2').empty();	
							    	 $('#selectedDiv3').empty();	
							    	 $('#selectedDiv1').append(link1);	
	      							 $('#selectedDiv2').append(link2);
	      							$('#selectedDiv3').append(link3);	
						       }
						       else {
						    	   $('#selectedDiv1').empty();	
							    	 $('#selectedDiv2').empty();	
							    	 $('#selectedDiv3').empty();
						       }
						    });	
						
						
						$('.contentType').change(function() {
							 if ($(this).val() == "Document" && $('#chosenSectionId').val() == "BluePrint") {
						    	   $('#selectedDiv1').empty();	
						    	   $('#selectedDiv2').empty();	
						    	   $('#selectedDiv3').empty();	
						    	   $('#selectedDiv1').append(document4);
	    						}
						       else if(($(this).val() == "Document") && (!$("#chosenSectionId").val() == "") && ($("#chosenSectionId").val() == "Examples" || $("#chosenSectionId").val() == "Toolbox" || $("#chosenSectionId").val() == "Reading" )){
						    	   $('#selectedDiv1').empty();	
						    	   $('#selectedDiv2').empty();	
						    	   $('#selectedDiv3').empty();	
						    	   $('#selectedDiv1').append(document1);
	      							$('#selectedDiv2').append(document2);
	      							$('#selectedDiv3').append(document3);
						       }
						       else if(($(this).val() == "Link") && (!$("#chosenSectionId").val() == "") && ($("#chosenSectionId").val() == "Examples" || $("#chosenSectionId").val() == "Toolbox" || $("#chosenSectionId").val() == "Reading" )){
						    	   $('#selectedDiv1').empty();	
							    	 $('#selectedDiv2').empty();	
							    	 $('#selectedDiv3').empty();	
							    	 $('#selectedDiv1').append(link1);	
	      							 $('#selectedDiv2').append(link2);
	      							$('#selectedDiv3').append(link3);	
						       }
						       else {
						    	   $('#selectedDiv1').empty();	
							    	 $('#selectedDiv2').empty();	
							    	 $('#selectedDiv3').empty();
						       }
						    });	
						
						
						
						
						
						$("#Document_form").validate(
								{	errorClass:"help-block animation-slideDown",
									errorElement:"div",
									errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
									highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
									success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
									
												rules : {		
													chosenModuleId: {
														required : !0
													},
													chosenCategoryId: {
														required : !0
													},
													chosenSectionId: {
														required : !0
													},
													docTitle1 : {
														required : !0,
														maxlength : 75
													},
													
													documentDescription1: {
														required : !0,
														maxlength : 500
													}, 
													document1: {
														required : !0,
														documentExtension: "doc|docx|pdf|xlsx|xls|pptx|ppt"
													},
													chosenLifeStage1: {
														required : !0
													},
													docTitle2 : {
														maxlength : 75
													},
													documentDescription2: {
														maxlength : 500
													}, 
													document2 : {
														documentExtension: "doc|docx|pdf|xlsx|xls|pptx|ppt"
													},
													docTitle3 : {
														maxlength : 75
													},
													documentDescription3: {
														maxlength : 500
													}, 
													document3 : {
														documentExtension: "doc|docx|pdf|xlsx|xls|pptx|ppt"
													},
													docTitle4 : {
														required : !0,
														maxlength : 75
													},
													
													documentDescription4: {
														required : !0,
														maxlength : 500
													}, 
													document4: {
														required : !0,
														blueprintExtension : "pptx|ppt"
													},
													chosenLifeStage4: {
														required : !0
													},
													linkTitle1 : {
														required : !0,
														maxlength : 75
													},
													linkName1: {
														required : !0,
														url :true,
														maxlength : 500
													},
													linkTitle2 : {
														maxlength : 75
													},
													linkName2: {
														url :true,
														maxlength : 500
													},
													linkTitle3 : {
														maxlength : 75
													},
													linkName3: {
														url :true,
														maxlength : 500
													},
													
												},
												messages : {
													
													chosenModuleId: {
														required : '<spring:message code="validation.selectmodule"/>'
													},
													chosenCategoryId: {
														required : '<spring:message code="validation.selectcategory"/>'
													},
													chosenSectionId: {
														required : '<spring:message code="validation.selectsection"/>'
													},
													docTitle1: {
														required: 'Please enter a Title',
														maxlength: 'Title should not be more than 75 characters'
													},
													documentDescription1: {
														required: 'Please enter a Description',
														maxlength: 'Description should not be more than 500 characters'
													},
													document1: {
														required: 'Please select a document',
														documentExtension: 'Please select a document with (.doc, .docx, .pdf, .xlsx, .xls, .ppt, .pptx) extension only'
													},
													chosenLifeStage1: {
														required: 'Please select a Life Stage'
													},
													docTitle2: {
														maxlength: 'Title should not be more than 75 characters'
													},
													documentDescription2: {
														maxlength: 'Description should not be more than 500 characters'
													},
													document2 : {
														documentExtension: 'Please select a document with (.doc, .docx, .pdf, .xlsx, .xls, .ppt, .pptx) extension only'
													},
													docTitle3: {
														maxlength: 'Title should not be more than 75 characters'
													},
													documentDescription3: {
														maxlength: 'Description should not be more than 500 characters'
													},
													document3 : {
														documentExtension: 'Please select a document with (.doc, .docx, .pdf, .xlsx, .xls, .ppt, .pptx) extension only'
													},
													docTitle4: {
														required: 'Please enter a Title',
														maxlength: 'Title should not be more than 75 characters'
													},
													documentDescription4: {
														required: 'Please enter a Description',
														maxlength: 'Description should not be more than 500 characters'
													},
													document4: {
														required: 'Please select a document',
														blueprintExtension : 'Please select a document with (.ppt, .pptx) extension only'
													},
													chosenLifeStage4: {
														required: 'Please select a Life Stage'
													},
													linkTitle1: {
														required: 'Please enter a Title',
														maxlength: 'Title should not be more than 75 characters'
													},
													linkName1: {
														required: 'Please enter a Link',
														url: 'Please enter a valid URL',
														maxlength: 'Link should not be more than 500 characters'
													},
													linkTitle2: {
														maxlength: 'Title should not be more than 75 characters'
													},
													linkName2: {
														url: 'Please enter a valid URL',
														maxlength: 'Link should not be more than 500 characters'
													},
													linkTitle3: {
														maxlength: 'Title should not be more than 75 characters'
													},
													linkName3: {
														url: 'Please enter a valid URL',
														maxlength: 'Link should not be more than 500 characters'
													},
												},
										});
						
						jQuery.validator.addMethod("documentExtension", function(value, element, param) {
							param = typeof param === "string" ? param.replace(/,/g, '|') : "doc|docx|pdf|xlsx|xls|pptx|ppt";
							return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
						}, jQuery.format("Please select images of (.doc, .docx, .pdf, .xlsx, .xls, .ppt, .pptx) extension."));
								
						jQuery.validator.addMethod("blueprintExtension", function(value, element, param) {
							param = typeof param === "string" ? param.replace(/,/g, '|') : "pptx|ppt";
							return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
						}, jQuery.format("Please select images of (.ppt, .pptx) extension."));

						$("#document_submit").click(function() {
// 							unsaved=false;
							if(($('#chosenSectionId').val() == "BluePrint") && $('input[name=contentType]:checked').val() == "Link"){
								return linknotValid();
							}
							$("#Document_form").submit();
						});
						
						

						
					});
	

</script>
<script>
function linknotValid(){
	alert("You cannot select Content Type 'Link' when the section is 'BluePrint'");
	return false;
}
</script>

<style>
.chosen-container {
	width: 250px !important;
}

#map-canvas {
	height: 300px
}
</style>
<%@include file="../../inc/template_end.jsp"%>






