<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>
<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				<i class="fa fa-map-marker"></i>
				<spring:message code="heading.coursecategorymaster" />
				<br> <small><spring:message
						code="heading.coursecategorymasterdetails" /></small>
			</h1>
			<span id="errorMsg"></span>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i> <spring:message code="label.success"/>
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>

			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">�</button>
					<h4>
						<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="heading.coursecategorymaster" /></li>
	</ul>
			
			<div class="block full" >
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.coursecategoryinfo" /></strong>
						</h2>
					</div>
					<form action="<%=contexturl %>ManageCourse/CourseCategoryList/UpdateCourseCategory" method="post" class="form-horizontal "
						id="Instructor_form" >
							<input name="categoryId" value="${courseCategory.categoryId}" type="hidden">
							
					<div class="col-md-12">
				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.coursecategoryinfo" /></strong>
						</h2>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="coursecategory_Name"><spring:message
								code="label.coursecategoryname" /><span class="text-danger">*</span> </label>
						<div class="col-md-6">

							<input id="coursecategory_Name" name="courseCategoryName" class="form-control"
								placeholder="<spring:message code='label.coursecategoryname'/>.."
								type="text" value="${courseCategory.categoryName}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="coursecategory_Short_Name"><spring:message
								code="label.coursecategoryshortname" /><span class="text-danger">*</span> </label>
						<div class="col-md-6">

							<input id="coursecategory_Short_Name" name="courseCategoryShortName" class="form-control"
								placeholder="<spring:message code="label.coursecategoryshortname"/>.."
								type="text" value="${courseCategory.shortName}">
						</div>
					</div>
					
				</div>
			</div>
				
					
					
					
					<div class="block">
			<div style="text-align: center; margin-bottom: 10px">
				<button id="user_submit" class="btn btn-sm btn-primary save"
					type="submit">
					<i class="fa fa-angle-right"></i>
					<spring:message code="button.save" />
				</button>
				<a href="<%=contexturl %>ManageCourse/CourseCategoryList/" class="btn btn-sm btn-primary"
					style="text-decoration: none;"><spring:message
						code="button.cancel" /></a>
			</div>
		</div>
	</form>
</div>



<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>

<script src="../resources/js/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
	
	$("#Instructor_form").validate(
			{	errorClass:"help-block animation-slideDown",
				errorElement:"div",
				errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
				highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
				success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
				rules:{ courseCategoryName : {required : !0,maxlength : 75},
					courseCategoryShortName : {required : !0,maxlength : 75}
				},
				messages:{
					courseCategoryName : {
						required :'<spring:message code="validation.pleaseentercoursecategoryname"/>',
						maxlength :'<spring:message code="validation.coursecategoryname75character"/>'
					},
					courseCategoryShortName : {
						required :'<spring:message code="validation.pleaseentercoursecategoryshortname"/>',
						maxlength :'<spring:message code="validation.coursecategoryshortname75character"/>'
					},
				},
			});
	
	 $("#user_submit").click(function(){
		 $("#Instructor_form").submit();
	 });

});
</script>