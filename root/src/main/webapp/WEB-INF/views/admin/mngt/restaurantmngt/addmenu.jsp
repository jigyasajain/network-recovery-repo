<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->

<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>

<style>
.sortable {
	list-style-type: none;
	margin: 0;
	padding: 0;
	width: 90%;
}

.menu_photo {
	width: 170px;
	text-align: left;
	font: 11px 'Calibri';
	color: #666;
	border: 10px solid #E6E6E6;;
	float: left;
	margin: 5px;
	position: relative;
	height: auto;
	display: table-cell;
}

#menuList {
	position: relative;
	height: auto;
	min-width: 100%;
	border: 2px dashed #eaedf1;
	background-color: #f9fafc;
	padding: 1em;
	display: table;
}

#showMenu {
	position: relative;
	height: auto;
	min-width: 100%;
	border: 2px dashed #eaedf1;
	background-color: #f9fafc;
	padding: 1em;
	display: table;
}

.menu_category_sequence,.menu_item_sequence {
	width: 100%;
	text-align: left;
	font: 13px;
	color: #666;
	background: #f5f5f5;;
	float: left;
	margin: 5px;
	min-height: 40px;
	vertical-align: middle;
	padding: 10px;
	border-radius: 4px;
	overflow: hidden;
}

#delete {
	float: right;
	margin-top: -8px;
	margin-left: 142px;
	padding-left: 4px;
	padding-right: 2px;
	position: absolute;
	z-index: 100000;
}

.menu_category {
	border: solid 1px #aaaaaa;
	padding: 15px;
	min-height: 150px;
	height: auto;
	margin-top: 50px;
	position: relative;
}

.menu_item {
	border: solid 1px #aaaaaa;
	padding: 10px;
	height: 165px;
}
</style>
<link rel="stylesheet"
	href="<%=contexturl%>resources/css/jquery-ui.css">

<div id="page-content" style="float: left; width: 100%;">
	<div class="content-header">
		<div class="header-section">
			<h1>
				<i class="fa fa-map-marker"></i>
				<spring:message code="heading.menu" />
				<br> <small><spring:message code="heading.menudetails" /></small>
			</h1>
			<span id="errorMsg"></span>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i>
						<spring:message code="label.success" />
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><strong><spring:message code="menu.restaurant" /></strong></li>
	</ul>
	<div class="block" style="position: relative; float: left; width: 100%; padding-bottom: 20px;">
		<!-- Search Styles Title -->
		<div class="block-title">
			<ul class="nav nav-tabs" data-toggle="tabs">
				<c:if test="${empty menuType}">
					<li class="active"><a href="#menu-image"><spring:message
								code="heading.imagemenu" /></a></li>
					<li><a href="#menu-item"><spring:message
								code="heading.menubuilder" /></a></li>
				</c:if>
				<c:if test="${!empty menuType}">
					<li><a href="#menu-image"><spring:message
								code="heading.imagemenu" /></a></li>
					<li class="active"><a href="#menu-item"><spring:message
								code="heading.menubuilder" /></a></li>
				</c:if>
			</ul>
		</div>
		<!-- END Search Styles Title -->

		<!-- Search Styles Content -->
		<div class="tab-content">
			<!-- Projects Search -->
			<div class="tab-pane <c:if test="${empty menuType}">active</c:if>"
				id="menu-image" style="min-height: 500px;">

				<!-- ######## Menu Image Actions ########## -->
				<div id="addmenu" class="btn btn-sm btn-primary save">
					<spring:message code="label.addmenu" />
				</div>
				<c:if test="${!empty imageList}">
					<div id="changeSequence" class="btn btn-sm btn-primary save">
						<spring:message code="label.changesequence" />
					</div>
				</c:if>
				<c:if test="${empty success}">
					<security:authorize
						ifAnyGranted="<%=Role.GORMET7_ADMIN.name() +','+ Role.COMPANY_ADMIN.name() +','+Role.BRAND_ADMIN.name()%>">
						<a href="<%=contexturl%>ManageRestaurant/RestaurantList"
							class="btn btn-sm btn-primary" title="Back to Restaturant"> 
							<spring:message code="label.restaurant" /></a>
					</security:authorize>
					<security:authorize
						ifAnyGranted="<%=Role.RESTAURANT_ADMIN.name()%>">
						<a href="ViewRestaurant?id=<%=user.getModuleId()%>"
							class="btn btn-sm btn-primary" title="Back to Restaturant"> 
							<spring:message code="label.restaurant" /></a>
					</security:authorize>
				</c:if>
				<c:if test="${!empty restaurantsaved}">
					<a href="<%=contexturl%>ManageRestaurant/RestaurantList"
						class="btn btn-sm btn-primary" title="Back to Restaturant"> 
						<spring:message code="label.menulater" /></a>
				</c:if>
				<a
					href="<%=contexturl %>ManageRestaurant/RestaurantList/MenuList?id=${outletId}"
					id="savemenu" class="btn btn-sm btn-primary save" style="display: none">
					<spring:message code="label.savemenu" />
				</a>


				<div style="height: 50px">
					<label class="col-md-2 control-label" for="imageMenu"><spring:message
							code="label.menuselection" /></label>
					<div class="col-md-2">
						<form id="imgItem_img_form"
							action="<%=contexturl%>ManageRestaurant/RestaurantList/UpdateImageMenu"
							method="post">
							<label class="switch switch-primary save"> <input
								id="imageMenu" name="imageMenu" type="checkbox"
								onchange="$('#imgItem_img_form').submit();"
								<c:if test="${imageMenu}"> checked </c:if> /> <span></span></label> <input
								type="hidden" name="outletId" value="${outletId}">
						</form>
					</div>
				</div>



				<!-- ######## Change Menu Image Sequence ########## -->

				<form
					action="<%=contexturl%>ManageRestaurant/RestaurantList/SaveMenuSequence"
					class="form-horizontal" style="display: none" id="sequenceForm">
					<input type="hidden" id="sequence" name="sequence" /> <input
						type="hidden" name="id" value="${outletId}" />
					<div id="menuList"
						style="height: 350px; border: 2px dashed #eaedf1; background-color: #f9fafc; padding: 1em;">
						<ul class="sortable">
							<c:forEach var="item" items="${imageList}">
								<li class="menu_photo"><img alt=""
									src="<%=contexturl %>${item.image.thumbImagePath}" width="150" />
									<input type="hidden" value="${item.menuImageId}"
									class="imageIds"></li>
							</c:forEach>
						</ul>
					</div>
					<div id="savesequence" class="btn btn-sm btn-primary save"
						style="margin-bottom: 50px;">
						<spring:message code="label.savesequense" />
					</div>
				</form>



				<!-- ######## Upload Menu Image ######### -->

				<div class="form-group" style="display: none; margin-top: 80px"
					id="uploadMenu">
					<div id="myId" class="dropzone dz-clickable"
						enctype="multipart/form-data">
						<div class="dz-default dz-message">
							<span><spring:message code="heading.uploadmsg" /> </span>
						</div>
					</div>
				</div>




				<!-- ######## Menu Image View ######### -->

				<div class="form-group" id="showMenu">
					<c:forEach var="item" items="${imageList}">
						<div class="menu_photo">
							<span id="delete"><a href="#delete_menuImg_popup"
								data-toggle="modal"
								onclick="deleteMenuImg(${item.image.imageId})" title="Delete"
								class="btn btn-xs btn-danger"> <i class="fa fa-times"></i></a> </span>
							<img alt="" src="<%=contexturl %>${item.image.thumbImagePath}"
								width="150" />
						</div>
					</c:forEach>
				</div>
			</div>

			<!-- ############################### MENU ITEM ############################################# -->

			<div class="tab-pane <c:if test="${!empty menuType}">active</c:if>"
				id="menu-item" style="min-height: 500px;">

				<!-- ############# Menu Item Actions ############## -->

				<div id="addmenuItem" class="btn btn-sm btn-primary save">
					<spring:message code="label.addmenu" />
				</div>
				<a
					href="<%=contexturl %>ManageRestaurant/RestaurantList/MenuList?id=${outletId}&menuType=ItemMenu"
					id="gobackmenuitem" style="display: none; padding-left: 14px;"
					class="btn btn-sm btn-primary"><spring:message code="label.goBack" /></a>
				<c:if test="${!empty menuItems}">
					<div id="changeCategorySequence" class="btn btn-sm btn-primary save">
						<spring:message code="label.changecategorysequence" />
					</div>
				</c:if>
				<!-- ############# Menu Item View ############## -->
				<div id="menuItemView">
					<c:choose>
						<c:when test="${!empty menuItems}">
							<div style="height: 50px">
								<label class="col-md-2 control-label" for="manualMenu"><spring:message code="label.useitemasmenu" /></label>
								<div class="col-md-2">
									<form id="menuItem_img_form" action="UpdateImageMenu"
										method="post">
										<label class="switch switch-primary save"> <input
											id="manualMenu" name="manualMenu" type="checkbox"
											onchange="$('#menuItem_img_form').submit();"
											<c:if test="${!imageMenu}"> checked </c:if> /> <span></span></label>
										<input type="hidden" name="menuType" value="ItemMenu">
										<input type="hidden" name="outletId" value="${outletId}">
									</form>
								</div>
							</div>
							<div id="category" class="panel-group">
								<c:forEach items="${menuItems}" var="menuCategory">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<i class="fa"></i> <a
													class="accordion-toggle control-label themed-color-autumn"
													data-toggle="collapse" data-parent="#category"
													href="#category_${menuCategory.menuCategoryId }"><strong>${menuCategory.menuCategoryName
														}</strong></a>
											</h4>
											<p style="float: right; margin-top: -22px">
												<span id="addMenu_${menuCategory.menuCategoryId}"
													class="btn btn-xs btn-success"
													title="<spring:message code="label.addmenuitem"/>">
													<i class="fa fa-plus"></i>
												</span>
												<c:if test="${! empty menuCategory.menuItems}">
													<span
														id="changeMenuSequence_${menuCategory.menuCategoryId}"
														class="btn btn-xs btn-default"
														title="<spring:message code="label.changemenusequence"/>">
														<i class="fa fa-pencil"></i>
													</span>
												</c:if>
												<a href="#delete_menucategory_popup" data-toggle="modal"
													onclick="deleteMenucategory(${menuCategory.menuCategoryId})"
													title="Delete Menu Category" class="btn btn-xs btn-danger"><i
													class="fa fa-times"></i> </a>
											</p>
										</div>
										<div id="category_${menuCategory.menuCategoryId }"
											class="panel-collapse collapse in">
											<div class="panel-body">
												<c:forEach items="${menuCategory.menuItems}" var="menuItem">
													<div class="row">
														<div class="col-md-3 control-label">
															${menuItem.name}</div>
														<div class="col-md-3 control-label">
															${menuItem.description}</div>
														<div class="col-md-3 control-label">
															${currency.currencySign} ${menuItem.rate}  </div>
														<div class="col-md-2">
															<a href="#delete_menuItem_popup" data-toggle="modal"
																onclick="deleteMenuItem(${menuItem.itemId},0)"
																title="Delete Menu Item" class="btn btn-xs btn-danger"><i
																class="fa fa-times"></i> </a>
														</div>
													</div>
												</c:forEach>
											</div>
										</div>
									</div>
								</c:forEach>
							</div>
						</c:when>
						<c:otherwise>
							<spring:message code="label.youdon'thaveanymenuitem" />
						</c:otherwise>
					</c:choose>
				</div>


				<!-- ########################## ADD Menu Item ###################### -->

				<form id="categoryForm"
					style="width: 100% !important; display: none">
					<input type="hidden" name="id" value="${outletId}" />
					<div class="form-group">
						<label class="col-md-2 control-label" for="category"><spring:message
								code="label.menucategory" /><span class="text-danger">*</span></label>
						<div class="col-md-3">
							<input id="category" name="menuCategory" class="form-control"
								placeholder="Menu Category..." type="text">
						</div>
						<div class="col-md-2">
							<div id="addmCategory" class="btn btn-sm btn-primary save">
								<spring:message code="label.addcategory" />
							</div>
						</div>
					</div>
				</form>
				<div id="menuItems"
					style="width: 100% !important; padding-top: 35px;"></div>


				<!-- ########################## Change Menu Item Category Sequence ###################### -->
				<form id="changeCategorySequnceForm"
					action="<%=contexturl%>ManageRestaurant/RestaurantList/SaveCategorySequence"
					style="display: none; margin-top: -40px;">
					<div id="savecategorysequence" class="btn btn-sm btn-primary save"
						style="margin-bottom: 15px; float: left;" type="submit">
						<spring:message code="label.savesequense" />
					</div>
					<div class="btn btn-sm btn-primary save">
						<a
							href="<%=contexturl %>ManageRestaurant/RestaurantList/MenuList?id=${outletId}&menuType=ItemMenu"><spring:message code="label.goBack" /></a>
					</div>
					<input type="hidden" id="sequence" name="sequence" /> <input
						type="hidden" name="id" value="${outletId}" />
					<c:choose>
						<c:when test="${!empty menuItems}">
							<ul class="sortable">
								<c:forEach items="${menuItems}" var="menuCategory">
									<li
										class="menu_category_sequence control-label themed-color-autumn">
										<strong>${menuCategory.menuCategoryName }</strong> <input
										type="hidden" value="${menuCategory.menuCategoryId}"
										class="menuCategoryIds">
									</li>

								</c:forEach>
							</ul>
						</c:when>
						<c:otherwise>
							<spring:message code="label.youdon'thaveanymenuitem" />
						</c:otherwise>
					</c:choose>
					<input type="hidden" id="categorySequence" name="categoryIds" />
				</form>
			</div>
		</div>
	</div>
</div>

<div id="delete_menuImg_popup" class="modal fade" tabindex="-1"
	role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<form
					action="<%=contexturl%>ManageRestaurant/RestaurantList/DeleteMenuImage"
					method="post" class="form-horizontal form-bordered"
					id="delete_menuImg_Form">
					<div style="padding: 10px; height: 110px;">
						<label><spring:message code="label.deleteimage" /></label>
						<div class="col-xs-12 text-right">
							<button type="button" class="btn btn-sm btn-default"
								data-dismiss="modal">
								<spring:message code="label.no" />
							</button>
							<div id="delete_menuImgYes" class="btn btn-sm btn-primary save">
								<spring:message code="label.yes" />
							</div>
							<input type="hidden" name="menuImgId" id="menuImgId"> <input
								type="hidden" name="outletId" id="outletId">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div id="delete_menuItem_popup" class="modal fade" tabindex="-1"
	role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<form
					action="<%=contexturl%>ManageRestaurant/RestaurantList/DeleteMenuItem"
					method="post" class="form-horizontal form-bordered"
					id="delete_menuitem_Form">
					<div style="padding: 10px; height: 110px;">
						<label><spring:message code="label.deletemenuitem" /></label>
						<div class="col-xs-12 text-right">
							<button type="button" class="btn btn-sm btn-default"
								data-dismiss="modal">
								<spring:message code="label.no" />
							</button>
							<div id="delete_menuitemYes" class="btn btn-sm btn-primary save">
								<spring:message code="label.yes" />
							</div>
							<input type="hidden" name="outletId" id="restId"> <input
								type="hidden" name="id" id="mennuitemId"> <input
								type="hidden" name="deletefrom" id="deletefrom">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div id="delete_menucategory_popup" class="modal fade" tabindex="-1"
	role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<form
					action="<%=contexturl%>ManageRestaurant/RestaurantList/DeleteMenuCategory"
					method="post" class="form-horizontal form-bordered"
					id="delete_menucategory_Form">
					<div style="padding: 10px; height: 110px;">
						<label><spring:message
								code="validation.deletemenucategory" /></label>
						<div class="col-xs-12 text-right">
							<button type="button" class="btn btn-sm btn-default"
								data-dismiss="modal">
								<spring:message code="label.no" />
							</button>
							<div id="delete_menucategoryYes" class="btn btn-sm btn-primary save">
								<spring:message code="label.yes" />
							</div>
							<input type="hidden" name="outletId" id="restaurantId"> <input
								type="hidden" name="id" id="menucatId">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<script src="<%=contexturl%>resources/js/vendor/jquery-ui.js"></script>
<script type="text/javascript">
	var selectedId = 0;
	var selectedMenucategoryId = 0;
	var deletefrom = 0;
	function deleteMenuItem(id, deletef){
		selectedId = id;
		deletefrom = deletef;
	}
	
	function deleteMenuImg(id){
		menuImgId = id;
	}
	
	$(document).ready(function() {
		
		$(".sortable").sortable();
		$(".sortable").disableSelection();
		
		Dropzone.autoDiscover = false;
		$("div#myId").dropzone(
				{
					acceptedFiles: "image/*",
					url : "<%=contexturl%>ManageRestaurant/RestaurantList/UploadMenu?id=${outletId}"
				});
		
		$("#delete_menuImgYes").click(function(){
			$("#menuImgId").val(menuImgId);
			$("#outletId").val(${outletId});
			$("#delete_menuImg_Form").submit();
		});
		
		$("#addmenu").click(function() {
			$("#uploadMenu").css("display", "block")
			$("#savemenu").css("display", "inline")
			$("#addmenu").css("display", "none")
			$("#showMenu").css("display", "none")
			$("#changeSequence").css("display", "none")
		})
		
		$("#changeSequence").click(function() {
			$("#sequenceForm").css("display", "block")
			$("#menuForm").css("display", "none")
			$("#addmenu").css("display", "none")
			$("#showMenu").css("display", "none")
			$("#changeSequence").css("display", "none")
		})
		
		$("#delete_menuitemYes").click(function(){
			$("#restId").val(${outletId});
			$("#mennuitemId").val(selectedId);
			$("#deletefrom").val(deletefrom);
			$("#delete_menuitem_Form").submit();
		});
		
		$("#delete_menucategoryYes").click(function(){
			$("#restaurantId").val(${outletId});
			$("#menucatId").val(selectedMenucategoryId);
			$("#delete_menucategory_Form").submit();
		});

		$("#savesequence").click(function() {
			var exp = "";
			$(".imageIds").each(function(index) {
				// 			 exp = exp+"," + (index +1)+ ":" + $( this ).val();
				exp = exp + "," + $(this).val();
			});
			$("#sequence").val(exp);
			$("#sequenceForm").submit();
		});

		$("#addmenuItem").click(function() {
			$("#categoryForm").css("display", "block")
			$("#addmenuItem").css("display", "none")
			$("#gobackmenuitem").css("display", "inline")
			$("#menuItemView").css("display", "none")
			$("#changeCategorySequence").css("display", "none")
		})
		
		$("#changeCategorySequence").click(function() {
			$("#categoryForm").css("display", "none")
			$("#addmenuItem").css("display", "none")
			$("#gobackmenuitem").css("display", "none")
			$("#menuItemView").css("display", "none")
			$("#changeCategorySequence").css("display", "none")
			$("#changeCategorySequnceForm").css("display","block")
		})
		
		
		
		$("#categoryForm").validate(
				{	errorClass:"help-block animation-slideDown",
					errorElement:"div",
					errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
					highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
					success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
					rules:{
						menuCategory:{required:!0,maxlength:75}
					},
					messages:{
						menuCategory:{required:'<spring:message code="validation.pleasementioncategoryname"/>',
							 maxlength:'<spring:message code="validation.menucategory75character"/>'}						
					},
					submitHandler: function() {
						var data = $("#categoryForm").serialize();
						$.ajax({
							type: "POST",
							async: false,
							url: "<%=contexturl%>ManageRestaurant/RestaurantList/SaveMenuCategory",
							data:data,
							success: function(data, textStatus, jqXHR){
								var i = data.trim().indexOf("errorCode");
								if(i == 2){
									var obj = $.parseJSON(data.trim());
									var html = '<div class="alert alert-danger alert-dismissable">	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="fa fa-times-circle"></i> Error</h4> <span >'+obj.errorCode+'</span></div>';
									$("#errorMsg").html(html);
								}
								else{
									$("#category").val("");
									$('#menuItems').append(data);
								}
							},
							dataType: 'html'
						});
	                }
				});
		
		
		$("#savecategorysequence").click(function() {
			var exp = "";
			$(".menuCategoryIds").each(function(index) {
				exp = exp + "," + $(this).val();
			});
			$("#categorySequence").val(exp);
			$("#changeCategorySequnceForm").submit();
		});
		
		 $("#addmCategory").click(function(){
			 $("#categoryForm").submit();
		 });
		 <c:forEach items="${menuItems}" var="category">
			 $("#addMenu_${category.menuCategoryId}").click(function(){
				 $.ajax({
						type: "POST",
						async: false,
						url: "<%=contexturl%>ManageRestaurant/RestaurantList/AddMenuItem",
						data:"id=${category.menuCategoryId}",
						success: function(data, textStatus, jqXHR){
							var i = data.trim().indexOf("errorCode");
							if(i == 2){
								var obj = $.parseJSON(data.trim());
								var html = '<div class="alert alert-danger alert-dismissable">	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="fa fa-times-circle"></i> Error</h4> <span >'+obj.errorCode+'</span></div>';
								$("#errorMsg").html(html);
							}
							else{
								$('#menu-item').html(data);
							}
						},
						dataType: 'html'
					});	                               				
			});
		
			$("#editCategory_${category.menuCategoryId}").click(function(){
				alert("editCategory");			                               				
			});
			
			$("#changeMenuSequence_${category.menuCategoryId}").click(function(){
				$.ajax({
					type: "POST",
					async: false,
					url: "<%=contexturl%>ManageRestaurant/RestaurantList/ChangeMenuItemSequence",
					data:"id=${outletId}&categoryId=${category.menuCategoryId}",
					success: function(data, textStatus, jqXHR){
						var i = data.trim().indexOf("errorCode");
						if(i == 2){
							var obj = $.parseJSON(data.trim());
							var html = '<div class="alert alert-danger alert-dismissable">	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="fa fa-times-circle"></i> Error</h4> <span >'+obj.errorCode+'</span></div>';
							$("#errorMsg").html(html);
						}
						else{
							$('#menu-item').html(data);
						}
					},
					dataType: 'html'
				});	                                        				
			});
		</c:forEach>
		
	});
	
	function deleteMenucategory(id){
		selectedMenucategoryId = id;
	}
	
</script>
<%@include file="../../inc/template_end.jsp"%>