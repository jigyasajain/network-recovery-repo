<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>



<div id="page-content">
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="fa fa-map-marker"></i>
                <spring:message code="heading.coursebatchMaster" />
                <br> <small><spring:message
                        code="heading.specifylistofcoursebatch" /> </small>
            </h1>
            <c:if test="${!empty success}">
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert"
                        aria-hidden="true">x</button>
                    <h4>
                        <i class="fa fa-check-circle"></i>
                        <spring:message code="label.success" />
                    </h4>
                    <spring:message code="${success}" />
                </div>
            </c:if>
            <div class="alert alert-danger alert-dismissable"
                style="display: none" id="error">
                <button type="button" class="close" data-dismiss="alert"
                    aria-hidden="true">x</button>
                <h4>
                    <i class="fa fa-times-circle"> </i> <spring:message code="label.error"/>
                </h4>
                <span id="errorMsg"></span>
            </div>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><spring:message code="heading.managecoursebatch" /></li>
        <li><a href="#"><spring:message code="heading.coursebatchmaster" /></a></li>
    </ul>
    
    <div class = "block">
    <div class="block full  gridView" >
        <div class="block-title">
            <h2>
                <strong><spring:message code="heading.activecoursebatch" /></strong>
            </h2>
        </div>
        <a href="<%=contexturl %>ManageCourseBatch/CourseBatchList/AddCourseBatch" id="addInstructor" class="btn btn-sm btn-primary"><spring:message
                code="label.addcoursebatch" /></a>
        <div class="table-responsive">
            <table id="active-datatable"
                class="table table-vcenter table-condensed table-bordered">
                <thead>
                    <tr>
                        <th class="text-center"><spring:message code="label.id" /></th>                     
                        <th class="text-center"><spring:message code="label.coursename" /></th>
                        <th class="text-center"><spring:message code="label.startdate" /></th>
                        <th class="text-center"><spring:message code="label.enddate" /></th>
                        <th class="text-center"><spring:message code="label.batchstatus" /></th>
                        <th class="text-center"><spring:message code="label.createdby" /></th>
                        <th class="text-center"><spring:message code="label.actions" /></th>
                    </tr>
                </thead>
                <tbody id="tbody"> 
                    <c:forEach var="currentCourseBatch" items="${activeList}" 
                        varStatus="count"> 

                        <tr> 
                            <td class="text-center">${count.count}</td> 
                            <td class="text-left"><c:out 
                                    value="${currentCourseBatch.courseBatch.course.courseName}" /></td> 
                            <td class="text-right"><fmt:formatDate
                                    pattern="<%=propvalue.specialDateFormat %>" value="${currentCourseBatch.courseBatch.startDate}" /></td> 
                            <td class="text-right"><fmt:formatDate
                                    pattern="<%=propvalue.specialDateFormat %>" value="${currentCourseBatch.courseBatch.endDate}" /></td> 
                            <td class="text-left"><c:out 
                                    value="${currentCourseBatch.courseBatch.status}" /></td>
                            <td class="text-left"><c:out 
                                    value="${currentCourseBatch.courseBatch.createdBy.firstName} ${currentCourseBatch.courseBatch.createdBy.lastName}" /></td>          

                            <td class="text-center">
                                    <div class="btn-group"> 
                                    <c:if test = "${currentCourseBatch.courseBatch.status eq status }" >
                                    <a href="<%=contexturl %>ManageCourseBatch/CourseBatchList/EditCourseBatch?id=${currentCourseBatch.courseBatch.courseBatchId}" 
                                        data-toggle="tooltip" title="Edit" 
                                        class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a> 
                                    </c:if>
                                    
                                    <c:if test = "${(currentCourseBatch.courseBatch.status eq status2) or (currentCourseBatch.courseBatch.status eq status1)}" >
                                    <a href="<%=contexturl %>ManageCourseBatch/CourseBatchList/ViewCourseBatch?id=${currentCourseBatch.courseBatch.courseBatchId}" 
                                        data-toggle="tooltip" title="View" 
                                        class="btn btn-xs btn-default"><i class="fa fa-file-text-o fa-fw"></i></a> 
                                    </c:if>
                                    
                                    <a href="#delete_course_batch_popup" data-toggle="modal" 
                                        onclick="deleteCourseBatch(${currentCourseBatch.courseBatch.courseBatchId})" 
                                        title="Delete" class="btn btn-xs btn-danger"><i 
                                        class="fa fa-times"></i></a> 
                                    </div>
                                    
                                
                            </td> 
                        </tr>
                    </c:forEach>

                </tbody> 
            </table>
        </div>
    </div>


    <div class="block full  gridView">
        <div class="block-title">
            <h2>
                <strong><spring:message code="heading.inactiveinstructor" /></strong>
            </h2>
        </div>
        <div class="table-responsive">
            <table id="inactive-datatable"
                class="table table-vcenter table-condensed table-bordered ">
                <thead>
                    <tr>
                        <th class="text-center"><spring:message code="label.id" /></th>
                        <th class="text-center"><spring:message code="label.coursename" /></th>
                        <th class="text-center"><spring:message code="label.startdate" /></th>
                        <th class="text-center"><spring:message code="label.enddate" /></th>
                        <th class="text-center"><spring:message code="label.batchstatus" /></th>
                        <th class="text-center"><spring:message code="label.createdby" /></th>
                        <th class="text-center"><spring:message code="label.actions" /></th>
                    </tr>
                </thead>
                <tbody id="tbody"> 
                    <c:forEach var="currentCourseBatch" items="${inActiveList}" 
                        varStatus="count"> 

                        <tr> 
                            <td class="text-center">${count.count}</td> 
                            <td class="text-left"><c:out 
                                    value="${currentCourseBatch.courseBatch.course.courseName}" /></td> 
                            <td class="text-right"><fmt:formatDate
                                    pattern="<%=propvalue.specialDateFormat %>" value="${currentCourseBatch.courseBatch.startDate}" /></td> 
                            <td class="text-right"><fmt:formatDate
                                    pattern="<%=propvalue.specialDateFormat %>" value="${currentCourseBatch.courseBatch.endDate}" /></td> 
                            <td class="text-left"><c:out 
                                    value="${currentCourseBatch.courseBatch.status}" /></td>
                            <td class="text-left"><c:out 
                                    value="${currentCourseBatch.courseBatch.createdBy.firstName} ${currentCourseBatch.courseBatch.createdBy.lastName}" /></td> 
                            <td class="text-center"> 
                                <div class="btn-group"> 
                                    <a href="#restore_Course_Batch_popup" data-toggle="modal"
                                        onclick="restoreCourseBatch(${currentCourseBatch.courseBatch.courseBatchId})" 
                                        title="Restore" class="btn btn-xs btn-danger"><i
                                        class="fa fa-times"></i> </a> 
                                </div> 
                            </td> 
                        </tr> 
                    </c:forEach> 
                </tbody> 
            </table>
        </div>
    </div>
    <div id="delete_course_batch_popup" class="modal fade" tabindex="-1"
        role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <form action="<%=contexturl %>ManageCourse/CourseBatchList/DeleteCourseBatch" method="post" class="form-horizontal form-bordered"
                        id="delete_course_batch_Form">
                        <div style="padding: 10px; height: 110px;">
                            <label><spring:message
                                    code="validation.doyouwanttodeletethiscoursebatch" /></label>
                            <div class="col-xs-12 text-right">
                                <button type="button" class="btn btn-sm btn-default"
                                    data-dismiss="modal">
                                    <spring:message code="label.no" />
                                </button>
                                <div id="delete_course_batch" class="btn btn-sm btn-primary">
                                    <spring:message code="label.yes" />
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="id" id="deleteCourseBatchId">

                    </form>

                </div>
            </div>
        </div>
    </div>


    <div id="restore_Course_Batch_popup" class="modal fade" tabindex="-1"
        role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <form action="<%=contexturl %>ManageCourse/CourseBatchList/RestoreCourseBatch" method="post" class="form-horizontal form-bordered" id="restore_course_batch_Form">
                        
                        <div style="padding: 10px; height: 110px;">
                            <label><spring:message
                                    code="validation.doyouwanttorestorethiscoursebatch" /></label>
                            <div class="col-xs-12 text-right">
                                <button type="button" class="btn btn-sm btn-default"
                                    data-dismiss="modal"><spring:message code="label.no" /></button>
                                <div id="restore_course_batch" class="btn btn-sm btn-primary">  <spring:message code="label.yes" /></div>
                            </div>
                        </div>
                        <input type="hidden" name="id" id="restoreCourseBatchId">
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>

</div>
</div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<script
    src="<%=contexturl%>resources/js/pages/activeTablesDatatables.js"></script>
<script
    src="<%=contexturl%>resources/js/pages/inactiveTablesDatatables.js"></script>
<script>$(function(){ ActiveTablesDatatables.init(6); });</script>
<script>$(function(){ InActiveTablesDatatables.init(6); });</script>
<script type="text/javascript">
    var selectedId = 0;
    var restoreId=0;
    $(document).ready(function() {
        
        $("#restore_course_batch").click(function(){
            
            $("#restoreCourseBatchId").val(restoreId);
            $("#restore_course_batch_Form").submit();
        });
        
        
        $("#delete_course_batch").click(function(){
            
            $("#deleteCourseBatchId").val(selectedId);
            $("#delete_course_batch_Form").submit();
        });
    });
    
    function deleteCourseBatch(id){
        selectedId = id;
    }
    function restoreCourseBatch(id){
        restoreId = id;
    }
</script>
<%@include file="../../inc/template_end.jsp"%>