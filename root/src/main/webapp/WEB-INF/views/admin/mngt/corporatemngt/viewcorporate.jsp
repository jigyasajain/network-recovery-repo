<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>
<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				<i class="gi gi-crown"></i>
				<spring:message code="heading.corporate" />
				<br> <small><spring:message
						code="heading.corporatedetails" /></small>
			</h1>
			<span id="errorMsg"></span>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i> <spring:message code="label.success"/>
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>
			
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">�</button>
					<h4>
						<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>
		</div>
	</div>
<ul class="breadcrumb breadcrumb-top">
		<li><a href="<%=contexturl %>ManageCorporate/CorporateList/EditCorporate?id=${corporate.corporateId}" id="brand">Edit Corporate</a></li>
	</ul>
	<form action="UpdateCorporate" method="post" class="form-horizontal "
		id="Corporate_form" enctype="multipart/form-data">
		<div class="row">
			
				
			<div class="col-md-6">
				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.corporateinfo" /></strong>
						</h2>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="corporate_Name"><spring:message
								code="label.corporatename" /> <span class="text-danger">*</span> </label>
						<div class="col-md-6">

							<input id="corporate_Name" name="corporateName" class="form-control"
								placeholder="<spring:message code='label.corporatename'/>.."
								type="text" value="${corporate.corporateName}" disabled="disabled">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="example-daterange1"><spring:message
								code="label.selectdaterange" /> </label>
						<div class="col-md-8">

							<label class="col-md-4 control-label" for="example-daterange1">
								${corporate.compAgreementStart} </label> <label
								class="col-md-4 control-label" for="example-daterange1">
								${corporate.compAgreementEnd} </label>
						</div>
					</div>


					<div class="form-group">
						
					
							<label class="col-md-4 control-label" for="example-daterange1"><spring:message
									code="label.agreementdoc" /> </label>
							<div class="col-md-8">
	                            <p class="form-control-static">${corporate.agreementFileName}
	                            	
	                            </p>
	                        </div>
						

					</div>
					<c:if test="${!empty corporate.agreementFileUlr}">
					<div class="form-group">
						<label class="col-md-4 control-label" for="example-daterange1">
							<spring:message code="label.changeagreement"/></label>
						<div class="col-md-6">
							<input type="file" name="file" accept="application/pdf"
								size="50" disabled="disabled"/>
						</div>
					</div>
					</c:if>
					<div class="form-group">
						<label class="col-md-4 control-label" for="domainName"><spring:message
								code="label.domainName" /><span class="text-danger">*</label>
						<div class="col-md-9" style="width: 66%;">
							<textarea id="domainName" name="domainName"
								class="form-control" rows="3"
								placeholder="<spring:message code="label.domainName"/>.." disabled="disabled">${corporate.domainName}</textarea>
					</div>
			</div>
				</div>
				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.contactinfo" /></strong>
						</h2>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="telephone1"><spring:message
								code="label.telephone1" /><span class="text-danger">*</span></label>
						<div class="col-md-6">

							<input id="corporateTelephone1" name="corporateTelephone1"
								class="form-control"
								placeholder="<spring:message code='label.telephone1'/>.."
								type="text" value="${corporate.corporateTelephone1}" disabled="disabled">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="telephone2"><spring:message
								code="label.telephone2" /></label>
						<div class="col-md-6">

							<input id="corporateTelephone2" name="corporateTelephone2"
								class="form-control"
								placeholder="<spring:message code='label.telephone2'/>.."
								type="text" value="${corporate.corporateTelephone2}" disabled="disabled">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="rest_email"><spring:message
								code="label.corporateemail" /><span class="text-danger">*</span></label>
						<div class="col-md-6">

							<input id="corporate_email" name="corporateEmail"
								class="form-control"
								placeholder="<spring:message code="label.corporateemail"/>.."
								type="text" value="${corporate.corporateEmail}" disabled="disabled">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="mobile1"><spring:message
								code="label.mobile1" /><span class="text-danger">*</span></label>
						<div class="col-md-6">

							<input id="corporateMobile1" name="corporateMobile1"
								class="form-control"
								placeholder="<spring:message code="label.mobile1"/>.."
								type="text" value="${corporate.corporateMobile1}" disabled="disabled">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="mobile2"><spring:message
								code="label.mobile2" /></label>
						<div class="col-md-6">

							<input id="corporateMobile2" name="corporateMobile2"
								class="form-control"
								placeholder="<spring:message code="label.mobile2"/>.."
								type="text" value="${corporate.corporateMobile2}" disabled="disabled">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="addressline1"><spring:message
								code="label.addressline1" /><span class="text-danger">*</span>
						</label>
						<div class="col-md-6">

							<input id="corporateAddressline1" name="corporateAddressLine1"
								class="form-control"
								placeholder="<spring:message code="label.addressline1"/>.."
								type="text" value="${corporate.corporateAddressLine1}" disabled="disabled">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="addressline2"><spring:message
								code="label.addressline2" /></label>
						<div class="col-md-6">

							<input id="corporateAddressline2" name="corporateAddressLine2"
								class="form-control"
								placeholder="<spring:message code="label.addressline2"/>.."
								type="text" value="${corporate.corporateAddressLine2}" disabled="disabled">
						</div>
					</div>

				
					
						
					<div class="form-group">
							<label class="col-md-4 control-label" for="country"><spring:message
									code="label.countryname" /><span class="text-danger">*</span></label>
							<div class="col-md-6">
								<input id="country" name="country" disabled="disabled"
									class="form-control" value="${corporate.corporateCountry.countryName}"
								type="text">
							</div>
						</div>
					
					<div class="form-group">
							<label class="col-md-4 control-label" for="city"><spring:message
									code="label.countryname" /><span class="text-danger">*</span></label>
							<div class="col-md-6">
								<input id="city" name="city" disabled="disabled"
									class="form-control" value="${corporate.corporateCity.cityName}"
									
									type="text">
							</div>
						</div>

					<div class="form-group">
							<label class="col-md-4 control-label" for="area"><spring:message
									code="label.countryname" /><span class="text-danger">*</span></label>
							<div class="col-md-6">
								<input id="area" name="area" disabled="disabled"
									class="form-control" value="${corporate.corporateArea.areaName}"
									
									type="text">
							</div>
						</div>
					

					<div class="form-group">
						<label class="col-md-4 control-label" for="corporatePincode"><spring:message
								code="label.pincode" /><span class="text-danger">*</span></label>
						<div class="col-md-6">

							<input id="corporatePincode" name="corporatePincode"
								class="form-control"
								placeholder="<spring:message code="label.pincode"/>.."
								type="text" value="${corporate.corporatePincode}" disabled="disabled">
						</div>
					</div>




				</div>
				
			</div>
			<div class="col-md-6">
				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.admininfo" /></strong>
						</h2>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="first_Name"><spring:message
								code="label.firstname" /><span class="text-danger">*</span> </label>
						<div class="col-md-6">

							<input id="first_Name" name="firstName" class="form-control"
								placeholder="<spring:message code='label.firstname'/>.."
								type="text" value="${corporate.corporateAdmin.firstName}" disabled="disabled">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="last_Name"><spring:message
								code="label.lastname" /><span class="text-danger">*</span> </label>
						<div class="col-md-6">

							<input id="last_Name" name="lastName" class="form-control"
								placeholder="<spring:message code="label.lastname"/>.."
								type="text" value="${corporate.corporateAdmin.lastName}" disabled="disabled">
						</div>
					</div>


					<div class="form-group">
						<label class="col-md-4 control-label" for="email"><spring:message
								code="label.emailid" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<input id="email" name="emailId" class="form-control"
								placeholder="test@example.com" type="text"
								value="${corporate.corporateAdmin.emailId}" disabled="disabled">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="mobile"><spring:message
								code="label.mobile" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<input id="mobile" name="mobile" class="form-control"
								placeholder='<spring:message code="label.mobile"></spring:message>'
								type="text" value="${corporate.corporateAdmin.mobile}" disabled="disabled">
						</div>
					</div>
					<div class="form-group">
                        <label class="col-md-4 control-label"><spring:message code="label.userlogin"/></label>
                        <div class="col-md-8">
                            <p class="form-control-static">${corporate.corporateAdmin.loginId}</p>
                        </div>
                    </div>
					
				</div>
			</div>
		</div>
		
	</form>

	
</div>
<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>

<script
	src="../resources/js/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>

