<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>


<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>

			<h1>
<!-- 				<i class="fa fa-map-marker"></i> -->
				<spring:message code="heading.globalsponsoredcontactimagemaster" />
				<br>
			</h1>
			<span id="errorMsg"></span>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="heading.manageglobalsponsoredcontactimage" /></li>
	</ul>
	<form action="<%=contexturl%>ManageGlobalSponsoredImage/GlobalSponsoredImageList/SaveGlobalSponsoredImage"
		method="post" class="form-horizontal ui-formwizard" id="Image_form"
		enctype="multipart/form-data">
		<div class="row">

			<div class="">
				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.imageinfo" /></strong>
						</h2>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="imageType"><spring:message
								code="label.imageType" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<c:choose>
								<c:when test="${not empty sponsorMaster.imageType}">
									<input id="sponsoredImage" name="imageType" class="imageType"
										type="radio" value="Global Sponsor"
										<c:if test="${sponsorMaster.imageType eq 'Global Sponsor'}"> checked = "checked"</c:if>>Global Sponsor Image<br>
									<input id="contactImage" name="imageType" class="imageType"
										type="radio" value="Contact"
										<c:if test="${sponsorMaster.imageType eq 'Contact'}">checked = "checked"</c:if>>Contact Image
								</c:when>
								<c:otherwise>
									<input id="sponsoredImage" name="imageType" class="imageType"
										type="radio" value="Global Sponsor">Global Sponsor Image<br>
									<input id="contactImage" name="imageType" class="imageType"
										type="radio" value="Contact" checked="checked">Contact Image
								</c:otherwise>
							</c:choose>
						</div>
					</div>

					<div id="temporaryContactDiv" style="display: none">
						<div class="form-group row" id="profilepic">
							<label class="col-md-4 control-label" for="offer_validtill">
								Atma Contact Image <span class="text-danger">*</span>
							</label>
							<div class="col-md-8">
								<div class="img-list2" id="imagediv">
									<!-- 						 class="img-circle pic"  -->
									<img border="0"
										src="<%=contexturl%>resources/img/application/default_profile.png"
										width="120" height="120" title="Click here to change photo"
										onclick="OpenFileDialog();" id="profileimage"
										style="cursor: pointer;" /> <span class="text-content"
										onclick="OpenFileDialog();" title="Click here to change photo"><br>
										<br> <span>Upload Contact Image</span></span>
								</div>
								<div>
									<input type="file" name="imageChooser1" id="imageChooser1"
										style="visibility: hidden; height: 0px" />
								</div>
							</div>

						</div>
					</div>
					<div id="temporarySponsorDiv" style="display: none">
						<div class="form-group" id="bpImages">
							<label class="col-md-4 control-label" for="document4">Global
								Sponsor Images <span class="text-danger">*</span>
							</label>
							<div id="myId" class="dropzone dz-clickable col-md-6"
								enctype="multipart/form-data">
								<div class="dz-default dz-message">
									<span>Upload Images here</span>
								</div>
							</div>
						</div>
					</div>

					<c:if test="${sponsorMaster.imageType eq 'Global Sponsor'}">
						<div class="form-group" id="bpImages">
							<label class="col-md-4 control-label" for="document4">Global
								Sponsor Images <span class="text-danger">*</span>
							</label>
							<div id="myId" class="dropzone dz-clickable col-md-6"
								enctype="multipart/form-data">
								<div class="dz-default dz-message">
									<span>Upload Images here</span>
								</div>
							</div>
						</div>
					</c:if>


				</div>
			</div>
		</div>
		<div style="text-align: center; margin-bottom: 10px">
			<div class="form-group form-actions">
				<!-- 				<div class="col-md-9 col-md-offset-3"> -->
				<button id="imageForm_submit" type="submit" value="Validate!"
					class="btn btn-sm btn-primary save">
					<i class="fa fa-angle-right"></i>
					<spring:message code="button.save" />
				</button>
				<a class="btn btn-sm btn-primary"
					href="<%=contexturl%>ManageGlobalSponsoredImage/GlobalSponsoredImageList"></i>
					<spring:message code="button.cancel" /> </a>
				<!-- 				</div> -->
			</div>
		</div>
	</form>



</div>
<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<%@include file="../../inc/chosen_scripts.jsp"%>
<script src="<%=contexturl %>resources/js/custom/featurecontrol.js"></script>
<script src="<%=contexturl %>resources/js/additional-methods.js"></script>
<!--

//-->

<script type="text/javascript"
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCheQF_yjKacwuVL4_59EO-3BThy85gbu4&sensor=false&region=GB">
	
</script>

<script type="text/javascript">
	$(document)
			.ready(
				function() {
					$("#manageGlobalSponsoredImage").addClass("active");
					
					Dropzone.autoDiscover = false;
					var i=0;
					$("div#myId")
							.dropzone(
									{
										acceptedFiles: "image/*",
										url : "<%=contexturl %>ManageGlobalSponsoredImage/GlobalSponsoredImageList/UploadSponsorImage"
									});
					
					var sponsorsDiv = $("#temporarySponsorDiv").contents();
					var contactDiv = $("#temporaryContactDiv").contents();
					
					if($("input[name='imageType']:checked").val() == "Contact"){
// 						$('#selectedDiv2').empty();
// 						$('#selectedDiv2').append(contactDiv);
						$('#temporaryContactDiv').show();
						$('#temporarySponsorDiv').hide();
					}
					
					
					$(".imageType").change(function(){
						if($("input[name='imageType']:checked").val() == "Global Sponsor"){
							$('#temporaryContactDiv').hide();
							$('#temporarySponsorDiv').show();
						}
						else{
							$('#temporarySponsorDiv').hide();
							$('#temporaryContactDiv').show();
							
						}
					});

						$("#Image_form").validate(
						{	errorClass:"help-block animation-slideDown",
							errorElement:"div",
							errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
							highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
							success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
										rules : {
											imageChooser1: {
											     required: true,
											     extension: "jpg|jpeg",
											     filesize: 900000,
											     maxlength: 1000
											   }
											
										},
										messages : {
											imageChooser1 : {
							                      required : 'Please select image',
							                      extension : 'Please select image of (.jpg,.jpeg) extension.',
							                   	 maxlength :'The Image is too long. Please select another image'
							           }
												
											
										}
								});

						$("#imageForm_submit").click(function() {
							unsaved=false;
							$("#Image_form").submit();
						});
						
				});
						
</script>

<script type="text/javascript">
	function OpenFileDialog(){
		document.getElementById("imageChooser1").click();
	}
	
</script>

<script type="text/javascript">
    function showimages(files) {
        for (var i = 0, f; f = files[i]; i++) {
           
                       
            var reader = new FileReader();
            reader.onload = function (evt) {
            	$( "#image" ).remove();
            	$( "#imagediv" ).html("");
            	
//                 var img = '<div class="col-md-6 id="imagediv"><img border="0" src="' + evt.target.result + '" width="120" height="120" title="Click here to change photo" style="cursor: pointer;"  onclick="OpenFileDialog();" class="img-circle pic" id="image"/></div>';
                  var img ='<img border="0" src="' + evt.target.result + '" width="120" height="120" title="Click here to change image" onclick="OpenFileDialog();"  id="image" style="cursor: pointer;" /><span class="text-content" onclick="OpenFileDialog();" title="Click here to change image"><br><br><span>Change Contact Image</span></span>'
                 $('#imagediv').html(img);
            }
            reader.readAsDataURL(f);
        }
    }

    $('#imageChooser1').change(function (evt) {
        showimages(evt.target.files);
    });
</script>
<style>
.chosen-container {
	width: 250px !important;
}
#map-canvas {
	height: 300px
}
</style>
<%@include file="../../inc/template_end.jsp"%>