<%@page import="com.astrika.common.model.TimeZoneEnum"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>

<style>

.chosen-container {
	width: 250px !important;
}
 #map-canvas { height: 300px;}
</style>

<div id="page-content">
<div class="content-header">
	<div class="header-section">
		<h1>
			<i class="fa fa-map-marker"></i>
			<spring:message code="heading.departmentmaster" />
			<br> <small><spring:message
					code="heading.specifylistofdepartment" /> </small>
		</h1>
		<span id="errorMsg"></span>
		<c:if test="${!empty success}">
			<div class="alert alert-success alert-dismissable">
				<button type="button" id="success_close" class="close" data-dismiss="alert"
					aria-hidden="true">x</button>
				<h4>
					<i class="fa fa-check-circle"></i> <spring:message code="label.success"/>
				</h4>
				<spring:message code="${success}" />
			</div>
		</c:if>
		<c:if test="${!empty error}">
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert"
					aria-hidden="true">x</button>
				<h4>
					<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
				</h4>
				<spring:message code="${error}" />
			</div>
		</c:if>
	</div>
</div>
<ul class="breadcrumb breadcrumb-top">
	<li><spring:message code="menu.managedepartments" /></li>
	<li><a href="#"><spring:message code="label.department" /></a></li>
</ul>
<div class="block full" id="formView">
	<div class="block-title">
		<h2>
			<strong><spring:message code="label.editdepartment" /></strong>
		</h2>
	</div>
	<form action="<%=contexturl %>ManageDepartment/DepartmentList/UpdateDepartment" method="post" class="form-horizontal form-bordered"
		id="Department_form">
		
		<div class="form-group">
			<input id="department_Id" name="departmentId" type="hidden" value="${department.departmentId}"> <label
				class="col-md-3 control-label" for="department_Name"><spring:message
					code="label.departmentname" /><span class="text-danger">*</span></label>
			<div class="col-md-9">
				<input id="department_Name" name="departmentName" class="form-control"
					placeholder="<spring:message code="label.departmentname"/>.."
					type="text" value="${department.departmentName}">
			</div>
		</div>





					

         
      <div class="form-group form-actions">
			<div class="col-md-9 col-md-offset-3">
				<div id="area_submit" class="btn btn-sm btn-primary save">
					<i class="fa fa-angle-right"></i> <spring:message code="button.save" />
				</div>
			
				<a class="btn btn-sm btn-primary" id="cancel" href="<%=contexturl %>ManageDepartment/DepartmentList">
					
					<spring:message code="button.cancel" />
				</a>
			</div>
		</div>
	</form>
</div>
</div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>

<script type="text/javascript">
	$(document).ready(function() {
		
		
		
		$("#Department_form").validate(
			{	errorClass:"help-block animation-slideDown",
				errorElement:"div",
				errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
				highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
				success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
				rules:{ departmentName:{required:!0,maxlength : 75},
					    
				},
				
				messages:{departmentName:{required:'<spring:message code="validation.department"/>',
					maxlength :'<spring:message code="validation.department75character"/>'}
				
			
				
			}});
			
			
			 $("#area_submit").click(function(){
				 $("#Department_form").submit();
			 });
			
	});
	
	
</script>
<%@include file="../../inc/template_end.jsp"%>
