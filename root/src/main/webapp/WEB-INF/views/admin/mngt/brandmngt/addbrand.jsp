<%@page import="com.astrika.outletmngt.model.BrandType"%>
<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>

<style>
.wizard-steps span {
width: 230px !important;
}
</style>
<div id="page-content">
<div class="content-header">
    <div class="header-section">
       <c:if test="${!empty error}">
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert"
                        aria-hidden="true">x</button>
                    <h4>
                        <i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
                    </h4>
                    <spring:message code="${error}" />
                </div>
            </c:if>
        <h1>
            <i class="fa fa-map-marker"></i> <spring:message code="heading.brand"/><br> <small><spring:message code="heading.createbrand"/></small>
        </h1>
        <span id="errorMsg"></span>
    </div>
</div>
<ul class="breadcrumb breadcrumb-top">
    <li><spring:message code="menu.brand"/> </li>
</ul>
<form action="<%=contexturl %>ManageBrand/BrandList/SaveBrand" method="post"
            class="form-horizontal ui-formwizard" id="Brand_form">
        <div style="display: block;" id="advanced-first"
            class="step ui-formwizard-content">
            <div class="wizard-steps">
                <div class="row">
                    <div class="col-xs-6 active">
                        <span><spring:message code="wizard.branddetail" /></span>
                    </div>
                    <div class="col-xs-6 done">
                        <span><spring:message code="wizard.brandtermsandconditions" /></span>
                    </div>
                </div>
            </div>
            <input type="hidden" name="brandType" value="<%=BrandType.RESTAURANT.getId()%>">
       <div class="row">
        
            <div class="col-md-6">
                <div class="block">
                    <div class="block-title">
                        <h2>
                            <strong><spring:message code="heading.brandinfo"/></strong> 
                        </h2>
                    </div>
                    <security:authorize ifAnyGranted="<%=Role.GORMET7_ADMIN.name() %>">
                    <div class="form-group" id="companyDiv">
                        <label class="col-md-4 control-label" for="company_Name"><spring:message code="label.companyname"/><span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            
                                <select class="chosen" style="width:200px;" id="company_Name" name="chosenCompanyId" data-placeholder="<spring:message code='label.choosecompany' />">
                                    <option value=""></option>
                                    <c:forEach items="${companyList}" var="company">
                                                <option value="${company.companyId}" <c:if test="${company.companyId eq brand.company.companyId}">selected="selected"</c:if>>${company.companyName}</option>
                                    </c:forEach>
                                </select>
                            
                            
                        </div>
                    </div>
                    </security:authorize>
                    <security:authorize ifAnyGranted="<%=Role.COMPANY_ADMIN.name() %>">
                                <input  type="hidden" name="chosenCompanyId" value="<%= user.getModuleId()%>">
                            </security:authorize>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="brand_Name"><spring:message code="label.brandname"/> <span class="text-danger">*</span>
                        </label>
                        <div class="col-md-6">
                            
                                <input id="brand_Name" name="brandName" class="form-control"
                                    placeholder="<spring:message code='label.brandname'/>.." type="text" value="${brand.brandName}"> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="gourmet7_Reach"><spring:message code="label.incudeinmembership"/></label>
                        <div class="col-md-6">
                            <label class="switch switch-primary">
                                <input  id="include" name="include" type="checkbox"  />
                            <span></span></label>
                        </div>
                        <div><a href="#includeTermsCond" data-toggle="modal" >Terms & Condition</a></div>
                    </div>
                </div>
                
                
            
            
            <div class="block">
                    <div class="block-title">
                        <h2>
                            <strong><spring:message code="heading.contactinfo" /></strong>
                            <small style="float:left; margin-top: 4px;">All your Gourmet7 related communication will be done on these details. * These details can be same as your admin details.</small>
                        </h2>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="telephone1"><spring:message
                                code="label.telephone1" /><span class="text-danger">*</span></label>
                        <div class="col-md-6">

                            <input id="brandTelephone1" name="brandTelephone1"
                                class="form-control"
                                placeholder="<spring:message code='label.telephone1'/>.."
                                type="text" value="${brand.brandTelephone1}" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="telephone2"><spring:message
                                code="label.telephone2" /></label>
                        <div class="col-md-6">

                            <input id="brandTelephone2" name="brandTelephone2"
                                class="form-control"
                                placeholder="<spring:message code='label.telephone2'/>.."
                                type="text" value="${brand.brandTelephone2}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="rest_email"><spring:message
                                code="label.brandemail" /><span class="text-danger">*</span></label>
                        <div class="col-md-6">

                            <input id="brand_email" name="brandEmail"
                                class="form-control"
                                placeholder="<spring:message code="label.brandemail"/>.."
                                type="text" value="${brand.brandEmail}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="mobile1"><spring:message
                                code="label.mobile1" /><span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <input id="brandMobile1" name="brandMobile1"
                                class="form-control"
                                placeholder="<spring:message code="label.mobile1"/>.."
                                type="text" value="${brand.brandMobile1}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="mobile2"><spring:message
                                code="label.mobile2" /></label>
                        <div class="col-md-6">

                            <input id="brandMobile2" name="brandMobile2"
                                class="form-control"
                                placeholder="<spring:message code="label.mobile2"/>.."
                                type="text" value="${brand.brandMobile2}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="addressline1"><spring:message
                                code="label.addressline1" /><span class="text-danger">*</span>
                        </label>
                        <div class="col-md-6">

                            <input id="brandAddressline1" name="brandAddressLine1"
                                class="form-control"
                                placeholder="<spring:message code="label.addressline1"/>.."
                                type="text" value="${brand.brandAddressLine1}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="addressline2"><spring:message
                                code="label.addressline2" /></label>
                        <div class="col-md-6">

                            <input id="brandAddressline2" name="brandAddressLine2"
                                class="form-control"
                                placeholder="<spring:message code="label.addressline2"/>.."
                                type="text" value="${brand.brandAddressLine2}">
                        </div>
                    </div>
                    <div class="form-group" id="countryDiv">
                        <label class="col-md-4 control-label" for="country"><spring:message
                                code="label.countryname" /><span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <select class="country_chosen" style="width: 200px;"
                                id="brandCountryId"
                                data-placeholder="<spring:message code='label.choosecountry' />"
                                name="chosenCountryId">
                                <option value=""></option>
                                <c:forEach items="${countryList}" var="country">
                                            <option value="${country.countryId}"  
                                            <c:if test="${country.countryId eq brand.brandCountry.countryId}">selected="selected"</c:if>>${country.countryName}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group" id="cityDiv">
                        <label class="col-md-4 control-label" for="city"><spring:message
                                code="label.cityname" /><span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <select class="city_chosen" style="width: 200px;"
                                id="brandCityId"
                                data-placeholder="<spring:message code='label.choosecity' />"
                                name="chosenCityId">
                                <c:forEach items="${cityList}" var="city">
                                    <option value="${city.cityId}"  <c:if test="${city.cityId eq brand.brandCity.cityId}">selected="selected"</c:if> >${city.cityName}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group" id="areaDiv">
                        <label class="col-md-4 control-label" for="area"><spring:message
                                code="label.areaname" /><span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <select class="area_chosen" style="width: 200px;"
                                id="brandAreaId"
                                data-placeholder="<spring:message code='label.choosearea' />"
                                name="chosenAreaId">
                                <c:forEach items="${areaList}" var="area">
                                                <option value="${area.areaId}"  <c:if test="${area.areaId eq brand.brandArea.areaId}">selected="selected"</c:if> >${area.areaName}</option>
                                    </c:forEach>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="brandPincode"><spring:message
                                code="label.pincode" /><span class="text-danger">*</span></label>
                        <div class="col-md-6">

                            <input id="brandPincode" name="brandPincode"
                                class="form-control"
                                placeholder="<spring:message code="label.pincode"/>.."
                                type="text" value="${brand.brandPincode }">
                        </div>
                    </div>
                </div>
            
            </div>
            
            <div class="col-md-6">
            
            
            
            <div class="block">
                    <div class="block-title">
                        <h2>
                            <strong><spring:message code="heading.admininfo"/></strong>
                            <small style="float:left; margin-top: 4px;">This info will be used for login. Please make sure you add the exact details and make sure it is not misused.</small>
                        </h2>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="first_Name"><spring:message code="label.firstname"/><span class="text-danger">*</span>
                        </label>
                        <div class="col-md-6">
                            
                                <input id="first_Name" name="firstName" class="form-control"
                                    placeholder="<spring:message code='label.firstname'/>.." type="text" value="${brand.brandAdmin.firstName }"> 
                        </div>
                    </div>
    
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="last_Name"><spring:message code="label.lastname"/><span class="text-danger">*</span>
                        </label>
                        <div class="col-md-6">
                            
                                <input id="last_Name" name="lastName" class="form-control"
                                    placeholder="<spring:message code="label.lastname"/>.." type="text" value="${brand.brandAdmin.lastName }">
                        </div>
                    </div>


                        <div class="form-group">
                            <label class="col-md-4 control-label" for="email"><spring:message
                                    code="label.emailid" /><span class="text-danger">*</span>
                            </label>
                            <div class="col-md-6">
                                <input id="email" name="emailId" class="form-control"
                                    onchange="emailVerification()" placeholder="test@example.com"
                                    type="text" value="${brand.brandAdmin.emailId }"> 
                                    <span class="text-success" id="emailavailable" style="display: none">
                                        <spring:message code="alert.emailavailable" /> </span> 
                                    <span class="text-danger" id="emailnotavailable" style="display: none">
                                        <spring:message code="alert.emailnotavailable" /> </span> 
                                    <span id="emailloading" style="display: none"><i
                                        class="fa fa-spinner fa-spin"></i> </span>
                            </div>
                        </div>

                        <div class="form-group">
                        <label class="col-md-4 control-label" for="mobile"><spring:message code="label.mobile"/><span
                            class="text-danger">*</span></label>
                        <div class="col-md-6">
                                <input id="mobile" name="mobile" class="form-control"
                                    placeholder='<spring:message code="label.mobile"></spring:message>' type="text" value="${brand.brandAdmin.mobile }">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="login"><spring:message code="label.loginid"/>
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon"><%=propvalue.brandLoginPrefix%></span>
                                <input id="login" name="loginId" class="form-control"
                                    placeholder='<spring:message code='label.loginid'/>' type="text"
                                    value="${brand.brandAdmin.loginId }">
                            </div>
                             <span
                                class="label label-success" id="availability"
                                style="cursor: pointer;"><i class="fa fa-check"></i>
                                <spring:message code="label.checkavailability"/></span><span class="text-success"
                                id="available" style="display: none"> <spring:message
                                    code="alert.loginavailable" />
                            </span> <span class="text-danger" id="notavailable"
                                style="display: none"> <spring:message
                                    code="alert.loginnotavailable" />
                            </span>
                            <span id="loading" style="display: none"><i class="fa fa-spinner fa-spin"></i>
                           </span>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="password"><spring:message code="label.password"/>
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-md-6">
                                <input id="password" name="password" class="form-control"
                                    placeholder="Choose a crazy one.." type="password"> 
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="confirm_password"><spring:message code="label.confirmpassword"/><span class="text-danger">*</span>
                        </label>
                        <div class="col-md-6">
                                <input id="confirm_password" name="confirmPassword"
                                    class="form-control" placeholder="..and confirm it!"
                                    type="password"> 
                        </div>
                    </div>
                </div>
            
            
                <div class="block" id="Feature_Control">
                    <%@include file="../featurecontrol/featurecontrol.jsp"%>
                </div>
                
            </div>
    
    </div>
</div>
<div style="display: none;" id="advanced-second"
            class="step ui-formwizard-content">
            <div class="wizard-steps">
                <div class="row">
                    <div class="col-xs-6 done">
                        <span><spring:message code="wizard.branddetail" /></span>
                    </div>
                    <div class="col-xs-6 active">
                        <span><spring:message code="wizard.brandtermsandconditions" /></span>
                    </div>
                </div>
            </div>
            <div class="block">
                <div class="block-title">
                    <h2>
                        <strong><spring:message code="label.termsandcondition" /></strong>
                    </h2>
                </div>
                <div class="terms_text">
                    <ol type="1">
                        <li>The Brand has the full power, legal
                            capacity, and authority to carry out the terms hereof.</li><br>
                            
                        <li>To ensure that members of Gourmet7 get
                            accurate information, the Brandat all times must provideauthentic
                            details offood and dishes offered, specialties,location of
                            allbranches, key features, affiliations and certifications ifany
                            along with photographs and offer details on the website.</li><br>
                            
                        <li>Creation, management and updating of the pages of various
                            outletsalong with the data on the website will be the
                            responsibility of the Brand.</li><br>
                            
                        <li>Gourmet7 at its sole discretion shall be
                            entitled to discontinue, halt or suspend displaying of all
                            information on its website, related to theBrand, in case it fails
                            to update the information over a period of time or honor any
                            points mentioned in the terms and conditions.</li><br>

                        <li>The Brandagrees and accepts to provide the
                            services of its outlets i.e.,food, drinks,and other allied
                            services,to the customers of Gourmet7,either complimentary / at
                            discounted rates.The complimentary offerings / discounts will
                            have to be decided by the Brand and can be differentforeach
                            outlet. Also, the offerings could be different depending on the
                            time of the day (Breakfast/Lunch/Dinner) and also the day of the
                            week.</li><br>

                        <li>To enhancemember loyalty, Gourmet7 suggests that
                            theBrand works on a principal whereby a member who visits a
                            particular outlet regularly is offered higher discount than
                            others. In event of the Brand deciding not to opt for such
                            discounting terms, it will still offer a flat pre-decided rate of
                            discount.</li><br>

                        <li>Theoutlets of the Brand are required to charge
                            and recover the charges for their services, directly from the
                            concerned member. Gourmet7 shall not be liable in case of any
                            default on the part of any member.</li><br>

                        <li>Gourmet7is entitled to issue / sellor distribute
                            membershipas a part of certain promotional offers, events, and
                            activitiesto its prospective customers.</li><br>

                        <li>The agreement /arrangement in respect to the
                            said membership offer shall be between Gourmet7and the concerned
                            member, and the Brandshall be no way concerned with the same.</li><br>

                        <li>Gourmet7 will carry out the marketing and
                            promotional activities as stated herein and theBrand hereby
                            grants conditional permission to Gourmet7 to use its logos, brand
                            name and trademarks for such purposes.</li><br>

                        <li>The Brandwill ensure that its outletsrender due
                            and proper and high quality services to the members of Gourmet7.</li><br>

                        <li>Gourmet7under any circumstances shall not be
                            responsible for the quality of the offerings and /or services
                            made available by the Brand’s outlets to the members of Gourmet7,
                            and the same shall be the sole responsibility of the Brand.</li><br>

                        <li>The Brand shall be solely liable to address and
                            remedy all the claims, disputes made by anyGourmet7member in
                            respect to the quality of the offerings and /or services rendered
                            by the Brand’s outlets.</li><br>

                        <li>The Brand shall ensure its outlets meetall
                            necessary statutory compliances including FDA compliance.</li><br>

                        <li>The Brand grantsGourmet7, a temporary,
                            non-exclusive, royalty free, worldwide license to display name
                            logos, trademarks and service marks on Gourmet7’swebsite,related
                            domain names and other publications of Gouret7 in its sole
                            discretion.</li><br>

                        <li>Neither party shall be responsible for delays or
                            in performance resulting from acts beyond control. Such acts
                            include but are not be limited to acts of gods, labour conflicts,
                            acts of war or civil disruption, governmental regulations imposed
                            after the fact, public utility out failures, industry wide
                            disruption, shortages of labour or material, or natural
                            disasters.</li><br>

                        <li>Toutilise the services of Gourmet7, the
                            Restaurant will have to access database and features of Gourmet7
                            through web, or downloadable applications on Android phones /
                            tablets. To ensure that all advanced features and services are
                            available and function smoothly, the Brandmust ensure that ithas
                            high speed internet connectivity at its locations and update the
                            app from time to time. Gourmet7 will not be held responsible for
                            delay in receiving any service notification due to such
                            connectivity issues.</li><br>

                        <li>Advanced features and services of Gourmet7 will
                            be exclusively available on mobiles and tablets running on
                            Android though basic features can be accessed through the web.To
                            make the best use of our services, we would recommend using a 7”
                            tablet of any brand.</li><br>

                        <li>In order to avail the services of the Brand’s
                            outlets, a Gourmet7 member will have to generate a GPIN each time
                            he visits. This GPIN will have to be verified at the
                            outletlevelthrough the Gourmet7 app. The Brand will have complete
                            rights to deny special benefits to a Gourmet7 member if he fails
                            to provide the GPIN.</li><br>

                        <li>Gourmet7 will keep introducing new free and paid
                            services from time to time and the availability of the same will
                            be communicated. The Brand will have the discretion to decide
                            whether to subscribe for them.</li><br>

                        <li>The Brand acknowledges that in the course of
                            performance, it will be given access to confidential information
                            that is proprietary and confidential to Gourmet7.</li><br>

                        <li>The Brand shall not use the Confidential
                            Information other than to perform its obligations hereunder and
                            will take all reasonable measures to safeguard the Confidential
                            Information and prevent its unauthorized disclosure to any third
                            party.</li><br>

                        <li>The Brand shall not be entitled to claim any
                            consideration or part of consideration from the monies received
                            by Gourmet7 by way of payment in respect to the said membership
                            program.</li><br>






                    </ol>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="bestdishes"><spring:message
                            code="label.name" /><span class="text-danger">*</span> </label>
                    <div class="col-md-6">
                        <input id="bestdishes" name="agreementBy" class="form-control"
                            placeholder="<spring:message code='label.enteryourname'/>"
                            type="text" value="<%=user.getFullName()%>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="offer_validtill"><spring:message
                            code="label.agreementdate" /><span class="text-danger">*</span>
                    </label>
                    <div class="col-md-6">
                        <input type="text" id="agreementDate_datepicker" name="agreementDate"
                            style="width: 200px;" class="form-control input-datepicker"
                            data-date-format="<spring:message code="label.dateformat" />"
                            placeholder="<spring:message code="label.dateformat" />">
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group form-actions">
            <div class="col-md-9 col-md-offset-3">
<!--                <input -->
<!--                    class="btn btn-sm btn-primary ui-wizard-content ui-formwizard-button" -->
<!--                    id="next2" value="Next" type="submit"> -->
<!--                <button type="reset" class="btn btn-sm btn-warning"> -->
<!--                    <i class="fa"></i> -->
<%--                    <spring:message code="button.reset" /> --%>
<!--                </button> -->
<!--                <a class="btn btn-sm btn-primary" href="BrandList"><i -->
<%--                    class="gi"></i> <spring:message code="button.cancel" /> --%>
<!--                </a> -->
                
                <input disabled="disabled"  class="btn btn-sm btn-warning ui-wizard-content ui-formwizard-button" id="back2" value="Back" type="reset" style="width: 56px;">
                 <input  class="btn btn-sm btn-primary ui-wizard-content ui-formwizard-button save" id="next2" value="Next" type="submit" style="width: 56px;"> 
                 <a class="btn btn-sm btn-primary" href="<%=contexturl %>ManageBrand/BrandList"><i class="gi gi-remove"></i> <spring:message code="button.cancel" />
                                </a>
                
                
                
                
            </div>
        </div>



    </form>
</div>

<div id="includeTermsCond" class="modal fade" tabindex="-1"
    role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div style="padding: 10px; height: 110px;">
                    <a href="#" data-dismiss="modal" style="margin-top: -40px; position: fixed; margin-left: 557px;"
                        class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                    <label>Terms and condition for include in membership.</label>
                </div>
            </div>
        </div>
    </div>
</div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<%@include file="../../inc/chosen_scripts.jsp"%>
<script src="<%=contexturl %>resources/js/custom/featurecontrol.js"></script>


<script type="text/javascript">
    $(document).ready(function() {
        $('#agreementDate_datepicker').datepicker('setStartDate', new Date());
        $('#agreementDate_datepicker').datepicker('setEndDate', new Date());
        $('#agreementDate_datepicker').datepicker('setDate', new Date());
        $(".chosen").data("placeholder","Select Currency From...").chosen();
        $(".area_chosen").chosen();
        $(".city_chosen").chosen();
        $(".country_chosen").chosen();

        $('select[name="chosenCountryId"]').chosen().change( function() {
            var countryId = ($(this).val());
            $.ajax({
                type: "GET",
                async: false,
                url: "<%=contexturl %>city/citybycountryid/"+countryId,
                success: function(data, textStatus, jqXHR){
                    var obj = jQuery.parseJSON(data);
                    var len=obj.length;
                    var myOptions = "<option value></option>";
                    var myAreaOptions = "<option value></option>";
                    for(var i=0; i<len; i++){
                    myOptions += '<option value="'+obj[i].cityId+'">'+obj[i].cityName+'</option>';
                    }
                    $(".city_chosen").html(myOptions).chosen().trigger("chosen:updated");
                    $(".area_chosen").html(myAreaOptions).chosen().trigger("chosen:updated");
                },
                dataType: 'html'
            });
        });
        
        $('select[name="chosenCityId"]').chosen().change( function() {
            var cityId = ($(this).val());
            $.ajax({
                type: "GET",
                async: false,
                url: "<%=contexturl %>area/areabycityid/"+cityId,
                success: function(data, textStatus, jqXHR){
                    var obj = jQuery.parseJSON(data);
                    var len=obj.length;
                    var myOptions = "<option value></option>";
                    for(var i=0; i<len; i++){
                    myOptions += '<option value="'+obj[i].areaId+'">'+obj[i].areaName+'</option>';
                    }
                    $(".area_chosen").html(myOptions).chosen().trigger("chosen:updated");
                },
                dataType: 'html'
            });
        });
        
        $("#availability").click(function(){
            var data = $("#login").val();
            data ='<%=propvalue.brandLoginPrefix%>'+data;
            $("#notavailable").css("display","none");
            $("#loading").css("display", "inline");
            $("#available").css("display","none");
            if( data != ""){
                $.getJSON("<%=contexturl%>ValidateLogin",{loginId: data}, function(data){
                    if(data == "exist"){
                        $("#notavailable").css("display","inline");
                        $("#loading").css("display", "none");
                    }
                    else if(data == "available"){
                        $("#available").css("display","inline");
                        $("#loading").css("display", "none");
                    }
                });
            }
        });
        
//      $("#cancel").click(function(){
//          $.ajax({
//              type: "GET",
//              async: false,
//              url:"",             
//              success: function(data, textStatus, jqXHR){
//                  $('#page-content').html(data);
//              },
//              dataType: 'html'
//          });
//      });
        
        
        $("#Brand_form").formwizard(
                {   
                    disableUIStyles : !0,
                    validationEnabled : !0,
                    validationOptions : {
                    onfocusout: false,
                    errorClass:"help-block animation-slideDown",
                    errorElement:"div",
                    
                    errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
                    highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error");
                    $(e).focus();},
                    success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
                    rules:{ brandName:{required:!0,maxlength : 75},
                        chosenCompanyId:{required:!0},
                            firstName:{required:!0, maxlength : 75},
                            lastName:{required:!0,maxlength : 75},
                            emailId:{required:!0,maxlength :75,email:!0},
                            mobile:{required:!0,maxlength : 75,phone:!0},
                            loginId:{required:!0,minlength:6,maxlength : 75},
                            password:{required:!0,minlength:6,maxlength : 75},
                            confirmPassword:{required:!0,equalTo:"#password"},
                            brandTelephone1 : {
                                required : !0,maxlength : 75,
                                phone : !0
                            },
                            brandTelephone2 : {
                                phone : !0, maxlength : 75,
                            },
                            brandEmail : {
                                required : !0,maxlength : 75,
                                email : !0
                            },
                            brandMobile1 : {
                                required : !0,maxlength : 75,
                                phone : !0
                            },
                            brandMobile2 : {
                                phone : !0, maxlength : 75
                            },
                            brandAddressLine1 : {
                                required : !0,maxlength : 75
                            },
                            brandAddressLine2 : {
                                maxlength : 75
                            },
                            brandPincode : {
                                required : !0,maxlength : 10,
                                digits : !0
                            },
                            chosenCityId: {
                                required : !0
                            },
                            chosenCountryId: {
                                required : !0
                            },
                            chosenAreaId: {
                                required : !0
                            },
                            agreementDate: {
                                required : !0
                            },
                            agreementBy: {
                                required : !0
                            }
                    },
                    messages:{
                        brandName:{
                            required:'<spring:message code="validation.pleaseenterbrandname"/>',
                        maxlength :'<spring:message code="validation.brnadname75character"/>'
                        },
                        chosenCompanyId:{
                            required:'<spring:message code="validation.pleaseselectcompany"/>'
                            },
                        firstName:{
                            required:'<spring:message code="validation.pleaseenterfirstname"/>',
                            maxlength :'<spring:message code="validation.firstname75character"/>'
                            },
                        lastName:{
                            required:'<spring:message code="validation.pleaseenterlastname"/>',
                            maxlength :'<spring:message code="validation.lastname75character"/>'
                            },
                        emailId:{
                            required:'<spring:message code="validation.pleaseenteremailid"/>',
                            email:'<spring:message code="validation.pleaseenteravalidemailid"/>',
                                maxlength : '<spring:message code="validation.email75character"/>'
                            },
                        mobile:{
                            required:'<spring:message code="validation.pleaseenteramobilenumber"/>',
                            phone :'<spring:message code="validation.phone"/>',
                            maxlength :'<spring:message code="validation.mobile75character"/>'
                            },
                        loginId:{
                            required:'<spring:message code="validation.pleaseenteraloginid"/>',
                            minlength:'<spring:message code="validation.loginidmustbeatleast6character"/>',
                            maxlength :'<spring:message code="validation.loginid75character"/>'
                            },
                        password:{
                            required:'<spring:message code="validation.pleaseenterapassword"/>',
                            minlength:'<spring:message code="validation.passwordmustbeatleast6character"/>',
                            maxlength :'<spring:message code="validation.passworddname75character"/>'
                            },
                        confirmPassword:{
                            required:'<spring:message code="validation.pleaseconfirmpassword"/>',
                            equalTo:'<spring:message code="validation.pleaseenterthesamepasswordasabove"/>'
                            },
                        brandTelephone1 : {
                            required : '<spring:message code="validation.pleaseenteratelephonenumber"/>',
                            phone :'<spring:message code="validation.phone"/>',
                            maxlength :'<spring:message code="validation.telephone75character"/>'
                        },
                        brandTelephone2 : {

                            phone :'<spring:message code="validation.phone"/>',
                            maxlength :'<spring:message code="validation.telephone75character"/>'
                        },
                        brandEmail : {
                            required :  '<spring:message code="validation.pleaseenteremailid"/>',
                            email : '<spring:message code="validation.pleaseenteravalidemailid"/>',
                            maxlength : '<spring:message code="validation.email75character"/>'
                        },
                        brandMobile1 : {
                            required :'<spring:message code="validation.pleaseenteramobilenumber"/>',
                            phone :'<spring:message code="validation.phone"/>',
                            maxlength :'<spring:message code="validation.mobile75character"/>'
                        },
                        brandMobile2 : {
                            phone :'<spring:message code="validation.phone"/>',
                            maxlength :'<spring:message code="validation.mobile75character"/>'
                        },
                        brandAddressLine1 : {
                            required :'<spring:message code="validation.pleaseenteraddressline1"/>',
                            maxlength :'<spring:message code="validation.address"/>'
                        },
                        brandAddressLine2 : {
                            maxlength :'<spring:message code="validation.address"/>'
                        },
                        brandPincode : {
                            required :  '<spring:message code="validation.pleaseenterpincode"/>',
                            digits :'<spring:message code="validation.digits"/>',
                            maxlength :'<spring:message code="validation.pincode75character"/>'
                        },
                        chosenCityId: {
                            required : '<spring:message code="validation.selectcity"/>'
                        },
                        chosenCountryId: {
                            required : '<spring:message code="validation.selectcountry"/>'
                        },
                        chosenAreaId: {
                            required : '<spring:message code="validation.selectarea"/>'
                        },
                        agreementDate: {
                            required :'<spring:message code="validation.pleaseenteragreementdate"/>'
                        },
                        
                        agreementBy: {
                            required :'<spring:message code="validation.pleaseenteryourname"/>'
                        }
                    },
                    }
                });
                        
                $("#next2").click(function(){
                     unsaved=false;
                });
                
                $("#company_Name").change(function(){
                    var companyId = $("#company_Name").val();
                    $.ajax({
                        type: "GET",
                        async: false,
                        url: "<%=contexturl %>/GetCompanyFeatures/"+companyId,              
                        success: function(data, textStatus, jqXHR){
                            $('#Feature_Control').html(data);
                            $('.feature-control').change(function() {
                                if(this.checked){
                                    $("#message_link" ).trigger( "click" );
                                }
                                else{
                                    $("#current_feature").val(this.id);
                                    $("#warning_message_link" ).trigger( "click" );
                                }
                            });
        
                            $("#no_deselect").click(function(){
                                var currentFetaure = $("#current_feature").val();
                                $("#"+currentFetaure).prop( "checked", true );
                            });
                        },
                        dataType: 'html'
                    });
                });
    });
    
    function emailVerification(){
        var data = $("#email").val();
        $("#emailnotavailable").css("display","none");
        $("#emailloading").css("display", "inline");
        $("#emailavailable").css("display","none");
        if( data != ""){
            $.getJSON("<%=contexturl%>ValidateEmailId",{emailId: data}, function(data){
                if(data == "exist"){
                    $("#emailnotavailable").css("display","inline");
                    $("#emailloading").css("display", "none");
                }
                else if(data == "available"){
                    $("#emailavailable").css("display","inline");
                    $("#emailloading").css("display", "none");
                }
            });
        }
    }
</script>
<style>

.chosen-container {
    width: 250px !important;
}
</style>
<%@include file="../../inc/template_end.jsp"%>