<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->

<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>

<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>

			<h1>
				<i class="fa fa-map-marker"></i>
				<spring:message code="heading.user" />
				<br> <small><spring:message
						code="heading.userdetails" /></small>
			</h1>
			<span id="errorMsg"></span>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="heading.user" /></li>
	</ul>
	<form action="<%=contexturl %>ManageUsers/UserList/SaveUser" method="post" class="form-horizontal ui-formwizard"
		id="User_form" >
		
		<div class="col-md-12">
				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.userinfo" /></strong>
						</h2>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="first_Name"><spring:message
								code="label.firstname" /><span class="text-danger">*</span> </label>
						<div class="col-md-6">

							<input id="first_Name" name="firstName" class="form-control"
								placeholder="<spring:message code='label.firstname'/>.."
								type="text" value="${user.firstName}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="last_Name"><spring:message
								code="label.lastname" /><span class="text-danger">*</span> </label>
						<div class="col-md-6">

							<input id="last_Name" name="lastName" class="form-control"
								placeholder="<spring:message code="label.lastname"/>.."
								type="text" value="${user.lastName}">
						</div>
					</div>


					<div class="form-group">
						<label class="col-md-4 control-label" for="email"><spring:message
								code="label.emailid" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<input id="email" name="emailId" class="form-control"
								placeholder="test@example.com" type="text" value="${user.emailId}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="mobile"><spring:message
								code="label.mobile" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<input id="mobile" name="mobile" class="form-control"
								placeholder='<spring:message code="label.mobile"></spring:message>'
								type="text" value="${user.mobile}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="login"><spring:message
								code="label.loginid" /> <span class="text-danger">*</span> </label>
						<div class="col-md-6">
							<input id="login" name="loginId" class="form-control"
								placeholder='<spring:message code='label.loginid'/>' type="text" value="${user.loginId}">
							<span class="label label-success" id="availability"
								style="cursor: pointer;"><i class="fa fa-check"></i>
								<spring:message code="label.checkavailability"/></span><span class="text-success" id="available"
								style="display: none"> <spring:message
									code="alert.loginavailable" />
							</span> <span class="text-danger" id="notavailable"
								style="display: none"> <spring:message
									code="alert.loginnotavailable" />
							</span>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="password"><spring:message
								code="label.password" /> <span class="text-danger">*</span> </label>
						<div class="col-md-6">
							<input id="password" name="password" class="form-control"
								placeholder="Choose a crazy one.." type="password" value="${user.password}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="confirm_password"><spring:message
								code="label.confirmpassword" /><span class="text-danger">*</span>
						</label>
						<div class="col-md-6">
							<input id="confirm_password" name="confirmPassword"
								class="form-control" placeholder="..and confirm it!"
								type="password" value="${user.password}">
						</div>
					</div>
				</div>
			</div>
			<div class="form-group form-actions">
				<div class="col-md-9 col-md-offset-3">
					<button id="user_submit" type="submit"
						class="btn btn-sm btn-primary save">
						<i class="fa fa-angle-right"></i> <spring:message code="button.save" />
					</button>
<!-- 					<button type="reset" class="btn btn-sm btn-warning"> -->
<!-- 						<i class="fa fa-repeat"></i> -->
<%-- 						<spring:message code="button.reset" /> --%>
<!-- 					</button> -->
					<a class="btn btn-sm btn-primary" href="<%=contexturl %>ManageUsers/UserList/"><i class="gi gi-remove"></i>
							<spring:message code="button.cancel" /> </a>
				</div>
			</div>
	</form>
</div>
<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>

<script type="text/javascript">
$(document).ready(function() {
	
	$("#availability").click(
			function() {
				var data = $("#login").val();
				$("#notavailable").css("display", "none")
				$("#available").css("display", "none")
				if (data != "") {
					$.getJSON("<%=contexturl%>ValidateLogin", {
						loginId : data
					}, function(data) {
						if (data == "exist") {
							$("#notavailable").css(
									"display", "inline")
						} else if (data == "available") {
							$("#available").css("display",
									"inline")
						}
					});
				}
			});
	
	$("#User_form").validate(
			{	errorClass:"help-block animation-slideDown",
				errorElement:"div",
				errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
				highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
				success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
				rules:{ firstName : {required : !0,maxlength : 75},
					lastName : {required : !0,maxlength : 75},
					emailId : {required : !0,maxlength : 75,email : !0},
					mobile : {required : !0,maxlength : 15,phone : !0,},
					loginId : {required : !0,maxlength : 75,minlength : 6},
					password : {required : !0,maxlength : 75,minlength : 6},
					confirmPassword : {required : !0,equalTo : "#password"}
				},
				messages:{
					firstName : {
						required :'<spring:message code="validation.pleaseenterfirstname"/>',
						maxlength :'<spring:message code="validation.firstname75character"/>'
					},
					lastName : {
						required :'<spring:message code="validation.pleaseenterlastname"/>',
						maxlength :'<spring:message code="validation.lastname75character"/>'
					},
					emailId : {
						required :'<spring:message code="validation.pleaseenteremailid"/>',
						email :'<spring:message code="validation.pleaseenteravalidemailid"/>',
						maxlength :'<spring:message code="validation.email75character"/>'
					},
					mobile : {
						required :'<spring:message code="validation.pleaseenteramobilenumber"/>',
						phone :'<spring:message code="validation.phone"/>'
					},
					loginId : {
						required :'<spring:message code="validation.pleaseenteraloginid"/>',
						minlength :'<spring:message code="validation.loginidmustbeatleast6character"/>',
						maxlength :'<spring:message code="validation.loginid75character"/>'
					},
					password : {
						required :'<spring:message code="validation.pleaseenterapassword"/>',
						minlength :'<spring:message code="validation.passwordmustbeatleast6character"/>',
						maxlength :'<spring:message code="validation.passworddname75character"/>'
					},
					confirmPassword : {
						required :'<spring:message code="validation.pleaseconfirmpassword"/>',
						equalTo :'<spring:message code="validation.pleaseenterthesamepasswordasabove"/>'
					}
				},
			});
	
	 $("#user_submit").click(function(){
		 $("#User_form").submit();
	 });
	
});
</script>

<%@include file="../../inc/template_end.jsp"%>