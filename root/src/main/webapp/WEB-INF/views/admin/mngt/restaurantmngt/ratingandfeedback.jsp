<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->

<%@page import="java.math.BigDecimal"%>
<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>

<style>
#page-content {
	padding: 0px 15px 0px;
	background-color: #eaedf1;
}

.rating_value {
	width: 100% !important;
}

.ratinglable {
	float: left;
}

.feedback_profile {
	height: 60px;
	width: 60px !important;
}

.comment_profile {
	height: 25px;
	width: 33px !important;
}

.block-title {
	font-weight: bold;
	margin-left: -13px !important;
	margin-right: -20px !important;
	margin-bottom: 5px !important;
	margin-left: -13px !important;
	margin-right: -20px !important;
	margin-bottom: 5px !important;
	margin: 0px -13px !important;
}

.row {
	margin-bottom: 5px;
}
</style>

<div id="page-content">

	<div class="row">
		<div class="col-md-6" style="margin-top: 10px;">
			<div class="widget">

				<div class="widget-extra themed-background-dark ">
					<h3 class="widget-content-light  animation-fadeIn">
						<strong>Ratings</strong>
					</h3>
				</div>

				<div class="widget-extra ">
					<div style="line-height: 50px;">
						<div id="b_details_usr_avg_rating_contatiner" class="row"
							style="margin-top: 20px;">
							<h4 class="col-md-4">Average Rating</h4>
							<div id="restaurant_rating"
								class="col-md-8   animation-fadeIn btn btn-default"
								style="cursor: pointer;"></div>
						</div>
						<div id="b_details_usr_food_rating_contatiner" class="row"
							style="margin-top: 20px;">
							<h4 class="col-md-4">Food</h4>
							<div id="restaurant_rating1"
								class="col-md-8   animation-fadeIn btn btn-default"
								style="cursor: pointer;"></div>
						</div>
						<div id="b_details_usr_ambiance_rating_contatiner" class="row">
							<h4 class="col-md-4">Ambiance</h4>
							<div id="restaurant_rating2"
								class="col-md-2   animation-fadeIn btn btn-default"
								style="cursor: pointer;"></div>
						</div>
						<div id="b_details_usr_service_rating_contatiner" class="row">
							<h4 class="col-md-4">Service</h4>
							<div id="restaurant_rating3"
								class="col-md-2   animation-fadeIn btn btn-default"
								style="cursor: pointer;"></div>
						</div>
						<div id="b_details_usr_valueForMoney_rating_contatiner"
							class="row">
							<h4 class="col-md-4">Value For Money</h4>
							<div id="restaurant_rating4"
								class="col-md-2   animation-fadeIn btn btn-default"
								style="cursor: pointer;"></div>
						</div>
						<div id="b_details_usr_hygiene_rating_contatiner" class="row">
							<h4 class="col-md-4">Hygiene</h4>
							<div id="restaurant_rating5"
								class="col-md-2  animation-fadeIn  btn btn-default"
								style="cursor: pointer;"></div>
						</div>


					</div>

					<br /> <br />

				</div>
			</div>
		</div>

		<div class="col-md-6" style="margin-top: 10px;">
			<div class="widget">
				<div class="widget-extra themed-background-dark">
					<h3 class="widget-content-light  animation-fadeIn">
						My <strong>FeedBack</strong>
					</h3>
				</div>
				<div class="widget-extra ">

					<ul class="media-list media-feed media-feed-hover">
					<c:forEach var="currentOutletFeedback"  items="${outletFeedbackList}" varStatus="count">
						<li class="media"><span class="pull-left">
						
						
						 
						 <c:choose>
								<c:when
									test="${! empty currentOutletFeedback.user.profileImage and !empty currentOutletFeedback.user.profileImage.imagePath}">
									    <img src="<%=contexturl%>${currentOutletFeedback.user.profileImage.imagePath}" alt="Avatar" class="feedback_profile img-circle">
									
										</c:when>
								<c:otherwise>
									<img
										src="<%=contexturl%>resources/img/application/default_profile.png"
										alt="Avatar" class="feedback_profile img-circle">
									
								</c:otherwise>
							</c:choose>
						 </span>
							<div class="media-body">
								<p class="push-bit">
									<span class="text-muted pull-right"> 
									<small><fmt:formatDate	pattern="<%=propvalue.simpleDateFormat %>" value="${currentOutletFeedback.createdDate}" /></small> 
									
									</span> <strong><span class="text-info">${currentOutletFeedback.user.firstName}</span> </strong>
								</p>
								<p>${currentOutletFeedback.feedback}</p>

								<!-- Comments -->
								<ul class="media-list push">
									<c:forEach var="currentComment" items="${currentOutletFeedback.comments}" varStatus="count12">
								
									<li class="media"><span  class="pull-left">
									<c:choose>
									<c:when test="${! empty currentComment.user.profileImage and !empty currentComment.user.profileImage.imagePath}">
										<img src="<%=contexturl%>${currentComment.user.profileImage.imagePath}" alt="avatar" class="feedback_profile img-circle">
									</c:when>
									<c:otherwise>
										<img src="<%=contexturl%>resources/img/application/default_profile.png" alt="avatar" class="feedback_profile img-circle">
										
									</c:otherwise>
								</c:choose>
									</span>
										<div class="media-body">
											<span class="text-info"><strong>${currentComment.user.firstName}</strong></span>
											<span class="text-muted"><small><em><fmt:formatDate
															pattern="<%=propvalue.simpleDateFormat %>"
															value="${currentOutletFeedback.createdDate}" /></em></small></span>
											<p>${currentComment.comment}</p>
										</div>
										
										</li>
									
								
								</c:forEach>
								<div class="media-body">
											<form action="SaveComment" method="post" id="Comment_form_${currentOutletFeedback.feedbackId}">
											<input type="hidden" value="${restaurant.outletId}" name="outletId">
											 <input type="hidden" value="${currentOutletFeedback.feedbackId}" name="feedbackId">
												<textarea id="comment_${currentOutletFeedback.feedbackId}"
													name="comment" class="form-control"
													rows="2" placeholder="Your comment.."></textarea>
													<label id="err_${currentOutletFeedback.feedbackId}" style="color: #e74c3c;"></label>
												<button type="submit" id="comment_submit_${currentOutletFeedback.feedbackId}" class="btn btn-xs btn-primary save">
													<i class="fa fa-pencil"></i> Post Comment
												</button>
											</form>

										
								</div>
								</ul>
								<!-- END Comments -->
							</div></li>
						</c:forEach>
					</ul>
					
					
				
				</div>

			</div>
		</div>
		<!-- 			</div> -->
		<!-- 		<div> -->
		<%-- 			<form action="<%=contexturl%>Restaurant/SaveFeedback" method="post" --%>
		<!-- 				class="form-horizontal " id="Feedback_form"> -->
		<!-- 				<div class="form-group"> -->
		<%-- 					<input type="hidden" value="${restaurant.outletId}" name="outletId"> --%>
		<!-- 										<label class="col-md-4 control-label" for="Feedback">FeedBack</label> -->
		<!-- 					<div class="col-md-9" style="width: 66%;"> -->
		<!-- 						<textarea id="feedback" name="feedback" class="form-control" -->
		<!-- 							rows="3" placeholder="Write your feed back"></textarea> -->
		<!-- 					</div> -->
		<!-- 				</div> -->
		<!-- 				<div class="form-group form-actions"> -->
		<!-- 					<div class="col-md-9 col-md-offset-3"> -->
		<!-- 						<button id="feedback_submit" type="submit" -->
		<!-- 							class="btn btn-sm btn-primary"> -->
		<!-- 							<i class="fa fa-angle-right"></i> -->
		<%-- 							<spring:message code="button.submit" /> --%>
		<!-- 						</button> -->
		<!-- 					</div> -->
		<!-- 				</div> -->

		<!-- 			</form> -->
		<!-- 		</div> -->

	</div>
</div>

<%
	BigDecimal avgRating = (BigDecimal) request
			.getAttribute("avgRating");
	if (avgRating == null) {
		avgRating = new BigDecimal("0");
	}
	BigDecimal avgRating1 = (BigDecimal) request
			.getAttribute("avgRating1");
	if (avgRating1 == null) {
		avgRating1 = new BigDecimal("0");
	}
	BigDecimal avgRating2 = (BigDecimal) request
			.getAttribute("avgRating2");
	if (avgRating2 == null) {
		avgRating2 = new BigDecimal("0");
	}
	BigDecimal avgRating3 = (BigDecimal) request
			.getAttribute("avgRating3");
	if (avgRating3 == null) {
		avgRating3 = new BigDecimal("0");
	}
	BigDecimal avgRating4 = (BigDecimal) request
			.getAttribute("avgRating4");
	if (avgRating4 == null) {
		avgRating4 = new BigDecimal("0");
	}
	BigDecimal avgRating5 = (BigDecimal) request
			.getAttribute("avgRating5");
	if (avgRating5 == null) {
		avgRating5 = new BigDecimal("0");
	}
%>
<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<script type="text/javascript">
$(document).ready(function() {
	var rating1=<%=avgRating1%>;
	var rating2=<%=avgRating2%>;
	var rating3=<%=avgRating3%>;
	var rating4=<%=avgRating4%>;
	var rating5=<%=avgRating5%>;
	var avgrating=<%=avgRating%>;
	
	
		$('#restaurant_rating').raty({
			readOnly	: true,
			hints		: ['', '', '', '', ''],
			half		: true,
			path		: '<%=contexturl%>resources/img/raty/',
			scoreName	: "avgRating",	
			width		: 125,
			score		: <%=String.valueOf(avgRating)%>
		});
	
		$('#restaurant_rating1').raty({
			readOnly	: true,
			hints		: ['', '', '', '', ''],
			half		: true,
			path		: '<%=contexturl%>resources/img/raty/',
			scoreName	: "rating1",	
			width		: 125,
			score		:<%=String.valueOf(avgRating1)%>,
		});
		$('#restaurant_rating2').raty({
			readOnly	: true,
			hints		: ['', '', '', '', ''],
			half		: true,
			path		: '<%=contexturl%>resources/img/raty/',
			scoreName	: "rating2",	
			width		: 125,
		    score		: <%=String.valueOf(avgRating2)%>,
		});
	   
		$('#restaurant_rating3').raty({
			readOnly	: true,
			hints		: ['', '', '', '', ''],
			half		: true,
			path		: '<%=contexturl%>resources/img/raty/',
			scoreName	: "rating3",	
			width		: 125,
			score		: <%=String.valueOf(avgRating3)%>,
		});
		$('#restaurant_rating4').raty({
			readOnly	: true,
			hints		: ['', '', '', '', ''],
			half		: true,
			path		: '<%=contexturl%>resources/img/raty/',
			scoreName	: "rating4",	
			width		: 125,
			score		:<%=String.valueOf(avgRating4)%>,
			
		});
		$('#restaurant_rating5').raty({
			readOnly	: true,
			hints		: ['', '', '', '', ''],
			half		: true,
			path		: '<%=contexturl%>resources/img/raty/',
			scoreName : "rating5",
			width : 125,
			score :<%=String.valueOf(avgRating5)%>,
		});

		// 		$("#comment_submit").click(function() {
		// 			$("#Comment_form").submit();
		// 		});
		
		<c:forEach var="currentOutletFeedback"  items="${outletFeedbackList}" varStatus="count">
		$("#comment_submit_${currentOutletFeedback.feedbackId}").click(function(event){
			var comment = $('#comment_${currentOutletFeedback.feedbackId}').val();
			
			
			if(comment.length<3 ){
				$("#err_${currentOutletFeedback.feedbackId}").text("<spring:message code="validation.commentrequired"/>")
				return false;
			}
			if(comment.length > 255 ){
				$("#err_${currentOutletFeedback.feedbackId}").text("<spring:message code="validation.commentlength"/>")
				return false;
			}
			 $("#Comment_form_${currentOutletFeedback.feedbackId}").submit();
		});
		</c:forEach>

	});
</script>

<%@include file="../../inc/template_end.jsp"%>