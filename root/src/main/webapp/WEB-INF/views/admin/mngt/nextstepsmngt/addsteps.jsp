<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>

<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i>
						<spring:message code="label.error" />
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>

			<h1>
				<!-- 				<i class="fa fa-map-marker"></i> -->
				<spring:message code="heading.nextsteps" />
				<br> <small><spring:message
						code="heading.managenextsteps" /></small>
			</h1>
			<span id="errorMsg"></span>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<%-- 		<li><spring:message code="menu.survey" /></li> --%>
		<li><strong>${module.moduleName}</strong></li>
	</ul>

	<form
		action="<%=contexturl%>ManageNextSteps/CreateNextSteps/SaveNextSteps"
		method="post" class="form-horizontal ui-formwizard"
		id="NextSteps_Form" enctype="multipart/form-data">
		<input name="moduleId" id="moduleId" type="hidden"
			value="${module.moduleId}" />
		<div class="row">

			<div class="">
				<div class="col-md-12 block">
					<div class="col-md-12 block">
						<div class="block-title">
							<h2>
								<strong><spring:message code="heading.lifeStage1" /></strong> <input
									id="act" type="hidden" name="act">

							</h2>
						</div>

						<div class="col-md-6 form-group">
							<div>
								<input id="step1" name="1" class="form-control"
									placeholder="Enter the first step" type="text">
							</div>
						</div>
						<div class=" form-group">
							<label class="col-md-2 control-label" for="chosenCategory1"><spring:message
									code="label.categoryname" /></label>
							<div class="col-md-4">
								<select class="category_chosen1" style="width: 200px;"
									id="chosenCategory1"
									data-placeholder="<spring:message code='label.choosecategory' />"
									name="chosenCategory1">
									<option value=""></option>
									<c:forEach items="${categoryList}" var="category1">
										<option value="${category1.categoryId}"
											<c:if test="${category1.categoryId eq documentMaster.category.categoryId}">selected="selected"</c:if>>${category1.categoryName}</option>
									</c:forEach>
								</select>
							</div>
						</div>


						<div class="col-md-6 form-group">
							<div>
								<input id="step2" name="2" class="form-control"
									placeholder="Enter the second step" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="chosenCategory2"><spring:message
									code="label.categoryname" /></label>
							<div class="col-md-4">
								<select class="category_chosen2" style="width: 200px;"
									id="chosenCategory2"
									data-placeholder="<spring:message code='label.choosecategory' />"
									name="chosenCategory2">
									<option value=""></option>
									<c:forEach items="${categoryList}" var="category2">
										<option value="${category2.categoryId}"
											<c:if test="${category2.categoryId eq documentMaster.category.categoryId}">selected="selected"</c:if>>${category2.categoryName}</option>
									</c:forEach>
								</select>
							</div>
						</div>


						<div class="col-md-6 form-group">
							<div>
								<input id="step3" name="3" class="form-control"
									placeholder="Enter the third step" type="text">
							</div>
						</div>

						<div class="form-group" id="categoryDiv3">
							<label class="col-md-2 control-label" for="chosenCategory3"><spring:message
									code="label.categoryname" /></label>
							<div class="col-md-4">
								<select class="category_chosen3" style="width: 200px;"
									id="chosenCategory3"
									data-placeholder="<spring:message code='label.choosecategory' />"
									name="chosenCategory3">
									<option value=""></option>
									<c:forEach items="${categoryList}" var="category3">
										<option value="${category3.categoryId}"
											<c:if test="${category3.categoryId eq documentMaster.category.categoryId}">selected="selected"</c:if>>${category3.categoryName}</option>
									</c:forEach>
								</select>
							</div>
						</div>


						<div class="col-md-6 form-group">
							<div>
								<input id="step4" name="4" class="form-control"
									placeholder="Enter the fourth step" type="text">
							</div>
						</div>

						<div class="form-group" id="categoryDiv4">
							<label class="col-md-2 control-label" for="chosenCategory4"><spring:message
									code="label.categoryname" /></label>
							<div class="col-md-4">
								<select class="category_chosen4" style="width: 200px;"
									id="chosenCategory4"
									data-placeholder="<spring:message code='label.choosecategory' />"
									name="chosenCategory4">
									<option value=""></option>
									<c:forEach items="${categoryList}" var="category4">
										<option value="${category4.categoryId}"
											<c:if test="${category4.categoryId eq documentMaster.category.categoryId}">selected="selected"</c:if>>${category4.categoryName}</option>
									</c:forEach>
								</select>
							</div>
						</div>


					</div>
					<div class="col-md-12 block">
						<div class="block-title">
							<h2>
								<strong><spring:message code="heading.lifeStage2" /></strong>
							</h2>
						</div>

						<div class="col-md-6 form-group">
							<div>
								<input id="step5" name="5" class="form-control"
									placeholder="Enter the fifth step" type="text">
							</div>
						</div>

						<div class="form-group" id="categoryDiv5">
							<label class="col-md-2 control-label" for="chosenCategory5"><spring:message
									code="label.categoryname" /></label>
							<div class="col-md-4">
								<select class="category_chosen5" style="width: 200px;"
									id="chosenCategory5"
									data-placeholder="<spring:message code='label.choosecategory' />"
									name="chosenCategory5">
									<option value=""></option>
									<c:forEach items="${categoryList}" var="category5">
										<option value="${category5.categoryId}"
											<c:if test="${category5.categoryId eq documentMaster.category.categoryId}">selected="selected"</c:if>>${category5.categoryName}</option>
									</c:forEach>
								</select>
							</div>
						</div>



						<div class="col-md-6 form-group">
							<div>
								<input id="step6" name="6" class="form-control"
									placeholder="Enter the sixth step" type="text">
							</div>
						</div>

						<div class="form-group" id="categoryDiv6">
							<label class="col-md-2 control-label" for="chosenCategory6"><spring:message
									code="label.categoryname" /></label>
							<div class="col-md-4">
								<select class="category_chosen6" style="width: 200px;"
									id="chosenCategory6"
									data-placeholder="<spring:message code='label.choosecategory' />"
									name="chosenCategory6">
									<option value=""></option>
									<c:forEach items="${categoryList}" var="category6">
										<option value="${category6.categoryId}"
											<c:if test="${category6.categoryId eq documentMaster.category.categoryId}">selected="selected"</c:if>>${category6.categoryName}</option>
									</c:forEach>
								</select>
							</div>
						</div>


						<div class="col-md-6 form-group">
							<div>
								<input id="step7" name="7" class="form-control"
									placeholder="Enter the seventh step" type="text">
							</div>
						</div>

						<div class="form-group" id="categoryDiv7">
							<label class="col-md-2 control-label" for="chosenCategory7"><spring:message
									code="label.categoryname" /></label>
							<div class="col-md-4">
								<select class="category_chosen7" style="width: 200px;"
									id="chosenCategory7"
									data-placeholder="<spring:message code='label.choosecategory' />"
									name="chosenCategory7">
									<option value=""></option>
									<c:forEach items="${categoryList}" var="category7">
										<option value="${category7.categoryId}"
											<c:if test="${category7.categoryId eq documentMaster.category.categoryId}">selected="selected"</c:if>>${category7.categoryName}</option>
									</c:forEach>
								</select>
							</div>
						</div>


						<div class="col-md-6 form-group">
							<div>
								<input id="step8" name="8" class="form-control"
									placeholder="Enter the eighth step" type="text">
							</div>
						</div>

						<div class="form-group" id="categoryDiv7">
							<label class="col-md-2 control-label" for="chosenCategory8"><spring:message
									code="label.categoryname" /></label>
							<div class="col-md-4">
								<select class="category_chosen8" style="width: 200px;"
									id="chosenCategory8"
									data-placeholder="<spring:message code='label.choosecategory' />"
									name="chosenCategory8">
									<option value=""></option>
									<c:forEach items="${categoryList}" var="category8">
										<option value="${category8.categoryId}"
											<c:if test="${category8.categoryId eq documentMaster.category.categoryId}">selected="selected"</c:if>>${category8.categoryName}</option>
									</c:forEach>
								</select>
							</div>
						</div>

					</div>
					<div class="col-md-12 block">
						<div class="block-title">
							<h2>
								<strong><spring:message code="heading.lifeStage3" /></strong>
							</h2>
						</div>

						<div class="col-md-6 form-group">
							<div>
								<input id="step9" name="9" class="form-control"
									placeholder="Enter the nineth step" type="text">
							</div>
						</div>

						<div class="form-group" id="categoryDiv9">
							<label class="col-md-2 control-label" for="chosenCategory9"><spring:message
									code="label.categoryname" /></label>
							<div class="col-md-4">
								<select class="category_chosen9" style="width: 200px;"
									id="chosenCategory9"
									data-placeholder="<spring:message code='label.choosecategory' />"
									name="chosenCategory9">
									<option value=""></option>
									<c:forEach items="${categoryList}" var="category9">
										<option value="${category9.categoryId}"
											<c:if test="${category9.categoryId eq documentMaster.category.categoryId}">selected="selected"</c:if>>${category9.categoryName}</option>
									</c:forEach>
								</select>
							</div>
						</div>



						<div class="col-md-6 form-group">
							<div>
								<input id="step10" name="10" class="form-control"
									placeholder="Enter the tenth step" type="text">
							</div>
						</div>

						<div class="form-group" id="categoryDiv10">
							<label class="col-md-2 control-label" for="chosenCategory10"><spring:message
									code="label.categoryname" /></label>
							<div class="col-md-4">
								<select class="category_chosen10" style="width: 200px;"
									id="chosenCategory10"
									data-placeholder="<spring:message code='label.choosecategory' />"
									name="chosenCategory10">
									<option value=""></option>
									<c:forEach items="${categoryList}" var="category10">
										<option value="${category10.categoryId}"
											<c:if test="${category10.categoryId eq documentMaster.category.categoryId}">selected="selected"</c:if>>${category10.categoryName}</option>
									</c:forEach>
								</select>
							</div>
						</div>


						<div class="col-md-6 form-group">
							<div>
								<input id="step11" name="11" class="form-control"
									placeholder="Enter the eleventh step" type="text">
							</div>
						</div>

						<div class="form-group" id="categoryDiv11">
							<label class="col-md-2 control-label" for="chosenCategory11"><spring:message
									code="label.categoryname" /></label>
							<div class="col-md-4">
								<select class="category_chosen11" style="width: 200px;"
									id="chosenCategory11"
									data-placeholder="<spring:message code='label.choosecategory' />"
									name="chosenCategory11">
									<option value=""></option>
									<c:forEach items="${categoryList}" var="category11">
										<option value="${category11.categoryId}"
											<c:if test="${category11.categoryId eq documentMaster.category.categoryId}">selected="selected"</c:if>>${category11.categoryName}</option>
									</c:forEach>
								</select>
							</div>
						</div>


						<div class="col-md-6 form-group">
							<div>
								<input id="step12" name="12" class="form-control"
									placeholder="Enter the twelveth step" type="text">
							</div>
						</div>

						<div class="form-group" id="categoryDiv12">
							<label class="col-md-2 control-label" for="chosenCategory12"><spring:message
									code="label.categoryname" /></label>
							<div class="col-md-4">
								<select class="category_chosen12" style="width: 200px;"
									id="chosenCategory12"
									data-placeholder="<spring:message code='label.choosecategory' />"
									name="chosenCategory12">
									<option value=""></option>
									<c:forEach items="${categoryList}" var="category12">
										<option value="${category12.categoryId}"
											<c:if test="${category12.categoryId eq documentMaster.category.categoryId}">selected="selected"</c:if>>${category12.categoryName}</option>
									</c:forEach>
								</select>
							</div>
						</div>

					</div>
					<div class="col-md-12 block">
						<div class="block-title">
							<h2>
								<strong><spring:message code="heading.lifeStage4" /></strong>
							</h2>
						</div>

						<div class="col-md-6 form-group">
							<div>
								<input id="step13" name="13" class="form-control"
									placeholder="Enter the thirteenth step" type="text">
							</div>
						</div>

						<div class="form-group" id="categoryDiv13">
							<label class="col-md-2 control-label" for="chosenCategory13"><spring:message
									code="label.categoryname" /></label>
							<div class="col-md-4">
								<select class="category_chosen13" style="width: 200px;"
									id="chosenCategory13"
									data-placeholder="<spring:message code='label.choosecategory' />"
									name="chosenCategory13">
									<option value=""></option>
									<c:forEach items="${categoryList}" var="category13">
										<option value="${category13.categoryId}"
											<c:if test="${category13.categoryId eq documentMaster.category.categoryId}">selected="selected"</c:if>>${category13.categoryName}</option>
									</c:forEach>
								</select>
							</div>
						</div>


						<div class="col-md-6 form-group">
							<div>
								<input id="step14" name="14" class="form-control"
									placeholder="Enter the fourteenth step" type="text">
							</div>
						</div>

						<div class="form-group" id="categoryDiv6">
							<label class="col-md-2 control-label" for="chosenCategory14"><spring:message
									code="label.categoryname" /></label>
							<div class="col-md-4">
								<select class="category_chosen14" style="width: 200px;"
									id="chosenCategory14"
									data-placeholder="<spring:message code='label.choosecategory' />"
									name="chosenCategory14">
									<option value=""></option>
									<c:forEach items="${categoryList}" var="category14">
										<option value="${category14.categoryId}"
											<c:if test="${category14.categoryId eq documentMaster.category.categoryId}">selected="selected"</c:if>>${category14.categoryName}</option>
									</c:forEach>
								</select>
							</div>
						</div>


						<div class="col-md-6 form-group">
							<div>
								<input id="step15" name="15" class="form-control"
									placeholder="Enter the fifteenth step" type="text">
							</div>
						</div>

						<div class="form-group" id="categoryDiv15">
							<label class="col-md-2 control-label" for="chosenCategory15"><spring:message
									code="label.categoryname" /></label>
							<div class="col-md-4">
								<select class="category_chosen15" style="width: 200px;"
									id="chosenCategory15"
									data-placeholder="<spring:message code='label.choosecategory' />"
									name="chosenCategory15">
									<option value=""></option>
									<c:forEach items="${categoryList}" var="category15">
										<option value="${category15.categoryId}"
											<c:if test="${category15.categoryId eq documentMaster.category.categoryId}">selected="selected"</c:if>>${category15.categoryName}</option>
									</c:forEach>
								</select>
							</div>
						</div>


						<div class="col-md-6 form-group">
							<div>
								<input id="step16" name="16" class="form-control"
									placeholder="Enter the sixteenth step" type="text">
							</div>
						</div>

						<div class="form-group" id="categoryDiv16">
							<label class="col-md-2 control-label" for="chosenCategory16"><spring:message
									code="label.categoryname" /></label>
							<div class="col-md-4">
								<select class="category_chosen16" style="width: 200px;"
									id="chosenCategory16"
									data-placeholder="<spring:message code='label.choosecategory' />"
									name="chosenCategory16">
									<option value=""></option>
									<c:forEach items="${categoryList}" var="category16">
										<option value="${category16.categoryId}"
											<c:if test="${category16.categoryId eq documentMaster.category.categoryId}">selected="selected"</c:if>>${category16.categoryName}</option>
									</c:forEach>
								</select>
							</div>
						</div>

					</div>
					<div class="col-md-12 block">
						<div class="block-title">
							<h2>
								<strong><spring:message code="heading.lifeStage5" /></strong>
							</h2>
						</div>

						<div class="col-md-6 form-group">
							<div>
								<input id="step17" name="17" class="form-control"
									placeholder="Enter the seventeenth step" type="text">
							</div>
						</div>

						<div class="form-group" id="categoryDiv17">
							<label class="col-md-2 control-label" for="chosenCategory17"><spring:message
									code="label.categoryname" /></label>
							<div class="col-md-4">
								<select class="category_chosen17" style="width: 200px;"
									id="chosenCategory17"
									data-placeholder="<spring:message code='label.choosecategory' />"
									name="chosenCategory17">
									<option value=""></option>
									<c:forEach items="${categoryList}" var="category17">
										<option value="${category17.categoryId}"
											<c:if test="${category17.categoryId eq documentMaster.category.categoryId}">selected="selected"</c:if>>${category17.categoryName}</option>
									</c:forEach>
								</select>
							</div>
						</div>



						<div class="col-md-6 form-group">
							<div>
								<input id="step18" name="18" class="form-control"
									placeholder="Enter the eighteenth step" type="text">
							</div>
						</div>

						<div class="form-group" id="categoryDiv18">
							<label class="col-md-2 control-label" for="chosenCategory18"><spring:message
									code="label.categoryname" /></label>
							<div class="col-md-4">
								<select class="category_chosen18" style="width: 200px;"
									id="chosenCategory18"
									data-placeholder="<spring:message code='label.choosecategory' />"
									name="chosenCategory18">
									<option value=""></option>
									<c:forEach items="${categoryList}" var="category18">
										<option value="${category18.categoryId}"
											<c:if test="${category18.categoryId eq documentMaster.category.categoryId}">selected="selected"</c:if>>${category18.categoryName}</option>
									</c:forEach>
								</select>
							</div>
						</div>


						<div class="col-md-6 form-group">
							<div>
								<input id="step19" name="19" class="form-control"
									placeholder="Enter the nineteenth step" type="text">
							</div>
						</div>

						<div class="form-group" id="categoryDiv19">
							<label class="col-md-2 control-label" for="chosenCategory19"><spring:message
									code="label.categoryname" /></label>
							<div class="col-md-4">
								<select class="category_chosen19" style="width: 200px;"
									id="chosenCategory19"
									data-placeholder="<spring:message code='label.choosecategory' />"
									name="chosenCategory19">
									<option value=""></option>
									<c:forEach items="${categoryList}" var="category19">
										<option value="${category19.categoryId}"
											<c:if test="${category19.categoryId eq documentMaster.category.categoryId}">selected="selected"</c:if>>${category19.categoryName}</option>
									</c:forEach>
								</select>
							</div>
						</div>


						<div class="col-md-6 form-group">
							<div>
								<input id="step20" name="20" class="form-control"
									placeholder="Enter the twentieth step" type="text">
							</div>
						</div>

						<div class="form-group" id="categoryDiv20">
							<label class="col-md-2 control-label" for="chosenCategory20"><spring:message
									code="label.categoryname" /></label>
							<div class="col-md-4">
								<select class="category_chosen20" style="width: 200px;"
									id="chosenCategory20"
									data-placeholder="<spring:message code='label.choosecategory' />"
									name="chosenCategory20">
									<option value=""></option>
									<c:forEach items="${categoryList}" var="category20">
										<option value="${category20.categoryId}"
											<c:if test="${category20.categoryId eq documentMaster.category.categoryId}">selected="selected"</c:if>>${category20.categoryName}</option>
									</c:forEach>
								</select>
							</div>
						</div>

					</div>
					<div style="text-align: center; margin-bottom: 10px">
						<div class="form-group form-actions">
							<!-- 				<div class="col-md-9 col-md-offset-3"> -->
							<button id="nextsteps_submit" type="submit" value="Submit"
								class="btn btn-sm btn-primary save">
								<i class="fa fa-angle-right"></i>
								<spring:message code="button.save" />
							</button>
							<a class="btn btn-sm btn-primary save"
								href="<%=contexturl %>AtmaAdmin"> <spring:message
									code="button.cancel" />
							</a>

						</div>
					</div>
				</div>

			</div>

		</div>
	</form>
</div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<%@include file="../../inc/chosen_scripts.jsp"%>
<script src="<%=contexturl %>resources/js/custom/featurecontrol.js"></script>
<script type="text/javascript"
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCheQF_yjKacwuVL4_59EO-3BThy85gbu4&sensor=false&region=GB">
	
</script>
<script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
	<script type="text/javascript">
	
		$(document)
				.ready(
						function() {
							$("#manageNextSteps").addClass("active");

							for (var i = 1; i <= 20; i++) {
								$(".category_chosen" + i).chosen();
							}

							$("#nextsteps_submit").click(function() {
								unsaved = false;
								$("#act").val("1");
								$("#NextSteps_Form").submit();
							});

							$("#NextSteps_Form")
									.validate(
											{
												errorClass : "help-block animation-slideDown",
												errorElement : "div",
												errorPlacement : function(e, a) {
													a
															.parents(
																	".form-group > div")
															.append(e)
												},
												highlight : function(e) {
													$(e)
															.closest(
																	".form-group")
															.removeClass(
																	"has-success has-error")
															.addClass(
																	"has-error")
												},
												success : function(e) {
													e
															.closest(
																	".form-group")
															.removeClass(
																	"has-success has-error")
												},
												rules : {
													1 : {
														maxlength : 100
													},
													2 : {
														maxlength : 100
													},
													3 : {
														maxlength : 100
													},
													4 : {
														maxlength : 100
													},
													5 : {
														maxlength : 100

													},
													6 : {
														maxlength : 100
													},
													7 : {
														maxlength : 100
													},

													8 : {
														maxlength : 100
													},
													9 : {
														maxlength : 100
													},
													10 : {
														maxlength : 100
													},
													11 : {
														maxlength : 100
													},
													12 : {
														maxlength : 100
													},

													13 : {
														maxlength : 100
													},
													14 : {
														maxlength : 100
													},
													15 : {
														maxlength : 100
													},
													16 : {
														maxlength : 100
													},
													17 : {
														maxlength : 100
													},
													18 : {
														maxlength : 100
													},
													19 : {
														maxlength : 100
													},
													20 : {
														maxlength : 100
													}
												},
												messages : {
													1 : {
														maxlength : 'Step should not be more than 100 words'
													},
													2 : {
														maxlength : 'Step should not be more than 100 words'
													},
													3 : {
														maxlength : 'Step should not be more than 100 words'
													},
													4 : {
														maxlength : 'Step should not be more than 100 words'
													},
													5 : {
														maxlength : 'Step should not be more than 100 words'
													},
													6 : {
														maxlength : 'Step should not be more than 100 words'
													},
													7 : {
														maxlength : 'Step should not be more than 100 words'
													},
													8 : {
														maxlength : 'Step should not be more than 100 words'
													},
													9 : {
														maxlength : 'Step should not be more than 100 words'
													},
													10 : {
														maxlength : 'Step should not be more than 100 words'
													},
													11 : {
														maxlength : 'Step should not be more than 100 words'
													},
													12 : {
														maxlength : 'Step should not be more than 100 words'
													},
													13 : {
														maxlength : 'Step should not be more than 100 words'
													},
													14 : {
														maxlength : 'Step should not be more than 100 words'
													},
													15 : {
														maxlength : 'Step should not be more than 100 words'
													},
													16 : {
														maxlength : 'Step should not be more than 100 words'
													},
													17 : {
														maxlength : 'Step should not be more than 100 words'
													},
													18 : {
														maxlength : 'Step should not be more than 100 words'
													},
													19 : {
														maxlength : 'Step should not be more than 100 words'
													},
													20 : {
														maxlength : 'Step should not be more than 100 words'
													}
												},
											});
						});
	</script>
<style>
.chosen-container {
	width: 250px !important;
}
#map-canvas {
	height: 300px
}
</style>
