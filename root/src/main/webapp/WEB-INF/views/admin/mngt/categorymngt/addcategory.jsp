<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>


<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>

			<h1>
<!-- 				<i class="fa fa-map-marker"></i> -->
				<spring:message code="heading.category" />
				<br> <small><spring:message
						code="heading.newcategory" /></small>
			</h1>
			<span id="errorMsg"></span>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="menu.managecategory" /></li>
	</ul>
	<form action="<%=contexturl %>ManageCategory/CategoryList/SaveCategory" method="post" class="form-horizontal ui-formwizard"
		id="Category_form" enctype="multipart/form-data">
		<div class="row">

			<div class="">
				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.categoryinfo" /></strong>
						</h2>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="category_Name"><spring:message
								code="label.categoryname" /> <span class="text-danger">*</span>
						</label>
						<div class="col-md-6">

							<input id="category_Name" name="categoryName" class="form-control"
								placeholder="<spring:message code='label.categoryname'/>.."
								type="text" value="${category.categoryName}">
						</div>
					</div>

					<div class="form-group">
							<label class="col-md-4 control-label" for="categoryDescription"><spring:message
								code="label.categoryDescription" /></label>
							<div class="col-md-6">
								<textarea id="categoryDescription" name="categoryDescription" class="form-control"
									   placeholder="Description.."  >${category.categoryDescription}</textarea>
							</div>
					</div>
					
					<div class="form-group">
							<label class="col-md-4 control-label" for="categorySummary"><spring:message
								code="label.categorySummary" /><span class="text-danger">*</span></label>
							<div class="col-md-6">
								<textarea id="categorySummary" name="categorySummary" class="form-control" 
 									   placeholder="Summary.."  >${category.categorySummary}</textarea> 
							</div> 
					</div> 
					
					<div class="form-group" id="moduleDiv">
						<label class="col-md-4 control-label" for="chosenModuleId"><spring:message
 								code="label.modulename" /><span class="text-danger">*</span></label> 
						<div class="col-md-6">
							<select class="module_chosen" style="width: 200px;"
								id="chosenModuleId"
								data-placeholder="<spring:message code='label.choosemodule' />"
								name="chosenModuleId">
								<option value=""></option>
								<c:forEach items="${moduleList}" var="module">
											<option value="${module.moduleId}" 
											<c:if test="${module.moduleId eq category.module.moduleId}">selected="selected"</c:if>>${module.moduleName}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					


				</div>



	</div>

			
</div>

	<div style="text-align: center; margin-bottom: 10px">
		<div class="form-group form-actions">
<!-- 				<div class="col-md-9 col-md-offset-3"> -->
					<button id="company_submit" type="submit"
						class="btn btn-sm btn-primary save">
						<i class="fa fa-angle-right"></i> <spring:message code="button.save" />
					</button>
						<a class="btn btn-sm btn-primary save" href="<%=contexturl %>ManageCategory/CategoryList">
							<spring:message code="button.cancel" /> </a>
		</div>
	</div>
</form>
</div>
<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>

<script src="<%=contexturl %>resources/js/custom/featurecontrol.js"></script>
<%@include file="../../inc/chosen_scripts.jsp"%>
<script type="text/javascript">
	$(document)
			.ready(
					function() {
						
						$("#manageCategory").addClass("active");
						
					
						$(".module_chosen").data("placeholder","Select Module From...").chosen();	
 						$("#manageCategory").addClass("active");
						$("#Category_form").validate(
						{	errorClass:"help-block animation-slideDown",
							errorElement:"div",
							errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
							highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
							success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
										rules : {
											categoryName : {
												required : !0,maxlength : 75
											},
											categoryDescription : {
												maxlength : 500
											},
											categorySummary : {
												required : !0,
												maxlength : 255
											},
											
											chosenModuleId: {
												required : !0
											}
											
										},
										messages : {
											categoryName : {
												required :'<spring:message code="validation.pleaseentercategoryName"/>',
													maxlength :'<spring:message code="validation.categoryName75character"/>'
											},
											categoryDescription : {
												maxlength :'<spring:message code="validation.description500character"/>'
											},
											categorySummary : {
												required : '<spring:message code="validation.pleaseentercategorySummary"/>',
												maxlength : '<spring:message code="validation.categorySummary255character"/>'
											},
											
											chosenModuleId: {
												required : '<spring:message code="validation.selectmodule"/>'
											}
											
										},
								});

						$("#company_submit").click(function() {
							unsaved=false;
							$("#Company_form").submit();
						});
						
						$("button[type=reset]").click(function(evt) {
							evt.preventDefault(); // stop default reset behavior (or the rest of this code will run before the reset actually happens)
							form = $(this).parents("form").first(); // get the reset buttons form
							form[0].reset(); // actually reset the form
							 // tell all Chosen fields in the form that the values may have changed
							form.find(".country_chosen").trigger("chosen:updated");
							
							var myOptions = "<option value></option>";
							$(".city_chosen").html(myOptions).chosen().trigger("chosen:updated");
							
							$(".state_chosen").html(myOptions).chosen().trigger("chosen:updated");
							});

						
					});
	

</script>

<style>
.chosen-container {
	width: 250px !important;
}
#map-canvas {
	height: 300px
}
</style>
<%@include file="../../inc/template_end.jsp"%>