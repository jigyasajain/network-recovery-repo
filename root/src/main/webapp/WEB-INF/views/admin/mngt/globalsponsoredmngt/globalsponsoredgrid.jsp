<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>





<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				<spring:message code="heading.globalsponsoredcontactimagemaster" />
			</h1>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i>
						<spring:message code="label.success" />
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>
			<div class="alert alert-danger alert-dismissable"
				style="display: none" id="error">
				<button type="button" class="close" data-dismiss="alert"
					aria-hidden="true">x</button>
				<h4>
					<i class="fa fa-times-circle"> </i> <spring:message code="label.error"/>
				</h4>
				<span id="errorMsg"></span>
			</div>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="heading.manageglobalsponsoredcontactimage" /></li>
	</ul>
	<div class="block full  gridView">
		<div class="block-title">
		</div>
		<a href="<%=contexturl %>ManageGlobalSponsoredImage/GlobalSponsoredImageList/AddGlobalSponsoredImage" id="addUniversity" class="btn btn-sm btn-primary save"><spring:message
 				code="label.addimage" /></a> 
<%--  		<a href="<%=contexturl %>ManageGlobalSponsoredImage/GlobalSponsoredImageList/EditContactImage" id="addContact" class="btn btn-sm btn-primary save pull-right">Add Contact Image</a>  --%>
		<div class="table ">
			<table id="active-datatable"
				class="table table-vcenter table-condensed table-bordered">
				<thead>
					<tr>
					<th class="text-center"><spring:message code="label.id" /></th> 
						<th class="text-center"/><spring:message code="label.imageType" /></th>
						<th class="text-center"><spring:message code="label.image" /></th>
						
				        <th class="text-center"><spring:message code="label.action" /></th>
					</tr>
				</thead>
				<tbody id="tbody">
					<c:forEach var="currentImage" items="${globalList}"
						varStatus="count">

						<tr>
							<td class="text-center">${count.count}</td>
							<td class="text-center">${currentImage.imageType}</td>
							<td class="text-center"><c:choose>
								<c:when test="${not empty(currentImage.globalImage)}"><img border="0" src="<%=contexturl%>${currentImage.globalImage.imagePath}" width="50" height="50" /></c:when>
								<c:when test="${not empty(currentImage.contactImage)}"><img border="0" src="<%=contexturl%>${currentImage.contactImage.imagePath}" width="50" height="50" /></c:when>
								<c:otherwise><c:out value="" /></c:otherwise>
							</c:choose>
							</td>
							
							<td class="text-center">
								<div class="btn-group">
									<c:if test= "${currentImage.imageType eq 'Contact'}">
									<a href="<%=contexturl %>ManageGlobalSponsoredImage/GlobalSponsoredImageList/EditContactImage?id=${currentImage.globalId}"
										data-toggle="tooltip" title="Edit"
										class="btn btn-xs btn-default save"><i class="fa fa-pencil"></i></a>
									</c:if>
									<a href="#delete_company_popup" data-toggle="modal"
										onclick="deleteCompany(${currentImage.globalId})"
										title="delete image" class="btn btn-xs btn-danger save"><i
										class="fa fa-times"></i></a>

								</div>
							</td>
						</tr>
					</c:forEach>

				</tbody>
			</table>
		</div>
	</div>



	<div id="delete_company_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="<%=contexturl %>ManageGlobalSponsoredImage/GlobalSponsoredImageList/DeleteGlobalSponsoredImage" method="post" class="form-horizontal form-bordered save"
						id="delete_company_Form">
						<div style="padding: 10px; height: 110px;">
							<label>Do you want to delete this image?</label><br>
							<label>Deleting this image will delete all the details of this image.</label>
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal">
									<spring:message code="label.no" />
								</button>
								<div id="delete_company" class="btn btn-sm btn-primary">
									<spring:message code="label.yes" />
								</div>
							</div>
						</div>
						<input type="hidden" name="id" id="deletecompanyId">

					</form>

				</div>
			</div>
		</div>
	</div>




</div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<script
	src="<%=contexturl%>resources/js/pages/activeTablesDatatables.js"></script>
<script
	src="<%=contexturl%>resources/js/pages/inactiveTablesDatatables.js"></script>
<script>$(function(){ ActiveTablesDatatables.init(3); });</script>

<script type="text/javascript">
	var selectedId = 0;
	var restoreId=0;
	$(document).ready(function() {
		
		
		
		$("#manageGlobalSponsoredImage").addClass("active");
		
		
		$("#delete_company").click(function(){
			$("#deletecompanyId").val(selectedId);
			$("#delete_company_Form").submit();
		});
	});
	
	function deleteCompany(id){
		selectedId = id;
	}

</script>
<%@include file="../../inc/template_end.jsp"%>