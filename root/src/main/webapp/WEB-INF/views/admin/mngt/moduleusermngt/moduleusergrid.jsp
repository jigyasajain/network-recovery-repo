<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="page_head.jsp"%>



<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
<!-- 				<i class="fa fa-map-marker"></i> -->
				<spring:message code="heading.moduleuser" />
				<br> <small><spring:message
						code="heading.specifylistofuser" /> </small>
			</h1>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i>
						<spring:message code="label.success" />
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>
			<div class="alert alert-danger alert-dismissable"
				style="display: none" id="error">
				<button type="button" class="close" data-dismiss="alert"
					aria-hidden="true">x</button>
				<h4>
					<i class="fa fa-times-circle"> </i> <spring:message code="label.error"/>
				</h4>
				<span id="errorMsg"></span>
			</div>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="heading.managemoduleuser" /></li>
		<li><a href="#"><spring:message code="heading.moduleuser" /></a></li>
	</ul>
	<div class="block full  gridView">
		<div class="block-title">
			<h2>
				<strong><spring:message code="heading.activemoduleuser" /></strong>
			</h2>
		</div>
		
		<P>
        <a href="<%=contexturl %>ManageModuleUser/ModuleUserList/AddModuleUser" id="addUniversity" class="btn btn-sm btn-primary save"><spring:message
				code="label.addmoduleuser"  /></a>
		<a href="<%=contexturl %>ManageModuleUser/ModuleUserList/AssignModule" id="assignModule" class="btn btn-sm btn-primary save pull-right"><spring:message
		        code="label.assigningModule"  /></a>
		</P>
		<div class="table-responsive">
			<table id="active-datatable"
				class="table table-vcenter table-condensed table-bordered">
				<thead>
					<tr>
						<th class="text-center"><spring:message code="label.id" /></th>
						<th class="text-center"><spring:message code="label.modulename" /></th>
						<th class="text-center"><spring:message code="label.moduleusername" /></th>
						<th class="text-center"><spring:message code="label.createdBy"/></th>
						<th class="text-center"><spring:message code="label.actions" /></th>
					</tr>
				</thead>
				<tbody id="tbody">
					<c:forEach var="currentModuleUser" items="${moduleuserActiveList}"
						varStatus="count">
						
						<tr>
						<td class="text-center">${count.count}</td>
						
							<td class="text-left"> <c:forEach var="modUser" items="${moduleUser}">
						    <c:if test="${modUser.user.userId eq currentModuleUser.userId}"><c:out value="${modUser.module.moduleName},"/></c:if></c:forEach></td>
							
							<td class="text-left"><c:out value="${currentModuleUser.fullName}" /></td>
							<td class="text-left"><c:out value="${currentModuleUser.createdBy.fullName}" /></td>
							
							<td class="text-center">
								<div class="btn-group">
									<a href="<%=contexturl %>ManageModuleUser/ModuleUserList/EditModuleUser?id=${currentModuleUser.userId}"
										data-toggle="tooltip" title="Edit"
										class="btn btn-xs btn-default save"><i class="fa fa-pencil"></i></a>
									<a href="#delete_company_popup" data-toggle="modal"
										onclick="deleteCompany(${currentModuleUser.userId})"
										title="Delete" class="btn btn-xs btn-danger save"><i
										class="fa fa-times"></i></a>
								</div>
								</td>
								
						</tr>
					</c:forEach>
					</tbody>
			</table>
		</div>
	</div>


	<div class="block full  gridView">
		<div class="block-title">
			<h2>
				<strong><spring:message code="heading.inactivemoduleuser" /></strong>
			</h2>
		</div>
		<div class="table-responsive">
			<table id="inactive-datatable"
				class="table table-vcenter table-condensed table-bordered ">
				<thead>
					<tr>
						<th class="text-center"><spring:message code="label.id" /></th>
						<th class="text-center"><spring:message code="label.modulename" /></th>
						<th class="text-center"><spring:message code="label.moduleusername" /></th>
						<th class="text-center"><spring:message code="label.createdBy"/></th>
						<th class="text-center"><spring:message code="label.actions" /></th>
					</tr>
				</thead>
				<tbody id="tablebody">
					<c:forEach var="currentModuleUser" items="${moduleuserInactiveList}"
						varStatus="count">
						<tr>
							<td class="text-center">${count.count}</td>
						
							<td class="text-left"> <c:forEach var="modUser" items="${moduleUser}">
						    <c:if test="${modUser.user.userId eq currentModuleUser.userId}"><c:out value="${modUser.module.moduleName},"/></c:if></c:forEach></td>
							<td class="text-left"><c:out
									value="${currentModuleUser.fullName}" /></td>
									<td class="text-left"><c:out value="${currentModuleUser.createdBy.fullName}" /></td>
							<td class="text-center">
								<div class="btn-group">
									<a href="#restore_Company_popup" data-toggle="modal"
										onclick="restoreCompany(${currentModuleUser.userId})"
										title="Restore" class="btn btn-xs btn-success save"><i
										class="fa fa-times"></i> </a>
								</div>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	<div id="delete_company_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="<%=contexturl %>ManageModuleUser/ModuleUserList/DeleteUser" method="post" class="form-horizontal form-bordered save"
						id="delete_company_Form">
						<div style="padding: 10px; height: 110px;">
							<label><spring:message
									code="validation.doyouwanttodeletethismoduleuser" /></label><br>
							<label><spring:message
									code="validation.deletingmoduleuserwilldeletealldetailsofmoduleuser" /></label>
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal">
									<spring:message code="label.no" />
								</button>
								<div id="delete_company" class="btn btn-sm btn-primary">
									<spring:message code="label.yes" />
								</div>
							</div>
						</div>
						<input type="hidden" name="id" id="deletecompanyId">

					</form>

				</div>
			</div>
		</div>
	</div>


	<div id="restore_Company_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="<%=contexturl %>ManageModuleUser/ModuleUserList/RestoreUser" method="post" class="form-horizontal form-bordered save" id="restore_company_Form">
						
						<div style="padding: 10px; height: 110px;">
							<label><spring:message
									code="validation.doyouwanttorestorethismoduleuser" /></label><br>
							<label><spring:message
									code="validation.restoremoduleuserwillrestoreallperviouslydeleteddetails" /></label>
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal"><spring:message code="label.no" /></button>
								<div id="restore_Company" class="btn btn-sm btn-primary">	<spring:message code="label.yes" /></div>
							</div>
						</div>
						<input type="hidden" name="id" id="restorecompanyId">
					</form>
				</div>
			</div>
		</div>
	</div>

</div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<script
	src="<%=contexturl%>resources/js/pages/activeTablesDatatables.js"></script>
<script
	src="<%=contexturl%>resources/js/pages/inactiveTablesDatatables.js"></script>
<script>$(function(){ ActiveTablesDatatables.init(4); });</script>
<script>$(function(){ InActiveTablesDatatables.init(4); });</script>
<script type="text/javascript">
	var selectedId = 0;
	var restoreId=0;
	$(document).ready(function() {
		$("#manageModuleUser").addClass("active");
		
        $("#restore_Company").click(function(){
			
			$("#restorecompanyId").val(restoreId);
			$("#restore_company_Form").submit();
		});
		
		
		$("#delete_company").click(function(){
			
			$("#deletecompanyId").val(selectedId);
			$("#delete_company_Form").submit();
		});
	});
	
	function deleteCompany(id){
		selectedId = id;
	}
	function restoreCompany(id){
		restoreId = id;
	}
</script>
<%@include file="../../inc/template_end.jsp"%>