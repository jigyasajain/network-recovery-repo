
<%@page import="com.astrika.common.model.MemberType"%>
<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>

<style>
.chosen-container {
	width: 250px !important;
}
.rating-breakup{
border: none !important;
margin-top: -30px !important;
}

.page-style {
	margin-left: 16px;
	text-align: left!important;
	
}
.label-align{
text-align: left;

}
.page-profile{
width: 98px!important;
height: 98px!important;
border-radius: 49px!important;}
</style>

<div id="page-content">


	<form action="" method="post" class="form-horizontal form-bordered" id="member_form">
		

		<div class="row">
		
				<div class="block" style="margin-left: 16px;">
					<div class="block-title">
						<h2>
							<span class="label label-success"><c:out
									value="${outletPoints.membershipType}" /></span>
							<c:set value="<%=MemberType.PAID_INDIVIDUAL%>" var="paidMember" />
							<c:set value="<%=MemberType.CORPORATE%>" var="corporateMember" />
							<c:choose>

								<c:when test="${member.user.memberType eq paidMember}">
									<span class="label label-primary"><spring:message
											code="label.paidmember" /></span>
								</c:when>
								<c:otherwise>
									<span class="label label-primary"><spring:message
											code="label.corporatemember" /></span>
								</c:otherwise>
							</c:choose>
							<span class="label label-info"><c:out
									value="${outletPoints.recieved}" /></span>
						</h2>

					</div>
					<div class="row">
						<div class="sidebar-user-avatar" style="margin-left: 18px;min-height: 300px;width:15%;float: left;">
							<c:choose>
								<c:when
									test="${! empty member.user.profileImage && ! empty member.user.profileImage.imagePath }">
									<img class="page-profile"
										src="<%=contexturl %>${member.user.profileImage.imagePath}"
										alt="avatar">
								</c:when>
								<c:otherwise>
									<img class="page-profile"
										src="<%=contexturl%>resources/img/application/default_profile.png"
										alt="avatar">
								</c:otherwise>
							</c:choose>

						</div>

						<div style="width:70%;float: left;">
							<div class="form-group">
								<label class="col-md-5 control-label page-style" for="first_Name"><spring:message
										code="label.firstname" /></label>
								<div class="col-md-6">
									<!-- 							<input id="first_Name" name="firstName" class="form-control" -->
									<%-- 								placeholder="<spring:message code='label.firstname'/>.." --%>
									<%-- 								type="text" value="${member.user.firstName}" disabled="disabled"> --%>

									<label class="control-label" for="maritalstatus"><c:out
											value="${member.user.firstName}" /></label>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-5 control-label page-style" for="last_Name"><spring:message
										code="label.lastname" /></label>
								<div class="col-md-6">

									<!-- 							<input id="last_Name" name="lastName" class="form-control" -->
									<%-- 								placeholder="<spring:message code="label.lastname"/>.." --%>
									<%-- 								type="text" value="${member.user.lastName}" disabled="disabled"> --%>

									<label class="control-label" for="maritalstatus"><c:out
											value="${member.user.lastName}" /></label>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-5 control-label page-style"><spring:message
										code="label.userlogin" /></label>
								<div class="col-md-6">
									<p class="form-control-static">${member.user.loginId}</p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-5 control-label page-style" for="birthday"><spring:message
										code="label.birthday" /></label>
								<div class="col-md-6">
									<label class="control-label" for="birthday"> 
									<fmt:formatDate pattern="<%=propvalue.simpleDateFormat %>" value="${member.birthday}" /></label>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-5 control-label page-style"
									for="maritalstatus"><spring:message
										code="label.maritalstatus" /></label>
								<div class="col-md-6">
									<label class="control-label" for="maritalstatus"><c:out
											value="${ member.maritalStatus}" /></label>
								</div>
							</div>
							<c:choose>
								<c:when test="${'Married' eq member.maritalStatus}">
									<div class="form-group" id="aniversary_div">
										<label class="col-md-5 control-label page-style"
											for="anniversary"><spring:message
												code="label.anniversary" /></label>
										<div class="col-md-6">
											<label class="control-label" for="anniversary"> <fmt:formatDate
													pattern="<%=propvalue.simpleDateFormat %>"
													value="${member.anniversary}" /></label>
										</div>
									</div>
								</c:when>
								<c:otherwise>

								</c:otherwise>
							</c:choose>
							<div class="form-group">
								<label class="col-md-5 control-label page-style"
									for="rating">Average Rating</label>
								<div id="b_" class="row col-md-6" >
									<div id="useravgrating" class="col-md-2"
										style="cursor: pointer;"></div>
								</div>
							</div>
							<div class="form-group" style="border: none;">
								<label class="col-md-5 control-label page-style"
									for="rating">Food</label>
								<div id="b_" class="row col-md-6" >
									<div id="user_rating1" class="col-md-2"
										style="cursor: pointer;"></div>
								</div>
							</div>
							<div class="form-group rating-breakup">
								<label class="col-md-5 control-label page-style"
									for="rating">Ambiance</label>
								<div id="b_" class="row col-md-6" >
									<div id="user_rating2" class="col-md-2"
										style="cursor: pointer;"></div>
								</div>
							</div>
							<div class="form-group rating-breakup">
								<label class="col-md-5 control-label page-style"
									for="rating">Service</label>
								<div id="b_" class="row col-md-6" >
									<div id="user_rating3" class="col-md-2"
										style="cursor: pointer;"></div>
								</div>
							</div>
							<div class="form-group rating-breakup">
								<label class="col-md-5 control-label page-style"
									for="rating">Value For Money</label>
								<div id="b_" class="row col-md-6" >
									<div id="user_rating4" class="col-md-2"
										style="cursor: pointer;"></div>
								</div>
							</div>
							<div class="form-group rating-breakup">
								<label class="col-md-5 control-label page-style"
									for="rating">Hygiene</label>
								<div id="b_" class="row col-md-6" >
									<div id="user_rating5" class="col-md-2"
										style="cursor: pointer;"></div>
								</div>
							</div>



							<!-- 					<div class="form-group"> -->
							<%-- 						<label class="col-md-5 control-label page-style" for="lastcheckedin"><spring:message --%>
							<%-- 								code="label.lastcheckedin" /></label> --%>
							<!-- 						<div class="col-md-6"> -->
							<%-- 							<label class="control-label" for="lastcheckedin"><c:out --%>
							<%-- 									value="${outletCustomerLast.checkInTime}" /></label> --%>
							<!-- 						</div> -->
							<!-- 					</div> -->

							<!-- 					<div class="form-group"> -->
							<%-- 						<label class="col-md-5 control-label page-style" for="lastbill"><spring:message --%>
							<%-- 								code="label.lastbill" /></label> --%>
							<!-- 						<div class="col-md-6"> -->
							<%-- 							<label class="control-label" for="lastbill"><c:out --%>
							<%-- 									value="${outletCustomerLast.billAmount}" /></label> --%>
							<!-- 						</div> -->
							<!-- 					</div> -->

							<!-- 					<div class="form-group"> -->
							<%-- 						<label class="col-md-5 control-label page-style" for="maritalstatus"><spring:message --%>
							<%-- 								code="label.points" /></label> --%>
							<!-- 						<div class="col-md-6"> -->
							<%-- 							<label class="control-label" for="points"><c:out --%>
							<%-- 									value="${outletPoints.recieved}" /></label> --%>
							<!-- 						</div> -->
							<!-- 					</div> -->


							<div class="form-group class="sub-header"">


								<div class="col-sm-4">
									<label class="col-md-6 control-label" for="creditCard"><spring:message
											code="label.smoking" /></label>
									<div class="col-md-4">
										<label class="switch switch-primary"> <input
											id="smoking" name="smoking" type="checkbox"
											<c:if test="${member.smoking}"> checked </c:if>
											disabled="disabled" /> <span></span></label>
									</div>
								</div>
								<div class="col-sm-4">
									<label class="col-md-6 control-label" for="alcohol"><spring:message
											code="label.alcohol" /></label>
									<div class="col-md-4">
										<label class="switch switch-primary"> <input
											id="alcohol" name="alcohol" type="checkbox"
											<c:if test="${member.preferAlcohol}"> checked </c:if>
											disabled="disabled" /> <span></span></label>
									</div>
								</div>
								<div class="col-sm-4">
									<label class="col-md-6 control-label" for="nonVeg"><spring:message
											code="label.nonvegitarian" /> </label>
									<div class="col-md-4">
										<label class="switch switch-primary"> <input
											id="nonVeg" name="nonVeg" type="checkbox"
											<c:if test="${member.nonvegeterian}"> checked </c:if>
											disabled="disabled" /> <span></span></label>
									</div>
								</div>
							</div>

						</div>
					</div>



					<div class="row">
						<div class="table-responsive">
							<table class="table table-vcenter table-striped ">
								<thead>
									<tr >
										<th>Check In Time</th>
										<th>Check Out Time</th>
										<th>Table Alloted</th>
										<th>Bill Amount</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="outletCustomer" items="${outletCustomerList}"
										varStatus="count">
										<tr>
											<td class="text-left">
											<fmt:formatDate pattern="<%=propvalue.simpleDateTimezoneFormat %>"
													value="${outletCustomer.checkInDate}" /></td>
											<td class="text-left"><fmt:formatDate
													pattern="<%=propvalue.simpleDatetimeFormat %>"
													value="${outletCustomer.checkOutDate}" /></td>
											<td class="text-left"><c:out
													value="${outletCustomer.tableAlloted}" /></td>
											<td class="text-left"><c:out
													value="${outletCustomer.billAmount}" /></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>

					</div>
					<div class="form-group form-actions">
						<div class="col-md-9 col-md-offset-3">
							<a class="btn btn-sm btn-primary"
								href="<%=contexturl%>ManageRestaurant/Checkins">
								 <spring:message code="button.back" />
							</a>
							<a class="btn btn-sm btn-primary"
								href="<%=contexturl%>ManageRestaurant/ViewCheckins/${member.user.userId}">
								 <spring:message code="button.seemore" />
							</a>
						</div>
					</div>

				</div>





			
			</div>
	</form>




</div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>

<script type="text/javascript">
        $(document).ready(function() {
			$('#useravgrating').raty({
				readOnly	: true,
				hints		: ['', '', '', '', ''],
				half		: true,
				path		: '<%=contexturl%>resources/img/raty/',
				scoreName	: "useravgRating",	
				width		: 125,
				score		: '${userOutletRating.avgrating}'
			});
        });
        $(document).ready(function() {
			$('#user_rating1').raty({
				readOnly	: true,
				hints		: ['', '', '', '', ''],
				half		: true,
				path		: '<%=contexturl%>resources/img/raty/',
				scoreName	: "user_rating1",	
				width		: 125,
				score		: '${userOutletRating.rating1}'
			});
        });
        $(document).ready(function() {
			$('#user_rating2').raty({
				readOnly	: true,
				hints		: ['', '', '', '', ''],
				half		: true,
				path		: '<%=contexturl%>resources/img/raty/',
				scoreName	: "user_rating2",	
				width		: 125,
				score		: '${userOutletRating.rating2}'
			});
        });
        $(document).ready(function() {
			$('#user_rating3').raty({
				readOnly	: true,
				hints		: ['', '', '', '', ''],
				half		: true,
				path		: '<%=contexturl%>resources/img/raty/',
				scoreName	: "user_rating3",	
				width		: 125,
				score		: '${userOutletRating.rating3}'
			});
        });
        $(document).ready(function() {
			$('#user_rating4').raty({
				readOnly	: true,
				hints		: ['', '', '', '', ''],
				half		: true,
				path		: '<%=contexturl%>resources/img/raty/',
				scoreName	: "user_rating4",	
				width		: 125,
				score		: '${userOutletRating.rating4}'
			});
        });
        $(document).ready(function() {
			$('#user_rating5').raty({
				readOnly	: true,
				hints		: ['', '', '', '', ''],
				half		: true,
				path		: '<%=contexturl%>resources/img/raty/',
				scoreName	: "user_rating5",	
				width		: 125,
				score		: '${userOutletRating.rating5}'
			});
        });
</script>
<%@include file="../../inc/template_end.jsp"%>