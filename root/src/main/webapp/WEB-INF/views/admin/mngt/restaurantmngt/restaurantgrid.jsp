<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>

<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				<i class="fa fa-map-marker"></i> <spring:message code="heading.restaurantmaster"/><br> 
				<small>You can add, remove or edit your restaurants here.
				</small>
			</h1>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i> <spring:message code="label.success"/>
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
					</h4>
					<spring:message code="${error}" /><c:if test="${!empty lockedBy }">${lockedBy}</c:if>
				</div>
			</c:if>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="menu.managerestaurants"/></li>
		<li><a href=""><spring:message code="menu.restaurant"/></a></li>
	</ul>
	<div class="block full ">
		<a href="<%=contexturl %>ManageRestaurant/RestaurantList/AddRestaurant" id="addRestaurant" class="btn btn-sm btn-primary"><spring:message code="label.addrestaurant"/></a>
		<small >You can add new restaurant here</small>
	</div>
	<div class="block full gridView">
		<div class="block-title">
			<h2>
				<strong><spring:message code="heading.activerestaurant"/></strong>
				<small style="float:left; margin-top: 4px;">Your current active restaurants with Gourmet7. you can make any changes in these restaurants and also make them inactive if they are no longer operational by clicking Action button on the extreme right along with the restaurant details</small>
			</h2>
		</div>
		
		<div class="table-responsive">
			<table id="active-datatable"
				class="table table-vcenter table-condensed table-bordered">
				<thead>
					<tr>
						<th class="text-center"><spring:message code="label.id" /></th>
						<th class="text-center"><spring:message code="menu.restaurant"/></th>
						<th class="text-center"><spring:message code="label.brand"/></th>
						<th class="text-center"><spring:message code="label.company"/></th>
						<th class="text-center"><spring:message code="label.city"/></th>
						<th class="text-center"><spring:message code="label.admin" /></th>
						<th class="text-center"><spring:message code="label.actions"/></th>
					</tr>
				</thead>
				<tbody id="tbody">
					<c:forEach var="currentRestaurant" items="${restaurantList}"
						varStatus="count">

						<tr>
							<td class="text-center">${count.count}</td>
							<td class="text-left"><c:out
									value="${currentRestaurant.name}" /></td>
							<td class="text-left"><c:out
								value="${currentRestaurant.brand.brandName}" /></td>
							<td class="text-left"><c:out
									value="${currentRestaurant.brand.company.companyName}" /></td>
							<td class="text-left"><c:out
									value="${currentRestaurant.city.cityName}" /></td>
							<td class="text-left"><c:out
									value="${currentRestaurant.admin.fullName}" /></td>
							<td class="text-center">
								<div class="btn-group">
									<a href="javascript:void(0)" data-toggle="dropdown"
										class="btn btn-primary dropdown-toggle"><spring:message code="label.actions"/><span
										class="caret"></span></a>
									<ul class="dropdown-menu text-left">
										<li><a
											href="<%=contexturl %>ManageRestaurant/RestaurantList/EditRestaurant?id=${currentRestaurant.outletId}"><spring:message code="label.editrestaurant"/>
												</a></li>
										<li><a href="#delete_restaurant_popup"
											data-toggle="modal"
											onclick="deleteRestaurant(${currentRestaurant.outletId})"><spring:message code="label.inactiverestaurant"/>
												</a></li>
<%-- 										<li><a href="javascript:void(0)"><spring:message code="menu.changerestaurantadmin"/> --%>
<!-- 												</a></li> -->
										<li><a href="<%=contexturl %>ManageRestaurant/RestaurantList/MenuList?id=${currentRestaurant.outletId}"><spring:message code="menu.menu"/></a></li>
										<li><a href="<%=contexturl %>ManageRestaurant/RestaurantList/OfferList?outletId=${currentRestaurant.outletId}"
											id="restaurantOffer"><spring:message code="heading.offer"/></a></li>
										<li><a href="<%=contexturl %>ManageRestaurant/RestaurantList/PromoDescriptionList?outletId=${currentRestaurant.outletId}" id="restaurantPromocode">
											<spring:message code="menu.promocode"/></a></li>
										<li><a href="<%=contexturl %>ManageRestaurant/RestaurantList/MembershipModalList?outletId=${currentRestaurant.outletId}" id="membershipModal">
											<spring:message code="menu.membershipmodal"/></a></li>
<%-- 										<li><a href="<%=contexturl %>ManageRestaurant/RestaurantList/MealMenuList?outletId=${currentRestaurant.outletId}" id="restaurantPromocode1"> --%>
<%-- 											<spring:message code="menu.mealmenu"/></a></li>	 --%>
<!--                                            <li><a href="CommingSoon" id="restaurantPromocode1"> -->
<%-- 											<spring:message code="menu.mealmenu"/></a></li>	 --%>
											
											<li><a href="<%=contexturl %>ManageRestaurant/RestaurantList/RestaurantRatingAndFeedback?outletId=${currentRestaurant.outletId}">Rating/Feedback</a></li>
							                 <li><a href="<%=contexturl %>ManageRestaurant/RestaurantList/FollowerList?outletId=${currentRestaurant.outletId}">Followers</a></li>
									</ul>
								</div>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	
		<div class="block full gridView">
		<div class="block-title">
			<h2>
				<strong><spring:message code="heading.inactiverestaurant" /></strong>
				<small style="float:left; margin-top: 4px;">Inactive restaurants don't appear for user search. If you wish to re-activate any of the restaurants, you can do so by clicking + sign on the extreme right along with the restaurant.</small>
			</h2>
		</div>
		<div class="table-responsive">
			<table id="inactive-datatable"
				class="table table-vcenter table-condensed table-bordered ">
				<thead>
					<tr>
						<th class="text-center"><spring:message code="label.id" /></th>
						<th class="text-center"><spring:message code="menu.restaurant"/></th>
						<th class="text-center"><spring:message code="label.brand"/></th>
						<th class="text-center"><spring:message code="label.company"/></th>
						<th class="text-center"><spring:message code="label.city"/></th>
						<th class="text-center"><spring:message code="label.admin" /></th>
						<th class="text-center"><spring:message code="label.actions"/></th>
					</tr>
				</thead>
				<tbody id="tablebody">
					<c:forEach var="currentRestaurant" items="${inactiveRestaurantList}"
						varStatus="count">
						<tr>
						<td class="text-center">${count.count}</td>
						<td class="text-left"><c:out
									value="${currentRestaurant.name}" /></td>
						<td class="text-left"><c:out
									value="${currentRestaurant.brand.brandName}" /></td>
							<td class="text-left"><c:out
									value="${currentRestaurant.brand.company.companyName}" /></td>
							<td class="text-left"><c:out
									value="${currentRestaurant.city.cityName}" /></td>
							<td class="text-left"><c:out
									value="${currentRestaurant.admin.fullName}" /></td>
							<td class="text-center">
								<div class="btn-group">
									<a href="#restore_Restaurant_popup" data-toggle="modal"
										onclick="restoreRestaurant(${currentRestaurant.outletId})"
										title="Restore Restaurant" class="btn btn-xs btn-success"><i
										class="fa fa-plus"></i> </a>
								</div>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	
	
	<div id="delete_restaurant_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="<%=contexturl %>ManageRestaurant/RestaurantList/DeleteRestaurant" method="post" class="form-horizontal form-bordered"
						id="DeleteRest_Form">
						<div style="padding: 10px; height: 110px;">
							<label><spring:message code="validation.doyouwanttodeletethisrestaurant"/></label><br>
							<label><spring:message code="validation.deletingrestaurantwilldeleteduserofrestaurant"/></label>
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal"><spring:message code="label.no"/></button>
								<div id="delete_restaurant" class="btn btn-sm btn-primary"
									><spring:message code="label.yes"/></div>
							</div>
						</div>
						<input type="hidden"  name="id" id="restId">
					</form>

				</div>
			</div>
		</div>
	</div>
	<div id="restore_Restaurant_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="<%=contexturl %>ManageRestaurant/RestaurantList/RestoreRestaurant" method="post" class="form-horizontal form-bordered" id="restore_restaurant_Form">
						
						<div style="padding: 10px; height: 110px;">
							<label><spring:message
									code="validation.doyouwanttorestorethisrestaurant" /></label><br>
							<label><spring:message code="validation.restorerestaurantwillrestoreallpreviouslydeleteduserofrestaurant" /></label>
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal"><spring:message code="label.no" /></button>
								<div id="restore_Restaurant" class="btn btn-sm btn-primary">	<spring:message code="label.yes" /></div>
							</div>
						</div>
						<input type="hidden"  name="id" id="restorerestaurantId">
					</form>
				</div>
			</div>
		</div>
	</div>
	
</div>
<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>

<!-- Load and execute javascript code used only in this page -->
<script src="<%=contexturl %>resources/js/pages/activeTablesDatatables.js"></script>
<script src="<%=contexturl %>resources/js/pages/inactiveTablesDatatables.js"></script>
<script>$(function(){ ActiveTablesDatatables.init(6); });</script>
<script>$(function(){ InActiveTablesDatatables.init(6); });</script>
<script type="text/javascript">
	var selectedId = 0;
	var restoreId =0;
	$(document).ready(function() {
		
		  $("#restore_Restaurant").click(function(){
				
				$("#restorerestaurantId").val(restoreId);
				$("#restore_restaurant_Form").submit();
			});
		
		$("#delete_restaurant").click(function(){
			$("#restId").val(selectedId);
			$("#DeleteRest_Form").submit();
		});
	});
	
	
	function deleteRestaurant(id){
		selectedId = id;
	}
	function restoreRestaurant(id){
		restoreId = id;
	}
</script>

<%@include file="../../inc/template_end.jsp"%>
