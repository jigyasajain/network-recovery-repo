<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>
<link rel="stylesheet" href="<%=contexturl %>resources/css/jquery-ui.css">
<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				<i class="fa fa-map-marker"></i>
				<spring:message code="heading.instructor" />
				<br> <small><spring:message
						code="heading.instructordetails" /></small>
			</h1>
			<span id="errorMsg"></span>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i> <spring:message code="label.success"/>
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>

			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">�</button>
					<h4>
						<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="heading.instructor" /></li>
	</ul>
			
			<div class="block full" >
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.Instructorinfo" /></strong>
						</h2>
					</div>
					<form action="<%=contexturl %>ManageInstructor/InstructorList/UpdateInstructor" method="post" class="form-horizontal "
						id="Instructor_form" enctype="multipart/form-data">
							<input name="instructorId" value="${instructor.userId}" type="hidden" >
							<input name="editProfile" value="true" type="hidden" >
							<input name="loginId" value="${instructor.user.loginId}" type="hidden" >
							
					<div class="form-group">
						<label class="col-md-4 control-label" for="first_Name"><spring:message
								code="label.firstname" /><span class="text-danger">*</span> </label>
						<div class="col-md-6">

							<input id="first_Name" name="firstName" class="form-control"
								placeholder="<spring:message code='label.firstname'/>.."
								type="text" value="${instructor.user.firstName}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="last_Name"><spring:message
								code="label.lastname" /><span class="text-danger">*</span> </label>
						<div class="col-md-6">

							<input id="last_Name" name="lastName" class="form-control"
								placeholder="<spring:message code="label.lastname"/>.."
								type="text" value="${instructor.user.lastName}">
						</div>
					</div>
					
					<div class="form-group img-list1" id="profilepic">
						<label class="col-md-4 control-label" for="offer_validtill"><spring:message code="label.profilepic" /></label>
						<div class="col-md-6 img-list2" id="imagediv">
						<c:choose >
					      <c:when test="${! empty instructor.profileImage}">
							<img border="0"
								src="<%=contexturl%>${instructor.profileImage.imagePath}"
								width="120" height="120" title="Click here to change photo"
								onclick="OpenFileDialogInstructor();" class="img-circle pic" id="profileimage" style="cursor: pointer;" />
								
								<span class="text-content" onclick="OpenFileDialogInstructor();" title="Click here to change photo"><br>
								<br><span>Upload Profile pic</span></span>
						  </c:when>
						  <c:otherwise>
						  <img border="0"
								src="<%=contexturl%>work/Catalina/localhost/SkillMe/default_profile.png"
								width="120" height="120" title="Click here to change photo"
								onclick="OpenFileDialogInstructor();" class="img-circle pic" id="profileimage" style="cursor: pointer;" />
								
								<span class="text-content" onclick="OpenFileDialogInstructor();" title="Click here to change photo"><br>
								<br><span>Change Profile pic</span></span>
						  </c:otherwise>
						  </c:choose>
						</div>
						<input type="file" name="imageChooserProfile" id="imageChooserProfile" style="display: none;"
							name="imageChooserProfile"
							accept="image/jpg,image/jpeg,image/png,image/gif,image/bmp,image/tiff" />

					</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label" for="last_Name"><spring:message
								code="label.department" /><span class="text-danger">*</span> </label>
						<div class="col-md-6">

							<input id="department" name="department" class="form-control"
								placeholder="<spring:message code="label.department"/>.."
								type="text" value="${instructor.department.departmentName}">
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label" for="last_Name"><spring:message
								code="label.designation" /><span class="text-danger"></span> </label>
						<div class="col-md-6">

							<input id="designation" name="designation" class="form-control"
								placeholder="<spring:message code="label.designation"/>.."
								type="text" value="${instructor.designation}">
						</div>
					</div>

					<div class="form-group">
									<label class="col-md-4 control-label" for="qualification"><spring:message
											code="label.qualification" /><span class="text-danger"></span></label>
									<div class="col-md-4">
										<textarea id="qualification" name="qualification" class="form-control"
											rows="3" placeholder="<spring:message code="label.qualification"/>.."  >${instructor.qualification}</textarea>
									</div>
					</div>
							
					<div class="form-group">
									<label class="col-md-4 control-label" for="aboutinstructor"><spring:message
											code="label.aboutinstructor" /><span class="text-danger"></span></label>
									<div class="col-md-4">
										<textarea id="aboutinstructor" name="aboutinstructor" class="form-control"
											rows="3" placeholder="<spring:message code="label.aboutinstructor"/>.."  >${instructor.aboutInstructor}</textarea>
									</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="email"><spring:message
								code="label.emailid" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<input id="email" name="emailId" class="form-control"
								placeholder="test@example.com" type="text"
								value="${instructor.user.emailId}">
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label" for="website"><spring:message
								code="label.website" /><span class="text-danger"></span> </label>
						<div class="col-md-6">

							<input id="website" name="website" class="form-control"
								placeholder="<spring:message code="label.website"/>.."
								type="text" value="${instructor.website}">
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label" for="facebook"><spring:message
								code="label.facebook" /><span class="text-danger"></span> </label>
						<div class="col-md-6">

							<input id="facebook" name="facebook" class="form-control"
								placeholder="<spring:message code="label.facebooklink"/>.."
								type="text" value="${instructor.facebookLink}">
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label" for="twitter"><spring:message
								code="label.twitter" /><span class="text-danger"></span> </label>
						<div class="col-md-6">

							<input id="twitter" name="twitter" class="form-control"
								placeholder="<spring:message code="label.twitterlink"/>.."
								type="text" value="${instructor.twitterLink}">
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label" for="linkedIn"><spring:message
								code="label.linkedIn" /><span class="text-danger"></span> </label>
						<div class="col-md-6">

							<input id="linkedIn" name="linkedIn" class="form-control"
								placeholder="<spring:message code="label.linkedInLink"/>.."
								type="text" value="${instructor.linkedinLink}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="mobile"><spring:message
								code="label.mobile" /><span class="text-danger"></span></label>
						<div class="col-md-6">
							<input id="mobile" name="mobile" class="form-control"
								placeholder='<spring:message code="label.mobile"></spring:message>'
								type="text" value="${instructor.user.mobile}">
						</div>
					</div>
					<div class="form-group">
                        <label class="col-md-4 control-label"><spring:message code="label.userlogin"/></label>
                        <div class="col-md-8">
                            <p class="form-control-static">${instructor.user.loginId}</p>
                        </div>
                    </div>
                    
					<div class="form-group">
						<a href="#reset-user-settings" data-toggle="modal"
							class="col-md-4 control-label"><spring:message code="label.resetpassword"/></a>
					</div>


			<div class="form-group">
			</div>
			
		
			<div class="form-group">
				<div style="text-align: center; margin-bottom: 10px">
					<button id="user_submit" class="btn btn-sm btn-primary save"
						type="submit">
						<i class="fa fa-angle-right"></i>
						<spring:message code="button.save" />
					</button>
					<a href="<%=contexturl%>/ManageInstructor"
						class="btn btn-sm btn-primary" style="text-decoration: none;"><i
						class="gi gi-remove"></i> <spring:message code="button.cancel" /></a>
				</div>
			</div>
			
		</form>
</div>

	

	<div id="reset-user-settings" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header text-center">
					<h2 class="modal-title">
						<i class="fa fa-pencil"></i> <spring:message code="label.resetpassword"/>
					</h2>
				</div>
				<span id="passwordErrorMsg"></span>
				<div class="modal-body">
					<form action="<%=contexturl %>ManageInstructor/InstructorList/ResetInstructorPassword" method="post"
						class="form-horizontal form-bordered" id="ResetPassword_Form">
						<input name="userId" value="${instructor.user.userId}" type="hidden">
						
						<fieldset>
							<div class="form-group">
								<label class="col-md-4 control-label"
									for="user-settings-password"><spring:message code="label.newpassword"/></label>
								<div class="col-md-8">
									<input type="password" id="password" name="password"
										class="form-control"
										placeholder="Please choose a complex one..">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label"
									for="user-settings-repassword"><spring:message code="label.confirmnewpassword"/></label>
								<div class="col-md-8">
									<input type="password" id="user-settings-repassword"
										name="confirmPassword" class="form-control"
										placeholder="..and confirm it!">
								</div>
							</div>
						</fieldset>
						<div class="form-group form-actions">
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal"><spring:message code="label.close"/></button>
								<button id="submit_password" class="btn btn-sm btn-primary save"
									type="submit"><spring:message code="label.savechanges"/></button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<script src="<%=contexturl %>resources/js/vendor/jquery-ui.js"></script>
<script src="<%=contexturl%>resources/js/pages/activeTablesDatatables.js"></script>
<script src="<%=contexturl%>resources/js/pages/inactiveTablesDatatables.js"></script>
<script>$(function(){ ActiveTablesDatatables.init(3); });</script>
<script>$(function(){ InActiveTablesDatatables.init(3); });</script>
<script type="text/javascript">
$(document).ready(function() {
	
	$("#ResetPassword_Form")
	.validate(
			{
				errorClass : "help-block animation-slideDown",
				errorElement : "div",
				errorPlacement : function(e, a) {
					a.parents(".form-group > div")
							.append(e)
				},
				highlight : function(e) {
					$(e)
							.closest(".form-group")
							.removeClass(
									"has-success has-error")
							.addClass("has-error")
				},
				success : function(e) {
					e
							.closest(".form-group")
							.removeClass(
									"has-success has-error")
				},
				rules : {
					password : {
						required : !0,
						minlength : 6
					},
					confirmPassword : {
						required : !0,
						equalTo : "#password"
					}
				},
				messages : {
					password : {
						required : "Please enter a password",
						minlength : "Password must be atleast 6 charecter"
					},
					confirmPassword : {
						required : "Please confirm password",
						equalTo : "Please enter the same password as above"
					},
				}
			});
	
	$("#Instructor_form").validate(
			{	errorClass:"help-block animation-slideDown",
				errorElement:"div",
				errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
				highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
				success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
				rules:{ firstName : {required : !0,maxlength : 75},
					lastName : {required : !0,maxlength : 75},
					department : {required : !0,maxlength : 75},
					emailId : {required : !0,maxlength : 75,email : !0},
					loginId : {required : !0,maxlength : 75,minlength : 6},
					password : {required : !0,maxlength : 75,minlength : 6},
					confirmPassword : {required : !0,equalTo : "#password"}
				},
				messages:{
					firstName : {
						required :'<spring:message code="validation.pleaseenterfirstname"/>',
						maxlength :'<spring:message code="validation.firstname75character"/>'
					},
					lastName : {
						required :'<spring:message code="validation.pleaseenterlastname"/>',
						maxlength :'<spring:message code="validation.lastname75character"/>'
					},
					
					department : {
						required :'<spring:message code="validation.pleaseenterdepartment"/>',
						maxlength :'<spring:message code="validation.department75character"/>'
					},
					emailId : {
						required :'<spring:message code="validation.pleaseenteremailid"/>',
						email :'<spring:message code="validation.pleaseenteravalidemailid"/>',
						maxlength :'<spring:message code="validation.email75character"/>'
					},
					loginId : {
						required :'<spring:message code="validation.pleaseenteraloginid"/>',
						minlength :'<spring:message code="validation.loginidmustbeatleast6character"/>',
						maxlength :'<spring:message code="validation.loginid75character"/>'
					},
					password : {
						required :'<spring:message code="validation.pleaseenterapassword"/>',
						minlength :'<spring:message code="validation.passwordmustbeatleast6character"/>',
						maxlength :'<spring:message code="validation.passworddname75character"/>'
					},
					confirmPassword : {
						required :'<spring:message code="validation.pleaseconfirmpassword"/>',
						equalTo :'<spring:message code="validation.pleaseenterthesamepasswordasabove"/>'
					}
				},
			});

	 $("#user_submit").click(function(){
		 $("#Instructor_form").submit();
	 });
	 

});

$("#view_course").click(function(){
	 $("#courseList").show();
})
$("#hide_course").click(function(){
	 $("#courseList").hide();
})


</script>
<script type="text/javascript">
	function OpenFileDialogInstructor(){
		document.getElementById("imageChooserProfile").click();
	}
	
	function changePhoto(){
		if($('#imageChooserProfile').val() != ""){			
			var imageName = document.getElementById("imageChooserProfile").value;
			var ext1 = imageName.substring(imageName.lastIndexOf("."));
			var ext = ext1.toLowerCase();
			if(ext == '.jpg' || ext == '.jpeg' || ext == '.png' || ext == 'gif' || ext == 'bmp' || ext == 'tiff')
				$('#changePhoto').submit();		
			else
				alert("Please select only image file");		
		}
	}
</script>

<script>
$(function() {
	
	var data = [
				<c:forEach var="department" items="${activeList}" varStatus="departmentCount">
					"${department.departmentName}" ${not departmentName.last ? "," : ""}
				</c:forEach>
			];

	$("#department").autocomplete({
	source:data
	});
});
</script>

<script type="text/javascript">
    function showimages(files) {
        for (var i = 0, f; f = files[i]; i++) {
           
                       
            var reader = new FileReader();
            reader.onload = function (evt) {
            	$( "#image" ).remove();
            	$( "#imagediv" ).remove();
            	
//                 var img = '<div class="col-md-6 id="imagediv"><img border="0" src="' + evt.target.result + '" width="120" height="120" title="Click here to change photo" style="cursor: pointer;"  onclick="OpenFileDialogInstructor();" class="img-circle pic" id="image"/></div>';
                  var img ='<div class="col-md-6 img-list2" id="imagediv"><img border="0" src="' + evt.target.result + '" width="120" height="120" title="Click here to change photo" onclick="OpenFileDialogInstructor();" class="img-circle pic" id="image" style="cursor: pointer;" /><span class="text-content" onclick="OpenFileDialogInstructor();" title="Click here to change photo"><br><br><span>Change Profile pic</span></span></div>'
                 $('#profilepic').append(img);
            }
            reader.readAsDataURL(f);
        }
    }

    $('#imageChooserProfile').change(function (evt) {
        showimages(evt.target.files);
    });
</script>
<%@include file="../../inc/template_end.jsp"%>