<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->

<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>

<link rel="stylesheet" type="text/css" href="../resources/css/jquery.cleditor.css" />
<style>
.cleditorToolbar {
	background: url('../resources/img/cleditor/toolbar.gif') repeat
}

.cleditorButton {
	float: left;
	width: 24px;
	height: 24px;
	margin: 1px 0 1px 0;
	background: url('../resources/img/cleditor/buttons.gif')
}

.cleditorMain {
	height: 250px !important
}

.rest_photo {
    width: 170px;
    text-align: left;
    font: 11px 'Calibri';
    color: #666;
    border: 10px solid #E6E6E6;;
    float: left;
    margin: 5px;
}

#delete{
	float: right;
	margin-top: -8px;
	margin-left: 142px;
	padding-left: 4px;
	padding-right: 2px;
	position: absolute;
	z-index: 100000;
}
</style>

<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				<i class="fa fa-map-marker"></i>
				<spring:message code="heading.meal" />
				<br> <small><spring:message code="heading.editmeals" />
				</small>
			</h1>
			<span id="errorMsg"></span>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i> <spring:message code="label.success"/>
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="heading.meal" />
		</li>
	</ul>
	<div class="block full" id="formView">
		<form action="UpdateMealMenu" method="post" class="form-horizontal"
			enctype="multipart/form-data" id="MealMenu_form">
			<div class="form-group">
				<input id="mealMenu_Id" name="mealMenuId" type="hidden"
					value="${mealMenu.mealMenuId}"> <input id="outlet_Id"
					name="outletId" type="hidden" value="${outletId}"> <label
					class="col-md-3 control-label" for="mealMenu_Name"><spring:message
						code="label.title" /><span class="text-danger">*</label>
				<div class="col-md-9">
					<input id="mealMenu_Name" name="title" class="form-control"
						value="${mealMenu.title}"
						placeholder="<spring:message code="label.title"/>.."
						type="text">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="mealMenu_description"><spring:message
						code="label.description" /><span class="text-danger">*</label>
				<div class="col-md-9">
					<textarea id="mealMenu_description" name="description"
						class="form-control" rows="5"
						placeholder="<spring:message code="label.description"/>..">${mealMenu.description}</textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="mealmenu_description"><spring:message
						code="label.cost" /><span class="text-danger">*</label>
				<div class="col-md-9">
					<input type="text" name="cost" value="${mealMenu.cost}"/>
				</div>
			</div>
			
			<c:if test="${image!=null}">
				<div class="form-group">
					<label class="col-md-3 control-label" for="mealMenu_validtill"></label>
					<div class="col-md-9">
						<span id="delete" ><a href="#delete_mealMenuImg_popup" data-toggle="modal"
							onclick="deleteMealMenuImg(${image.imageId})" title="Delete"class="btn btn-xs btn-danger">
							<i class="fa fa-times"></i></a>
						</span>
						<img border="0" src="/Gourmet7${image.imagePath}" width="150"
							height="110">
					</div>
				</div>
			</c:if>
			<div class="form-group">
				<c:if test="${image!=null}">
					<label class="col-md-3 control-label" for="mealMenu_image"><spring:message code="label.changepicture"/>
						</label>
				</c:if>
				<c:if test="${image==null}">
					<label class="col-md-3 control-label" for="mealMenu_image"><spring:message code="label.addpicture"/>
						</label>
				</c:if>
				<div class="col-md-9">
					<input type="file" name="photo" id="profile"
						accept="image/jpg,image/jpeg,image/png,image/gif,image/bmp,image/tiff"
						style="padding-top: 7px;" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="mealMenu_termsCondition"><spring:message
						code="label.termsandcondition" /><span class="text-danger">* </label>
				<div class="col-md-9">
					<textarea id="mealMenu_termsCondition" name="termsandcondition">${mealMenu.termsandcondition}</textarea>
				</div>
			</div>
			<div class="form-group form-actions">
				<div class="col-md-9 col-md-offset-3">
					<button id="mealMenu_submit" type="submit"
						class="btn btn-sm btn-primary">
						<i class="fa fa-angle-right"></i> <spring:message code="button.save" />
					</button>
						<a  class="btn btn-sm btn-primary" href="MealMenuList?outletId=${outletId}"><i class="gi gi-remove"></i>
							<spring:message code="button.cancel" /> </a>
				</div>
			</div>
		</form>
	</div>
</div>
<div id="delete_mealMenuImg_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="DeleteMealMenuImage" method="post" class="form-horizontal form-bordered"
						id="delete_mealMenuImg_Form">
						<div style="padding: 10px; height: 110px;">
							<label><spring:message code="label.doyouwanttodeletetheimage"/></label>
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal"><spring:message code="label.no"/></button>
								<div id="delete_mealMenuImgYes" class="btn btn-sm btn-primary"><spring:message code="label.yes"/></div>
								<input type="hidden" name="mealMenuImgId" id="mealMenuImgId">
								<input type="hidden" name="outletId" id="outletId">
								<input type="hidden" name="mealMenuId" id="mealMenuId">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>

<script type="text/javascript" src="../resources/js/jquery.cleditor.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		
		$("#mealMenu_termsCondition").cleditor(); 
		
		$(".input-datepicker, .input-daterange").datepicker({weekStart : 1});
		
		$(".input-timepicker24").timepicker({minuteStep:1,showSeconds:!0,showMeridian:!1});

		$("#MealMenu_form").validate(
		{	errorClass:"help-block animation-slideDown",
			errorElement:"div",
			errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
			highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
			success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
			rules:{ title:{required:!0},
					description:{required:!0},
					termsandcondition:{required:!0}
			},
			messages:{title:{required:'<spring:message code="validation.title"/>'},
				description:{required:'<spring:message code="validation.description"/>'},
				termsandcondition:{required:'<spring:message code="validation.termsandconditions"/>'}
			},
		});
						
		$("#mealMenu_submit").click(function(){
			 $("#MealMenu_form").submit();
		});
		
		$("#delete_mealMenuImgYes").click(function(){
			$("#mealMenuImgId").val(selectedId);
			$("#outletId").val(${outletId});
			$("#mealMenuId").val(${mealMenu.mealMenuId});
			$("#delete_mealMenuImg_Form").submit();
		});
		
	});
	
	function deleteMealMenuImg(id){
		selectedId = id;
	}
</script>
<%@include file="../../inc/template_end.jsp"%>