<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>


<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>

			<h1>
<!-- 				<i class="fa fa-map-marker"></i> -->
				<spring:message code="heading.modulemaster" />
				<br> <small><spring:message
						code="heading.modulenewmodule" /></small>
			</h1>
			<span id="errorMsg"></span>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="heading.managemodule" /></li>
		<li><a href="#"><spring:message code="label.module" /></a></li>
		<li><spring:message code="label.addmodule"/></li>
	</ul>
	<form action="<%=contexturl %>ManageModule/ModuleList/SaveModule" method="post" class="form-horizontal ui-formwizard"
		id="Module_form" enctype="multipart/form-data">
		<div class="row">

			<div class="">
				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.moduleinfo" /></strong>
						</h2>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="module_Name"><spring:message
								code="label.modulename" /> <span class="text-danger">*</span>
						</label>
						<div class="col-md-6">

							<input id="module_Name" name="moduleName" class="form-control"
								placeholder="<spring:message code='label.modulename'/>.."
								type="text" value="${module.moduleName}">
						</div>
					</div>
					
					<div class="form-group">
           				<label class="col-md-4 control-label" for="moduleDescription"><spring:message code="label.decsription"/></label>
          					 <div class="col-md-6">
          					 <textarea id="moduleDescription" name="moduleDescription" 
           							class="form-control" 
          							 placeholder="<spring:message code='label.description'/>..">${module.moduleDescription}</textarea>
          					 </div>
           			</div>
           			
           			
           			
           			
           			<div class="form-group row" id="profilepic">
						<label class="col-md-4 control-label" for="offer_validtill">
							Module Image <span class="text-danger">*</span>
						</label>
						<div class="col-md-8">
							<div class=" img-list2" id="imagediv">
<!-- 						 class="img-circle pic"  -->
								<img border="0"
									src="<%=contexturl%>resources/img/application/default_profile.png"
									width="120" height="120" title="Click here to change photo"
									onclick="OpenFileDialog();"id="profileimage" style="cursor: pointer;" />
									
									<span class="text-content" onclick="OpenFileDialog();" title="Click here to change photo"><br>
									<br><span>Upload Module Image</span></span>
							</div>
							<div>
								<input type="file" name="imageChooser1" id="imageChooser1" style="visibility: hidden;height: 0px"	/>
							</div>
						</div>
						
					</div>
           			
				</div>		
			</div>		
		</div>
		

         



	<div style="text-align: center; margin-bottom: 10px">
		<div class="form-group form-actions">
<!-- 				<div class="col-md-9 col-md-offset-3"> -->
					<button id="module_submit" type="submit"  value="Validate!"
						class="btn btn-sm btn-primary save">
						<i class="fa fa-angle-right"></i> <spring:message code="button.save" />
					</button>
<!-- 					<button type="reset" class="btn btn-sm btn-warning"> -->
<!-- 						<i class="fa fa-repeat"></i> -->
<%-- 						<spring:message code="button.reset" /> --%>
<!-- 					</button> -->
						<a class="btn btn-sm btn-primary" href="<%=contexturl %>ManageModule/ModuleList"></i>
							<spring:message code="button.cancel" /> </a>
<!-- 				</div> -->
			</div>
		</div>
	</form>
</div>
<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<%@include file="../../inc/chosen_scripts.jsp"%>
<script src="<%=contexturl %>resources/js/custom/featurecontrol.js"></script>
<script src="<%=contexturl %>resources/js/additional-methods.js"></script>
<!--

//-->
</script>
<script type="text/javascript"
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCheQF_yjKacwuVL4_59EO-3BThy85gbu4&sensor=false&region=GB">
	
</script>

<script type="text/javascript">
	$(document)
			.ready(
				function() {
					$("#manageModule").addClass("active");

						$("#Module_form").validate(
						{	errorClass:"help-block animation-slideDown",
							errorElement:"div",
							errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
							highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
							success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
										rules : {
											moduleName : {
												required : !0,maxlength : 75
											},
											moduleDescription : {
												 maxlength: 500
											},
											
											imageChooser1: {
											     required: true,
											     extension: "jpg|jpeg|png|gif",
											     maxlength: 1000
											   }
											
										},
										messages : {
											moduleName : {
												required :'<spring:message code="validation.pleaseentermoduleName"/>',
												maxlength :'<spring:message code="validation.institutionName75character"/>'
											},
										moduleDescription : {
											
											maxlength :'<spring:message code="validation.decription500character"/>'
											
											},
											imageChooser1 : {
							                      required : 'Please select image',
							                      extension : 'Please select image of (.jpg,.jpeg,.png,.gif,.bmp,.tiff) extension.',
							                   	 maxlength :'The Image is too long. Please select another image'
							           		}
												
											
										}
								});

						$("#module_submit").click(function() {
							unsaved=false;
							$("#Module_form").submit();
						});
						
				});
						
</script>

<script type="text/javascript">
	function OpenFileDialog(){
		document.getElementById("imageChooser1").click();
	}
	
</script>

<script type="text/javascript">
    function showimages(files) {
        for (var i = 0, f; f = files[i]; i++) {
           
                       
            var reader = new FileReader();
            reader.onload = function (evt) {
            	$( "#image" ).remove();
            	$( "#imagediv" ).html("");
            	
//                 var img = '<div class="col-md-6 id="imagediv"><img border="0" src="' + evt.target.result + '" width="120" height="120" title="Click here to change photo" style="cursor: pointer;"  onclick="OpenFileDialog();" class="img-circle pic" id="image"/></div>';
                  var img ='<img border="0" src="' + evt.target.result + '" width="120" height="120" title="Click here to change photo" onclick="OpenFileDialog();"  id="image" style="cursor: pointer;" /><span class="text-content" onclick="OpenFileDialog();" title="Click here to change photo"><br><br><span>Change Module Image</span></span>'
                 $('#imagediv').html(img);
            }
            reader.readAsDataURL(f);
        }
    }

    $('#imageChooser1').change(function (evt) {
        showimages(evt.target.files);
    });
</script>
<style>
.chosen-container {
	width: 250px !important;
}
#map-canvas {
	height: 300px
}
</style>
<%@include file="../../inc/template_end.jsp"%>