
<style>
.wizard-steps span {
	width: 230px !important;
}

.chosen-container {
	width: 250px !important;
}
</style>

<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i><spring:message code="label.error"/>
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>
			<h1>
				<i class="gi gi-cup"></i>
				<spring:message code="heading.membership" />
				<br> <small><spring:message
						code="heading.membershipdetails" /></small>
			</h1>
			<span id="errorMsg"></span>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12" >
			<div class="block" style="height:85px; padding-top: 10px;">
				<div class="form-group">
					<div class="col-md-4">
						<label for="countryCount">Country Count:</label>
						<label class="control-label" name="countryCount" id="countryCount"></label>
					</div>
					<div class="col-md-4">
						 <label for="cityCount">City Count:</label>
						<label class="control-label" name="cityCount" id="cityCount"></label>
					</div>
					<div class="col-md-4">
						 <label for="brandCount">Brand Count:</label>
						<label class="control-label" name="brandCount" id="brandCount"></label>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-4">
						<label for="countryTotal">Country Total Amount:</label>
						<label for="countryTotal">${currency.currencySign}</label>
						<label class="control-label" name="countryTotal" id="countryTotal"></label>
					</div>
					<div class="col-md-4">
						 <label for="cityTotal">City Total Amount:</label>
						 <label for="cityTotal">${currency.currencySign}</label>
						<label class="control-label" name="cityTotal" id="cityTotal"></label>
					</div>
					<div class="col-md-4">
						 <label for="brandTotal">Brand Total Amount:</label>
						 <label for="brandTotal">${currency.currencySign}</label>
						<label class="control-label" name="brandTotal" id="brandTotal"></label>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-4">
						<label for="countryDiscount">Country Discount Amount:</label>
						<label for="countryDiscount">${currency.currencySign}</label>
						<label class="control-label" name="countryDiscount" id="countryDiscount"></label>
					</div>
					<div class="col-md-4">
						 <label for="cityDiscount">City Discount Amount:</label>
						 <label for="cityDiscount">${currency.currencySign}</label>
						<label class="control-label" name="cityDiscount" id="cityDiscount"></label>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-4">
						 <label for="total">Total Amount:</label>
						 <label for="total">${currency.currencySign}</label>
						<label class="control-label" name="total" id="total"></label>
					</div>
				</div>
	    	</div>
    	</div>
    </div>
	<form action="SaveMembership" method="post" class="form-horizontal ui-formwizard" id="Membership_form" >
		<div style="display: block;" id="advanced-first"
			class="step ui-formwizard-content">
			<div class="wizard-steps">
				<div class="row">
					<div class="col-xs-4 active">
						<span><spring:message code="heading.locationdetail" /></span>
					</div>
					<div class="col-xs-4">
						<span><spring:message code="heading.branddetail" /></span>
					</div>
					<div class="col-xs-4">
						<span><spring:message code="heading.tenuredetail" /></span>
					</div>
				</div>
			</div>
			<div class="row">

				<div class="col-md-12">
					<div class="block">
						<div class="block-title">
							<h2>
								<strong><spring:message code="heading.locationdetail" /></strong>
							</h2>
						</div>
						<input id="countryIds" name="countryIds" type="hidden" value="">
						<input id="cityIds" name="cityIds" type="hidden" value="">
						<input id="brandIds" name="brandIds" type="hidden" value="">
						<input id="totalAmt" name="totalAmt" type="hidden" value="">
						<c:forEach var="country" items="${countryList}" varStatus="count">
							<div class="panel panel-default">
	                            <div class="panel-heading">
	                            	<div class="row ">
										<div class="col-xs-2">
										
			                            	 <label class="switch switch-primary"> 
			                            	 	<input value="<c:choose>
																<c:when test="${visitor.role.id eq CORPORATE}">${country.monthlyCorporateMembershipRate}</c:when>
																<c:otherwise>${country.monthlyIndividualMembershipRate}</c:otherwise>
															</c:choose>"
														name="country" type="checkbox" id="country_${country.country.countryId}" onclick="selectAllCities(${country.country.countryId});" class="countryRate"/>
														<span></span> </label>
										</div>
										<div class="col-xs-4">
			                                <h4 class="panel-title"><i class="fa fa-angle-right"></i> <a class="accordion-toggle" data-toggle="collapse" 
			                                    data-parent="#accordion" href="#accordion_${country.id}">
			                                    ${country.country.countryName} </a></h4>
			                             </div>
										<div class="col-xs-2">
											 <p style="word-spacing:15px;">
											 	${currency.currencySign}  
											 	<c:choose>
													<c:when test="${visitor.role.id eq CORPORATE}">${country.monthlyCorporateMembershipRate}</c:when>
													<c:otherwise>${country.monthlyIndividualMembershipRate}</c:otherwise>
												</c:choose>
											</p>	
										</div>
			                        </div>
	                            </div>
	                            <div id="accordion_${country.id}" class="panel-collapse collapse in" style="height: auto;">
		                            <c:forEach var="city" items="${country.cityRates}" varStatus="count2">
		                                <div class="panel-body">
		                                	<div class="row ">
												<div class="col-xs-2">
		                                			<label class="switch switch-primary ${country.id}"> <input value="<c:choose>
																														<c:when test="${visitor.role.id eq CORPORATE}">${city.monthlyCorporateMembershipRate}</c:when>
																														<c:otherwise>${city.monthlyIndividualMembershipRate}</c:otherwise>
																													</c:choose>" 
														id="city_${city.city.cityId}_${country.country.countryId}" name="city" type="checkbox" class="country_${country.country.countryId} cityRate" />
														<span></span> </label>
												</div>
												<div class="col-xs-4">
													<p>${city.city.cityName}</p>
												</div>
												<div class="col-xs-2">     
													<p style="word-spacing:15px;">${currency.currencySign}  
															<c:choose>
																<c:when test="${visitor.role.id eq CORPORATE}">${city.monthlyCorporateMembershipRate}</c:when>
																<c:otherwise>${city.monthlyIndividualMembershipRate}</c:otherwise>
															</c:choose></p>
												</div>
											</div>
		                                </div>
		                            </c:forEach>
	                            </div>
	                        </div>
	                      </c:forEach>
					</div>
				</div>

			</div>
		</div>

		<div style="display: none;" id="advanced-second"
			class="step ui-formwizard-content">
			<div class="wizard-steps">
				<div class="row">
					<div class="col-xs-4 done">
						<span><spring:message code="heading.locationdetail" /></span>
					</div>
					<div class="col-xs-4 active">
						<span><spring:message code="heading.branddetail" /></span>
					</div>
					<div class="col-xs-4">
						<span><spring:message code="heading.tenuredetail" /></span>
					</div>
				</div>
			</div>
			<div class="block">
				<div class="block-title">
					<h2>
						<strong><spring:message code="heading.branddetail" /></strong>
					</h2>
				</div>
				<c:forEach var="brand" items="${brandList}" varStatus="count">
				
					<div class="panel panel-default">
                          <div class="panel-heading">
	                          	<div class="row ">
									<div class="col-xs-2">
		                            	 <label class="switch switch-primary"> <input value="<c:choose>
																								<c:when test="${visitor.role.id eq CORPORATE}">${brand.monthlyCorporateMembershipRate}</c:when>
																								<c:otherwise>${brand.monthlyIndividualMembershipRate}</c:otherwise>
																							</c:choose>"
								id="brand_${brand.id}" name="brand" type="checkbox" class="brandRate"/>
								<span></span> </label>
									</div>
									<div class="col-xs-4">
		                                <h4 class="panel-title"><i class="fa fa-angle-right"></i> <a class="accordion-toggle" data-toggle="collapse" 
		                                    data-parent="#brandAccordion" href="#brandAccordion_${brand.brand.brandId}">
		                                   ${brand.brand.brandName}  </a></h4>
		                             </div>
									<div class="col-xs-2">
										 <p style="word-spacing:15px;">${currency.currencySign}  
											<c:choose>
												<c:when test="${visitor.role.id eq CORPORATE}">${brand.monthlyCorporateMembershipRate}</c:when>
												<c:otherwise>${brand.monthlyIndividualMembershipRate}</c:otherwise>
											</c:choose>
										</p>
									</div>
		                        </div>
	                       </div>
	                       <div id="brandAccordion_${brand.brand.brandId}" class="panel-collapse collapse in" style="height: auto;">
	                           <c:forEach var="restaurant" items="${brand.restaurants}" varStatus="count3">
	                               <div class="panel-body">
	                               	<div class="row ">
										<div class="col-xs-3">
	                               			<label class="switch switch-primary"><strong>${restaurant.name}</strong> </label>
										</div>
										<div class="col-xs-3">
											<p>${restaurant.city.cityName}</p>
										</div>
									</div>
	                               </div>
	                           </c:forEach>
                          </div>
                      </div>
				
				
				
	            </c:forEach>
			</div>
		</div>

		<div style="display: none;" id="advanced-third"
			class="step ui-formwizard-content">
			<div class="wizard-steps">
				<div class="row">
					<div class="col-xs-4 done">
						<span><spring:message code="heading.locationdetail" /></span>
					</div>
					<div class="col-xs-4 done">
						<span><spring:message code="heading.branddetail" /></span>
					</div>
					<div class="col-xs-4 active">
						<span><spring:message code="heading.tenuredetail" /></span>
					</div>
				</div>
			</div>
			<div class="block">
				<div class="block-title">
					<h2>
						<strong><spring:message code="heading.tenuredetail" /></strong>
					</h2>
				</div>
				<c:choose>
					<c:when test="${visitor.role.id eq CORPORATE}">
						<div class="form-group">
							<label class="col-md-3 control-label" for="no_of_users"><spring:message
									code="label.noofusers" /><span class="text-danger">*</span> </label>
							<div class="col-md-4">
								<input id="no_of_users" name="noofusers" class="form-control"
									placeholder="<spring:message code="label.noofusers"/>.."
									type="text">
							</div>
						</div>
					</c:when>
					<c:otherwise>
					<div class="form-group">
						<input id="no_of_users" type="hidden" name="noofusers" value="1"/>
						</div>
					</c:otherwise>
				</c:choose>
				<div class="form-group" id="tenure_formgroup">
					<label class="col-md-3 control-label" for="tenure">Tenure in Months
					<span class="text-danger">*</span> </label>
					<div class="col-md-3">
						<select class="tenure_chosen" style="width: 200px;"
							id="tenure" name="tenure">
							<option value="" >Select Tenure</option>
							<option value="1" >1</option>
							<option value="2" >2</option>
							<option value="3" >3</option>
							<option value="4" >4</option>
							<option value="5" >5</option>
							<option value="6" >6</option>
							<option value="7" >7</option>
							<option value="8" >8</option>
							<option value="9" >9</option>
							<option value="10" >10</option>
							<option value="11" >11</option>
							<option value="12" >12</option>
						</select>
					</div>
					<div class="col-md-3" id="tenure_error" style="display: none;margin-top: 5px;color: #e74c3c;">
						<spring:message code="validation.tenure"/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-md-3 control-label" for="discountAmount">
							Tenure Discount</label>
					<div class="col-md-4">
							${currency.currencySign} <label class="control-label" style="font-weight: normal;" id="tenureDiscount"></label>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-md-3 control-label" for="discount_Code"><spring:message
							code="label.discountcode" /></label>
					<div class="col-md-4">
						<input id="discount_Code" name="code" class="form-control"
							placeholder="<spring:message code="label.discountcode"/>.."
							type="text" style="width: 90%;float: left">
							<a  id="removeCode" href="#" data-toggle="modal" style="margin: 5px"
										 title="Remove Code" 
										class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
						<span class="label label-success" id="availability"
							style="cursor: pointer;"><i class="fa fa-check"></i>Apply Discount Code
						</span>
						<span class="text-success" id="available"
							style="display: none"><spring:message code="label.couponvalid"/>
						</span> 
						<span class="text-danger" id="notavailable" style="display: none">
                           <c:choose>
								<c:when test="${visitor.role.id eq CORPORATE}">
									<spring:message code="label.corporatecouponinvalid" />
								</c:when>
								<c:otherwise>
									<spring:message code="label.couponinvalid" />
								</c:otherwise>
							</c:choose>
						</span>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="discountAmount">
							Discount Amount</label>
					<div class="col-md-4">
							${currency.currencySign} <label class="control-label" style="font-weight: normal;" id="discountAmount"></label>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="totalAmountPaid">
						<spring:message	code="label.netamount" /></label>
					<div class="col-md-4">
							${currency.currencySign} <label class="control-label" id="totalAmountPaid" style="font-weight: normal;"></label>
					</div>
				</div>
			</div>
		</div>
		<div class="form-group form-actions">
			<div class="col-md-8 col-md-offset-4">
				<input disabled="disabled"
					class="btn btn-sm btn-warning ui-wizard-content ui-formwizard-button"
					id="back2" value="Back" type="reset">
					 <input
					class="btn btn-sm btn-primary ui-wizard-content ui-formwizard-button save"
					id="next2" value="Next" type="submit">
			</div>
		</div>
	</form>
	    
<script type="text/javascript">
var tenureDiscountObject = null;
var voucherAmount = 0;
$(document).ready(function() {
		$(".tenure_chosen").chosen();
		
		$("#removeCode").click(function(){
			$("#discount_Code").val("");
			$("#available").css("display", "none");
			$("#notavailable").css("display", "none");
			voucherAmount = 0;
			netAmountCalculation();
		});
		$("#next2").click(function(){
			
				$('html, body').animate({ scrollTop: $("#Membership_form").offset().top },500);
		 });
		$("#Membership_form").formwizard(
				{
					disableUIStyles : !0,
					validationEnabled : !0,
					validationOptions : {
						errorClass : "help-block animation-slideDown",
						errorElement : "div",
						errorPlacement : function(e, a) {
							a
									.parents(
											".form-group > div")
									.append(e)
						},
						highlight : function(e) {
							$(e)
									.closest(
											".form-group")
									.removeClass(
											"has-success has-error")
									.addClass(
											"has-error")
						},
						success : function(e) {
							e
									.closest(
											".form-group")
									.removeClass(
											"has-success has-error")
						},
						rules : {
							
							noofusers:{required:!0,maxlength :6,digits : !0},
							tenure:{required:!0}
						},
						messages : {
							noofusers:{required:'<spring:message code="validation.noofuser"/>',
								maxlength :'<spring:message code="validation.noofuser6character"/>',
								digits :'<spring:message code="validation.pleaseenterno"/>'},
								tenure:{required:'<spring:message code="validation.tenure"/>'}	
							
						}
					}
				});
		
		$('input[type=checkbox]').change(function() {
			calculateAmount();
		});
		
		$("#availability").click(
				function() {
					var data = $("#discount_Code").val();
					var noofusers = $("#no_of_users").val();
					$("#notavailable").css("display", "none")
					$("#available").css("display", "none")
					if (data != "") {
						$.getJSON("CheckDiscountCode", {
							code : data,
							noOfUsers : noofusers,
						}, function(data) {
							if (data == "invalid") {
								$("#notavailable").css("display", "inline");
								$('#discountAmount').text(0);
							} else {
								$("#available").css("display", "inline");
								voucherAmount = data;
								 <c:if test="${!empty rate}">voucherAmount= voucherAmount*${rate};</c:if> 
								netAmountCalculation();
							}
						});
					}
				});
		
		$("#tenure").change(function() {
			var data = $("#tenure").val();
			$.getJSON("GetTenureDiscount", {
				count : data,
			}, function(data) {
				if (data != null) {
					tenureDiscountObject = data;
					netAmountCalculation();
				} 
			});
		});
		
		$("#no_of_users").change(function(){ 
			netAmountCalculation();
		});
		
	});

function calculateAmount(){
	var total = 0;
	var countryCount = 0;
	var cityCount = 0;
	var countryTotal = 0;
	var cityTotal = 0;
	var countryDiscount = 0;
	var cityDiscount = 0;
	var brandCount = 0;
	var brandTotal = 0;
	var cityIds = "";
	var countryIds = "";
	var brandIds = "";
	var post_array =new Array();
	var country_array = new Array();
	$('.countryRate:checked').each(function(){
		 var str = this.getAttribute("id");
        var str1 = str.indexOf("_");
        var countryId = str.substring(str1+1);
		 countryCount = countryCount + 1; 
   	 countryTotal = countryTotal + parseFloat($(this).val(), 10);
   	 countryIds = countryIds + countryId + ',';
   	 var countryDiscountPercent = 0;
   	 <c:forEach var="countryDiscount" items="${countryDiscountList}" varStatus="count4">
       	 var minRangeValue = ${countryDiscount.minRangeValue};
       	 var maxRangeValue = ${countryDiscount.maxRangeValue};
       	 if(countryCount >= minRangeValue && countryCount <= maxRangeValue){
       		 countryDiscountPercent = ${countryDiscount.percentDiscount};
       		 countryDiscount = (countryDiscountPercent * countryTotal)/100;
       	 }
   	 </c:forEach>
   	 country_array.push(countryId);
	});
	
	$('.cityRate:checked').each(function(){
		var str = this.getAttribute("id");
        var str1 = str.indexOf("_");
        var str2 = str.lastIndexOf("_");
        var cityId= str.substring(str1+1,str2);
        var countryId = str.substring(str2+1);
        var citySelected = false;
        for(var i=0;i<country_array.length;i++){
       	 if(countryId == country_array[i]){
       		 citySelected = true;
       	 }
        }
        if(citySelected == false){
       	 cityCount = cityCount + 1; 
       	 cityTotal = cityTotal + parseFloat($(this).val(), 10);
       	 cityIds = cityIds + cityId + ',';
       	 var cityDiscountPercent = 0;
       	 <c:forEach var="cityDiscount" items="${cityDiscountList}" varStatus="count3">
	        	 var minRangeValue = ${cityDiscount.minRangeValue};
	        	 var maxRangeValue = ${cityDiscount.maxRangeValue};
	        	 if(cityCount >= minRangeValue && cityCount <= maxRangeValue){
	        		 cityDiscountPercent = ${cityDiscount.percentDiscount};
	        		 cityDiscount = (cityDiscountPercent * cityTotal)/100;
	        	 }
       	 </c:forEach>
        }
		 
	})
	
	$('.brandRate:checked').each(function(){
		var str = this.getAttribute("id");
        var str1 = str.indexOf("_");
        var brandId = str.substring(str1+1);
		 brandCount = brandCount + 1; 
    	 brandTotal = brandTotal + parseFloat($(this).val(), 10);
    	 brandIds = brandIds + brandId + ',';
	});
	    
	total = total+countryTotal+cityTotal+brandTotal;
	    $('#countryCount').text(countryCount);
	    $('#cityCount').text(cityCount);
	    $('#cityTotal').text(cityTotal);
	    $('#countryTotal').text(countryTotal);
	    $('#countryDiscount').text(countryDiscount);
	    $('#cityDiscount').text(cityDiscount);
         $('#total').text(total - cityDiscount - countryDiscount);
         $('#brandCount').text(brandCount);
         $('#brandTotal').text(brandTotal);
         $('#countryIds').val(countryIds);
        $('#cityIds').val(cityIds);
        $('#brandIds').val(brandIds);
        $('#totalAmountPaid').text((total - cityDiscount - countryDiscount) * 3);
        $('#totalAmt').val((total - cityDiscount - countryDiscount) * 3);
        $('#discountAmount').text(0);
}

function netAmountCalculation() {
	 var total = parseFloat($('#total').text(), 10);
	 var tenure = parseFloat($('#tenure').val(), 10);
	 var noOfUser = $('#no_of_users').val();
	 var newVal = 0;
	 if(!isNaN(tenure)){
		 newVal = total * tenure * noOfUser;
	 }
	 else{
		 newVal = total * noOfUser;
	 }
	 
	 if(tenureDiscountObject != null){
		 var tenureDiscount = (newVal*tenureDiscountObject.percentDiscount)/100;
		 newVal = newVal-tenureDiscount;
		 $("#tenureDiscount").text(tenureDiscount);
	 }
	 	newVal = newVal-voucherAmount;
		$('#totalAmountPaid').text(newVal);
		$('#totalAmt').val(newVal);
		$('#discountAmount').text(voucherAmount);
	}
	
function selectAllCities(countryId) {
	if($("#country_"+countryId).prop('checked') == true){
		$(".country_"+countryId).each(function() {
			 $(this).prop('checked', true);
			 $(this).attr("disabled", true);
			});
	}
	else{
		$(".country_"+countryId).each(function() {
			 $(this).prop('checked', false);
			 $(this).removeAttr("disabled");
			});
	}
	calculateAmount();
}
	
</script>
</div>

