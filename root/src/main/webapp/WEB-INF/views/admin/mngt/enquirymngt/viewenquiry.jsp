<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>
<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>

				Registration <br> <small>Company Details</small>
			</h1>
			<span id="errorMsg"></span>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i>
						<spring:message code="label.success" />
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>

			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">�</button>
					<h4>
						<i class="fa fa-times-circle"></i>
						<spring:message code="label.error" />
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>
		</div>
	</div>

		<div class="row">
			<form class="form-horizontal" action="<%=contexturl %>ManageEnquiry/ListEnquiry/ApproveEnquiry?id=${enquiry.enquiryId}" method="post" id="Company_form" enctype="multipart/form-data">
				<input  id="act" type="hidden" name="act"> 
				<div class="col-md-6">
                    <div class="block full">
                        <div class="block-title">
                            <h4 class=""><strong>Personal Details</strong></h4>
                        </div>
						<div class="form-group">
							<label class="col-sm-4 control-label">First Name<span class="text-danger">*</span>
							</label>
							<p class="form-control-static">${enquiry.firstName}</p>
<%-- 							<label class="col-sm-4 control-label">${enquiry.firstName} --%>
<!-- 							</label> -->
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Last Name<span class="text-danger">*</span>
							</label>
							<p class="form-control-static">${enquiry.lastName}</p>
<%-- 							<label class="col-sm-4 control-label">${enquiry.lastName} --%>
<!-- 							</label> -->
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">E-mail<span class="text-danger">*</span>
							</label>
							<p title="${enquiry.emailId}" class="form-control-static" style="white-space: nowrap; overflow: hidden;    
								text-overflow: ellipsis;">${enquiry.emailId}</p>
<%-- 							<label class="col-sm-4 control-label">${enquiry.emailId} --%>
<!-- 							</label> -->
						</div>
					</div>
					<div class="block full">
						<div class="block-title">
							<h4 class=""><strong>Company Details</strong></h4>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">NGO Name <span class="text-danger">*</span>
							</label>
							<p class="form-control-static">${enquiry.ngoName}</p>
<%-- 							<label class="col-sm-4 control-label">${enquiry.ngoName} --%>
<!-- 							</label> -->
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Field/Sector or
								Work <span class="text-danger">*</span>
							</label>
							<p class="form-control-static">${enquiry.sectorWork}</p>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Address Line1</label>
							<c:choose>
								<c:when test="${not empty enquiry.ngoAddress1}">
									<p class="form-control-static">${enquiry.ngoAddress1}</p>
								</c:when>
								<c:otherwise>
									<p class="form-control-static">No Data filled</p>
								</c:otherwise>
							</c:choose>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Address Line2</label>
							<c:choose>
								<c:when test="${not empty enquiry.ngoAddress2}">
									<p class="form-control-static">${enquiry.ngoAddress2}</p>
								</c:when>
								<c:otherwise>
									<p class="form-control-static">No Data filled</p>
								</c:otherwise>
							</c:choose>
							
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Address Line3</label>
							<c:choose>
								<c:when test="${not empty enquiry.ngoAddress3}">
									<p class="form-control-static">${enquiry.ngoAddress3}</p>
								</c:when>
								<c:otherwise>
									<p class="form-control-static">No Data filled</p>
								</c:otherwise>
							</c:choose>						
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="country"><spring:message
									code="label.countryname" /><span class="text-danger">*</span></label>
							<p class="form-control-static">${enquiry.country.countryName}</p>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="area"><spring:message
									code="label.statename" /><span class="text-danger">*</span></label>
							<p class="form-control-static">${enquiry.state.stateName}</p>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="city"><spring:message
									code="label.cityname" /><span class="text-danger">*</span></label>
							<p class="form-control-static">${enquiry.city.cityName}</p>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Pincode<span class="text-danger">*</span>
							</label>
							<p class="form-control-static">${enquiry.ngoPincode}</p>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Telephone</label>
							<c:choose>
								<c:when test="${not empty enquiry.ngoPhone}">
									<p class="form-control-static">${enquiry.ngoPhone}</p>
								</c:when>
								<c:otherwise>
									<p class="form-control-static">No Data filled</p>
								</c:otherwise>
							</c:choose>		
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">NGO Email<span class="text-danger">*</span>
							</label>
							<p title="${enquiry.ngoEmail}" class="form-control-static" style="white-space: nowrap; overflow: hidden;    
								text-overflow: ellipsis;">${enquiry.ngoEmail}</p>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">NGO Website<span class="text-danger">*</span>
							</label>
							<p class="form-control-static wrap-word">${enquiry.ngoWebsite}</p>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="block full">
						<div class="block-title">
							<h4 class=""><strong>About Organisation</strong></h4>
						</div>
						<div class="form-group">
							<label class="col-sm-5 control-label">Organisation Vision<span class="text-danger">*</span>
							</label>
							<div class="col-md-7">
								<textarea id="organisationVision" name="organisationVision" disabled="disabled"
									class="form-control" rows="2" placeholder="Organisation Vision">${enquiry.organisationVision}</textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-5 control-label">Organisation
								Mission<span class="text-danger">*</span>
							</label>
							<div class="col-md-7">
								<textarea id="organisationMission" name="organisationMission" disabled="disabled"
									class="form-control" rows="2"
									placeholder="Organisation Mission">${enquiry.organisationMission}</textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-5 control-label">3 sentences
								describing what your organisation does.<span class="text-danger">*</span>
							</label>
							<div class="col-md-7">
								<textarea id="organisationDescription"
									name="organisationDescription" class="form-control" rows="2" disabled="disabled"
									placeholder="Orginasation Discription">${enquiry.organisationDescription}</textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-6 control-label">Is your
								organisation presently or has it in the past recieved
								organisational development assistance from any other agency?<span class="text-danger">*</span>
							</label>
							<div class="col-sm-6">
							<c:if test = "${enquiry.assitanceFromOtherAgency == false}">
								<p class="form-control-static">No</p>
							</c:if>
							<c:if test = "${enquiry.assitanceFromOtherAgency == true}">
								<p class="form-control-static">Yes</p>
							</c:if>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-6 control-label">Please Indicate the
								Legal Type of Registration<span class="text-danger">*</span>
							</label>
							<div class="col-sm-6">
								<c:if test="${enquiry.legalType eq 'Societies Reg.Act 1860'}">
									<p class="form-control-static">Societies Reg.Act 1860 </p>
								</c:if>
								<c:if test="${enquiry.legalType eq 'Public Trust Act'}">
									<p class="form-control-static">Public Trust Act</p>
								</c:if>
								<c:if test="${enquiry.legalType eq 'Section 25 Company'}">
									<p class="form-control-static">Section 25 Company </p>
								</c:if>
								<c:if test="${enquiry.legalType eq 'Private Limited Company'}">
									<p class="form-control-static">Private Limited Company </p>
								</c:if>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-6 control-label">FCRA Registration<span class="text-danger">*</span>
							</label>
							<div class="col-sm-6">
								<c:if test="${enquiry.fcraRegistarion == false}">
									<p class="form-control-static">No</p>
								</c:if>
								<c:if test="${enquiry.fcraRegistarion == true}">
									<p class="form-control-static">Yes</p>
								</c:if>
							</div>
						</div>
						<hr>
					</div>
					<div class="block full">
						<div class="block-title">
							<h4 class=""><strong>Organisation Details</strong></h4>
						</div>
						<div class="form-group" id="educationTypeDiv">
							<label class="col-sm-4 control-label">Type of Organisation
							</label>
							<c:choose>
								<c:when test="${not empty enquiry.typeOfEducation}">
									<p class="form-control-static">${enquiry.typeOfEducation}</p>
								</c:when>
								<c:otherwise>
									<p class="form-control-static">No Data filled</p>
								</c:otherwise>
							</c:choose>	
							
						</div>
					</div>
					<div class="block full">
						<div class="block-title">
							<h4 class=""><strong>Remark</strong></h4>
						</div>
						 <small>Remark is an important field. It will be
								a reason to the organisation as to why his/her organistaion has
								been approved or rejected</small><br>
						<div class="form-group" id="educationTypeDiv">
							<label class="col-md-4 control-label" for="remark">
								Remark<span class="text-danger">*</span>
							</label>
							<div class="col-md-6">
								<textarea id="remark" name="remark" class="form-control"
									placeholder="Remark..">${company.remark}</textarea>
							</div>
						</div>
					</div>
				</div>
				
			</form>
		</div>
	<div class="row">
		<div class="form-group">
			<div style="text-align: center; margin-bottom: 10px">
				<div class="form-group form-actions">
					<div id="company_submit" class="btn btn-sm btn-primary save">
						<i class="fa fa-angle-right"></i>
						<spring:message code="button.approve" />
					</div>
					<div id="reject_submit" class="btn btn-sm btn-primary save">
						<i class="fa fa-angle-right"></i> Reject
					</div>
					<a class="btn btn-sm btn-primary"
						href="<%=contexturl%>ManageEnquiry/ListEnquiry"></i> <spring:message
							code="button.cancel" /> </a>
				</div>


			</div>
		</div>
	</div>

</div>


<%@include file="../../inc/page_footer.jsp"%>

<a href="#delete_company_popup" data-toggle="modal" id = "enquirypopup"></a>
<div id="delete_company_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					
						<div style="padding: 10px; height: 110px;">
							<label>Are you sure whether you want to approve/reject the Company?</label>
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal">
									<spring:message code="label.no" />
								</button>
								<div id="delete_company" class="btn btn-sm btn-primary">
									<spring:message code="label.yes" />
								</div>
							</div>
						</div>
				</div>
			</div>
		</div>
	</div>
	


<%-- <%@include file="../../inc/page_footer.jsp"%> --%>
<%@include file="../../inc/template_scripts.jsp"%>
<script src="<%=contexturl %>resources/js/additional-methods.js"></script>
<script src="<%=contexturl%>resources/js/custom/featurecontrol.js"></script>
<script
	src="../resources/js/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		var companyForm ;
		$("#manageEnquiry").addClass("active");
		
		$("#company_submit").click(function() {
			unsaved = false;
			$("#act").val("1");	
			$("#Company_form").submit();
		});
		$("#reject_submit").click(function(){
			unsaved = false;
			$("#act").val("0");	
			$("#Company_form").submit();
		});
		
		$("#delete_company").click(function(){
			companyForm.submit();
		});
	$("#Company_form").validate(
			{	errorClass:"help-block animation-slideDown",
				errorElement:"div",
				errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
				highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
				success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
							rules : {
						
								remark : {
									required : !0, 
									maxlength : 500,
								}
							},
							message : {
								remark : {
									required : 'Please enter a remark before submit',
									maxlength :'Remark cannot be more than 500 characters',
								}
							},
							
							submitHandler: function(form) {
									$("#enquirypopup").click();
									companyForm = form;								
			                }
			});
	 

	});
</script>
<style>
.chosen-container {
	width: 250px !important;
}
#map-canvas {
	height: 300px
}
</style>
<%@include file="../../inc/template_end.jsp"%>