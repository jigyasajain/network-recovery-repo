<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/search_page_head.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<style>
#sidebar .chosen-results {
    color: #394263;
}

.block {
    padding: 4px 10px 1px;
    background-color: #FBFBFB;
}

.chosen-container{
    width: 150px !important;
}

.box {
    height: 200px;
    width: 250px;
}

.gi{
    margin-right: 7px;
}
.icon{
height: 16px;
}
.chat-form .form-control{
background-color: #fff;
color:#555;
}

.sidebar-brand,.sidebar-title,.chat-form {
    background: none;
}
@media (min-width:992px){
    .btn-view-service{
        position: absolute;bottom: 0;right: 0;
    }
}

</style>
<!-- Main Restaurant Filter Sidebar -->
<div id="sidebar" >
    <!-- Wrapper for scrolling functionality -->
    <div class="sidebar-scroll">
        <!-- Sidebar Content -->
        <div class="sidebar-content">
            <label style="padding-bottom: 10px; padding-top: 10px; font-weight: 400; font-size: 15px; padding-left: 45px;"><spring:message code="label.refinesearch" /></label>
            <!-- Chat Users -->
            <!-- Chat Input -->
            <form action="<%=contexturl%>AdvanceSearch" 
                class="chat-form" id="advance_search">
                <label class="gi gi-glass pull-left" style="font-size: 15px; padding-top: 6px;"></label>
                        
                <label style="font-weight: 300;"><spring:message code="label.universityname" /></label>
                <input type="hidden" value="0" name="start" id="start">
                <input type="hidden" value="<%=propvalue.paginationInterval %>" name="end" id="end">
                <c:choose>
                <c:when test="${! empty pageNo}">
                <input type="hidden" value="${pageNo}" name="pageNo" id="pageNo">
                </c:when>
                <c:otherwise>
                <input type="hidden" value="1" name="pageNo" id="pageNo">
                </c:otherwise>
                </c:choose>
                
                </a> <input type="text" id="sidebar-chat-message" name="name"
                    class="form-control form-control-borderless"
                    placeholder='<spring:message code="label.universityname"/>' style=" border-radius: 4px; width: 150px; position: relative;"
                    value="${name}" />
                    <button type="submit" class="btn btn-sm btn-primary" style="float: right; margin-top: -35px; margin-right: -3px; font-size: 15px;">
                      <i class="gi gi-search" style="margin-right: 0px;"></i>
                    </button>
                <!-- END Chat Input -->
                
                <label style="padding-top: 10px; font-weight: 300;"><spring:message code="label.course" /></label>
                <label class="gi gi-google_maps pull-left" style="padding-top: 12px; font-size: 17px;"></label>

                <!-- Area Input -->
                <select class="course_chosen"
                    data-placeholder="<spring:message code='label.choosecourse' />"
                    multiple="multiple" class="chosen" style="width: 150px !important;" id="course"
                    name="course">
                    <c:forEach items="${courses}" var="course">
                        <c:set var="present" value="false" />
                        <c:forEach items="${courseArray}" var="courseName">
                            <c:if test="${course.courseName eq courseName}">
                                <c:set var="present" value="true" />
                            </c:if>
                        </c:forEach>
                        <option value="${course.courseName}"<c:if test="${present}"> selected="selected" </c:if>>
                        ${course.courseName}</option>
                    </c:forEach>
                </select>
                <button type="submit" class="btn btn-sm btn-primary" style="float: right; margin-top: -34px; margin-right: -3px; font-size: 15px;">
                    <i class="gi gi-search" style="margin-right: 0px;"></i>
                </button>


<%--                <label style="padding-top: 10px; font-weight: 300;"><spring:message code="label.coursecategory" /></label> --%>
<!--                <label class="gi gi-cutlery pull-left" style="padding-top: 12px; font-size: 17px;"></label> -->

<!--                Area Input -->
<!--                <select class="cuisine_chosen" -->
<%--                    data-placeholder="<spring:message code='label.choosecoursecategory' />" --%>
<!--                    multiple="multiple" class="chosen" id="cuisines" -->
<!--                    name="cuisines"> -->
<%--                    <c:forEach items="${cuisineList}" var="cuisine"> --%>
<%--                        <c:set var="present" value="false" /> --%>
<%--                        <c:forEach items="${searchCuisine}" var="restCuisine"> --%>
<%--                            <c:if test="${cuisine.name eq restCuisine}"> --%>
<%--                                <c:set var="present" value="true" /> --%>
<%--                            </c:if> --%>
<%--                        </c:forEach> --%>
<%--                        <option value="${cuisine.name}" --%>
<%--                            <c:if test="${present}"> selected="selected" </c:if>>${cuisine.name}</option> --%>
<%--                    </c:forEach> --%>
<!--                </select> -->
                
<!--                <button type="submit" class="btn btn-sm btn-primary" style="float: right; margin-top: -34px; margin-right: -3px; font-size: 15px;"> -->
<!--                    <i class="gi gi-search" style="margin-right: 0px;"></i> -->
<!--                </button> -->

                <!-- END Area Input -->
                
                <label style="padding-top: 10px; font-weight: 300;"><spring:message code="heading.languages" /></label>
                <label class="gi gi-notes_2 pull-left" style="padding-top: 15px; font-size: 16px;"></label>

                <!-- Sidebar lang Facilities -->
                <div class="col-sm-12" style="padding-left: 0;">
                <c:forEach items="${langList}" var="lang" varStatus="counter">
                    <div class="checkbox">
                            <label for="example-checkbox3"> <input
                                 class="facilities"
                                id="${lang.languageName }" name="${lang.languageName }" type="checkbox" />${lang.languageName}
                            </label>
                        </div>
                </c:forEach>
                    <div class="checkbox" style="padding-bottom: 5px;">
                        <button type="submit" style="color: black;">
                            <spring:message code="label.searchbutton" />
                        </button>
                    </div>

                </div>
                <!-- END Sidebar lang Facilities -->
                <label style="padding-top: 10px; font-weight: 300;"><spring:message code="label.coursecategory" /></label>
                <label class="gi gi-notes_2 pull-left" style="padding-top: 15px; font-size: 16px;"></label>
                <!-- Sidebar category Facilities -->
                <div class="col-sm-12" style="padding-left: 0;">
                <c:forEach items="${categoryList}" var="category" varStatus="counter">
                    <div class="checkbox">
                            <label for="example-checkbox3"> <input
                                 class="facilities"
                                id="${category.categoryName }" name="${category.categoryName }" type="checkbox" />${category.categoryName}
                            </label>
                        </div>
                </c:forEach>
                    <div class="checkbox" style="padding-bottom: 5px;">
                        <button type="submit" style="color: black;">
                            <spring:message code="label.searchbutton" />
                        </button>
                    </div>

                </div>
                <!-- END Sidebar category Facilities -->

            </form>

            <!-- END Sidebar Notifications -->
        </div>
        <!-- END Sidebar Content -->
    </div>
    <!-- END Wrapper for scrolling functionality -->
</div>
<!-- END Main Filter Sidebar -->



<!-- Main Container -->
<div id="main-container">
    <%
        int count = 0;
        int pageNo=1;
    if(request.getAttribute("count") != null)
        count = (Integer)request.getAttribute("count");
    if(request.getAttribute("pageNo") != null)
        pageNo = (Integer)request.getAttribute("pageNo");
    %>

<style>
#page-content {
    background-color: #FFFFFF;
}

.facilities i {
    padding-right: 10px;
}
</style>


<div id="page-content" >
<%if(request.getAttribute("keyword") != null && request.getAttribute("keyword") !=""){ %>
<div style="font-weight: bold;">showing Search Result - <a  href="<%=contexturl %>SearchResult" style="color:red;text-decoration: none;" >X Clear Search </a> </div>
<%} %>
<div class="row">
        <div class="col-md-9 col-sm-12">
     <!-- Search Info - Pagination -->
            <div class="row">
                <div class="col-md-12 block-section clearfix">
                <%if(count!=0){ %>
                <c:choose>
                
                    <c:when test="${searchType eq 'Advance' }">
                
                    <ul class="pagination remove-margin pull-right" >
                            <li <c:if test="<%=(pageNo <= 1)%>"> class="disabled"</c:if>><a
                                <c:if test="<%=!(pageNo <= 1)%>">  href="javascript:void(0)" 
                         onclick="advanceSearch(<%=(Math.max(0, pageNo - 1))
                        * (propvalue.paginationInterval)%>,<%=propvalue.paginationInterval%>,<%=Math.max(1, pageNo - 1)%>)" 
                        </c:if>><i
                                    class="fa fa-chevron-left"></i></a></li>
                            <li <c:if test="<%=(pageNo ==1)%>">id="selected1" class="active" </c:if>><a href="javascript:void(0)" onclick="advanceSearch(0,<%=propvalue.paginationInterval%>,1)">1</a></li>
                        <%
                            int j=1;
                            for(int i=propvalue.paginationInterval; i<count; i += propvalue.paginationInterval){
                                j++;
                            %>
                                <li <c:if test="<%= (pageNo == j)%>">id="selected1" class="active" </c:if>><a href="javascript:void(0)" onclick="advanceSearch(<%=i%>,<%=propvalue.paginationInterval%>,<%=j %>)"><%=j %></a></li>
                        <%}
                        %>
                        <li <c:if test="<%=(pageNo ==Math.ceil((float)count / propvalue.paginationInterval)) %>"> class="disabled"</c:if>><a <c:if test="<%= ! (pageNo ==Math.ceil((float)count / propvalue.paginationInterval))%>"> 
                        href="javascript:void(0)"  onclick="advanceSearch(<%= (pageNo)*(propvalue.paginationInterval)%>,<%=propvalue.paginationInterval%>,<%=pageNo+1%>)"
                        </c:if>><i class="fa fa-chevron-right"></i></a></li>
                    </ul>
                
                
                
                
                </c:when>
                <c:otherwise>
                
                    <ul class="pagination remove-margin pull-right" >
                            <li <c:if test="<%=(pageNo <= 1)%>"> class="disabled"</c:if>><a
                                <c:if test="<%=!(pageNo <= 1)%>">  href="javascript:void(0)" 
                         onclick="quickSearch(<%=(Math.max(0, pageNo - 1))
                        * (propvalue.paginationInterval)%>,<%=propvalue.paginationInterval%>,<%=Math.max(1, pageNo - 1)%>)" 
                        </c:if>><i
                                    class="fa fa-chevron-left"></i></a></li>
                            <li <c:if test="<%=(pageNo ==1)%>">id="selected1" class="active" </c:if>><a href="javascript:void(0)" onclick="quickSearch(0,<%=propvalue.paginationInterval%>,1)">1</a></li>
                        <%
                            int j=1;
                            for(int i=propvalue.paginationInterval; i<count; i += propvalue.paginationInterval){
                                j++;
                            %>
                                <li <c:if test="<%= (pageNo == j)%>">id="selected1" class="active" </c:if>><a href="javascript:void(0)" onclick="quickSearch(<%=i%>,<%=propvalue.paginationInterval%>,<%=j %>)"><%=j %></a></li>
                        <%}
                        %>
                        <li <c:if test="<%=(pageNo ==Math.ceil((float)count / propvalue.paginationInterval)) %>"> class="disabled"</c:if>><a <c:if test="<%= ! (pageNo ==Math.ceil((float)count / propvalue.paginationInterval))%>"> 
                        href="javascript:void(0)"  onclick="quickSearch(<%= (pageNo)*(propvalue.paginationInterval)%>,<%=propvalue.paginationInterval%>,<%=pageNo+1%>)"
                        </c:if>><i class="fa fa-chevron-right"></i></a></li>
                    </ul>
                
                
                </c:otherwise>
                
                
                </c:choose>
            
                <%} %>
                
                
                </div>
            </div>  
                <!-- END Search Info - Pagination -->
                
                <c:forEach items="${courseList}" var="course" varStatus="counter">
                    <!-- Classic Results -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="block" >
                                 <div class="row" style="padding: 0 5px;">
                                        <div class="col-md-12" >
                                            <div class="row">
                                                <div class="col-md-10 pull-left">
                                                    <h4>                                                                                                    
                                                         <c:choose>
                                                            <c:when test="${course.previewImage!=null}">
                                                                <img src="<%=contexturl %>${course.previewImage.thumbImagePath}" alt="Avatar" class="pull-left img-circle" style="margin-top: -5px; width: 33px;" />
                                                            </c:when>
                                                            <c:otherwise >
                                                                <img src="<%=contexturl %>resources/img/application/rest-logo-default.jpg" alt="Avatar" class="pull-left img-circle" style="margin-top: -5px; width: 33px;" />
                                                            </c:otherwise>
                                                          </c:choose>
                                                          
                                                        <a href="<%=contexturl %>ShowCourse/${course.courseId}" style="padding-left: 5px;"><strong>${course.courseName}</strong></a>
                                                    </h4>
                                                </div>
                                                <div class="col-md-2 pull-right">
                                                    <h4 class="pull-left text-left">
                                                    <div class="col-md-12 " ><i class="fa fa-calendar fa-fw"></i>${course.createdOn}</div>
                                                    <div class="col-md-12 " ><i class="fa fa-calendar fa-fw"></i>${course.courseDuration} Days course</div>                                                 
                                                    </h4>
                                                        
                                                </div>                                          
                                            </div>
                                        </div>
                                        <div class="col-md-12" >
                                            <div class="row">
                                                <div class="col-md-2" >
                                                    <c:if test="${!empty  course.previewImage}">
                                                        <img src="<%=contexturl %>${course.previewImage.thumbImagePath}" class="img-responsive" alt="image" style="max-width: 100px;width: 100%;margin: 0 auto !important;">    
                                                    </c:if>
                                                    <c:if test="${empty course.previewImage}">
                                                        <img src="<%=contexturl %>resources/img/placeholders/photos/default_rest.jpg" class="img-responsive" alt="image" style="max-width: 100px;width: 100%;margin: 0 auto !important;">
                                                    </c:if>
                                                </div>
                                                <div class="col-md-8" style="padding:0;line-height: 1.8em;">
                                                    <div class="col-md-12 text-left pull-left" ><i class="gi gi-compass"></i>University Name :${course.university.universityName}</div>
                                                    <div class="col-md-12 text-left pull-left" ><i class="gi gi-phone_alt"></i>Course Name : ${course.courseName}</div>
<%--                                                    <div class="col-md-12 text-left pull-left" ><i class="gi gi-phone_alt"></i>Created By : ${course.createdBy.firstName} ${course.createdBy.lastName}</div> --%>

                                                </div>
                                            </div>                                              
                                        </div>
                                        </div>                                          
                                </div>

                            </div>
                        </div>
                    
                <!-- END Classic Results -->
                </c:forEach>
                <div class="row">
                    <div class="col-md-3" >
                        <ul class="pagination remove-margin">
                    <c:choose>
                      <c:when test="${count ne 0}">
                            <li class="active" id="result"><span><strong>${count}</strong>
                                    Results</span></li>
                        </c:when>
                        <c:otherwise>
                        <li class="active" id="result" ><span style="width: 136%;"><strong></strong>${resultMsg}</span></li>
                        </c:otherwise>
                    </c:choose>
                        
                        </ul>
                    </div>
                    <div class="col-md-9" >
                        <!-- Bottom Navigation -->
                        <div class="block-section clearfix">
                <%if(count!=0){ %>
                            <c:choose>
                
                    <c:when test="${searchType eq 'Advance' }">
                
                <ul class="pagination remove-margin pull-right" >
                            <li <c:if test="<%=(pageNo <= 1)%>"> class="disabled"</c:if>><a
                                <c:if test="<%=!(pageNo <= 1)%>">  href="javascript:void(0)" 
                         onclick="advanceSearch(<%=(Math.max(0, pageNo - 1))
                        * (propvalue.paginationInterval)%>,<%=propvalue.paginationInterval%>,<%=Math.max(1, pageNo - 1)%>)" 
                        </c:if>><i
                                    class="fa fa-chevron-left"></i></a></li>
                            <li <c:if test="<%=(pageNo ==1)%>">id="selected2" class="active" </c:if>><a href="javascript:void(0)" onclick="advanceSearch(0,<%=propvalue.paginationInterval%>,1)">1</a></li>
                        <%
                            int j=1;
                            for(int i=propvalue.paginationInterval; i<count; i += propvalue.paginationInterval){
                                j++;
                            %>
                                <li <c:if test="<%= (pageNo == j)%>">id="selected2" class="active" </c:if>><a href="javascript:void(0)" onclick="advanceSearch(<%=i%>,<%=propvalue.paginationInterval%>,<%=j %>)"><%=j %></a></li>
                        <%}
                        %>
                        <li <c:if test="<%=(pageNo ==Math.ceil((float)count / propvalue.paginationInterval)) %>"> class="disabled"</c:if>><a <c:if test="<%= ! (pageNo ==Math.ceil((float)count / propvalue.paginationInterval))%>"> 
                        href="javascript:void(0)"  onclick="advanceSearch(<%= (pageNo)*(propvalue.paginationInterval)%>,<%=propvalue.paginationInterval%>,<%=pageNo+1%>)"
                        </c:if>><i class="fa fa-chevron-right"></i></a></li>
                    </ul>
                
                
                
                
                </c:when>
                <c:otherwise>
                
                    <ul class="pagination remove-margin pull-right" >
                            <li <c:if test="<%=(pageNo <= 1)%>"> class="disabled"</c:if>><a
                                <c:if test="<%=!(pageNo <= 1)%>">  href="javascript:void(0)" 
                         onclick="quickSearch(<%=(Math.max(0, pageNo - 1))
                        * (propvalue.paginationInterval)%>,<%=propvalue.paginationInterval%>,<%=Math.max(1, pageNo - 1)%>)" 
                        </c:if>><i
                                    class="fa fa-chevron-left"></i></a></li>
                            <li <c:if test="<%=(pageNo ==1)%>">id="selected2" class="active" </c:if>><a href="javascript:void(0)" onclick="quickSearch(0,<%=propvalue.paginationInterval%>,1)">1</a></li>
                        <%
                            int j=1;
                            for(int i=propvalue.paginationInterval; i<count; i += propvalue.paginationInterval){
                                j++;
                            %>
                                <li <c:if test="<%= (pageNo == j)%>">id="selected2" class="active" </c:if>><a href="javascript:void(0)" onclick="quickSearch(<%=i%>,<%=propvalue.paginationInterval%>,<%=j %>)"><%=j %></a></li>
                        <%}
                        %>
                        <li <c:if test="<%=(pageNo ==Math.ceil((float)count / propvalue.paginationInterval)) %>"> class="disabled"</c:if>><a <c:if test="<%= ! (pageNo ==Math.ceil((float)count / propvalue.paginationInterval))%>"> 
                        href="javascript:void(0)"  onclick="quickSearch(<%= (pageNo)*(propvalue.paginationInterval)%>,<%=propvalue.paginationInterval%>,<%=pageNo+1%>)"
                        </c:if>><i class="fa fa-chevron-right"></i></a></li>
                    </ul>
                
                
                </c:otherwise>
                
                
                </c:choose>
                <%} %>
                        </div>
                        <!-- END Bottom Navigation -->
                    </div>                      
                </div>

                

                
        </div>
            <div class="col-md-3 hidden-sm hidden-xs">
                <div class="row">
                    <div class="col-md-12">
                        <div class="block box">
                            <c:forEach items="${adPerAdLoc1}" var="adLoc">
                                <c:if test="${adLoc.key.slot eq 'LEFT_POSITION1' }">
                                    <c:choose>
                                        <c:when test="${fn:length(adLoc.value) gt 1}">

                                            <div id="slider1"
                                                style="width: 228px; height: 200px !important; margin-top: 4px;">
                                                <c:forEach items="${adLoc.value}" var="ad">
                                                    <div align="left">
                                                        <a href="${ad.destinationURL}" target="_blank"
                                                            style="width: 228px; height: 180px !important"
                                                            title="clickhere"><img
                                                            src="<%=contexturl %>${ad.img.imagePath}"
                                                            style="width: 228px; height: 180px !important; border-radius: 5px;" /></a>
                                                    </div>
                                                </c:forEach>
                                            </div>

                                        </c:when>
                                        <c:when test="${fn:length(adLoc.value) eq 1}">

                                            <div id=""
                                                style="width: 228px; height: 200px !important; margin-top: 4px;">
                                                <c:forEach items="${adLoc.value}" var="ad">
                                                    <div align="left"
                                                        style="left: 0px !important; width: 228px; height: 180px !important">
                                                        <a href="${ad.destinationURL}" target="_blank"
                                                            style="left: 0px !important; width: 228px; height: 180px !important"
                                                            title="clickhere"><img
                                                            src="<%=contexturl %>${ad.img.imagePath}"
                                                            style="left: 0px !important; width: 228px; height: 180px !important; border-radius: 5px;" /></a>
                                                    </div>
                                                </c:forEach>
                                            </div>

                                        </c:when>
                                        <c:otherwise>
                                            <div>Advertise with us</div>
                                        </c:otherwise>
                                    </c:choose>
                                </c:if>
                            </c:forEach>
                        </div>

                        <div class="block box">
                            <c:forEach items="${adPerAdLoc2}" var="adLoc">
                                <c:if test="${adLoc.key.slot eq 'LEFT_POSITION2' }">
                                    <c:choose>
                                        <c:when test="${fn:length(adLoc.value) gt 1}">

                                            <div id="slider1"
                                                style="width: 228px; height: 200px !important; margin-top: 4px;">
                                                <c:forEach items="${adLoc.value}" var="ad">
                                                    <div align="left">
                                                        <a href="${ad.destinationURL}" target="_blank"
                                                            style="width: 228px; height: 180px !important"
                                                            title="clickhere"><img
                                                            src="<%=contexturl %>${ad.img.imagePath}"
                                                            style="width: 228px; height: 180px !important; border-radius: 5px;" /></a>
                                                    </div>
                                                </c:forEach>
                                            </div>

                                        </c:when>
                                        <c:when test="${fn:length(adLoc.value) eq 1}">

                                            <div id=""
                                                style="width: 228px; height: 200px !important; margin-top: 4px;">
                                                <c:forEach items="${adLoc.value}" var="ad">
                                                    <div align="left"
                                                        style="left: 0px !important; width: 228px; height: 180px !important">
                                                        <a href="${ad.destinationURL}" target="_blank"
                                                            style="left: 0px !important; width: 228px; height: 180px !important"
                                                            title="clickhere"><img
                                                            src="<%=contexturl %>${ad.img.imagePath}"
                                                            style="left: 0px !important; width: 228px; height: 180px !important; border-radius: 5px;" /></a>
                                                    </div>
                                                </c:forEach>
                                            </div>

                                        </c:when>
                                        <c:otherwise>
                                            <div>Advertise with us</div>
                                        </c:otherwise>
                                    </c:choose>
                                </c:if>
                            </c:forEach>
                        </div>

                        <div class="block box">
                            <c:forEach items="${adPerAdLoc3}" var="adLoc">
                                <c:if test="${adLoc.key.slot eq 'LEFT_POSITION3' }">
                                    <c:choose>
                                        <c:when test="${fn:length(adLoc.value) gt 1}">

                                            <div id="slider1"
                                                style="width: 228px; height: 200px !important; margin-top: 4px;">
                                                <c:forEach items="${adLoc.value}" var="ad">
                                                    <div align="left">
                                                        <a href="${ad.destinationURL}" target="_blank"
                                                            style="width: 228px; height: 180px !important"
                                                            title="clickhere"><img
                                                            src="<%=contexturl %>${ad.img.imagePath}"
                                                            style="width: 228px; height: 180px !important; border-radius: 5px;" /></a>
                                                    </div>
                                                </c:forEach>
                                            </div>

                                        </c:when>
                                        <c:when test="${fn:length(adLoc.value) eq 1}">

                                            <div id=""
                                                style="width: 228px; height: 200px !important; margin-top: 4px;">
                                                <c:forEach items="${adLoc.value}" var="ad">
                                                    <div align="left"
                                                        style="left: 0px !important; width: 228px; height: 180px !important">
                                                        <a href="${ad.destinationURL}" target="_blank"
                                                            style="left: 0px !important; width: 228px; height: 180px !important"
                                                            title="clickhere"><img
                                                            src="<%=contexturl %>${ad.img.imagePath}"
                                                            style="left: 0px !important; width: 228px; height: 180px !important; border-radius: 5px;" /></a>
                                                    </div>
                                                </c:forEach>
                                            </div>

                                        </c:when>
                                        <c:otherwise>
                                            <div>Advertise with us</div>
                                        </c:otherwise>
                                    </c:choose>
                                </c:if>
                            </c:forEach>
                        </div>

                        <div class="block box">
                            <c:forEach items="${adPerAdLoc4}" var="adLoc">
                                <c:if test="${adLoc.key.slot eq 'LEFT_POSITION4' }">
                                    <c:choose>
                                        <c:when test="${fn:length(adLoc.value) gt 1}">

                                            <div id="slider1"
                                                style="width: 228px; height: 200px !important; margin-top: 4px;">
                                                <c:forEach items="${adLoc.value}" var="ad">
                                                    <div align="left">
                                                        <a href="${ad.destinationURL}" target="_blank"
                                                            style="width: 228px; height: 180px !important"
                                                            title="clickhere"><img
                                                            src="<%=contexturl %>${ad.img.imagePath}"
                                                            style="width: 228px; height: 180px !important; border-radius: 5px;" /></a>
                                                    </div>
                                                </c:forEach>
                                            </div>

                                        </c:when>
                                        <c:when test="${fn:length(adLoc.value) eq 1}">

                                            <div id=""
                                                style="width: 228px; height: 200px !important; margin-top: 4px;">
                                                <c:forEach items="${adLoc.value}" var="ad">
                                                    <div align="left"
                                                        style="left: 0px !important; width: 228px; height: 180px !important">
                                                        <a href="${ad.destinationURL}" target="_blank"
                                                            style="left: 0px !important; width: 228px; height: 180px !important"
                                                            title="clickhere"><img
                                                            src="<%=contexturl %>${ad.img.imagePath}"
                                                            style="left: 0px !important; width: 228px; height: 180px !important; border-radius: 5px;" /></a>
                                                    </div>
                                                </c:forEach>
                                            </div>

                                        </c:when>
                                        <c:otherwise>
                                            <div>Advertise with us</div>
                                        </c:otherwise>
                                    </c:choose>
                                </c:if>
                            </c:forEach>
                        </div>

                        <div class="block box">
                            <c:forEach items="${adPerAdLoc5}" var="adLoc">
                                <c:if test="${adLoc.key.slot eq 'LEFT_POSITION5' }">
                                    <c:choose>
                                        <c:when test="${fn:length(adLoc.value) gt 1}">

                                            <div id="slider1"
                                                style="width: 228px; height: 200px !important; margin-top: 4px;">
                                                <c:forEach items="${adLoc.value}" var="ad">
                                                    <div align="left">
                                                        <a href="${ad.destinationURL}" target="_blank"
                                                            style="width: 228px; height: 180px !important"
                                                            title="clickhere"><img
                                                            src="<%=contexturl %>${ad.img.imagePath}"
                                                            style="width: 228px; height: 180px !important; border-radius: 5px;" /></a>
                                                    </div>
                                                </c:forEach>
                                            </div>

                                        </c:when>
                                        <c:when test="${fn:length(adLoc.value) eq 1}">

                                            <div id=""
                                                style="width: 228px; height: 200px !important; margin-top: 4px;">
                                                <c:forEach items="${adLoc.value}" var="ad">
                                                    <div align="left"
                                                        style="left: 0px !important; width: 228px; height: 180px !important">
                                                        <a href="${ad.destinationURL}" target="_blank"
                                                            style="left: 0px !important; width: 228px; height: 180px !important"
                                                            title="clickhere"><img
                                                            src="<%=contexturl %>${ad.img.imagePath}"
                                                            style="left: 0px !important; width: 228px; height: 180px !important; border-radius: 5px;" /></a>
                                                    </div>
                                                </c:forEach>
                                            </div>

                                        </c:when>
                                        <c:otherwise>
                                            <div>Advertise with us</div>
                                        </c:otherwise>
                                    </c:choose>
                                </c:if>
                            </c:forEach>
                        </div>

                    </div>
                </div>


            </div>
    </div>
</div>
<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
        

<script type="text/javascript"  src="<%=contexturl %>resources/js/jquery.slides.min.js"></script>

<script type="text/javascript">
    
    $(document).ready(function() {
        
        $("#QuickSearchForm").validate(
                {   errorClass:"help-block animation-slideDown",
                    errorElement:"div",
                    errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
                    highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
                    success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
                    rules:{ keyword:{maxlength : 75}
                    },
                    messages:{
                        keyword:{maxlength :'<spring:message code="validation.keyword75character"/>'}
                    },
                    
                });
        
        $("#advance_search").validate(
                {   errorClass:"help-block animation-slideDown",
                    errorElement:"div",
                    errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
                    highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
                    success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
                    rules:{ name:{maxlength : 75}
                    },
                    messages:{
                        name:{maxlength :'<spring:message code="validation.name75character"/>'}
                    },
                    
                });
    });
</script>

<script>

        jQuery(document).ready(function(){
            
            
            $('#selected1').addClass('active');
            $('#selected2').addClass('active');
            $('#result').addClass('active');
        
        jQuery("#slider1").slidesjs({
                width: 228,
                height: 180,
                play: {
                  active: false,
                    // [boolean] Generate the play and stop buttons.
                    // You cannot use your own buttons. Sorry.
                  effect: "slide",
                    // [string] Can be either "slide" or "fade".
                  interval: 5000,
                    // [number] Time spent on each slide in milliseconds.
                  auto: true,
                    // [boolean] Start playing the slideshow on load.
                  swap: false,
                    // [boolean] show/hide stop and play buttons
                  pauseOnHover: false,
                    // [boolean] pause a playing slideshow on hover
                  restartDelay: 250
 
                }
                
            });
            
            
            
            
 
            jQuery("#slider2").slidesjs({
                width: 250,
                height: 200,
                play: {
                  active: false,
                    // [boolean] Generate the play and stop buttons.
                    // You cannot use your own buttons. Sorry.
                  effect: "slide",
                    // [string] Can be either "slide" or "fade".
                  interval: 5000,
                    // [number] Time spent on each slide in milliseconds.
                  auto: true,
                    // [boolean] Start playing the slideshow on load.
                  swap: false,
                    // [boolean] show/hide stop and play buttons
                  pauseOnHover: false,
                    // [boolean] pause a playing slideshow on hover
                  restartDelay: 250
 
                }
                
            });
            
            
            
            
        

            jQuery("#slider3").slidesjs({
                width: 250,
                height: 200,
                play: {
                  active: false,
                    // [boolean] Generate the play and stop buttons.
                    // You cannot use your own buttons. Sorry.
                  effect: "slide",
                    // [string] Can be either "slide" or "fade".
                  interval: 5000,
                    // [number] Time spent on each slide in milliseconds.
                  auto: true,
                    // [boolean] Start playing the slideshow on load.
                  swap: false,
                    // [boolean] show/hide stop and play buttons
                  pauseOnHover: false,
                    // [boolean] pause a playing slideshow on hover
                  restartDelay: 250
 
                }
                
            });

        
            
        

           
            jQuery("#slider4").slidesjs({
                width: 250,
                height: 200,
                play: {
                  active: false,
                    // [boolean] Generate the play and stop buttons.
                    // You cannot use your own buttons. Sorry.
                  effect: "slide",
                    // [string] Can be either "slide" or "fade".
                  interval: 5000,
                    // [number] Time spent on each slide in milliseconds.
                  auto: true,
                    // [boolean] Start playing the slideshow on load.
                  swap: false,
                    // [boolean] show/hide stop and play buttons
                  pauseOnHover: false,
                    // [boolean] pause a playing slideshow on hover
                  restartDelay: 250
 
                }
                
            });
            
            
            
            jQuery("#slider5").slidesjs({
                width: 250,
                height: 200,
                play: {
                  active: false,
                    // [boolean] Generate the play and stop buttons.
                    // You cannot use your own buttons. Sorry.
                  effect: "slide",
                    // [string] Can be either "slide" or "fade".
                  interval: 5000,
                    // [number] Time spent on each slide in milliseconds.
                  auto: true,
                    // [boolean] Start playing the slideshow on load.
                  swap: false,
                    // [boolean] show/hide stop and play buttons
                  pauseOnHover: false,
                    // [boolean] pause a playing slideshow on hover
                  restartDelay: 250
 
                }
                
            });
            
            
            
            
        
            });

    
    $(".course_chosen").chosen();
    
    
    function quickSearch(start, end,pageno){
        $("#paginationStart").val(start);
        $("#paginationEnd").val(end);
        $("#paginationPageno").val(pageno);
        $("#QuickSearchForm").submit();
    }
    
    
    function advanceSearch(start, end,pageno){
        $("#start").val(start);
        $("#end").val(end);
        $("#pageNo").val(pageno);
        $("#advance_search").submit();
    }
    
    function openCloseFacilities() {
        var element = document.getElementById("viewdetailslabel");
        if (element.innerHTML === 'View More..'){
            element.innerHTML = 'Hide';
        }
        else {
            element.innerHTML = 'View More..';
        }
    }
    
    $(document).ready(function() {
        
        gridShowFollow();
        
        function gridShowFollow(){
            <c:forEach items="${restaurantList}" var="rest" varStatus="counter13">
            if(${rest.follow} == 1){
                 $('#unFollowRestDiv_${rest.outletId}').css("display", "block");
            }
            else{
                 $('#followRestDiv_${rest.outletId}').css("display", "block");
            }
             </c:forEach>
        }
    });
    
    function gridFollowChanged(followVal, outletId){
         $.ajax({
                type: "GET",
                async: false,
                url: "<%=contexturl %>RestaurantFollow/"+outletId+"/"+followVal,
                success: function(data, textStatus, jqXHR){
                    var obj = $.parseJSON(data);
                    if(obj.success == 'followed'){
                         $('#unFollowRestDiv_'+outletId).css("display","inline");
                         $('#followRestDiv_'+outletId).css("display","none");
                    }
                    else{
                        $('#unFollowRestDiv_'+outletId).css("display","none");
                        $('#followRestDiv_'+outletId).css("display","inline");
                    }
                },
                dataType: 'html'
            });
    }
</script>
<%@include file="../../inc/template_end.jsp"%>