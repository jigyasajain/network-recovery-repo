<%@page import="com.astrika.outletmngt.model.RestaurantMembershipType"%>
<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>
<style>
	.memberType{
		float: left;
		margin-right: 40px;
		margin-top: 10px;
		margin-left: 50px;
	}
	
	.memberRange{
		float: left;
		margin-right: 10px;
		margin-left: 50px;
	
	}
</style>
<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i>
						<spring:message code="label.error" />
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>
			<h1>
				<i class="fa fa-map-marker"></i>
				<spring:message code="heading.membershipmodal" />
			</h1>
			<span id="errorMsg"></span>
		</div>
	</div>
<!-- 	<ul class="breadcrumb breadcrumb-top"> -->
<%-- 		<li><spring:message code="menu.brand" /></li> --%>
<!-- 	</ul> -->
	<div class="block full" id="formView">
		<form id="update_form" action="<%=contexturl %>ManageRestaurant/RestaurantList/UpdateRestaurantModal">
			<input type="hidden" name="outletId" value="${outletId}">
			<table id="example-datatable"
						class="table table-vcenter table-condensed table-bordered">
						<tbody id="tbody">
							<c:forEach var="modal" items="${membershipList}" varStatus="counter">
								<c:set var="count" value="${counter.count}"></c:set>
									<input  name="id" value="${modal.id}" class="data data_${modal.id}" disabled="disabled" type="hidden">
									<tr>
										<td class="text-center" colspan="2" style="font-weight: bold;background-color: #F8F8F8;">
											<div class="memberType"><spring:message code="${modal.restMembership.type}"/></div>
										</td>
										<td  class="text-center" colspan="2" style="font-weight: bold;background-color: #F8F8F8;">
											<div class="input-group">
												<span class="input-group-addon">${restCurrency.currencySign}</span>
												<div><input id="minRange_${modal.id}" class="form-control text-right"
															type="text" value="${modal.restMembership.minAmount}" disabled="disabled"></div>	
											</div>
										</td>
										<td  class="text-center" colspan="2" style="font-weight: bold;background-color: #F8F8F8;">
											<div class="input-group">
												<span class="input-group-addon">${restCurrency.currencySign}</span>
												<div ><input id="maxRange_${modal.id}" class="form-control text-right"
																type="text" value="${modal.restMembership.maxAmount}" disabled="disabled">
												</div>
											</div>
										</td>
										<td  class="text-center" colspan="2" style="font-weight: bold;background-color: #F8F8F8;">
											<div style="float: right; margin: 4px"  id="actionDiv_${modal.id}">
												<a href="#" onclick="editModal(${modal.id})" id="edit_${modal.id}" class="btn btn-xs btn-default edit_action" title="Edit"><i class="fa fa-pencil"></i></a>
												<a href="#" onclick="cancelModal(${modal.id})" id="cancel_${modal.id}" class="btn btn-xs btn-default action" style="display: none" title="Cancel"><i class="fa fa-times"></i></a>
												<button type="submit" id="save_${modal.id}" class="btn btn-xs btn-default action save" style="display: none"><i class="gi gi-ok_2" title="Save"></i></button>
											</div>
											
										</td>
									</tr>
									<tr>
										<th class="text-center"></th>
										<th class="text-center"><spring:message code="days.monday" /></th>
										<th class="text-center"><spring:message code="days.tuesday" /></th>
										<th class="text-center"><spring:message
												code="days.wednesday" /></th>
										<th class="text-center"><spring:message
												code="days.thursday" /></th>
										<th class="text-center"><spring:message code="days.friday" /></th>
										<th class="text-center"><spring:message
												code="days.saturday" /></th>
										<th class="text-center"><spring:message code="days.sunday" /></th>
									</tr>
									<tr>
										<td class="text-center" style="font-weight: bold;"><spring:message code="label.discount"/></td>
										<td class="text-center">
											<div class="form-group" style="margin: 0">
												<div class="input-group">
													<div>
														<input id="d_monday" name="dMonday"  class="form-control text-right data data_${modal.id}"
															type="text" value="${modal.mondayDiscount}" disabled="disabled">
													</div>
													<span class="input-group-addon">%</span>
												</div>
											</div>
										</td>
										<td class="text-center">
											<div class="form-group" style="margin: 0">
												<div class="input-group">
													<div>
														<input id="d_tuesday" name="dTuesday" class="form-control text-right data data_${modal.id}"
															type="text" value="${modal.tuesdayDiscount}" disabled="disabled">
													</div>
													<span class="input-group-addon">%</span>
												</div>
											</div>
										</td>
										<td class="text-center">
											<div class="form-group" style="margin: 0">
												<div class="input-group">
													<div>
														<input id="d_wednesday" name="dWednesday"
															class="form-control text-right data data_${modal.id}" disabled="disabled"
															type="text" value="${modal.wednesdayDiscount}">
													</div>
													<span class="input-group-addon">%</span>
												</div>
											</div>
										</td>
										<td class="text-center">
											<div class="form-group" style="margin: 0">
												<div class="input-group">
													<div>
														<input id="d_thursday" name="dThursday" class="form-control text-right data data_${modal.id}"
															type="text" value="${modal.thursdayDiscount}" disabled="disabled">
													</div>
													<span class="input-group-addon">%</span>
												</div>
											</div>
										</td>
										<td class="text-center">
											<div class="form-group" style="margin: 0">
												<div class="input-group">
													<div>
														<input id="d_friday" name="dFriday" class="form-control text-right data data_${modal.id}"
															type="text" value="${modal.fridayDiscount}" disabled="disabled">
													</div>
													<span class="input-group-addon">%</span>
												</div>
											</div>
										</td>
										<td class="text-center">
											<div class="form-group" style="margin: 0">
												<div class="input-group">
													<div>
														<input id="d_saturday" name="dSaturday" class="form-control text-right data data_${modal.id}"
															type="text" value="${modal.saturdayDiscount}" disabled="disabled">
													</div>
													<span class="input-group-addon">%</span>
												</div>
											</div>
										</td>
										<td class="text-center">
											<div class="form-group" style="margin: 0">
												<div class="input-group">
													<div>
														<input id="d_sunday" name="dSunday" class="form-control text-right data data_${modal.id}"
															type="text" value="${modal.sundaydiscount}" disabled="disabled">
													</div>
													<span class="input-group-addon">%</span>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td class="text-center" style="font-weight: bold;"><spring:message code="label.complementory"/></td>
										<td class="text-center"><input id="c_monday" name="cMonday"
											class="form-control data data_${modal.id}"
											type="text" value="${modal.mondayComp}" disabled="disabled"></td>
										<td class="text-center"><input id="c_tuesday"
											name="cTuesday" class="form-control data data_${modal.id}"
											type="text" value="${modal.tuesdayComp}" disabled="disabled"></td>
										<td class="text-center"><input id="c_wednesday"
											name="cWednesday" class="form-control data data_${modal.id}"
											type="text" value="${modal.wednesdayComp}" disabled="disabled"></td>
										<td class="text-center"><input id="c_thursday"
											name="cThursday" class="form-control data data_${modal.id}"
											type="text" value="${modal.thursdayComp}" disabled="disabled"></td>
										<td class="text-center"><input id="c_friday" name="cFriday"
											class="form-control data data_${modal.id}"
											type="text" value="${modal.fridayComp}" disabled="disabled"></td>
										<td class="text-center"><input id="c_saturday"
											name="cSaturday" class="form-control data data_${modal.id}"
											type="text" value="${modal.saturdayComp}" disabled="disabled"></td>
										<td class="text-center"><input id="c_sunday" name="cSunday"
											class="form-control data data_${modal.id}"
											type="text" value="${modal.sundayComp}" disabled="disabled"></td>
									</tr>
									<tr>
										<td class="text-center" style="font-weight: bold;"><spring:message code="label.comment"/></td>
										<td class="text-center"><input id="comment_monday"
											name="commentMonday" class="form-control data data_${modal.id}"
											type="text" value="${modal.tuesdayComment}" disabled="disabled"></td>
										<td class="text-center"><input id="comment_tuesday"
											name="commentTuesday" class="form-control data data_${modal.id}"
											type="text" value="${modal.tuesdayComment}" disabled="disabled"></td>
										<td class="text-center"><input id="comment_wednesday"
											name="commentWednesday" class="form-control data data_${modal.id}"
											type="text" value="${modal.wednesdayComment}" disabled="disabled"></td>
										<td class="text-center"><input id="comment_thursday"
											name="commentThursday" class="form-control data data_${modal.id}"
											type="text" value="${modal.thursdayComment}" disabled="disabled"></td>
										<td class="text-center"><input id="comment_friday"
											name="commentFriday" class="form-control data data_${modal.id}"
											type="text" value="${modal.fridayComment}" disabled="disabled"></td>
										<td class="text-center"><input id="comment_saturday"
											name="commentSaturday" class="form-control data data_${modal.id}"
											type="text" value="${modal.saturdayComment}" disabled="disabled"></td>
										<td class="text-center"><input id="comment_sunday"
											name="commentSunday" class="form-control data data_${modal.id}"
											type="text" value="${modal.sundayComment}" disabled="disabled"></td>
									</tr>
									<tr style="height: 50px;border: 0">
										<td colspan="8" ></td>
									</tr>
								
						</c:forEach>
					</tbody>
				</table>
			</form>
				${counter.count}
		<c:if test="${count lt 8 }">
			<div id="addMembership" class="btn btn-sm btn-primary">
							<i class="fa fa-angle-right"></i> <spring:message code="label.addmembershipmodal" />
			</div>
		</c:if>
		<form action="SaveMembershipModal" method="post"
			class="form-horizontal" 
			id="membership_form">	
			<input type="hidden" name="outletId" value="${outletId}">
			<input type="hidden" name="membershipType" value="${type}">
		</form>
			
		
	</div>
</div>
<%
String nextType = RestaurantMembershipType.fromInt((Integer)request.getAttribute("type")).name();
%>

<c:set var="membershipType" value="<%=nextType %>"></c:set>
<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<script type="text/javascript">
$(document).ready(function() {
	$("#addMembership").click(function(){
		var html = '<button class="btn btn-sm btn-primary">	<i class="fa fa-angle-right"></i> <spring:message code="button.submit" /></button><table id="example-datatable" class="table table-vcenter table-condensed table-bordered"><tr><td class="text-center" colspan="2" style="font-weight: bold;background-color: #F8F8F8;">	<div class="memberType"><spring:message code="${membershipType}"/></div></td><td class="text-center" colspan="3" style="font-weight: bold;background-color: #F8F8F8;">	<div class="input-group"><div class=""><input id="minRange" name="minRange" class="form-control text-right" placeholder="from.." type="text" value="${startRange}"  disabled="disabled"><input name="minRange" type="hidden" value="${startRange}" ></div><span class="input-group-addon">${restCurrency.currencySign}</span></div></td>'+
		'<td class="text-center" colspan="3" style="font-weight: bold;background-color: #F8F8F8;"> <div class="form-group" style="margin: 0"><div><div class="input-group"><div><input id="maxRange" name="maxRange" class="form-control text-right"	placeholder="to.."	type="text" ></div> <span class="input-group-addon">${restCurrency.currencySign}</span></div></div></div></td></tr>'+
		'<tr><th class="text-center"></th><th class="text-center"><spring:message code="days.monday" /></th><th class="text-center"><spring:message code="days.tuesday" /></th><th class="text-center"><spring:message code="days.wednesday" /></th><th class="text-center"><spring:message code="days.thursday" /></th><th class="text-center"><spring:message code="days.friday" /></th><th class="text-center"><spring:message code="days.saturday" /></th><th class="text-center"><spring:message code="days.sunday" /></th></tr>'+
		'<tr><td class="text-center" style="font-weight: bold;"><spring:message code="label.discount"/></td>	<td class="text-center">'+
		' <div class="form-group" style="margin: 0"><div><div class="input-group"><div><input id="d_monday" name="dMonday" class="form-control text-right"	placeholder="<spring:message code="label.disountmsg"/>.." type="text" value="${modal.mondayDiscount}"></div><span class="input-group-addon">%</span></div></div></div></td>	<td class="text-center">'+
		' <div class="form-group" style="margin: 0"><div><div class="input-group">	<div><input id="d_tuesday" name="dTuesday" class="form-control text-right" placeholder="<spring:message code="label.disountmsg"/>.." type="text" value="${modal.tuesdayDiscount}">	</div><span class="input-group-addon">%</span></div></div></div></td>'+
		' <td class="text-center"><div class="form-group" style="margin: 0"><div><div class="input-group"><div><input id="d_wednesday" name="dWednesday" class="form-control text-right" placeholder="<spring:message code="label.disountmsg"/>.." type="text" value="${modal.wednesdayDiscount}">'+
		' </div><span class="input-group-addon">%</span></div></div></div></td><td class="text-center"><div class="form-group" style="margin: 0"><div><div class="input-group"><div><input id="d_thursday" name="dThursday" class="form-control text-right" placeholder="<spring:message code="label.disountmsg"/>.."	type="text" value="${modal.thursdayDiscount}">'+
		' </div><span class="input-group-addon">%</span></div></div></div></td><td class="text-center"><div class="form-group" style="margin: 0"><div><div class="input-group"><div><input id="d_friday" name="dFriday" class="form-control text-right" placeholder="<spring:message code="label.disountmsg"/>.." type="text" value="${modal.fridayDiscount}">'+
		' </div><span class="input-group-addon">%</span></div></div></div></td><td class="text-center"><div class="form-group" style="margin: 0"><div><div class="input-group"><div><input id="d_saturday" name="dSaturday" class="form-control text-right" placeholder="<spring:message code="label.disountmsg"/>.."	type="text" value="${modal.saturdayDiscount}">'+
		' </div><span class="input-group-addon">%</span></div></div></div></td><td class="text-center"><div class="form-group" style="margin: 0"><div><div class="input-group"><div><input id="d_sunday" name="dSunday" class="form-control text-right" placeholder="<spring:message code="label.disountmsg"/>.." type="text" value="${modal.sundaydiscount}"></div><span class="input-group-addon">%</span></div></div></div></td></tr>'+
		' <tr><td class="text-center" style="font-weight: bold;"><spring:message code="label.complementory"/></td><td class="text-center"><input id="c_monday" name="cMonday" class="form-control"	placeholder="<spring:message code="label.complementrymsg"/>.."	type="text" value="${modal.mondayComp}"></td>'+
		' <td class="text-center"><input id="c_tuesday"	name="cTuesday" class="form-control" placeholder="<spring:message code="label.complementrymsg"/>.."	type="text" value="${modal.tuesdayComp}"></td><td class="text-center"><input id="c_wednesday" name="cWednesday" class="form-control" placeholder="<spring:message code="label.complementrymsg"/>.."'+
		' type="text" value="${modal.wednesdayComp}"></td><td class="text-center"><input id="c_thursday"	name="cThursday" class="form-control"	placeholder="<spring:message code="label.complementrymsg"/>.."	type="text" value="${modal.thursdayComp}"></td><td class="text-center"><input id="c_friday" name="cFriday"'+
		' class="form-control"	placeholder="<spring:message code="label.complementrymsg"/>.."	type="text" value="${modal.fridayComp}"></td> <td class="text-center"><input id="c_saturday" name="cSaturday" class="form-control"	placeholder="<spring:message code="label.complementrymsg"/>.."'+
		' type="text" value="${modal.saturdayComp}"></td><td class="text-center"><input id="c_sunday" name="cSunday" class="form-control" placeholder="<spring:message code="label.complementrymsg"/>.." type="text" value="${modal.sundayComp}"></td></tr><tr>	<td class="text-center" style="font-weight: bold;"><spring:message code="label.comment"/></td>'+
		' <td class="text-center"><input id="comment_monday" name="commentMonday" class="form-control" 	placeholder="<spring:message code="label.commentmsg"/>.." type="text" value="${modal.tuesdayComment}"></td> <td class="text-center"><input id="comment_tuesday" '+
		' name="commentTuesday" class="form-control" placeholder="<spring:message code="label.commentmsg"/>.."	type="text" value="${modal.tuesdayComment}"></td><td class="text-center"><input id="comment_wednesday"	name="commentWednesday" class="form-control" placeholder="<spring:message code="label.commentmsg"/>.."	type="text" value="${modal.wednesdayComment}"></td>'+
		' <td class="text-center"><input id="comment_thursday"	name="commentThursday" class="form-control"	placeholder="<spring:message code="label.commentmsg"/>.." type="text" value="${modal.thursdayComment}"></td> <td class="text-center"><input id="comment_friday"'+
		' name="commentFriday" class="form-control"	placeholder="<spring:message code="label.commentmsg"/>.." type="text" value="${modal.fridayComment}"></td><td class="text-center"><input id="comment_saturday"	name="commentSaturday" class="form-control" placeholder="<spring:message code="label.commentmsg"/>.."'+
		' type="text" value="${modal.saturdayComment}"></td><td class="text-center"><input id="comment_sunday"	name="commentSunday" class="form-control" placeholder="<spring:message code="label.commentmsg"/>.."	type="text" value="${modal.sundayComment}"></td></tr></table>';
		$("#membership_form").append(html);
		$("#addMembership").css("display","none");
	});
	
	$("#membership_form").validate(
			{	errorClass:"help-block animation-slideDown",
				errorElement:"div",
				errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
				highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
				success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
				rules:{
					dMonday : {
					required : !0,
					discount : !0,
					
				},
				dTuesday : {
					required : !0,
					discount : !0,
				},
				dWednesday : {
					required : !0,
					discount : !0,
				},
				dThursday : {
					required : !0,
					discount : !0,
				},
				dFriday : {
					required : !0,
					discount : !0,
				},
				dSaturday : {
					required : !0,
					discount : !0,
				},
				dSunday : {
					required : !0,
					discount : !0,
				},
				maxRange :{
					required : !0,
					greaterThan :'#minRange'
				
					}
											
				
				},
				messages:{
					dMonday : {
						required :'<spring:message code="validation.monday"/>',
						discount :'<spring:message code="validation.discount"/>',
						
					},
					dTuesday : {
						required : '<spring:message code="validation.tuesday"/>',
						discount :'<spring:message code="validation.discount"/>',
					},
					dWednesday : {
						required :'<spring:message code="validation.wednesday"/>',
						discount :'<spring:message code="validation.discount"/>',
					},
					dThursday : {
						required : '<spring:message code="validation.thursday"/>',
						discount :'<spring:message code="validation.discount"/>',
					},
					dFriday : {
						required : '<spring:message code="validation.friday"/>',
						discount :'<spring:message code="validation.discount"/>',
					},
					dSaturday : {
						required :'<spring:message code="validation.saturday"/>',
						discount :'<spring:message code="validation.discount"/>',
					},
					dSunday : {
						required :'<spring:message code="validation.sunday"/>',
						discount :'<spring:message code="validation.discount"/>',
					},
					maxRange : {
						required : "please enter max no",
						greaterThan :"Range should be greater than minimum range "
						
					}
				},
        });
	
	
	$("#update_form").validate(
			{	errorClass:"help-block animation-slideDown",
				errorElement:"div",
				errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
				highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
				success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
				rules:{
					dMonday : {
					required : !0,
					discount : !0
					
				},
				dTuesday : {
					required : !0,
					discount : !0
				},
				dWednesday : {
					required : !0,
					discount : !0
				},
				dThursday : {
					required : !0,
					discount : !0
				},
				dFriday : {
					required : !0,
					discount : !0
				},
				dSaturday : {
					required : !0,
					discount : !0
				},
				dSunday : {
					required : !0,
					discount : !0
				}										
				
				},
				messages:{
					dMonday : {
						required :'<spring:message code="validation.monday"/>',
						discount :'<spring:message code="validation.discount"/>',
					},
					dTuesday : {
						required : '<spring:message code="validation.tuesday"/>',
						discount :'<spring:message code="validation.discount"/>',
					},
					dWednesday : {
						required :'<spring:message code="validation.wednesday"/>',
						discount :'<spring:message code="validation.discount"/>',
					},
					dThursday : {
						required : '<spring:message code="validation.thursday"/>',
						discount :'<spring:message code="validation.discount"/>',
					},
					dFriday : {
						required : '<spring:message code="validation.friday"/>',
						discount :'<spring:message code="validation.discount"/>',
					},
					dSaturday : {
						required :'<spring:message code="validation.saturday"/>',
						discount :'<spring:message code="validation.discount"/>',
					},
					dSunday : {
						required :'<spring:message code="validation.sunday"/>',
						discount :'<spring:message code="validation.discount"/>',
					}
				}
        });
});

function editModal(id){
	$('.data').each(function(){
		this.setAttribute("disabled","disabled");
	});
	
	$('.action').each(function(){
		this.setAttribute("style","display:none");
	});
	
	$('.edit_action').each(function(){
		this.setAttribute("style","display:inline");
	});
	
	$("#edit_"+id).css("display","none");
	$("#cancel_"+id).css("display","inline");
	$("#save_"+id).css("display","inline");
	
	
	
	$('.data_'+id).each(function(){
		this.removeAttribute("disabled");
	});
}

function cancelModal(id){
	$('.data').each(function(){
		this.setAttribute("disabled","disabled");
	});
	
	$('.action').each(function(){
		this.setAttribute("style","display:none");
	});
	
	$('.edit_action').each(function(){
		this.setAttribute("style","display:inline");
	});
}
</script>
</script>

<%@include file="../../inc/template_end.jsp"%>