<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>
<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				<i class="fa fa-map-marker"></i>
				<spring:message code="heading.corporate" />
				<br> <small><spring:message
						code="heading.corporatedetails" /></small>
			</h1>
			<span id="errorMsg"></span>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i> <spring:message code="label.success"/>
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>
			
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">�</button>
					<h4>
						<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>
			
			<c:if test="${!empty domainError}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">�</button>
					<h4>
						<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
					</h4>
					<spring:message code="${domainError}" /> ie. ${domain}
				</div>
			</c:if>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="menu.corporate" /></li>
	</ul>
	<form action="UpdateCorporate" method="post" class="form-horizontal "
		id="Corporate_form" enctype="multipart/form-data">
		<div class="row">
			<input name="corporateId" value="${corporate.corporateId}" type="hidden">
			<input name="userId" value="${corporate.corporateAdmin.userId}"
				type="hidden">
			<div class="col-md-6">
				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.corporateinfo" /></strong>
						</h2>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="corporate_Name"><spring:message
								code="label.corporatename" /><span class="text-danger">*</span> </label>
						<div class="col-md-6">

							<input id="corporate_Name" name="corporateName" class="form-control"
								placeholder="<spring:message code='label.corporatename'/>.."
								type="text" value="${corporate.corporateName}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="example-daterange1"><spring:message
								code="label.selectdaterange" /> </label>
						<div class="col-md-8">

							<label class="col-md-4 control-label" for="example-daterange1">
								${ corporate.compAgreementStart} </label> <label
								class="col-md-4 control-label" for="example-daterange1">
								${ corporate.compAgreementEnd} </label>
						</div>
					</div>


					<div class="form-group">
						<c:if test="${empty corporate.agreementFileUlr  }">
							<security:authorize ifAnyGranted="<%=Role.GORMET7_ADMIN.name()  %>">
								<label class="col-md-4 control-label" for="example-daterange1"><spring:message
										code="label.selectagreement" /> </label>
								<div class="col-md-6">
									<input type="file" name="file" accept="application/pdf"
										size="50" />
								</div>
							</security:authorize>
						</c:if>
						<c:if test="${!empty corporate.agreementFileUlr}">

							<label class="col-md-4 control-label" for="example-daterange1"><spring:message
									code="label.agreementdoc" /> </label>
							<div class="col-md-8">
	                            <p class="form-control-static">${corporate.agreementFileName}
	                            	<a href="<%=documenturl%>${corporate.agreementFileUlr}"
										download="${corporate.agreementFileName}"><spring:message
										code="label.download" />
									</a>
	                            </p>
	                        </div>
						</c:if>

					</div>
					<security:authorize ifAnyGranted="<%=Role.GORMET7_ADMIN.name()  %>">
						<c:if test="${!empty corporate.agreementFileUlr}">
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-daterange1">
								<spring:message code="label.changeagreement"/></label>
							<div class="col-md-6">
								<input type="file" name="file" accept="application/pdf"
									size="50" />
							</div>
						</div>
						</c:if>
					</security:authorize>
					<div class="form-group">
						<label class="col-md-4 control-label" for="domainName"><spring:message
								code="label.domainName" /><span class="text-danger">*</label>
						<div class="col-md-9" style="width: 66%;">
							<textarea id="domainName" name="domainName"
								class="form-control" rows="3"
								placeholder="<spring:message code="label.domainName"/>..">${corporate.domainName}</textarea>
					</div>
			</div>
				</div>
				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.contactinfo" /></strong>
						</h2>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="telephone1"><spring:message
								code="label.telephone1" /><span class="text-danger">*</span></label>
						<div class="col-md-6">

							<input id="corporateTelephone1" name="corporateTelephone1"
								class="form-control"
								placeholder="<spring:message code='label.telephone1'/>.."
								type="text" value="${corporate.corporateTelephone1}" >
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="telephone2"><spring:message
								code="label.telephone2" /></label>
						<div class="col-md-6">

							<input id="corporateTelephone2" name="corporateTelephone2"
								class="form-control"
								placeholder="<spring:message code='label.telephone2'/>.."
								type="text" value="${corporate.corporateTelephone2}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="rest_email"><spring:message
								code="label.corporateemail" /><span class="text-danger">*</span></label>
						<div class="col-md-6">

							<input id="corporate_email" name="corporateEmail"
								class="form-control"
								placeholder="<spring:message code="label.corporateemail"/>.."
								type="text" value="${corporate.corporateEmail}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="mobile1"><spring:message
								code="label.mobile1" /><span class="text-danger">*</span></label>
						<div class="col-md-6">

							<input id="corporateMobile1" name="corporateMobile1"
								class="form-control"
								placeholder="<spring:message code="label.mobile1"/>.."
								type="text" value="${corporate.corporateMobile1}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="mobile2"><spring:message
								code="label.mobile2" /></label>
						<div class="col-md-6">

							<input id="corporateMobile2" name="corporateMobile2"
								class="form-control"
								placeholder="<spring:message code="label.mobile2"/>.."
								type="text" value="${corporate.corporateMobile2}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="addressline1"><spring:message
								code="label.addressline1" /><span class="text-danger">*</span>
						</label>
						<div class="col-md-6">

							<input id="corporateAddressline1" name="corporateAddressLine1"
								class="form-control"
								placeholder="<spring:message code="label.addressline1"/>.."
								type="text" value="${corporate.corporateAddressLine1}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="addressline2"><spring:message
								code="label.addressline2" /></label>
						<div class="col-md-6">

							<input id="corporateAddressline2" name="corporateAddressLine2"
								class="form-control"
								placeholder="<spring:message code="label.addressline2"/>.."
								type="text" value="${corporate.corporateAddressLine2}">
						</div>
					</div>

<!-- 					<div class="form-group"> -->
<%-- 						<label class="col-md-4 control-label" for="area"><spring:message --%>
<%-- 								code="label.areaname" /><span class="text-danger">*</span></label> --%>
<!-- 						<div class="col-md-6"> -->
<!-- 							<select class="area_chosen" style="width: 200px;" -->
<!-- 								id="corporateAreaId" -->
<%-- 								data-placeholder="<spring:message code='label.choosearea' />" --%>
<!-- 								name="corporateAreaId"> -->
<%-- 								<c:forEach items="${areaList}" var="area"> --%>
<%-- 									<c:choose> --%>
<%-- 										<c:when test="${area.active == true}"> --%>
<%-- 											<option value="${area.areaId}" <c:if test="${area.areaId eq corporate.corporateArea.areaId }">selected="selected"</c:if>>${area.areaName}</option>  --%>
<%-- 										</c:when> --%>
<%-- 										<c:otherwise> --%>
<%-- 											<option value="placeholder"><spring:message --%>
<%-- 													code="label.areaname" />.. --%>
<!-- 											</option> -->
<%-- 										</c:otherwise> --%>
<%-- 									</c:choose> --%>
<%-- 								</c:forEach> --%>
<!-- 							</select> -->
<!-- 						</div> -->
<!-- 					</div> -->

<!-- 					<div class="form-group"> -->
<%-- 						<label class="col-md-4 control-label" for="city"><spring:message --%>
<%-- 								code="label.cityname" /><span class="text-danger">*</span></label> --%>
<!-- 						<div class="col-md-6"> -->
<!-- 							<select class="city_chosen" style="width: 200px;" -->
<!-- 								id="corporateCityId" -->
<%-- 								data-placeholder="<spring:message code='label.choosecity' />" --%>
<!-- 								name="corporateCityId"> -->
<%-- 								<c:forEach items="${cityList}" var="city"> --%>
<%-- 									<c:choose> --%>
<%-- 										<c:when test="${city.active == true}"> --%>
<%-- 											<option value="${city.cityId}" <c:if test="${city.cityId eq corporate.corporateCity.cityId }">selected="selected"</c:if>>${city.cityName}</option> --%>
<%--                                         </c:when> --%>

<%-- 										<c:otherwise> --%>
<%-- 											<option value="placeholder"><spring:message --%>
<%-- 													code="label.cityname" />.. --%>
<!-- 											</option> -->
<%-- 										</c:otherwise> --%>
<%-- 									</c:choose> --%>
<%-- 								</c:forEach> --%>
<!-- 							</select> -->
<!-- 						</div> -->
<!-- 					</div> -->

<!-- 					<div class="form-group"> -->
<%-- 						<label class="col-md-4 control-label" for="country"><spring:message --%>
<%-- 								code="label.countryname" /><span class="text-danger">*</span></label> --%>
<!-- 						<div class="col-md-6"> -->
<!-- 							<select class="country_chosen" style="width: 200px;" -->
<!-- 								id="corporateCountryId" -->
<%-- 								data-placeholder="<spring:message code='label.choosecountry' />" --%>
<!-- 								name="corporateCountryId"> -->
<%-- 								<c:forEach items="${countryList}" var="country"> --%>
<%-- 									<c:choose> --%>
<%-- 										<c:when test="${country.active == true}"> --%>
<%-- 											<option value="${country.countryId}" <c:if test="${country.countryId eq corporate.corporateCountry.countryId}">selected="selected"</c:if>>${country.countryName}</option> --%>
<%-- 										</c:when> --%>
<%-- 										<c:otherwise> --%>
<%-- 											<option value="placeholder"><spring:message --%>
<%-- 													code="label.countryname" />.. --%>
<!-- 											</option> -->
<%-- 										</c:otherwise> --%>
<%-- 									</c:choose> --%>
<%-- 								</c:forEach> --%>
<!-- 							</select> -->
<!-- 						</div> -->
<!-- 					</div> -->
					
					
					<div class="form-group" id="countryDiv">
						<label class="col-md-4 control-label" for="country"><spring:message
								code="label.countryname" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<select class="country_chosen" style="width: 200px;"
								id="corporateCountryId"
								data-placeholder="<spring:message code='label.choosecountry' />"
								name="chosenCountryId">
								<c:forEach items="${countryList}" var="country">
									<option value="${country.countryId}"
										<c:if test="${country.countryId eq corporate.corporateCountry.countryId}">selected="selected"</c:if>>${country.countryName}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					
					<div class="form-group" id="cityDiv">
						<label class="col-md-4 control-label" for="city"><spring:message
								code="label.cityname" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<select class="city_chosen" style="width: 200px;"
								id="corporateCityId"
								data-placeholder="<spring:message code='label.choosecity' />"
								name="chosenCityId">
							</select>
						</div>
					</div>

					<div class="form-group" id="areaDiv">
						<label class="col-md-4 control-label" for="area"><spring:message
								code="label.areaname" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<select class="area_chosen" style="width: 200px;"
								id="corporateAreaId"
								data-placeholder="<spring:message code='label.choosearea' />"
								name="chosenAreaId">
							</select>
						</div>
					</div>
					
					

					<div class="form-group">
						<label class="col-md-4 control-label" for="corporatePincode"><spring:message
								code="label.pincode" /><span class="text-danger">*</span></label>
						<div class="col-md-6">

							<input id="corporatePincode" name="corporatePincode"
								class="form-control"
								placeholder="<spring:message code="label.pincode"/>.."
								type="text" value="${corporate.corporatePincode}">
						</div>
					</div>




				</div>
				
			</div>
			<div class="col-md-6">
				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.admininfo" /></strong>
						</h2>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="first_Name"><spring:message
								code="label.firstname" /><span class="text-danger">*</span> </label>
						<div class="col-md-6">

							<input id="first_Name" name="firstName" class="form-control"
								placeholder="<spring:message code='label.firstname'/>.."
								type="text" value="${corporate.corporateAdmin.firstName}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="last_Name"><spring:message
								code="label.lastname" /><span class="text-danger">*</span> </label>
						<div class="col-md-6">

							<input id="last_Name" name="lastName" class="form-control"
								placeholder="<spring:message code="label.lastname"/>.."
								type="text" value="${corporate.corporateAdmin.lastName}">
						</div>
					</div>


					<div class="form-group">
						<label class="col-md-4 control-label" for="email"><spring:message
								code="label.emailid" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<input id="email" name="emailId" class="form-control"
								placeholder="test@example.com" type="text"
								value="${corporate.corporateAdmin.emailId}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="mobile"><spring:message
								code="label.mobile" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<input id="mobile" name="mobile" class="form-control"
								placeholder='<spring:message code="label.mobile"></spring:message>'
								type="text" value="${corporate.corporateAdmin.mobile}">
						</div>
					</div>
					<div class="form-group">
                        <label class="col-md-4 control-label"><spring:message code="label.userlogin"/></label>
                        <div class="col-md-8">
                            <p class="form-control-static">${corporate.corporateAdmin.loginId}</p>
                        </div>
                    </div>
					<div class="form-group">
						<a href="#reset-user-settings" data-toggle="modal"
							class="col-md-4 control-label"><spring:message code="label.resetpassword"/></a>
					</div>
				</div>
			</div>
		</div>
		<div class="block">
			<div style="text-align: center; margin-bottom: 10px">
				<button id="corporate_submit" class="btn btn-sm btn-primary save" type="submit">
					<i class="fa fa-angle-right"></i> <spring:message code="button.save" />
				</button>
				<security:authorize ifAnyGranted="<%=Role.GORMET7_ADMIN.name() %>">
					<a href="<%=contexturl %>ManageCorporate/CorporateList/"  class="btn btn-sm btn-primary" style="text-decoration: none;"><i class="gi gi-remove"></i> <spring:message code="button.cancel"/></a>
				</security:authorize>
				
				<security:authorize ifAnyGranted="<%=Role.CORPORATE_ADMIN.name() %>">
				 <a href="<%=contexturl %>ManageCorporate/ViewCorporate?id=${corporate.corporateId}"  class="btn btn-sm btn-primary" style="text-decoration: none;"><i class="gi gi-remove"></i> <spring:message code="button.cancel"/></a>
				</security:authorize>
			</div>
		</div>
	</form>

	<div id="reset-user-settings" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header text-center">
					<h2 class="modal-title">
						<i class="fa fa-pencil"></i> <spring:message code="label.resetpassword"/>
					</h2>
				</div>
				<span id="passwordErrorMsg"></span>
				<div class="modal-body">
					<form action="<%=contexturl %>ManageCorporate/CorporateList/ResetCorporateAdminPassword" method="post" class="form-horizontal form-bordered"
						id="ResetPassword_Form">
						<input name="corporateId" value="${corporate.corporateId}" type="hidden">
						<input name="userId" value="${corporate.corporateAdmin.userId}"
							type="hidden">
						<fieldset>
							<div class="form-group">
								<label class="col-md-4 control-label"
									for="user-settings-password"><spring:message code="label.newpassword"/></label>
								<div class="col-md-8">
									<input type="password" id="password" name="password"
										class="form-control"
										placeholder="Please choose a complex one..">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label"
									for="user-settings-repassword"><spring:message code="label.confirmnewpassword"/></label>
								<div class="col-md-8">
									<input type="password" id="user-settings-repassword"
										name="confirmPassword" class="form-control"
										placeholder="..and confirm it!">
								</div>
							</div>
						</fieldset>
						<div class="form-group form-actions">
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal"><spring:message code="label.close"/></button>
								<button id="submit_password" class="btn btn-sm btn-primary save" type="submit"><spring:message code="label.savechanges"/>
									</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<%@include file="../../inc/chosen_scripts.jsp"%>
<script
	src="../resources/js/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>

<script type="text/javascript">
	$(document)
			.ready(
					function() {
						$(".area_chosen").chosen();
						$(".city_chosen").chosen();
						$(".country_chosen").chosen();
						
						$('select[name="chosenCountryId"]').chosen().change( function() {
							countryId = ($(this).val());
							$.ajax({
								type: "GET",
								async: false,
								url: "<%=contexturl %>city/citybycountryid/"+countryId,
								success: function(data, textStatus, jqXHR){
									var obj = jQuery.parseJSON(data);
									var len=obj.length;
									var myOptions = "<option value></option>";
									var myAreaOptions = "<option value></option>";
									for(var i=0; i<len; i++){
									myOptions += '<option value="'+obj[i].cityId+'">'+obj[i].cityName+'</option>';
									}
									$(".city_chosen").html(myOptions).chosen().trigger("chosen:updated");
									$(".area_chosen").html(myAreaOptions).chosen().trigger("chosen:updated");
								},
								dataType: 'html'
							});
						});
						
						
						$('select[name="chosenCityId"]').chosen().change( function() {
							cityId = ($(this).val());
							$.ajax({
								type: "GET",
								async: false,
								url: "<%=contexturl %>area/areabycityid/"+cityId,
								success: function(data, textStatus, jqXHR){
									var obj = jQuery.parseJSON(data);
									var len=obj.length;
									var myOptions = "<option value></option>";
									for(var i=0; i<len; i++){
									myOptions += '<option value="'+obj[i].areaId+'">'+obj[i].areaName+'</option>';
									}
									$(".area_chosen").html(myOptions).chosen().trigger("chosen:updated");
								},
								dataType: 'html'
							});
						});
						
						
             changeCountry($("#corporateCountryId").val());
						
						function changeCountry(countryId){
							$.ajax({
								type: "GET",
								async: false,
								url: "<%=contexturl %>city/citybycountryid/"+countryId,
								success: function(data, textStatus, jqXHR){
									var obj = jQuery.parseJSON(data);
									var len=obj.length;
									var myOptions = "<option value></option>";
									for(var i=0; i<len; i++){
										if(${corporate.corporateCity.cityId} != obj[i].cityId){
											myOptions += '<option value="'+obj[i].cityId+'">'+obj[i].cityName+'</option>';
										}
										else{
											myOptions += '<option value="'+obj[i].cityId+'" selected="selected">'+obj[i].cityName+'</option>';
										}
									}
									$(".city_chosen").html(myOptions).chosen().trigger("chosen:updated");
									changeCity($("#corporateCityId").val());
								},
								dataType: 'html'
							});
						}
						
						function changeCity(cityId){
							$.ajax({
								type: "GET",
								async: false,
								url: "<%=contexturl %>area/areabycityid/"+cityId,
								success: function(data, textStatus, jqXHR){
									var obj = jQuery.parseJSON(data);
									var len=obj.length;
									var myOptions = "<option value></option>";
									for(var i=0; i<len; i++){
										if(${corporate.corporateArea.areaId} != obj[i].areaId){
											myOptions += '<option value="'+obj[i].areaId+'">'+obj[i].areaName+'</option>';
										}
										else{
											myOptions += '<option value="'+obj[i].areaId+'" selected="selected">'+obj[i].areaName+'</option>';
										}
									}
									$(".area_chosen").html(myOptions).chosen().trigger("chosen:updated");
								},
								dataType: 'html'
							});
						}
						
						
						
						
						$("#Corporate_form")
								.validate(
										{
											errorClass : "help-block animation-slideDown",
											errorElement : "div",
											errorPlacement : function(e, a) {
												a.parents(".form-group > div")
														.append(e)
											},
											highlight : function(e) {
												$(e)
														.closest(".form-group")
														.removeClass(
																"has-success has-error")
														.addClass("has-error")
											},
											success : function(e) {
												e
														.closest(".form-group")
														.removeClass(
																"has-success has-error")
											},
											rules : {
												corporateName : {
													required : !0,maxlength : 75
												},
												firstName : {
													required : !0,maxlength : 75
												},
												lastName : {
													required : !0,maxlength : 75
												},
												emailId : {
													required : !0,
													email : !0,maxlength : 75
												},
												mobile : {
													required : !0,
													phone : !0,maxlength : 75
												},
												corporateTelephone1 : {
													required : !0,
													phone : !0,maxlength : 75
												},
												corporateTelephone2 : {
													phone : !0,maxlength : 75
												},
												corporateEmail : {
													required : !0,
													email : !0,maxlength : 75
												},
												chosenCityId : {
													required : !0
												},
												chosenCountryId : {
													required : !0
												},
												chosenAreaId : {
													required : !0
												},
												corporateMobile1 : {
													required : !0,
													phone : !0,maxlength : 75
												},
												corporateMobile2 : {
													phone : !0,maxlength : 75
												},
												corporateAddressLine1 : {
													required : !0,maxlength : 75
												},
												corporateAddressLine2 : {
													maxlength : 75
												},
												corporatePincode : {
													required : !0,
													digits : !0,maxlength : 10
												},
												
												domainName : {
													domain:!0,
													required : !0
												},
											},
											messages : {
												corporateName : {
													required : '<spring:message code="validation.pleaseentercorporatename"/>',
													maxlength :'<spring:message code="validation.corporatename75character"/>'
												},
												firstName : {
													required : '<spring:message code="validation.pleaseenterfirstname"/>',
													maxlength :'<spring:message code="validation.firstname75character"/>'
												},
												lastName : {
													required :'<spring:message code="validation.pleaseenterlastname"/>',
													maxlength :'<spring:message code="validation.lastname75character"/>'
												},
												emailId : {
													required :'<spring:message code="validation.pleaseenteremailid"/>',
													email:'<spring:message code="validation.pleaseenteravalidemailid"/>',
													maxlength : '<spring:message code="validation.email75character"/>'
												},
												mobile : {
													required : '<spring:message code="validation.pleaseenteramobilenumber"/>',
													phone :'<spring:message code="validation.phone"/>',
													maxlength :'<spring:message code="validation.mobile75character"/>'
												},
												corporateTelephone1 : {
													required : '<spring:message code="validation.pleaseenteratelephonenumber"/>',
													phone :'<spring:message code="validation.phone"/>',
													maxlength :'<spring:message code="validation.telephone75character"/>'
												},
												corporateTelephone2 : {

													phone :'<spring:message code="validation.phone"/>',
												    maxlength :'<spring:message code="validation.telephone75character"/>'
												},
												corporateEmail : {
													required :'<spring:message code="validation.pleaseenteremailid"/>',
													email:'<spring:message code="validation.pleaseenteravalidemailid"/>',
													maxlength : '<spring:message code="validation.email75character"/>'
												},
												chosenCityId : {
													required : '<spring:message code="validation.selectcity"/>'
												},
												chosenCountryId : {
													required : '<spring:message code="validation.selectcountry"/>'
												},
												chosenAreaId : {
													required : '<spring:message code="validation.selectarea"/>'
												},
												corporateMobile1 : {
													required :'<spring:message code="validation.pleaseenteramobilenumber"/>',
													phone :'<spring:message code="validation.phone"/>',
													maxlength :'<spring:message code="validation.mobile75character"/>'
												},
												companMobile2 : {

													phone :'<spring:message code="validation.phone"/>',
													maxlength :'<spring:message code="validation.mobile75character"/>'
												},
												corporateAddressLine1 : {
													required :'<spring:message code="validation.pleaseenteraddressline1"/>',
													maxlength :'<spring:message code="validation.corporateaddress"/>'
												},
												corporateAddressLine2 : {
													maxlength :'<spring:message code="validation.corporateaddress"/>'
												},
												corporatePincode : {
													required :'<spring:message code="validation.pleaseenterpincode"/>',
													digits :'<spring:message code="validation.digits"/>',
													maxlength :'<spring:message code="validation.pincode75character"/>'
												},
												domainName : {
													domain :  '<spring:message code="validation.pleaseentervaliddomain"/>',
													required : '<spring:message code="validation.pleaseenterdomain"/>'
												},
											}
										});

						$("#ResetPassword_Form")
								.validate(
										{
											errorClass : "help-block animation-slideDown",
											errorElement : "div",
											errorPlacement : function(e, a) {
												a.parents(".form-group > div")
														.append(e)
											},
											highlight : function(e) {
												$(e)
														.closest(".form-group")
														.removeClass(
																"has-success has-error")
														.addClass("has-error")
											},
											success : function(e) {
												e
														.closest(".form-group")
														.removeClass(
																"has-success has-error")
											},
											rules : {
												password : {
													required : !0,
													minlength : 6,maxlength : 75
												},
												confirmPassword : {
													required : !0,
													equalTo : "#password"
												}
											},
											messages : {
												password : {
													required :'<spring:message code="validation.pleaseenterapassword"/>',
													minlength:'<spring:message code="validation.passwordmustbeatleast6character"/>',
													maxlength :'<spring:message code="validation.passworddname75character"/>'
													},
												},
												confirmPassword : {
													required : '<spring:message code="validation.pleaseconfirmpassword"/>',
													equalTo:'<spring:message code="validation.pleaseenterthesamepasswordasabove"/>'
												},
											
										});

					});
</script>