<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>

<div id="page-content">
<div class="content-header">
	<div class="header-section">
		<h1>
			<i class="fa fa-map-marker"></i> <spring:message code="heading.brandmaster" /><br> <small>You can add, remove or edit your brands here.
				</small>
		</h1>
		<c:if test="${!empty success}">
			<div class="alert alert-success alert-dismissable">
				<button type="button" class="close" data-dismiss="alert"
					aria-hidden="true">x</button>
				<h4>
					<i class="fa fa-check-circle"></i> <spring:message code="label.success"/>
				</h4>
				<spring:message code="${success}" />
			</div>
		</c:if>
		<c:if test="${!empty error}">
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert"
					aria-hidden="true">x</button>
				<h4>
					<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
				</h4>
				${error}
			</div>
		</c:if>
	</div>
</div>
<ul class="breadcrumb breadcrumb-top">
	<li><spring:message code="heading.managebrand" /></li>
	<li><a href="#"><spring:message code="label.brand" /></a></li>
</ul>
<div class="block full ">
		<a href="<%=contexturl %>ManageBrand/BrandList/AddBrand" id="addBrand" class="btn btn-sm btn-primary"><spring:message code="label.addbrand" /></a>
		<small >You can add new brand here</small>
</div>
<div class="block full gridView">
	<div class="block-title">
		<h2>
			<strong><spring:message code="heading.activebrand" /></strong> 
			<small style="float:left; margin-top: 4px;">Your current active brands with Gourmet7. you can make any changes in these brands and also make them inactive if they are no longer operational by clicking Action button on the extreme right along with the brand details</small>
		</h2>
	</div>
	
	<div class="table-responsive">
		<table id="active-datatable"
			class="table table-vcenter table-condensed table-bordered">
			<thead>
				<tr>
					<th class="text-center"><spring:message code="label.id" /></th>
					<th class="text-center"><spring:message code="label.brand" /></th>
					<th class="text-center"><spring:message code="label.company" /></th>
					<th class="text-center"><spring:message code="label.admin" /></th>
					<th class="text-center"><spring:message code="label.actions" /></th>
				</tr>
			</thead>
			<tbody id="tbody">
				<c:forEach var="currentBrand" items="${brandList}" varStatus="count">

					<tr>
						<td class="text-center">${count.count}</td>
						<td class="text-left"><c:out
								value="${currentBrand.brandName}" /></td>
						<td class="text-left"><c:out
								value="${currentBrand.company.companyName}" /></td>
						<td class="text-left"><c:out
								value="${currentBrand.brandAdmin.fullName}" /></td>
						<td class="text-center">
							<div class="btn-group">
								<a href="<%=contexturl %>ManageBrand/BrandList/EditBrand?id=${currentBrand.brandId}" data-toggle="tooltip" title="Edit"
									
									class="btn btn-xs btn-default concurrency"><i class="fa fa-pencil"></i></a>
								<a href="#delete_brand_popup" data-toggle="modal"
									onclick="deleteBrand(${currentBrand.brandId})" title="Inactiv Brand"
									class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
							</div>
						</td>
					</tr>
				</c:forEach>

			</tbody>
		</table>
	</div>
</div>


	<div class="block full gridView">
		<div class="block-title">
			<h2>
				<strong><spring:message code="heading.inactivebrand" /></strong>
				<small style="float:left; margin-top: 4px;">Inactive brands and restaurants under them don't appear for user search and you will not be able to add any new restaurant under inactive brand. If you wish to re-activate any of the brands, you can do so by clicking + sign on the extreme right along with the brand</small>
			</h2>
		</div>
		<div class="table-responsive">
			<table id="inactive-datatable"
				class="table table-vcenter table-condensed table-bordered ">
				<thead>
					<tr>
						<th class="text-center"><spring:message code="label.id" /></th>
						<th class="text-center"><spring:message code="label.company" /></th>
						<th class="text-center"><spring:message code="label.brand" /></th>
						<th class="text-center"><spring:message code="label.admin" /></th>
						<th class="text-center"><spring:message code="label.actions" /></th>
					</tr>
				</thead>
				<tbody id="tablebody">
					<c:forEach var="currentBrand" items="${inactiveBrandList}"
						varStatus="count">
						<tr>
							<td class="text-center">${count.count}</td>
							<td class="text-left"><c:out value="${currentBrand.brandName}" /></td>
						<td class="text-left"><c:out value="${currentBrand.company.companyName}" /></td>
						<td class="text-left"><c:out 	value="${currentBrand.brandAdmin.fullName}" /></td>
							<td class="text-center">
								<div class="btn-group">
									<a href="#restore_Brand_popup" data-toggle="modal"
										onclick="restoreBrand(${currentBrand.brandId})"
										title="Restore Brand" class="btn btn-xs btn-success"><i
										class="fa fa-plus"></i> </a>
								</div>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>



<div id="delete_brand_popup" class="modal fade" tabindex="-1"
	role="dialog" aria-hidden="true" >
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<form action="<%=contexturl %>ManageBrand/BrandList/DeleteBrand" method="post" class="form-horizontal form-bordered"
					id="delete_brand_Form">
						
					
					<div style="padding: 10px; height: 110px;">
						<label><spring:message code="validation.doyouwanttodeletethisbrand"/></label><br>
						<label><spring:message code="validation.deletingthisbrandwilldeleterestuarantandthereuser"/></label>
						<div class="col-xs-12 text-right">
							<button type="button" class="btn btn-sm btn-default"
								data-dismiss="modal"><spring:message code="label.no"/></button>
							<div id="delete_Brand" class="btn btn-sm btn-primary"><spring:message code="label.yes"/></div>
						</div>
					</div>
                      <input type="hidden" name="id" id="deletebrandId">
				</form>

			</div>
		</div>
	</div>
</div>

<div id="restore_Brand_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="<%=contexturl %>ManageBrand/BrandList/RestoreBrand" method="post" class="form-horizontal form-bordered" id="restore_brand_Form">
						
						<div style="padding: 10px; height: 110px;">
							<label><spring:message
									code="validation.doyouwanttorestorethisbrand" /></label><br>
							<label><spring:message code="validation.restorebrandwillrestoreallpreviouslydeletedrestaurantanduserofbrand" /></label>
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal"><spring:message code="label.no" /></button>
								<div id="restore_Brand" class="btn btn-sm btn-primary">	<spring:message code="label.yes" /></div>
							</div>
						</div>
						<input type="hidden"  name="id" id="restorebrandId">
					</form>
				</div>
			</div>
		</div>
	</div>

</div>

	<%@include file="../../inc/page_footer.jsp"%>
       <%@include file="../../inc/template_scripts.jsp"%>

<script src="<%=contexturl %>resources/js/pages/activeTablesDatatables.js"></script>
<script src="<%=contexturl %>resources/js/pages/inactiveTablesDatatables.js"></script>
<script>$(function(){ ActiveTablesDatatables.init(4); });</script>
<script>$(function(){ InActiveTablesDatatables.init(4); });</script>


<script type="text/javascript">
	var selectedId = 0;
	$(document).ready(function() {
// 		$("#addBrand").click(function(){
// 			$.ajax({
// 				type: "GET",
// 				async: false,
// 				url: "AddBrand",
// 				success: function(data, textStatus, jqXHR){
// 					$('#page-content').html(data);
// 				},
// 				dataType: 'html'
// 			});
// 		});
		
		
        $("#restore_Brand").click(function(){
			
			$("#restorebrandId").val(restoreId);
			$("#restore_brand_Form").submit();
		});
		
         $("#delete_Brand").click(function(){
			
			$("#deletebrandId").val(selectedId);
			$("#delete_brand_Form").submit();
		});
		
// 		$("#delete_brand").click(function(){
// 			$.ajax({
// 				type: "GET",
// 				async: false,
// 				url: "DeleteBrand?id="+selectedId,
// 				success: function(data, textStatus, jqXHR){
// 					$('#page-content').html(data);
// 					$('body').removeClass('modal-open');
// 					$('.modal-backdrop').remove();
// 					$('#delete_brand_popup').modal('hide');	
// 				},
// 				dataType: 'html'
// 			});
// 		});
	});
	
// 	function editBrand(id){
// 		$.ajax({
// 			type: "GET",
// 			async: false,
// 			url: "EditBrand?id="+id,
// 			success: function(data, textStatus, jqXHR){
// 				$('#page-content').html(data);
// 			},
// 			dataType: 'html'
// 		});
// 	}
	
	function deleteBrand(id){
		selectedId = id;
	}
	
	function restoreBrand(id){
		restoreId = id;
	}
</script>
<%@include file="../../inc/template_end.jsp"%>