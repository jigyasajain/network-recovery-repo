<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->




<%@page import="java.util.Date"%>
<%@include file="../inc/config.jsp"%>
<%@include file="../inc/template_start.jsp"%>
<%-- <%@include file="../inc/page_head.jsp"%> --%>
<style>
.feedback_profile {
	height: 30px;
	width: 30px !important;
}

.ratinglable {
	float: left;
}

.row {
	margin-bottom: 5px;
}

.col-md-2 {
	width: 100% !important;
}

.ratingwid {
	width: 269px;
}

.rating_value {
	width: 100% !important;
}
</style>
<style type="text/css">
        .db-icon {
            padding: 10px 5px;
        }
        .db-icon img {
            border: thin solid #ccc;
        }
        .db-icon a:hover{
            opacity: .5;
        }
 </style>
<!-- Page content -->
<div id="sidebar-alt">
            <!-- Wrapper for scrolling functionality -->
            <div class="sidebar-scroll">
                <!-- Sidebar Content -->
                <div class="sidebar-content">
                    <!-- Chat -->
                    <!-- Chat demo functionality initialized in js/app.js -> chatUi() -->
                    <a href="javascript:void(0)" class="sidebar-title">
                        <i class="gi gi-comments pull-right"></i>  <strong>Chat</strong>UI
                    </a>
                    <!-- Chat Users -->
                    <ul class="chat-users clearfix">
                        <li>
                            <a href="javascript:void(0)" class="chat-user-online">
                                <span></span>
                                <img src="<%=contexturl%>resources/img/placeholders/avatars/avatar12.jpg" alt="avatar" class="img-circle">
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" class="chat-user-online">
                                <span></span>
                                <img src="<%=contexturl%>resources/img/placeholders/avatars/avatar15.jpg" alt="avatar" class="img-circle">
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" class="chat-user-online">
                                <span></span>
                                <img src="<%=contexturl%>resources/img/placeholders/avatars/avatar10.jpg" alt="avatar" class="img-circle">
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" class="chat-user-online">
                                <span></span>
                                <img src="<%=contexturl%>resources/img/placeholders/avatars/avatar4.jpg" alt="avatar" class="img-circle">
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" class="chat-user-away">
                                <span></span>
                                <img src="<%=contexturl%>resources/img/placeholders/avatars/avatar7.jpg" alt="avatar" class="img-circle">
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" class="chat-user-away">
                                <span></span>
                                <img src="<%=contexturl%>resources/img/placeholders/avatars/avatar9.jpg" alt="avatar" class="img-circle">
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" class="chat-user-busy">
                                <span></span>
                                <img src="<%=contexturl%>resources/img/placeholders/avatars/avatar16.jpg" alt="avatar" class="img-circle">
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <span></span>
                                <img src="<%=contexturl%>resources/img/placeholders/avatars/avatar1.jpg" alt="avatar" class="img-circle">
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <span></span>
                                <img src="<%=contexturl%>resources/img/placeholders/avatars/avatar4.jpg" alt="avatar" class="img-circle">
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <span></span>
                                <img src="<%=contexturl%>resources/img/placeholders/avatars/avatar3.jpg" alt="avatar" class="img-circle">
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <span></span>
                                <img src="<%=contexturl%>resources/img/placeholders/avatars/avatar13.jpg" alt="avatar" class="img-circle">
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <span></span>
                                <img src="<%=contexturl%>resources/img/placeholders/avatars/avatar5.jpg" alt="avatar" class="img-circle">
                            </a>
                        </li>
                    </ul>
                    <!-- END Chat Users -->

                    <!-- Chat Talk -->
                    <div class="chat-talk display-none">
                        <!-- Chat Info -->
                        <div class="chat-talk-info sidebar-section">
                            <img src="img/placeholders/avatars/avatar5.jpg" alt="avatar" class="img-circle pull-left">
                            <strong>John</strong> Doe
                            <button id="chat-talk-close-btn" class="btn btn-xs btn-default pull-right">
                                <i class="fa fa-times"></i>
                            </button>
                        </div>
                        <!-- END Chat Info -->

                        <!-- Chat Messages -->
                        <ul class="chat-talk-messages">
                            <li class="text-center"><small>Yesterday, 18:35</small>
                            </li>
                            <li class="chat-talk-msg animation-slideRight">Hey admin?</li>
                            <li class="chat-talk-msg animation-slideRight">How are you?</li>
                            <li class="text-center"><small>Today, 7:10</small>
                            </li>
                            <li class="chat-talk-msg chat-talk-msg-highlight themed-border animation-slideLeft">I'm fine, thanks!</li>
                        </ul>
                        <!-- END Chat Messages -->

                        <!-- Chat Input -->
                        <form action="index.html" method="post" id="sidebar-chat-form" class="chat-form">
                            <input type="text" id="sidebar-chat-message" name="sidebar-chat-message" class="form-control form-control-borderless" placeholder="Type a message..">
                        </form>
                        <!-- END Chat Input -->
                    </div>
                    <!--  END Chat Talk -->
                    <!-- END Chat -->

                    <!-- Activity -->
                    <a href="javascript:void(0)" class="sidebar-title">
                        <i class="fa fa-globe pull-right"></i>  <strong>Activity</strong>UI
                    </a>
                    <div class="sidebar-section">
                        <div class="alert alert-danger alert-alt">
                            <small>just now</small>
                            <br>
                            <i class="fa fa-thumbs-up fa-fw"></i> Upgraded to Pro plan
                        </div>
                        <div class="alert alert-info alert-alt">
                            <small>2 hours ago</small>
                            <br>
                            <i class="gi gi-coins fa-fw"></i> You had a new sale!
                        </div>
                        <div class="alert alert-success alert-alt">
                            <small>3 hours ago</small>
                            <br>
                            <i class="fa fa-plus fa-fw"></i>  <a href="page_ready_user_profile.html"><strong>John Doe</strong></a> would like to become friends!
                            <br>
                            <a href="javascript:void(0)" class="btn btn-xs btn-primary"><i class="fa fa-check"></i> Accept</a>
                            <a href="javascript:void(0)" class="btn btn-xs btn-default"><i class="fa fa-times"></i> Ignore</a>
                        </div>
                        <div class="alert alert-warning alert-alt">
                            <small>2 days ago</small>
                            <br>Running low on space
                            <br><strong>18GB in use</strong> 2GB left
                            <br>
                            <a href="page_ready_pricing_tables.html" class="btn btn-xs btn-primary"><i class="fa fa-arrow-up"></i> Upgrade Plan</a>
                        </div>
                    </div>
                    <!-- END Activity -->

                    <!-- Messages -->
                    <a href="page_ready_inbox.html" class="sidebar-title">
                        <i class="fa fa-envelope pull-right"></i>  <strong>Messages</strong>UI (5)
                    </a>
                    <div class="sidebar-section">
                        <div class="alert alert-alt">
                            Debra Stanley<small class="pull-right">just now</small>
                            <br>
                            <a href="page_ready_inbox_message.html"><strong>New Follower</strong></a>
                        </div>
                        <div class="alert alert-alt">
                            Sarah Cole<small class="pull-right">2 min ago</small>
                            <br>
                            <a href="page_ready_inbox_message.html"><strong>Your subscription was updated</strong></a>
                        </div>
                        <div class="alert alert-alt">
                            Bryan Porter<small class="pull-right">10 min ago</small>
                            <br>
                            <a href="page_ready_inbox_message.html"><strong>A great opportunity</strong></a>
                        </div>
                        <div class="alert alert-alt">
                            Jose Duncan<small class="pull-right">30 min ago</small>
                            <br>
                            <a href="page_ready_inbox_message.html"><strong>Account Activation</strong></a>
                        </div>
                        <div class="alert alert-alt">
                            Henry Ellis<small class="pull-right">40 min ago</small>
                            <br>
                            <a href="page_ready_inbox_message.html"><strong>You reached 10.000 Followers!</strong></a>
                        </div>
                    </div>
                    <!-- END Messages -->
                </div>
                <!-- END Sidebar Content -->
            </div>
            <!-- END Wrapper for scrolling functionality -->
        </div>
        <!-- END Alternative Sidebar -->

        <!-- Main Sidebar -->
        <div id="sidebar">
            <!-- Wrapper for scrolling functionality -->
            <div class="sidebar-scroll">
                <!-- Sidebar Content -->
                <div class="sidebar-content">
                    <!-- Brand -->
                    <a href="index.html" class="sidebar-brand">
                        <i class="gi gi-flash"></i><strong>Pro</strong>UI
                    </a>
                    <!-- END Brand -->

                    <!-- User Info -->
                    <div class="sidebar-section sidebar-user clearfix">
                        <div class="sidebar-user-avatar">
                            <a href="page_ready_user_profile.html">
                                <img src="<%=contexturl%>resources/img/placeholders/avatars/avatar2.jpg" alt="avatar">
                            </a>
                        </div>
                        <div class="sidebar-user-name">John Doe</div>
                        <div class="sidebar-user-links">
                            <a href="page_ready_user_profile.html" data-toggle="tooltip" data-placement="bottom" title="Profile"><i class="gi gi-user"></i></a>
                            <a href="page_ready_inbox.html" data-toggle="tooltip" data-placement="bottom" title="Messages"><i class="gi gi-envelope"></i></a>
                            <!-- Opens the user settings modal that can be found at the bottom of each page (page_footer.html in PHP version) -->
                            <a href="#modal-user-settings" data-toggle="modal" class="enable-tooltip" data-placement="bottom" title="Settings"><i class="gi gi-cogwheel"></i></a>
                            <a href="login.html" data-toggle="tooltip" data-placement="bottom" title="Logout"><i class="gi gi-exit"></i></a>
                        </div>
                    </div>
                    <!-- END User Info -->

                    <!-- Theme Colors -->
                    <!-- Change Color Theme functionality can be found in js/app.js - templateOptions() -->
                    <ul class="sidebar-section sidebar-themes clearfix">
                        <li class="active">
                            <a href="javascript:void(0)" class="themed-background-dark-default themed-border-default" data-theme="default" data-toggle="tooltip" title="Default Blue"></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" class="themed-background-dark-night themed-border-night" data-theme="css/themes/night.css" data-toggle="tooltip" title="Night"></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" class="themed-background-dark-amethyst themed-border-amethyst" data-theme="css/themes/amethyst.css" data-toggle="tooltip" title="Amethyst"></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" class="themed-background-dark-modern themed-border-modern" data-theme="css/themes/modern.css" data-toggle="tooltip" title="Modern"></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" class="themed-background-dark-autumn themed-border-autumn" data-theme="css/themes/autumn.css" data-toggle="tooltip" title="Autumn"></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" class="themed-background-dark-flatie themed-border-flatie" data-theme="css/themes/flatie.css" data-toggle="tooltip" title="Flatie"></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" class="themed-background-dark-spring themed-border-spring" data-theme="css/themes/spring.css" data-toggle="tooltip" title="Spring"></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" class="themed-background-dark-fancy themed-border-fancy" data-theme="css/themes/fancy.css" data-toggle="tooltip" title="Fancy"></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" class="themed-background-dark-fire themed-border-fire" data-theme="css/themes/fire.css" data-toggle="tooltip" title="Fire"></a>
                        </li>
                    </ul>
                    <!-- END Theme Colors -->

                    <!-- Sidebar Navigation -->
                    <ul class="sidebar-nav">
                        <li>
                            <a href="index.html"><i class="gi gi-stopwatch sidebar-nav-icon"></i>Dashboard</a>
                        </li>
                        <li class="sidebar-header">
                            <span class="sidebar-header-options clearfix"><a href="javascript:void(0)" data-toggle="tooltip" title="Quick Settings"><i class="gi gi-settings"></i></a><a href="javascript:void(0)" data-toggle="tooltip" title="Create the most amazing pages with the widget kit!"><i class="gi gi-lightbulb"></i></a></span>
                            <span class="sidebar-header-title">Widget Kit</span>
                        </li>
                        <li>
                            <a href="page_widgets_stats.html" class=" active"><i class="gi gi-charts sidebar-nav-icon"></i>Statistics</a>
                        </li>
                        <li>
                            <a href="page_widgets_social.html"><i class="gi gi-share_alt sidebar-nav-icon"></i>Social</a>
                        </li>
                        <li>
                            <a href="page_widgets_media.html"><i class="gi gi-film sidebar-nav-icon"></i>Media</a>
                        </li>
                        <li class="sidebar-header">
                            <span class="sidebar-header-options clearfix"><a href="javascript:void(0)" data-toggle="tooltip" title="Quick Settings"><i class="gi gi-settings"></i></a></span>
                            <span class="sidebar-header-title">Design Kit</span>
                        </li>
                        <li>
                            <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator"></i><i class="gi gi-certificate sidebar-nav-icon"></i>User Interface</a>
                            <ul>
                                <li>
                                    <a href="page_ui_grid_blocks.html">Grid &amp; Blocks</a>
                                </li>
                                <li>
                                    <a href="page_ui_typography.html">Typography</a>
                                </li>
                                <li>
                                    <a href="page_ui_buttons_dropdowns.html">Buttons &amp; Dropdowns</a>
                                </li>
                                <li>
                                    <a href="page_ui_navigation_more.html">Navigation &amp; More</a>
                                </li>
                                <li>
                                    <a href="page_ui_horizontal_menu.html">Horizontal Menu</a>
                                </li>
                                <li>
                                    <a href="page_ui_progress_loading.html">Progress &amp; Loading</a>
                                </li>
                                <li>
                                    <a href="page_ui_color_themes.html">Color Themes</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator"></i><i class="gi gi-notes_2 sidebar-nav-icon"></i>Forms</a>
                            <ul>
                                <li>
                                    <a href="page_forms_general.html">General</a>
                                </li>
                                <li>
                                    <a href="page_forms_components.html">Components</a>
                                </li>
                                <li>
                                    <a href="page_forms_validation.html">Validation</a>
                                </li>
                                <li>
                                    <a href="page_forms_wizard.html">Wizard</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator"></i><i class="gi gi-table sidebar-nav-icon"></i>Tables</a>
                            <ul>
                                <li>
                                    <a href="page_tables_general.html">General</a>
                                </li>
                                <li>
                                    <a href="page_tables_responsive.html">Responsive</a>
                                </li>
                                <li>
                                    <a href="page_tables_datatables.html">Datatables</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator"></i><i class="gi gi-cup sidebar-nav-icon"></i>Icon Sets</a>
                            <ul>
                                <li>
                                    <a href="page_icons_fontawesome.html">Font Awesome</a>
                                </li>
                                <li>
                                    <a href="page_icons_glyphicons_pro.html">Glyphicons Pro</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator"></i><i class="gi gi-show_big_thumbnails sidebar-nav-icon"></i>Page Layouts</a>
                            <ul>
                                <li>
                                    <a href="page_layout_static.html">Static</a>
                                </li>
                                <li>
                                    <a href="page_layout_static_fixed_footer.html">Static + Fixed Footer</a>
                                </li>
                                <li>
                                    <a href="page_layout_fixed_top.html">Fixed Top Header</a>
                                </li>
                                <li>
                                    <a href="page_layout_fixed_top_footer.html">Fixed Top Header + Footer</a>
                                </li>
                                <li>
                                    <a href="page_layout_fixed_bottom.html">Fixed Bottom Header</a>
                                </li>
                                <li>
                                    <a href="page_layout_fixed_bottom_footer.html">Fixed Bottom Header + Footer</a>
                                </li>
                                <li>
                                    <a href="page_layout_static_main_sidebar_partial.html">Partial Main Sidebar</a>
                                </li>
                                <li>
                                    <a href="page_layout_static_main_sidebar_visible.html">Visible Main Sidebar</a>
                                </li>
                                <li>
                                    <a href="page_layout_static_alternative_sidebar_partial.html">Partial Alternative Sidebar</a>
                                </li>
                                <li>
                                    <a href="page_layout_static_alternative_sidebar_visible.html">Visible Alternative Sidebar</a>
                                </li>
                                <li>
                                    <a href="page_layout_static_no_sidebars.html">No Sidebars</a>
                                </li>
                                <li>
                                    <a href="page_layout_static_both_partial.html">Both Sidebars Partial</a>
                                </li>
                                <li>
                                    <a href="page_layout_static_animated.html">Animated Sidebar Transitions</a>
                                </li>
                            </ul>
                        </li>
                        <li class="sidebar-header">
                            <span class="sidebar-header-options clearfix"><a href="javascript:void(0)" data-toggle="tooltip" title="Quick Settings"><i class="gi gi-settings"></i></a></span>
                            <span class="sidebar-header-title">Develop Kit</span>
                        </li>
                        <li>
                            <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator"></i><i class="fa fa-wrench sidebar-nav-icon"></i>Components</a>
                            <ul>
                                <li>
                                    <a href="#" class="sidebar-nav-submenu"><i class="fa fa-angle-left sidebar-nav-indicator"></i>3 Level Menu</a>
                                    <ul>
                                        <li>
                                            <a href="#">Link 1</a>
                                        </li>
                                        <li>
                                            <a href="#">Link 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="page_comp_maps.html">Maps</a>
                                </li>
                                <li>
                                    <a href="page_comp_charts.html">Charts</a>
                                </li>
                                <li>
                                    <a href="page_comp_gallery.html">Gallery</a>
                                </li>
                                <li>
                                    <a href="page_comp_carousel.html">Carousel</a>
                                </li>
                                <li>
                                    <a href="page_comp_calendar.html">Calendar</a>
                                </li>
                                <li>
                                    <a href="page_comp_animations.html">CSS3 Animations</a>
                                </li>
                                <li>
                                    <a href="page_comp_syntax_highlighting.html">Syntax Highlighting</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator"></i><i class="gi gi-brush sidebar-nav-icon"></i>Ready Pages</a>
                            <ul>
                                <li>
                                    <a href="#" class="sidebar-nav-submenu"><i class="fa fa-angle-left sidebar-nav-indicator"></i>Errors</a>
                                    <ul>
                                        <li>
                                            <a href="page_ready_400.html">400</a>
                                        </li>
                                        <li>
                                            <a href="page_ready_401.html">401</a>
                                        </li>
                                        <li>
                                            <a href="page_ready_403.html">403</a>
                                        </li>
                                        <li>
                                            <a href="page_ready_404.html">404</a>
                                        </li>
                                        <li>
                                            <a href="page_ready_500.html">500</a>
                                        </li>
                                        <li>
                                            <a href="page_ready_503.html">503</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#" class="sidebar-nav-submenu"><i class="fa fa-angle-left sidebar-nav-indicator"></i>Get Started</a>
                                    <ul>
                                        <li>
                                            <a href="page_ready_blank.html">Blank</a>
                                        </li>
                                        <li>
                                            <a href="page_ready_blank_alt.html">Blank Alternative</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="page_ready_search_results.html">Search Results (4)</a>
                                </li>
                                <li>
                                    <a href="page_ready_article.html">Article</a>
                                </li>
                                <li>
                                    <a href="page_ready_user_profile.html">User Profile</a>
                                </li>
                                <li>
                                    <a href="#" class="sidebar-nav-submenu"><i class="fa fa-angle-left sidebar-nav-indicator"></i>Message Center</a>
                                    <ul>
                                        <li>
                                            <a href="page_ready_inbox.html">Inbox</a>
                                        </li>
                                        <li>
                                            <a href="page_ready_inbox_compose.html">Compose Message</a>
                                        </li>
                                        <li>
                                            <a href="page_ready_inbox_message.html">View Message</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="page_ready_timeline.html">Timeline</a>
                                </li>
                                <li>
                                    <a href="page_ready_faq.html">FAQ</a>
                                </li>
                                <li>
                                    <a href="page_ready_pricing_tables.html">Pricing Tables</a>
                                </li>
                                <li>
                                    <a href="page_ready_invoice.html">Invoice</a>
                                </li>
                                <li>
                                    <a href="page_ready_forum.html">Forum (3)</a>
                                </li>
                                <li>
                                    <a href="page_ready_lock_screen.html">Lock Screen</a>
                                </li>
                                <li>
                                    <a href="login.html">Login</a>
                                </li>
                                <li>
                                    <a href="login.html#register">Register</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <!-- END Sidebar Navigation -->

                    <!-- Sidebar Notifications -->
                    <div class="sidebar-header">
                        <span class="sidebar-header-options clearfix">
                                <a href="javascript:void(0)" data-toggle="tooltip" title="Refresh"><i class="gi gi-refresh"></i></a>
                            </span>
                        <span class="sidebar-header-title">Activity</span>
                    </div>
                    <div class="sidebar-section">
                        <div class="alert alert-success alert-alt">
                            <small>5 min ago</small>
                            <br>
                            <i class="fa fa-thumbs-up fa-fw"></i> You had a new sale ($10)
                        </div>
                        <div class="alert alert-info alert-alt">
                            <small>10 min ago</small>
                            <br>
                            <i class="fa fa-arrow-up fa-fw"></i> Upgraded to Pro plan
                        </div>
                        <div class="alert alert-warning alert-alt">
                            <small>3 hours ago</small>
                            <br>
                            <i class="fa fa-exclamation fa-fw"></i> Running low on space
                            <br><strong>18GB in use</strong> 2GB left
                        </div>
                        <div class="alert alert-danger alert-alt">
                            <small>Yesterday</small>
                            <br>
                            <i class="fa fa-bug fa-fw"></i>  <a href="javascript:void(0)"><strong>New bug submitted</strong></a>
                        </div>
                    </div>
                    <!-- END Sidebar Notifications -->
                </div>
                <!-- END Sidebar Content -->
            </div>
            <!-- END Wrapper for scrolling functionality -->
        </div>
        <!-- END Main Sidebar -->

        <!-- Main Container -->
        <div id="main-container">
            <!-- Header -->
            <!-- In the PHP version you can set the following options from inc/config.html file -->
            <!--
                    Available header.navbar classes:

                    'navbar-default'            for the default light header
                    'navbar-inverse'            for an alternative dark header

                    'navbar-fixed-top'          for a top fixed header (fixed sidebars with scroll will be auto initialized, functionality can be found in js/app.js - handleSidebar())
                        'header-fixed-top'      has to be added on #page-container only if the class 'navbar-fixed-top' was added

                    'navbar-fixed-bottom'       for a bottom fixed header (fixed sidebars with scroll will be auto initialized, functionality can be found in js/app.js - handleSidebar()))
                        'header-fixed-bottom'   has to be added on #page-container only if the class 'navbar-fixed-bottom' was added
                -->
<!--             <header class="navbar navbar-default"> -->
<!--                 Left Header Navigation -->
<!--                 <ul class="nav navbar-nav-custom"> -->
<!--                     Main Sidebar Toggle Button -->
<!--                     <li> -->
<!--                         <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');"> -->
<!--                             <i class="fa fa-bars fa-fw"></i> -->
<!--                         </a> -->
<!--                     </li> -->
<!--                     END Main Sidebar Toggle Button -->

<!--                     Template Options -->
<!--                     Change Options functionality can be found in js/app.js - templateOptions() -->
<!--                     <li class="dropdown"> -->
<!--                         <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"> -->
<!--                             <i class="gi gi-settings"></i> -->
<!--                         </a> -->
<!--                         <ul class="dropdown-menu dropdown-custom dropdown-options"> -->
<!--                             <li class="dropdown-header text-center">Header Style</li> -->
<!--                             <li> -->
<!--                                 <div class="btn-group btn-group-justified btn-group-sm"> -->
<!--                                     <a href="javascript:void(0)" class="btn btn-primary" id="options-header-default">Light</a> -->
<!--                                     <a href="javascript:void(0)" class="btn btn-primary" id="options-header-inverse">Dark</a> -->
<!--                                 </div> -->
<!--                             </li> -->
<!--                             <li class="dropdown-header text-center">Page Style</li> -->
<!--                             <li> -->
<!--                                 <div class="btn-group btn-group-justified btn-group-sm"> -->
<!--                                     <a href="javascript:void(0)" class="btn btn-primary" id="options-main-style">Default</a> -->
<!--                                     <a href="javascript:void(0)" class="btn btn-primary" id="options-main-style-alt">Alternative</a> -->
<!--                                 </div> -->
<!--                             </li> -->
<!--                             <li class="dropdown-header text-center">Main Layout</li> -->
<!--                             <li> -->
<!--                                 <button class="btn btn-sm btn-block btn-primary" id="options-header-top">Fixed Side/Header (Top)</button> -->
<!--                                 <button class="btn btn-sm btn-block btn-primary" id="options-header-bottom">Fixed Side/Header (Bottom)</button> -->
<!--                             </li> -->
<!--                             <li class="dropdown-header text-center">Footer</li> -->
<!--                             <li> -->
<!--                                 <div class="btn-group btn-group-justified btn-group-sm"> -->
<!--                                     <a href="javascript:void(0)" class="btn btn-primary" id="options-footer-static">Default</a> -->
<!--                                     <a href="javascript:void(0)" class="btn btn-primary" id="options-footer-fixed">Fixed</a> -->
<!--                                 </div> -->
<!--                             </li> -->
<!--                         </ul> -->
<!--                     </li> -->
<!--                     END Template Options -->
<!--                 </ul> -->
<!--                 END Left Header Navigation -->

<!--                 Search Form -->
<!--                 <form action="page_ready_search_results.html" method="post" class="navbar-form-custom" role="search"> -->
<!--                     <div class="form-group"> -->
<!--                         <input type="text" id="top-search" name="top-search" class="form-control" placeholder="Search.."> -->
<!--                     </div> -->
<!--                 </form> -->
<!--                 END Search Form -->

<!--                 Right Header Navigation -->
<!--                 <ul class="nav navbar-nav-custom pull-right"> -->
<!--                     Alternative Sidebar Toggle Button -->
<!--                     <li> -->
<!--                         If you do not want the main sidebar to open when the alternative sidebar is closed, just remove the second parameter: App.sidebar('toggle-sidebar-alt'); -->
<!--                         <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar-alt', 'toggle-other');"> -->
<!--                             <i class="gi gi-share_alt"></i> -->
<!--                             <span class="label label-primary label-indicator animation-floating">4</span> -->
<!--                         </a> -->
<!--                     </li> -->
<!--                     END Alternative Sidebar Toggle Button -->

<!--                     User Dropdown -->
<!--                     <li class="dropdown"> -->
<!--                         <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"> -->
<!--                             <img src="img/placeholders/avatars/avatar2.jpg" alt="avatar"> <i class="fa fa-angle-down"></i> -->
<!--                         </a> -->
<!--                         <ul class="dropdown-menu dropdown-custom dropdown-menu-right"> -->
<!--                             <li class="dropdown-header text-center">Account</li> -->
<!--                             <li> -->
<!--                                 <a href="page_ready_timeline.html"> -->
<!--                                     <i class="fa fa-clock-o fa-fw pull-right"></i> -->
<!--                                     <span class="badge pull-right">10</span> -->
<!--                                     Updates -->
<!--                                 </a> -->
<!--                                 <a href="page_ready_inbox.html"> -->
<!--                                     <i class="fa fa-envelope-o fa-fw pull-right"></i> -->
<!--                                     <span class="badge pull-right">5</span> -->
<!--                                     Messages -->
<!--                                 </a> -->
<!--                                 <a href="page_ready_pricing_tables.html"><i class="fa fa-magnet fa-fw pull-right"></i> -->
<!--                                         <span class="badge pull-right">3</span> -->
<!--                                         Subscriptions -->
<!--                                     </a> -->
<!--                                 <a href="page_ready_faq.html"><i class="fa fa-question fa-fw pull-right"></i> -->
<!--                                         <span class="badge pull-right">11</span> -->
<!--                                         FAQ -->
<!--                                     </a> -->
<!--                             </li> -->
<!--                             <li class="divider"></li> -->
<!--                             <li> -->
<!--                                 <a href="page_ready_user_profile.html"> -->
<!--                                     <i class="fa fa-user fa-fw pull-right"></i> -->
<!--                                     Profile -->
<!--                                 </a> -->
<!--                                 Opens the user settings modal that can be found at the bottom of each page (page_footer.html in PHP version) -->
<!--                                 <a href="#modal-user-settings" data-toggle="modal"> -->
<!--                                     <i class="fa fa-cog fa-fw pull-right"></i> -->
<!--                                     Settings -->
<!--                                 </a> -->
<!--                             </li> -->
<!--                             <li class="divider"></li> -->
<!--                             <li> -->
<!--                                 <a href="page_ready_lock_screen.html"><i class="fa fa-lock fa-fw pull-right"></i> Lock Account</a> -->
<!--                                 <a href="login.html"><i class="fa fa-ban fa-fw pull-right"></i> Logout</a> -->
<!--                             </li> -->
<!--                             <li class="dropdown-header text-center">Activity</li> -->
<!--                             <li> -->
<!--                                 <div class="alert alert-success alert-alt"> -->
<!--                                     <small>5 min ago</small> -->
<!--                                     <br> -->
<!--                                     <i class="fa fa-thumbs-up fa-fw"></i> You had a new sale ($10) -->
<!--                                 </div> -->
<!--                                 <div class="alert alert-info alert-alt"> -->
<!--                                     <small>10 min ago</small> -->
<!--                                     <br> -->
<!--                                     <i class="fa fa-arrow-up fa-fw"></i> Upgraded to Pro plan -->
<!--                                 </div> -->
<!--                                 <div class="alert alert-warning alert-alt"> -->
<!--                                     <small>3 hours ago</small> -->
<!--                                     <br> -->
<!--                                     <i class="fa fa-exclamation fa-fw"></i> Running low on space -->
<!--                                     <br><strong>18GB in use</strong> 2GB left -->
<!--                                 </div> -->
<!--                                 <div class="alert alert-danger alert-alt"> -->
<!--                                     <small>Yesterday</small> -->
<!--                                     <br> -->
<!--                                     <i class="fa fa-bug fa-fw"></i>  <a href="javascript:void(0)" class="alert-link">New bug submitted</a> -->
<!--                                 </div> -->
<!--                             </li> -->
<!--                         </ul> -->
<!--                     </li> -->
<!--                     END User Dropdown -->
<!--                 </ul> -->
<!--                 END Right Header Navigation -->
<!--             </header> -->
            <!-- END Header -->

            <!-- Page content -->
            <div id="page-content">
                <!-- Statistics Widgets Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>Module User Dashboard
<%--                         <img src="<%=contexturl%>resources/img/school-img.png" alt="Charcha" class=""> --%>
                        </h1>
						<a href="#logout_popup"><i class="fa fa-ban fa-fw pull-right"></i> Logout</a>
                    </div>
                </div>
                <!-- END Statistics Widgets Header -->
					<div class="row">
                	<c:forEach var="currentUser" items="${modulemaster}"
						varStatus="count">
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <!-- Widget -->
                        <div class="widget">
                            <div class="widget-extra">
                                <div class="text-center db-icon">
                                    <a href="<%=contexturl %>ManageVisitorsPage/ListofCategories/ChooseCategory">
                                        <img src="<%=contexturl %>${currentUser.profileImage.imagePath}" width="120" height="120" alt="Module" >
                                    </a>
                                </div>
                            </div>
                            <div class="widget-extra themed-background-spring text-center">
                                <h4 class="widget-content-light"><strong>${currentUser.moduleName}</strong></h4>
                            </div>
                             <p>${currentUser.moduleDescription}</p>
                        </div>
                        <!-- END Widget -->
                    </div>
				</c:forEach>
                </div>
			</div>
            <!-- END Page Content -->

            
        </div>
        <!-- END Main Container -->
    </div>
<!-- END Page Content -->
<%@include file="../inc/page_footer.jsp"%>

<!-- Remember to include excanvas for IE8 chart support -->
<!--[if IE 8]><script src="<%=contexturl%>resources/js/helpers/excanvas.min.js"></script><![endif]-->

<%@include file="../inc/template_scripts.jsp"%>

<!-- Google Maps API + Gmaps Plugin, must be loaded in the page you would like to use maps (Remove 'http:' if you have SSL) -->
<!-- <script src="http://maps.google.com/maps/api/js?sensor=true"></script> -->
<%-- <script src="<%=contexturl%>resources/js/helpers/gmaps.min.js"></script> --%>

<!-- <!-- Load and execute javascript code used only in this page --> 
<%-- <script src="<%=contexturl%>resources/js/pages/index.js"></script> --%>
<!-- <script>$(function(){ Index.init(); });</script> -->



<%@include file="../inc/template_end.jsp"%>