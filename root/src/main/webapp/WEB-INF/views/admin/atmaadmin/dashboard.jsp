<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->



<%@include file="../inc/config.jsp"%>
<%@include file="../inc/template_start.jsp"%>
<%@include file="../inc/page_head.jsp"%>

<script src="<%=contexturl%>resources/js/canvasjs.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="<%=contexturl%>resources/js/highcharts.js"></script>
<script src="<%=contexturl%>resources/js/highcharts-more.js"></script>
<script src="<%=contexturl%>resources/js/solid-gauge.js"></script>
<script src="<%=contexturl%>resources/js/Chart.js"></script>

<div id="page-content">
	<div class="row">
	<a href="<%=contexturl%>historyReportExcelAtmaAdmin" class="btn btn-primary" style="float: right;margin: 1%;">DownLoad History Report</a>
	</div>
	<div class="row">
		<div class="well" style="height: 240px;">
			<div class="row">
				<div class="col-md-2 col-md-offset-1 col-lg-2">
					<div class="row" style="text-align: center;">
						<h3>
							<small>Total Organisations</small>
						</h3>
					</div>
					<div class="row" style="text-align: center;">
						<canvas id="canvasone" height="150" width="150"></canvas>
						<div class="donut-inner">
							<h4 style="color: #21DC54;">${totalOrganisations}</h4>
						</div>
					</div>
				</div>
				<div class="col-md-2 col-lg-2 ">
					<div class="row" style="text-align: center;">
						<h3>
							<small>Total Users</small>
						</h3>
					</div>
					<div class="row" style="text-align: center;">
						<canvas id="canvastwo" height="150" width="150"></canvas>
						<div class="donut-inner">
							<h4 style="color: #21DC54;">${ActiveTotalUsers}
							
						</div>
					</div>
				</div>

				<div class="col-md-2 col-lg-2">
					<div class="row" style="text-align: center;">
						<h3>
							<small>Total Downloads</strong>
						</h3>
					</div>
					<div class="row" style="text-align: center;">
						<canvas id="canvasthree" height="150" width="150"></canvas>
						<div class="donut-inner">
							<h4 style="color: #21DC54;">${totalDownloads}</h4>
						</div>
					</div>
				</div>

				<div class="col-md-2 col-lg-2">
					<div class="row" style="text-align: center;">
						<h3>
							<small >Total Forum</small>
						</h3>
					</div>
					<div class="row" style="text-align: center;">
						<canvas id="canvasfour" height="150" width="150"></canvas>
						<div class="donut-inner">
							<h4 style="color: #21DC54;">${totalPost}</h4>
						</div>
					</div>
				</div>

				<div class="col-md-2 col-lg-2">
					<div class="row" style="text-align: center;">
						<h3>
							<small>Total Comments</small>
						</h3>
					</div>
					<div class="row" style="text-align: center;">
						<canvas id="canvasfive" height="150" width="150"></canvas>
						<div class="donut-inner">
							<h4 style="color: #21DC54;">${totalComment}</h4>
							
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-6">
			<div class="widget">
				<div class="widget-extra themed-background-dark">
					<h4 class="widget-content-light">
						<strong>Towards Goal</strong><small> <a
							href="<%=contexturl%>excelOfUserTowardGoal"> <strong>Download
									User</strong>
						</a> <a href="<%=contexturl%>excelOfLSSTowardGoal"> <strong>Download
									LSS</strong>
						</a>
						</small>

					</h4>
				</div>
				<div class="block">
					<div class="row">

						<div class="col-sm-6">
							<div id="container-rpm"
								style="width: 200px; height: 190px; float: left"></div>
						</div>
						<div class="col-sm-6">
							<div id="container-speed"
								style="width: 200px; height: 190px; float: left"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="widget">
				<div class="widget-extra themed-background-dark">
					<h4 class="widget-content-light">
						<strong>Recent Activity</strong>
					</h4>
				</div>
				<div class="block">
					<div class="table-responsive">
						<table id="general-table" class="table table-vcenter table-hover">
							<tbody id="tbody" class="TFtable">
								<tr>
									<td class="text-left"><i class="fa fa-comments"></i></td>
									<td class="text-left"><span>${lastComments}</span><span style="margin-left: 15%;">Comments</span></td>
									<td class="text-center">
										<div class="btn-group">
											<p class="form-control-static">
												<a href="<%=contexturl%>lastComments">View </a>
											</p>
										</div>
									</td>
								</tr>
								<tr>
									<td class="text-left"><i class="fa fa-download"></i></td>
									<td class="text-left"><span>${lastDownloadCount}</span><span style="margin-left: 15%;">Downloads</span>  </td>
									<td class="text-center">
										<div class="btn-group">
											<p class="form-control-static">
												<a href="<%=contexturl%>lastDownloads">View </a>
												
											</p>
										</div>
									</td>
								</tr>
								<tr>
									<td class="text-left"><i class="fa fa-user"></i></td>
									<td class="text-left"><span>${lastAddedUserCount}</span><span style="margin-left: 15%;">Last Added Users</span></td>
									<td class="text-center">
										<div class="btn-group">
											<p class="form-control-static">
												<a href="<%=contexturl%>lastAddedUsers">View </a>
											</p>
										</div>
									</td>
								</tr>
								<tr>
									<td class="text-left"><i class="fa fa-plus "></i></td>
									<td class="text-left"><span>${lastAddedOrganisations}</span><span style="margin-left: 15%;">Last Added Organisation</span>  
										</td>
									<td class="text-center">
										<div class="btn-group">
											<p class="form-control-static">
												<a href="<%=contexturl%>lastAddedOrganisation">View </a>
											</p>
										</div>
									</td>
								</tr>
								<tr>
									<td class="text-left"><i class="fa fa-envelope"></i></td>
									<td class="text-left"><span>${lastSevenDayPosts}</span><span style="margin-left: 15%;">Posts</span>  </td>
									<td class="text-center">
										<div class="btn-group">
											<p class="form-control-static">
												<a href="<%=contexturl%>lastposts">View </a>
											</p>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>

		</div>
	</div>

	<div class="row">
		<div class="col-md-6">
			<div class="widget">
				<div class="widget-extra themed-background-dark">
					<h4 class="widget-content-light">
						<strong>Download Analysis</strong>
					</h4>
				</div>

				<div class="block">
					<div class="row">
						<!-- <div class="col-sm-1">
								
						</div> -->
						<div class="col-sm-11 col-sm-offset-1">
							<div id="piechart5" style="width: 400px; height: 400px;">
							</div>
							<div class="text-center" style="padding: 5px 0px 15px;">
								<img src="<%=contexturl%>resources/img/downloadPerModule- 1.png" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">

			<div class="widget">
				<div class="widget-extra themed-background-dark">
					<h4 class="widget-content-light">
						<strong>Average ODA Values</strong> Chart
					</h4>
				</div>
				<div class="block">
					<div class="row">
						<div class="col-sm-1">
							<div
								style="position: relative; height: 200px; margin-top: 135px;">
								<img class="no-of-user"
									src="<%=contexturl%>resources/img/avgOda.png" />
							</div>
						</div>
						<div class="col-sm-11">
							<canvas id="myChart" width="400" height="400"></canvas>
							<div class="text-center" style="padding: 5px 0px 15px;">
								<img src="<%=contexturl%>resources/img/departments.png" />
							</div>
						</div>
					</div>
				</div>
			</div>



		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="widget">
				<div class="widget-extra themed-background-dark">
					<h4 class="widget-content-light">
						<strong>Inactive Organisation And User</strong> Chart<small>
							<a href="<%=contexturl%>DownloadExcelOfInactiveUserAndOrg"> <strong>Download
									Excel</strong>
						</a>
						</small>
					</h4>
				</div>
				<div class="block">

					<div class="row">
						<div class="col-md-offset-2 col-md-2">
							<select id="year" name="year" class="form-control" size="1">
								<option value=""></option>
								<c:forEach items="${yearList}" var="yearName">
									<option value="${yearName}"
										<c:if test="${yearName eq currentYear}">selected="selected"</c:if>>${yearName}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-1">
							<div
								style="position: relative; height: 200px; margin-top: 135px;">
								<img class="no-of-user"
									src="<%=contexturl%>resources/img/noOfCom.png" />
							</div>
						</div>
						<div class="col-sm-8">
							<canvas id="myChart3" width="860" height="400"></canvas>
							<div class="text-center" style="padding: 5px 0px 15px;">
								<img
									src="<%=contexturl%>resources/img/text2image_X31693_20160229_070637.png" />
							</div>
						</div>
						<div class="col-sm-3" style="margin-top: 21px;">
							<div id="js-legend" class="chart-legend"></div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
<!-- END Page Content -->
<%@include file="../inc/page_footer.jsp"%>
<%@include file="../inc/template_scripts.jsp"%>


<script src="<%=contexturl%>resources/js/pages/index.js"></script>

<script>
/* five pie chart */
var yr=${yearList};

var a=${activeLastMonthCompanies};
var b=${activeMoreThanOneMonthCompanies};

var a1=${lastMonthCreatedUser};
var b1=${oldUsers};

var a2=${oneMonthDownload};
var b2=${morethanOneMonthDownload};

var a4=${noOfLastComments};
var b4=${noOfOldComments};


$(function () {
var ctx = document.getElementById("canvasone").getContext("2d");
var data = [
            {
                value: a,
                color:"#21DC54",
                highlight: "#BCF5A9",
                label: "< One Month"
            },
            {
                value: b,
                color: "lightgray",
                highlight: "lightgray",
                label: "> One Month"
            }
            
        ]
    var option={
		tooltipFontSize: 12,
		percentageInnerCutout : 80,
		    
     }    
var myDoughnutChart = new Chart(ctx).Doughnut(data,option); 
});


$(function () {
	var ctx = document.getElementById("canvastwo").getContext("2d");
	var data = [
	            {
	                value: a1,
	                color:"#21DC54",
	                highlight: "#BCF5A9",
	                label: "< One Month"
	            },
	            {
	                value: b1,
	                color: "lightgray",
	                highlight: "lightgray",
	                label: "> One Month"
	            }
	            
	        ]
	    var option={
			tooltipFontSize: 12,
			percentageInnerCutout : 80	
	     }    
	var myDoughnutChart = new Chart(ctx).Doughnut(data,option); 
	});


$(function () {
	var ctx = document.getElementById("canvasthree").getContext("2d");
	var data = [
	            {
	                value: a2,
	                color:"#21DC54",
	                highlight: "#BCF5A9",
	                label: "< One Month"
	            },
	            {
	                value: b2,
	                color: "lightgray",
	                highlight: "lightgray",
	                label: "> One Month"
	            }
	            
	        ]
	    var option={
			tooltipFontSize: 12,
			percentageInnerCutout : 80	
	     }    
	var myDoughnutChart = new Chart(ctx).Doughnut(data,option); 
	});
	
$(function () {
	var ctx = document.getElementById("canvasfour").getContext("2d");
	var data = [
	            {
	                value: ${lastMonthPost},
	                color:"#21DC54",
	                highlight: "#BCF5A9",
	                label: "< One Month"
	            },
	            {
	                value: ${olderThanOneMonthPost},
	                color: "lightgray",
	                highlight: "lightgray",
	                label: "> One Month"
	            }
	            
	        ]
	    var option={
			tooltipFontSize: 12,
			percentageInnerCutout : 80	
	     }    
	var myDoughnutChart = new Chart(ctx).Doughnut(data,option); 
	});
	
$(function () {
	var ctx = document.getElementById("canvasfive").getContext("2d");
	var data = [
	            {
	                value: a4,
	                color:"#21DC54",
	                highlight: "#BCF5A9",
	                label: "< One Month"
	            },
	            {
	                value: b4,
	                color: "lightgray",
	                highlight: "lightgray",
	                label: "> One Month"
	            }
	            
	        ]
	    var option={
			tooltipFontSize: 12,
			percentageInnerCutout : 80	
	     }    
	var myDoughnutChart = new Chart(ctx).Doughnut(data,option); 
	});
</script>
<!--

//-->
</script>


<!-- highchart solid guage -->

<script>
var value=${fullProfileComplete};
var total=${totalUser};
var tatalCom=${totalOrganisations};
var surveyCompletes=${noOdSurveyComplited};


$(function () {

    var gaugeOptions = {

        chart: {
            type: 'solidgauge'
        },

        title: null,

        pane: {
            center: ['50%', '85%'],
            size: '110%',
            startAngle: -90,
            endAngle: 90,
            background: {
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
                innerRadius: '60%',
                outerRadius: '100%',
                shape: 'arc'
            }
        },

        tooltip: {
            enabled: false
        },

        // the value axis
        yAxis: {
            stops: [
                [0.1, '#55BF3B'], // green
                [0.5, '#DDDF0D'], // yellow
                [0.9, '#DF5353'] // red
            ],
            lineWidth: 0,
            minorTickInterval: null,
            tickPixelInterval: 400,
            tickWidth: 0,
            title: {
                y: -70
            },
            labels: {
                y: 18,
                
            }
        },

        plotOptions: {
            solidgauge: {
                dataLabels: {
                    y: 5,
                  
                    borderWidth: 0,
                    useHTML: true
                }
            }
        }
    };

    var chart = new Highcharts.Chart(Highcharts.merge(gaugeOptions, {
    	chart: {
            
             renderTo: 'container-speed'
         },
        yAxis: {
            min: 0,
            max: tatalCom,
             tickInterval: tatalCom, 
         
           title: {
                text: 'LSS Completion',
                style:{
                    color: 'black',
                    fontSize: '16px'
                   }  
            },
             labels: {
                step: 1,
                align: 'center',
                enabled: true,
                style: {
                    fontSize:'12px'
                }
                
            },
        },

        credits: {
            enabled: false
        },

        series: [{
            name: 'Speed',
            data: [surveyCompletes],
            dataLabels: {
                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
                       '<span style="font-size:12px;color:silver"></span></div>'
            },
            tooltip: {
                valueSuffix: ' km/h'
            }
        }]

    }));

    // The RPM gauge
     var chart = new Highcharts.Chart(Highcharts.merge(gaugeOptions, {
    	 chart: {
             
             renderTo: 'container-rpm'
         },
    	 yAxis: {
            min: 0,
            max: total,
            tickInterval: total, 
            title: {
                text: 'Profile Completion',
                style:{
                    color: 'black',
                    fontSize: '16px'
                   }  
            },
         labels: {
            step: 1,
            enabled: true,
            style: {
                fontSize:'12px'
            }
        }
        },

        series: [{
            name: 'RPM',
            data: [value],
            dataLabels: {
                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
                       '<span style="font-size:12px;color:silver"></span></div>'
            },
            tooltip: {
                valueSuffix: ' revolutions/min'
            }
        }]

    }));

});
</script>



<script>

${company.companyName};
 google.charts.load('current', {'packages':['corechart']});
 
 var pie3=${pieChartDatalist};
    google.charts.setOnLoadCallback(drawChart5);
   function drawChart5() {
       var data = google.visualization.arrayToDataTable(pie3);

       var options = {
         title: 'Downloads Per Module',
         is3D:false,
         width:'400',
         height:'400',
         legend:'none',
         pieSliceTextStyle:{color: 'green', fontName: 'bold', fontSize: '16'},
         colors: ['#00F9B7', '#51DAB6', '#02A278', '#8BD4B3', '#2BB376','#1BF7F9', '#49ADF7', '#3B93D4', '#0F27A0','#03177B'],
        /*  colors: ['#8DBCD8', '#8CE4A4', '#6EB8E4', '#9CDAAD', '#93BBD4','#BCE8D2', '#93BBD4', '#B3ECB7', '#70A8CA','#93BBD4','#B1ECBB'], */
         titleTextStyle: 
       	  {color: 'black', fontName:'bold', fontSize: '23'},
         
       };

       var chart = new google.visualization.PieChart(document.getElementById('piechart5'));

       chart.draw(data, options);
     } 
</script>

<script>
$(function(){
	var proReport1 = 5;
	var proReport2 = 4;
	var proReport3 = 3;
	var proReport4 = 2;
	var proReport5 = 1;
	
	var ctx = document.getElementById("myChart").getContext("2d");
	var d=${jsonBar};
	var dataset=[];
	var lables=[];
	var counts=[];
    
	for(var a=0; a<d.length; a++){
 		dataset.push(d[a].avg);
 		lables.push(d[a].lable);
 		counts.push(d[a].count);
	}
	
	var data = {
		     labels:
		    	  ["HR", "Admin", "Leader", "Finance", "Fundraising","Monitoring","Programs","Marketing","Governance","Strategy"],
		      
		      datasets: [
		        {
		            label: counts,
		            fillColor: "#46A5F1",
		            strokeColor: "rgba(220,220,220,0.8)",
		            highlightFill: "rgba(220,220,220,0.75)",
		            highlightStroke: "rgba(220,220,220,1)",
		            data:dataset /* [proReport1, proReport2, proReport3, proReport4, proReport5,'1','2','3','3','4','4'] */
		        }
		    ]
		};
	var myBarChart = new Chart(ctx).Bar(data,{
		
		<%-- multiTooltipTemplate: "<%= datasetLabel  %> - <%= value %>,<%= labels %>", --%>
		showTooltips: false,
	    scaleIntegersOnly: false,
	    onAnimationComplete: function () {

	        var ctx = this.chart.ctx;
	        ctx.font = this.scale.font;
	        ctx.fillStyle = this.scale.textColor
	        ctx.textAlign = "center";
	        ctx.textBaseline = "bottom";

	        this.datasets.forEach(function (dataset) {
	            var i=0;
	        	dataset.bars.forEach(function (bar) {
	                ctx.fillText(counts[i], bar.x, bar.y - 5);
	                i++;
	            });
	        })
	    }
	});
		
});


</script>

<script>
/* Inactive Organisation */

$(document).ready(
		function() {
	
			$(".forumHover").hover(function() {
				// this is the mouseenter event handler
				var boom = $(this).attr('id');
				//$(this).addClass("my_hover");
				//alert(boom);
				$(this).popover('show');
			}, function() {
				// this is the mouseleave event handler
				$(this).popover('hide');

			});
			
			/* var mnth = $('#monthOftheYear').val(); */
			var yr = $('#year').val();
			$.ajax({
						type: "GET",
						async: false,
						url: "<%=contexturl%>InactiveOrganisationsAndUser/"+yr,
						success: function(data, textStatus, jqXHR){
							var re1=[];
							var re2=[];
							re1=data.one;
							re2=data.two;
							
						
							barGraph(re1,re2);
						},
					});
			
			$('#year').change(function() {
			//	window.alert("in ajax call year change")
				var year = ($(this).val());
			//	var month = $('#monthOftheYear').val();
				$.ajax({
					type: "GET",
					async: false,
					url: "<%=contexturl%>InactiveOrganisationsAndUser/"+year,
					success: function(data, textStatus, jqXHR){
						var re1=[];
						var re2=[];
						re1=data.one;
						re2=data.two;
						
					
						barGraph(re1,re2);
					},
				});
		    });	

			
		});
		
function barGraph(re1,re2){

	
var lineChartData = {
		   "datasets": [{ "label": "User",
		       "data": re1,
		           "pointStrokeColor": "#fff",
		            "fillColor": "rgba(234,209,209,0.5)",
		           "pointColor": "#FF0000",
		           "strokeColor": "#FF0000"
		   },
		   { "label": "Organiisation",
			  "data":re2, 
	           "pointStrokeColor": "#fff",
	            "fillColor": "rgba(253,255,165,0.5)",
	           "pointColor": "rgba(255,255,0,0.3)",
	           "strokeColor": "rgba(255,255,0,0.3)"
		   }],
		       "labels": ["Jan", "Feb", "March", "April","May","Jun","July","August","Sept","Oct","Nov","Dec"]
		};
        
        var options={
		   bezierCurve: false,
		   showTooltips: true
        }
		var myLine = new Chart(document.getElementById("myChart3").getContext("2d")).Line(lineChartData,options);
		
	   
	   document.getElementById('js-legend').innerHTML = myLine.generateLegend();
	   
	   
	   
		
}

</script>

<%@include file="../inc/template_end.jsp"%>