<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@include file="../../../inc/config.jsp"%>
<%@include file="../../../inc/template_start.jsp"%>
<%@include file="../../../inc/page_head.jsp"%>


<style>

.chosen-container {
	width: 250px !important;
}
 #map-canvas { height: 300px;}
</style>
<div id="page-content">
<div class="content-header">
	<div class="header-section">
		<h1>
			<i class="fa fa-map-marker"></i>
			<spring:message code="heading.citymaster" />
			<br> <small><spring:message
					code="heading.specifylistofcity" /> </small>
		</h1>
		<span id="errorMsg"></span>
		<c:if test="${!empty success}">
			<div class="alert alert-success alert-dismissable">
				<button type="button" id="success_close" class="close" data-dismiss="alert"
					aria-hidden="true">x</button>
				<h4>
					<i class="fa fa-check-circle"></i> <spring:message code="label.success"/>
				</h4>
				<spring:message code="${success}" />
			</div>
		</c:if>
		<c:if test="${!empty error}">
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert"
					aria-hidden="true">x</button>
				<h4>
					<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
				</h4>
				<spring:message code="${error}" />
			</div>
		</c:if>
	</div>
</div>
<ul class="breadcrumb breadcrumb-top">
	<li><spring:message code="menu.managelocations" /></li>
	<li><a href="#"><spring:message code="label.city" /></a></li>
</ul>
<div class="block full gridView">
	<div class="block-title">
		<h2>
			<strong><spring:message code="heading.activecity" /></strong>
		</h2>
	</div>
	<a  class="btn btn-sm btn-primary save" href="<%=contexturl %>ManageCity/City/AddCity">
						<i class="fa fa-angle-right"></i> <spring:message code="label.addcity" />
		</a>
<%-- 	<a href="#" id="addArea"><spring:message code="label.addarea" /></a> --%>
	<!--<p><a href="https://datatables.net/" target="_blank">DataTables</a> is a plug-in for the Jquery Javascript library. It is a highly flexible tool, based upon the foundations of progressive enhancement, which will add advanced interaction controls to any HTML table. It is integrated with template's design and it offers many features such as on-the-fly filtering and variable length pagination.</p> -->
	<div class="table-responsive">
		<table id="active-datatable"
			class="table table-vcenter table-condensed table-bordered">
			<thead>
				<tr>
					<th class="text-center"><spring:message code="label.id" /></th>
					<th class="text-center"><spring:message code="label.city" /></th>
					<th class="text-center"><spring:message code="label.state" /></th>
					<th class="text-center"><spring:message code="label.country" /></th>
					<th class="text-center"><spring:message code="label.actions" /></th>
				</tr>
			</thead>
			<tbody id="tbody">
				<%-- <c:forEach var="currentCity" items="${cityList}" varStatus="count">
					<tr>
						<td class="text-center">${count.count}</td>
						<td class="text-left"><c:out
								value="${currentCity.cityName}" /></td>
						<td class="text-left"><c:out
								value="${currentCity.state.stateName}" /></td>
						<td class="text-left"><c:out
								value="${currentCity.country.countryName}" /></td>
						<td class="text-center">
							<div class="btn-group">
								<a href="<%=contexturl %>ManageCity/City/EditCity?id=${currentCity.cityId}" data-toggle="tooltip" title="Edit"
									class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
							<a href="#delete_area_popup" data-toggle="modal"
									onclick="deletearea(${currentCity.cityId})" title="Delete"
									class="btn btn-xs btn-danger"><i class="fa fa-times"></i> </a>
							</div>
						</td>
					</tr>
				</c:forEach> --%>
			</tbody>
		</table>
	</div>
	</div>
	<div class="block full gridView">
		<div class="block-title">
			<h2>
				<strong><spring:message code="heading.inactivecity" /></strong>
			</h2>
		</div>
<div class="table-responsive">
			<table id="inactive-datatable"
				class="table table-vcenter table-condensed table-bordered">
				<thead>
					<tr>
						<th class="text-center"><spring:message code="label.id" /></th>
						<th class="text-center"><spring:message code="label.city" /></th>
						<th class="text-center"><spring:message code="label.state" /></th>
						<th class="text-center"><spring:message code="label.country" /></th>
						<th class="text-center"><spring:message code="label.actions" /></th>
					</tr>
				</thead>
				<tbody id="tablebody">
				<%-- 	<c:forEach var="currentCity" items="${inActiveCityList}"
						varStatus="count">
						<tr>
								<td class="text-center">${count.count}</td>
						<td class="text-left"><c:out
								value="${currentCity.cityName}" /></td>
						<td class="text-left"><c:out
								value="${currentCity.state.stateName}" /></td>
						<td class="text-left"><c:out
								value="${currentCity.country.countryName}" /></td>
							<td class="text-center">
								<div class="btn-group">
									<a href="#restore_Area_popup" data-toggle="modal"
										onclick="restoreArea(${currentCity.cityId})"
										title="Restore" class="btn btn-xs btn-success save"><i
										class="fa fa-plus"></i> </a>
								</div>
							</td>
						</tr>
					</c:forEach> --%>
				</tbody>
			</table>
		</div>
	
	
</div>
<div id="delete_area_popup" class="modal fade" tabindex="-1"
	role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<form action="DeleteCity" method="post" class="form-horizontal form-bordered" id="delete_area_form">
					<div style="padding: 10px; height: 110px;">
						<label><spring:message code="validation.doyouwanttodeletethiscity"/></label>
						<div class="col-xs-12 text-right">
							<button type="button" class="btn btn-sm btn-default"
								data-dismiss="modal"><spring:message code="label.no"/></button>
							<div id="delete_area" class="btn btn-sm btn-primary"><spring:message code="label.yes"/></div>
						</div>
					</div>
					<input type="hidden"  name="id" id="areaId">
				</form>
			</div>
		</div>
	</div>
</div>

<div id="restore_Area_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="RestoreCity" method="post" class="form-horizontal form-bordered" id="restore_area_form">
						<div style="padding: 10px; height: 110px;">
							<label><spring:message code="validation.doyouwanttorestorethiscity"/></label>
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal"><spring:message code="label.no"/></button>
								<div id="restore_Area" class="btn btn-sm btn-primary"><spring:message code="label.yes"/></div>
							</div>
						</div>
						<input type="hidden"  name="id" id="area_id">
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<%@include file="../../../inc/page_footer.jsp"%>
<%@include file="../../../inc/template_scripts.jsp"%>

<script src="<%=contexturl %>resources/js/pages/activeTablesDatatables.js"></script>
<script src="<%=contexturl %>resources/js/pages/inactiveTablesDatatables.js"></script>

<script type="text/javascript">
var id1 =0 ;
var id2 = 0 ;
function deletearea(id){
	  id1 = id;
}
function restoreArea(id){
		id2 = id;
}
$(document).ready(function() {
	$("#manageLocation").addClass("active");
	
	App.datatables();
	 $("#active-datatable").dataTable( {
		   "aoColumnDefs": [ { "bSortable": false,"bSearchable": true, "aTargets": [4 ] } ],
		    "iDisplayLength": 10,
		    "sPaginationType": "bootstrap",
          "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
	        "bProcessing": true,
	        "bServerSide": true,
	        "sort": "position",
	        "sAjaxSource": "/CityList/Active",
	        
	    } );
	 
	 $("#inactive-datatable").dataTable( {
		   "aoColumnDefs": [ { "bSortable": false,"bSearchable": true, "aTargets": [4 ] } ],
		    "iDisplayLength": 10,
		    "sPaginationType": "bootstrap",
          "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
	        "bProcessing": true,
	        "bServerSide": true,
	        "sort": "position",
	        "sAjaxSource": "/CityList/Inactive",
	        
	    } );
	 
	 $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
    $('.dataTables_length select').addClass('form-control');
	
		  $("#restore_Area").click(function(){
				$("#area_id").val(id2);
				$("#restore_area_form").submit();
			});
		
		$("#delete_area").click(function(){
			$("#areaId").val(id1);
			$("#delete_area_form").submit();
		});	
});
</script>
<%@include file="../../../inc/template_end.jsp"%>