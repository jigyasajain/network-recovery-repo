<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div id="sidebar">
	<div class="sidebar-scroll">
		<div class="sidebar-content">
			<a href="../GourmetAdmin/" class="sidebar-brand"> <i
				class="gi gi-flash"></i><strong>Gourmet7</strong>
			</a>
			<div class="sidebar-section sidebar-user clearfix">
				<div class="sidebar-user-avatar">
					<a href="page_ready_user_profile.html"> <img
						src="../resources/img/placeholders/avatars/avatar2.jpg"
						alt="avatar">
					</a>
				</div>
				<div class="sidebar-user-name">Rohit</div>
				<div class="sidebar-user-links">
					<a href="page_ready_user_profile.html" data-toggle="tooltip"
						data-placement="bottom" title="Profile"><i class="gi gi-user"></i></a>
					<a href="page_ready_inbox.html" data-toggle="tooltip"
						data-placement="bottom" title="Messages"><i
						class="gi gi-envelope"></i></a> <a href="#modal-user-settings"
						data-toggle="modal" class="enable-tooltip" data-placement="bottom"
						title="Settings"><i class="gi gi-cogwheel"></i></a> <a
						href="login.html" data-toggle="tooltip" data-placement="bottom"
						title="Logout"><i class="gi gi-exit"></i></a>
				</div>
			</div>
			<!--
<ul class="sidebar-section sidebar-themes clearfix">
<li class="active">
<a href="javascript:void(0)" class="themed-background-dark-default themed-border-default" data-theme="default" data-toggle="tooltip" title="Default Blue"></a>
</li>
<li>
<a href="javascript:void(0)" class="themed-background-dark-night themed-border-night" data-theme="superadmin/cssthemes/night.css" data-toggle="tooltip" title="Night"></a>
</li>
<li>
<a href="javascript:void(0)" class="themed-background-dark-amethyst themed-border-amethyst" data-theme="superadmin/cssthemes/amethyst.css" data-toggle="tooltip" title="Amethyst"></a>
</li>
<li>
<a href="javascript:void(0)" class="themed-background-dark-modern themed-border-modern" data-theme="superadmin/cssthemes/modern.css" data-toggle="tooltip" title="Modern"></a>
</li>
<li>
<a href="javascript:void(0)" class="themed-background-dark-autumn themed-border-autumn" data-theme="superadmin/cssthemes/autumn.css" data-toggle="tooltip" title="Autumn"></a>
</li>
<li>
<a href="javascript:void(0)" class="themed-background-dark-flatie themed-border-flatie" data-theme="superadmin/cssthemes/flatie.css" data-toggle="tooltip" title="Flatie"></a>
</li>
<li>
<a href="javascript:void(0)" class="themed-background-dark-spring themed-border-spring" data-theme="superadmin/cssthemes/spring.css" data-toggle="tooltip" title="Spring"></a>
</li>
<li>
<a href="javascript:void(0)" class="themed-background-dark-fancy themed-border-fancy" data-theme="superadmin/cssthemes/fancy.css" data-toggle="tooltip" title="Fancy"></a>
</li>
<li>
<a href="javascript:void(0)" class="themed-background-dark-fire themed-border-fire" data-theme="superadmin/cssthemes/fire.css" data-toggle="tooltip" title="Fire"></a>
</li>
</ul>
 -->
			<ul class="sidebar-nav">
				<li><a href="index.html" class=" active"><i
						class="gi gi-stopwatch sidebar-nav-icon"></i> <spring:message code="menu.dashboard"/></a></li>
				<li class="sidebar-header"><span
					class="sidebar-header-options clearfix"><a
						href="javascript:void(0)" data-toggle="tooltip"
						title="Quick Settings"><i class="gi gi-settings"></i></a><a
						href="javascript:void(0)" data-toggle="tooltip"
						title="Create the most amazing pages with the widget kit!"><i
							class="gi gi-lightbulb"></i></a></span> <span class="sidebar-header-title"><spring:message code="menu.analysiskit"/>
						</span></li>
				<li><a href="page_widgets_stats.html"><i
						class="gi gi-charts sidebar-nav-icon"></i> <spring:message code="menu.statistics"/></a></li>
				<!--
<li>
<a href="page_widgets_social.html"><i class="gi gi-share_alt sidebar-nav-icon"></i>Social</a>
</li>
<li>
<a href="page_widgets_media.html"><i class="gi gi-film sidebar-nav-icon"></i>Media</a>
</li>
-->
				<li class="sidebar-header"><span
					class="sidebar-header-options clearfix"><a
						href="javascript:void(0)" data-toggle="tooltip"
						title="Quick Settings"><i class="gi gi-settings"></i></a></span> <span
					class="sidebar-header-title"><spring:message code="menu.restaurantmanagement"/></span></li>

				<li><a href="#" class="sidebar-nav-menu"><i
						class="fa fa-angle-left sidebar-nav-indicator"></i><i
						class="gi gi-crown sidebar-nav-icon"></i> <spring:message code="menu.managecompany"/></a>
					<ul>
						<li><a href="#" id="companyList"><spring:message code="menu.companylist"/></a></li>
						<li><a href="page_ui_typography.html"><spring:message code="menu.createcompany"/></a></li>
					</ul></li>

				<li><a href="#" class="sidebar-nav-menu"><i
						class="fa fa-angle-left sidebar-nav-indicator"></i><i
						class="gi gi-package sidebar-nav-icon"></i> <spring:message code="menu.managebrand"/></a>
					<ul>
						<li><a href="#" id="brandList"><spring:message code="menu.brandlist"/></a></li>
						<li><a href="page_ui_typography.html"><spring:message code="menu.createbrand"/></a></li>
					</ul></li>


				<li><a href="#" class="sidebar-nav-menu"><i
						class="fa fa-angle-left sidebar-nav-indicator"></i><i
						class="fa fa-cutlery sidebar-nav-icon"></i> <spring:message code="menu.managerestaurants"/></a>
					<ul>
						<li><a href="#" id="restaurant"><spring:message code="menu.restaurant"/></a></li>
						<li><a href="page_ui_typography.html"><spring:message code="menu.menu"/></a></li>
						<li><a href="page_ui_buttons_dropdowns.html"><spring:message code="menu.offers"/></a></li>
						<li><a href="page_ui_navigation_more.html"><spring:message code="menu.promocode"/></a></li>
						<li><a href="page_ui_progress_loading.html"><spring:message code="menu.giftameal"/></a></li>
						<li><a href="page_ui_grid_blocks.html"><spring:message code="menu.changerestaurantadmin"/>
								</a></li>
						<li><a href="page_ui_color_themes.html"><spring:message code="menu.restaurantoftheday"/>
								</a></li>
						<li><a href="#" id="cuisine"><spring:message code="menu.cuisine"/></a></li>
					</ul></li>

				<li class="sidebar-header"><span
					class="sidebar-header-options clearfix"><a
						href="javascript:void(0)" data-toggle="tooltip"
						title="Quick Settings"><i class="gi gi-settings"></i></a></span> <span
					class="sidebar-header-title"><spring:message code="menu.membershipmanagement"/></span></li>

				<li><a href="#" class="sidebar-nav-menu"><i
						class="fa fa-angle-left sidebar-nav-indicator"></i><i
						class="fa fa-users sidebar-nav-icon"></i> <spring:message code="menu.managemembers"/></a>
					<ul>
						<li><a href="page_ui_grid_blocks.html"><spring:message code="menu.freemembers"/></a></li>
						<li><a href="page_ui_typography.html"><spring:message code="menu.paidmembers"/></a></li>
						<li><a href="page_ui_typography.html"><spring:message code="menu.corporatemembers"/></a></li>
					</ul></li>

				<li><a href="#" class="sidebar-nav-menu"><i
						class="fa fa-angle-left sidebar-nav-indicator"></i><i
						class="gi gi-usd sidebar-nav-icon"></i> <spring:message code="menu.managerates"/></a>
					<ul>
						<li><a href="#" id="cityWiseRate"><spring:message code="menu.citywiserate"/></a></li>
						<li><a href="#" id="countryWiseRate" ><spring:message code="menu.countrywiserate"/></a></li>
						<li><a href="page_ui_typography.html"><spring:message code="menu.brandwiserate"/></a></li>
					</ul></li>

				<li><a href="#" class="sidebar-nav-menu"><i
						class="fa fa-angle-left sidebar-nav-indicator"></i><i
						class="gi gi-tag sidebar-nav-icon"></i> <spring:message code="menu.discountmodel"/></a>
					<ul>
						<li><a href="page_ui_grid_blocks.html"><spring:message code="menu.citydiscountslab"/></a>
						</li>
						<li><a href="page_ui_typography.html"><spring:message code="menu.countrydiscountslab"/>
								</a></li>
						<li><a href="page_ui_typography.html"><spring:message code="menu.tenurediscountslab"/>
								</a></li>
					</ul></li>



				<li><a href="#" class="sidebar-nav-menu"><i
						class="fa fa-angle-left sidebar-nav-indicator"></i><i
						class="gi gi-crown sidebar-nav-icon"></i> <spring:message code="menu.corporateaccounts"/></a>
					<ul>
						<li><a href="page_ui_grid_blocks.html"><spring:message code="menu.enquiry"/></a></li>
						<li><a href="page_ui_grid_blocks.html"><spring:message code="menu.corporatelist"/></a></li>
						<li><a href="page_ui_typography.html"><spring:message code="menu.createcorporate"/></a></li>
						<li><a href="page_ui_typography.html"><spring:message code="menu.corporateusers"/></a></li>
					</ul></li>


				<li><a href="#" class="sidebar-nav-menu"><i
						class="fa fa-angle-left sidebar-nav-indicator"></i><i
						class="gi gi-tag sidebar-nav-icon"></i> <spring:message code="menu.managevoucher"/></a>
					<ul>
					     <li><a href="#" id="corporateVoucher"><spring:message code="menu.corvoucher"/></a></li>
						 <li><a href="#" id="individualVoucher"><spring:message code="menu.indvoucher"/></a></li>
						 <li><a href="page_ui_grid_blocks.html"><spring:message code="menu.pendingvoucherlist"/>
								</a></li>
					</ul></li>



				<li class="sidebar-header"><span
					class="sidebar-header-options clearfix"><a
						href="javascript:void(0)" data-toggle="tooltip"
						title="Quick Settings"><i class="gi gi-settings"></i></a></span> <span
					class="sidebar-header-title"><spring:message code="menu.managepointssystem"/></span></li>

				<li><a href="page_widgets_social.html"><i
						class="gi gi-cup sidebar-nav-icon"></i> <spring:message code="menu.definepoints"/></a></li>


				<li class="sidebar-header"><span
					class="sidebar-header-options clearfix"><a
						href="javascript:void(0)" data-toggle="tooltip"
						title="Quick Settings"><i class="gi gi-settings"></i></a></span> <span
					class="sidebar-header-title"><spring:message code="menu.admanagement"/></span></li>

				<li><a href="page_widgets_social.html"><i
						class="gi gi-share_alt sidebar-nav-icon"></i> <spring:message code="menu.manage.adslots"/></a></li>

				<li><a href="page_widgets_social.html"><i
						class="gi gi-share_alt sidebar-nav-icon"></i> <spring:message code="menu.enquiry"/></a></li>



				<li class="sidebar-header"><span
					class="sidebar-header-options clearfix"><a
						href="javascript:void(0)" data-toggle="tooltip"
						title="Quick Settings"><i class="gi gi-settings"></i></a></span> <span
					class="sidebar-header-title"><spring:message code="menu.usermanagement"/></span></li>


				<li><a href="#" class="sidebar-nav-menu"><i
						class="fa fa-angle-left sidebar-nav-indicator"></i><i
						class="gi gi-user sidebar-nav-icon"></i> <spring:message code="menu.manageuser"/></a>
					<ul>
						<li><a href="page_ui_grid_blocks.html"><spring:message code="menu.userlist"/></a></li>
					</ul></li>


				<li class="sidebar-header"><span
					class="sidebar-header-options clearfix"><a
						href="javascript:void(0)" data-toggle="tooltip"
						title="Quick Settings"><i class="gi gi-settings"></i></a></span> <span
					class="sidebar-header-title"><spring:message code="menu.masters"/></span></li>


				<li><a href="#" class="sidebar-nav-menu"><i
						class="fa fa-angle-left sidebar-nav-indicator"></i><i
						class="fa fa-map-marker sidebar-nav-icon"></i> <spring:message code="menu.managelocations"/></a>
					<ul>
						<li><a href="#" id="area"><spring:message code="menu.area"/></a></li>
						<li><a href="#" id="city"><spring:message code="menu.city"/></a></li>
						<li><a href="#" id="country"><spring:message code="menu.country"/></a></li>
					</ul></li>


				<li><a href="#" class="sidebar-nav-menu"><i
						class="fa fa-angle-left sidebar-nav-indicator"></i><i
						class="gi gi-usd sidebar-nav-icon"></i> <spring:message code="menu.managecurrency"/></a>
					<ul>
						<li><a href="#" id="currency"><spring:message code="menu.currencymaster"/></a></li>
						<li><a href="#" id="currencyExchangeRate"><spring:message code="menu.exchangerate"/></a></li>
					</ul></li>
					
				<li><a href="#" class="sidebar-nav-menu"><i
						class="fa fa-angle-left sidebar-nav-indicator"></i><i
						class="gi gi-usd sidebar-nav-icon"></i> <spring:message code="menu.managelanguage"/></a>
						<ul>
						<li><a href="#" id="language"><spring:message code="menu.language"/></a></li>
					</ul>
				</li>
			</ul>
			<!--
<div class="sidebar-header">
<span class="sidebar-header-options clearfix">
<a href="javascript:void(0)" data-toggle="tooltip" title="Refresh"><i class="gi gi-refresh"></i></a>
</span>
<span class="sidebar-header-title">Activity</span>
</div>
<div class="sidebar-section">
<div class="alert alert-success alert-alt">
<small>5 min ago</small><br>
<i class="fa fa-thumbs-up fa-fw"></i> You had a new sale ($10)
</div>
<div class="alert alert-info alert-alt">
<small>10 min ago</small><br>
<i class="fa fa-arrow-up fa-fw"></i> Upgraded to Pro plan
</div>
<div class="alert alert-warning alert-alt">
<small>3 hours ago</small><br>
<i class="fa fa-exclamation fa-fw"></i> Running low on space<br><strong>18GB in use</strong> 2GB left
</div>
<div class="alert alert-danger alert-alt">
<small>Yesterday</small><br>
<i class="fa fa-bug fa-fw"></i> <a href="javascript:void(0)">New bug submitted</a>
</div>
</div> 
  -->
		</div>
	</div>
</div>

