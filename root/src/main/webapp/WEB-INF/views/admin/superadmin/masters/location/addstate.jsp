<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@include file="../../../inc/config.jsp"%>
<%@include file="../../../inc/template_start.jsp"%>
<%@include file="../../../inc/page_head.jsp"%>

<style>

.chosen-container {
	width: 250px !important;
}
</style>
<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				<i class="fa fa-map-marker"></i>
				<spring:message code="heading.statemaster" />
				<br> <small><spring:message code="heading.createstate" />
				</small>
			</h1>
			<span id="errorMsg"></span>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" id="success_close" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i> <spring:message code="label.success"/>
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>
			<c:if test="${!empty error}">
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert"
					aria-hidden="true">x</button>
				<h4>
					<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
				</h4>
				<spring:message code="${error}" />
			</div>
		</c:if>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="menu.managelocations" /></li>
		<li><a href="#"><spring:message code="label.state" /></a></li>
	</ul>
	
	
	<div class="block full" id="formView" >
		<div class="block-title">
			<h2>
				<strong><spring:message code="label.addstatemaster" /></strong>
			</h2>
		</div>
		<form action="<%=contexturl %>ManageState/SaveState" method="post" class="form-horizontal form-bordered"
			id="State_form"  enctype="multipart/form-data">
			
			
						<div class="form-group">
				<input id="state_Id" name="stateId" type="hidden" value="0"> <label
					class="col-md-3 control-label" for="stateName"><spring:message
						code="label.statename" /><span class="text-danger">*</span></label>
				 <div class="col-md-9">
					<input id="state_Name" name="stateName" class="form-control"
						placeholder="<spring:message code="label.statename"/>.."
						type="text" value="${state.stateName}">
				</div> 
			</div>
			
			
			<div class="form-group" id="countryDiv">
				<label class="col-md-3 control-label" for="countryFrom_Name"><spring:message code="label.country"/><span class="text-danger">*</span></label>
						<div class="col-md-9">
							<select class="chosen" style="width:400px;" id="fromCountryId" name="chosenCountryId" data-placeholder="<spring:message code='label.choosecountry' />">
								<option value=""></option>
								<c:forEach items="${countryList}" var="country">
											<option value="${country.countryId}" <c:if test="${country.countryId eq state.country.countryId}" >selected="selected"</c:if>>${country.countryName}</option>
								</c:forEach>
							</select>
						</div>
				
			</div>


				
				<div class="form-group form-actions">
				<div class="col-md-9 col-md-offset-3">
					<div id="state_submit" class="btn btn-sm btn-primary save">
						<i class="fa fa-angle-right"></i> <spring:message code="button.save" />
					</div>
					<a class="btn btn-sm btn-primary" id="cancel" href="<%=contexturl %>ManageState/StateList">
						
						<spring:message code="button.cancel" />
					</a>
				</div>
			</div>
		</form>
	</div>
</div>


<%@include file="../../../inc/page_footer.jsp"%>
<%@include file="../../../inc/template_scripts.jsp"%>

<script type="text/javascript">
$(document).ready(function() {
	
	$(".chosen").data("placeholder","Select Country From...").chosen();
	$("#manageLocation").addClass("active");
	$("#State_form").validate(
			{	errorClass:"help-block animation-slideDown",
				errorElement:"div",
				errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
				highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
				success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
				rules:{ stateName:{required:!0,maxlength : 75},	
					chosenCountryId:{required:!0}
						
				},
				messages:{
					stateName:{required:'<spring:message code="validation.state"/>',
						maxlength :'<spring:message code="validation.state75character"/>'},
						chosenCountryId:{required:'<spring:message code="validation.country"/>'}
				}
					
				
			});
		
		 $("#state_submit").click(function(){
			 $("#State_form").submit();
		 });
		
		
	});
	
 
	
</script>
<%@include file="../../../inc/template_end.jsp"%>