<%@page import="com.astrika.common.model.location.CityMaster"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@include file="../../../inc/config.jsp"%>
<%@include file="../../../inc/template_start.jsp"%>
<%@include file="../../../inc/page_head.jsp"%>

<style>

.chosen-container {
	width: 250px !important;
}
 #map-canvas { height: 300px;}
</style>
<div id="page-content">
<div class="content-header">
	<div class="header-section">
		<h1>
			<i class="fa fa-map-marker"></i>
			<spring:message code="heading.citymaster" />
			<br> <small><spring:message
					code="heading.createcity" /> </small>
		</h1>
		<span id="errorMsg"></span>
		<c:if test="${!empty success}">
			<div class="alert alert-success alert-dismissable">
				<button type="button" id="success_close" class="close" data-dismiss="alert"
					aria-hidden="true">x</button>
				<h4>
					<i class="fa fa-check-circle"></i> <spring:message code="label.success"/>
				</h4>
				<spring:message code="${success}" />
			</div>
		</c:if>
		<c:if test="${!empty error}">
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert"
					aria-hidden="true">x</button>
				<h4>
					<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
				</h4>
				<spring:message code="${error}" />
			</div>
		</c:if>
	</div>
</div>


<div class="block full" id="formView" >
	<div class="block-title">
		<h2>
			<strong><spring:message code="label.add.citymaster" /></strong>
		</h2>
	</div>
	<form action="<%=contexturl %>ManageCity/SaveCity" method="post" class="form-horizontal form-bordered"
		id="City_form">
		<div class="form-group">
			<input id="city_Id" name="cityId" type="hidden" value="0"> <label
				class="col-md-3 control-label" for="cityName"><spring:message
					code="label.cityname" /><span class="text-danger">*</span></label>
			<div class="col-md-9">
				<input id="city_Name" name="cityName" class="form-control"
					placeholder="<spring:message code="label.cityname"/>.."
					type="text" value = "${city.cityName}">
			</div>
		</div>


		<div class="form-group" id="countryDiv">
			<label class="col-md-3 control-label" for="city1_Name"><spring:message
					code="label.country" /><span class="text-danger">*</span>
			</label>
			<div class="col-md-9">
				<select class="chosen" style="width: 400px;" id="fromCountryId"
					name="chosenCountryId"
					data-placeholder="<spring:message code='label.choosecountry' />">
					<option value=""></option>
					<c:forEach items="${countryList}" var="country">
						<option value="${country.countryId}" <c:if test="${city.country.countryId eq country.countryId}">selected="selected"</c:if>>${country.countryName}</option>
					</c:forEach>
				</select>
			</div>
		</div>

		<div class="form-group" id="stateDiv">
				<label class="col-md-3 control-label" for="stateName"><spring:message code="label.state"/><span class="text-danger">*</span></label>
						<div class="col-md-9">
							<select class="state_chosen" style="width:400px;" id="stateId" name="chosenStateId" data-placeholder="<spring:message code='label.choosestate' />">
								<option value=""></option>
								<c:forEach items="${stateList}" var="state">
											<option value="${state.stateId}" <c:if test="${city.state.stateId eq state.stateId}">selected="selected"</c:if>>${state.stateName}</option>
								</c:forEach>
							</select>
						</div>	
		</div>



		
		
		

		<div class="form-group form-actions">
			<div class="col-md-9 col-md-offset-3">
				<div id="city_submit" class="btn btn-sm btn-primary save">
					<i class="fa fa-angle-right"></i> <spring:message code="button.save" />
				</div>
<!-- 				<button type="reset" class="btn btn-sm btn-warning"> -->
<%-- 						<i class="fa fa-repeat"></i> <spring:message code="button.reset" /> --%>
<!-- 					</button> -->
				<a class="btn btn-sm btn-primary" id="cancel" href="<%=contexturl %>ManageCity/City">
					
					<spring:message code="button.cancel" />
				</a>
			</div>
		</div>
	</form>
</div>
</div>
<%@include file="../../../inc/page_footer.jsp"%>
<%@include file="../../../inc/template_scripts.jsp"%>
<%@include file="../../../inc/chosen_scripts.jsp"%>
 <script type="text/javascript"
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCheQF_yjKacwuVL4_59EO-3BThy85gbu4&sensor=false&region=GB">
    </script>
 	
<script type="text/javascript">
	$(document).ready(function() {
		$("#manageLocation").addClass("active");
		
		$(".chosen").data("placeholder","Select Country From...").chosen();
		
		$(".state_chosen").data("placeholder","Select State From...").chosen();
		
		$('select[name="chosenCountryId"]').chosen().change( function() {
			changeCountry($(this).val());
		});
		
		
			$("#City_form").validate(
			{	errorClass:"help-block animation-slideDown",
				errorElement:"div",
				errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
				highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
				success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
				rules:{ cityName:{required:!0,maxlength : 75},
						chosenStateId:{required:!0},	
						chosenCountryId:{required:!0,maxlength : 75},
						
				},
				messages:{cityName:{required:'<spring:message code="validation.city"/>',
					maxlength :'<spring:message code="validation.city75character"/>'},
					chosenStateId:{required:'<spring:message code="validation.selectstate"/>'},	
					chosenCountryId:{required:'<spring:message code="validation.selectcountry"/>'},
					
				},
			
			});
			
			 $("#city_submit").click(function(){
				 $("#City_form").submit();
			 });
			 
				 $("button[type=reset]").click(function(evt) {
					evt.preventDefault(); 
					form = $(this).parents("form").first(); 
					form[0].reset(); 
					form.find(".chosen").trigger("chosen:updated");
					var myOptions = "<option value></option>";
					$(".state_chosen").html(myOptions).chosen().trigger("chosen:updated");
				});
	});
	
	function changeCountry(countryId){
		$.ajax({
			type: "GET",
			async: false,
			url: "<%=contexturl %>state/statebycountryid/"+countryId,
			success: function(data, textStatus, jqXHR){
				var obj = jQuery.parseJSON(data);
				var len=obj.length;
				var myOptions = "<option value></option>";
				for(var i=0; i<len; i++){
				myOptions += '<option value="'+obj[i].stateId+'">'+obj[i].stateName+'</option>';
				}
				$(".state_chosen").html(myOptions).chosen().trigger("chosen:updated");
			},
			dataType: 'html'
		});
	}
</script>
<%@include file="../../../inc/template_end.jsp"%>