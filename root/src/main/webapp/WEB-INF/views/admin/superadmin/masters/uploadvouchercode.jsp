<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->

<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>

<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>
			<h1>
				<i class="fa fa-map-marker"></i>
				<spring:message code="heading.voucher" />
				<br> <small>
				</small>
			</h1>
			<span id="errorMsg"></span>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="heading.promocodeDescription" />
		</li>
	</ul>
	<div class="block full" id="formView">
		<form action="<%=contexturl %>ManageVoucher/IndividualVoucher/GenerateVoucherCodesFromCSV" method="post" class="form-horizontal form-bordered"
			id="uploadVoucher_form" enctype="multipart/form-data">
			<div class="form-group">
				<label class="col-md-3 control-label" >
					Sample Voucher Code</label>
				<div class="col-md-8">
                    <p class="form-control-static">
                    	<a href="<%=contexturl %>resources/files/samplevouchercode.csv"
							download="samplevouchercode.csv"><spring:message
							code="label.download" />
						</a>
                    </p>
                </div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" >
					Upload Vouchers<span class="text-danger">*</span></label>
				<div class="col-md-9">
					<input type="file" name="voucherFile" id="voucherFile"
						accept=".csv"
						style="padding-top: 7px;" />
				</div>
			</div>

			<div class="form-group form-actions">
				<div class="col-md-9 col-md-offset-3">
					<button id="submit" type="submit"
						class="btn btn-sm btn-primary">
						<i class="fa fa-angle-right"></i> <spring:message code="button.save" />
					</button>
					<a class="btn btn-sm btn-primary" href="<%=contexturl %>ManageVoucher/IndividualVoucher/"><i class="gi gi-remove"></i>
						<spring:message code="button.cancel" /> </a>
				</div>
			</div>
		</form>
	</div>
</div>
<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>

<script type="text/javascript">
	$(document).ready(function() {
		
		$("#uploadVoucher_form").validate(
				{	errorClass:"help-block animation-slideDown",
					errorElement:"div",
					errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
					highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
					success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
					rules:{ 
						voucherFile:{required:!0},
							
					},
					messages:{
						
						voucherFile:{required:'<spring:message code="validation.voucherFile"/>'},
							
					},
					
				});
		
		
		
	});
</script>
<%@include file="../../inc/template_end.jsp"%>