<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@include file="../../../inc/config.jsp"%>
<style>

.chosen-container {
	width: 250px !important;
}
</style>
	<div class="content-header">
		<div class="header-section">
			<h1>
				<spring:message code="heading.countrymaster" /><br> 
<%-- 				<small><spring:message code="heading.specifylistofcountries" /> --%>
<!-- 					 </small> -->
			</h1>
			<span id="errorMsg"></span>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" id="success_close" class="close" data-dismiss="alert" aria-hidden="true">x</button>
					<h4><i class="fa fa-check-circle"></i> <spring:message code="label.success"/></h4> <spring:message code="${success}"/>
				</div>
			</c:if>
			<c:if test="${!empty error}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" id="success_close" class="close" data-dismiss="alert" aria-hidden="true">x</button>
					<h4><i class="fa fa-check-circle"></i> Error</h4> <spring:message code="${error}"/>
				</div>
			</c:if>
			<div class="alert alert-danger alert-dismissable" style="display: none" id="error">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="fa fa-times-circle">
				</i> <spring:message code="label.error"/></h4> <span id="errorMsg"></span>
			</div>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="menu.managelocations" /></li>
		<li><a href="#"><spring:message code="label.country"/></a></li>
	</ul>
	<div class="block full gridView">
		<div class="block-title">
			<h2>
				<strong><spring:message code="heading.activecountry" /></strong> 
			</h2>
		</div>
		<a id="addCountry" class="btn btn-sm btn-primary save" href="#">
						<i class="fa fa-angle-right"></i> <spring:message code="label.addcountry" />
		</a>
<%-- 		<a href="#" id="addCountry"><spring:message code="label.addcountry"/></a> --%>
		<!--<p><a href="https://datatables.net/" target="_blank">DataTables</a> is a plug-in for the Jquery Javascript library. It is a highly flexible tool, based upon the foundations of progressive enhancement, which will add advanced interaction controls to any HTML table. It is integrated with template's design and it offers many features such as on-the-fly filtering and variable length pagination.</p> -->
		<div class="table-responsive">
			<table id="active-datatable"
				class="table table-vcenter table-condensed table-bordered">
				<thead>
					<tr>
						<th class="text-center"><spring:message code="label.id" /></th>
						<th class="text-center"><spring:message code="label.country" /></th>
<%-- 						<th class="text-center"><spring:message code="label.currency" /></th> --%>						<th class="text-center"><spring:message code="label.actions" /></th>
					</tr>
				</thead>
				<tbody id="tbody">
					<c:forEach var="currentCountry" items="${countryList}" varStatus="count">
						<tr>
							<td class="text-center">${count.count}</td>
							<td class="text-left"><c:out value="${currentCountry.countryName}" /></td>
<%-- 							<td class="text-left"><c:out value="${currentCountry.currency.currencyName}" /></td> --%>
							<td class="text-center">
								<div class="btn-group">
									<a href="#"  data-toggle="tooltip" title="Edit" onclick="editCountry(${currentCountry.countryId},${count.count})"
										class="btn btn-xs btn-default save"><i class="fa fa-pencil"></i></a>
									<a href="#delete_country_popup" data-toggle="modal"
										 onclick="deleteCountry(${currentCountry.countryId})"
										 title="Delete" class="btn btn-xs btn-danger save "><i
										class="fa fa-times"></i></a>
								</div>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	<div class="block full gridView">
	
		<div class="block-title">
			<h2>
				<strong><spring:message code="heading.inactivecountry" /></strong> 
			</h2>
		</div>
		<div class="table-responsive">
			<table id="inactive-datatable"
				class="table table-vcenter table-condensed table-bordered">
				<thead>
					<tr>
						<th class="text-center"><spring:message code="label.id" /></th>
						<th class="text-center"><spring:message code="label.country" /></th>
<%-- 						<th class="text-center"><spring:message code="label.currency" /></th> --%>
						<th class="text-center"><spring:message code="label.actions" /></th>
					</tr>
				</thead>
				<tbody id="tablebody">
					<c:forEach var="currentCountry" items="${inActiveCountryList}"
						varStatus="count">
						<tr>
							<td class="text-center">${count.count}</td>
							<td class="text-left"><c:out value="${currentCountry.countryName}" /></td>
<%-- 							<td class="text-left"><c:out value="${currentCountry.currency.currencyName}" /></td> --%>
							<td class="text-center">
								<div class="btn-group">
									<a href="#restore_Country_popup" data-toggle="modal"
										onclick="restoreCountry(${currentCountry.countryId})"
										title="Restore" class="btn btn-xs btn-success"><i
										class="fa fa-plus"></i> </a>
								</div>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>


	</div>
	
	<div id="delete_country_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="" method="post" class="form-horizontal form-bordered">
						<div style="padding: 10px; height: 110px;">
							<label><spring:message code="validation.doyouwanttodeletethiscountry"/></label>
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal"><spring:message code="label.no"/></button>
								<div id="delete_country" class="btn btn-sm btn-primary save"><spring:message code="label.yes"/></div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	
	<div id="restore_Country_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="" method="post" class="form-horizontal form-bordered">
						<div style="padding: 10px; height: 110px;">
							<label><spring:message code="validation.doyouwanttorestorethiscountry"/></label>
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal"><spring:message code="label.no"/></button>
								<div id="restore_Country" class="btn btn-sm btn-primary save"><spring:message code="label.yes"/></div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	
	
	<div class="block full" id="formView" style="display: none">
		<div class="block-title" id="addHeading">
			<h2>
				<strong><spring:message code="label.addcountrymaster" /></strong>
			</h2>
		</div>
		<div class="block-title" id="editHeading">
			<h2>
				<strong><spring:message code="label.editcountry" /></strong>
			</h2>
		</div>
		<form action="" method="post"
			class="form-horizontal form-bordered save" id="Country_form" >
			<div class="form-group">
				<input id="country_Id" name="countryId" 
						 type="hidden" value="0">
				<label class="col-md-3 control-label" for="country_Name"><spring:message code="label.countryname" /><span class="text-danger">*</span></label>
				<div class="col-md-9">
					<input id="country_Name" name="countryName" class="form-control"
						placeholder="<spring:message code="label.countryname"/>.." type="text">
					<!-- 								<span class="help-block">Please enter your email</span> -->
				</div>
				</div>
<!-- 				<div class="form-group" id="currencyDiv"> -->
<%-- 				<label class="col-md-3 control-label" for="currencyFrom_Name"><spring:message code="label.currency"/><span class="text-danger">*</span></label> --%>
<!-- 						<div class="col-md-9"> -->
<%-- 							<select class="chosen" style="width:400px;" id="fromCurrencyId" name="chosenCurrencyId" data-placeholder="<spring:message code='label.choosecurrency' />"> --%>
<!-- 								<option value=""></option> -->
<%-- 								<c:forEach items="${currencyList}" var="currency"> --%>
<%-- 									<option value="${currency.currencyId}" >${currency.currencyName}</option> --%>
<%-- 								</c:forEach> --%>
<!-- 							</select> -->
<!-- 						</div> -->
				
<!-- 			</div> -->

			<div class="form-group form-actions">
				<div class="col-md-9 col-md-offset-3">
					<div id="country_submit" class="btn btn-sm btn-primary save">
						<i class="fa fa-angle-right"></i> <spring:message code="button.save" />
					</div>
<!-- 					<button type="reset" class="btn btn-sm btn-warning"> -->
<%-- 						<i class="fa fa-repeat"></i> <spring:message code="button.reset" /> --%>
<!-- 					</button> -->
					<a class="btn btn-sm btn-primary save" id="cancel" href="#">
						<spring:message code="button.cancel" />					</a>
				</div>
			</div>
		</form>
	</div>

<script src="<%=contexturl %>resources/js/pages/activeTablesDatatables.js"></script>
<script src="<%=contexturl %>resources/js/pages/inactiveTablesDatatables.js"></script>
<script>$(function(){ ActiveTablesDatatables.init(2); });</script>
<script>$(function(){ InActiveTablesDatatables.init(2); });</script>



<script type="text/javascript">

	$(document).ready(function() {
		$("#manageLocation").addClass("active");
	
// 		$(".chosen").data("placeholder","Select Currency From...").chosen();
		
		$("#addCountry").click(function() {
			$('#Country_form').find("input[type=text], textarea").val("");
			$('#Country_form').find("input[type=hidden], textarea").val("0");
			$("#formView").css("display", "block");
			$(".gridView").css("display", "none");
			$("#addHeading").css("display", "block");
			$("#editHeading").css("display", "none");
			$("#success_close").click();
		});
		
		$("#cancel").click(function() {
			$("#formView").css("display", "none");
			$(".gridView").css("display", "block");
			$("#addHeading").css("display", "none");
			$("#editHeading").css("display", "none");
			$('.help-block').css('display', 'none');
			$("div.form-group").removeClass('has-error');
			$("#success_close").click();
		});
		
		
		$("#restore_Country").click(function(){
			$.ajax({
				type: "GET",
				async: false,
				url: "<%=contexturl%>Admin/RestoreCountry?id="+restoreId,
				success: function(data, textStatus, jqXHR){
					$('#page-content').html(data);
					$('div').removeClass("modal-backdrop fade in");
					$('body').removeClass('modal-open');
					$('.modal-backdrop').remove();
					$('#restore_Country_popup').modal('hide');	
					$('html, body').animate({ scrollTop: $("#errorMsg").offset().top },500);
				},
				dataType: 'html'
			});
		});
		
		$("#delete_country").click(function(){
			$.ajax({
				type: "GET",
				async: false,
				url: "<%=contexturl%>Admin/DeleteCountry?id="+selectedId,
				success: function(data, textStatus, jqXHR){
					$('#page-content').html(data);
					$('body').removeClass('modal-open');
					$('.modal-backdrop').remove();
					$('#delete_country_popup').modal('hide');	
					$('html, body').animate({ scrollTop: $("#errorMsg").offset().top },500);
				},
				dataType: 'html'
			});
		});
		
		$("#Country_form").validate(
				{	errorClass:"help-block animation-slideDown",
					errorElement:"div",
					errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
					highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
					success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
					rules:{countryName:{required:!0,maxlength : 75}
// 						   chosenCurrencyId:{required:!0}
						
					},
					messages:{
						countryName:{required:'<spring:message code="validation.mentioncountry"/>',
						maxlength :'<spring:message code="validation.country75character"/>'}
// 						chosenCurrencyId:{required:'<spring:message code="validation.currency"/>'}
						
					},
					submitHandler: function() {
						
						var data = $("#Country_form").serialize();
						$.ajax({
							type: "POST",
							async: false,
							url: "<%=contexturl%>Admin/SaveCountry",
							data:data,
							success: function(data, textStatus, jqXHR){
								var i = data.trim().indexOf("errorCode");
								if(i == 2){
									var obj = $.parseJSON(data.trim());
									var html = '<div class="alert alert-danger alert-dismissable">	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="fa fa-times-circle"></i> Error</h4> <span >'+obj.errorCode+'</span></div>';
									$("#errorMsg").html(html).show();
									$("#errorMsg").html(html).fadeOut(8000);
									$('html, body').animate({ scrollTop: $("#errorMsg").offset().top },500);
								}
								else{
									$('html, body').animate({ scrollTop: $("#errorMsg").offset().top },500);
									$('#page-content').html(data);
								}
							},
							dataType: 'html'
						});
	                }
				});
		
		
	
		 $("#country_submit").click(function(){
			 $("#Country_form").submit();
		 });
		
				

	});
	function editCountry(id,position){
		var oTable = $("#active-datatable").dataTable();
		var data = oTable.fnGetData()[position-1];
		$("#country_Id").val(id);
		$("#country_Name").val(data[1]);
		$('select[name="chosenCurrencyId"]').find('option:contains("'+data[2]+'")').attr("selected",true);
		$('select[name="chosenCurrencyId"]').trigger("chosen:updated");	
		
		$("#formView").css("display", "block");
		$(".gridView").css("display", "none");
		$('.help-block').css('display', 'none');
		$("#addHeading").css("display", "none");
		$("#editHeading").css("display", "block");
		$("div.form-group").removeClass('has-error');
		$("#success_close").click();
	}
	
	function  deleteCountry(id){
		selectedId = id;
	}
	function restoreCountry(id){
		restoreId = id;
	}
</script>
