<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->



<%@include file="../inc/config.jsp"%>
<%@include file="../inc/template_start.jsp"%>
<%@include file="../inc/page_head.jsp"%>


<script src="<%=contexturl%>resources/js/Chart.js"></script>


<script src="<%=contexturl%>resources/js/canvasjs.min.js"></script>

<style>

	.chart-legend li span {
    display: inline-block; 
	width: 12px;
	height: 12px;
	margin-right: 5px;
}

.chart-legends {
    display: inline-block; 
	width: 12px;
	height: 12px;
	margin-right: 5px;
}


</style>


<div id="page-content">
	<div class="row">
	<a href="<%=contexturl%>historyReportExcel" class="btn btn-primary" style="float: right;margin: 1%;">DownLoad History Report</a>
	</div>
	<div class="row">
		<div class="well">
			<div class="row">
				<div class="col-md-4 col-xs-4 col-lg-4"
					style="border: 2px solid lightgray; height: 215px;">
					<div class="row" style="text-align: center;">
						<h3>
							<small>Total Organisations</small>
						</h3>
					</div>
					<div class="row">
						<div class="col-md-6 col-xs-6 col-lg-6" style="text-align: center;">
							<canvas id="canvasone" height="150" width="130"></canvas>
							<div class="donut-innerSuperTwo" >
								<h4 style="color: #21DC54;">${totalOraganisation}</h4>
							</div>
						</div>
						<div class="col-md-6 col-xs-6 col-lg-6">
							<div class="col-md-1 col-xs-1 col-lg-1"></div>
							<div class="col-md-11 col-xs-11 col-lg-11" style="margin-top: -18px;">

								<div class="row" style="padding-top: 33px;">
									<h4>
										<small>last month: ${lastMonthActiveOrganisation}</small>
									</h4>
								</div>
								<div class="row">
									<h4>
										<small>2nd Last Month:
											${secondLastMonthActiveOrganisation}</small>
									</h4>
								</div>
								<div class="row">
									<h4>
										<small>% Increase:
											${increaseoaganisationPercentage}%</small>
									</h4>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-4 col-xs-4 col-lg-4"
					style="border: 2px solid lightgray; height: 215px;">
					<div class="row" style="text-align: center;">
						<h3>
							<small>Total Users </small>
						</h3>
					</div>
					<div class="row">
						<div class="col-md-6 col-xs-6 col-lg-6" style="text-align: center;">
							<canvas id="canvastwo" height="150" width="130"></canvas>
							<div class="donut-innerSuperTwo">
								<h4 style="color: #21DC54;">${totalUser}</h4>
							</div>
						</div>
						<div class="col-md-6 col-xs-6 col-lg-6">
							<div class="col-md-1 col-xs-1 col-lg-1"></div>
							<div class="col-md-11 col-xs-11 col-lg-11" style="margin-top: -18px;">

								<div class="row" style="padding-top: 33px;">
									<h4>
										<small>Last Month: ${lastMonthCreatedUser}</small>
									</h4>
								</div>
								<div class="row">
									<h4>
										<small>2nd Last Month:
											${secondLastMonthCreatedUser}</small>
									</h4>
								</div>
								<div class="row">
									<h4>
										<small>% Increase: ${resultInpercentage}</small>
									</h4>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-xs-4 col-lg-4"
					style="border: 2px solid lightgray; height: 215px;">
					<div class="row" style="text-align: center;">
						<h3>
							<small>Total Downloads</small>
						</h3>
					</div>
					<div class="row">
						<div class="col-md-6 col-xs-6 col-lg-6" style="text-align: center;">
							<canvas id="canvasthree" height="150" width="130"></canvas>
							<div class="donut-innerSuperTwo">
								<h4 style="color: #21DC54;">${totalDownloads}</h4>
							</div>
						</div>
						<div class="col-md-6 col-xs-6 col-lg-6">
							<div class="col-md-1 col-xs-1 col-lg-1"></div>
							<div class="col-md-11 col-xs-11 col-lg-11" style="margin-top: -18px;">

								<div class="row" style="padding-top: 33px;">
									<h4>
										<small >Last Month: ${oneMonthDownload}</small>
									</h4>
								</div>
								<div class="row">
									<h4>
										<small>2nd Last Month: ${secondLastMonthDownloads}</small>
									</h4>
								</div>
								<div class="row">
									<h4>
										<small>% Increase: ${percentageIncreaseDownloads}</small>
									</h4>
								</div>
							</div>
						</div>
					</div>
				</div>


			</div>
		</div>
	</div>
	<!-- END Dashboard Header -->

	<!-- Widgets Row -->
	<div class="row">
		<div class="row">
			<div class="col-md-6">
				<div class="widget">
					<div class="widget-extra themed-background-dark">
						<h4 class="widget-content-light">
							<strong>Profile Completion</strong> Chart
						</h4>
					</div>
					<div class="block">
						<div class="row">
							<div class="col-sm-1">
								<div
									style="position: relative; height: 200px; margin-top: 135px;">
									<img class="no-of-user"
										src="<%=contexturl%>resources/img/noOfUser.png" />
								</div>
							</div>
							<div class="col-sm-11">
								<canvas id="myChart" width="400" height="400"></canvas>
								<div class="text-center" style="padding: 5px 0px 15px;">
									<img src="<%=contexturl%>resources/img/profileCom.png" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="widget">
					<div class="widget-extra themed-background-dark">
						<h4 class="widget-content-light">
							<strong>Inactive User And Organisation</strong> Chart<small>
								<a href="<%=contexturl%>DownloadInfoExcel"> <strong>Download
										All</strong>
							</a> <%-- <a href="<%=contexturl%>DownloadInfoExcel"
														download="Reality GivesReality Gives_LSSRating.csv"><spring:message
															code="label.download" /> </a> --%>
							</small>
						</h4>
					</div>
					<div class="block">
						<div class="row">
							<div class="col-sm-1">
								<div
									style="position: relative; height: 200px; margin-top: 135px;">
									<img class="no-of-user"
										src="<%=contexturl%>resources/img/noOFUsersAndOrg3.png" />
								</div>
							</div>
							<div class="col-sm-7">
								<a href="<%=contexturl%>InactiveUserOrganisation">
									<canvas id="myChart11" width="400" height="400"></canvas>
								</a>
								<div class="text-center" style="padding: 5px 0px 15px;">
									<img
										src="<%=contexturl%>resources/img/text2image_X31693_20160229_070637.png" />
								</div>
							</div>
							<div class="col-sm-4" style="margin-top: 47px;">
								<div id="js-legend" class="chart-legend"></div>
							</div>

						</div>
					</div>

				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="widget">

					<div class="widget-extra themed-background-dark">
						<h4 class="widget-content-light">
							<strong>Life Stage Survey</strong> Chart
						</h4>
					</div>
					<div class="block">

						<div class="row">
							<div class="col-md-offset-2 col-md-2">
								<select id="monthOftheYear" name="month" class="form-control"
									size="1">
									<option value=""></option>
									<c:forEach items="${monthList}" var="monthName"
										varStatus="counter">
										<c:if test="${not empty monthName}">
											<option value="${counter.count}"
												<c:if test="${monthName eq currentMonth}">selected="selected"</c:if>>${monthName}</option>
										</c:if>
									</c:forEach>
								</select>
							</div>
							<div class="col-md-4">
								<select id="year" name="year" class="form-control" size="1">
									<option value=""></option>
									<c:forEach items="${yearList}" var="yearName">
										<option value="${yearName}"
											<c:if test="${yearName eq currentYear}">selected="selected"</c:if>>${yearName}</option>
									</c:forEach>
								</select>
							</div>
							<div class="col-md-2">
								<button id="personalFormId" type="button"
									class="btn btn-default" onclick="getinfo()">Go</button>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-1">
								<div
									style="position: relative; height: 200px; margin-top: 135px;">
									<img class="no-of-user"
										src="<%=contexturl%>resources/img/noOfCom.png" />
								</div>
							</div>
							<div class="col-sm-9">
								<canvas id="myChart3" width="800" height="400"></canvas>
								<div class="text-center" style="padding: 5px 0px 15px;">
									<img src="<%=contexturl%>resources/img/modul.png" />
								</div>
							</div>
							<div class="col-sm-2" style="margin-top: 21px;">
							  <div id="js-legend2" class="chart-legend"></div>
						    </div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="widget">
					<div class="widget-extra themed-background-dark">
						<h4 class="widget-content-light">
							<strong>Organisational Development </strong>Chart
						</h4>
					</div>
					<div class="block">
						<div class="table-responsive" id="odaTable">
							<table id="general-table" class="table table-vcenter table-hover">
								<thead>
									<tr>
										<th class="text-left">Name of the ODA</th>
										<th class="text-center">No. of Users assigned</th>
										<th class="text-center">No. of Downloads</th>
										<th class="text-center">No. of Forum Interactions</th>
										<th class="text-left">Most Popular Download</th>
									</tr>
								</thead>
								<tbody id="tbody" class="TFtable">
									<c:forEach var="module" items="${moduleList}"
										varStatus="counter">
										<tr>
											<td class="text-left">${module.moduleName}</td>
											<td class="text-center">${userperModule[counter.index]}</td>
											<td class="text-center">${countperModule[counter.index]}</td>
											<td class="text-center">${forumperModule[counter.index]}</td>
											<td class="text-left">${documentList[counter.index].title}</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="widget">
					<div class="widget-extra themed-background-dark">
						<h4 class="widget-content-light">
							<strong>NGO Sector </strong>Chart
						</h4>
					</div>
					<div class="block">
						<div class="row">

							<div class="col-sm-12">
								<div id="chartContainer" style="height: 300px; width: 100%;">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="widget">
					<div class="widget-extra themed-background-dark">
						<h4 class="widget-content-light">
							<strong>Geographical Distribution Analysis </strong> <small> <!-- <a href="javaScript:{openPopUp();}"> -->
								<a href="<%=contexturl%>CompleteCityInfo"> <strong>View
										All</strong>
							</a>
							</small>
						
					</div>

					<div class="block">

						<div class="table-responsive">
							<table id="general-table" class="table table-vcenter table-hover">
								<thead>
									<tr>
										<th class="text-left">City Name</th>
										<th class="text-center">No Of Cities</th>
									</tr>
								</thead>
								<tbody id="tbody" class="TFtable">
									<c:forEach var="org1" items="${cmjson}" varStatus="count">
										<tr>
											<td class="text-left">${org1.city_cityName}</td>
											<td class="text-center">${org1.companyId}
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>


		</div>

	</div>
	
	
		<div id="reportPopup" class="modal fade" tabindex="-1"role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="<%=contexturl %>" method="post" class="form-horizontal form-bordered save"
						id="delete_company_Form">
						<div style="padding: 10px; height: 110px;">
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal">search
								</button>
								<%-- <div id="delete_company" class="btn btn-sm btn-primary">
									<spring:message code="label.yes" />
								</div> --%>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	
	
	
	
	
</div>
<%@include file="../inc/page_footer.jsp"%>

<%@include file="../inc/template_scripts.jsp"%>

<script type="text/javascript">
/* Inactive User And Organisation */


	
	var lineChartData = {
			   "datasets": [{
				   label: "User",
			       "data": ${inactiveUserArrayJson},
			           "pointStrokeColor": "#fff",
			            "fillColor": "rgba(234,209,209,0.5)",
			           "pointColor": "#FF0000",
			           "strokeColor": "#FF0000"
			   },
			   {
				   label: "Organisation",
				   "data": ${inactiveOrganisationArrayJson},   
					           "pointStrokeColor": "#fff",
					            "fillColor": "rgba(253,255,165,0.5)",
					           "pointColor": "#FFFF00",
					           "strokeColor": "#FFFF00"
			   }],
			       "labels": ["0 month", "1 month", "2 month", "3 month","4 month","5 month","6 month"]
			};
            var options={
            		bezierCurve: false 
            		
            		}
			var myLine = new Chart(document.getElementById("myChart11").getContext("2d")).Line(lineChartData,options);
			
           var legendHolder = document.createElement('div');
     	   legendHolder.innerHTML = myLine.generateLegend();
     	   document.getElementById('js-legend').appendChild(legendHolder.firstChild);
     	   
     	   

</script>

<script>

  var a1=${lastMonthCreatedUser};
  var b1=${activeUsersMoreThanOneMonth};
  var a2=${oneMonthDownload};
  var b2=${moreThanOneMonthDownloads};
  var cc1=${lastMonthActiveOrganisation};
  var cc2=${moreThanLastMonthOrganisation};

  
 $(function () {
 var ctx = document.getElementById("canvasone").getContext("2d");
 var data = [
             {
                 value: cc1,
                 color:"#21DC54",
                 highlight: "#BCF5A9",
                 label: "< One Month"
             },
             {
                 value: cc2,
                 color: "lightgray",
                 highlight: "lightgray",
                 label: "> One Month"
             }
             
         ]
     var option={
 		percentageInnerCutout : 80,
 		tooltipFontSize: 12
 	
 		    
      }    
 var myDoughnutChart = new Chart(ctx).Doughnut(data,option); 
 });

 $(function () {
		var ctx = document.getElementById("canvastwo").getContext("2d");
		var data = [
		            {
		                value: a1,
		                color:"#21DC54",
		                highlight: "#BCF5A9",
		                label: "< One Month"
		            },
		            {
		                value: b1,
		                color: "lightgray",
		                highlight: "lightgray",
		                label: "> One Month"
		            }
		            
		        ]
		    var option={
				tooltipFontSize: 12,
				percentageInnerCutout : 80	
		     }    
		var myDoughnutChart = new Chart(ctx).Doughnut(data,option); 
		});


	$(function () {
		var ctx = document.getElementById("canvasthree").getContext("2d");
		var data = [
		            {
		                value: a2,
		                color:"#21DC54",
		                highlight: "#BCF5A9",
		                label: "< One Month"
		            },
		            {
		                value: b2,
		                color: "lightgray",
		                highlight: "lightgray",
		                label: "> One Month"
		            }
		            
		        ]
		    var option={
				tooltipFontSize: 12,
				percentageInnerCutout : 80	
		     }    
		var myDoughnutChart = new Chart(ctx).Doughnut(data,option); 
		});


 </script>

<script>

  $(document).ready(
	function(){
		var datasetInactiveOrg=${dataset};
		barGraph(datasetInactiveOrg);
		
	}
);
	
 function getinfo()
 {
		var month = $('#monthOftheYear').val();
		var year = $('#year').val();
		$.ajax({
			type: "GET",
			async: false,
			url: "<%=contexturl%>noOFCompaniesPerModule/"+month+"/"+year,
			success: function(data, textStatus, jqXHR){
				console.log("in mont result consol");
				barGraph(data);
			
			},
		});
 } 

 
 function barGraph(datas){
			var d=datas;
		    var dataset=[];
		    var rating=[1,2,3,4,5];
		    var j=0;
			for(var a in d.data){
				var v={
			            label: rating[j],
			            fillColor: d.color[a],
			            strokeColor: "rgba(220,220,220,0.8)",
			            highlightFill: "rgba(220,220,220,0.75)",
			            highlightStroke: "rgba(220,220,220,1)",
			            data: d.data[a]
			        };
				dataset.push(v);
				j++;
			}
			
				var ctx = document.getElementById("myChart3").getContext("2d");
				var data = {
					    labels: ["HR", "Admin", "Leader", "Finance", "Fundraising","Monitoring","Programs","Marketing","Governance","Strategy"],
					    datasets:dataset
					    
					};
				var myBarChart = new Chart(ctx).Bar(data, {
				    showTooltips: true,
				    
				});
				
				 document.getElementById('js-legend2').innerHTML = myBarChart.generateLegend();
				 
				myBarChart.destroy();
					
 }
	
 
 </script>


<script type="text/javascript">

window.onload = function () {
	
	 var d=${eduJason};
	    var chart = new CanvasJS.Chart("chartContainer",
	    {
	      title:{
	      },
	      animationEnabled: true,
	      legend: {
	        cursor:"pointer",
	        itemclick : function(e) {
	          if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
	              e.dataSeries.visible = false;
	          }
	          else {
	              e.dataSeries.visible = true;
	          }
	          chart.render();
	        }
	      },
	      axisY: {
	        title: "No Of Companies",
	        text: "Education Area", 
            fontFamily: "Verdana",
            fontColor: "Peru",
            fontSize: 24
	      },
	      toolTip: {
	        shared: true,  
	        content: function(e){
	          var str = '';
	          var total = 0 ;
	          var str3;
	          var str2 ;
	          for (var i = 0; i < e.entries.length; i++){
	            var  str1 = "<span style= 'color:"+e.entries[i].dataSeries.color + "'> " + e.entries[i].dataSeries.name + "</span>: <strong>"+  e.entries[i].dataPoint.y + "</strong> <br/>" ; 
	            total = e.entries[i].dataPoint.y + total;
	            str = str.concat(str1);
	          }
	          str2 = "<span style = 'color:DodgerBlue; '><strong>"+e.entries[0].dataPoint.label + "</strong></span><br/>";
	          str3 = "<span style = 'color:Tomato '>Total: </span><strong>" + total + "</strong><br/>";
	          
	          return (str2.concat(str)).concat(str3);
	        }

	      },
	      data: [
	      {        
	        type: "bar",
	        showInLegend: false,
	        color: "#E91E1E",
	        dataPoints:d

	      }
	      


	      ]
	    });

	chart.render();
	}
      
</script>


<script>

	var lineChartData = {
			   "datasets": [{
			       "data": ${companyProfileArrayJson},
			           "pointStrokeColor": "#fff",
			           "fillColor": "rgba(234,209,209,0.5)",
			           "pointColor": "#808080",
			           "strokeColor": "#FF0000"
			   }],
			       "labels": ["0", "10", "20", "30","40","50","60","70","80","90","100"]
			};
            var options={
            		bezierCurve: false
            }
			var myLine = new Chart(document.getElementById("myChart").getContext("2d")).Line(lineChartData,options);
			


 </script>	

<%@include file="../inc/template_end.jsp"%>