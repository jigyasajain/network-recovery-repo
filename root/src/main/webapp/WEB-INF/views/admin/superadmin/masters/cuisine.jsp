<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@include file="../../inc/config.jsp"%>

	<div class="content-header">
		<div class="header-section">
			<h1>
				<i class="fa fa-map-marker"></i> <spring:message code="heading.cuisinemaster" /><br> <small><spring:message code="heading.specifylistofcuisines" />
					</small>
			</h1>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" id="success_close" class="close" data-dismiss="alert" aria-hidden="true">x</button>
					<h4><i class="fa fa-check-circle"></i> <spring:message code="label.success"/></h4> <spring:message code="${success}"/>
				</div>
			</c:if>
			
			<span id="errorMsg"></span>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="menu.managerestaurants" /></li>
		<li><a href="#"><spring:message code="heading.cuisine" /></a>
		</li>
	</ul>
	<div class="block full gridView">
		<div class="block-title">
			<h2>
				<strong><spring:message code="heading.activecuisine" /></strong>
			</h2>
		</div>
		<a id="addCuisine" class="btn btn-sm btn-primary" href="#">
						<i class="fa fa-angle-right"></i> <spring:message code="label.addcuisine" />
		</a>
<%-- 		<a href="#" id="addCuisine"><spring:message code="label.addcuisine" /></a> --%>
		<div class="table-responsive">
			<table id="active-datatable"
				class="table table-vcenter table-condensed table-bordered">
				<thead>
					<tr>
						<th class="text-center"><spring:message code="label.id" /></th>
						<th class="text-center"><spring:message code="heading.cuisine" /></th>
						<th class="text-center"><spring:message code="label.actions" /></th>
					</tr>
				</thead>
				<tbody id="tbody">
					<c:forEach var="currentCuisine" items="${cuisineList}"
						varStatus="count">

						<tr>
							<td class="text-center">${count.count}</td>
							<td class="text-left"><c:out
									value="${currentCuisine.name}" /></td>
							<td class="text-center">
								<div class="btn-group">
									<a href="#" data-toggle="tooltip" title="Edit"
										onclick="editCuisine(${currentCuisine.id},${count.count})"
										class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
									<a href="#delete_cuisine_popup" data-toggle="modal" title="Delete"
										onclick="deleteCuisine(${currentCuisine.id})"
										class="btn btn-xs btn-danger"><i class="fa fa-times"></i>
									</a>
								</div>
							</td>
						</tr>
					</c:forEach>

				</tbody>
			</table>
		</div>
		</div>
		<div class="block full gridView">
			<div class="block-title">
				<h2>
					<strong><spring:message code="heading.inactivecuisine" /></strong> 
				</h2>
			</div>
			<div class="table-responsive">
				<table  id="inactive-datatable" class="table table-vcenter table-condensed table-bordered ">
					<thead>
						<tr>
							<th class="text-center"><spring:message code="label.id" /></th>
						    <th class="text-center"><spring:message code="heading.cuisine" /></th>
						    <th class="text-center"><spring:message code="label.actions" /></th>
						</tr>
					</thead>
					<tbody id="tablebody">
						<c:forEach var="currentCuisine" items="${inActiveCuisineList}"
							varStatus="count">
							<tr>
								<td class="text-center">${count.count}</td>
							    <td class="text-center"><c:out
									value="${currentCuisine.name}" /></td>
								<td class="text-center">
									<div class="btn-group">
										<a href="#restore_Cuisine_popup" data-toggle="modal"
											onclick="restoreCuisine(${currentCuisine.id})"
											title="Restore" class="btn btn-xs btn-danger"><i
											class="fa fa-times"></i> </a>
									</div>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	   
	   
	
	<div id="delete_cuisine_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="" method="post" class="form-horizontal form-bordered">
						<div style="padding: 10px; height: 110px;">
							<label><spring:message code="validation.doyouwanttodeletethiscuisine"/></label>
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal"><spring:message code="label.no"/></button>
								<div id="delete_cuisine" class="btn btn-sm btn-primary"><spring:message code="label.yes"/></div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div id="restore_Cuisine_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="" method="post" class="form-horizontal form-bordered">
						<div style="padding: 10px; height: 110px;">
							<label><spring:message code="validation.doyouwanttorestorethiscuisine"/></label>
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal"><spring:message code="label.no"/></button>
								<div id="restore_Cuisine" class="btn btn-sm btn-primary"><spring:message code="label.yes"/></div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="block full" id="formView" style="display: none">
		<div class="block-title">
			<h2>
				<strong><spring:message code="label.addcuisinemaster" /></strong>
			</h2>
		</div>
		<form action="" method="post" class="form-horizontal form-bordered"
			id="cuisine_form">
			<div class="form-group">
				<input id="cuisine_Id" name="cuisineId" type="hidden" value="0">
				<label class="col-md-3 control-label" for="area_Name"><spring:message code="heading.cuisine" /><span class="text-danger">*</span></label>
				<div class="col-md-9">
					<input id="cuisine_Name" name="cuisineName" class="form-control"
						placeholder="<spring:message code="label.cuisinename"/>.." type="text">
				</div>
			</div>

			<div class="form-group form-actions">
				<div class="col-md-9 col-md-offset-3">
					<div id="cuisine_submit" class="btn btn-sm btn-primary">
						<i class="fa fa-angle-right"></i>
						<spring:message code="button.save" />
					</div>
					<button type="reset" class="btn btn-sm btn-warning">
						<i class="fa fa-repeat"></i>
						<spring:message code="button.reset" />
					</button>
					<a class="btn btn-sm btn-primary" id="cancel" href="#">
						<i class="gi gi-remove"></i>
						<spring:message code="button.cancel" />
					</a>
				</div>
			</div>
		</form>
	</div>
<script src="<%=contexturl %>resources/js/pages/activeTablesDatatables.js"></script>
<script src="<%=contexturl %>resources/js/pages/inactiveTablesDatatables.js"></script>
<script>$(function(){ ActiveTablesDatatables.init(2); });</script>
<script>$(function(){ InActiveTablesDatatables.init(2); });</script>

<script type="text/javascript">
	$(document).ready(function() {
		
	
		
		$("#addCuisine").click(function() {
			$('#cuisine_form').find("input[type=text], textarea").val("");
			$('#cuisine_form').find("input[type=hidden], textarea").val("0");
			$("#formView").css("display", "block");
			$(".gridView").css("display", "none");
			$("#success_close").click();
		});
		
		$("#cancel").click(function() {
			$("#formView").css("display", "none");
			$(".gridView").css("display", "block");
			$('.help-block').css('display', 'none');
			$("div.form-group").removeClass('has-error');
			$("#success_close").click();
		});
		
		$("#restore_Cuisine").click(function(){
			$.ajax({
				type: "GET",
				async: false,
				url: "<%=contexturl %>Admin/RestoreCuisine?id="+restoreId,
				success: function(data, textStatus, jqXHR){
					$('#page-content').html(data);
					$('body').removeClass('modal-open');
					$('.modal-backdrop').remove();
					$('#restore_Cuisine_popup').modal('hide');
					$('html, body').animate({ scrollTop: $("#errorMsg").offset().top },500);
				},
				dataType: 'html'
			});
		});
		
		$("#delete_cuisine").click(function(){
			$.ajax({
				type: "GET",
				async: false,
				url: "<%=contexturl %>Admin/DeleteCuisine?id="+selectedId,
				success: function(data, textStatus, jqXHR){
					$('#page-content').html(data);
					$('body').removeClass('modal-open');
					$('.modal-backdrop').remove();
					$('#delete_cuisine_popup').modal('hide');	
					$('html, body').animate({ scrollTop: $("#errorMsg").offset().top },500);
				},
				dataType: 'html'
			});
		});
		
	
		$('#cuisine_form').validate(
				{   
					errorClass:"help-block animation-slideDown",
				    errorElement:"div",
					errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
					highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
					success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
					rules:{ cuisineName:{required:!0,maxlength:75},	
					},
					messages:{
						cuisineName:{
							required:'<spring:message code="validation.pleasementioncuisine"/>',
							maxlength :'<spring:message code="validation.cuisinename75character"/>'}
					},
			submitHandler: function() {			
			var data = $("#cuisine_form").serialize();
			 $.ajax({
				type: "POST",
				async: false,
				url: "<%=contexturl %>Admin/SaveCuisine",
				data:data,
				success: function(data, textStatus, jqXHR){
					var i = data.trim().indexOf("errorCode");
					if(i == 2){
						var obj = $.parseJSON(data.trim());
						var html = '<div class="alert alert-danger alert-dismissable">	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="fa fa-times-circle"></i> Error</h4> <span >'+obj.errorCode+'</span></div>';
						$("#errorMsg").html(html).show();
						$("#errorMsg").html(html).fadeOut(8000);
						$('html, body').animate({ scrollTop: $("#errorMsg").offset().top },500);
					}
					else{
						$('html, body').animate({ scrollTop: $("#errorMsg").offset().top },500);
						$('#page-content').html(data);
					}
				},
				dataType: 'html'
			});
		}
		
	});
		
		
		 $("#cuisine_submit").click(function(){
			 $("#cuisine_form").submit();
		 });
		
	});
	
	
	function editCuisine(id,position){
		var oTable = $("#active-datatable").dataTable();
		var data = oTable.fnGetData()[position-1];
		$("#cuisine_Id").val(id);
		$("#cuisine_Name").val(data[1]);
		$('.help-block').css('display', 'none');
		$("div.form-group").removeClass('has-error');
		$("#formView").css("display", "block");
		$(".gridView").css("display", "none");
		$("#success_close").click();
	}
	
	
	function deleteCuisine(id){
		selectedId = id;
	}
	
	function restoreCuisine(id){
		restoreId = id;
	}
</script>