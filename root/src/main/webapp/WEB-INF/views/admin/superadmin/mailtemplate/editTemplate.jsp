<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>

<link rel="stylesheet" type="text/css" href="<%=contexturl %>resources/css/jquery.cleditor.css" />
<style>
.cleditorToolbar {
	background: url('<%=contexturl %>resources/img/cleditor/toolbar.gif') repeat
}

.cleditorButton {
	float: left;
	width: 24px;
	height: 24px;
	margin: 1px 0 1px 0;
	background: url('<%=contexturl %>resources/img/cleditor/buttons.gif')
}

</style>
<div id="page-content">
<div class="content-header">
	<div class="header-section">
		<h1>
			<i class="fa fa-map-marker"></i>
			<spring:message code="heading.emailtemplates" />
		</h1>
		<span id="errorMsg"></span>
		<c:if test="${!empty success}">
			<div class="alert alert-success alert-dismissable">
				<button type="button" id="success_close" class="close" data-dismiss="alert"
					aria-hidden="true">x</button>
				<h4>
					<i class="fa fa-check-circle"></i> <spring:message code="label.success"/>
				</h4>
				<spring:message code="${success}" />
			</div>
		</c:if>
		<c:if test="${!empty error}">
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert"
					aria-hidden="true">x</button>
				<h4>
					<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
				</h4>
				<spring:message code="${error}" />
			</div>
		</c:if>
	</div>
</div>


<div class="block full" id="formView" >
	<div class="block-title">
		<h2>
			<strong><spring:message code="label.edittemplate" /></strong>
		</h2>
	</div>
	<form action="<%=contexturl %>ManageTemplate/SaveEmailTemplate" method="post" class="form-horizontal">
		<textarea id="input" name="templateText">${templateText}</textarea>
		<input type="hidden" name="templateName" value="${template.templatePath}"/>
		<div class="form-group form-actions">
			<div class="col-md-9 col-md-offset-3">
				<button id="offer_submit" type="submit"
					class="btn btn-sm btn-primary">
					<i class="fa fa-angle-right"></i> <spring:message code="button.save" />
				</button>
				<a class="btn btn-sm btn-primary" href="<%=contexturl %>ManageTemplate/EmailList"><i class="gi gi-remove"></i>
					<spring:message code="button.cancel" /> </a>
			</div>
		</div>
		<div style="margin: 10px">
			${template.terms}
		</div>
		
	</form>
</div>
</div>


<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<script type="text/javascript" src="<%=contexturl %>resources/js/jquery.cleditor.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#input").cleditor(); 
	});
</script>

<%@include file="../../inc/template_end.jsp"%>