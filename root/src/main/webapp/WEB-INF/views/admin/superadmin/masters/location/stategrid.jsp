<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@include file="../../../inc/config.jsp"%>

<%@include file="../../../inc/template_start.jsp"%>
<%@include file="../../../inc/page_head.jsp"%>
<style>

.chosen-container {
	width: 250px !important;
}
</style>
<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				<i class="fa fa-map-marker"></i>
				<spring:message code="heading.statemaster" />
				<br> <small><spring:message code="heading.statedetails" />
				</small>
			</h1>
			<span id="errorMsg"></span>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" id="success_close" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i> <spring:message code="label.success"/>
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="menu.managelocations" /></li>
		<li><a href="#"><spring:message code="label.state" /></a></li>
	</ul>
	<div class="block full gridView">
		<div class="block-title">
			<h2>
				<strong><spring:message code="heading.activestate" /></strong>
			</h2>

		</div>
		<a id="addCity" class="btn btn-sm btn-primary" href="<%=contexturl %>ManageLocation/State/AddState">
						<i class="fa fa-angle-right"></i> <spring:message code="label.addstate" />
		</a>
<%-- 		<a href="#" id="addCity"><spring:message code="label.addcity" /></a> --%>
		<!--<p><a href="https://datatables.net/" target="_blank">DataTables</a> is a plug-in for the Jquery Javascript library. It is a highly flexible tool, based upon the foundations of progressive enhancement, which will add advanced interaction controls to any HTML table. It is integrated with template's design and it offers many features such as on-the-fly filtering and variable length pagination.</p> -->
		<div class="table-responsive">
			<table id="active-datatable"
				class="table table-vcenter table-condensed table-bordered">
				<thead>
					<tr>
						<th class="text-center"><spring:message code="label.id" /></th>
					    <th class="text-center"><spring:message code="label.statename" /></th>
						<th class="text-center"><spring:message code="label.countryname" /></th>
						<th class="text-center"><spring:message code="label.actions" /></th>
					</tr>
				</thead>
				<tbody id="tbody">
					<%-- <c:forEach var="currentState" items="${activeStateList}" varStatus="count">

						<tr>
							<td class="text-center">${count.count}</td>
							<!-- 							<td class="text-center"><img -->
							<!-- 								src="../img/placeholders/avatars/avatar15.jpg" alt="avatar" -->
							<!-- 								class="img-circle"></td> -->
							<td class="text-left"><c:out
									value="${currentState.stateName}" /></td>
							<td class="text-left"><c:out
									value="${currentState.country.countryName}" /></td>
							<td class="text-center">
								<div class="btn-group">
									<a href="<%=contexturl %>ManageState/StateList/EditState?id=${currentState.stateId}" data-toggle="tooltip" title="Edit"
										
										class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
									<a href="#delete_city_popup" data-toggle="modal"
										onclick="deleteCity(${currentState.stateId})" title="Delete"
										class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
								</div>
							</td>
						</tr>
					</c:forEach> --%>

				</tbody>
			</table>
		</div>
	</div>
	<div class="block full gridView">
		<div class="block-title">
			<h2>
			<strong><spring:message code="heading.inactivestate" /></strong>
		   </h2>
		 </div>
		<div class="table-responsive">
			<table id="inactive-datatable"
				class="table table-vcenter table-condensed table-bordered">
				<thead>
					<tr>
						<th class="text-center"><spring:message code="label.id" /></th>
					    <th class="text-center"><spring:message code="label.statename" /></th>
						<th class="text-center"><spring:message code="label.countryname" /></th>
						<th class="text-center"><spring:message code="label.actions" /></th>
					</tr>
				</thead>
				<tbody id="tablebody">
					<%-- <c:forEach var="currentState" items="${inActiveStateList}"
						varStatus="count">
						<tr>
							<td class="text-center">${count.count}</td>
						
							<td class="text-left"><c:out
									value="${currentState.stateName}" /></td>
							<td class="text-left"><c:out
									value="${currentState.country.countryName}" /></td>
							<td class="text-center">
								<div class="btn-group">
									<a href="#restore_City_popup" data-toggle="modal"
										onclick="restoreCity(${currentState.stateId})"
										title="Restore" class="btn btn-xs btn-success save"><i
										class="fa fa-plus"></i> </a>
								</div>
							</td>
						</tr>
					</c:forEach> --%>
				</tbody>
			</table>
		</div>
	</div>

	<div id="delete_city_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="DeleteState" method="post" class="form-horizontal form-bordered" id="delete_city_form">
						<div style="padding: 10px; height: 110px;">
							<label><spring:message code="validation.doyouwanttodeletethisstate"/></label>
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal"><spring:message code="label.no" /></button>
								<div id="delete_city" class="btn btn-sm btn-primary"><spring:message code="label.yes" /></div>
							</div>
						</div>
						<input type="hidden"  name="id" id="cityId">
					</form>
				</div>
			</div>
		</div>
	</div>
<div id="restore_City_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="RestoreState" method="post" class="form-horizontal form-bordered" id="restore_city_form">
						<div style="padding: 10px; height: 110px;">
							<label><spring:message code="validation.doyouwanttorestorethisstate"/></label>
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal"><spring:message code="label.no" /></button>
								<div id="restore_City" class="btn btn-sm btn-primary"><spring:message code="label.yes"/></div>
							</div>
						</div>
						<input type="hidden"  name="id" id="city_id">
					</form>
				</div>
			</div>
		</div>
	</div>

	
</div>


<%@include file="../../../inc/page_footer.jsp"%>
<%@include file="../../../inc/template_scripts.jsp"%>
<script src="<%=contexturl %>resources/js/pages/activeTablesDatatables.js"></script>
<script src="<%=contexturl %>resources/js/pages/inactiveTablesDatatables.js"></script>
<!-- <script>$(function(){ ActiveTablesDatatables.init(3); });</script> -->
<!-- <script>$(function(){ InActiveTablesDatatables.init(3); });</script> -->
<script type="text/javascript">
var selectedId = 0;
var restoreId=0;
	$(document).ready(function() {
		
		$("#manageLocation").addClass("active");
		
		App.datatables();
		 $("#active-datatable").dataTable( {
			   "aoColumnDefs": [ { "bSortable": false,"bSearchable": true, "aTargets": [3 ] } ],
			    "iDisplayLength": 10,
			    "sPaginationType": "bootstrap",
               "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
		        "bProcessing": true,
		        "bServerSide": true,
		        "sort": "position",
		        "sAjaxSource": "/StateList/Active",
		        
		    } );
		 
		 $("#inactive-datatable").dataTable( {
			   "aoColumnDefs": [ { "bSortable": false,"bSearchable": true, "aTargets": [3 ] } ],
			    "iDisplayLength": 10,
			    "sPaginationType": "bootstrap",
               "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
		        "bProcessing": true,
		        "bServerSide": true,
		        "sort": "position",
		        "sAjaxSource": "/StateList/Inactive",
		        
		    } );
		 
		 $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
         $('.dataTables_length select').addClass('form-control');
		
		 $("#restore_City").click(function(){
				
				$("#city_id").val(restoreId);
				$("#restore_city_form").submit();
			});
		
		$("#delete_city").click(function(){
			$("#cityId").val(selectedId);
			$("#delete_city_form").submit();
		});
		
	});
	
	 
	
	function deleteCity(id){
		selectedId = id;
	}
	
	function restoreCity(id){
		restoreId = id;
	}

</script>
<%@include file="../../../inc/template_end.jsp"%>