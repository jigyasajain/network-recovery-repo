<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<%@include file="../inc/config.jsp"%>
<%@include file="../inc/template_start.jsp"%>
<%@include file="../inc/page_head.jsp"%>

<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i>
						<spring:message code="label.error" />
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>

			<h1>
				<!-- 				<i class="fa fa-map-marker"></i> -->
				<spring:message code="heading.surveymaster" />
				<br> <small><spring:message code="heading.managesurvey" /></small>
			</h1>
			<span id="errorMsg"></span>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
<%-- 		<li><spring:message code="menu.survey" /></li> --%>
           <li><strong>${module.moduleName}</strong></li>
	</ul>

	<form action="<%=contexturl%>ManageSurvey/CreateSurvey/SaveSurvey"
		method="post" class="form-horizontal ui-formwizard" id="Survey_form"
		enctype="multipart/form-data">
		<input name="moduleId" id="moduleId" type="hidden" value="${moduleId}" />
		<div class="row">
			<div class="">
				<div class="block">
					<div class="block">
						<div class="block-title">
							<h2>
								<strong><spring:message code="heading.section1" /></strong>
							<input  id="act" type="hidden" name="act"> 
							</h2>
						</div>

						<c:forEach var="list1" items="${list1}" varStatus="counter">
							<div class="form-group">

								<label class="col-md-3 control-label" for="question_one">
									Question ${list1.questionNo} </label>

								<div class="col-md-6">

									<input id="${counter.count}" name="${counter.count}" class="form-control"
										placeholder="Enter the question" type="text"
										value="${list1.question}">
								</div>
							</div>
							<div class="form-group">

								<label class="col-md-3 control-label">
                                    Explanation ${list1.questionNo} </label>

                                <div class="col-md-6">

                                    <input id="exp_${counter.count}" name="exp_${counter.count}" class="form-control"
                                        placeholder="Enter the question explanation" type="text"
                                        value="${list1.questionExplanation}">
                                </div>
								
							</div>
						</c:forEach>
					</div>
					<div class="block">
						<div class="block-title">
							<h2>
								<strong>Section2</strong>
							</h2>
						</div>
						<c:forEach var="list2" items="${list2}" varStatus="counter">
							<div class="form-group">

								<label class="col-md-3 control-label" for="question_two">
									Question ${list2.questionNo} </label>

								<div class="col-md-6">

									<input id="${counter.count+3}" name="${counter.count+3}" class="form-control"
										placeholder="Enter the question" type="text"
										value="${list2.question}">
								</div>
								
							</div>
							<div class="form-group">

                                <label class="col-md-3 control-label">
                                    Explanation ${list2.questionNo} </label>

                                <div class="col-md-6">

                                    <input id="exp_${counter.count+3}" name="exp_${counter.count+3}" class="form-control"
                                        placeholder="Enter the question explanation" type="text"
                                        value="${list2.questionExplanation}">
                                </div>

                            </div>
						</c:forEach>
					</div>
					<div class="block">
						<div class="block-title">
							<h2>
								<strong>Section3</strong>
							</h2>
						</div>
						<c:forEach var="list3" items="${list3}" varStatus="counter">
							<div class="form-group">

								<label class="col-md-3 control-label" for="question_three">
									Question ${list3.questionNo} </label>

								<div class="col-md-6">

									<input id="${counter.count+6}" name="${counter.count+6}" class="form-control"
										placeholder="Enter the question" type="text"
										value="${list3.question}">
								</div>
								
							</div>
							<div class="form-group">

                                <label class="col-md-3 control-label" for="question_three">
                                    Explanation ${list3.questionNo} </label>

                                <div class="col-md-6">

                                    <input id="exp_${counter.count+6}" name="exp_${counter.count+6}" class="form-control"
                                        placeholder="Enter the question explanation" type="text"
                                        value="${list3.questionExplanation}">
                                </div>

                            </div>
						</c:forEach>
					</div>
					<div class="block">
						<div class="block-title">
							<h2>
								<strong>Section4</strong>
							</h2>
						</div>
						<c:forEach var="list4" items="${list4}" varStatus="counter">
							<div class="form-group">

								<label class="col-md-3 control-label" for="question_four">
									Question ${list4.questionNo} </label>

								<div class="col-md-6">

									<input id="${counter.count+9}" name="${counter.count+9}" class="form-control"
										placeholder="Enter the question" type="text"
										value="${list4.question}">
								</div>
								
							</div>
							<div class="form-group">

                                <label class="col-md-3 control-label" for="question_four">
                                    Explanation ${list4.questionNo} </label>

                                <div class="col-md-6">

                                    <input id="exp_${counter.count+9}" name="exp_${counter.count+9}" class="form-control"
                                        placeholder="Enter the question explanation" type="text"
                                        value="${list4.questionExplanation}">
                                </div>

                            </div>
						</c:forEach>
					</div>
					<div class="block">
						<div class="block-title">
							<h2>
								<strong>Section5</strong>
							</h2>
						</div>
						<c:forEach var="list5" items="${list5}" varStatus="counter">
							<div class="form-group">

								<label class="col-md-3 control-label" for="question_five">
									Question ${list5.questionNo} </label>

								<div class="col-md-6">

									<input id="${counter.count+12}" name="${counter.count+12}" class="form-control"
										placeholder="Enter the question" type="text"
										value="${list5.question}">

								</div>
								
							</div>
							<div class="form-group">

                                <label class="col-md-3 control-label" for="question_five">
                                    Explanation ${list5.questionNo} </label>

                                <div class="col-md-6">

                                    <input id="exp_${counter.count+12}" name="exp_${counter.count+12}" class="form-control"
                                        placeholder="Enter the question explanation" type="text"
                                        value="${list5.questionExplanation}">
                                </div>

                            </div>
						</c:forEach>
					</div>
				</div>
			</div>
		</div>
		<div style="text-align: center; margin-bottom: 10px">
			<div class="form-group form-actions">
				<!-- 				<div class="col-md-9 col-md-offset-3"> -->

					<button id="survey_submit" type="button" 
						class="btn btn-sm btn-primary save">
						<i class="fa fa-angle-right"></i>
						<spring:message code="button.savepublish" />
					</button>
					<a class="btn btn-sm btn-primary save" href="<%=contexturl %>AtmaAdmin">
							<spring:message code="button.cancel" /> </a>

			</div>
		</div>
	</form>
</div>
<%@include file="../inc/page_footer.jsp"%>
<%@include file="../inc/template_scripts.jsp"%>
<%@include file="../inc/chosen_scripts.jsp"%>
<script type="text/javascript">
	$(document).ready(function() {
		$("#surveyBuilder").addClass("active");


      $("#survey_submit").click(function(){
    	  unsaved=false;
    	  $("#act").val("1");

    	  $("#Survey_form").submit();
      });

						$("#Survey_form")
								.validate(
										{
											errorClass : "help-block animation-slideDown",
											errorElement : "div",
											errorPlacement : function(e, a) {
												a.parents(".form-group > div")
														.append(e)
											},
											highlight : function(e) {
												$(e)
														.closest(".form-group")
														.removeClass(
																"has-success has-error")
														.addClass("has-error")
											},
											success : function(e) {
												e
														.closest(".form-group")
														.removeClass(
																"has-success has-error")
											},
											rules : {
												1:{required:!0,maxlength:255},2:{required:!0,maxlength:255},3:{required:!0,maxlength:255},4:{required:!0,maxlength:255},5:{required:!0,maxlength:255},6:{required:!0,maxlength:255},7:{required:!0,maxlength:255},8:{required:!0,maxlength:255},9:{required:!0,maxlength:255},10:{required:!0,maxlength:255},11:{required:!0,maxlength:255},12:{required:!0,maxlength:255},13:{required:!0,maxlength:255},14:{required:!0,maxlength:255},15:{required:!0,maxlength:255},
                                                exp_1:{required:!0,maxlength:255},exp_2:{required:!0,maxlength:255},exp_3:{required:!0,maxlength:255},exp_4:{required:!0,maxlength:255},exp_5:{required:!0,maxlength:255},exp_6:{required:!0,maxlength:255},exp_7:{required:!0,maxlength:255},exp_8:{required:!0,maxlength:255},exp_9:{required:!0,maxlength:255},exp_10:{required:!0,maxlength:255},exp_11:{required:!0,maxlength:255},exp_12:{required:!0,maxlength:255},exp_13:{required:!0,maxlength:255},exp_14:{required:!0,maxlength:255},exp_15:{required:!0,maxlength:255}
											},
											messages : {
												1 : {
													maxlength : 'Question should not be more than 255 words'
												},
												2 : {
													maxlength : 'Question should not be more than 255 words'
												},
												3 : {
													maxlength : 'Question should not be more than 255 words'
												},
												4 : {
													maxlength : 'Question should not be more than 255 words'
												},
												5 : {
													maxlength : 'Question should not be more than 255 words'
												},
												6 : {
													maxlength : 'Question should not be more than 255 words'
												},
												7 : {
													maxlength : 'Question should not be more than 255 words'
												},
												8 : {
													maxlength : 'Question should not be more than 255 words'
												},
												9 : {
													maxlength : 'Question should not be more than 255 words'
												},
												10 : {
													maxlength : 'Question should not be more than 255 words'
												},
												11 : {
													maxlength : 'Question should not be more than 255 words'
												},
												12 : {
													maxlength : 'Question should not be more than 255 words'
												},
												13 : {
													maxlength : 'Question should not be more than 255 words'
												},
												14 : {
													maxlength : 'Question should not be more than 255 words'
												},
												15 : {
													maxlength : 'Question should not be more than 255 words'
												}
											},
											submitHandler: function(form) {
                                                for(i = 1; i <= 15; ++i){
                                                    if (!($("#" + i).val()).trim()) {
                                                      // is empty or whitespace
                                                      continue;
                                                    }
                                                    $("#" + i).val($("#" + i).val() + "<->" + $("#exp_" + i).val());
                                                }
                                                form.submit();
                                            }
										});

					});
</script>