<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<%@include file="../inc/config.jsp"%>
<%@include file="../inc/template_start.jsp"%>
<%@include file="../inc/page_head.jsp"%>
 
<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i>
						<spring:message code="label.error" />
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>

			<h1>
				<!-- 				<i class="fa fa-map-marker"></i> -->
				<spring:message code="heading.surveymaster" />
				<br> <small><spring:message code="heading.managesurvey" /></small>
				
			</h1>
			<span id="errorMsg"></span>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
<%-- 		<li><strong>Module Based <spring:message code="menu.survey" /></strong></li> --%>
             <li><strong>${module.moduleName}</strong></li>
	</ul>

	<form action="<%=contexturl%>ManageSurvey/CreateSurvey/SaveSurvey"
		method="post" class="form-horizontal ui-formwizard" id="Survey_form"
		enctype="multipart/form-data">
		<input name="moduleId" id="moduleId" type="hidden" value="${moduleId}" />
		<div class="row">
			<div class="">
				<div class="block">
					<div class="block">
						<div class="block-title">
							<h2>
								<strong><spring:message code="heading.section1" /></strong>
							</h2>
						</div>

						<c:forEach var="list1" items="${list1}" varStatus = "counter">
							<div class="form-group">

								<label class="col-md-3 control-label" for="question_one">
									Question${counter.count}</label>

								<div class="col-md-6">

									<input id="${counter.count}" name="${counter.count}" class="form-control"
										placeholder="Enter the question" type="text"
										value="${list1.question}" readonly>
								</div>
							</div>
						</c:forEach>
					</div>
					<div class="block">
						<div class="block-title">
							<h2>
								<strong>Section2</strong>
							</h2>
						</div>
						<c:forEach var="list2" items="${list2}" varStatus = "counter">
							<div class="form-group">

								<label class="col-md-3 control-label" for="question_two">
									Question${counter.count}</label>

								<div class="col-md-6">

									<input id="${counter.count}" name="${counter.count}" class="form-control"
										placeholder="Enter the question" type="text"
										value="${list2.question}" readonly>
								</div>
								
							</div>
						</c:forEach>
					</div>
					<div class="block">
						<div class="block-title">
							<h2>
								<strong>Section3</strong>
							</h2>
						</div>
						<c:forEach var="list3" items="${list3}" varStatus = "counter">
							<div class="form-group">

								<label class="col-md-3 control-label" for="question_three">
									Question${counter.count}</label>

								<div class="col-md-6">

									<input id="${counter.count}" name="${counter.count}" class="form-control"
										placeholder="Enter the question" type="text"
										value="${list3.question}" readonly>
								</div>
								
							</div>
						</c:forEach>
					</div>
					<div class="block">
						<div class="block-title">
							<h2>
								<strong>Section4</strong>
							</h2>
						</div>
						<c:forEach var="list4" items="${list4}" varStatus = "counter">
							<div class="form-group">

								<label class="col-md-3 control-label" for="question_four">
									Question${counter.count} </label>

								<div class="col-md-6">

									<input id="${counter.count}" name="${counter.count}" class="form-control"
										placeholder="Enter the question" type="text"
										value="${list4.question}" readonly>
								</div>
								
							</div>
						</c:forEach>
					</div>
					<div class="block">
						<div class="block-title">
							<h2>
								<strong>Section5</strong>
							</h2>
						</div>
						<c:forEach var="list5" items="${list5}" varStatus = "counter">
							<div class="form-group">

								<label class="col-md-3 control-label" for="question_five">
								Question${counter.count}</label>

								<div class="col-md-6">

									<input id="${counter.count}" name="${counter.count}" class="form-control"
										placeholder="Enter the question" type="text"
										value="${list5.question}" readonly>
								</div>
								
							</div>
						</c:forEach>
					</div>
				</div>
			</div>
		</div>

	</form>
</div>
<%@include file="../inc/page_footer.jsp"%>
<%@include file="../inc/template_scripts.jsp"%>
<%-- <%@include file="../inc/chosen_scripts.jsp"%> --%>
<script src="<%=contexturl %>resources/js/custom/featurecontrol.js"></script>
<script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>

<script type="text/javascript">
$(document).ready(
			function() {
				var companyForm ;
				$("#surveyBuilder").addClass("active");
				
				

	                  
	                  
	$("#Survey_form").validate(
	{	errorClass:"help-block animation-slideDown",
		errorElement:"div",
		errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
		highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
		success:function(e){e.closest(".form-group").removeClass("has-success has-error")},					
		rules : {
													1 : {
														required : !0,
														maxlength : 255
													},
													2 : {
														required : !0,
														maxlength : 255
													},
													3 : {
														required : !0,
														maxlength : 255
													},
													4 : {
														required : !0,
														maxlength : 255
													},
													5 : {
														required : !0,
														maxlength : 255

													},
													6 : {
														required : !0,
														maxlength : 255
													},
													7 : {
														required : !0,
														maxlength : 255
													},

													8 : {
														required : !0,
														maxlength : 255
													},
													9 : {
														required : !0,
														maxlength : 255
													},
													10 : {
														required : !0,
														maxlength : 255
													},
													11 : {
														required : !0,
														maxlength : 255
													},
													12 : {
														required : !0,
														maxlength : 255
													},

													13 : {
														required : !0,
														maxlength : 255
													},
													14 : {
														required : !0,
														maxlength : 255
													},
													15 : {
														required : !0,
														maxlength : 255
													}
												},
												messages : {
													1 : {
														required : 'Please enter a Question',
														maxlength : 'Question should not be more than 255 words'
													},
													2 : {
														required : 'Please enter a Question',
														maxlength : 'Question should not be more than 255 words'
													},
													3 : {
														required : 'Please enter a Question',
														maxlength : 'Question should not be more than 255 words'
													},
													4 : {
														required : 'Please enter a Question',
														maxlength : 'Question should not be more than 255 words'
													},
													5 : {
														required : 'Please enter a Question',
														maxlength : 'Question should not be more than 255 words'
													},
													6 : {
														required : 'Please enter a Question',
														maxlength : 'Question should not be more than 255 words'
													},
													7 : {
														required : 'Please enter a Question',
														maxlength : 'Question should not be more than 255 words'
													},
													8 : {
														required : 'Please enter a Question',
														maxlength : 'Question should not be more than 255 words'
													},

													9 : {

														required : 'Please enter a Question',
														maxlength : 'Question should not be more than 255 words'
													},
													10 : {

														required : 'Please enter a Question',
														maxlength : 'Question should not be more than 255 words'
													},
													11 : {
														required : 'Please enter a Question',
														maxlength : 'Question should not be more than 255 words'
													},
													12 : {
														required : 'Please enter a Question',
														maxlength : 'Question should not be more than 255 words'
													},

													13 : {

														required : 'Please enter a Question',
														maxlength : 'Question should not be more than 255 words'
													},
													14 : {
														required : 'Please enter a Question',
														maxlength : 'Question should not be more than 255 words'
													},
													15 : {
														required : 'Please enter a Question',
														maxlength : 'Question should not be more than 255 words'
													}
												},
												
												submitHandler: function(form) {
													$("#surveypopup").click();
													companyForm = form;								
							                }
					});
});
	</script>
<%@include file="../inc/template_end.jsp"%>