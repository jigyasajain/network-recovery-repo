<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->



<%@include file="../inc/config.jsp"%>
<%@include file="../inc/template_start.jsp"%>
<%@include file="../inc/profile_page_head.jsp"%>
<style>
	.chosen-container{
	width: 150px !important;
}
</style>

<!-- Page content -->
<div id="page-content">
    <!-- Dashboard Header -->
    <!-- For an image header add the class 'content-header-media' and an image as in the following example -->
    <div class="content-header content-header-media">
        <div class="header-section">
            <div class="row">
               <div class="col-md-4 col-lg-6 hidden-xs hidden-sm">
                </div>
                <!-- END Main Title -->

                <!-- Top Stats -->
                <div class="col-md-8 col-lg-6">
                    <div class="row text-center">
                        <div class="col-xs-4 col-sm-3" style="float: right;">
                            <h2 class="animation-hatch">
                                <strong>${followedCount}</strong><br>
                                <small><i class="fa fa-thumbs-o-up"></i> Followed</small>
                            </h2>
                        </div>

                    </div>
                </div>
                <!-- END Top Stats -->
            </div>
        </div>
        
<%--         <%if( city.getProfileImage()!= null){ %>  --%>
<!--         For best results use an image with a resolution of 2560x248 pixels (You can also use a blurred image with ratio 10:1 - eg: 1000x100 pixels - it will adjust and look great!) -->
<%--         <img src="<%=contexturl %><%=city.getProfileImage().getImagePath()%>" alt="header image" class="animation-pulseSlow"> --%>
<%--    <%}else{ %> --%>
<%--     <img src="<%=contexturl %>resources/img/placeholders/headers/dashboard_header.jpg" alt="header image" class="animation-pulseSlow"> --%>
<%--    <%} %> --%>
    </div>
    <!-- END Dashboard Header -->
    
    <!-- Widgets Row -->
    <div class="row">
        <div class="col-md-6">
        	<!-- START Follow Restaurant Widget -->
            <div class="widget">
				<div class="widget-extra themed-background-dark">
					<h3 class="widget-content-light">
						Latest <strong>Following University</strong> <small>
							<a href="<%=contexturl %>ManageRestaurant/MyFollowedList">
									<strong>View All</strong></a></small>
					</h3>
				</div>
				
				<div class="block">
                        <div class="table-responsive">
                            <table id="general-table" class="table table-vcenter table-hover">
                                <thead>
                                    <tr>
										<th class="text-center"><spring:message code="menu.university"/></th>
										<th class="text-center"><spring:message code="menu.country"/></th>
                                    </tr>
                                </thead>

                            </table>
                        </div>
                    </div>
			</div>
            
        <!-- END Follow Restaurant Widget -->
        
        
        	<!-- START Gift Voucher Widget -->
            <div class="widget">
				<div class="widget-extra themed-background-dark">
					<h3 class="widget-content-light">
						Latest <strong>Courses</strong> <small>
							<a href="<%=contexturl%>GiftVoucher">
									<strong>View All</strong></a></small>
					</h3>
				</div>
				

			</div>
            
        <!-- END Gift Voucher Widget -->
        
        <!-- START Top 5 resaurant Widget -->
            <div class="widget">
				<div class="widget-extra themed-background-dark">
					<h3 class="widget-content-light">
						<strong>Top 5 University</strong> 
					</h3>
				</div>
				
				<div class="block">
                       
                  </div>
			</div>
            
        <!-- END Top 5 resaurant Widget -->
        		
        </div>
        
        
        <div class="col-md-6">
        	<!-- START new added Restaurant Widget -->
            <div class="widget">
				<div class="widget-extra themed-background-dark">
					<h3 class="widget-content-light">
						Latest <strong>Added Courses</strong> 
					</h3>
				</div>
				
				<div class="block">
                        
                    </div>
			</div>
            
        <!-- END new added Restaurant Widget -->
        
        
         
        
        
        </div>
        
    </div>
    <!-- END Widgets Row -->
    
    
</div>
<!-- END Page Content -->

<%@include file="../inc/page_footer.jsp"%>

<!-- Remember to include excanvas for IE8 chart support -->
<!--[if IE 8]><script src="<%=contexturl %>resources/js/helpers/excanvas.min.js"></script><![endif]-->

<%@include file="../inc/template_scripts.jsp"%>



<!-- Google Maps API + Gmaps Plugin, must be loaded in the page you would like to use maps (Remove 'http:' if you have SSL) -->
<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script src="<%=contexturl %>resources/js/helpers/gmaps.min.js"></script>

<!-- Load and execute javascript code used only in this page -->
<script src="<%=contexturl %>resources/js/pages/index.js"></script>
<script>$(function(){ Index.init(); });</script>



<%@include file="../inc/template_end.jsp"%>