package com.astrika.forummngt.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.astrika.forummngt.model.ForumMaster;
import com.astrika.forummngt.model.ForumTag;

public interface ForumRepository extends JpaRepository<ForumMaster, Long> {

	@Query("select f from ForumMaster f left join fetch f.module left join fetch f.forumComments where f.forumId = ?1")
	ForumMaster findByForumId(long forumId);

	@Query("select f from ForumMaster f where f.active = ?1 Order By f.createdOn DESC")
	List<ForumMaster> findByActive(boolean active);

	@Query("select f from ForumMaster f where f.title = ?1 and f.tag = ?2 and f.module.moduleId = ?3")
	ForumMaster findByTitleandTagandModuleId(String title, ForumTag tag, long moduleId);

	@Query(value = "select f from ForumMaster f left join fetch f.forumComments where f.module.moduleId = ?1 and f.active = ?2", countQuery="select count(f) from ForumMaster f where f.module.moduleId = ?1 and f.active = ?2")
	Page<ForumMaster> findByModuleIdandActive (long moduleId, boolean active, Pageable page);

	@Query(value = "select f from ForumMaster f left join fetch f.createdBy where f.module.moduleId = ?1 and f.active = ?2")
	List<ForumMaster> findByModuleandActive(long moduleId, boolean active);

	@Query("select count(f.forumId) from ForumMaster f where f.module.moduleId=?1")
	int totalForumsperModule(long moduleId);

	@Query("select count(f) from ForumMaster f where f.flag=1")
	int getLastSevenDayPosts();

	@Query("select f.forumId,f.createdBy.firstName,f.createdBy.lastName ,f.createdOn,f.module.moduleName,f.title,f.flag from ForumMaster f where DATEDIFF(now(),f.createdOn)<7")
	List<Object[]> getlastpostss();

	@Query("select f from ForumMaster f where f.flag=1")
	List<ForumMaster> getInactiveFlagLastPosts();

	@Query("select count(f.forumId) from ForumMaster f")
	int getTotalPosts();

	@Query("select count(f.forumId) from ForumMaster f where DATEDIFF(now(),f.createdOn)<30")
	int getlastMonthPost();

	@Query("select count(f.forumId) from ForumMaster f where DATEDIFF(now(),f.createdOn)>30")
	int getolderThanOneMonthPost();

}
