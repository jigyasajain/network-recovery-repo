package com.astrika.forummngt.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.envers.Audited;
import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import com.astrika.common.model.User;
import com.astrika.companymngt.model.module.ModuleMaster;



@Entity
@Audited
public class ForumMaster {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long forumId;
	
	@Column(nullable = false)
	private String title;
	
	@Enumerated(EnumType.ORDINAL)
	private ForumTag tag;
	
	@Column(nullable=false, length=255)
	private String description;
	
	@JsonIgnore
	@OneToMany(mappedBy = "forum",fetch = FetchType.LAZY)
	private List<Comments> forumComments;
	
	@Column(columnDefinition = "boolean default false", nullable = false)
	private boolean flag;
	
	@JsonIgnore
	@CreatedBy
	@ManyToOne(fetch = FetchType.LAZY)
	private User createdBy;
	
	@Transient
	private Date createdDate;
	
	@CreatedDate
	private DateTime createdOn;
	
	@LastModifiedDate
	private DateTime lastModifiedOn;
	
	@JsonIgnore@LastModifiedBy
	@ManyToOne(fetch = FetchType.LAZY)
	private User lastModifiedBy;
	
	@Column(columnDefinition = "boolean default true", nullable = false)
	private boolean active = true;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JsonIgnore
	private ModuleMaster module;
	

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public Long getForumId() {
		return forumId;
	}

	public void setForumId(Long forumId) {
		this.forumId = forumId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public ForumTag getTag() {
		return tag;
	}

	public void setTag(ForumTag tag) {
		this.tag = tag;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdOn.toDate();
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public DateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(DateTime createdOn) {
		this.createdOn = createdOn;
	}

	public DateTime getLastModifiedOn() {
		return lastModifiedOn;
	}

	public void setLastModifiedOn(DateTime lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	public User getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(User lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public ModuleMaster getModule() {
		return module;
	}

	public void setModule(ModuleMaster module) {
		this.module = module;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Comments> getForumComments() {
		return forumComments;
	}

	public void setForumComments(List<Comments> forumComments) {
		this.forumComments = forumComments;
	}
	

}
