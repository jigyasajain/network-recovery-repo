package com.astrika.forummngt.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.envers.Audited;
import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;

import com.astrika.categorymngt.model.CategoryMaster;
import com.astrika.common.model.User;
import com.astrika.companymngt.model.module.ModuleMaster;


@Entity
@Audited
public class Comments {
		
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long commentId;
	
	@Column(length = 255)
	private String comments;

	@JsonIgnore
	@CreatedBy
	@ManyToOne(fetch = FetchType.LAZY)
	private User createdBy;	

	@Transient
	private Date createdDate;
	
	@CreatedDate
	private DateTime createdOn;
	
	@ManyToOne(optional = true, fetch=FetchType.LAZY)
	@JsonIgnore
	private ForumMaster forum;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JsonIgnore
	private Comments comment;
	
	@ManyToOne
	private ModuleMaster module;
	
	@ManyToOne
	private CategoryMaster category;
	
	@Column(columnDefinition = "boolean default true", nullable = false)
	private boolean active = true;

	@Column(columnDefinition = "boolean default false", nullable = false)
	private boolean flag;
	
	
	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Long getCommentId() {
		return commentId;
	}

	public void setCommentId(Long commentId) {
		this.commentId = commentId;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public DateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(DateTime createdOn) {
		this.createdOn = createdOn;
	}

	public ForumMaster getForum() {
		return forum;
	}

	public void setForum(ForumMaster forum) {
		this.forum = forum;
	}

	public Comments getComment() {
		return comment;
	}

	public void setComment(Comments comment) {
		this.comment = comment;
	}

	public ModuleMaster getModule() {
		return module;
	}

	public void setModule(ModuleMaster module) {
		this.module = module;
	}

	public CategoryMaster getCategory() {
		return category;
	}

	public void setCategory(CategoryMaster category) {
		this.category = category;
	}
	
}
