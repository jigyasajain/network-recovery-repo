package com.astrika.forummngt.service;

import java.util.List;

import com.astrika.forummngt.model.Comments;

public interface CommentService {

	public Comments findByCommentId(long commentId);

	public List<Comments> findByActiveTrueAndCategoryIdandModuleId(long categoryId, long moduleId,  int start, int end);

	public Comments save(Comments comment);

	public List<Comments> findByActiveandForumId(boolean active, Long forumId);

	public Comments delete(long commentId);

	public List<Comments> getAllCommentsOfLastMonth();

	public List<Comments> getOldComments();

	public int getTotalComment();

	public List<Object[]> getLastComments();

	public List<Object[]> getLastCommentsForum();

	public int getLastCommentCount();

	public List<Comments> getCommentOfTrueFlag();

	public Comments update(Comments comments);

	public int getTotalPosts();

	public int getLastSevenDayPosts();

	public List<Object[]> getlastpostss();

	public int getlastMonthPost();

	public int getolderThanOneMonthPost();

	public List<Comments> getInactiveFlagLastPosts();

}
