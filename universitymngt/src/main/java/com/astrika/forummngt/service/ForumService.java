package com.astrika.forummngt.service;

import java.util.List;

import com.astrika.forummngt.exception.DuplicateForumException;
import com.astrika.forummngt.model.ForumMaster;
import com.astrika.forummngt.model.ForumTag;

public interface ForumService {
		public ForumMaster save(ForumMaster forum) throws DuplicateForumException;
		
		public ForumMaster update(ForumMaster forum);
		
		public ForumMaster delete(long forumId);
		
		public ForumMaster restore(Long forumId);
		
		public ForumMaster findByForumId(long forumId);
		
		public List<ForumMaster> findByActive(boolean active);
		
		public ForumMaster findByTitleandTagandModuleId(String title, ForumTag tag, long moduleId);
		
		public List<ForumMaster> findByModuleIdandActive(long moduleId, boolean active, int start, int end);
		
		public List<ForumMaster> findByModuleandActive(long moduleId, boolean active);
		
		public int totalForumsperModule(long moduleId);

		public int getLastSevenDayPosts();

		public List<Object[]> getlastpostss();

		public List<ForumMaster> getInactiveFlagLastPosts();

		public ForumMaster update1(ForumMaster forumMaster);

		public int getTotalPosts();

		public int getlastMonthPost();

		public int getolderThanOneMonthPost();
}
