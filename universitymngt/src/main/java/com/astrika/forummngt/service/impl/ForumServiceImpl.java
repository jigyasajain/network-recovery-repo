package com.astrika.forummngt.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.astrika.forummngt.exception.DuplicateForumException;
import com.astrika.forummngt.model.ForumMaster;
import com.astrika.forummngt.model.ForumTag;
import com.astrika.forummngt.repository.ForumRepository;
import com.astrika.forummngt.service.ForumService;
import com.astrika.kernel.exception.CustomException;

@Service
public class ForumServiceImpl implements ForumService {

	@Autowired
	private ForumRepository forumRepository;
	
	@Override
	public ForumMaster save(ForumMaster forum) throws DuplicateForumException {
		ForumMaster forum1 = findByTitleandTagandModuleId(forum.getTitle(), forum.getTag(), forum.getModule().getModuleId());
		if(forum1!= null && forum1.getTitle().equals(forum.getTitle())){
				throw new DuplicateForumException(CustomException.DUPLICATE_FORUM.getCode());
		}
		return forumRepository.save(forum);
	}

	@Override
	public ForumMaster update(ForumMaster forum) {
		return null;
	}

	@Override
	public ForumMaster delete(long forumId) {
	
		ForumMaster forum = findByForumId(forumId);
		forum.setActive(false);
		return forumRepository.save(forum);
	}

	@Override
	public ForumMaster restore(Long forumId) {
		ForumMaster forum = findByForumId(forumId);
		forum.setActive(true);
		return forumRepository.save(forum);
	}

	@Override
	public ForumMaster findByForumId(long forumId) {
		return forumRepository.findByForumId(forumId);
	}

	@Override
	public List<ForumMaster> findByActive(boolean active) {
		return forumRepository.findByActive(active);
	}

	@Override
	public ForumMaster findByTitleandTagandModuleId(String title,  ForumTag tag,
			long moduleId) {
		return forumRepository.findByTitleandTagandModuleId(title, tag, moduleId);
	}

	@Override
	public List<ForumMaster> findByModuleIdandActive(long moduleId,
			boolean active,  int start, int end) {
		Page<ForumMaster> recentForums = forumRepository.findByModuleIdandActive(moduleId, true, new PageRequest(start,end,Direction.DESC,"createdOn"));
		return recentForums.getContent();
	}

	@Override
	public List<ForumMaster> findByModuleandActive(long moduleId, boolean active) {
		return forumRepository.findByModuleandActive(moduleId, active);
	}

	@Override
	public int totalForumsperModule(long moduleId) {
		List<ForumMaster> forumlist = forumRepository.findByModuleandActive(moduleId, true);
		if(forumlist.isEmpty()){
			return 0;
		}
		else{
			return forumRepository.totalForumsperModule(moduleId);
		}
		
	}

	@Override
	public int getLastSevenDayPosts() {
		return forumRepository.getLastSevenDayPosts();
	}
	
	@Override
	public List<Object[]> getlastpostss() {
		return forumRepository.getlastpostss();
	}
	
	@Override
	public List<ForumMaster> getInactiveFlagLastPosts() {
		return forumRepository.getInactiveFlagLastPosts();
	}
	
	@Override
	public ForumMaster update1(ForumMaster forumMaster) {
		return forumRepository.save(forumMaster);
	}
	
	@Override
	public int getTotalPosts() {
		return forumRepository.getTotalPosts();
	}
	
	@Override
	public int getlastMonthPost() {
		return forumRepository.getlastMonthPost();
	}
	
	@Override
	public int getolderThanOneMonthPost() {
		return forumRepository.getolderThanOneMonthPost();
	}
}
