package com.astrika.forummngt.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.astrika.forummngt.model.Comments;
import com.astrika.forummngt.repository.CommentsRepository;
import com.astrika.forummngt.service.CommentService;

@Service
public class CommentServiceImpl implements CommentService {

	@Autowired
	private CommentsRepository commentRepository;

	@Override
	public Comments findByCommentId(long commentId) {

		return commentRepository.findByCommentId(commentId);
	}

	@Override
	public Comments save(Comments comment) {

		return commentRepository.save(comment);
	}

	@Override
	public List<Comments> findByActiveTrueAndCategoryIdandModuleId(long categoryId, long moduleId, int start, int end) {
		Page<Comments> recentComments = commentRepository.findAllByActiveTrueAndCategoryIdAndModuleId(true, categoryId, moduleId, new PageRequest(start,end,Direction.DESC,"createdOn"));
		return recentComments.getContent();
	}

	@Override
	public List<Comments> findByActiveandForumId(boolean active, Long forumId) {
		return commentRepository.findByActiveandForumId(active, forumId);
	}

	@Override
	public Comments delete(long commentId) {
		Comments comment = findByCommentId(commentId);
		comment.setActive(false);
		return commentRepository.save(comment);
	}

	@Override
	public List<Comments> getAllCommentsOfLastMonth() {
		return commentRepository.getAllCommentsOfLastMonth();
	}
	@Override
	public List<Comments> getOldComments() {
		return commentRepository.getOldComments();
	}

	@Override
	public int getTotalComment(){
		return commentRepository.getTotalComment();
	}
	
	@Override
	public int getTotalPosts(){
		return commentRepository.getTotalPosts();
	}
	@Override
	public int getlastMonthPost(){
		return commentRepository.getlastMonthPost();
	}
	
	@Override
	public int getolderThanOneMonthPost(){
		return commentRepository.getolderThanOneMonthPost();
	}
	
	
	@Override
	public int getLastSevenDayPosts(){
		return commentRepository.getLastSevenDayPosts();
	}
	
	
	@Override
	public List<Comments> getCommentOfTrueFlag() {
		return commentRepository.getCommentOfTrueFlag();
	}
	
	@Override
	public List<Comments> getInactiveFlagLastPosts() {
		return commentRepository.getInactiveFlagLastPosts();
	}
	
	@Override
	public Comments update(Comments comments) {		
		return commentRepository.save(comments);		
	}

	@Override
	public List<Object[]> getLastComments() {
		return commentRepository.getLastComments();
	}
	
	@Override
	public List<Object[]> getlastpostss() {
		return commentRepository.getlastpostss();
	}
	
	
	@Override
	public List<Object[]> getLastCommentsForum() {
		return commentRepository.getLastCommentsForum();
	}


	@Override
	public int getLastCommentCount() {
		return commentRepository.getLastCommentCount();
	}
}


