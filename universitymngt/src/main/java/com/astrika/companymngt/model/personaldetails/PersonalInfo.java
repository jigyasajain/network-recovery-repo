package com.astrika.companymngt.model.personaldetails;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.envers.Audited;

import com.astrika.common.model.User;

@Entity
@Audited
public class PersonalInfo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long personalInfoId;
	
	@JsonIgnore
	@OneToOne(fetch = FetchType.LAZY)
	private User user;
	
	@Column(length=500)
	private String linkedin;
	
	@Column
	private String educationalBackground;
	
	@Column
	private String workExperience;
	
	@Column
	private String functionalAreas;
	
	@Column
	private String funcAreaReason;
	
	@Column
	private String aboutNetwork;
	
	@Column
	private String networkReason;

	public Long getPersonalInfoId() {
		return personalInfoId;
	}

	public void setPersonalInfoId(Long personalInfoId) {
		this.personalInfoId = personalInfoId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getLinkedin() {
		return linkedin;
	}

	public void setLinkedin(String linkedin) {
		this.linkedin = linkedin;
	}

	public String getEducationalBackground() {
		return educationalBackground;
	}

	public void setEducationalBackground(String educationalBackground) {
		this.educationalBackground = educationalBackground;
	}

	public String getWorkExperience() {
		return workExperience;
	}

	public void setWorkExperience(String workExperience) {
		this.workExperience = workExperience;
	}

	public String getFunctionalAreas() {
		return functionalAreas;
	}

	public void setFunctionalAreas(String functionalAreas) {
		this.functionalAreas = functionalAreas;
	}

	public String getAboutNetwork() {
		return aboutNetwork;
	}

	public void setAboutNetwork(String aboutNetwork) {
		this.aboutNetwork = aboutNetwork;
	}

	public String getFuncAreaReason() {
		return funcAreaReason;
	}

	public void setFuncAreaReason(String funcAreaReason) {
		this.funcAreaReason = funcAreaReason;
	}

	public String getNetworkReason() {
		return networkReason;
	}

	public void setNetworkReason(String networkReason) {
		this.networkReason = networkReason;
	}


}
