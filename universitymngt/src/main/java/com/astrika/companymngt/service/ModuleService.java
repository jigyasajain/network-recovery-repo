package com.astrika.companymngt.service;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.astrika.common.exception.NoSuchUserException;
import com.astrika.companymngt.model.module.ModuleMaster;

public interface ModuleService {

	public List<ModuleMaster> findByActive(boolean active);

	public ModuleMaster findByModuleName(String moduleName);

	public ModuleMaster delete(Long moduleId) throws NoSuchUserException;

	public ModuleMaster restore(Long moduleId) throws NoSuchUserException;

	public ModuleMaster save(String moduleName, String moduleDescription,
			MultipartFile profile) throws IOException;

	ModuleMaster save(ModuleMaster module);

	ModuleMaster update(long moduleId, String moduleName,
			String moduleDescription, MultipartFile profile)
					throws IOException;

	public ModuleMaster findByModuleId(long moduleId);

	public ModuleMaster update(ModuleMaster module);

	public ModuleMaster deleteSponsoredImage(Long moduleId) throws UnsupportedEncodingException;

	public ModuleMaster findByModuleIdandActive(long moduleId, boolean active);

	public ModuleMaster deletePromoImage(Long moduleId) throws UnsupportedEncodingException;

	public List<ModuleMaster> findTheNextRow(Long moduleId);

}
