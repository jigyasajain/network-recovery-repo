package com.astrika.companymngt.service;

import com.astrika.companymngt.model.personaldetails.PersonalInfo;

public interface PersonalInfoService {
	
	public PersonalInfo findById(Long userId);
	
	public PersonalInfo save(PersonalInfo personalInfo);

}
