package com.astrika.companymngt.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.astrika.common.exception.DuplicateEmailException;
import com.astrika.common.exception.DuplicateLoginException;
import com.astrika.common.exception.NoSuchUserException;
import com.astrika.common.model.User;
import com.astrika.common.model.UserStatus;
import com.astrika.common.service.CityService;
import com.astrika.common.service.CountryService;
import com.astrika.common.service.DocumentService;
import com.astrika.common.service.StateService;
import com.astrika.common.service.UserService;
import com.astrika.common.util.CommonUtil;
import com.astrika.common.util.PropsValues;
import com.astrika.companymngt.exception.DuplicateCompanyException;
import com.astrika.companymngt.model.company.CompanyMaster;
import com.astrika.companymngt.model.company.CompanySurveyResetManager;
import com.astrika.companymngt.model.module.ModuleMaster;
import com.astrika.companymngt.repository.CompanyRepository;
import com.astrika.companymngt.service.CompanyService;
import com.astrika.companymngt.service.ModuleService;
import com.astrika.kernel.exception.CustomException;
import com.astrika.moduleusermanagement.exception.FileNotFoundException;
import com.astrika.surveymngt.model.SurveyManager;
import com.astrika.surveymngt.model.SurveyStatus;
import com.astrika.surveymngt.service.SurveyManagerService;


@Service
public class CompanyServiceImpl implements CompanyService {

    @Autowired
    private CompanyRepository companyRepository;



    private static final Logger LOGGER = Logger.getLogger(CompanyServiceImpl.class);

    @Autowired
    private UserService userService;
    @Autowired
    private StateService stateService;
    @Autowired
    private CityService cityService;
    @Autowired
    private CountryService countryService;
    @Autowired
    private SurveyManagerService surveyManagerService;
    @Autowired
    private ModuleService moduleService;
    @Autowired
    private DocumentService documentService;
    @Autowired
    private PropsValues propsValue;
    @Autowired
    private CommonUtil commonUtil;
    @Autowired
    PropsValues propertyValues;

    private static final String REPORT="Report";
    private static final String ORGANISATION_NAME="Organisation Name";
    private static final String UTF8="UTF-8";
    private static final String USERDETAILS_CSV="UserDetails.csv";

    public CompanyMaster save(CompanyMaster company, User admin)
            throws IOException {



        User userAdmin=admin;

        CompanyMaster companyMaster = companyRepository
                .findByCompanyName(company.getCompanyName());
        if (companyMaster != null) { // University already exist
            if (companyMaster.isActive())
                throw new DuplicateCompanyException(
                        CustomException.DUPLICATE_COMPANY.getCode());
            else
                throw new DuplicateCompanyException(
                        CustomException.DUPLICATE_DELETED_COMPANY.getCode());
        } else {
            try {

                userService.findByEmailId(userAdmin.getEmailId());
                throw new DuplicateEmailException(
                        CustomException.DUPLICATE_ADMIN_EMAIL.getCode());
            } catch (NoSuchUserException e) {
                LOGGER.info(e);
            }

            try {
                userService.findByLoginId(userAdmin.getLoginId());
                throw new DuplicateLoginException(
                        CustomException.DUPLICATE_LOGIN.getCode());
            } catch (NoSuchUserException e) {
                LOGGER.info(e);
            }

            userAdmin.setStatus(UserStatus.ACTIVE);
            userAdmin = userService.save(userAdmin);

            company.setAdmin(userAdmin);
            CompanyMaster company1 = companyRepository.save(company);
            userAdmin.setModuleId(company1.getCompanyId());
            DateTime date=company1.getCreatedOn();
            date.getMonthOfYear();
            if(date.getMonthOfYear()>3 && date.getMonthOfYear()<7)
            {
                company1.setQuarter("Q1");
            }
            else if (date.getMonthOfYear()>6 && date.getMonthOfYear()<10) {
                company1.setQuarter("Q2");	
            }
            else if (date.getMonthOfYear()>9 && date.getMonthOfYear()<=12) {
                company1.setQuarter("Q3");
            }
            else if (date.getMonthOfYear()>=3) {

                company1.setQuarter("Q4");
            }
            companyRepository.save(company1);
            userService.update(userAdmin);
            List<ModuleMaster> modules = moduleService.findByActive(true);
            int totalModules = modules.size();

            SurveyManager surveyManager = new SurveyManager();
            surveyManager.setCompany(company1);
            surveyManager.setSurveyStatus(SurveyStatus.PENDING);
            surveyManager.setTotalModules(totalModules);
            surveyManager.setTotalModulesCompleted(0);
            surveyManager.setSurveyPublished(date);
            surveyManager.setOngoingSurvey(true);
            surveyManager.setSurveySkipOption(false);
            SurveyManager surveyManager1 = surveyManagerService.save(surveyManager);
            DateTime surveyPublishedDate = surveyManager1.getSurveyPublished();
            DateTime lastDatetoComplete = surveyPublishedDate.plusMonths(1);
            surveyManager1.setLastDateToComplete(lastDatetoComplete);
            surveyManagerService.save(surveyManager1);
        }
        return company;
    }

    public List<CompanyMaster> findByActive(boolean active) {
        return companyRepository.findAllByActive(active);
    }
    public List<CompanyMaster> activeInLastMonth() {
        return companyRepository.activeInLastMonth();
    }
    public List<CompanyMaster> activeInMoteThanMonth() {
        return companyRepository.activeInMoteThanMonth();
    }

    public int gettotalOrganisations() {
        return companyRepository.gettotalOrganisations();
    }

    public int getLastMonthCreatedOrganisation() {
        return companyRepository.getLastMonthActiveOrganisation();
    }

    public int getlastAddedOrganisations() {
        return companyRepository.getlastAddedOrganisations();
    }

    public int getInctiveOrganisation(int a){
        return companyRepository.getInctiveOrganisation(a);
    }

    public int getSecondLastMonthActiveOrganisation() {
        return companyRepository.getSecondLastMonthActiveOrganisation();
    }

    public int getMoreThanLastMonthOrganisation() {
        return companyRepository.getMoreThanLastMonthOrganisation();
    }

    public int getTotalOraganisation() {
        return companyRepository.getTotalOraganisation();
    }
    public long getTotalOraganisation1() {
        return companyRepository.getTotalOraganisation1();
    }

    public int getTotalOrg(int yr) {
        return companyRepository.getTotalOrg(yr);
    }

    public List<Object[]> getTopCities() {
        return companyRepository.getTopCities();
    }

    public List<Object[]> topCities() {
        return companyRepository.topCities();
    }

	public long getTotalOrgUptomonth(Date d){
		return companyRepository.getTotalOrgUptomonth(d);
		
	}
	
	public long getInactiveOrgOfThatMonth(long totalOrgUptoMonth, int yr, int x){
		return companyRepository.getInactiveOrgOfThatMonth(totalOrgUptoMonth,yr,x);
	}
	
    public List<Object[]> getAllRecod() {
        return companyRepository.getAllRecod();
    }

    public CompanyMaster findByCompanyId(long companyId) {
        return companyRepository.findByCompanyCompanyId(companyId);
    }

    @Override
    public CompanyMaster save(CompanyMaster company) {

        return companyRepository.save(company);
    }


    @Override
    public CompanyMaster delete(Long companyId) throws NoSuchUserException {
        CompanyMaster company = findByCompanyId(companyId);
        company.setActive(false);
        Authentication authentication = commonUtil.getCurrentLoggedInUser();
        if(authentication == null) 
            throw new IllegalStateException("Logged in user cannot be null");
        company.setDeActivatedBy((User) authentication.getPrincipal());
        company.setDeActivatedOn(new DateTime());
        return companyRepository.save(company);
    }

    @Override
    public CompanyMaster restore(Long companyId) throws NoSuchUserException {
        CompanyMaster company = findByCompanyId(companyId);
        company.setActive(true);
        Authentication authentication = commonUtil.getCurrentLoggedInUser();
        if(authentication == null) 
            throw new IllegalStateException("Logged in user cannot be null");
        company.setReActivatedBy((User) authentication.getPrincipal());
        company.setReActivatedOn(new DateTime());
        return companyRepository.save(company);
    }

    @Override
    public CompanyMaster update(CompanyMaster company)
            throws IOException {
        CompanyMaster companyMaster1 = companyRepository
                .findByCompanyName(company.getCompanyName());
        if (companyMaster1 != null
                && companyMaster1.getCompanyId() != company.getCompanyId()) { // company
            // already
            // exist
            if (companyMaster1.isActive())
                throw new DuplicateCompanyException(
                        CustomException.DUPLICATE_COMPANY.getCode());
            else
                throw new DuplicateCompanyException(
                        CustomException.DUPLICATE_DELETED_COMPANY.getCode());
        } 
        return companyRepository.save(company);
    }

    @Override
    public CompanyMaster findByUserId(long userId) {
        return companyRepository.findByUserId(userId);
    }

    @Override
    public List<CompanyMaster> findByActiveTrueOrderByCompanyNameAsc(int start,
            int end) {
        Page<CompanyMaster> newlyapproved = companyRepository.findByActiveTrueOrderByCompanyNameAsc(new PageRequest(start,end,Direction.DESC,"createdOn"));
        return newlyapproved.getContent();
    }

    @Override
    public int noOfOrgPerArea(String educationArea) {
        List<CompanyMaster> companyList = companyRepository.findByEduArea(educationArea);
        if(companyList.isEmpty()){
            return 0;
        }
        else{
            return companyRepository.NoOfOrgPerEduArea(educationArea);
        }
    }

    @Override
    public CompanyMaster findByCompanyEmail(String emailId) {
        return companyRepository.findByCompanyEmail(emailId);
    }

    @Override
    public CompanyMaster findByCompanyName(String companyName) {
        return companyRepository.findByCompanyName(companyName);
    }

    @Override
    public void createCompanyInfoXlSheet(CompanyMaster company)
            throws IOException, FileNotFoundException {
        Workbook wb = new XSSFWorkbook();
        CreationHelper createHelper = wb.getCreationHelper();
        Sheet sheet = wb.createSheet(REPORT);

        int rowIndex = 0;

        Row headerRow = sheet.createRow((short) rowIndex++);
        headerRow.createCell(0).setCellValue(
                createHelper.createRichTextString(ORGANISATION_NAME));
        headerRow.createCell(1).setCellValue(
                createHelper.createRichTextString("Company Address Line 1"));
        headerRow.createCell(2).setCellValue(
                createHelper.createRichTextString("Company Address Line 2"));
        headerRow.createCell(3).setCellValue(
                createHelper.createRichTextString("Company Address Line 3"));
        headerRow.createCell(4).setCellValue(
                createHelper.createRichTextString("Pincode"));
        headerRow.createCell(5).setCellValue(
                createHelper.createRichTextString("City"));
        headerRow.createCell(6).setCellValue(
                createHelper.createRichTextString("State"));
        headerRow.createCell(7).setCellValue(
                createHelper.createRichTextString("Country"));
        headerRow.createCell(8).setCellValue(
                createHelper.createRichTextString("Company Email"));
        headerRow.createCell(9).setCellValue(
                createHelper.createRichTextString("Company Telephone 1"));

        headerRow.createCell(10).setCellValue(
                createHelper.createRichTextString("Company Website"));
        headerRow.createCell(11).setCellValue(
                createHelper.createRichTextString("Field/Sector of Work"));
        headerRow.createCell(12).setCellValue(
                createHelper.createRichTextString("Organisation Vision"));
        headerRow.createCell(13).setCellValue(
                createHelper.createRichTextString("Organisation Mission"));
        headerRow.createCell(14).setCellValue(
                createHelper.createRichTextString("What does the organisation do?"));
        headerRow.createCell(15).setCellValue(
                createHelper.createRichTextString("Has/Had received assistance from other agency"));
        headerRow.createCell(16).setCellValue(
                createHelper.createRichTextString("Legal Type of Registration"));
        headerRow.createCell(17).setCellValue(
                createHelper.createRichTextString("FCRA registration"));
        headerRow.createCell(18).setCellValue(
                createHelper.createRichTextString("Type of Education organisation offers"));
        headerRow.createCell(19).setCellValue(
                createHelper.createRichTextString("Religious/Political Inclination"));
        headerRow.createCell(20).setCellValue(
                createHelper.createRichTextString("Reason for inclination if yes"));
        headerRow.createCell(21).setCellValue(
                createHelper.createRichTextString("Areas of program operations"));
        headerRow.createCell(22).setCellValue(
                createHelper.createRichTextString("Other Areas of operation if present"));
        headerRow.createCell(23).setCellValue(
                createHelper.createRichTextString("Type of Education organisation offers"));
        headerRow.createCell(24).setCellValue(
                createHelper.createRichTextString("Registration Number"));
        headerRow.createCell(25).setCellValue(
                createHelper.createRichTextString("Pan Number"));
        headerRow.createCell(26).setCellValue(
                createHelper.createRichTextString("12 A Certificate Number"));
        headerRow.createCell(27).setCellValue(
                createHelper.createRichTextString("80 G Number"));
        headerRow.createCell(28).setCellValue(
                createHelper.createRichTextString("FCRA Registration Number"));
        headerRow.createCell(29).setCellValue(
                createHelper.createRichTextString("Number of Trustees"));
        headerRow.createCell(30).setCellValue(
                createHelper.createRichTextString("Number of children in organisation program"));
        headerRow.createCell(31).setCellValue(
                createHelper.createRichTextString("Number of girls"));
        headerRow.createCell(32).setCellValue(
                createHelper.createRichTextString("Number of boys"));
        headerRow.createCell(33).setCellValue(
                createHelper.createRichTextString("Age of students served"));
        headerRow.createCell(34).setCellValue(
                createHelper.createRichTextString("Main source of funding"));
        headerRow.createCell(35).setCellValue(
                createHelper.createRichTextString("Number of employees"));
        headerRow.createCell(36).setCellValue(
                createHelper.createRichTextString("Budget Size"));
        headerRow.createCell(37).setCellValue(
                createHelper.createRichTextString("Existing funders"));
        headerRow.createCell(38).setCellValue(
                createHelper.createRichTextString("Other funders if present"));
        headerRow.createCell(39).setCellValue(
                createHelper.createRichTextString("Name of Trustees and Designations"));

        Row row = sheet.createRow((short) rowIndex);
        row.createCell(0).setCellValue(createHelper.createRichTextString(company.getCompanyName()));
        row.createCell(1).setCellValue(createHelper.createRichTextString(company.getCompanyAddressLine1()));
        row.createCell(2).setCellValue(createHelper.createRichTextString(company.getCompanyAddressLine2()));
        row.createCell(3).setCellValue(createHelper.createRichTextString(company.getCompanyAddressLine3()));	
        row.createCell(4).setCellValue(createHelper.createRichTextString(company.getCompanyPincode()));
        row.createCell(5).setCellValue(createHelper.createRichTextString(company.getCity().getCityName()));
        row.createCell(6).setCellValue(createHelper.createRichTextString(company.getState().getStateName()));
        row.createCell(7).setCellValue(createHelper.createRichTextString(company.getCountry().getCountryName()));
        row.createCell(8).setCellValue(createHelper.createRichTextString(company.getCompanyEmail()));
        row.createCell(9).setCellValue(createHelper.createRichTextString(company.getCompanyTelephone1()));
        row.createCell(10).setCellValue(createHelper.createRichTextString(company.getCompanyWebsite()));
        row.createCell(11).setCellValue(createHelper.createRichTextString(company.getSectorWork()));
        row.createCell(12).setCellValue(createHelper.createRichTextString(company.getOrganisationVision()));
        row.createCell(13).setCellValue(createHelper.createRichTextString(company.getOrganisationMission()));
        row.createCell(14).setCellValue(createHelper.createRichTextString(company.getOrganisationDescription()));
        if(!company.isAssitanceFromOtherAgency()){
            row.createCell(15).setCellValue(createHelper.createRichTextString("No"));
        }
        else{
            row.createCell(15).setCellValue(createHelper.createRichTextString("Yes"));
        }
        row.createCell(16).setCellValue(createHelper.createRichTextString(company.getLegalType()));
        if(!company.isFcraRegistarion()){
            row.createCell(17).setCellValue(createHelper.createRichTextString("No"));
        }
        else{
            row.createCell(17).setCellValue(createHelper.createRichTextString("Yes"));
        }

        row.createCell(18).setCellValue(createHelper.createRichTextString(company.getTypeOfEducation()));
        if(!company.isReligiousInclination()){
            row.createCell(19).setCellValue(createHelper.createRichTextString("No"));
        }
        else{
            row.createCell(19).setCellValue(createHelper.createRichTextString("Yes"));
        }
        row.createCell(20).setCellValue(createHelper.createRichTextString(company.getReasonOfInclination()));
        row.createCell(21).setCellValue(createHelper.createRichTextString(company.getAreaOfOperations()));
        row.createCell(22).setCellValue(createHelper.createRichTextString(company.getReasonForOtherArea()));
        row.createCell(23).setCellValue(createHelper.createRichTextString(company.getRegistrationNumber()));
        row.createCell(24).setCellValue(createHelper.createRichTextString(company.getPanNumber()));
        row.createCell(25).setCellValue(createHelper.createRichTextString(company.getTwelveACertificate()));
        row.createCell(26).setCellValue(createHelper.createRichTextString(company.getEightyGNumber()));
        row.createCell(27).setCellValue(createHelper.createRichTextString(company.getFcraRegNumber()));

        if(company.getNoOfTrustees() == 0){
            row.createCell(28).setCellValue(createHelper.createRichTextString(""));
        }
        else{
            row.createCell(28).setCellValue(createHelper.createRichTextString(Integer.toString(company.getNoOfTrustees())));
        }
        if(company.getNoOfChildren() == 0){
            row.createCell(29).setCellValue(createHelper.createRichTextString(""));
        }
        else{
            row.createCell(29).setCellValue(createHelper.createRichTextString(Integer.toString(company.getNoOfChildren())));
        }
        if(company.getNoOfGirls() == 0){
            row.createCell(30).setCellValue(createHelper.createRichTextString(""));
        }
        else{
            row.createCell(30).setCellValue(createHelper.createRichTextString(Integer.toString(company.getNoOfGirls())));
        }
        if(company.getNoOfBoys() == 0){
            row.createCell(31).setCellValue(createHelper.createRichTextString(""));
        }
        else{
            row.createCell(31).setCellValue(createHelper.createRichTextString(Integer.toString(company.getNoOfBoys())));
        }

        row.createCell(32).setCellValue(createHelper.createRichTextString(company.getAgeOfStudents()));
        row.createCell(33).setCellValue(createHelper.createRichTextString(company.getMainSourceOfFunding()));
        row.createCell(34).setCellValue(createHelper.createRichTextString(company.getNoOfEmployees()));
        if(company.getBudgetSize() == null){
            row.createCell(35).setCellValue(createHelper.createRichTextString(""));
        }
        else{
            row.createCell(35).setCellValue(createHelper.createRichTextString(Long.toString(company.getBudgetSize())));
        }	
        row.createCell(36).setCellValue(createHelper.createRichTextString(company.getExistingFunders()));
        row.createCell(37).setCellValue(createHelper.createRichTextString(company.getOtherFunders()));
        row.createCell(38).setCellValue(createHelper.createRichTextString(company.getTrustees()));

        String absolutePath = documentService.getAbsoluteSaveLocationDir(company.getCompanyName());
        FileOutputStream fileOut = new FileOutputStream(absolutePath + File.separator +"CompanyDetails.csv");
        wb.write(fileOut);
        fileOut.close();
    }

    @Override
    public void createCompanyInfoZip(CompanyMaster company)
            throws IOException {

        String absolutePath = documentService.getAbsoluteSaveLocationDir(company.getCompanyName());
        createCompanyInfoXlSheet(company);
        FileOutputStream fileOut = new FileOutputStream(absolutePath +".zip");
        ZipOutputStream zos = new ZipOutputStream(fileOut);
        byte[] buffer = new byte[1024];
        String folderToSearch=company.getCompanyName()+ File.separator;
        String companyDetailsFilePath = absolutePath + File.separator + "CompanyDetails.csv";
        String companyDetailsFile = URLDecoder.decode(companyDetailsFilePath.substring(companyDetailsFilePath.lastIndexOf(folderToSearch)),UTF8);
        ZipEntry ze= new ZipEntry(companyDetailsFile);
        zos.putNextEntry(ze);
        FileInputStream in = new FileInputStream(companyDetailsFilePath);
        int len;
        while ((len = in.read(buffer)) > 0) {
            zos.write(buffer, 0, len);
        }
        in.close();
        zos.closeEntry();

        if(company.getPanFile()!=null){
            String panFilePath = absolutePath + company.getPanFile().substring(company.getPanFile().lastIndexOf(File.separator)); 
            String panFile = URLDecoder.decode(panFilePath.substring(panFilePath.lastIndexOf(folderToSearch)),UTF8);
            ze= new ZipEntry(panFile);
            zos.putNextEntry(ze);
            in = new FileInputStream(panFilePath);
            while ((len = in.read(buffer)) > 0) {
                zos.write(buffer, 0, len);
            }
            in.close();
            zos.closeEntry();
        }
        if(company.getTwelveAFile()!=null){
            String twelveAFilePath = absolutePath + company.getTwelveAFile().substring(company.getTwelveAFile().lastIndexOf(File.separator)); 
            String twelveAFile = URLDecoder.decode(twelveAFilePath.substring(twelveAFilePath.lastIndexOf(folderToSearch)),UTF8);
            ze= new ZipEntry(twelveAFile);
            zos.putNextEntry(ze);
            in = new FileInputStream(twelveAFilePath);
            while ((len = in.read(buffer)) > 0) {
                zos.write(buffer, 0, len);
            }
            in.close();
            zos.closeEntry();
        }
        if(company.getEightyGFile()!=null){
            String eightyGFilePath = absolutePath + company.getEightyGFile().substring(company.getEightyGFile().lastIndexOf(File.separator)); 
            String eightyGFile = URLDecoder.decode(eightyGFilePath.substring(eightyGFilePath.lastIndexOf(folderToSearch)),UTF8); 
            ze= new ZipEntry(eightyGFile);
            zos.putNextEntry(ze);
            in = new FileInputStream(eightyGFilePath);
            while ((len = in.read(buffer)) > 0) {
                zos.write(buffer, 0, len);
            }
            in.close();
            zos.closeEntry();
        }
        if(company.getFcraFile()!=null){
            String fcraFilePath = absolutePath + company.getFcraFile().substring(company.getFcraFile().lastIndexOf(File.separator)); 
            String fcraFile = URLDecoder.decode(fcraFilePath.substring(fcraFilePath.lastIndexOf(folderToSearch)),UTF8);
            ze= new ZipEntry(fcraFile);
            zos.putNextEntry(ze); 
            in = new FileInputStream(fcraFilePath);
            while ((len = in.read(buffer)) > 0) {
                zos.write(buffer, 0, len);
            }
            in.close();
            zos.closeEntry();
        }
        if(company.getAudit1()!=null){
            String auditFile1Path = absolutePath + company.getAudit1().substring(company.getAudit1().lastIndexOf(File.separator)); 
            String auditFile1 = URLDecoder.decode(auditFile1Path.substring(auditFile1Path.lastIndexOf(folderToSearch)),UTF8);
            ze= new ZipEntry(auditFile1);
            zos.putNextEntry(ze);
            in = new FileInputStream(auditFile1Path);
            while ((len = in.read(buffer)) > 0) {
                zos.write(buffer, 0, len);
            }
            in.close();
            zos.closeEntry();
        }
        if(company.getAudit2()!=null){
            String auditFile2Path = absolutePath + company.getAudit2().substring(company.getAudit2().lastIndexOf(File.separator)); 

            String auditFile2 = URLDecoder.decode(auditFile2Path.substring(auditFile2Path.lastIndexOf(folderToSearch)),UTF8);
            ze= new ZipEntry(auditFile2);
            zos.putNextEntry(ze);
            in = new FileInputStream(auditFile2Path);
            while ((len = in.read(buffer)) > 0) {
                zos.write(buffer, 0, len);
            }
            in.close();
            zos.closeEntry();
        }
        zos.close();
        fileOut.close();	
    }

    @Override
    public List<Object[]> findOrgByCity() {
        return companyRepository.findOrgByCity();
    }

    @Override
    public List<Object[]> getLastMonthInactiveOrganisation(int a) {
        return companyRepository.getLastMonthInactiveOrganisation(a);
    }

    @Override
    public List<Object[]> getLastMonthInactiveOrganisationByModulUser(int a) {
        return companyRepository.getLastMonthInactiveOrganisationByModulUser(a);
    }

    @Override
    public List<Object[]> getLastTwoMonthInactiveOrganisation() {
        return companyRepository.getLastTwoMonthInactiveOrganisation();
    }

    @Override
    public List<Object[]> getInactiveOrg(long totalOraganisation2, int yr2) {
        List<Object[]> b=companyRepository.getInactiveOrg(totalOraganisation2,yr2);
        if(b!=null){
            return b;
        }else{
            return Collections.emptyList();
        }
    }


    @Override
    public int getInactiveOrgListExcel(long totalOraganisation2, int yr2,int month) {
        return companyRepository.getInactiveOrgListExcel(totalOraganisation2,yr2,month);
    }

    @Override
    public List<Object[]> getInactiveOrg1(int yr2) {

        List<Object[]> b=companyRepository.getInactiveOrg1(yr2);
        if(b!=null){
            return b;
        }else{
            return Collections.emptyList();
        }
    }

    @Override
    public List<Object[]> findCompanyDetail() {
        return companyRepository.findInactiveOrgInfo();

    }

    @Override
    public String createExcelSheetForInactiveOrg() 
            throws IOException, FileNotFoundException{

        Workbook wb = new XSSFWorkbook();
        CreationHelper createHelper = wb.getCreationHelper();
        Sheet sheet = wb.createSheet(REPORT);
        int rowIndex = 0;
        Row headerRow = sheet.createRow((short) rowIndex++);
        headerRow.createCell(0).setCellValue(
                createHelper.createRichTextString(ORGANISATION_NAME));
        headerRow.createCell(1).setCellValue(
                createHelper.createRichTextString("Company Admin Name"));
        headerRow.createCell(2).setCellValue(
                createHelper.createRichTextString("Contact Details"));
        List<Object[]> resultlist = findCompanyDetail();
        java.util.Iterator<Object[]> it = resultlist.iterator();
        while(it.hasNext()){
            Object[] obj= it.next();
            Row row1=sheet.createRow(rowIndex);
            row1.createCell(0).setCellValue(createHelper.createRichTextString(obj[0].toString()));
            row1.createCell(1).setCellValue(createHelper.createRichTextString(obj[1].toString()));
            row1.createCell(2).setCellValue(createHelper.createRichTextString(obj[2].toString()));
            rowIndex++;
        }

        String tempUrl=propertyValues.tempCsvDownloadpath;
        FileOutputStream fileOutUrl = new FileOutputStream( tempUrl+ File.separator +USERDETAILS_CSV);
        String fileOut=tempUrl+ File.separator +USERDETAILS_CSV;
        wb.write(fileOutUrl);
        fileOutUrl.close();
        return fileOut;
    }

    @Override
    public String createXlSheetForInactiveOrg() 
            throws IOException, FileNotFoundException{

        Workbook wb = new XSSFWorkbook();
        CreationHelper createHelper = wb.getCreationHelper();
        Sheet sheet = wb.createSheet(REPORT);
        int rowIndex = 0;
        Row headerRow = sheet.createRow((short) rowIndex++);
        headerRow.createCell(0).setCellValue(
                createHelper.createRichTextString(ORGANISATION_NAME));
        headerRow.createCell(1).setCellValue(
                createHelper.createRichTextString("Company Admin Name"));
        headerRow.createCell(2).setCellValue(
                createHelper.createRichTextString("Contact Details"));

        List<Object[]> resultlist = findCompanyDetail();
        java.util.Iterator<Object[]> it = resultlist.iterator();
        while(it.hasNext()){
            Object[] obj= it.next();
            Row row1=sheet.createRow(rowIndex);
            row1.createCell(0).setCellValue(createHelper.createRichTextString(obj[0].toString()));
            row1.createCell(1).setCellValue(createHelper.createRichTextString(obj[1].toString()));
            row1.createCell(2).setCellValue(createHelper.createRichTextString(obj[2].toString()));
            rowIndex++;
        }
        String tempUrl=propertyValues.tempCsvDownloadpath;
        FileOutputStream fileOutUrl = new FileOutputStream( tempUrl+ File.separator +USERDETAILS_CSV);
        String fileOut=tempUrl+ File.separator +USERDETAILS_CSV;
        wb.write(fileOutUrl);
        fileOutUrl.close();
        return fileOut;	
    }

    @Override
    public List<Object[]> getLastAddedOrganisation() {

        List<Object[]> b=companyRepository.getLastAddedOrganisation();
        if(b!=null){
            return b;
        }else{
            return Collections.emptyList();
        }
    }

    @Override
    public List<CompanyMaster> getInactiveFlagLastOrganisation() {
        return companyRepository.getInactiveFlagLastOrganisation();
    }

    @Override
    public CompanyMaster update1(CompanyMaster companyMaster) {
        return companyRepository.save(companyMaster);
    }

    @Override
    public List<Object[]> findByFiveOrgByCity(int start,
            int end) {
        Page<Object[]> newlyapproved = companyRepository.findByFiveOrgByCity(new PageRequest(start,end));
        return newlyapproved.getContent();
    }

    @Override
    public int getLastMonthActiveOrganisation() {
        return 0;
    }

    public List<CompanySurveyResetManager> findByActiveWithSurvey(boolean active) {

        List<CompanyMaster> companyMasters = companyRepository.findAllByActive(active);
        Map<Long,CompanySurveyResetManager> companySurveyResetManagerMap = new LinkedHashMap<>();
        List<Long> companyIds = new ArrayList<>();
        for (CompanyMaster companyMaster : companyMasters){
            companyIds.add(companyMaster.getCompanyId());
            companySurveyResetManagerMap.put(companyMaster.getCompanyId(),new CompanySurveyResetManager(companyMaster,null));
        }
        if(!companyIds.isEmpty()){
            List<SurveyManager>	surveyManagers = surveyManagerService.findLatestSurveyManagerByCompanyIds(companyIds);
            for (SurveyManager surveyManager : surveyManagers){
                if(SurveyStatus.COMPLETED.equals(surveyManager.getSurveyStatus()))
                    continue;
                CompanySurveyResetManager companySurveyResetManager = companySurveyResetManagerMap.get(surveyManager.getCompany().getCompanyId());
                companySurveyResetManager.setSurveyManager(surveyManager);
            }
        }
        return new ArrayList<>(companySurveyResetManagerMap.values());
    }

}


