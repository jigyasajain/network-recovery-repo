package com.astrika.companymngt.service.impl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.astrika.common.exception.NoSuchImageException;
import com.astrika.common.exception.NoSuchUserException;
import com.astrika.common.model.ImageMaster;
import com.astrika.common.service.ImageService;
import com.astrika.companymngt.model.module.ModuleMaster;
import com.astrika.companymngt.repository.ModuleRepository;
import com.astrika.companymngt.service.ModuleService;
import com.astrika.kernel.exception.BusinessException;
import com.astrika.kernel.exception.CustomException;
import com.astrika.modulemngt.exception.DuplicateModuleException;

@Service
public class ModuleServiceImpl implements ModuleService {

	@Autowired
	private ModuleRepository moduleRepository;

	@Autowired
	ImageService imageService;

	@Override
	public ModuleMaster findByModuleId(long moduleId) {
		return moduleRepository.findByModuleModuleId(moduleId);
	}

	@Override
	public ModuleMaster findByModuleName(String moduleName) {

		return moduleRepository.findByModuleName(moduleName);
	}

	@Override
	public ModuleMaster update(long moduleId, String moduleName,
			String moduleDescription, MultipartFile profile)
			throws BusinessException, IOException {
		ModuleMaster module = findByModuleName(moduleName);
		if (module != null && module.getModuleId() != moduleId) {
			throw new DuplicateModuleException(
					CustomException.DUPLICATE_MODULE.getCode());
		} else {

			module = findByModuleId(moduleId);
		
			if (profile != null && profile.getSize() > 0) {
				String filename = profile.getOriginalFilename();
				String ext = filename.substring(filename.lastIndexOf("."));
				ImageMaster profileImage = imageService.addImage(profile.getBytes(), ext,
						"ModuleImg" + ext, true, false, module.getModuleName()
								.toString());
				if (module.getProfileImage() != null) {
					imageService.delete(module.getProfileImage().getImageId());
				}
				module.setProfileImage(profileImage);
			}
			module.setModuleName(moduleName);
			module.setModuleDescription(moduleDescription);

			return moduleRepository.save(module);

		}
	}

	@Override
	public ModuleMaster save(String moduleName, String moduleDescription,
			MultipartFile profile) throws BusinessException, IOException {

		ModuleMaster module = moduleRepository.findByModuleName(moduleName);
		if (module != null && module.getModuleName().equals(moduleName)) {

				throw new DuplicateModuleException(
						CustomException.DUPLICATE_MODULE.getCode());

		}
		ImageMaster profileImage = null;

		ModuleMaster module1 = new ModuleMaster(moduleName, moduleDescription);
		module = moduleRepository.save(module1);
		if (profile != null && profile.getSize() > 0) {
			String filename = profile.getOriginalFilename();
			String ext = filename.substring(filename.lastIndexOf(".")+1);
			profileImage = imageService.addImage(profile.getBytes(), ext,
					"ModuleImg." + ext, true, false, module.getModuleName()
							.toString());
		}
		module.setProfileImage(profileImage);

		return moduleRepository.save(module);

	}

	@Override
	public ModuleMaster restore(Long moduleId) throws NoSuchUserException {

		ModuleMaster module = findByModuleId(moduleId);
		module.setActive(true);
		return moduleRepository.save(module);
	}

	@Override
	public ModuleMaster delete(Long moduleId) throws NoSuchUserException {

		ModuleMaster module = findByModuleId(moduleId);
		module.setActive(false);
		return moduleRepository.save(module);
	}

	public List<ModuleMaster> findByActive(boolean active) {
		return moduleRepository.findAllByActive(active);
	}

	@Override
	public ModuleMaster save(ModuleMaster module) {
		return moduleRepository.save(module);
	}

	@Override
	public ModuleMaster update(ModuleMaster module) {

		return moduleRepository.save(module);
	}

	@Override
	public ModuleMaster deleteSponsoredImage(Long moduleId)
			throws NoSuchImageException, UnsupportedEncodingException {
		ModuleMaster module = findByModuleId(moduleId);
		Long imageId = module.getSponsoredImage().getImageId();
		imageService.delete(imageId);
		module.setSponsoredImage(null);
		module.setLink(null);
		return moduleRepository.save(module);
	}

	@Override
	public ModuleMaster findByModuleIdandActive(long moduleId, boolean active) {
		return moduleRepository.findByModuleIdandActive(moduleId, active);
	}

	@Override
	public ModuleMaster deletePromoImage(Long moduleId)
			throws NoSuchImageException, UnsupportedEncodingException {
		ModuleMaster module = findByModuleId(moduleId);
		Long imageId = module.getPromotionalImage().getImageId();
		imageService.delete(imageId);
		module.setPromotionalImage(null);
		return moduleRepository.save(module);
	}

	@Override
	public List<ModuleMaster> findTheNextRow(Long moduleId) {
		Page<ModuleMaster> nextVal =  moduleRepository.findTheNextRow(moduleId, true, new PageRequest(0,1));
		return nextVal.getContent();
	}
	
	

}
