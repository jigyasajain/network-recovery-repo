package com.astrika.companymngt.exception;

import com.astrika.kernel.exception.BusinessException;
import com.astrika.kernel.exception.CustomException;

public class DuplicateCompanyException extends BusinessException {

	public DuplicateCompanyException() {
		super(CustomException.DUPLICATE_COMPANY.getCode());
	}

	public DuplicateCompanyException(String errorCode) {
		super(errorCode);
	}

}
	


