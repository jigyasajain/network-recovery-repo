package com.astrika.modulemngt.exception;

import com.astrika.common.exception.NoSuchModelException;
import com.astrika.kernel.exception.CustomException;

public class NoSuchModuleException extends NoSuchModelException{
	
	
	public NoSuchModuleException() {
		super(CustomException.NO_SUCH_MODULE.getCode());
	}

	public NoSuchModuleException(String msg) {
		super(msg);
	}

	public NoSuchModuleException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public NoSuchModuleException(Throwable cause) {
		super(cause);
	}


}
