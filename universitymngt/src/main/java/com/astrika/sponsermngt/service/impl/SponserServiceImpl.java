package com.astrika.sponsermngt.service.impl;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.astrika.common.exception.NoSuchUserException;
import com.astrika.common.service.ImageService;
import com.astrika.companymngt.service.ModuleService;
import com.astrika.sponsermngt.model.SponserManagement;
import com.astrika.sponsermngt.repository.SponserRepository;
import com.astrika.sponsermngt.service.SponserService;

@Service
public class SponserServiceImpl implements SponserService{

	@Autowired
	private SponserRepository sponserRepository;


	@Autowired
	private ModuleService moduleService;

	@Autowired
	private ImageService imageService;


	@Override
	public List<SponserManagement> findByActive(boolean active) {

		return sponserRepository.findAllByActive(active);
	}

	@Override
	public SponserManagement findBySponserId(long sponserId) {

		return sponserRepository.findBySponserId(sponserId);
	}

	@Override
	public SponserManagement save(SponserManagement sponser)
			throws IOException {
		return sponserRepository.save(sponser);
	}


	@Override
	public SponserManagement delete(long sponserId) throws NoSuchUserException {
		SponserManagement sponser=findBySponserId(sponserId);
		sponser.setActive(false);
		return sponserRepository.save(sponser) ;
	}

	@Override
	public SponserManagement restore(long sponserId) throws NoSuchUserException {
		SponserManagement sponser=findBySponserId(sponserId);
		sponser.setActive(true);
		return sponserRepository.save(sponser);	
	}

	@Override
	public SponserManagement findBySponserIdAndModuleId(long sponserId,long moduleId) {
		/*Auto-generated method stub*/
		return null;
	}

	@Override
	public SponserManagement update(SponserManagement sponser)
			throws IOException {
		/*Auto-generated method stub*/
		return null;
	}

}
