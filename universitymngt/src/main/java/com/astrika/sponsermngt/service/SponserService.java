package com.astrika.sponsermngt.service;

import java.io.IOException;
import java.util.List;

import com.astrika.common.exception.NoSuchUserException;
import com.astrika.sponsermngt.model.SponserManagement;

public interface SponserService {

	public List<SponserManagement> findByActive(boolean active);

	public SponserManagement findBySponserId(long sponserId);

	public SponserManagement findBySponserIdAndModuleId(long sponserId , long moduleId);

	public SponserManagement save(SponserManagement sponser) throws IOException;

	public SponserManagement update(SponserManagement sponser) throws IOException ;

	public SponserManagement delete(long sponserId) throws NoSuchUserException;

	public SponserManagement restore(long sponserId) throws NoSuchUserException;



}
