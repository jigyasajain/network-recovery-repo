package com.astrika.sponsermngt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.astrika.sponsermngt.model.SponserManagement;

public interface SponserRepository extends JpaRepository<SponserManagement, Long> {
	
	
	@Query("select s from SponserManagement s left join fetch s.createdBy left join fetch s.module  where s.active = ?1 ")
	List<SponserManagement> findAllByActive(boolean active);

	@Query("select s from SponserManagement s left join fetch s.module  where s.sponserId = ?1")
	SponserManagement findBySponserId(long sponserId);
	
	 @Query("select c from SponserManagement c  left join fetch c.module  where c.sponserId = ?1 and c.module.moduleId = ?2")
	 SponserManagement findBySponserIdAndModuleId(Long sponserId  , Long moduleId);	

}
