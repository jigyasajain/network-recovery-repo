package com.astrika.sponsermngt.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.envers.Audited;
import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import com.astrika.common.model.ImageMaster;
import com.astrika.common.model.User;
import com.astrika.companymngt.model.module.ModuleMaster;



@Entity
@Audited
public class SponserManagement {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long sponserId;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JsonIgnore
	private ModuleMaster module;
	
	@OneToOne
	private ImageMaster sponserdImage;
	
	@Column
	private String link;
	
	@CreatedDate
	private DateTime createdOn;

	@LastModifiedDate
	private DateTime lastModifiedOn;

	@JsonIgnore
	@CreatedBy
	@ManyToOne(fetch = FetchType.LAZY)
	private User createdBy;

	@JsonIgnore
	@LastModifiedBy
	@ManyToOne(fetch = FetchType.LAZY)
	private User lastModifiedBy;
	
	@Column(columnDefinition = "boolean default true", nullable = false)
	private boolean active = true;
	
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public long getSponserId() {
		return sponserId;
	}

	public void setSponserId(long sponserId) {
		this.sponserId = sponserId;
	}

	public ModuleMaster getModule() {
		return module;
	}

	public void setModule(ModuleMaster module) {
		this.module = module;
	}

	public ImageMaster getSponserdImage() {
		return sponserdImage;
	}

	public void setSponserdImage(ImageMaster sponserdImage) {
		this.sponserdImage = sponserdImage;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public DateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(DateTime createdOn) {
		this.createdOn = createdOn;
	}

	public DateTime getLastModifiedOn() {
		return lastModifiedOn;
	}

	public void setLastModifiedOn(DateTime lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public User getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(User lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	
}
