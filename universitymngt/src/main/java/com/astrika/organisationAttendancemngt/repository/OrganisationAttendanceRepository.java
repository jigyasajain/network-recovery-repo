package com.astrika.organisationAttendancemngt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.astrika.organisationAttendancemngt.model.OrganisationAttendance;

public interface OrganisationAttendanceRepository extends JpaRepository<OrganisationAttendance, Long>{
	
	@Query("select o from OrganisationAttendance o left join fetch o.company where o.company.companyId = ?1 and o.createdOn >= CURRENT_DATE")
	OrganisationAttendance findByOrganisationId(long companyId);
	
	@Query("select count(o), day(o.createdOn) from OrganisationAttendance o where year(o.createdOn) = ?1 and  month(o.createdOn) = ?2 and o.active = 0 group by day(o.createdOn) ")
	List<Object[]> countOfNoOfUsersInactive(int year,int month);

}
