package com.astrika.organisationAttendancemngt.service;

import java.util.List;

import com.astrika.companymngt.model.company.CompanyMaster;
import com.astrika.organisationAttendancemngt.model.OrganisationAttendance;

public interface OrganisationAttendanceService {
	
	public OrganisationAttendance save(OrganisationAttendance attendance);
	
	public OrganisationAttendance findByOrganisationId(long companyId);
	
	public OrganisationAttendance update(CompanyMaster company);
	
	public List<Long> getCountOfInactiveUsers(int month, int year);

}
