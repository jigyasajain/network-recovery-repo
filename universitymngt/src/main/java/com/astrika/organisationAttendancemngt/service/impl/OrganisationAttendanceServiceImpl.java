package com.astrika.organisationAttendancemngt.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.astrika.companymngt.model.company.CompanyMaster;
import com.astrika.organisationAttendancemngt.model.OrganisationAttendance;
import com.astrika.organisationAttendancemngt.repository.OrganisationAttendanceRepository;
import com.astrika.organisationAttendancemngt.service.OrganisationAttendanceService;

@Service
public class OrganisationAttendanceServiceImpl implements OrganisationAttendanceService{

	@Autowired
	private OrganisationAttendanceRepository attendanceRepository;

	@Override
	public OrganisationAttendance save(OrganisationAttendance attendance) {
		return attendanceRepository.save(attendance);
	}

	@Override
	public OrganisationAttendance findByOrganisationId(long companyId) {
		return attendanceRepository.findByOrganisationId(companyId);
	}

	@Override
	public OrganisationAttendance update(CompanyMaster company){
		OrganisationAttendance org = findByOrganisationId(company.getCompanyId());
		if(org == null){
			return null;
		}
		else{
			if(!org.isActive()){
				org.setActive(true);
				save(org);
			}
			return org;
		}	
	}

	@Override
	public List<Long> getCountOfInactiveUsers(int month,
			int year) {

		List<Object[]> users = attendanceRepository.countOfNoOfUsersInactive(year, month);
		List<Long> countList = new ArrayList<>(31);
		if(!users.isEmpty()){
			if(month == 1 || month == 3 || month == 5 || chechMonth(month)){
			    
				return buildCountListTwo(users, countList);
				
				
				
			}
			else if (month == 4 || month == 6 || ckeckMonthOne(month)) {
			    
				return buildCountListOne(users, countList);
				
			}
			else if(month == 2){
				boolean checkLeapYear = isLeapYear(year);
				if(checkLeapYear){
				    
				  return buildCountList(users,countList);
					
				}
				else{
					
					return checkCountList(users,countList);
					
				}
			}
			else{
				return Collections.emptyList();
			}
		}
		else{
			return Collections.emptyList();
		}

	}

	private static List<Long> buildCountListTwo(List<Object[]> users,
            List<Long> countList) {
	    for(int i = 0 ; i<=31; i++){
            for(Object[] user : users){
                Long count = (Long) user[0];
                int day = (int) user[1];
                if(i == day){
                    countList.add(i-1, count);
                }
                else {
                    countList.add(i, null);
                }

            }
        }
        return countList;
    }

    private static List<Long> buildCountListOne(List<Object[]> users, List<Long> countList) {
        
	    for (int i = 0; i <= 30; i++) {
            for (Object[] user : users) {
                Long count = (Long) user[0];
                int day = (int) user[1];
                if (i == day) {
                    countList.add(i-1, count);
                }
                else{
                    countList.add(i, null);
                }

            }
        }
        return countList;
    }

    private static List<Long> buildCountList(List<Object[]> users, List<Long> countList) {
        
	    for(int i = 0 ; i<=29; i++){
            for(Object[] user : users){
                Long count = (Long) user[0];
                int day = (int) user[1];
                if(i == day){
                    countList.add(i-1, count);
                }
                else{
                    countList.add(i, null);
                }

            }
        }
        return countList;
    }

    private static List<Long> checkCountList(List<Object[]> users, List<Long> countList) {
        
	    for(int i = 0 ; i<=28; i++){
            for(Object[] user : users){
                Long count = (Long) user[0];
                int day = (int) user[1];
                if(i == day){
                    countList.add(i-1, count);
                }
                else{
                    countList.add(i, null);
                }

            }
        }
	    
	    return countList;
    }

    private static boolean ckeckMonthOne(int month) {

	    return month == 9 || month == 11;
    }

    private static boolean chechMonth(int month) {
	    
	    return month == 7 ||month == 8 || month == 10 || month == 12;
    }

    public static boolean isLeapYear(int year) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		return cal.getActualMaximum(Calendar.DAY_OF_YEAR) > 365;
	}
}
