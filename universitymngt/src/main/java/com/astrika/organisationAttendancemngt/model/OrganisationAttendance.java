package com.astrika.organisationAttendancemngt.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.hibernate.envers.Audited;
import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedDate;

import com.astrika.companymngt.model.company.CompanyMaster;


@Entity
@Audited
public class OrganisationAttendance {
		
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long attendanceId;
	
	@OneToOne(fetch = FetchType.LAZY)
	private CompanyMaster company;
	
	@Column(columnDefinition = "boolean default false", nullable = false)
	private boolean active = false;
	
	@CreatedDate
	private DateTime createdOn;


	public OrganisationAttendance() {
		super();
	}

	public long getAttendanceId() {
		return attendanceId;
	}

	public void setAttendanceId(long attendanceId) {
		this.attendanceId = attendanceId;
	}

	public CompanyMaster getCompany() {
		return company;
	}

	public void setCompany(CompanyMaster company) {
		this.company = company;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public DateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(DateTime createdOn) {
		this.createdOn = createdOn;
	}

	
}
