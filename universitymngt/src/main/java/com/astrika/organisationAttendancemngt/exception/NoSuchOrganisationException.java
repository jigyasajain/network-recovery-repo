package com.astrika.organisationAttendancemngt.exception;

import com.astrika.kernel.exception.BusinessException;
import com.astrika.kernel.exception.CustomException;

public class NoSuchOrganisationException extends BusinessException{

	public NoSuchOrganisationException() {
		super(CustomException.NO_SUCH_COMPANY.getCode());
	}

	public NoSuchOrganisationException(String errorCode) {
		super(errorCode);
	}
	
}
