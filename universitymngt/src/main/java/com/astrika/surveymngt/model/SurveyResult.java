package com.astrika.surveymngt.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.envers.Audited;
import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import com.astrika.common.model.User;
import com.astrika.companymngt.model.company.CompanyMaster;
import com.astrika.companymngt.model.module.ModuleMaster;

@Entity
@Audited
public class SurveyResult {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long surveyResultId;
	
	@Enumerated(EnumType.ORDINAL)
	private LifeStages lifeStages;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JsonIgnore
	private ModuleMaster module;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JsonIgnore
	private CompanyMaster company;
	
	@Column
	private int totalOflifestage;
	
	@CreatedDate
	private DateTime createdOn;

	@LastModifiedDate
	private DateTime lastModifiedOn;

	@JsonIgnore
	@CreatedBy
	@ManyToOne(fetch = FetchType.LAZY)
	private User createdBy;

	@JsonIgnore
	@LastModifiedBy
	@ManyToOne(fetch = FetchType.LAZY)
	private User lastModifiedBy;
	
	@Column(columnDefinition = "boolean default true", nullable = false)
	private boolean active = true;

	public Long getSurveyResultId() {
		return surveyResultId;
	}

	public void setSurveyResultId(Long surveyResultId) {
		this.surveyResultId = surveyResultId;
	}

	public LifeStages getLifeStages() {
		return lifeStages;
	}

	public void setLifeStages(LifeStages lifeStages) {
		this.lifeStages = lifeStages;
	}

	public ModuleMaster getModule() {
		return module;
	}

	public void setModule(ModuleMaster module) {
		this.module = module;
	}

	public CompanyMaster getCompany() {
		return company;
	}

	public void setCompany(CompanyMaster company) {
		this.company = company;
	}

	public DateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(DateTime createdOn) {
		this.createdOn = createdOn;
	}

	public DateTime getLastModifiedOn() {
		return lastModifiedOn;
	}

	public void setLastModifiedOn(DateTime lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public User getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(User lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public int getTotalOflifestage() {
		return totalOflifestage;
	}

	public void setTotalOflifestage(int totalOflifestage) {
		this.totalOflifestage = totalOflifestage;
	}
	
}
