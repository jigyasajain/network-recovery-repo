package com.astrika.surveymngt.model;



public enum LifeStages{
	ALL_STAGES(1,"All Stages"),
	LIFE_STAGE1(2,"Life Stage 1"),
	LIFE_STAGE2(3,"Life Stage 2"),
	LIFE_STAGE3(4,"Life Stage 3"),
	LIFE_STAGE4(5,"Life Stage 4"),
	LIFE_STAGE5(6,"Life Stage 5"),
	NO_STAGE(7,"No Stage");
	
	private final int id;
	private String name;

	private LifeStages(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}

	public static LifeStages fromInt(int id){
		switch(id){
		case 1:
			return ALL_STAGES;
		case 2:
			return LIFE_STAGE1;
		case 3:
			return LIFE_STAGE2;
		case 4:
			return LIFE_STAGE3;
		case 5:
			return LIFE_STAGE4;
		case 6: 
			return LIFE_STAGE5;
		case 7:
			return NO_STAGE;
		default:
			return null;
		}
	}

	

}
