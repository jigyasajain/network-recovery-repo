package com.astrika.surveymngt.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.envers.Audited;
import org.joda.time.DateTime;

import com.astrika.companymngt.model.company.CompanyMaster;


@Entity
@Audited
public class SurveyManager {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long surveyManagerId;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JsonIgnore
	private CompanyMaster company;
	
	@Column
	private DateTime surveyPublished;
	
	@Column
	private DateTime lastDateToComplete;

	@Enumerated(EnumType.ORDINAL)
	private SurveyStatus surveyStatus;
	
	@Column
	private int totalModules;
	
	@Column
	private int totalModulesCompleted;
	
	@Column
	private boolean ongoingSurvey;
	
	@Column
	private boolean surveySkipOption;

	public SurveyManager(CompanyMaster company, DateTime lastDateToComplete,
			SurveyStatus surveyStatus) {
		super();
		this.company = company;
		this.lastDateToComplete = lastDateToComplete;
		this.surveyStatus = surveyStatus;
	}

	public SurveyManager() {
		super();
	}

	public Long getSurveyManagerId() {
		return surveyManagerId;
	}

	public void setSurveyManagerId(Long surveyManagerId) {
		this.surveyManagerId = surveyManagerId;
	}

	public CompanyMaster getCompany() {
		return company;
	}

	public void setCompany(CompanyMaster company) {
		this.company = company;
	}

	public DateTime getSurveyPublished() {
		return surveyPublished;
	}

	public void setSurveyPublished(DateTime surveyPublished) {
		this.surveyPublished = surveyPublished;
	}

	public DateTime getLastDateToComplete() {
		return lastDateToComplete;
	}

	public void setLastDateToComplete(DateTime lastDateToComplete) {
		this.lastDateToComplete = lastDateToComplete;
	}

	public SurveyStatus getSurveyStatus() {
		return surveyStatus;
	}

	public void setSurveyStatus(SurveyStatus surveyStatus) {
		this.surveyStatus = surveyStatus;
	}

	public int getTotalModules() {
		return totalModules;
	}

	public void setTotalModules(int totalModules) {
		this.totalModules = totalModules;
	}

	public int getTotalModulesCompleted() {
		return totalModulesCompleted;
	}

	public void setTotalModulesCompleted(int totalModulesCompleted) {
		this.totalModulesCompleted = totalModulesCompleted;
	}

	public boolean isOngoingSurvey() {
		return ongoingSurvey;
	}

	public void setOngoingSurvey(boolean ongoingSurvey) {
		this.ongoingSurvey = ongoingSurvey;
	}

	public boolean isSurveySkipOption() {
		return surveySkipOption;
	}

	public void setSurveySkipOption(boolean surveySkipOption) {
		this.surveySkipOption = surveySkipOption;
	}
	
}
