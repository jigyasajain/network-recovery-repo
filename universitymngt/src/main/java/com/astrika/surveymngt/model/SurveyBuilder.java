package com.astrika.surveymngt.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.envers.Audited;
import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import com.astrika.common.model.User;
import com.astrika.companymngt.model.module.ModuleMaster;
import com.astrika.universitymngt.model.Status;

@Entity
@Audited
public class SurveyBuilder {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long surveyBuilderId;

	@Enumerated(EnumType.ORDINAL)
	private LifeStages lifeStages;
	
	@Enumerated(EnumType.ORDINAL)
	private Status status;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JsonIgnore
	private ModuleMaster module;

	@Column
	private int questionNo;

	@Column
	private String question;

	@Column
	private String questionExplanation;

	@CreatedDate
	private DateTime createdOn;

	@LastModifiedDate
	private DateTime lastModifiedOn;

	@JsonIgnore
	@CreatedBy
	@ManyToOne(fetch = FetchType.LAZY)
	private User createdBy;

	@JsonIgnore
	@LastModifiedBy
	@ManyToOne(fetch = FetchType.LAZY)
	private User lastModifiedBy;

	@Column(columnDefinition = "boolean default true", nullable = false)
	private boolean active = true;
	
	public SurveyBuilder() {
        super();
    }

	public SurveyBuilder(int questionNo, String question, ModuleMaster module,
	        boolean active, LifeStages lifeStages, Status status) {
	    super();
	    this.questionNo = questionNo;
	    if(question.contains("<->")){
	        String[] parts = parseQuestionAndExplanation(question);
	        this.question = parts[0];
	        this.questionExplanation = parts[1];
	    }else {
	        this.question = question;
	        this.questionExplanation = "-";
	    }
	    this.module = module;
	    this.active = active;
	    this.lifeStages = lifeStages;
	    this.status = status;
	}

	public Status getStatus() {
	    return status;
	}

	public void setStatus(Status status) {
	    this.status = status;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Long getSurveyBuilderId() {
		return surveyBuilderId;
	}

	public void setSurveyBuilderId(Long surveyBuilderId) {
		this.surveyBuilderId = surveyBuilderId;
	}

	public LifeStages getLifeStages() {
		return lifeStages;
	}

	public void setLifeStages(LifeStages lifeStages) {
		this.lifeStages = lifeStages;
	}

	public ModuleMaster getModule() {
		return module;
	}

	public void setModule(ModuleMaster module) {
		this.module = module;
	}

	public int getQuestionNo() {
		return questionNo;
	}

	public void setQuestionNo(int questionNo) {
		this.questionNo = questionNo;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public DateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(DateTime createdOn) {
		this.createdOn = createdOn;
	}

	public DateTime getLastModifiedOn() {
		return lastModifiedOn;
	}

	public void setLastModifiedOn(DateTime lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public User getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(User lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public String getQuestionExplanation() {
		return questionExplanation;
	}

	public void setQuestionExplanation(String questionExplanation) {
		this.questionExplanation = questionExplanation;
	}

	public static String[] parseQuestionAndExplanation(String value){
		String[] parts = value.split("<->");
		if(parts.length == 2) 
		    return parts;
		return new String[]{parts[0] , "-"};
	}
}
