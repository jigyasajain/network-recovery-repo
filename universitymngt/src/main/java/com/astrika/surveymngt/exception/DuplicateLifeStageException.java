package com.astrika.surveymngt.exception;

import com.astrika.kernel.exception.BusinessException;
import com.astrika.kernel.exception.CustomException;

public class DuplicateLifeStageException extends BusinessException{

	public DuplicateLifeStageException() {
		super(CustomException.DUPLICATE_LIFESTAGE.getCode());
	}

	public DuplicateLifeStageException(String errorCode) {
		super(errorCode);
	}
}
