package com.astrika.surveymngt.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.astrika.surveymngt.model.SurveyResult;
import com.astrika.surveymngt.repository.SurveyResultRepository;
import com.astrika.surveymngt.service.SurveyResultService;

@Service
public class SurveyResultServiceImpl implements SurveyResultService{

	@Autowired
	SurveyResultRepository surveyResultRepository;

	@Override
	public SurveyResult save(SurveyResult surveyResult) {
		return surveyResultRepository.save(surveyResult);
	}

	@Override
	public int averageModuleResult(Long companyId, Long moduleId) {
		List<SurveyResult> surveyResult = surveyResultRepository.findByCompanyandModuleId(companyId, moduleId);
		int averageStage = 0;
		int totalStage = 0;
		if(!surveyResult.isEmpty()){
			for(SurveyResult su : surveyResult){
				averageStage = averageStage + su.getLifeStages().ordinal();
				totalStage = totalStage + 5;
			}
			return 100 * averageStage/totalStage;
			

		}
		else{
			return 0;
		}		
	}


	@Override
	public List<Object[]> getAllByModuleIdAndRating() {
		return surveyResultRepository.getAllByModuleIdAndRating();
	}

	@Override
	public List<Object[]> getAllByModuleIdAndRatingByMonthYear(int mnth,int yr) {
		return surveyResultRepository.getAllByModuleIdAndRatingByMonthYear(mnth,yr);
	}


	@Override
	public List<Object[]> 	getAllByModuleIdAndRatingByYear(Date dt) {
		return surveyResultRepository.	getAllByModuleIdAndRatingByYear(dt);
	}




	@Override
	public List<Object[]> getAllByModuleIdAndRatingByDate(String date) {
		return surveyResultRepository.getAllByModuleIdAndRatingByDate(date);
	}
}
