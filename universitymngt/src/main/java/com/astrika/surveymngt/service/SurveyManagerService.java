package com.astrika.surveymngt.service;

import java.io.IOException;
import java.util.List;

import com.astrika.surveymngt.model.SurveyManager;
import com.astrika.surveymngt.model.SurveyStatus;

public interface SurveyManagerService {
	public SurveyManager save(SurveyManager surveyManager);
	
	public SurveyManager findByCompanyId(long companyId);
	
	public SurveyManager findByCompanyIdandOngoingSurvey(long companyId, boolean ongoingSurvey);
	
	public SurveyManager findByCompanyIdandSurveyStatus(long companyId, SurveyStatus status);
	
	public SurveyManager findBySurveyManagerId(long surveyManagerId);

	public void resetSurveyBySurveyMangerId(Long surveyManagerId);

	public List<SurveyManager> findSurveyManagerByCompanyIds(List<Long> ids);

	public List<SurveyManager> findLatestSurveyManagerByCompanyIds(List<Long> ids);

	public List<SurveyManager> findLatestSurveyManagerByCompanyIdsAndStatus(List<Long> ids,SurveyStatus surveyStatus);

	public int getNoOfCompaniesTakenSurvey();

	public int getNoOdSurveyComplited();

	public String getIncompliteSurvey() throws IOException;
	
	List<Object[]>  getIncompliteLss();
}
