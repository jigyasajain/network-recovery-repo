package com.astrika.surveymngt.service;

import java.util.Date;
import java.util.List;

import com.astrika.surveymngt.model.SurveyResult;

public interface SurveyResultService {
	
	public SurveyResult save(SurveyResult surveyResult);
	
	public int averageModuleResult(Long companyId, Long moduleId);

	public List<Object[]> getAllByModuleIdAndRating();

	public List<Object[]> getAllByModuleIdAndRatingByMonthYear(int mnth, int yr);
	
	public List<Object[]> getAllByModuleIdAndRatingByYear(Date dt);
	
	
	public List<Object[]> getAllByModuleIdAndRatingByDate(String newDateString);
	
}
