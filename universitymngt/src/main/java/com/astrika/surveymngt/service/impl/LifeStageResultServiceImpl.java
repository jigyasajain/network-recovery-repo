package com.astrika.surveymngt.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.astrika.kernel.exception.CustomException;
import com.astrika.surveymngt.exception.DuplicateLifeStageException;
import com.astrika.surveymngt.model.LifeStageResultMaster;
import com.astrika.surveymngt.model.LifeStages;
import com.astrika.surveymngt.repository.LifeStageResultRepository;
import com.astrika.surveymngt.service.LifeStageResultService;

@Service
public class LifeStageResultServiceImpl implements LifeStageResultService{

	@Autowired
	private LifeStageResultRepository lsResultRepository;

	@Override
	public List<LifeStageResultMaster> findByActive(boolean active) {
		return lsResultRepository.findByActive(active);
	}

	@Override
	public LifeStageResultMaster findByModuleandLifeStage(long moduleId,
			LifeStages lifeStage) {
		return lsResultRepository.findByModuleandLifeStage(moduleId, lifeStage);
	}

	@Override
	public LifeStageResultMaster save(LifeStageResultMaster lifeStageResultData) throws DuplicateLifeStageException {
		LifeStageResultMaster resultSet = findByModuleandLifeStage(lifeStageResultData.getModule().getModuleId(), lifeStageResultData.getLifeStages());

		if (resultSet != null && resultSet.getLifeStages()
				.equals(lifeStageResultData.getLifeStages())) {
			throw new DuplicateLifeStageException(CustomException.DUPLICATE_LIFESTAGE.getCode());
		}

		return lsResultRepository.save(lifeStageResultData);
	}

	@Override
	public LifeStageResultMaster findByResultId(long resultId) {
		return lsResultRepository.findByResultId(resultId);
	}

	@Override
	public LifeStageResultMaster update(
			LifeStageResultMaster lifeStageResultData) {
		return lsResultRepository.save(lifeStageResultData);
	}

}
