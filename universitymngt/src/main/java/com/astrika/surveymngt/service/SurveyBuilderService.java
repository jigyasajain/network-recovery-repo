package com.astrika.surveymngt.service;

import java.util.List;

import com.astrika.companymngt.model.module.ModuleMaster;
import com.astrika.surveymngt.model.LifeStages;
import com.astrika.surveymngt.model.SurveyBuilder;
import com.astrika.universitymngt.model.Status;

public interface SurveyBuilderService {
	public List<SurveyBuilder> findBySurveyBuilderId(long surveyBuilderId);
	
	public SurveyBuilder save(SurveyBuilder surveyBuilder);
	
	public List<SurveyBuilder> findByActive(boolean active);
	
	public List<SurveyBuilder> findByModuleId(long moduleId);

	public List<SurveyBuilder> findByModuleIdOrderByQuestionNo(long moduleId);
	
	public List<SurveyBuilder> findByLifeStagesAndModuleId(LifeStages lifeStages,long moduleId);
	
	public void deleteAllByModuleId(long moduleId);
	
	public List<SurveyBuilder> findByStatus(Status status,long moduleId);
	
	public List<SurveyBuilder> findByLifeStagesandModuleIdandStatus(LifeStages lifeStages,long moduleId, Status status);
	
	public List<SurveyBuilder> findByModuleIdandStatus(long moduleId, Status status);

	public void processSurveyBuilderUpdateAlongWithVistorSurveys(List<SurveyBuilder> surveyBuilders, ModuleMaster moduleMaster);

}
