package com.astrika.surveymngt.service;

import java.util.List;

import com.astrika.surveymngt.exception.DuplicateLifeStageException;
import com.astrika.surveymngt.model.LifeStageResultMaster;
import com.astrika.surveymngt.model.LifeStages;

public interface LifeStageResultService {
	
	public List<LifeStageResultMaster> findByActive(boolean active);
	
	public LifeStageResultMaster findByModuleandLifeStage(long moduleId, LifeStages lifeStage);
	
	public LifeStageResultMaster save(LifeStageResultMaster lifeStageResultData) throws DuplicateLifeStageException;
	
	public LifeStageResultMaster findByResultId(long resultId);
	
	public LifeStageResultMaster update(LifeStageResultMaster lifeStageResultData);

}
