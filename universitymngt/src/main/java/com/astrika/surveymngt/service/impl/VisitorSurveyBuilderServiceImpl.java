package com.astrika.surveymngt.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.astrika.surveymngt.model.LifeStages;
import com.astrika.surveymngt.model.SurveyManager;
import com.astrika.surveymngt.model.SurveyStatus;
import com.astrika.surveymngt.model.VisitorSurveyBuilder;
import com.astrika.surveymngt.repository.VisitorSurveyBuilderRepository;
import com.astrika.surveymngt.service.SurveyManagerService;
import com.astrika.surveymngt.service.VisitorSurveyBuilderService;

@Service
public class VisitorSurveyBuilderServiceImpl implements VisitorSurveyBuilderService{
	
	@Autowired
	public VisitorSurveyBuilderRepository visitorSurveyBuilderRepository;
	
	@Autowired
	public SurveyManagerService surveyManagerService;

	@Override
	public VisitorSurveyBuilder save(VisitorSurveyBuilder visitorSurveyBuilder) {
		return visitorSurveyBuilderRepository.save(visitorSurveyBuilder);
	}

	@Override
	public void save(List<VisitorSurveyBuilder> visitorSurveyBuilders) {
		visitorSurveyBuilderRepository.save(visitorSurveyBuilders);
	}

	@Override
	public List<VisitorSurveyBuilder> findByModuleIdandUserIdandCompanyId(long moduleId, long userId, long companyId) {
		SurveyManager survey = surveyManagerService.findByCompanyIdandSurveyStatus(companyId, SurveyStatus.PENDING);
		return visitorSurveyBuilderRepository.findByModuleIdandUserIdandSurveyManager(moduleId, userId, survey.getSurveyManagerId());
	}

	@Override
	public void deleteAllByModuleIdandUserIdandCompanyId(long moduleId, long userId, long companyId) {
		SurveyManager survey = surveyManagerService.findByCompanyIdandSurveyStatus(companyId, SurveyStatus.PENDING);
		visitorSurveyBuilderRepository.deleteAllByModuleIdandUserIdandSurveyStatus(moduleId, userId, survey.getSurveyManagerId());
	}

	@Override
	public List<VisitorSurveyBuilder> findByLifeStagesAndModuleIdandUserIdandSurveyId(
			LifeStages lifeStages, long moduleId, long userId, long surveyId) {
		return visitorSurveyBuilderRepository. findByLifeStagesAndModuleIdandUserIdandSurveyManager(lifeStages,moduleId, userId, surveyId);
	}

	@Override
	public List<VisitorSurveyBuilder> findByModuleIdandUserIdandSurveyId(
			long moduleId, long userId, long surveyId) {
		return visitorSurveyBuilderRepository.findByModuleIdandUserIdandSurveyManager(moduleId, userId, surveyId);
	}

	@Override
	public List<VisitorSurveyBuilder> findByModuleIdandQuestionNo(long moduleId, int questionNo) {
		return visitorSurveyBuilderRepository.findByModuleIdandQuestionNo(moduleId,questionNo);
	}


}
