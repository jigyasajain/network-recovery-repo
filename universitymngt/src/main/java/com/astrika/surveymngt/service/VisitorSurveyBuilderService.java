package com.astrika.surveymngt.service;


import java.util.List;

import com.astrika.surveymngt.model.LifeStages;
import com.astrika.surveymngt.model.VisitorSurveyBuilder;

public interface VisitorSurveyBuilderService {
	
	public VisitorSurveyBuilder save(VisitorSurveyBuilder visitorSurveyBuilder);

	public void save(List<VisitorSurveyBuilder> visitorSurveyBuilders);
	
	public List<VisitorSurveyBuilder> findByModuleIdandUserIdandCompanyId(long moduleId, long userId, long companyId);
	
	public void deleteAllByModuleIdandUserIdandCompanyId(long moduleId, long userId, long companyId);
	
	public List<VisitorSurveyBuilder> findByLifeStagesAndModuleIdandUserIdandSurveyId(LifeStages lifeStages,long moduleId, long userId, long surveyId);
	
	public List<VisitorSurveyBuilder> findByModuleIdandUserIdandSurveyId(long moduleId, long userId, long surveyId);

	public List<VisitorSurveyBuilder> findByModuleIdandQuestionNo(long moduleId, int questionNo);

}
