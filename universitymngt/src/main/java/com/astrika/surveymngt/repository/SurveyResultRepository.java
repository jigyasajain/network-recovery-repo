package com.astrika.surveymngt.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.astrika.surveymngt.model.SurveyResult;

public interface SurveyResultRepository extends JpaRepository<SurveyResult, Long> {
	
	@Query("select s from SurveyResult s left join fetch s.company left join fetch s.module where s.company.companyId = ?1 and s.module.moduleId = ?2")
	List<SurveyResult> findByCompanyandModuleId(Long companyId, Long moduleId);

    @Query("select count(s.company.companyId), s.module.moduleId, s.lifeStages,s.module.moduleName from SurveyResult s where s.lifeStages BETWEEN 1 and 5 and s.module.moduleName!='Guest Login Sample Module' group by s.module.moduleId,s.lifeStages")
	List<Object[]> getAllByModuleIdAndRating();

    @Query("select count(s.company.companyId), s.module.moduleId, s.lifeStages,s.module.moduleName from SurveyResult s where s.lifeStages BETWEEN 1 and 5 and month(s.createdOn)<=?1 and year(s.createdOn)<=?2 and s.module.moduleName!='Guest Login Sample Module' group by s.module.moduleId,s.lifeStages")
	List<Object[]> getAllByModuleIdAndRatingByMonthYear(int mnth, int yr);


    @Query("select count(s.company.companyId), s.module.moduleId, s.lifeStages,s.module.moduleName from SurveyResult s where s.createdOn <?1 and s.lifeStages BETWEEN 1 and 5  group by s.module.moduleId,s.lifeStages")
	List<Object[]> getAllByModuleIdAndRatingByDate(String date);

	
	@Query("select count(s.company.companyId), s.module.moduleId, s.lifeStages,s.module.moduleName from SurveyResult s where date(s.createdOn) <?1 and s.lifeStages BETWEEN 1 and 5  group by s.module.moduleId,s.lifeStages")
	List<Object[]> getAllByModuleIdAndRatingByYear(Date dt);
		
}
