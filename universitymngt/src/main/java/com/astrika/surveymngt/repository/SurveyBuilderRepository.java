package com.astrika.surveymngt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.astrika.surveymngt.model.LifeStages;
import com.astrika.surveymngt.model.SurveyBuilder;
import com.astrika.universitymngt.model.Status;

public interface SurveyBuilderRepository extends JpaRepository<SurveyBuilder, Long> {
	
	@Query("select c from SurveyBuilder c left join fetch c.module where c.surveyBuilderId=?1")
   List <SurveyBuilder> findBySurveyBuilderId(long surveyBuilderId);
	
	@Query("select s from SurveyBuilder s left join fetch s.module where s.active = ?1 ")
	List<SurveyBuilder> findByActive(boolean active);
	
	@Query("select c from SurveyBuilder c where c.module.moduleId = ?1")
    List<SurveyBuilder> findByModuleId(Long moduleId);

	@Query("select c from SurveyBuilder c where c.module.moduleId = ?1 order by c.questionNo")
	List<SurveyBuilder> findByModuleIdOrderByQuestionNo(Long moduleId);
	
	
	@Query("select c from SurveyBuilder c where c.lifeStages=?1 and c.module.moduleId=?2")
	List<SurveyBuilder> findByLifeStagesAndModuleId(LifeStages lifeStages,long moduleId);
	
	@Modifying
	@Transactional
	@Query("delete from SurveyBuilder c where c.module.moduleId =?1")
	void deleteAllByModuleId(long moduleId);
	
	@Query("select c from SurveyBuilder c where c.status=?1 and c.module.moduleId=?2 ")
	List<SurveyBuilder> findByStatus(Status status,long moduleId);
	
	@Query("select c from SurveyBuilder c where c.lifeStages=?1 and c.module.moduleId=?2 and c.status=?3")
	List<SurveyBuilder> findByLifeStagesandModuleIdandStatus(LifeStages lifeStages,long moduleId, Status status);
	
	@Query("select c from SurveyBuilder c where c.module.moduleId=?1 and c.status=?2")
	List<SurveyBuilder> findByModuleIdandStatus(long moduleId, Status status);
}
