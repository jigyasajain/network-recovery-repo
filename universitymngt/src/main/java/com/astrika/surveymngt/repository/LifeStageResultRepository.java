package com.astrika.surveymngt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.astrika.surveymngt.model.LifeStageResultMaster;
import com.astrika.surveymngt.model.LifeStages;

public interface LifeStageResultRepository extends JpaRepository<LifeStageResultMaster, Long> {
	
	@Query("select r from LifeStageResultMaster r left join fetch r.module where r.active=?1")
	List<LifeStageResultMaster> findByActive(boolean active);
	
	@Query("select r from LifeStageResultMaster r left join fetch r.module where r.module.moduleId=?1 and r.lifeStages=?2")
	LifeStageResultMaster findByModuleandLifeStage(long moduleId, LifeStages lifeStage);
	
	@Query("select r from LifeStageResultMaster r left join fetch r.module where r.resultId =?1")
	LifeStageResultMaster findByResultId(long resultId);

}
