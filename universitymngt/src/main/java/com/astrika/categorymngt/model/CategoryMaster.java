package com.astrika.categorymngt.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.envers.Audited;
import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import com.astrika.common.model.User;
import com.astrika.companymngt.model.module.ModuleMaster;





@Entity
@Audited
public class CategoryMaster {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long categoryId;
	
	@Column(nullable = false, length = 75)
	private String categoryName;
	
	@Column(length=500)
	private String categoryDescription;
	
	@Column(nullable=false, length=255)
	private String categorySummary;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JsonIgnore
	private ModuleMaster module;
	
	@Column(columnDefinition = "boolean default true", nullable = false)
	private boolean active = true;
	
	@CreatedDate
	private DateTime createdOn;

	@LastModifiedDate
	private DateTime lastModifiedOn;

	@JsonIgnore
	@CreatedBy
	@ManyToOne(fetch = FetchType.LAZY)
	private User createdBy;

	@JsonIgnore
	@LastModifiedBy
	@ManyToOne(fetch = FetchType.LAZY)
	private User lastModifiedBy;
	 
	
	public CategoryMaster(){
		super();
	}
	
	public CategoryMaster(String categoryName)
	
	{
		super();
		this.categoryName=categoryName;
	}
	
	public CategoryMaster(String categoryName,String categoryDescription, ModuleMaster module, String categorySummary)
	{
		super();
		this.categoryName=categoryName;
		this.categoryDescription=categoryDescription;
		this.module = module;
		this.categorySummary = categorySummary;
	}

	public long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCategoryDescription() {
		return categoryDescription;
	}

	public void setCategoryDescription(String categoryDescription) {
		this.categoryDescription = categoryDescription;
	}

	public ModuleMaster getModule() {
		return module;
	}

	public void setModule(ModuleMaster module) {
		this.module = module;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public DateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(DateTime createdOn) {
		this.createdOn = createdOn;
	}

	public DateTime getLastModifiedOn() {
		return lastModifiedOn;
	}

	public void setLastModifiedOn(DateTime lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public User getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(User lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public String getCategorySummary() {
		return categorySummary;
	}

	public void setCategorySummary(String categorySummary) {
		this.categorySummary = categorySummary;
	}

	
}
