package com.astrika.categorymngt.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.astrika.categorymngt.model.CategoryMaster;
import com.astrika.categorymngt.service.CategoryService;
import com.astrika.kernel.exception.BusinessException;


@Controller
@RequestMapping("/category/*")
public class CategoryController {
	
		@Autowired
		private CategoryService categoryService;
		
		@RequestMapping("/categorybymoduleid/{moduleId}")
		@ResponseBody
		public List<CategoryMaster> getCategory(@PathVariable("moduleId") Long moduleId)
				throws BusinessException {
			return categoryService.findByModuleId(moduleId);
		}
}
