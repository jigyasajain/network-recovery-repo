package com.astrika.categorymngt.service.impl;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.astrika.categorymngt.exception.DuplicateCategoryException;
import com.astrika.categorymngt.model.CategoryMaster;
import com.astrika.categorymngt.repository.CategoryRepository;
import com.astrika.categorymngt.service.CategoryService;
import com.astrika.common.exception.NoSuchUserException;
import com.astrika.common.service.ImageService;
import com.astrika.companymngt.model.module.ModuleMaster;
import com.astrika.companymngt.service.ModuleService;
import com.astrika.kernel.exception.BusinessException;
import com.astrika.kernel.exception.CustomException;

@Service
public class CategoryServiceImpl implements CategoryService{
	
	
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	@Autowired
	private ModuleService moduleService;
	
	@Autowired
	private ImageService imageService;

	@Override
	public List<CategoryMaster> findByActive(boolean active) {
		return categoryRepository.findAllByActive(active);
	}

	@Override
	public CategoryMaster findByCategoryId(long categoryId) {
		return categoryRepository.findByCategoryId(categoryId);
	}

	

	
	@Override
	public CategoryMaster restore(Long categoryId) throws NoSuchUserException {

		CategoryMaster category=findByCategoryId(categoryId);
		category.setActive(true);
		return categoryRepository.save(category);
	}

	@Override
	public CategoryMaster delete(Long categoryId) throws NoSuchUserException {
		
			CategoryMaster category=findByCategoryId(categoryId);
			category.setActive(false);
			return categoryRepository.save(category);
		}

	@Override
	public CategoryMaster save(String categoryName, String categoryDescription, Long moduleId, String categorySummary)
			throws  BusinessException, IOException {
		CategoryMaster category = findByCategoryNameAndModuleId(categoryName, moduleId);
		
		if(category !=null && category.getCategoryName().equals(categoryName))
		{

			throw new DuplicateCategoryException(CustomException.DUPLICATE_CATEGORY.getCode());
			
		}
		
		ModuleMaster module = moduleService.findByModuleId(moduleId);
		CategoryMaster category1 = new CategoryMaster(categoryName,categoryDescription, module, categorySummary);
		category = categoryRepository.save(category1);
		return categoryRepository.save(category);
	}


	@Override
	public List<CategoryMaster> findByCategoryIdAndActiveTrue(long categoryId) {
		
		return categoryRepository.findByCategoryCategoryIdAndActiveTrueOrderByCategoryNameAsc(categoryId);
	}

	@Override
	public List<CategoryMaster> findByCategoryIdAndActiveFalse(long categoryId) {
		
	    return Collections.emptyList();
	}

	@Override
	public CategoryMaster update(Long categoryId,String categoryName,String categoryDescription ,Long moduleId, String categorySummary) throws BusinessException, IOException{
		CategoryMaster categoryMaster1 = findByCategoryNameAndModuleId(categoryName ,moduleId);
		if( categoryMaster1 != null && categoryMaster1.getCategoryId() != categoryId && categoryMaster1.isActive()){						//company already exist
				throw new DuplicateCategoryException(CustomException.DUPLICATE_CATEGORY.getCode());
		}
		    CategoryMaster categoryMaster =findByCategoryId(categoryId); 
            categoryMaster.setCategoryName(categoryName);
			categoryMaster.setCategoryDescription(categoryDescription);
			categoryMaster.setCategorySummary(categorySummary);
			ModuleMaster module = moduleService.findByModuleId(moduleId);
			categoryMaster.setModule(module);
			return categoryRepository.save(categoryMaster);
		}
	

	@Override
	public CategoryMaster findByCategoryName(String categoryName) {
		
		return categoryRepository.findByCategoryName(categoryName);
	}

	@Override
	public CategoryMaster findByCategoryNameAndModuleId(String categoryName,
			Long moduleId) {
		
		return categoryRepository.findByCategoryNameAndModuleId(categoryName, moduleId);
	}


	@Override
	public List<CategoryMaster> findByModuleId(long moduleId) {
		return categoryRepository.findByModuleId(moduleId);
	}

	@Override
	public List<CategoryMaster> findByModuleIdandActive(Long moduleId,
			boolean active) {
		return categoryRepository.findByModuleIdandActive(moduleId, active);
	}

	@Override
	public int totalActiveCategoriesByModule(Long moduleId) {
		 List<CategoryMaster> categoryList = findByModuleIdandActive(moduleId,
					true);
		 if(categoryList.isEmpty()){
			 return 0;
		 }
		 else{
			 return categoryRepository.totalActiveCategoriesbyModule(moduleId, true);
		 }
		
	}

	

	

}
