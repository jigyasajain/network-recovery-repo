package com.astrika.categorymngt.service;

import java.io.IOException;
import java.util.List;

import com.astrika.categorymngt.model.CategoryMaster;
import com.astrika.common.exception.NoSuchUserException;




public interface CategoryService {
	
	public List<CategoryMaster> findByActive(boolean active); 
	
	public CategoryMaster findByCategoryId(long categoryId);
	
	public CategoryMaster findByCategoryName(String categoryName);
	
	public CategoryMaster findByCategoryNameAndModuleId(String categoryName,Long moduleId);
	
	public List<CategoryMaster> findByCategoryIdAndActiveTrue(long categoryId);
	
	public List<CategoryMaster> findByCategoryIdAndActiveFalse(long categoryId);  
	
	public CategoryMaster delete(Long categoryId) throws NoSuchUserException ;
	
	public CategoryMaster restore(Long categoryId) throws NoSuchUserException ;
	
	public CategoryMaster save(String categoryName,String categoryDescription, Long moduleId, String categorySummary) throws IOException ;
	
	public CategoryMaster update(Long categoryId,String categoryName,String categoryDescription ,Long moduleId, String categorySummary) throws IOException;

	public List<CategoryMaster> findByModuleId(long moduleId);
	
	public List<CategoryMaster> findByModuleIdandActive(Long moduleId, boolean active);
	
	public int totalActiveCategoriesByModule(Long moduleId);
	

}
