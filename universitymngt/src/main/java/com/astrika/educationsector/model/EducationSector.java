package com.astrika.educationsector.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.envers.Audited;



@Entity
@Audited
public class EducationSector {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column
	private String educationtype;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getEducationtype() {
		return educationtype;
	}

	public void setEducationtype(String educationtype) {
		this.educationtype = educationtype;
	}
	
	
}
