package com.astrika.globalsponsoredmngt.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.envers.Audited;
import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import com.astrika.common.model.ImageMaster;
import com.astrika.common.model.User;


@Entity
@Audited
public class GlobalSponsoredManagement {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long globalId;
	
	@ManyToOne
	private ImageMaster globalImage;
	
	@OneToOne
	private ImageMaster contactImage;
	
	@Column
	private String imageType;
	
	@JsonIgnore
	@CreatedBy
	@ManyToOne(fetch = FetchType.LAZY)
	private User createdBy;
	
	@CreatedDate
	private DateTime createdOn;
	
	@LastModifiedDate
	private DateTime lastModifiedOn;
	
	@JsonIgnore@LastModifiedBy
	@ManyToOne(fetch = FetchType.LAZY)
	private User lastModifiedBy;
	
	@Column(columnDefinition = "boolean default true", nullable = false)
	private boolean active = true;

	public GlobalSponsoredManagement() {
		super();
	}

	public GlobalSponsoredManagement(ImageMaster globalImage,
			ImageMaster contactImage, String imageType) {
		super();
		this.globalImage = globalImage;
		this.contactImage = contactImage;
		this.imageType = imageType;
	}

	public String getImageType() {
		return imageType;
	}

	public void setImageType(String imageType) {
		this.imageType = imageType;
	}

	public Long getGlobalId() {
		return globalId;
	}

	public void setGlobalId(Long globalId) {
		this.globalId = globalId;
	}

	public ImageMaster getGlobalImage() {
		return globalImage;
	}

	public void setGlobalImage(ImageMaster globalImage) {
		this.globalImage = globalImage;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public DateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(DateTime createdOn) {
		this.createdOn = createdOn;
	}

	public DateTime getLastModifiedOn() {
		return lastModifiedOn;
	}

	public void setLastModifiedOn(DateTime lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	public User getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(User lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public ImageMaster getContactImage() {
		return contactImage;
	}

	public void setContactImage(ImageMaster contactImage) {
		this.contactImage = contactImage;
	}
	
	
	
}