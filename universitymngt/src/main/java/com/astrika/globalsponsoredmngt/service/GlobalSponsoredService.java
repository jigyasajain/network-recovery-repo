package com.astrika.globalsponsoredmngt.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.astrika.common.model.ImageMaster;
import com.astrika.globalsponsoredmngt.model.GlobalSponsoredManagement;

public interface GlobalSponsoredService {

	public List<GlobalSponsoredManagement> findByActive(boolean active);

	public GlobalSponsoredManagement save(List<ImageMaster> globalSponsorImage,
			MultipartFile contactimage, String imageType) throws IOException;

	public void deleteImage(long imageId) throws UnsupportedEncodingException;

	public GlobalSponsoredManagement findByImageType(String imageType);

	public GlobalSponsoredManagement findByImageId(long imageId);

	public GlobalSponsoredManagement update(GlobalSponsoredManagement sponsorManagement);

	public List<GlobalSponsoredManagement> findByActiveandImageTypeGlobalSponsor(boolean active, String imageType);

	public GlobalSponsoredManagement findByActiveandImageTypeContact(boolean active, String imageType);
}
