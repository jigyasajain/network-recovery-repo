package com.astrika.moduleusermanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.astrika.common.model.Role;
import com.astrika.common.model.User;
import com.astrika.common.model.UserStatus;
import com.astrika.companymngt.model.module.ModuleMaster;
import com.astrika.moduleusermanagement.model.ModuleUser;

public interface ModuleUserRepository extends JpaRepository<ModuleUser, Long>{

	@Query("select c from ModuleUser c left join fetch c.module  where c.moduleuserId = ?1")
	ModuleUser findByModuleuserId(long moduleuserId);

	@Query("select c from ModuleUser c join fetch c.module where c.module.moduleId = ?1")
	ModuleUser findByModuleId(Long moduleId);

	@Query("select c from ModuleUser c join fetch c.module where c.user.userId = ?1")
	List<ModuleUser> findByUserId(Long userId);

	@Query("select u from User u  where u.role = ?1 and u.status = ?2 ") 
	List<User> findByRoleAndStatus(Role role, UserStatus status);

	@Query("select c from ModuleMaster c where c.active=1")
	List<ModuleMaster> findByActive(boolean active);

	@Query("select c from ModuleUser c left join fetch c.module left join fetch c.user left join fetch c.company co where co.companyId  = ?1")
	List<ModuleUser> findByCompanyCompanyId(Long companyId);

	@Query("select c from ModuleUser c left join fetch c.company left join fetch c.module left join fetch c.user where c.company.companyId = ?1 and c.module.moduleId = ?2")
	ModuleUser findByCompanyIdAndModuleId(Long companyId , Long moduleId);	

	@Query("select c from ModuleUser c left join fetch c.company left join fetch c.module left join fetch c.user where c.user.userId = ?1 and c.module.moduleId = ?2")
	ModuleUser findByUserIdandModuleId(Long userId, Long moduleId);

	@Query("select count(c.user.userId) from ModuleUser c where c.module.moduleId=?1")
	int totalUsersperModule(long moduleId);

	@Query("select count(c.company.companyId), c.module.moduleId, c.totalOflifestage,c.module.moduleName from ModuleUser c where c.totalOflifestage BETWEEN 1 and 5 group by c.module.moduleId,c.totalOflifestage")
	List<Object[]> getAllByModuleIdAndRating();

	@Query("select max(totalOflifestage) from ModuleUser")
	int getmaxRating();

	@Query("select count(c.company.companyId), c.module.moduleId, c.totalOflifestage,c.module.moduleName from ModuleUser c where c.totalOflifestage BETWEEN 1 and 5 and month(c.createdOn)=?1 and year(c.createdOn)=?2 group by c.module.moduleId,c.totalOflifestage")
	List<Object[]> getAllByModuleIdAndRatingByMonthYear(int mnth, int yr);

	@Query("select avg(c.lifeStages),count(c.company.companyId),c.module.moduleName from ModuleUser c where c.module.moduleName!='Guest Login Sample Module' and c.company.active=1 and  (c.lifeStages BETWEEN 1 and 5) group by c.module.moduleId")
	List<Object[]> getAvgOda();

}
