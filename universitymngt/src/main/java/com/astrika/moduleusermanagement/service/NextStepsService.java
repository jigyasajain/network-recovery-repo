package com.astrika.moduleusermanagement.service;

import java.util.List;

import com.astrika.moduleusermanagement.model.NextSteps;
import com.astrika.surveymngt.model.LifeStages;

public interface NextStepsService {
	
	public NextSteps save(NextSteps nextSteps);
	
	public List<NextSteps> findByLifeStageandModuleId(LifeStages lifestages, long moduleId);
	
	public void deleteByModuleId(long moduleId);

}
