package com.astrika.moduleusermanagement.service.impl;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.astrika.common.exception.NoSuchUserException;
import com.astrika.common.model.Role;
import com.astrika.common.model.User;
import com.astrika.common.model.UserStatus;
import com.astrika.common.repository.UserRepository;
import com.astrika.common.service.DocumentService;
import com.astrika.common.service.UserService;
import com.astrika.companymngt.model.company.CompanyMaster;
import com.astrika.companymngt.model.module.ModuleMaster;
import com.astrika.companymngt.repository.ModuleRepository;
import com.astrika.companymngt.service.CompanyService;
import com.astrika.companymngt.service.ModuleService;
import com.astrika.modulemngt.exception.DuplicateModuleException;
import com.astrika.moduleusermanagement.exception.FileNotFoundException;
import com.astrika.moduleusermanagement.model.ModuleUser;
import com.astrika.moduleusermanagement.repository.ModuleUserRepository;
import com.astrika.moduleusermanagement.service.ModuleUserService;

@Service
public class ModuleUserServiceImpl implements ModuleUserService {

	@Autowired
	private ModuleUserRepository moduleUserRepository;

	@Autowired
	private ModuleRepository moduleRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ModuleService moduleService;

	@Autowired
	private CompanyService companyService;

	@Autowired
	private UserService userService;

	@Autowired
	private DocumentService documentService;
	
	private static final String SURVEY_NOT_COMPLETED_YET="Survey Not completed yet";

	@Override
	public ModuleUser findByModuleuserId(long moduleuserId) {
		return moduleUserRepository.findByModuleuserId(moduleuserId);
	}

	@Override
	public ModuleUser findByModuleId(long moduleId) {
		return moduleUserRepository.findByModuleId(moduleId);
	}

	@Override
	public List<ModuleUser> findByUserId(long userId) {
		return moduleUserRepository.findByUserId(userId);
	}

	@Override
	public List<ModuleMaster> findByActive(boolean active) {

		return moduleUserRepository
				.findByActive(active);
	}

	@Override
	public List<User> findByRoleAndStatus(Role role, UserStatus status) {
		return moduleUserRepository.findByRoleAndStatus(role,
				status);
	}

	@Override
	public ModuleUser save(ModuleUser moduleUser)
			throws NoSuchUserException {
		ModuleUser moduleUser1 = findByCompanyIdAndModuleId(moduleUser
				.getCompany().getCompanyId(), moduleUser.getModule()
				.getModuleId());
		if (moduleUser1 != null) {

			moduleUser1.setUser(moduleUser.getUser());
			return moduleUserRepository.save(moduleUser1);
		}

		return moduleUserRepository.save(moduleUser);
	}

	@Override
	public List<ModuleUser> findByCompanyCompanyId(long companyId) {
		return moduleUserRepository.findByCompanyCompanyId(companyId);
	}

	@Override
	public ModuleUser findByCompanyIdAndModuleId(Long companyId, long moduleId) {

		return moduleUserRepository.findByCompanyIdAndModuleId(companyId,
				moduleId);
	}

	@Override
	public ModuleUser findByUserIdandModuleId(long userId, long moduleId) {
		return moduleUserRepository.findByUserIdandModuleId(userId, moduleId);
	}

	@Override
	public ModuleUser saveSurveyResult(ModuleUser surveyResult) throws DuplicateModuleException{
		return moduleUserRepository.save(surveyResult);
	}

	@Override
	public ModuleUser update(ModuleUser moduleUser) {
		return moduleUserRepository.save(moduleUser);
	}

	@Override
	public List<ModuleMaster> findByModuleModuleId(long moduleId) {
		return Collections.emptyList();
	}

	@Override
	public void createLSSXlSheet(CompanyMaster company)
			throws IOException, FileNotFoundException{
		Workbook wb = new XSSFWorkbook();
		CreationHelper createHelper = wb.getCreationHelper();
		Sheet sheet = wb.createSheet("Report");

		int rowIndex = 0;

		Row headerRow = sheet.createRow((short) rowIndex++);
		headerRow.createCell(0).setCellValue(
				createHelper.createRichTextString("Organisation Name"));
		headerRow.createCell(1).setCellValue(
				createHelper.createRichTextString("Survey Start Date"));
		headerRow.createCell(2).setCellValue(
				createHelper.createRichTextString("Last Date to complete"));
		headerRow.createCell(3).setCellValue(
				createHelper.createRichTextString("Actual Completion Date"));
		headerRow.createCell(4).setCellValue(
				createHelper.createRichTextString("Organisational Development Area"));
		headerRow.createCell(5).setCellValue(
				createHelper.createRichTextString("Life Stage"));
		headerRow.createCell(6).setCellValue(
				createHelper.createRichTextString("LSS Total"));


		List<ModuleUser> list = findByCompanyCompanyId(company.getCompanyId());

		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");				
		Date date1 = list.get(0).getCreatedOn().toDate();
		String	startDate = formatter.format(date1);
		DateTime date2;
		DateTime date3;
		if(list.get(0).getCreatedOn().getMonthOfYear()==12){
			date2 =  list.get(0).getCreatedOn().plusYears(1);
			date3 = date2.plusMonths(1);
		}
		else{
			date3 = list.get(0).getCreatedOn().plusMonths(1);
		}
		int count = rowIndex;
		Row row1 = sheet.createRow(rowIndex);

		row1.createCell(0).setCellValue(createHelper.createRichTextString(company.getCompanyName()));
		row1.createCell(1).setCellValue(createHelper.createRichTextString(startDate));

		Date changedDate = date3.toDate();
		String	completiondate = formatter.format(changedDate);		
		row1.createCell(2).setCellValue(createHelper.createRichTextString(completiondate));

		java.util.Iterator<ModuleUser> it = list.iterator();
		while(it.hasNext()){

			ModuleUser moduleUser = it.next();

			Date date4 = moduleUser.getLastModifiedOn().toDate();
			String	actualCompletiondate = formatter.format(date4);
			if(count==rowIndex){
				row1.createCell(4).setCellValue(createHelper.createRichTextString(moduleUser.getModule().getModuleName()));
				if(moduleUser.getLifeStages()!=null){
					row1.createCell(3).setCellValue(createHelper.createRichTextString(actualCompletiondate));
					row1.createCell(5).setCellValue(createHelper.createRichTextString(moduleUser.getLifeStages().getName()));
				}
				else{
					row1.createCell(3).setCellValue(createHelper.createRichTextString(""));
					row1.createCell(5).setCellValue(createHelper.createRichTextString(SURVEY_NOT_COMPLETED_YET));
				}
				if(moduleUser.getTotalOflifestage() == 0){
					row1.createCell(6).setCellValue(createHelper.createRichTextString(SURVEY_NOT_COMPLETED_YET));
				}
				else{
					row1.createCell(6).setCellValue(createHelper.createRichTextString(Integer.toString(moduleUser.getTotalOflifestage())));
				}
				rowIndex++;
			}
			else{
				Row row = sheet.createRow((short) rowIndex++);

				row.createCell(4).setCellValue(createHelper.createRichTextString(moduleUser.getModule().getModuleName()));
				if(moduleUser.getLifeStages()!=null){
					row.createCell(3).setCellValue(createHelper.createRichTextString(actualCompletiondate));
					row.createCell(5).setCellValue(createHelper.createRichTextString(moduleUser.getLifeStages().getName()));
				}
				else{
					row.createCell(3).setCellValue(createHelper.createRichTextString(""));
					row.createCell(5).setCellValue(createHelper.createRichTextString(SURVEY_NOT_COMPLETED_YET));
				}
				if(moduleUser.getTotalOflifestage() == 0){
					row.createCell(6).setCellValue(createHelper.createRichTextString(SURVEY_NOT_COMPLETED_YET));
				}
				else{
					row.createCell(6).setCellValue(createHelper.createRichTextString(Integer.toString(moduleUser.getTotalOflifestage())));
				}
			}

		}

		String absolutePath = documentService
				.getAbsoluteSaveLocationDir(company.getCompanyName());
		FileOutputStream fileOut = new FileOutputStream(absolutePath + company.getCompanyName() +"_LSSRating.csv");
		wb.write(fileOut);
		fileOut.close();

	}

	@Override
	public int totalUsersperModule(long moduleId) {
		return moduleUserRepository.totalUsersperModule(moduleId);
	}

	@Override
	public List<Object[]> getAllByModuleIdAndRating() {
		return moduleUserRepository.getAllByModuleIdAndRating();
	}

	@Override
	public List<Object[]> getAllByModuleIdAndRatingByMonthYear(int mnth, int yr) {
		return moduleUserRepository.getAllByModuleIdAndRatingByMonthYear(mnth,yr);
	}

	@Override
	public int getmaxRating() {
		return moduleUserRepository.getmaxRating();
	}

	@Override
	public List<Object[]> getAvgOda() {
		return moduleUserRepository.getAvgOda();
	}

}
