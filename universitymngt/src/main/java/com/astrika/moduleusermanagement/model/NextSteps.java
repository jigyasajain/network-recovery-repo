package com.astrika.moduleusermanagement.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.envers.Audited;

import com.astrika.categorymngt.model.CategoryMaster;
import com.astrika.companymngt.model.module.ModuleMaster;
import com.astrika.surveymngt.model.LifeStages;

@Entity
@Audited
public class NextSteps {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long nextStepsId;

	@OneToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	private ModuleMaster module;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	private CategoryMaster category;
	
	@Column
	private String step;
	
	@Enumerated(EnumType.ORDINAL)
	private LifeStages lifestages;
	
	public NextSteps() {
		super();
	}
	
	public NextSteps(ModuleMaster module, CategoryMaster category, String step,
			LifeStages lifestages) {
		super();
		this.module = module;
		this.category = category;
		this.step = step;
		this.lifestages = lifestages;
	}



	public Long getNextStepsId() {
		return nextStepsId;
	}

	public void setNextStepsId(Long nextStepsId) {
		this.nextStepsId = nextStepsId;
	}

	public ModuleMaster getModule() {
		return module;
	}

	public void setModule(ModuleMaster module) {
		this.module = module;
	}

	public CategoryMaster getCategory() {
		return category;
	}

	public void setCategory(CategoryMaster category) {
		this.category = category;
	}

	public String getStep() {
		return step;
	}

	public void setStep(String step) {
		this.step = step;
	}

	public LifeStages getLifestages() {
		return lifestages;
	}

	public void setLifestages(LifeStages lifestages) {
		this.lifestages = lifestages;
	}

	
	
}
