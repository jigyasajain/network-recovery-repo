package com.astrika.moduleusermanagement.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.envers.Audited;
import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import com.astrika.common.model.User;
import com.astrika.companymngt.model.company.CompanyMaster;
import com.astrika.companymngt.model.module.ModuleMaster;
import com.astrika.surveymngt.model.LifeStages;





@Entity
@Audited
public class ModuleUser {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long moduleuserId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	private ModuleMaster module;
	
	@ManyToOne(fetch = FetchType.LAZY)
	private CompanyMaster company;
	
	@JsonIgnore
	@OneToOne(fetch = FetchType.LAZY)
	private User user;
	
	
	@CreatedDate
	private DateTime createdOn;

	@LastModifiedDate
	private DateTime lastModifiedOn;

	@JsonIgnore
	@CreatedBy
	@ManyToOne(fetch = FetchType.LAZY)
	private User createdBy;

	@JsonIgnore
	@LastModifiedBy
	@ManyToOne(fetch = FetchType.LAZY)
	private User lastModifiedBy;
	
	@Enumerated(EnumType.ORDINAL)
	private LifeStages lifeStages;
	
	@Column
	private int totalOflifestage = 0;
	
	@Column
	private int totalSurveyWeight = 0;
	
	@Column
	private int surveyWeight = 0;
	
	@Column
	private int surveyWeightPercent = 0;
	
	@Column
	private boolean moduleStarted = false;
	
	@Column
	private int projectsAccessed = 0;
	
	@Column(columnDefinition = "boolean default true", nullable = false)
	private boolean active = true;
	
	public ModuleUser(ModuleMaster module,ModuleMaster module1,User user){
		super();
		this.module=module;
		this.module=module1;
		this.user=user;
	}

	
	public ModuleUser() {
		super();
	}


	public long getModuleuserId() {
		return moduleuserId;
	}

	public void setModuleuserId(long moduleuserId) {
		this.moduleuserId = moduleuserId;
	}

	public ModuleMaster getModule() {
		return module;
	}

	public void setModule(ModuleMaster module) {
		this.module = module;
	}

	public CompanyMaster getCompany() {
		return company;
	}

	public void setCompany(CompanyMaster company) {
		this.company = company;
	}

	public User getAdmin() {
		return user;
	}

	public void setAdmin(User user) {
		this.user = user;
	}

	public DateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(DateTime createdOn) {
		this.createdOn = createdOn;
	}

	public DateTime getLastModifiedOn() {
		return lastModifiedOn;
	}

	public void setLastModifiedOn(DateTime lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public User getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(User lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}
	
	public LifeStages getLifeStages() {
		return lifeStages;
	}


	public void setLifeStages(LifeStages lifeStages) {
		this.lifeStages = lifeStages;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((module == null) ? 0 : module.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ModuleUser other = (ModuleUser) obj;
		if (module == null) {
			if (other.module != null)
				return false;
		} else if (!module.equals(other.module))
			return false;
		return true;
	}


	public int getTotalOflifestage() {
		return totalOflifestage;
	}


	public void setTotalOflifestage(int totalOflifestage) {
		this.totalOflifestage = totalOflifestage;
	}


	public boolean isActive() {
		return active;
	}


	public void setActive(boolean active) {
		this.active = active;
	}


	public int getTotalSurveyWeight() {
		return totalSurveyWeight;
	}


	public void setTotalSurveyWeight(int totalSurveyWeight) {
		this.totalSurveyWeight = totalSurveyWeight;
	}


	public int getSurveyWeight() {
		return surveyWeight;
	}


	public void setSurveyWeight(int surveyWeight) {
		this.surveyWeight = surveyWeight;
	}


	public int getSurveyWeightPercent() {
		return surveyWeightPercent;
	}


	public void setSurveyWeightPercent(int surveyWeightPercent) {
		this.surveyWeightPercent = surveyWeightPercent;
	}


	public boolean isModuleStarted() {
		return moduleStarted;
	}


	public void setModuleStarted(boolean moduleStarted) {
		this.moduleStarted = moduleStarted;
	}


	public int getProjectsAccessed() {
		return projectsAccessed;
	}


	public void setProjectsAccessed(int projectsAccessed) {
		this.projectsAccessed = projectsAccessed;
	}
	
	
	
}
