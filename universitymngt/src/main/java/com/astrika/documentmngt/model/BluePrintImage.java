package com.astrika.documentmngt.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.envers.Audited;
import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import com.astrika.common.model.ImageMaster;
import com.astrika.common.model.User;


@Entity
@Audited
public class BluePrintImage {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long blueprintImageId;
	
	@ManyToOne(optional = true, fetch=FetchType.LAZY)
	@JsonIgnore
	private AtmaDocumentMaster document;
	
	@ManyToOne
	private ImageMaster image;
	
	@Column(columnDefinition = "boolean default true", nullable = false)
	private boolean active = true;

	@CreatedDate
	private DateTime createdOn;

	@LastModifiedDate
	private DateTime lastModifiedOn;

	@JsonIgnore
	@CreatedBy
	@ManyToOne(fetch= FetchType.LAZY)
	private User createdBy;

	@JsonIgnore
	@LastModifiedBy
	@ManyToOne(fetch=FetchType.LAZY)
	private User lastModifiedBy;

	public BluePrintImage() {
		super();
	}	
	
	public BluePrintImage(AtmaDocumentMaster document, ImageMaster image) {
		super();
		this.document = document;
		this.image = image;
	}



	public Long getBlueprintImageId() {
		return blueprintImageId;
	}

	public void setBlueprintImageId(Long blueprintImageId) {
		this.blueprintImageId = blueprintImageId;
	}

	public AtmaDocumentMaster getDocument() {
		return document;
	}

	public void setDocument(AtmaDocumentMaster document) {
		this.document = document;
	}

	public ImageMaster getImage() {
		return image;
	}

	public void setImage(ImageMaster image) {
		this.image = image;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public DateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(DateTime createdOn) {
		this.createdOn = createdOn;
	}

	public DateTime getLastModifiedOn() {
		return lastModifiedOn;
	}

	public void setLastModifiedOn(DateTime lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public User getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(User lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	
	
}
