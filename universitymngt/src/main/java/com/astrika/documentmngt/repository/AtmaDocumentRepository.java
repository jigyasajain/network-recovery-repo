package com.astrika.documentmngt.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.astrika.documentmngt.model.AtmaDocumentMaster;
import com.astrika.surveymngt.model.LifeStages;

public interface AtmaDocumentRepository extends JpaRepository<AtmaDocumentMaster, Long> {
	
	
	@Query("select d from AtmaDocumentMaster d left join fetch d.createdBy left join fetch d.module left join fetch d.category where d.active = ?1 Order By d.documentName ASC")
	List<AtmaDocumentMaster> findAllByActive(boolean active);

	@Query("select d from AtmaDocumentMaster d left join fetch d.blueprintImages left join fetch d.module left join fetch d.category where d.documentId = ?1")
	AtmaDocumentMaster findByDocumentId(long documentId);
	
	@Query("select d from AtmaDocumentMaster d where d.documentName = ?1 and d.module.moduleId = ?2 and d.category.categoryId = ?3 and d.section = ?4 and d.contentType = ?5 and d.lifeStages = ?6")
	AtmaDocumentMaster findByDocumentNameandModuleIdandCategoryIdandSectionandContentTypeandLifeStage(String documentName, long moduleId, long categoryId, String section, String contentType, LifeStages lifeStage);
	
	@Query("select d from AtmaDocumentMaster d where d.link = ?1 and d.module.moduleId = ?2 and d.category.categoryId = ?3 and d.section = ?4 and d.contentType = ?5")
	AtmaDocumentMaster findByLinkandModuleIdandCategoryIdandSectionandContentType(String link, long moduleId, long categoryId, String section, String contentType);
	
	@Query("select d from AtmaDocumentMaster d where d.module.moduleId = ?1 and d.category.categoryId = ?2 and d.section = ?3 and d.active = ?4")
	List<AtmaDocumentMaster> findByModuleIdandCategoryIdandSectionandActive(long moduleId, long categoryId, String section, boolean active);
	
	@Query("select d from AtmaDocumentMaster d left join fetch d.blueprintImages where d.module.moduleId = ?1 and d.category.categoryId = ?2 and d.section = ?3 and d.active = ?4")
	AtmaDocumentMaster findByModuleandCategoryandSectionandActive(long moduleId, long categoryId, String section, boolean active);
	
	@Query("select d from AtmaDocumentMaster d where d.module.moduleId = ?1 and d.category.categoryId = ?2 and d.section = ?3 and d.contentType = ?4")
	AtmaDocumentMaster findByModuleIdandCategoryIdandSectionandContentType(long moduleId, long categoryId, String section, String contentType);
	
	@Query("select d from AtmaDocumentMaster d where d.module.moduleId = ?1 and d.active=?2")
	List<AtmaDocumentMaster> findByModuleIdandActive(long moduleId, boolean active);
	
	@Query("select sum(noOfDownloads) from AtmaDocumentMaster where lastModifiedBy.userId not in(24,97)")
	int totalSumOfDownloads();
	
	@Query("select sum(noOfDownloads) from AtmaDocumentMaster d where d.module.moduleId=?1")
	int totalDownloadsperModule(long moduleId);
	
	@Query("select d from AtmaDocumentMaster d where d.noOfDownloads = (select max(d.noOfDownloads) from AtmaDocumentMaster d where d.module.moduleId = ?1 and d.active = 1) and d.active = 1 and d.module.moduleId = ?1 group by d.title")
	List<AtmaDocumentMaster> findTopDownload(long moduleId);
	
	@Query(value = "select d from AtmaDocumentMaster d where d.active = 1", countQuery= "select count(d) from AtmaDocumentMaster d where d.active = 1")
	Page<AtmaDocumentMaster> findTopfiveDownloads(Pageable page);

	@Query("select d.module.moduleName,sum(d.noOfDownloads) from AtmaDocumentMaster d group by d.module.moduleId")
	List<Object[]> getDownloadPerModule();

	@Query("select count(d) from AtmaDocumentMaster d where d.lastModifiedBy.userId not in(24,97) and d.flag=1")
	int getlastDownloadCount();

	@Query("select d.documentId,d.lastModifiedOn,d.lastModifiedBy.firstName,d.lastModifiedBy.lastName,d.documentName,d.flag from AtmaDocumentMaster d where DATEDIFF(now(),d.lastModifiedOn)<7 and d.lastModifiedBy.userId not in(24,97)")
	List<Object[]> getLastDownload();

	@Query("select d from AtmaDocumentMaster d where d.flag=1")
	List<AtmaDocumentMaster> getInactiveFlagLastDownloads();

	
}
 