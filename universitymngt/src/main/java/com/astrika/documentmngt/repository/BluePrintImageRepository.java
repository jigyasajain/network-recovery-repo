package com.astrika.documentmngt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.astrika.documentmngt.model.BluePrintImage;

public interface BluePrintImageRepository extends JpaRepository<BluePrintImage, Long> {
	
	@Query("select b from BluePrintImage b left join fetch b.document where b.blueprintImageId = ?1")
	BluePrintImage findByImageImageId(long imageId);

}
