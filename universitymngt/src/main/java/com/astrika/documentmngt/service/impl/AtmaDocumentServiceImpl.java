package com.astrika.documentmngt.service.impl;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.astrika.categorymngt.model.CategoryMaster;
import com.astrika.categorymngt.service.CategoryService;
import com.astrika.common.exception.NoSuchImageException;
import com.astrika.common.model.ImageMaster;
import com.astrika.common.model.User;
import com.astrika.common.service.DocumentService;
import com.astrika.common.service.ImageService;
import com.astrika.common.util.CommonUtil;
import com.astrika.common.util.PropsValues;
import com.astrika.companymngt.model.module.ModuleMaster;
import com.astrika.companymngt.service.ModuleService;
import com.astrika.documentmngt.exception.DocumentPresentException;
import com.astrika.documentmngt.exception.DuplicateDocumentException;
import com.astrika.documentmngt.exception.DuplicateLinkException;
import com.astrika.documentmngt.model.AtmaDocumentMaster;
import com.astrika.documentmngt.model.AtmaDocumentMaster_aud;
import com.astrika.documentmngt.model.BluePrintImage;
import com.astrika.documentmngt.repository.AtmaDocumentRepository;
import com.astrika.documentmngt.repository.AtmaDocumentmngt_audRepository;
import com.astrika.documentmngt.repository.BluePrintImageRepository;
import com.astrika.documentmngt.service.AtmaDocumentService;
import com.astrika.kernel.exception.CustomException;
import com.astrika.surveymngt.model.LifeStages;


@Service
@Transactional
public class AtmaDocumentServiceImpl implements AtmaDocumentService {

	
	@Autowired
	private AtmaDocumentRepository documentRepository;
	
	@Autowired
	private AtmaDocumentmngt_audRepository documentAudRepository;
	
	@Autowired
	private ModuleService moduleService;
	
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private BluePrintImageRepository bluePrintImageRepository;
	
	@Autowired
	private ImageService imageService;
	
	@Autowired
	private DocumentService documentService;
	
	@Autowired
	private PropsValues propsValue;

	@Autowired
	private CommonUtil commonUtil;
	
	@Override
	public AtmaDocumentMaster save(Long moduleId, Long categoryId, String section,String documentName,
			String documenturlpath, String contentType, String link, String documenttype, String title, Long sizeofFile,
			String documentDescription, LifeStages lifeStage) throws DuplicateDocumentException{
		
	    
		String doctype =documenttype;
		if(documentName!= ""){
			AtmaDocumentMaster documentMaster = findByDocumentNameandModuleIdandCategoryIdandSectionandContentTypeandLifeStage(documentName, moduleId, categoryId, section, contentType, lifeStage);
			if("BluePrint".equals(section)){
				AtmaDocumentMaster documentMaster1 = findByModuleIdandCategoryIdandSectionandContentType(moduleId, categoryId, section, contentType);
				if(documentMaster1!=null){
				    ckeck(documentMaster1,documentName);
					
				}
			}
			if(documentMaster!=null && documentMaster.getDocumentName().equals(documentName)){
				
				throw new DuplicateDocumentException(CustomException.DUPLICATE_DOCUMENT.getCode());
			}
		}
		else{
			
				AtmaDocumentMaster documentMaster = findByLinkandModuleIdandCategoryIdandSectionandContentType(
						link, moduleId, categoryId, section, contentType);
				if (documentMaster != null && documentMaster.getLink().equals(link)) {
						throw new DuplicateLinkException(
								CustomException.DUPLICATE_LINK.getCode());
				}
	
				
		}
		
		ModuleMaster module = moduleService.findByModuleId(moduleId);
		CategoryMaster category = categoryService.findByCategoryId(categoryId);
		AtmaDocumentMaster documentMaster1 = new AtmaDocumentMaster();
		documentMaster1.setModule(module);
		documentMaster1.setCategory(category);
		documentMaster1.setSection(section);
		documentMaster1.setDocumentName(documentName);
		documentMaster1.setDocumentDescription(documentDescription);
		documentMaster1.setDocumenturlpath(documenturlpath);
		documentMaster1.setContentType(contentType);
		documentMaster1.setLink(link);
		if((doctype.isEmpty()) && (link!=null)){
			doctype = "link";
		}
		documentMaster1.setDoctype(doctype);
		documentMaster1.setLifeStages(lifeStage);
		documentMaster1.setTitle(title);
		documentMaster1.setSizeofFile(sizeofFile);
		return documentRepository.save(documentMaster1);
		
	}

	private void ckeck(AtmaDocumentMaster documentMaster1, String documentName) {

	    if(documentMaster1.getDocumentName().equals(documentName)){
            throw new DuplicateDocumentException(CustomException.DUPLICATE_DOCUMENT.getCode());
        }
        else{
            throw new DocumentPresentException(CustomException.DOCUMENT_ALREADY_PRESENT.getCode());
        }
    }

    @Override
	public AtmaDocumentMaster update(Long documentId, Long moduleId,
			Long categoryId, String section,String documentName, String documenturlpath, String contentType, String link, String doctype, String title, Long sizeofFile, String documentDescription, LifeStages lifeStage) throws DuplicateDocumentException {
		if("Document".equalsIgnoreCase(contentType)){
			AtmaDocumentMaster documentMaster = findByDocumentNameandModuleIdandCategoryIdandSectionandContentTypeandLifeStage(
					documentName, moduleId, categoryId, section,
					contentType, lifeStage);
			if (documentMaster != null && documentMaster.getDocumentName().equals(documentName)) {
					throw new DuplicateDocumentException(
							CustomException.DUPLICATE_DOCUMENT.getCode());
			}
		}
		else{
			AtmaDocumentMaster documentMaster = findByLinkandModuleIdandCategoryIdandSectionandContentType(
					link, moduleId, categoryId, section, contentType);
			if (documentMaster != null && documentMaster.getLink().equals(link)) {
					throw new DuplicateLinkException(
							CustomException.DUPLICATE_LINK.getCode());
			}
		}
		
		
		
		
		
		
		
		
		AtmaDocumentMaster documentMaster1 = findByDocumentId(documentId);
		ModuleMaster module = moduleService.findByModuleId(moduleId);
		CategoryMaster category = categoryService.findByCategoryId(categoryId);
		documentMaster1.setModule(module);
		documentMaster1.setCategory(category);
		documentMaster1.setSection(section);
		documentMaster1.setDocumentName(documentName);
		documentMaster1.setDocumentDescription(documentDescription);
		documentMaster1.setDocumenturlpath(documenturlpath);
		documentMaster1.setContentType(contentType);
		documentMaster1.setLink(link);
		documentMaster1.setDoctype(doctype);
		documentMaster1.setLifeStages(lifeStage);
		documentMaster1.setTitle(title);
		documentMaster1.setSizeofFile(sizeofFile);
		return documentRepository.save(documentMaster1);
	}

	@Override
	public AtmaDocumentMaster delete(long documentId) {
		AtmaDocumentMaster documentMaster = findByDocumentId(documentId);
		Authentication authentication = commonUtil.getCurrentLoggedInUser();
		if(authentication == null) 
		    throw new IllegalStateException("Logged in user cannot be null");
		documentMaster.setDeActivatedBy((User) authentication.getPrincipal());
		documentMaster.setDeActivatedOn(new DateTime());
		documentMaster.setActive(false);
		return documentRepository.save(documentMaster);
	}

	@Override
	public AtmaDocumentMaster restore(long documentId) {
		AtmaDocumentMaster documentMaster = findByDocumentId(documentId);
		Authentication authentication = commonUtil.getCurrentLoggedInUser();
		if(authentication == null)
		    throw new IllegalStateException("Logged in user cannot be null");
		documentMaster.setReActivatedBy((User) authentication.getPrincipal());
		documentMaster.setReActivatedOn(new DateTime());
		documentMaster.setActive(true);
		return documentRepository.save(documentMaster);
	}

	@Override
	public List<AtmaDocumentMaster> findByActive(boolean active) {
		return documentRepository.findAllByActive(active);
	}

	@Override
	public AtmaDocumentMaster findByDocumentId(long documentId) {
		return documentRepository.findByDocumentId(documentId);
	}

	@Override
	public AtmaDocumentMaster findByDocumentNameandModuleIdandCategoryIdandSectionandContentTypeandLifeStage(
			String documentName, long moduleId, long categoryId, String section, String contentType, LifeStages lifeStage) {
		return documentRepository.findByDocumentNameandModuleIdandCategoryIdandSectionandContentTypeandLifeStage(documentName, moduleId, categoryId, section, contentType, lifeStage);
	}

	@Override
	public AtmaDocumentMaster findByLinkandModuleIdandCategoryIdandSectionandContentType(
			String link, long moduleId, long categoryId, String section,
			String contentType) {
		return documentRepository.findByLinkandModuleIdandCategoryIdandSectionandContentType(link, moduleId, categoryId, section, contentType);
	}

	@Override
	public AtmaDocumentMaster update(Long documentId, Long moduleId,
			Long categoryId, String section,
			String contentType, String title, String documentDescription,String link,
			LifeStages lifeStage){
		
		
		AtmaDocumentMaster documentMaster1 = findByDocumentId(documentId);
		
		ModuleMaster module = moduleService.findByModuleId(moduleId);
		CategoryMaster category = categoryService.findByCategoryId(categoryId);
		documentMaster1.setModule(module);
		documentMaster1.setCategory(category);
		documentMaster1.setSection(section);

		
		
		documentMaster1.setDocumentDescription(documentDescription);
		documentMaster1.setLink(link);
		documentMaster1.setContentType(contentType);
		documentMaster1.setLifeStages(lifeStage);
		documentMaster1.setTitle(title);
		return documentRepository.save(documentMaster1);
	}

	@Override
	public List<AtmaDocumentMaster> findByModuleIdandCategoryIdandSectionandActive(
			long moduleId, long categoryId, String section, boolean active) {
		return documentRepository.findByModuleIdandCategoryIdandSectionandActive(moduleId, categoryId, section, active);
	}

	@Override
	public AtmaDocumentMaster findByModuleandCategoryandSectionandActive(
			long moduleId, long categoryId, String section, boolean active) {
		return documentRepository.findByModuleandCategoryandSectionandActive(moduleId, categoryId, section, active);
	}

	@Override
	public AtmaDocumentMaster saveDocument(AtmaDocumentMaster document,
			List<ImageMaster> bluePrintImages)
			{
		if (bluePrintImages != null && !bluePrintImages.isEmpty()) {
			List<BluePrintImage> restaurantImages = new ArrayList<>();
			for (ImageMaster image : bluePrintImages) {
				BluePrintImage bluePrintImg = new BluePrintImage(document, image);
				restaurantImages.add(bluePrintImg);
			}
			bluePrintImageRepository.save(restaurantImages);
		}
		return document;
	}

	@Override
	public void deleteBlueprintImage(long imageId) throws NoSuchImageException, UnsupportedEncodingException {
		BluePrintImage blueprintImage = bluePrintImageRepository.findByImageImageId(imageId);
		bluePrintImageRepository.delete(blueprintImage.getBlueprintImageId());
		imageService.delete(blueprintImage.getImage().getImageId());
	}

	@Override
	public AtmaDocumentMaster findByModuleIdandCategoryIdandSectionandContentType(
			long moduleId, long categoryId,
			String section, String contentType) {
		return documentRepository.findByModuleIdandCategoryIdandSectionandContentType(moduleId, categoryId, section, contentType);
	}

	@Override
	public AtmaDocumentMaster update(AtmaDocumentMaster document) {		
			return documentRepository.save(document);		
	}

	@Override
	public AtmaDocumentMaster_aud updateDocument(AtmaDocumentMaster_aud document) {		
			return documentAudRepository.save(document);		
	}
	
	@Override
	public void updateActiveAuditDownload() {		
		 documentAudRepository.updateActiveAuditDownload();		
	}
	
	
	
	@Override
	public int totalSumofDownloads() {
		List<AtmaDocumentMaster> documentList = findByActive(true);
		if(documentList.isEmpty()){
			return 0;
		}
		else{
			return documentRepository.totalSumOfDownloads();
		}
	}
	

	@Override
	public int getLastMonthDownloads() {
		return documentAudRepository.getLastMonthDownloads();
	}

	@Override
	public int getsecondLastMonthDownloads() {
		return documentAudRepository.getsecondLastMonthDownloads();
	}
	
	@Override
	public int getlastDownloadCount() {
		return documentRepository.getlastDownloadCount();
	}
	
	@Override
	public List<Object[]> getLastDownload() {
	
	//	List<Object[]> l= documentRepository.getLastDownload();
		List<Object[]> l= documentAudRepository.getLastDownload();
		
		if(!l.isEmpty()){
			return l;
		}else{
			return Collections.emptyList();
		}
		
	}
	
	
	
	@Override
	public int totalDownloadsperModule(long moduleId) {
		List<AtmaDocumentMaster> documentList = documentRepository.findByModuleIdandActive(moduleId, true);
		if(documentList.isEmpty()){
			return 0;
		}
		else{
			return documentRepository.totalDownloadsperModule(moduleId);
		}
	}


	@Override
	public List<AtmaDocumentMaster> findTopfiveDownloads(int start, int end) {
		Page<AtmaDocumentMaster> fiveDownloads =  documentRepository.findTopfiveDownloads(new PageRequest(start,end,Direction.DESC,"noOfDownloads"));
		return fiveDownloads.getContent();
	}

	
	@Override
	public List<AtmaDocumentMaster> getInactiveFlagLastDownloads() {
		return documentRepository.getInactiveFlagLastDownloads();
	}
	
	@Override
	public List<AtmaDocumentMaster_aud> getInactiveFlagLastAuditDownloads() {
		return documentAudRepository.getInactiveFlagLastAuditDownloads();
	}
	
	@Override
	public String validateFile(MultipartFile file) throws IOException {

		String agreementFileUlr = "";	

		if (file != null && file.getOriginalFilename() != null
				&& file.getOriginalFilename().length() > 0) {
			String 	filename = file.getOriginalFilename();
			String ext = filename.substring(filename.lastIndexOf("."));
			byte[] byteArray = file.getBytes();
			String absolutePath = documentService
					.getAbsoluteSaveLocationDir(1);

			File uploadedFile = documentService.save(byteArray, ext,
					filename, absolutePath);
			agreementFileUlr = uploadedFile.getAbsolutePath();
			agreementFileUlr = agreementFileUlr
					.split(propsValue.documentsRoot)[1];
		}

		return agreementFileUlr;
	}

	@Override
	public AtmaDocumentMaster findTopDownload(long moduleId) {
		List<AtmaDocumentMaster> documentList =  documentRepository.findTopDownload(moduleId);
		
		if(!documentList.isEmpty()){
			return documentList.get(0);
		}
		else{
			AtmaDocumentMaster document = new AtmaDocumentMaster();
			document.setTitle("");
			return document;
		}
		
	}
	
	@Override
	public List<Object[]> getDownloadPerModule() {
		return documentRepository.getDownloadPerModule();
	}
	
	
}
