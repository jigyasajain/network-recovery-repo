package com.astrika.documentmngt.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.astrika.common.model.ImageMaster;
import com.astrika.documentmngt.exception.DuplicateDocumentException;
import com.astrika.documentmngt.model.AtmaDocumentMaster;
import com.astrika.documentmngt.model.AtmaDocumentMaster_aud;
import com.astrika.surveymngt.model.LifeStages;

public interface AtmaDocumentService {

	public AtmaDocumentMaster save(Long moduleId, Long categoryId, String section,String documentName, String documenturlpath, String contentType, String link, String doctype, String title, Long sizeofFile, String documentDescription, LifeStages lifeStage) throws DuplicateDocumentException;
	
	public AtmaDocumentMaster update(Long documentId, Long moduleId, Long categoryId, String section,String documentName, String documenturlpath, String contentType, String link, String doctype, String title, Long sizeofFile, String documentDescription, LifeStages lifeStage) throws DuplicateDocumentException;
	
	public AtmaDocumentMaster delete(long documentId);
	
	public AtmaDocumentMaster restore(long documentId);
	
	List<AtmaDocumentMaster> findByActive(boolean active);
	
	public AtmaDocumentMaster findByDocumentId(long documentId);
	
	public AtmaDocumentMaster findByDocumentNameandModuleIdandCategoryIdandSectionandContentTypeandLifeStage(String documentName, long moduleId, long categoryId, String section,String contentType, LifeStages lifeStage);
	
	public AtmaDocumentMaster findByLinkandModuleIdandCategoryIdandSectionandContentType(String link, long moduleId, long categoryId, String section,String contentType);
	
	public AtmaDocumentMaster update(Long documentId, Long moduleId, Long categoryId, String section, String contentType, String title,String documentDescription, String link,LifeStages lifeStage);

	public List<AtmaDocumentMaster> findByModuleIdandCategoryIdandSectionandActive(long moduleId, long categoryId, String section, boolean active);
	
	public AtmaDocumentMaster findByModuleandCategoryandSectionandActive(long moduleId, long categoryId, String section, boolean active);
	
	public AtmaDocumentMaster saveDocument(AtmaDocumentMaster document, List<ImageMaster> bluePrintImages);
	
	public void deleteBlueprintImage(long imageId) throws UnsupportedEncodingException;
	
	public AtmaDocumentMaster findByModuleIdandCategoryIdandSectionandContentType(long moduleId, long categoryId, String section,String contentType);
	
	public int totalSumofDownloads();
    	
	public int totalDownloadsperModule(long moduleId);
	
	public List<AtmaDocumentMaster> findTopfiveDownloads(int start, int end);
	
	public String validateFile(MultipartFile file) throws IOException;
	
	public AtmaDocumentMaster findTopDownload(long moduleId);

	public int getLastMonthDownloads();

	public List<Object[]> getDownloadPerModule();

	public int getsecondLastMonthDownloads();

	public int getlastDownloadCount();

	public List<Object[]> getLastDownload();

	public List<AtmaDocumentMaster> getInactiveFlagLastDownloads();

	public List<AtmaDocumentMaster_aud> getInactiveFlagLastAuditDownloads();

	public AtmaDocumentMaster update(AtmaDocumentMaster document);
	
	public AtmaDocumentMaster_aud updateDocument(AtmaDocumentMaster_aud atmaDocumentMaster);

	public void updateActiveAuditDownload();
	
	

}
