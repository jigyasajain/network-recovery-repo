package com.astrika.registrationmngt.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.envers.Audited;
import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedDate;

import com.astrika.common.model.CompanyApproval;
import com.astrika.common.model.location.CityMaster;
import com.astrika.common.model.location.CountryMaster;
import com.astrika.common.model.location.StateMaster;


@Entity
@Audited
public class EnquiryList {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long enquiryId;
	
	@Column
	private String ngoName;
	
	@Column
	private String sectorWork;
	
	@Column
	private String ngoEmail;
	
	@Column
	private String ngoPhone;
	
	@Column
	private String ngoAddress1;
	
	@Column
	private String ngoAddress2;
	
	@Column
	private String ngoAddress3;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JsonIgnore
	private StateMaster state;
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	private CityMaster city;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	private CountryMaster country;
	
	@Column
	private String ngoPincode;
	
	@Column(nullable=false,length=500)
	private String ngoWebsite;
	
	@Column(nullable=false,length=500)
	private String organisationVision;
	
	@Column(nullable=false,length=500)
	private String organisationMission;
	
	@Column(nullable=false,length=255)
	private String organisationDescription;
	
	@Column
	private boolean assitanceFromOtherAgency;
	
	@Column
	private String legalType;
	
	@Column
	private boolean fcraRegistarion;
	
	@Column
	private String typeOfEducation;
	
	@Column(nullable = false, length = 75)
	private String firstName;

	@Column(length = 75)
	private String lastName;
	
	@Column(unique = true, nullable = false, length = 75)
	private String emailId = null;
	
	@Column(length = 75)
	private String mobile;

	@Enumerated(EnumType.ORDINAL)
	private CompanyApproval approval;
	
	@CreatedDate
	private DateTime createdOn;
	
	@Transient
	private Date createdDate;
	
	@Column(length = 500)
	private String remark;
	
	@Column
	private Date rejectedDate;
	


	public EnquiryList(String firstName, String lastName, String emailId, String mobile,
			String ngoName,String sectorWork,String ngoEmail,String ngoPhone,
			String ngoAddress1,String ngoAddress2,String ngoAddress3,CityMaster ngoCity,StateMaster ngoState,CountryMaster ngoCountry,
			String ngoPincode,String ngoWebsite,String organisationVision,String organisationMission,
			String organisationDescription,boolean assitanceFromOtherAgency,String legalType, boolean fcraRegistarion,
			String typeOfEducation,CompanyApproval approval)
	{
		super();
		this.ngoName=ngoName;
		this.sectorWork=sectorWork;
		this.ngoEmail=ngoEmail;
		this.ngoPhone=ngoPhone;
		this.ngoAddress1=ngoAddress1;
		this.ngoAddress2=ngoAddress2;
		this.ngoAddress3=ngoAddress3;
		this.city = ngoCity;
		this.state = ngoState;
	    this.country = ngoCountry;
		this.ngoPincode=ngoPincode;
		this.ngoWebsite=ngoWebsite;
		this.organisationVision=organisationVision;
		this.organisationMission=organisationMission;
		this.organisationDescription=organisationDescription;
		this.assitanceFromOtherAgency=assitanceFromOtherAgency;
		this.legalType=legalType;
		this.fcraRegistarion=fcraRegistarion;
		this.typeOfEducation=typeOfEducation;
		this.approval=approval;
		this.firstName = firstName;
		this.lastName = lastName;
		this.emailId = emailId;
		this.mobile = mobile;
		
	}

public EnquiryList()
{
	super();
}
	

	public Long getEnquiryId() {
		return enquiryId;
	}



	public void setEnquiryId(Long enquiryId) {
		this.enquiryId = enquiryId;
	}



	public String getNgoName() {
		return ngoName;
	}



	public void setNgoName(String ngoName) {
		this.ngoName = ngoName;
	}



	public String getSectorWork() {
		return sectorWork;
	}



	public void setSectorWork(String sectorWork) {
		this.sectorWork = sectorWork;
	}



	public String getNgoEmail() {
		return ngoEmail;
	}



	public void setNgoEmail(String ngoEmail) {
		this.ngoEmail = ngoEmail;
	}



	public String getNgoPhone() {
		return ngoPhone;
	}



	public void setNgoPhone(String ngoPhone) {
		this.ngoPhone = ngoPhone;
	}



	public String getNgoAddress1() {
		return ngoAddress1;
	}



	public void setNgoAddress1(String ngoAddress1) {
		this.ngoAddress1 = ngoAddress1;
	}



	public String getNgoAddress2() {
		return ngoAddress2;
	}



	public void setNgoAddress2(String ngoAddress2) {
		this.ngoAddress2 = ngoAddress2;
	}



	public String getNgoAddress3() {
		return ngoAddress3;
	}



	public void setNgoAddress3(String ngoAddress3) {
		this.ngoAddress3 = ngoAddress3;
	}



	public StateMaster getState() {
		return state;
	}



	public void setState(StateMaster state) {
		this.state = state;
	}



	public CityMaster getCity() {
		return city;
	}



	public void setCity(CityMaster city) {
		this.city = city;
	}



	public CountryMaster getCountry() {
		return country;
	}



	public void setCountry(CountryMaster country) {
		this.country = country;
	}



	public String getNgoPincode() {
		return ngoPincode;
	}



	public void setNgoPincode(String ngoPincode) {
		this.ngoPincode = ngoPincode;
	}



	public String getNgoWebsite() {
		return ngoWebsite;
	}



	public void setNgoWebsite(String ngoWebsite) {
		this.ngoWebsite = ngoWebsite;
	}



	public String getOrganisationVision() {
		return organisationVision;
	}



	public void setOrganisationVision(String organisationVision) {
		this.organisationVision = organisationVision;
	}


	public String getOrganisationMission() {
		return organisationMission;
	}


	public void setOrganisationMission(String organisationMission) {
		this.organisationMission = organisationMission;
	}


	public String getOrganisationDescription() {
		return organisationDescription;
	}


	public void setOrganisationDescription(String organisationDescription) {
		this.organisationDescription = organisationDescription;
	}


	public boolean isAssitanceFromOtherAgency() {
		return assitanceFromOtherAgency;
	}


	public void setAssitanceFromOtherAgency(boolean assitanceFromOtherAgency) {
		this.assitanceFromOtherAgency = assitanceFromOtherAgency;
	}

	public String getLegalType() {
		return legalType;
	}

	public void setLegalType(String legalType) {
		this.legalType = legalType;
	}


	public boolean isFcraRegistarion() {
		return fcraRegistarion;
	}


	public void setFcraRegistarion(boolean fcraRegistarion) {
		this.fcraRegistarion = fcraRegistarion;
	}


	public String getTypeOfEducation() {
		return typeOfEducation;
	}


	public void setTypeOfEducation(String typeOfEducation) {
		this.typeOfEducation = typeOfEducation;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public DateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(DateTime createdOn) {
		this.createdOn = createdOn;
	}

	public Date getCreatedDate() {
		return createdOn.toDate();
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public CompanyApproval getApproval() {
		return approval;
	}

	public void setApproval(CompanyApproval approval) {
		this.approval = approval;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Date getRejectedDate() {
		return rejectedDate;
	}

	public void setRejectedDate(Date rejectedDate) {
		this.rejectedDate = rejectedDate;
	}
	
	
}
