package com.astrika.registrationmngt.exception;

import com.astrika.kernel.exception.BusinessException;
import com.astrika.kernel.exception.CustomException;

public class DuplicateNGOEmailException extends BusinessException{

	public DuplicateNGOEmailException() {
		super(CustomException.DUPLICATE_NGO_EMAIL.getCode());
	}

	public DuplicateNGOEmailException(String errorCode) {
		super(errorCode);
	}
}
