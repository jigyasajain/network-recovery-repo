package com.astrika.registrationmngt.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.astrika.common.exception.DuplicateEmailException;
import com.astrika.common.exception.NoSuchUserException;
import com.astrika.common.model.CompanyApproval;
import com.astrika.common.model.User;
import com.astrika.common.model.mail.EmailData;
import com.astrika.common.service.SendMailService;
import com.astrika.common.service.UserService;
import com.astrika.common.util.PropsValues;
import com.astrika.companymngt.exception.DuplicateCompanyException;
import com.astrika.companymngt.model.company.CompanyMaster;
import com.astrika.companymngt.service.CompanyService;
import com.astrika.kernel.exception.CustomException;
import com.astrika.registrationmngt.exception.DuplicateNGOEmailException;
import com.astrika.registrationmngt.model.EnquiryList;
import com.astrika.registrationmngt.repository.EnquiryRepository;
import com.astrika.registrationmngt.service.EnquiryService;

@Service
public class EnquiryServiceImpl implements EnquiryService {

	@Autowired
	private EnquiryRepository enquiryRepository;

	@Autowired
	private PropsValues props;

	@Autowired
	private SendMailService mailService;

	@Autowired
	private CompanyService companyService;

	@Autowired
	private UserService userService;
	
	private static final Logger LOGGER = Logger.getLogger(EnquiryServiceImpl.class);

	@Override
	public EnquiryList save(EnquiryList enquiry)
			throws DuplicateEmailException, NoSuchUserException,
			DuplicateCompanyException, DuplicateNGOEmailException {
		CompanyMaster company = companyService.findByCompanyEmail(enquiry.getNgoEmail());
		EnquiryList e1 = findByNgoEmail(enquiry.getNgoEmail());
		if (company!= null || e1 != null) {

			throw new DuplicateNGOEmailException(
					CustomException.DUPLICATE_NGO_EMAIL.getCode());
		}
		CompanyMaster company1 = companyService.findByCompanyName(enquiry.getNgoName());
		EnquiryList e2 = findByNgoName(enquiry.getNgoName());
		if (company1!=null || e2 != null) {
			throw new DuplicateCompanyException(
					CustomException.DUPLICATE_COMPANY.getCode());
		}
		try{
			User user1 = userService.findByEmailId(enquiry.getEmailId());
			EnquiryList e3 = findByUserEmail(enquiry.getEmailId());
			if(user1!=null || e3 != null) {
				throw new DuplicateEmailException(
						CustomException.DUPLICATE_ADMIN_EMAIL.getCode());
			}
		}
		catch(NoSuchUserException e){
			LOGGER.info(e);
		}
		
		return enquiryRepository.save(enquiry);
	}

	@Override
	public EnquiryList findByEnqiuryId(long enquiryId) {

		return enquiryRepository.findByEnquiryId(enquiryId);
	}

	@Override
	public EnquiryList findByNgoEmail(String ngoEmail) {

		return enquiryRepository.findByNgoEmail(ngoEmail);
	}

	@Override
	public List<EnquiryList> findByApproval(CompanyApproval approval) {
		return enquiryRepository.findByApproval(approval);

	}

	@Override
	public EnquiryList approve(long enquiryId) {
		EnquiryList enquiry = findByEnqiuryId(enquiryId);
		enquiry.setApproval(CompanyApproval.APPROVED);
		return enquiryRepository.save(enquiry);
	}

	@Override
	public EnquiryList reject(long enquiryId, String remark) {
		EnquiryList enquiry = findByEnqiuryId(enquiryId);
		enquiry.setApproval(CompanyApproval.REJECTED);
		enquiry.setRemark(remark);
		enquiry.setRejectedDate(new Date());
		return enquiryRepository.save(enquiry);
	}

	@Override
	public void sendEnquiryMail(EnquiryList enquiry, String ngoName, String url) {

		if (enquiry != null) {

			HashMap<String, Object> model = new HashMap<>();
			model.put("firstName", enquiry.getFirstName());
			model.put("lastName", enquiry.getLastName());
			model.put("ngoName", enquiry.getNgoName());
			model.put("ngoEmail", enquiry.getEmailId());
			model.put("ngoWebsite", enquiry.getNgoWebsite());

			EmailData data1 = new EmailData();
			data1.setTo(enquiry.getEmailId());
			data1.setSubject("Registration Submitted Successfully");
			data1.setFrom(props.emailFrom);
			data1.setModel(model);

			EmailData data2 = new EmailData();
			data2.setTo(props.emailToAdmin);
			data2.setFrom(props.emailFrom);
			data2.setSubject("New Admin Details");
			data2.setModel(model);

			if (enquiry.getApproval() == CompanyApproval.PENDING) {
				data1.setMailTemplate(props.ngoDetailsMail);
				data2.setMailTemplate(props.ngoEnquiryDetailsMail);
			}

			mailService.sendMail(data1);
			mailService.sendMail(data2);
		}
	}

	@Override
	public void sendRejectionMail(EnquiryList enquiry) {
		if (enquiry != null) {

			HashMap<String, Object> model = new HashMap<>();
			model.put("firstName", enquiry.getFirstName());
			model.put("lastName", enquiry.getLastName());
			model.put("ngoName", enquiry.getNgoName());
			model.put("ngoEmail", enquiry.getEmailId());
			model.put("ngoWebsite", enquiry.getNgoWebsite());
			model.put("remark", enquiry.getRemark());

			EmailData data1 = new EmailData();
			data1.setTo(enquiry.getEmailId());
			//data1.setSubject("Rejection from Atma");
			data1.setSubject("Reject Request from Atma Network!");
			data1.setFrom(props.emailFrom);
			data1.setModel(model);

			EmailData data2 = new EmailData();
			data2.setTo(props.emailToAdmin);
			data2.setFrom(props.emailFrom);
			data2.setSubject("New Admin Details");
			data2.setModel(model);

			if (enquiry.getApproval() == CompanyApproval.REJECTED) {
				data1.setMailTemplate(props.ngoRejectionDetailsMail);
				data2.setMailTemplate(props.ngoRejectionEnquiryDetailsMail);
			}

			mailService.sendMail(data1);
			mailService.sendMail(data2);
		}

	}

	@Override
	public EnquiryList findByUserEmail(String emailId) {
		return enquiryRepository.findByUserEmail(emailId);
	}

	@Override
	public EnquiryList findByNgoName(String ngoName) {
		return enquiryRepository.findByNgoName(ngoName);
	}

}
