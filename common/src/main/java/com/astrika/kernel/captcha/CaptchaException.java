/**
 * Copyright (c) 2014 Astrika, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.astrika.kernel.captcha;

import com.astrika.kernel.exception.BusinessException;

/**
 * @author Priyanka
 */
public class CaptchaException extends BusinessException {

	public CaptchaException() {
		super();
	}

	public CaptchaException(String msg) {
		super(msg);
	}

	public CaptchaException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public CaptchaException(Throwable cause) {
		super(cause);
	}

}