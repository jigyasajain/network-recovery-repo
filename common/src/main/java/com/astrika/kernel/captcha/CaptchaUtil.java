/**
 * Copyright (c) 2014 Astrika, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.astrika.kernel.captcha;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Priyanka
 */
public class CaptchaUtil {

	private Captcha captcha ;

	public void check(HttpServletRequest request)
		throws CaptchaException {

		getCaptcha().check(request);
	}

	public Captcha getCaptcha() {
		return captcha;
	}

	public String getTaglibPath() {
		return getCaptcha().getTaglibPath();
	}

	public boolean isEnabled(HttpServletRequest request)
		throws CaptchaException {

		return getCaptcha().isEnabled(request);
	}

	public void serveImage(
			HttpServletRequest request, HttpServletResponse response)
		throws IOException {

		getCaptcha().serveImage(request, response);
	}

	public void setCaptcha(Captcha captch) {
	    captcha = captch;
	}

}
