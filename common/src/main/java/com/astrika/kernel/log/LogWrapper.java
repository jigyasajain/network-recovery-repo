/**
 * Copyright (c) 2014 Astrika, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.astrika.kernel.log;

import org.apache.log4j.Logger;

/**
 * @author Priyanka
 */
public class LogWrapper implements Log {

	private static final Logger LOGGER = Logger.getLogger(LogWrapper.class);
	private Log logg;
	
	public LogWrapper(Log log) {
	    logg = log;
	}

	public void setLog(Log log) {
	    logg = log;
	}

	public void debug(Object msg) {
		try {
		    logg.debug(msg);
		}
		catch (Exception e) {
			printMsg(msg);
			throw e;
		}
	}

	public void debug(Throwable t) {
		try {
		    logg.debug(t);
		}
		catch (Exception e) {
			printMsg(t.getMessage());
			throw e;
		}
	}

	public void debug(Object msg, Throwable t) {
		try {
		    logg.debug(msg, t);
		}
		catch (Exception e) {
			printMsg(msg);
			 throw e;
		}
	}

	public void error(Object msg) {
		try {
			logg.error(msg);
		}
		catch (Exception e) {
			printMsg(msg);
			throw e;
		}
	}

	public void error(Throwable t) {
		try {
			logg.error(t);
		}
		catch (Exception e) {
			printMsg(t.getMessage());
			throw e;
		}
	}

	public void error(Object msg, Throwable t) {
		try {
			logg.error(msg, t);
		}
		catch (Exception e) {
			printMsg(msg);
			throw e;
		}
	}

	public void fatal(Object msg) {
		try {
			logg.fatal(msg);
		}
		catch (Exception e) {
			printMsg(msg);
			throw e;
		}
	}

	public void fatal(Throwable t) {
		try {
			logg.fatal(t);
		}
		catch (Exception e) {
			printMsg(t.getMessage());
			throw e;
		}
	}

	public void fatal(Object msg, Throwable t) {
		try {
			logg.fatal(msg, t);
		}
		catch (Exception e) {
			printMsg(msg);
			throw e;
		}
	}

	public void info(Object msg) {
		try {
			logg.info(msg);
		}
		catch (Exception e) {
			printMsg(msg);
			throw e;
		}
	}

	public void info(Throwable t) {
		try {
			logg.info(t);
		}
		catch (Exception e) {
			printMsg(t.getMessage());
			throw e;
		}
	}

	public void info(Object msg, Throwable t) {
		try {
			logg.info(msg, t);
		}
		catch (Exception e) {
			printMsg(msg);
			throw e;
		}
	}

	public boolean isDebugEnabled() {
		return logg.isDebugEnabled();
	}

	public boolean isErrorEnabled() {
		return logg.isErrorEnabled();
	}

	public boolean isFatalEnabled() {
		return logg.isFatalEnabled();
	}

	public boolean isInfoEnabled() {
		return logg.isInfoEnabled();
	}

	public boolean isTraceEnabled() {
		return logg.isTraceEnabled();
	}

	public boolean isWarnEnabled() {
		return logg.isWarnEnabled();
	}

	public void trace(Object msg) {
		try {
			logg.trace(msg);
		}
		catch (Exception e) {
			printMsg(msg);
			throw e;
		}
	}

	public void trace(Throwable t) {
		try {
			logg.trace(t);
		}
		catch (Exception e) {
			printMsg(t.getMessage());
			throw e;
		}
	}

	public void trace(Object msg, Throwable t) {
		try {
			logg.trace(msg, t);
		}
		catch (Exception e) {
			printMsg(msg);
			throw e;
		}
	}

	public void warn(Object msg) {
		try {
			logg.warn(msg);
		}
		catch (Exception e) {
			printMsg(msg);
			throw e;
		}
	}

	public void warn(Throwable t) {
		try {
			logg.warn(t);
		}
		catch (Exception e) {
			printMsg(t.getMessage());
			throw e;
		}
	}

	public void warn(Object msg, Throwable t) {
		try {
			logg.warn(msg, t);
		}
		catch (Exception e) {
			printMsg(msg);
			throw e;
		}
	}

	protected void printMsg(Object msg) {
		LOGGER.info(msg);
	}


}