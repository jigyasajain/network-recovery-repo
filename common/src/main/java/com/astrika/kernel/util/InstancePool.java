/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.astrika.kernel.util;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.astrika.kernel.log.Log;
import com.astrika.kernel.log.LogFactoryUtil;

/**
 * @author Priyanka
 */
public class InstancePool {

    private static final Log LOGGER = LogFactoryUtil.getLog(InstancePool.class);

    private static InstancePool instance = new InstancePool();

    private final Map<String, Object> instances;
    
    private InstancePool() {
        instances = new ConcurrentHashMap<>();
    }
    
	public static boolean contains(String className) {
		return instance.containss(className);
	}

	public static Object get(String className) {
		return instance.gett(className);
	}

	public static Object get(String className, boolean logErrors) {
		return instance.geting(className, logErrors);
	}

	public static void put(String className, Object obj) {
	    instance.putt(className, obj);
	}

	public static void reset() {
	    instance.resett();
	}

	private boolean containss(String name) {
		String className = name.trim();

		return instances.containsKey(className);
	}

	private Object gett(String className) {
		return geting(className, true);
	}

	private Object geting(String name, boolean logErrors) {
		String className = name.trim();

		Object instanceOf = instances.get(className);

		if (instanceOf == null) {
			ClassLoader portalClassLoader = this.getClass().getClassLoader();

			try {
				Class<?> clazz = portalClassLoader.loadClass(className);

				instanceOf = clazz.newInstance();

				instances.put(className, instanceOf);
			}
			catch (Exception e1) {
				if (logErrors && LOGGER.isWarnEnabled()) {
					LOGGER.warn(
						"Unable to load " + className +
							" with the portal class loader",
						e1);
				}

				Thread currentThread = Thread.currentThread();

				ClassLoader contextClassLoader =
					currentThread.getContextClassLoader();

				try {
					Class<?> clazz = contextClassLoader.loadClass(className);

					instanceOf = clazz.newInstance();

					instances.put(className, instanceOf);
				}
				catch (Exception e2) {
					if (logErrors) {
						LOGGER.error(
							"Unable to load " + className +
								" with the portal class loader or the " +
									"current context class loader",
							e2);
					}
				}
			}
		}

		return instanceOf;
	}

	private void putt(String name, Object obj) {
		String className = name.trim();

		instances.put(className, obj);
	}

	private void resett() {
	    instances.clear();
	}



}