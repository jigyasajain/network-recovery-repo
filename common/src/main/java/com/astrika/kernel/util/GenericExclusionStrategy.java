package com.astrika.kernel.util;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

public class GenericExclusionStrategy implements ExclusionStrategy{
    private final List<String> skipFields = new ArrayList<>();
	private final Class<?> claz;

	/**
	 * Create a new exclusion strategy for gson.
	 * 
	 * @param clazz
	 *            The class (transfer object) on which this exclusion will work
	 * @param fields
	 *            The field names which should not be serialized
	 */
	public GenericExclusionStrategy(Class<?> clazz, String... fields) {
		claz = clazz;

		for (String field : fields) {
			skipFields.add(field);
		}
	}

	@Override
	public boolean shouldSkipClass(Class<?> clazz) {
		return false;
	}

	@Override
	public boolean shouldSkipField(FieldAttributes f) {
		return f.getDeclaringClass() == claz
				&& skipFields.contains(f.getName());
	}
}
