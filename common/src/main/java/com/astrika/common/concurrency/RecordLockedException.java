package com.astrika.common.concurrency;

import com.astrika.kernel.exception.BusinessException;


public class RecordLockedException extends BusinessException {

	private static final long serialVersionUID = 1L;
	private final String errorCode;

	private final LockDetails lockDetails;

	public RecordLockedException(LockDetails lockDetails, String errorCode) {
		this.lockDetails = lockDetails;
		this.errorCode = errorCode;
	}
	public LockDetails getLockDetails() {
		return lockDetails;
	}

	public String getErrorCode() {
		return errorCode;
	}

}
