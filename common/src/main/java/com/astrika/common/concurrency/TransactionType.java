package com.astrika.common.concurrency;


public enum TransactionType {
	COMPANY,
	BRAND,
	RESTAURANT;
}