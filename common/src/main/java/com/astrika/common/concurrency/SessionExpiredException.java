package com.astrika.common.concurrency;


public class SessionExpiredException extends Exception {

	private static final long serialVersionUID = 1L;
	private final String errorCode;
	private LockDetails lockDetails;

	public SessionExpiredException( String errorCode) {
		this.errorCode = errorCode;
	}

	public LockDetails getLockDetails() {
		return lockDetails;
	}

	public String getErrorCode() {
		return errorCode;
	}

}
