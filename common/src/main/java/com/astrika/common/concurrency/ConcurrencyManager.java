package com.astrika.common.concurrency;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.astrika.common.model.User;
import com.astrika.common.util.PropsValues;
import com.astrika.kernel.exception.CustomException;
/**
 * Used to manage locks on records. A service will typically create an instance
 * of this class for itself by calling the constructor and passing its
 * transactionType.
 */
@Component
public class ConcurrencyManager {

	@Autowired
	PropsValues props;
	
	protected int warnTimeout = 0;
	protected int timeout = 0;
	public static final boolean ENABLED = true;
	private Map<TransactionKey, LockDetails> locks;
	
	
	public ConcurrencyManager() {
		this.locks = Collections.synchronizedMap(new HashMap<TransactionKey, LockDetails>());
	}
	
	@PostConstruct
	void postconstruct()
	{
		warnTimeout = props.concurrencySessionWarning;
		timeout = props.concurrencySessionExpire;
	}
	
	


	/**
	 * A Map containing the locks on records. The Transaction ID will be the Key
	 */
	


	/**
	 * Create a new instance of the ConcurrencyManager
	 * 
	 * @param transactionType
	 *            The Transaction for which concurrency needs to be managed
	 */
	

	/**
	 * Tries to get the lock for the specified transaction
	 * 
	 * @param transactionId
	 *            A valid transaction Id
	 * @param user
	 *            A valid User
	 * @throws RecordLockedException
	 *             if the record is already locked
	 */
	public synchronized void lock(long transactionId, User user,String transactionType, String token) throws RecordLockedException {
		TransactionKey key = new TransactionKey(transactionId,transactionType, token);
		if (!ENABLED) {
			return;
		}
		if (transactionId <= 0) {
			throw new IllegalArgumentException("Invalid TransactionId");
		} else if (user == null || user.getUserId() == 0) {
			throw new IllegalArgumentException("User must not be null");
		}
		
		lockDetail(transactionId,user,key);
		
	}

	private void lockDetail(long transactionId, User user, TransactionKey key) {

		LockDetails lockDetail = locks.get(key);
		if (lockDetail != null && !lockDetail.isClientConnected()) {
			releaseLock(transactionId);
		}
		if (isLocked(key,user)) {
			throw new RecordLockedException(locks.get(key),CustomException.RECORD_LOCK_EXCEPTION.getCode());
		} else {
			LockDetails lockDetails = new LockDetails(key, user, this);
			locks.put(key, lockDetails);
		}
		
	}

	public synchronized void forceLock(long transactionId, User user,String transactionType, String token) {
		TransactionKey key = new TransactionKey(transactionId,transactionType,token);
		if (transactionId <= 0) {
			throw new IllegalArgumentException("Invalid TransactionId");
		} else if (user == null || user.getUserId() == 0) {
			throw new IllegalArgumentException("User must not be null");
		}

		if (isLocked(key,user)) {
			releaseLock(transactionId);
		}
		LockDetails lockDetails = new LockDetails(key, user, this);
		locks.put(key, lockDetails);
	}
	
	public boolean isLocked(TransactionKey transactionKey, User user) {
		LockDetails lockDetails = locks.get(transactionKey);
		if(lockDetails != null && user.getUserId() != lockDetails.getOwner().getUserId())
		{
			return true;
		}
		else return lockDetails != null && maacId(user.getMacId(),lockDetails.getOwner().getMacId()) && !checkMac(user.getMacId(),lockDetails.getOwner().getMacId());
	
	}
	
	
	private static boolean checkMac(String macId, String macId2) {
		
			return macId!=null && macId.equals(macId2);
		
	}

	private static boolean maacId(String macId, String macId2) {
			return macId!=null || macId2!=null;
	}

	public synchronized boolean requestLockRelease() {
			return true;
	}

	public synchronized void releaseLock(Object request) {
		/*sonar*/
	}

	public synchronized void releaseLock(TransactionKey key) {
		LockDetails lockDetails = locks.remove(key);
		lockDetails.cancelTimers();
	}

	/**
	 * Removes the lock on the transaction, only if the owner is the same as the
	 * specified user
	 * 
	 * @param transactionId
	 * @param ownerId
	 * @return <code>true</code> if the lock was removed (or non-existent),
	 *         <code>false</code> otherwise
	 */
	public synchronized boolean releaseLock(TransactionKey transactionId, int ownerId) {
		LockDetails lockDetails = locks.get(transactionId);
		if (lockDetails == null) {
			return true;
		}
		if (lockDetails.getOwner().getUserId() == ownerId) {
			releaseLock(transactionId);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Extends the lock on the transaction, only if the owner is the same as the
	 * specified user
	 * 
	 * @param transactionId
	 * @param ownerId
	 * @return <code>true</code> if the lock was extended, <code>false</code>
	 *         otherwise
	 */
	public synchronized boolean extendLock(TransactionKey transactionId, int ownerId) {
		LockDetails lockDetails = locks.get(transactionId);
		if (lockDetails == null) {
			return false;
		}
		if (lockDetails.getOwner().getUserId() == ownerId) {
			lockDetails.extendTimers(this);
			return true;
		} else {
			return false;
		}
	}

	public synchronized void sendTimeoutWarning(TransactionKey lockKey) {
		/*sonar*/
	}

	public synchronized void timeout(TransactionKey lockKey) {
		releaseLock(lockKey);
	}

	public synchronized LockDetails getLockInfo(TransactionKey key) {
		return locks.get(key);
	}

	public synchronized Collection<LockDetails> getAllLockDetails() {
		return locks.values();
	}

}
