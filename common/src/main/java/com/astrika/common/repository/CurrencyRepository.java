package com.astrika.common.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.astrika.common.model.CurrencyMaster;

public interface CurrencyRepository extends JpaRepository<CurrencyMaster, Long> {

	Page<CurrencyMaster> findByActiveTrueOrderByCurrencyNameAsc(Pageable page);

	Page<CurrencyMaster> findByActiveFalseOrderByCurrencyNameAsc(Pageable page);

	List<CurrencyMaster> findAllByActiveTrueOrderByCurrencyNameAsc();

	List<CurrencyMaster> findAllByActiveFalseOrderByCurrencyNameAsc();

	CurrencyMaster findByCurrencyName(String currencyName);
	
	CurrencyMaster findByCurrencyCode(String currencyCode);
}
