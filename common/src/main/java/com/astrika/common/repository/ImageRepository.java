package com.astrika.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.astrika.common.model.ImageMaster;


public interface ImageRepository extends JpaRepository<ImageMaster, Long> {

}
