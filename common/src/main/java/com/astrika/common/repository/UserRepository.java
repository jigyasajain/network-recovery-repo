package com.astrika.common.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.astrika.common.model.Role;
import com.astrika.common.model.User;
import com.astrika.common.model.UserStatus;


public interface UserRepository extends JpaRepository<User, Long> {

	@Query("select u from User u left join fetch u.createdBy left join fetch u.city left join fetch u.country where u.userId = ?1")
	User findById(long id);
	
	@Query("select u from User u join fetch u.city join fetch u.country where u.status = ?1")
	List<User> findByStatus(UserStatus status);
	
	@Query("select u from User u left join fetch u.city left join fetch u.country where u.loginId = ?1")
	User findByLoginId(String loginId);
	
	@Query("select u from User u where u.emailId = ?1")
	User findByEmailId(String emailId);
	
	@Query("select u from User u where u.moduleName =?1")
	User findByModuleName(String moduleName);
	
	@Query("select u from User u where u.passwordResetKey = ?1")
	User findByPasswordToken(String token);
	
	@Query("select u from User u left join fetch u.city left join fetch u.country where u.token = ?1")
	User findByToken(String token);
	
	@Query("select u from User u  where u.role = ?1 and u.status = ?2 ")
	List<User> findByRoleAndStatus(Role role, UserStatus status);
	
	
	@Query("select u from User u left join fetch u.createdBy where u.role = ?1 and u.status = ?2 and u.createdBy.userId=?3") 
	List<User> findByRoleAndStatusAndUser(Role role,UserStatus status,long userId);	
	
	
	@Query("select u from User u  where u.designation =?1 and u.status = ?2 ")
	List<User> findByDesignation( String designation, UserStatus status);
	
	@Query("select u from User u where u.lastLoginDate = (select max(lastLoginDate) from User u where u.moduleId = ?1)")
	User findByMaxLastLogin(Long companyId);
	
	@Query("select count(userId) from User u where u.role=?1")
	int totalNoOfUsers(Role role);
	
	@Query("select u from User u where u.role = ?1")
	List<User> findByRole(Role role);
	
	@Query("select count(userId) from User u where u.role=?1 and u.status=?2")
	int totalNoOfActiveUsers(Role role, UserStatus status);

	@Query("select count(userId) from User u where u.status=?1")
	int totalActiveUsers(UserStatus status);
	
	@Query("select count(userId) from User u where u.profileCompletion between ?1 and ?2 and not(u.role = ?3) and not(u.role = ?4)")
	int  noOfUsersProfileReport(int start, int end, Role superAdminRole, Role atmaAdminRole);

	@Query("select count(userId) from User u where u.profileCompletion between ?1 and ?2 and (u.role = ?3 or u.role=8) and u.status=1")
	int noOfComProfileReport(int start, int end, Role role);

	@Query("select count(userId) from User u where DATEDIFF(?1,u.lastLoginDate) >?2 and (u.role=8 or u.role=3) and u.status=1")
	int noOfInactiveUser(String date, int a);
	
	@Query("select count(userId) from User u where DATEDIFF(?1,u.lastLoginDate) >?2")
	int noOfInactiveUserGreaterThanSixMonth(String date, int a);

	@Query("select userId as userId, firstName as firstName,lastLoginDate as lastLoginDate from User u where DATEDIFF(NOW(),u.lastLoginDate) >?1")
	List<Object[]> inactiveUserProfile( int a);

	@Query("select count(userId) from User u where (u.role=?1 or u.role=?2) and DATEDIFF(NOW(),u.lastLoginDate) <30")
	int fingLastMonthActiveUsers(Role companyAdmin, Role moduleUser);

	@Query("select count(userId) from User u where (u.role=?1 or u.role=?2) and u.status=1 and DATEDIFF(NOW(),u.createdOn) >30")
	int activeUsersMoreThanOneMonth(Role companyAdmin, Role moduleUser);

	@Query("select count(userId) from User u where u.profileCompletion=100 ")
	int getFullProfileComplete();

	@Query("select count(userId) from User u where u.status=1 and (u.role=3 or u.role=8) and u.status=1")
	int getTotalUser();

	@Query("select count(u.userId) from User u where DATEDIFF(now(),u.lastLoginDate) BETWEEN 30 and 60")
	int getaciveInSecondLastMonthUsers();

	@Query("select count(u) from User u where u.flag=1")
	int getLastAddedUserCount();

	@Query("select u.userId ,u.firstName,u.lastName,u.createdOn,u.createdBy.userId,u.role,u.flag from User u where DATEDIFF(now(),u.createdOn)<7")
	List<Object[]> getLastAddedUser();

	@Query("select u from User u where u.flag=1")
	List<User> getInactiveFlagLastUser();

	@Query("select userId from User x where x.role=3 or x.role=8")
	List<Object[]> getAllUsers();

	@Query("select u.firstName,u.lastName,u.mobile from User u where u.profileCompletion!=100 and u.role=8 or u.role=3")
	List<Object[]> getUserOfInactiveProfile();

	@Query("select count(userId) from User u where (u.role=?1 or u.role=?2) and u.status=1 and DATEDIFF(NOW(),u.createdOn) <30")
	int getlastMonthCreatedUser(Role companyAdmin, Role moduleUser);

	@Query("select count(userId) from User u where (u.role=?1 or u.role=?2) and u.status=1 and DATEDIFF(NOW(),u.createdOn) BETWEEN 30 and 60")
	int secondLastMonthCreatedUser(Role companyAdmin, Role moduleUser);

	@Query("select count(userId) from User u where (u.role=?1 or u.role=?2) and u.status=?3")
	int getActiveTotalUsers(Role companyUser, Role moduleUser, UserStatus status);

	@Query("select count(userId) from User u where DATEDIFF(now(),u.lastLoginDate) >?1 and (u.role=8 or u.role=3) and u.status=1")
	int noOfInactiveUserExcel(int a);

	
	@Query("select count(userId) from User u where u.status=1 and (u.role=3 or u.role=8) and u.status=1 and year(u.createdOn)<=?1")
	int getTUser(int yr);

	@Query("select count(userId) from User u where u.status=1 and (u.role=3 or u.role=8) and date(u.createdOn)<=?1")
	long getTotaluserUptomonth(Date d);


	
}
