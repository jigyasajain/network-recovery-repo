package com.astrika.common.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.astrika.common.model.Role;
import com.astrika.common.model.User_aud;

public interface User_audRepository extends JpaRepository<User_aud,Long> {

	@Query("select ?1-count(distinct v.userId) ,month(v.lastLoginDate) from User_aud v,User u where v.userId=u.userId and year(v.lastLoginDate)=?2 and (v.role=?3 or v.role=?4)  GROUP BY month(v.lastLoginDate)")
	List<Object[]> getInactiveUserList(long totaUser,int year,Role companyAdmin, Role moduleUser);
	
	
	@Query("select count(distinct v.userId) from User_aud v")
	int getTotalNoOfUser();

	
	@Query("select ?1-count(distinct v.userId) from User_aud v where year(v.lastLoginDate)=?2 and month(v.lastLoginDate)=?5 and (v.role=?3 or v.role=?4)")
	int getInactiveUserListExcel(long totaUser, int year, Role companyAdmin,
			Role moduleUser, int month);


	@Query("select ?1-count(distinct v.userId) from User_aud v,User u where v.userId=u.userId and year(v.lastLoginDate)=?2 and (v.role=3 or v.role=8) and month(v.lastLoginDate)=?3")
	long getInactiveUserOfThatMonth(long userCount,int year, int month);

	
}
