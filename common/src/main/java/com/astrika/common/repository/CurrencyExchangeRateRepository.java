package com.astrika.common.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.astrika.common.model.CurrencyExchangeRate;
import com.astrika.common.model.CurrencyMaster;

public interface CurrencyExchangeRateRepository extends
		JpaRepository<CurrencyExchangeRate, Long> {

	@Query("Select exc from CurrencyExchangeRate exc where exc.currencyFrom = ?1")
	CurrencyExchangeRate findByActiveCurrencyFromCode(String code);

	List<CurrencyExchangeRate> findByActiveTrue();

	CurrencyExchangeRate findByCurrencyToCurrencyId(long currencyId);
	
	CurrencyExchangeRate findByCurrencyFromCurrencyId(long currencyId);
	
	@Query("Select exc.currencyTo from CurrencyExchangeRate exc where exc.rate !=0 and exc.currencyTo.active=1")
	List<CurrencyMaster> findByActiveCurrencyFromRate();

}
