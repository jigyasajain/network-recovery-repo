package com.astrika.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.astrika.common.model.EmailTemplate;

public interface EmailTemplateRepository extends JpaRepository<EmailTemplate, Long> {
	
}
