/**
 * Copyright (c) 2014 Astrika, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.astrika.common.log;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.astrika.kernel.log.Log;
import com.astrika.kernel.log.LogWrapper;

/**
 * @author Priyanka
 */
public class Log4jLogImpl implements Log {

	private static final String FQCN = LogWrapper.class.getName();

	private static Logger logger=Logger.getLogger(Log4jLogImpl.class);
	
	public Log4jLogImpl(Logger log) {
	    logger = log;
	}

	public void debug(Object msg) {
	    logger.log(FQCN, Level.DEBUG, msg, null);
	}

	public void debug(Throwable t) {
	    logger.log(FQCN, Level.DEBUG, null, t);
	}

	public void debug(Object msg, Throwable t) {
	    logger.log(FQCN, Level.DEBUG, msg, t);
	}

	public void error(Object msg) {
	    logger.log(FQCN, Level.ERROR, msg, null);
	}

	public void error(Throwable t) {
	    logger.log(FQCN, Level.ERROR, null, t);
	}

	public void error(Object msg, Throwable t) {
	    logger.log(FQCN, Level.ERROR, msg, t);
	}

	public void fatal(Object msg) {
	    logger.log(FQCN, Level.FATAL, msg, null);
	}

	public void fatal(Throwable t) {
	    logger.log(FQCN, Level.FATAL, null, t);
	}

	public void fatal(Object msg, Throwable t) {
	    logger.log(FQCN, Level.FATAL, msg, t);
	}

	public void info(Object msg) {
	    logger.log(FQCN, Level.INFO, msg, null);
	}

	public void info(Throwable t) {
	    logger.log(FQCN, Level.INFO, null, t);
	}

	public void info(Object msg, Throwable t) {
	    logger.log(FQCN, Level.INFO, msg, t);
	}

	public boolean isDebugEnabled() {
		return logger.isDebugEnabled();
	}

	public boolean isErrorEnabled() {
		return logger.isEnabledFor(Level.ERROR);
	}

	public boolean isFatalEnabled() {
		return logger.isEnabledFor(Level.FATAL);
	}

	public boolean isInfoEnabled() {
		return logger.isInfoEnabled();
	}

	public boolean isTraceEnabled() {
		return logger.isTraceEnabled();
	}

	public boolean isWarnEnabled() {
		return logger.isEnabledFor(Level.WARN);
	}

	public void trace(Object msg) {
	    logger.log(FQCN, Level.TRACE, msg, null);
	}

	public void trace(Throwable t) {
	    logger.log(FQCN, Level.TRACE, null, t);
	}

	public void trace(Object msg, Throwable t) {
	    logger.log(FQCN, Level.TRACE, msg, t);
	}

	public void warn(Object msg) {
	    logger.log(FQCN, Level.WARN, msg, null);
	}

	public void warn(Throwable t) {
	    logger.log(FQCN, Level.WARN, null, t);
	}

	public void warn(Object msg, Throwable t) {
	    logger.log(FQCN, Level.WARN, msg, t);
	}



}