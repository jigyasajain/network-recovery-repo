/**
 * Copyright (c) 2014 Astrika, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.astrika.common.captcha;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.astrika.common.captcha.simplecaptcha.SimpleCaptchaImpl;
import com.astrika.kernel.captcha.Captcha;
import com.astrika.kernel.captcha.CaptchaException;

/**
 * @author Priyanka
 */
@Component
public class CaptchaImpl implements Captcha {

	
	private static final Logger LOGGER = Logger.getLogger(CaptchaImpl.class);

	private volatile Captcha cap;
	private Captcha originalCap;

	public void check(HttpServletRequest request) throws CaptchaException {
		initialize();

		cap.check(request);
	}


	public String getTaglibPath() {
		initialize();

		return cap.getTaglibPath();
	}

	public boolean isEnabled(HttpServletRequest request)
		throws CaptchaException {

		initialize();

		return cap.isEnabled(request);
	}


	public void serveImage(
			HttpServletRequest request, HttpServletResponse response)
		throws IOException {

		initialize();

		cap.serveImage(request, response);
	}


	public void setCaptcha(Captcha captcha) {
		initialize();

		if (captcha == null) {
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("Restoring " + originalCap.getClass().getName());
			}

			cap = originalCap;
		}
		else {
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("Setting " + captcha.getClass().getName());
			}

			cap = captcha;
		}
	}

	private void initialize() {
		if (cap != null) {
			return;
		}

		synchronized (this) {
			if (cap != null) {
				return;
			}

			try {
				
				if (LOGGER.isInfoEnabled()) {
					LOGGER.info("Initializing SimpleCaptchaImpl" );
				}

				cap = new SimpleCaptchaImpl();

				originalCap = cap;
			}
			catch (Exception e) {
				LOGGER.error(e, e);
			}
		}
	}

	
}