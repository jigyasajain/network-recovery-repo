package com.astrika.common.controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.astrika.common.exception.NoSuchCityException;
import com.astrika.common.model.location.StateMaster;
import com.astrika.common.service.StateService;

@Controller
@RequestMapping("/state/*")
public class StateController {

	@Autowired
	private StateService stateService;

	@RequestMapping("/statebycountryid/{countryId}")
	@ResponseBody
	public List<StateMaster> getState(@PathVariable("countryId") Long countryId)
			throws NoSuchCityException {
		return stateService.findByStateCountryCountryId(countryId);
	}


}


