package com.astrika.common.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.astrika.common.exception.NoSuchCountryException;
import com.astrika.common.model.location.CityMaster;
import com.astrika.common.service.CityService;

@Controller
@RequestMapping("/city/*")
public class CityController {

	@Autowired
	private CityService cityServiceImpl;

	@RequestMapping("/citybycountryid/{countryId}")
	@ResponseBody
	public List<CityMaster> getCity(@PathVariable("countryId") Long countryId)
			throws NoSuchCountryException {
		return cityServiceImpl.findByCountryId(countryId);
	}

	@RequestMapping("/findAll")
	@ResponseBody
	public List<CityMaster> findAllActiveCities(){
		return cityServiceImpl.findByActive(true);
	}

	@RequestMapping("/citybystateid/{stateId}")
	@ResponseBody
	public List<CityMaster> getCityByState(@PathVariable("stateId") Long stateId)
			throws NoSuchCountryException {
		return cityServiceImpl.findByStateId(stateId);
	}

}
