package com.astrika.common.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/footer")
public class FooterController {

	@RequestMapping("/privacy")
	public String privacy() {
		return "/visitor/footer/privacy";
	}

	@RequestMapping("/guides")
	public String guides() {
		return "/visitor/footer/how_to_guides";
	}

	@RequestMapping("/contribute")
	public String contribute() {
		return "/visitor/footer/contribute_to_the_network";
	}

	@RequestMapping("/faq")
	public String faq() {
		return "/visitor/footer/faq";
	}

}
