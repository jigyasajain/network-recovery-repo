package com.astrika.common.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.astrika.common.exception.NoSuchCountryException;
import com.astrika.common.model.location.AreaMaster;
import com.astrika.common.service.AreaService;
import com.astrika.common.util.PropsValues;

@Controller
@RequestMapping("/area/*")
public class AreaController {

	@Autowired
	private AreaService areaServiceImpl;

	@Autowired
	private PropsValues propsValue;

	@RequestMapping("/findAll")
	@ResponseBody
	public List<AreaMaster> getAllArea() throws NoSuchCountryException {
		return areaServiceImpl.findByActive(true);
	}

	@RequestMapping("/findAllByCountryAndCity/{countryId}/{cityId}")
	@ResponseBody
	public List<AreaMaster> getAreaByCountryANdCityId(
			@PathVariable("countryId") long countryId,
			@PathVariable("cityId") long cityId)
					throws NoSuchCountryException {
		return areaServiceImpl.findAllByActiveByCityIdAndCountryId(cityId,
				countryId);
	}

	@RequestMapping("/areabycityid/{cityId}")
	@ResponseBody
	public List<AreaMaster> getAreas(@PathVariable("cityId") Long cityId) {
		return areaServiceImpl.findActiveByCityId(cityId);
	}

}
