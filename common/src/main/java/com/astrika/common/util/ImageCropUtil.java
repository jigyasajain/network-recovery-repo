package com.astrika.common.util;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.awt.image.RasterFormatException;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;

import com.astrika.common.model.ImageMaster;
import com.astrika.common.model.User;
import com.astrika.common.service.ImageService;

public class ImageCropUtil {

    private static final Logger LOGGER = Logger.getLogger(ImageCropUtil.class);

    static Rectangle clip;
    
	private ImageCropUtil() {
		super();
	}

	public static byte [] cropImage(ImageService imgService,ImageMaster image, int x,int y, int w, int h) throws IOException{
		String filePath = User.class.getResource("User.class").getPath();
		filePath = filePath.substring(1, filePath.lastIndexOf("/WEB-INF"));
		filePath = filePath+image.getImagePath();
		filePath = filePath.substring(filePath.indexOf("/")+1);
		BufferedImage originalImage = ImageIO.read(new File(filePath));
		BufferedImage processedImage = null;
		try {
			processedImage = cropMyImage(originalImage, w,h,x,y);
		} catch (Exception e) {
			LOGGER.info(e);
		}
		String ext = filePath.substring(filePath.lastIndexOf(".")+1);
		filePath = filePath.substring(0,filePath.lastIndexOf("."));
		filePath = filePath+"_cropped."+ext;
		File outputfile = new File(filePath);
		ImageIO.write(processedImage, ext, outputfile);
		imgService.delete(image.getImageId());
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write( processedImage, ext, baos );
		baos.flush();
		byte[] imageInByte = baos.toByteArray();
		baos.close();
		return imageInByte;
	}

	public static BufferedImage cropMyImage(BufferedImage img, int cropWidth,
			int cropHeight, int cropStartX, int cropStartY) throws IOException {
		BufferedImage clipped ;
		Dimension size = new Dimension(cropWidth, cropHeight);

		try {
			createClip(img, size, cropStartX, cropStartY);
		} catch (Exception e) {
			LOGGER.info(e);
		}

		try {
			int w = clip.width;
			int h = clip.height;
			clipped = img.getSubimage(clip.x, clip.y, w, h);

		} catch (RasterFormatException rfe) {
			LOGGER.info(rfe);
			return null;
		}
		return clipped;
	}

	private static void createClip(BufferedImage img, Dimension size,
			int clipX, int clipY) throws IOException {
		/**
		 * Some times clip area might lie outside the original image, fully or
		 * partially. In such cases, this program will adjust the crop area to
		 * fit within the original image.
		 * 
		 * isClipAreaAdjusted flas is usded to denote if there was any
		 * adjustment made.
		 */
		int clipXX=clipX;
		int clipYY=clipY;

		/** Checking for negative X Co-ordinate **/
		if (clipXX < 0) {
			clipXX = 0;
		}
		/** Checking for negative Y Co-ordinate **/
		if (clipYY < 0) {
			clipYY = 0;
		}

		/** Checking if the clip area lies outside the rectangle **/
		if ((size.width + clipXX) <= img.getWidth()
				&& (size.height + clipYY) <= img.getHeight()) {

			/**
			 * Setting up a clip rectangle when clip area lies within the image.
			 */

			clip = new Rectangle(size);
			clip.x = clipXX;
			clip.y = clipYY;
		} else {

			/**
			 * Checking if the width of the clip area lies outside the image. If
			 * so, making the image width boundary as the clip width.
			 */
			if ((size.width + clipXX) > img.getWidth())
				size.width = img.getWidth() - clipXX;

			/**
			 * Checking if the height of the clip area lies outside the image.
			 * If so, making the image height boundary as the clip height.
			 */
			if ((size.height + clipYY) > img.getHeight())
				size.height = img.getHeight() - clipYY;

			/** Setting up the clip are based on our clip area size adjustment **/
			clip = new Rectangle(size);
			clip.x = clipXX;
			clip.y = clipYY;

		}

	}
}

