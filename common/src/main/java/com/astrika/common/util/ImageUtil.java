package com.astrika.common.util;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.ImageProducer;
import java.awt.image.RGBImageFilter;
import java.awt.image.Raster;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.ImageOutputStream;

import magick.ColorspaceType;
import magick.ImageInfo;
import magick.MagickImage;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.astrika.common.model.User;
import com.astrika.common.repository.ImageRepository;
import com.astrika.kernel.util.CharPool;

@Component
public class ImageUtil {

	@Autowired
	ImageRepository imageRepository;
	
	@Autowired
	PropsValues propertyValues;
	
	/**
	 * This method takes the bytes, converts it to image and perform operations on the image.It also saves the file at a location specified by <code>IMAGE_COPY_DEST_LOCATION</code> in {@link PropsValues} the property file  
	 * @param imageData in bytes
	 * @param fileFormat in string
	 * @param fileName in string
	 * @param isRestaurant {@linkplain boolean} if the request came from restaurant page
	 * @param isEvite {@linkplain boolean} if the request came from evite page
	 * @param isMenu {@linkplain boolean} if the request came from menu page
	 * @return {@link File}
	 * @throws IOException
	 */
	public ArrayList<File> save(byte[] imageData,String fileFormat,String fileName,String locationToSave,boolean isRestaurant,boolean isEvite,boolean isMenu,boolean applyWatermark) throws IOException
	{
		File thumbnailImageFile = null;
		locationToSave=createFilesDir(locationToSave);
		BufferedImage uploadedImage=createImageFromBytes(imageData, fileFormat);
		if(fileFormat.equalsIgnoreCase("png") || fileFormat.equalsIgnoreCase(".png")){
			uploadedImage=changeImageFormat(uploadedImage,"jpg",true);
			fileFormat="jpg";
		}
		BufferedImage originalImage=createImageFromBytes(imageData, fileFormat);
		if(isRestaurant || isMenu)
		{
			if(applyWatermark){
				uploadedImage=addWaterMark(uploadedImage,propertyValues.imageWatermarkToApply);
			}
			BufferedImage thumbnailImage=convertImageToThumbnail(originalImage,propertyValues.imageCreateThumbnail);
			if(propertyValues.imageCreateThumbnail)
			{
				String thumbNailFileName="";
				if(fileName.contains(".")){
					thumbNailFileName=fileName.substring(0,fileName.indexOf(CharPool.PERIOD))+"Thumbs"+CharPool.PERIOD+fileFormat;
				}
				else{
					thumbNailFileName=fileName+"Thumbs"+CharPool.PERIOD+fileFormat;
				}
				
				thumbnailImageFile=copyImageFile(thumbnailImage, locationToSave, thumbNailFileName);
			}
			uploadedImage=validateAndCompressImage(uploadedImage, propertyValues.imageValidateForComress, fileFormat, fileName);
			
		}
		
		else if(isEvite)
		{
			if(applyWatermark){
				uploadedImage=addWaterMark(uploadedImage,propertyValues.imageWatermarkToApply);
			}
			BufferedImage thumbnailImage=convertImageToThumbnail(originalImage,propertyValues.imageCreateThumbnail);
			if(propertyValues.imageCreateThumbnail)
			{
				thumbnailImageFile=copyImageFile(thumbnailImage, locationToSave, fileName+"Thumbs"+fileFormat);
			}
		}
//		else if(isMenu)
//		{
//			
//		}
		String fileNameToPersist ="filename";
		if(fileName.contains(".")){
			fileNameToPersist = fileName.substring(0,fileName.indexOf('.'));
		}
		else{
			fileNameToPersist = fileName;
		}
	
		String fullFileName=fileNameToPersist+"."+fileFormat;
		
		File location=copyImageFile(uploadedImage,locationToSave,fullFileName);
		ArrayList<File> filesToSend=new ArrayList<File>();
		filesToSend.add(location);
		filesToSend.add(thumbnailImageFile);
		return filesToSend;
		
	}
	
	
	
	/**
	 * This method takes the bytes, converts it to image and perform operations on the image.It also saves the file at a location specified by <code>IMAGE_COPY_DEST_LOCATION</code> in {@link PropsValues} the property file  
	 * @param imageData in bytes
	 * @param fileFormat in string
	 * @param fileName in string
	 * @return {@File}
	 * @throws IOException
	 */
	public File saveMemberImage(byte[] imageData,String fileFormat,String fileName,String locationToSave) throws IOException
	{
		
		locationToSave=createFilesDir(locationToSave);
		BufferedImage uploadedImage=createImageFromBytes(imageData, fileFormat);
		uploadedImage=validateAndCompressImage(uploadedImage, propertyValues.imageValidateForComress, fileFormat, fileName);
		String fileNameToPersist=fileName.substring(0,fileName.indexOf('.'));
		String fullFileName=fileNameToPersist+"."+fileFormat;
		File location=copyImageFile(uploadedImage,locationToSave,fullFileName);
		return location;
   }
	/**
	 * Saves User image and returns the file location 
	 * 
	 * @param imageData in bytes
	 * @param fileFormat in string
	 * @param fileName in string
	 * @param location to save the image in string
	 * @return {@link File}
	 * @throws IOException
	 */
	public File saveMember(byte[] imageData,String fileFormat,String fileName,String saveLocation) throws IOException
	{
		return saveMemberImage(imageData, fileFormat, fileName,saveLocation);
	}
	
	
	
	/**
	 * Saves restaurant image and returns the file location 
	 * 
	 * @param imageData in bytes
	 * @param fileFormat in string
	 * @param fileName in string
	 * @param location to save the image in string
	 * @return {@link File}
	 * @throws IOException
	 */
	public ArrayList<File> saveInstitution(byte[] imageData,String fileFormat,String fileName,String saveLocation,boolean applyWaterMark) throws IOException
	{
		return save(imageData, fileFormat, fileName,saveLocation, true, false, false,applyWaterMark);
	}
	
	
	/**
	 * Saves menu image and returns the file location 
	 * 
	 * @param imageData in bytes
	 * @param fileFormat in string
	 * @param fileName in string
	 * @param location to save the image in string
	 * @return {@link File}
	 * @throws IOException
	 */
	public ArrayList<File> saveMenu(byte[] imageData,String fileFormat,String fileName,String saveLocation) throws IOException
	{
		return save(imageData, fileFormat, fileName,saveLocation, false, false, true,true);
	}
	
	/**
	 * Saves evite image and returns the file location 
	 * 
	 * @param imageData in bytes
	 * @param fileFormat in string
	 * @param fileName in string
	 * @param location to save the image in string
	 * @return {@link File}
	 * @throws IOException
	 */
	public ArrayList<File> saveEvite(byte[] imageData,String fileFormat,String fileName,String saveLocation) throws IOException
	{
		return save(imageData, fileFormat, fileName,saveLocation, false, true, false,true);
	}
	
	/**
	 * This method checks if the image needs to be compressed. It takes a image and checks if it exceeds the size specified by <code>IMAGE_COMPRESS_VALUE</code> in {@link PropsValues}.
	 * If the size exceeds then the image is compressed.
	 * This method first checks if the file format has writer ,if not then it converts the file to jpeg format
	 * @param uploadedImage
	 * @param isImageSizeToValidate
	 * @param fileFormat
	 * @param fileName
	 * @return if compression is allowed it returns the compressed image as BufferedImage
	 * @throws IOException
	 * @author Debjyoti Nath
	 */
	public BufferedImage validateAndCompressImage(BufferedImage uploadedImage,boolean isImageSizeToValidate,String fileFormat,String fileName) throws IOException
	{
		if(isImageSizeToValidate){
			long sizeValue=propertyValues.imageCompressValue;
			String tempFileExtension=fileName.substring(fileName.lastIndexOf('.')+1);
			File temp=File.createTempFile("gourme7FileSize",CharPool.PERIOD+tempFileExtension );
//			Iterator<ImageWriter> writers = ImageIO.getImageWritersBySuffix("png");
//			if (!writers.hasNext())
//			{
//				uploadedImage=changeImageFormat(uploadedImage, "jpeg", true);
//			}
			ImageIO.write(uploadedImage, tempFileExtension, temp);
			long sizeOfImage=temp.length();
			
			if(sizeValue<sizeOfImage)
			{
				uploadedImage=compressImage(uploadedImage,propertyValues.imageCompressQuality,sizeValue,fileFormat);
				FileUtils.deleteQuietly(temp);
			}
		}
		
		return uploadedImage;
	}
	/**
	 * Compares the aspect ratio of the image with the provided aspect ratio. To get the aspect ratio of single image use the method {@link getAspectRatio}
	 * @param uploadedImage
	 * @param aspectValueToCompare
	 * @return  if the returned value is 0 the uploadedImage aspectRatio is same as aspectValueToCompare<br>
	 * 			if the returned value is greater than 0 then uploadedImage aspect ratio is higher than aspectValueToCompare<br>
	 * 			if the returned value is smaller than 0 then uploadedImage aspect ratio is lesser than aspectValueToCompare
	 * @author Debjyoti Nath
	 */
	public int compareAspectRatio(BufferedImage uploadedImage,double aspectValueToCompare)
	{
		aspectValueToCompare=round(aspectValueToCompare,2);
		double aspectRatioImage=round((double)uploadedImage.getHeight()/uploadedImage.getWidth(),2);
		return Double.compare(aspectRatioImage,aspectValueToCompare);
	}
	
	/**
	 * Compares the aspect ratio of the provided images.
	 * @param image1
	 * @param image2
	 * @return  if the returned value is 0 the image1 aspectRatio is same as image2<br>
	 * 			if the returned value is greater than 0 then image1 aspect ratio is higher than image2<br>
	 * 			if the returned value is smaller than 0 then image1 aspect ratio is lesser than image2
	 * @author Debjyoti Nath
	 */
	public int compareAspectRatio(BufferedImage image1,BufferedImage image2)
	{
		return compareAspectRatio(image1,getAspectRatio(image2));
	}
	
	/**
	 * Finds the aspect ratio of the image.
	 * @param uploadedImage
	 * @return {@link double} the aspect ratio of the image
	 * @author Debjyoti Nath
	 */
	public double getAspectRatio(BufferedImage uploadedImage)
	{
		return (double)uploadedImage.getHeight()/uploadedImage.getWidth();
	}
	
	/**
	 * Converts the bytes data recieved from the request object into {@link BufferedImage}
	 * @param imageData
	 * @return {@link BufferedImage}
	 * @author Debjyoti Nath
	 */
	public BufferedImage createImageFromBytes(byte[] imageData,String fileFormat) {
	    ByteArrayInputStream bais = new ByteArrayInputStream(imageData);
	    try {
	        return ImageIO.read(bais);
	    } catch (IOException e) {
	    	 return checkImageIsCMYK(imageData, ".jpg");
	    }
	}
	private BufferedImage checkImageIsCMYK(byte[] imageData,String fileFormat) 
	{
		File temp;
		try {
			temp = File.createTempFile("CMYKImage", fileFormat);
			InputStream input = new ByteArrayInputStream(imageData);
			OutputStream output = new FileOutputStream(temp);
			IOUtils.copy(input, output);
			ImageInfo info = new ImageInfo(temp.getAbsolutePath());
			MagickImage image = new MagickImage(info);
			image.transformRgbImage(ColorspaceType.CMYKColorspace);
			image.setFileName("rgb.jpg");
			image.writeImage(info);
			return ImageIO.read(temp);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	public static BufferedImage readCMYKImage(File file) throws Exception {
        Iterator<ImageReader> readers = ImageIO.getImageReadersByFormatName("JPEG");
        ImageReader reader = null;
        while(readers.hasNext()) {
            reader = readers.next();
            if(reader.canReadRaster())
                break;
        }

        FileInputStream fis = new FileInputStream(file);
        try {
            ImageInputStream input = ImageIO.createImageInputStream(fis); 
            reader.setInput(input); // original CMYK-jpeg stream
            Raster raster = reader.readRaster(0, null); // read image raster 
            BufferedImage image = new BufferedImage(raster.getWidth(), raster.getHeight(), BufferedImage.TYPE_4BYTE_ABGR);
            image.getRaster().setRect(raster);
            return image;
        } finally {
            try { fis.close(); } catch(Exception ex) {}
        }
    }
	
	/**
	 * Resize the image.
	 * 
	 * @param image:The image to resize
	 * @param width:Width of the new image
	 * @param height:height of the new image
	 * @param typeOfImage:Color schem to apply to each pixel
	 * @return {@link BufferedImage}
	 * @author Debjyoti Nath
	 */
	
	public BufferedImage resizeImage(BufferedImage image,int width,int height, int typeOfImage)
	{
		int type=0;
		if(typeOfImage==0)
			type= image.getType() == 0? BufferedImage.TYPE_INT_ARGB : image.getType();
		else
			type=typeOfImage;
		BufferedImage resizedImage = new BufferedImage(width, height, type);
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(image, 0, 0, width, height, null);
		g.dispose();
		resizedImage=scale(resizedImage, 1);
		return resizedImage;
	}
	
	/**
	 * Change the image format.Supports almost all the common file image extensions.
	 * This method preserves the image quality i.e. aspect ratio and dimensions, only the file type is changed.For maintaining the quality of the image the 
	 * {@link resizeImage} method is called .
	 * This functions requires to save the image to a temp file with the converted format. The converted file is read back and returned as {@link BufferedImage} 
	 * 
	 * @param sourceImage
	 * @param fileFormat
	 * @param changeImageExt as boolean specifying if format is to be changed
	 * @return {@link BufferedImage}
	 * @throws IOException
	 * @author Debjyoti Nath
	 */
	public BufferedImage changeImageFormat(BufferedImage sourceImage,String fileFormat,boolean changeImageExt) throws IOException
	{
		if(changeImageExt){
	         int type = sourceImage.getType() == 0? BufferedImage.TYPE_INT_ARGB : sourceImage.getType();
	         BufferedImage resizeImagePng = resizeImage(sourceImage,sourceImage.getWidth(),sourceImage.getHeight(),type);
	         
	         File temp=File.createTempFile("gourmet7", fileFormat);
	         
	         ImageIO.write(resizeImagePng,fileFormat,temp);
	         File url = new File(temp.getPath());
	         BufferedImage image = ImageIO.read(url);
	         
	         FileUtils.deleteQuietly(temp);
	         
			return image;
		}
		return sourceImage;
	}
	
	/**
	 * Adds watermark to a image.The image is provided and the watermark read from <code>IMAGE_WATERMARK_VALUE</code> found in property file {@link PropsValues}.More info on the function available on
	 * {@code "http://www.codebeach.com/2008/02/watermarking-images-in-java-servlet.html"} and {@code http://www.codejava.net/java-se/graphics/adding-a-watermark-over-an-image-programmatically-using-java}
	 * @param image as BufferedImage
	 * @param addWatermark as boolean specifying if the watermark to be applied or not
	 * @return {@link BufferedImage}
	 * @author Debjyoti Nath
	 * @throws IOException 
	 */
	public BufferedImage addWaterMark(BufferedImage image,boolean addWatermark) throws IOException
	{
		if(addWatermark){
//			 Graphics2D g2d = (Graphics2D) image.getGraphics();
////			 AlphaComposite alpha = AlphaComposite.getInstance(AlphaComposite.SRC_OVER,0.5f);
////	         g2d.setComposite(alpha);
//	
//	         g2d.setColor(Color.white);
//	         g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
//	                              RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
//	
//	         g2d.setFont(new Font("Arial", Font.BOLD, 11));
	
//	         BufferedImage waterMarkImage=getImageFromFileUrl(ClassLoader.getSystemResource(propertyValues.IMAGE_WATERMARK_VALUE));
//	         String watermark = propertyValues.IMAGE_WATERMARK_VALUE;
//			 String rootPath = User.class.getResource("User.class").getPath();
//			 rootPath = rootPath.substring(0, rootPath.indexOf("/WEB-INF/"));
//			 URL url =  new URL(rootPath+"/WEB-INF/classes/"+propertyValues.IMAGE_WATERMARK_VALUE);
			String rootPath = this.getClass().getResource("ImageUtil.class").getPath();
			//check if the system is windows or unix if windows replace the / with \\ and if unix let it remain /
			//rootPath.indexOf(CharPool.FORWARD_SLASH) if the path copntanins / then go next check. File.separator.indexOf(CharPool.FORWARD_SLASH) shud not be 0,a 0 value means its in unix file system.
			if(rootPath.indexOf(CharPool.FORWARD_SLASH)==-1) 
				rootPath=rootPath.replace("\\", "/");
			String fileUrl=URLDecoder.decode(rootPath.toString(),"UTF-8");
			if(fileUrl.contains("file"))
				fileUrl=fileUrl.substring(fileUrl.indexOf(CharPool.FORWARD_SLASH)-1);
			if(fileUrl.indexOf(CharPool.FORWARD_SLASH+"WEB-INF"+CharPool.FORWARD_SLASH)!=-1)
				fileUrl = fileUrl.substring(1, fileUrl.indexOf(CharPool.FORWARD_SLASH+"WEB-INF"+CharPool.FORWARD_SLASH))+CharPool.FORWARD_SLASH+"WEB-INF"+CharPool.FORWARD_SLASH+"classes"+CharPool.FORWARD_SLASH+propertyValues.imageWatermarkValue;
			else
			{
				fileUrl = fileUrl.substring(1, fileUrl.indexOf(CharPool.FORWARD_SLASH+"target"))+CharPool.FORWARD_SLASH+"target"+CharPool.FORWARD_SLASH+"test-classes"+CharPool.FORWARD_SLASH+propertyValues.imageWatermarkValue;
			}
				
//			fileUrl = fileUrl.replace("/", File.separator);
//			URL url =  new URL(fileUrl);
			Graphics2D g2d = (Graphics2D) image.getGraphics();
			BufferedImage watermarkSource=getImageFromFilePath(fileUrl);
			Graphics2D g1d=(Graphics2D)watermarkSource.getGraphics();
			 BufferedImage waterMarkImage=imageToBufferedImage(makeColorTransparent(watermarkSource,g1d.getBackground()));
//			 Graphics2D g1d=(Graphics2D)waterMarkImage.getGraphics();
//			 int width=waterMarkImage.getWidth();
//			 int height=waterMarkImage.getHeight();
//			 g1d.setComposite(AlphaComposite.Clear);
//			 g1d.fillRect(0, 0, width, height);
//			 g1d.dispose();
			 
	         
	         AlphaComposite alphaChannel = AlphaComposite.getInstance(AlphaComposite.SRC_OVER,0.1f);
	         g2d.setComposite(alphaChannel);
	         waterMarkImage=resizeImage(waterMarkImage, image.getWidth(), image.getHeight(), 0);
	         // calculates the coordinate where the image is painted
	         int topLeftX = (image.getWidth() - waterMarkImage.getWidth()) / 2;
	         int topLeftY = (image.getHeight() - waterMarkImage.getHeight()) / 2;
	  
	         // paints the image watermark
	         g2d.drawImage(waterMarkImage, topLeftX, topLeftY, null);
	
	         //Free graphic resources
	         g2d.dispose();
		}
		return image;
	
	}
	
	 private static BufferedImage imageToBufferedImage(final Image image)
	   {
	      final BufferedImage bufferedImage =
	         new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_ARGB);
	      final Graphics2D g2 = bufferedImage.createGraphics();
	      g2.drawImage(image, 0, 0, null);
	      g2.dispose();
	      return bufferedImage;
	    }
	public static Image makeColorTransparent(final BufferedImage im, final Color color)
	   {
	      final ImageFilter filter = new RGBImageFilter() {
			
	    	  public final int filterRGB(final int x, final int y, final int rgb)
		         {
	    		  int markerRGB = color.getRGB() | 0xFFFFFFFF;
		            if ((rgb | 0xFF000000) == markerRGB)
		            {
		               // Mark the alpha bits as zero - transparent
		               return 0x00FFFFFF & rgb;
		            }
		            else
		            {
		               // nothing to do
		               return rgb;
		            }
		         }
	      		};

	      		final ImageProducer ip = new FilteredImageSource(im.getSource(), filter);
	      	     return Toolkit.getDefaultToolkit().createImage(ip);
	      }
	
	/**
	 * Changes the aspect ratio or dimension ratio.The aspect ration value is read from a property file.It assumes that the aspect ratio is specified as width:height.
	 * This method then separates the two values width and height and performs division height/width to get a double value that is given to the {@link scale} method,which does the rest
	 * The aspect ratio values are read from <code>IMAGE_ASPECT_RATIO_VALUE</code> in {@link PropsValues}
	 * @param image a BufferedImage
	 * @param changeAspectRatio as boolean specifying if the aspect ratio to be changed or not
	 * @return {@link BufferedImage}
	 * @author Debjyoti Nath
	 */
	
	public BufferedImage changeAspectRatio(BufferedImage image,boolean changeAspectRatio)
	{
		if(changeAspectRatio){
			double aspectValue=getAspecValueFromProp(propertyValues.imageAspectRatioValueChange);
			
			BufferedImage scaledImage=scale(image, aspectValue);
			return scaledImage;
		}
		return image;
	}
	
	/**
	 * This method gets the aspect value as string and returns its double equivalent
	 * @param aspectRatio
	 * @return the aspect ratio specified as string into double e.g. 16:9 will be returned as 0.5624 
	 * @author Debjyoti Nath
	 */
	public double getAspecValueFromProp(String aspectRatio)
	{
		int indexOfSeparator=aspectRatio.indexOf(CharPool.COLON);
		String width=aspectRatio.substring(0,indexOfSeparator).trim();;
		String height=aspectRatio.substring(indexOfSeparator+1).trim();
		double aspectValue=Double.parseDouble(height)/Double.parseDouble(width);
		return aspectValue;
	}
	
	/**
	 * This method writes a image to the destination provided.
	 * 
	 * @param image
	 * @param destinationLoc
	 * @param filename
	 * @return the file that is being created in this method as {@link File}
	 * @throws IOException
	 * @author Debjyoti Nath
	 */
	public File copyImageFile(BufferedImage image,String destinationLoc,String filename) throws IOException
	{
		Date date=new Date();
		Long dateVal=date.getTime();
		String fileNameToSave=filename.substring(0,filename.indexOf(CharPool.PERIOD)).concat(dateVal.toString()).concat(filename.substring(filename.indexOf(CharPool.PERIOD)));
		File dest=new File(destinationLoc+CharPool.FORWARD_SLASH+fileNameToSave);
		if(!dest.exists())
		{
			dest.createNewFile();
		}
		String fileFormat=filename.substring(filename.lastIndexOf('.')+1);
		ImageIO.write(image, fileFormat, dest);
		
		return dest;
	}
	
	/**
	 * This method converts a image to thumbnail image. The dimentions of the image are taken from the  <code>IMAGE_THUMBNAIL_VALUES</code> in {@link PropsValues}
	 * @param image as BufferedImage
	 * @param createThumbnail as boolean specifying if thumbnail is to be created or not.
	 * @return the thumbnail image as {@link BufferedImage}
	 * @author Debjyoti Nath
	 */
	
	public BufferedImage convertImageToThumbnail(BufferedImage image,boolean createThumbnail)
	{
		if(createThumbnail){
			String dimention=propertyValues.imageThumbnailValues;
			int indexOfDimentionSeparator=dimention.indexOf("x");
			if(indexOfDimentionSeparator==-1)
				indexOfDimentionSeparator=dimention.indexOf("X");
			
			String width= dimention.substring(0,indexOfDimentionSeparator).trim();
			String height= dimention.substring(indexOfDimentionSeparator+1).trim();
			
			int typeOfImage=BufferedImage.SCALE_SMOOTH;
				 
			BufferedImage thumbnailImage=resizeImage(image, Integer.parseInt(width), Integer.parseInt(height), typeOfImage);
			
			return thumbnailImage;
		}
		return image;
	}
	/**
	 * Scales the image based on the aspect ratio provided.More info on {@link http://stackoverflow.com/questions/1069095/how-do-you-create-a-thumbnail-image-out-of-a-jpeg-in-java}
	 * and also look through interpolation http://stackoverflow.com/questions/4216123/how-to-scale-a-bufferedimage
	 * @param source as BufferedImage
	 * @param ratio as double
	 * @return the scaled image as {@link BufferedImage}
	 * @author Debjyoti Nath
	 */
	public BufferedImage scale(BufferedImage source,double ratio) {
		  int w = (int) (source.getWidth() * ratio);
		  int h = (int) (source.getHeight() * ratio);
//		  BufferedImage bi = getCompatibleImage(w, h);
//		  Graphics2D g2d = bi.createGraphics();
//		  double xScale = (double) w / source.getWidth();
//		  double yScale = (double) h / source.getHeight();
//		  AffineTransform at = AffineTransform.getScaleInstance(xScale,yScale);
//		  g2d.drawRenderedImage(source, at);
//		  g2d.dispose();
//		  return bi;
		  return copy( source, new BufferedImage( w, h, BufferedImage.TYPE_INT_RGB ) );
	}
	  private static BufferedImage copy(BufferedImage source, BufferedImage target) {
	         Graphics2D g2 = target.createGraphics();
	         g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR );
	         double scalex = (double) target.getWidth()/ source.getWidth();
	         double scaley = (double) target.getHeight()/ source.getHeight();
	         AffineTransform at = AffineTransform.getScaleInstance(scalex, scaley);
	         g2.drawRenderedImage(source, at);
	         g2.dispose();
	         return target;
	     }

	/**
	 * 
	 * @param w as width
	 * @param h as height
	 * @return image as BufferedImage
	 */
	private BufferedImage getCompatibleImage(int w, int h) {
		  GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		  GraphicsDevice gd = ge.getDefaultScreenDevice();
		  GraphicsConfiguration gc = gd.getDefaultConfiguration();
		  BufferedImage image = gc.createCompatibleImage(w, h);
		  return image;
	}
	
	/**
	 * Checks if the provided image is landscape
	 * @param image as BufferedImage
	 * @return true if its landscape else false
	 * @author Debjyoti Nath
	 */
	public boolean landscapeImage(BufferedImage image)
	{
		return image.getHeight()<image.getWidth()?true:false;
	}
	
	/**
	 * Checks if the image is potrait.
	 * @param image as BufferedImage
	 * @return true if its potrait else false
	 * @author Debjyoti Nath
	 */
	public boolean potraitImage(BufferedImage image)
	{
		return image.getHeight()>image.getWidth()?true:false;
	}
	
	/**
	 * Compresses the image to the specified quality.This method first find the writers for the file format then does the compression.If the fileFormat
	 * is not supported by {@link ImageIO} class it throws {@link IllegalStateException}.It iterates over the method untill the image is reduces to the specified size.
	 * It reduces the quality of the image to compress it further.The quality value can be provided of found in <code>IMAGE_COMPRESS_QUALITY</code> in {@link PropsValues}
	 * @param imageToCompress
	 * @param quality
	 * @param sizeToReduce
	 * @param fileFormat
	 * @return the compressed image as BufferedImage
	 * @throws IOException
	 * @author Debjyoti Nath
	 */
	public BufferedImage compressImage(BufferedImage imageToCompress,float quality,long sizeToReduce,String fileFormat) throws IOException
	{
		Iterator<ImageWriter> writers = ImageIO.getImageWritersBySuffix(fileFormat);
		if (!writers.hasNext()) throw new IllegalStateException("No writers found");
		ImageWriter writer = (ImageWriter) writers.next();
		// Create the ImageWriteParam to compress the image.
		ImageWriteParam param = writer.getDefaultWriteParam();
		param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
		param.setCompressionQuality(quality);
		// The output will be a ByteArrayOutputStream (in memory)
		ByteArrayOutputStream bos = new ByteArrayOutputStream(32768);
		ImageOutputStream ios = ImageIO.createImageOutputStream(bos);
		writer.setOutput(ios);
		writer.write(null, new IIOImage(imageToCompress, null, null), param);
		ios.flush(); // otherwise the buffer size will be zero!
		// From the ByteArrayOutputStream create a RenderedImage.
		ByteArrayInputStream in = new ByteArrayInputStream(bos.toByteArray());
		BufferedImage out = ImageIO.read(in);
		long size = bos.toByteArray().length; 
		
		if(sizeToReduce<size)
		{
			quality=quality-0.05f;
			compressImage(out,quality,sizeToReduce,fileFormat);
		}
		return out;
		
	}
	/**
	 * Deletes a file from the given file location.
	 * @param pathToFile the absolute location of the file
	 * @return boolean if the file is deleted
	 */
	public boolean deleteFile(String pathToFile)
	{
		File fileToDelete=new File(pathToFile);
		return fileToDelete.delete();
	}
	
	/**
	 * Rounds the double value
	 * @param value
	 * @param places
	 * @return rounded double value
	 */
	private double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    long factor = (long) Math.pow(10, places);
	    value = value * factor;
	    long tmp = Math.round(value);
	    return (double) tmp / factor;
	}
	
	/**
	 * This method makes deirectory inside <code>"Files"</code> folder created in {@link createFilesDir} method.The directory structure is defined by the array passed as integer to this method.
	 * @param locationDir as int array
	 * @return the absolute location of the last created folder/directory
	 * @throws UnsupportedEncodingException 
	 */
	public String getAbsoluteSaveLocationDir(int...locationDir) throws UnsupportedEncodingException
	{
		String fileLocation=createFilesDir("");
		for(int i:locationDir)
		{
			File fileFolder=new File(fileLocation+File.separator+i);
			if(!fileFolder.exists())
				fileFolder.mkdir();
			fileLocation=fileFolder.getPath();
		}
		return URLDecoder.decode(fileLocation,"UTF-8");
	}
	
	/**
	 * This method makes deirectory inside <code>"Files"</code> folder created in {@link createFilesDir} method.The directory structure is defined by the array passed as String to this method.
	 * @param locationDir as String array
	 * @return the absolute location of the last created folder/directory
	 * @throws UnsupportedEncodingException 
	 */
	public String getAbsoluteSaveLocationDir(String...locationDir) throws UnsupportedEncodingException
	{
		String fileLocation=createFilesDir("");
		for(String i:locationDir)
		{
			File fileFolder=new File(fileLocation+File.separator+i);
			if(!fileFolder.exists())
				fileFolder.mkdir();
			fileLocation=fileFolder.getPath();
		}
		return URLDecoder.decode(fileLocation,"UTF-8");
	}
	
	/**
	 * This method creates a directory named <code>"Files"</code>
	 * 
	 * @param location optional parameter,if provided the dir <code>"Files"</code> will be created at this location
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public String createFilesDir(String location) throws UnsupportedEncodingException
	{
		String fileUrl=location;
		if(location==null || location.isEmpty()){
			String rootPath = this.getClass().getResource("ImageUtil.class").getPath();
			//check if the system is windows or unix if windows replace the / with \\ and if unix let it remain /
			//rootPath.indexOf(CharPool.FORWARD_SLASH) if the path copntanins / then go next check. File.separator.indexOf(CharPool.FORWARD_SLASH) shud not be 0,a 0 value means its in unix file system.
			if(rootPath.indexOf(CharPool.FORWARD_SLASH)==-1 )
				rootPath=rootPath.replace("\\", "/");
			fileUrl=URLDecoder.decode(rootPath.toString(),"UTF-8");
			if(fileUrl.contains("file"))
				fileUrl=fileUrl.substring(fileUrl.indexOf(CharPool.FORWARD_SLASH));
			
			if(fileUrl.indexOf(CharPool.FORWARD_SLASH+"WEB-INF"+CharPool.FORWARD_SLASH)!=-1){
				fileUrl = fileUrl.substring(0, fileUrl.indexOf(CharPool.FORWARD_SLASH+"WEB-INF"+CharPool.FORWARD_SLASH));
			}
			else
				fileUrl = fileUrl.substring(1, fileUrl.indexOf(CharPool.FORWARD_SLASH+"common"+CharPool.FORWARD_SLASH+"target"+CharPool.FORWARD_SLASH+"classes"));
		
			File fileFolder=new File(fileUrl+CharPool.FORWARD_SLASH+propertyValues.imageSaveRootDir);
			if(!fileFolder.exists())
				fileFolder.mkdir();
			
			location=fileFolder.getAbsolutePath();
		}
		else
		{
			File fileFolder=new File(location);
			if(!fileFolder.exists())
				fileFolder.mkdir();
			
			location=fileFolder.getAbsolutePath();
		}
		return URLDecoder.decode(location,"UTF-8");
	}
	/**
	 * Gets the relative path of a location i.e the location from the root file folder/dir <code>"Files"</code>.Example if the absolute path is "D:/myFiles/Files/abc/.." this method will return "/Files/abc/..." 
	 * @param path to get the relative path of a location
	 * @return the location from the <code>"Files"</code> folder/dir
	 * @throws UnsupportedEncodingException 
	 */
	public String getRelativePath(String path) throws UnsupportedEncodingException
	{
		String folderToSearch=File.separator+propertyValues.imageSaveRootDir+File.separator;
		return URLDecoder.decode(path.substring(path.lastIndexOf(folderToSearch)),"UTF-8");
	}
	
	/**
	 * Converts the relative path to absolute path to the <code>"Files"</code> directory.
	 * 
	 * @return absolute path to the <code>"Files"</code> directory
	 * @throws UnsupportedEncodingException
	 */
	public String getLocationToFilesDirectory() throws UnsupportedEncodingException
	{
		return createFilesDir("")+File.separator;
	}
	
	private BufferedImage getImageFromFileUrl(URL url) throws IOException
	{
		String fileUrl=URLDecoder.decode(url.toString(),"UTF-8");
		char fileSeparator;
		if(File.separatorChar=='\\' && fileUrl.contains("\\"))
			fileSeparator=File.separatorChar;
		else
			fileSeparator='/';
		if(fileUrl.contains("file"))
			fileUrl=fileUrl.substring(fileUrl.indexOf(fileSeparator));
		
		return ImageIO.read(new File(fileUrl));
	}
	
	private BufferedImage getImageFromFilePath(String url) throws IOException
	{
		return ImageIO.read(new File(url));
	}



	public ArrayList<File> saveAd(byte[] imageInByteArray, String fileFormat,
			String fileName, String locationToSave) throws IOException {
		
		locationToSave=createFilesDir(locationToSave);
		BufferedImage uploadedImage=createImageFromBytes(imageInByteArray, fileFormat);
		String fileNameToPersist=fileName.substring(0,fileName.indexOf('.'));
		String fullFileName=fileNameToPersist+"."+fileFormat;
		
		File location=copyImageFile(uploadedImage,locationToSave,fullFileName);
		ArrayList<File> filesToSend=new ArrayList<File>();
		filesToSend.add(location);
		return filesToSend;
	}



	public ArrayList<File> saveInstructor(byte[] byteArray, String ext,
			String fileName, String locationToSave, boolean applyWatermark) throws IOException {

		return save(byteArray, ext, fileName, locationToSave, true, false, false, applyWatermark);
	}
}
