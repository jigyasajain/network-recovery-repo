/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.astrika.common.util;

import com.astrika.kernel.log.Log;
import com.astrika.kernel.log.LogFactoryUtil;
import com.astrika.kernel.util.StringUtil;
import com.astrika.kernel.util.Validator;

/**
 * @author Priyanka
 */
public class PwdGenerator {

	public static final String KEY1 = "0123456789";

	public static final String KEY2 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	public static final String KEY3 = "abcdefghijklmnopqrstuvwxyz";
	
	private static final Log LOGGER = LogFactoryUtil.getLog(PwdGenerator.class);

	private PwdGenerator() {
		super();
	}

	public static String getPinNumber() {
		return getPasswordd(KEY1, 4, true);
	}

	public static String getPassword() {
		return getPassword(8);
	}

	public static String getPassword(int length) {
		return getPasswordd(KEY1 + KEY2 + KEY3, length, true);
	}

	public static String getPassword(String key, int length) {
		return getPassword(key, length, true);
	}

	public static String getPassword(
		String key, int length, boolean useAllKeys) {

		return getPasswordd(key, length, useAllKeys);
	}

	private static String getPasswordd(
		String key, int lengthh, boolean useAllKeys) {

		int keysCount = 0;
		int length=lengthh;

		if (key.contains(KEY1)) {
			keysCount++;
		}

		if (key.contains(KEY2)) {
			keysCount++;
		}

		if (key.contains(KEY3)) {
			keysCount++;
		}

		if (keysCount > length) {
			if (LOGGER.isWarnEnabled()) {
				LOGGER.warn("Length is too short");
			}

			length = keysCount;
		}

		StringBuilder sb = new StringBuilder(length);

		for (int i = 0; i < length; i++) {
			sb.append(key.charAt((int)(Math.random() * key.length())));
		}

		String password = sb.toString();

		if (!useAllKeys) {
			return password;
		}

		boolean invalidPassword = false;

		if (key.contains(KEY1) && Validator.isNull(StringUtil.extractDigits(password))) {
				invalidPassword = true;
		}

		if (key.contains(KEY2) && password.equalsIgnoreCase(password)) {
				invalidPassword = true;
		}

		if (key.contains(KEY3) && password.equalsIgnoreCase(password)) {
				invalidPassword = true;
		}

		if (invalidPassword) {
			return getPasswordd(key, length, useAllKeys);
		}

		return password;
	}

	
}