package com.astrika.common.model;


public enum UserStatus {
	INACTIVE(0),
	ACTIVE(1);
	
	private final int id;
	
	private UserStatus(int id){
		this.id = id;
	}
	
	public int getId() {
		return id;
	}


	public static UserStatus fromInt(int id) {
		switch (id) {
		case 0:
			return INACTIVE;
		case 1:
			return ACTIVE;
		default:
			return null;
		}
	}
}