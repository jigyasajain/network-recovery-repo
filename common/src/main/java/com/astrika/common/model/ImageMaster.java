package  com.astrika.common.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.envers.Audited;
import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

@Entity
@Audited
public class ImageMaster {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long imageId;
	
	@Column(nullable = false,length = 75)
	private String imageName;
	
	@Column(unique = true, nullable = false,length = 500)
	private String imagePath;
	
	@Column(nullable = false,length = 75)
	private String extension;
	
	@Column(nullable = false,length = 75)
	private long size;
	
	@LastModifiedDate
    private DateTime lastModifiedOn;
    
	@Column(columnDefinition = "boolean default true" , nullable = false)
	private boolean active=true;
	
	@Column(unique = true,nullable = true,length = 100)
	private String thumbImagePath;

	@JsonIgnore
	@LastModifiedBy
	@ManyToOne(fetch = FetchType.LAZY)
	private User lastModifiedBy;

	@JsonIgnore
    @CreatedBy
    @ManyToOne(fetch = FetchType.LAZY)
    private User createdBy;
    
	@CreatedDate
    private DateTime createdOn;
	
	public Long getImageId() {
		return imageId;
	}

	public void setImageId(Long imageId) {
		this.imageId = imageId;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}


	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	
	public boolean isActive() {
        return active;
    }

    public void setCreatedOn(DateTime createdOn) {
        this.createdOn = createdOn;
    }
    
	public DateTime getCreatedOn() {
		return createdOn;
	}

    public void setLastModifiedOn(DateTime lastModifiedOn) {
        this.lastModifiedOn = lastModifiedOn;
    }
	
	public DateTime getLastModifiedOn() {
		return lastModifiedOn;
	}
	
	@JsonIgnore
    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }
	
	@JsonIgnore
	public User getCreatedBy() {
		return createdBy;
	}

	@JsonIgnore
    public void setLastModifiedBy(User lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }
	
	@JsonIgnore
	public User getLastModifiedBy() {
		return lastModifiedBy;
	}
	
	public String getThumbImagePath() {
		return thumbImagePath;
	}

	public void setThumbImagePath(String thumbImagePath) {
		this.thumbImagePath = thumbImagePath;
	}
	
}
