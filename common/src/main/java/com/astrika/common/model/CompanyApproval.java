package com.astrika.common.model;

public enum CompanyApproval {
	
	APPROVED(1),
	PENDING(2),
	REJECTED(3);
	
	private final int id;

	private CompanyApproval(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}
	
	public static CompanyApproval fromInt(int id){
		switch(id){
		
			case 1 : 
				return APPROVED;
			case 2 : 
				return PENDING;
			case 3 : 
				return REJECTED;
			default :
				return null;
		}
	}
}
