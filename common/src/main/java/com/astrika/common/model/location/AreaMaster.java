package com.astrika.common.model.location;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.envers.Audited;
import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import com.astrika.common.model.User;

@Entity
@Audited
//@JsonSerialize
public class AreaMaster {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long areaId;

	@Column(nullable = false, length = 75)
	private String areaName;

	@Column(columnDefinition = "boolean default true", nullable = false)
	private boolean active = true;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	private CityMaster city;

	@LastModifiedDate
	private DateTime lastModifiedOn;

	
	@JsonIgnore
	@ManyToOne(fetch =FetchType.LAZY)
	private CountryMaster country;

	@CreatedDate
	private DateTime createdOn;

	@JsonIgnore
	@CreatedBy
	@ManyToOne(fetch = FetchType.LAZY)
	private User createdBy;

	@Column(nullable = false)
	private double latitude;

	@Column(nullable = false)
	private double longitude;

	@JsonIgnore
	@LastModifiedBy
	@ManyToOne(fetch = FetchType.LAZY)
	private User lastModifiedBy;


	public AreaMaster() {
		super();
	}

	public AreaMaster(String areaName, CityMaster city, CountryMaster country,
			double latitude, double longitude) {
		super();
		this.areaName = areaName;
		this.city = city;
		this.country = country;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public Long getAreaId() {
		return areaId;
	}

	public void setAreaId(Long areaId) {
		this.areaId = areaId;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}


	
	public void setActive(boolean active) {
		this.active = active;
	}

	public void setCreatedOn(DateTime createdOn) {
		this.createdOn = createdOn;
	}
	
	public DateTime getCreatedOn() {
        return createdOn;
    }
	
	public DateTime getLastModifiedOn() {
		return lastModifiedOn;
	}

	@JsonIgnore
	public User getCreatedBy() {
		return createdBy;
	}

	public boolean isActive() {
	    return active;
	}


	@JsonIgnore
	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	
	public void setLastModifiedOn(DateTime lastModifiedOn) {
        this.lastModifiedOn = lastModifiedOn;
    }

	@JsonIgnore
    public void setLastModifiedBy(User lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }
	
	@JsonIgnore
	public User getLastModifiedBy() {
		return lastModifiedBy;
	}
	
	public void setCity(CityMaster city) {
        this.city = city;
    }
	
	public CityMaster getCity() {
		return city;
	}

	
	@JsonIgnore
	public CountryMaster getCountry() {
		return country;
	}
	
	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	@JsonIgnore
    public void setCountry(CountryMaster country) {
        this.country = country;
    }
	
	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

}
