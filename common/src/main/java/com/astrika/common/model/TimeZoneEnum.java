package com.astrika.common.model;

import java.util.TimeZone;
/**
 *
 * This is simply all the available TimeZones from java.util.TimeZone as type
 * safe enum.
 *
 * @author Varra
 * @version 1.0
 *
 */
public enum TimeZoneEnum
{
    
    PACIFIC_MIDWAY("(UTC -11:00) Samoa Standard Time"),

    PACIFIC_HONOLULU("(UTC -10:00) Hawaii Standard Time"),

    AMERICA_ANCHORAGE("(UTC -09:00) Alaska Standard Time"),

    AMERICA_LOS_ANGELES("(UTC -08:00) Pacific Standard Time"),

    AMERICA_DENVER("(UTC -07:00) Mountain Standard Time"),

    AMERICA_CHICAGO("(UTC -06:00) Central Standard Time"),

    AMERICA_NEW_YORK("(UTC -05:00) Eastern Standard Time"),

    AMERICA_CARACAS("(UTC -04:30) Venezuela Time"),

    AMERICA_PUERTO_RICO("(UTC -04:00) Atlantic Standard Time"),

    AMERICA_ST_JOHNS("(UTC -03:30) Newfoundland Standard Time"),

    AMERICA_SAO_PAULO("(UTC -03:00) Brasilia Time"),

    AMERICA_NORONHA("(UTC -02:00) Fernando de Noronha Time"),

    ATLANTIC_AZORES("(UTC -01:00) Azores Time"),

    UTC("(UTC ) Coordinated Universal Time"),

    EUROPE_LISBON("(UTC ) Western European Time"),

    EUROPE_PARIS("(UTC +01:00) Central European Time"),

    EUROPE_ISTANBUL("(UTC +02:00) Eastern European Time"),

    ASIA_JERUSALEM("(UTC +02:00) Israel Standard Time"),

    ASIA_BAGHDAD("(UTC +03:00) Arabia Standard Time"),

    ASIA_TEHRAN("(UTC +03:30) Iran Standard Time"),

    ASIA_DUBAI("(UTC +04:00) Gulf Standard Time"),

    ASIA_KABUL("(UTC +04:30) Afghanistan Time"),

    ASIA_KARACHI("(UTC +05:00) Pakistan Time"),

    ASIA_CALCUTTA("(UTC +05:30) India Standard Time"),

    ASIA_KATMANDU("(UTC +05:45) Nepal Time"),

    ASIA_DHAKA("(UTC +06:00) Bangladesh Time"),

    ASIA_RANGOON("(UTC +06:30) Myanmar Time"),

    ASIA_SAIGON("(UTC +07:00) Indochina Time"),

    ASIA_SHANGHAI("(UTC +08:00) China Standard Time"),

    ASIA_TOKYO("(UTC +09:00) Japan Standard Time"),

    ASIA_SEOUL("(UTC +09:00) Korea Standard Time"),

    AUSTRALIA_DARWIN("(UTC +09:30) Central Standard Time (Northern Territory)"),

    AUSTRALIA_SYDNEY("(UTC +10:00) Eastern Standard Time (New South Wales)"),

    PACIFIC_GUADALCANAL("(UTC +11:00) Solomon Is. Time"),

    PACIFIC_AUCKLAND("(UTC +12:00) New Zealand Standard Time"),

    PACIFIC_ENDERBURY("(UTC +13:00) Phoenix Is. Time"),

    PACIFIC_KIRITIMATI("(UTC +14:00) Line Is. Time");
    
    
    private final String timeZone;
    
    /**
     * Sets the {@link TimeZone}
     * @param tz
     */
    private TimeZoneEnum(final String timeZone)
    {
        this.timeZone =timeZone;
    }
    
    /**
     * Gets the {@link TimeZone}
     * 
     * @return
     */
    public final String getTimeZone()
    {
        return timeZone;
    }
}