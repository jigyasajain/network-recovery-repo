package com.astrika.common.model;

public enum InviteStatus {

	PENDING(0),
	ACTIVATED(1);

	private final int id;
	private InviteStatus(int id){
		this.id = id;
	}


	public int getId() {
		return id;
	}


	public static InviteStatus fromInt(int id){
		switch (id) {
		case 0:
			return PENDING;
		case 1:
			return ACTIVATED;
		default:
			return null;
		}
	}
}
