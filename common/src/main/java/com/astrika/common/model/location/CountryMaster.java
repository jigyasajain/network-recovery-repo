package com.astrika.common.model.location;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.envers.Audited;
import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import com.astrika.common.model.User;

@Entity
@Audited
public class CountryMaster {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long countryId;

	@Column(unique = true, nullable = false, length = 75)
	private String countryName;

	
	
	
	@Column(columnDefinition = "boolean default true", nullable = false)
	private boolean active = true;

	@LastModifiedDate
	private DateTime lastModifiedOn;

	@CreatedDate
	private DateTime createdOn;

	@JsonIgnore
	@LastModifiedBy
	@ManyToOne(fetch = FetchType.LAZY)
	private User lastModifiedBy;


	@JsonIgnore
	@CreatedBy
	@ManyToOne(fetch = FetchType.LAZY)
	private User createdBy;

	public CountryMaster() {
		super();
	}

	public CountryMaster(String countryName) {
		super();
		this.countryName = countryName;
	}

	public Long getCountryId() {
		return countryId;
	}

	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	

	public void setActive(boolean active) {
		this.active = active;
	}

	public DateTime getLastModifiedOn() {
        return lastModifiedOn;
    }

	public DateTime getCreatedOn() {
		return createdOn;
	}
	
	public boolean isActive() {
        return active;
    }

	public void setCreatedOn(DateTime createdOn) {
		this.createdOn = createdOn;
	}

	public void setLastModifiedOn(DateTime lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	@JsonIgnore
	public User getCreatedBy() {
		return createdBy;
	}

	@JsonIgnore
	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	@JsonIgnore
	public User getLastModifiedBy() {
		return lastModifiedBy;
	}

	@JsonIgnore
	public void setLastModifiedBy(User lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

}
