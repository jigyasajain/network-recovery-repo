
package com.astrika.common.exception;

import com.astrika.kernel.exception.BusinessException;
import com.astrika.kernel.exception.CustomException;
/**
 * @author Priyanka
 */
public class FileNameException extends BusinessException {

	

	public FileNameException() {
		super(CustomException.DUPLICATE_FILE_NAME.getCode());
	}

	public FileNameException(String msg) {
		super(msg);
	}

	public FileNameException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public FileNameException(Throwable cause) {
		super(cause);
	}

}