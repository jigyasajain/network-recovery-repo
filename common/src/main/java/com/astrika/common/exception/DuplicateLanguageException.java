package com.astrika.common.exception;

import com.astrika.kernel.exception.BusinessException;
import com.astrika.kernel.exception.CustomException;

public class DuplicateLanguageException extends BusinessException {
	
	
	public DuplicateLanguageException() {
		super(CustomException.DUPLICATE_LANGUAGE.getCode());
	}

	public DuplicateLanguageException(String errorCode) {
		super(errorCode);
	}

}
