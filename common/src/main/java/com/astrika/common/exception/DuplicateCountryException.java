package com.astrika.common.exception;

import com.astrika.kernel.exception.BusinessException;
import com.astrika.kernel.exception.CustomException;

public class DuplicateCountryException extends BusinessException{
	
	
	public DuplicateCountryException() {
		super(CustomException.DUPLICATE_COUNTRY.getCode());
	}

	public DuplicateCountryException(String errorCode){
		super(errorCode);
	}
	 
}
