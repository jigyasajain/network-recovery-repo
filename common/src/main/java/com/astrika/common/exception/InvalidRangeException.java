package com.astrika.common.exception;

import com.astrika.kernel.exception.BusinessException;
import com.astrika.kernel.exception.CustomException;

public class InvalidRangeException extends BusinessException{
	
	public InvalidRangeException() {
		super(CustomException.INVALID_RANGE.getCode());
	}

	public InvalidRangeException(String errorCode){
		super(errorCode);
	}
	 
}
