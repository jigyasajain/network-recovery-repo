package com.astrika.common.exception;

import com.astrika.kernel.exception.BusinessException;
import com.astrika.kernel.exception.CustomException;

public class DuplicateStateException extends BusinessException{
	
	
	public DuplicateStateException() {
		super(CustomException.DUPLICATE_STATE.getCode());
	}

	public DuplicateStateException(String errorCode){
		super(errorCode);
	}
	 
}
