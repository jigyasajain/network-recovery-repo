package com.astrika.common.exception;

import com.astrika.kernel.exception.BusinessException;
import com.astrika.kernel.exception.CustomException;


public class PasswordException extends BusinessException{
	
	public PasswordException() {
		super(CustomException.PASSWORD_EXCEPTION.getCode());
	}

	public PasswordException(String errorCode){
		super(errorCode);
	}
}
