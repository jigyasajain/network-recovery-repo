package com.astrika.common.exception;

import com.astrika.kernel.exception.BusinessException;
import com.astrika.kernel.exception.CustomException;

public class DuplicateAreaException extends BusinessException{
	
	
	public DuplicateAreaException() {
		super(CustomException.DUPLICATE_AREA.getCode());
	}

	public DuplicateAreaException(String errorCode){
		super(errorCode);
	}
	 
}
