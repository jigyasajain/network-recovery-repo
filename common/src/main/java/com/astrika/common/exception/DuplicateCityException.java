package com.astrika.common.exception;

import com.astrika.kernel.exception.BusinessException;
import com.astrika.kernel.exception.CustomException;

public class DuplicateCityException extends BusinessException{
	
	
	public DuplicateCityException() {
		super(CustomException.DUPLICATE_CITY.getCode());
	}

	public DuplicateCityException(String errorCode){
		super(errorCode);
	}
	 
}
