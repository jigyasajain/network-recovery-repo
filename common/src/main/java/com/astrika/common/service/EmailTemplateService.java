package com.astrika.common.service;

import java.util.List;

import com.astrika.common.model.EmailTemplate;

public interface EmailTemplateService {

	List<EmailTemplate> findEmailTemplates();
	
	EmailTemplate findById(long id);
}
