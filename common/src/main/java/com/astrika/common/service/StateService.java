package com.astrika.common.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.astrika.common.exception.NoSuchCountryException;
import com.astrika.common.model.location.StateMaster;
import com.astrika.kernel.exception.BusinessException;

public interface StateService {
	
	
	StateMaster save(String stateName, Long countryId) throws NoSuchCountryException ;

	List<StateMaster> findByActive(boolean active);
	
	StateMaster findByStateNameAndStateId(String stateName, Long stateId);
	
	List<StateMaster> findByStateCountryCountryId(Long countryId);

	StateMaster findByStateId(long stateId) throws BusinessException;

	StateMaster update(Long stateId,String cityName, long countryId) throws NoSuchCountryException;
	
	List<StateMaster> findByStateName(String name);

	StateMaster delete(Long stateId) ;

	StateMaster  restore(Long stateId) ;
	
	Page<Object[]> findAllRequiredParaByActive(boolean status, int sortColNo, String sortColDir, String sSearch,
			int start, int end);
}

