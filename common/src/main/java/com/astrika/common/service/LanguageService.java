package com.astrika.common.service;

import java.util.List;

import com.astrika.common.exception.DuplicateLanguageException;
import com.astrika.common.exception.NoSuchLanguageException;
import com.astrika.common.model.LanguageMaster;

/**
 * Service interface for Area.
 * 
 * @author Rohit
 */
public interface LanguageService {

	LanguageMaster save(String name, String shortName)
			throws DuplicateLanguageException;

	LanguageMaster delete(long languageId) throws NoSuchLanguageException;

	LanguageMaster findByName(String name);

	LanguageMaster findById(long languageId) throws NoSuchLanguageException;

	List<LanguageMaster> findAllByActive();

	LanguageMaster update(long languageId, String name, String shortName)
			throws NoSuchLanguageException;

	List<LanguageMaster> findAllByInActive();

	public LanguageMaster restore(long languageId)
			throws NoSuchLanguageException;
	
	public LanguageMaster findByShortName(String shortName);
}
