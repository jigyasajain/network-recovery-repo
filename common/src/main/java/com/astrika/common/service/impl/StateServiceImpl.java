package com.astrika.common.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.astrika.common.exception.DuplicateStateException;
import com.astrika.common.exception.NoSuchCountryException;
import com.astrika.common.model.location.CountryMaster;
import com.astrika.common.model.location.StateMaster;
import com.astrika.common.repository.StateRepository;
import com.astrika.common.service.CountryService;
import com.astrika.common.service.StateService;
import com.astrika.kernel.exception.CustomException;

@Service
public class StateServiceImpl implements StateService {
	
	@Autowired
	private StateRepository stateRepository;
	
	@Autowired
	private CountryService countryService;

	@Override
	public StateMaster save(String stateName, Long countryId)
			throws NoSuchCountryException {
		List<StateMaster> stateList = findByStateName(stateName);
		
		for(StateMaster smm : stateList)
		{
			if (smm!=null && smm.getCountry().getCountryId() == countryId)
			{
				throw new DuplicateStateException(CustomException.DUPLICATE_STATE.getCode());
			}
		}
		CountryMaster country = countryService.findById(countryId);
		StateMaster state = new StateMaster(stateName,country);
		return stateRepository.save(state);
	}

	@Override
	public List<StateMaster> findByActive(boolean active) {
		
		return stateRepository.findByActive(active);
	}

	@Override
	public StateMaster findByStateNameAndStateId(String stateName, Long stateId) {
		
		return null;
	}

	@Override
	public List<StateMaster> findByStateCountryCountryId(Long countryId) {
		return stateRepository.findByCountryCountryIdAndActiveTrueOrderByStateNameAsc(countryId);
	}

	@Override
	public StateMaster findByStateId(long stateId) {			
		return stateRepository.findByStateId(stateId);
	}

	@Override
	public StateMaster update(Long stateId, String stateName, long countryId)
			throws NoSuchCountryException {
		List<StateMaster> sm = findByStateName(stateName);
		for(StateMaster stm: sm)
		{
			if(stm!=null && stm.getCountry().getCountryId()==countryId && stm.getStateId()!=stateId)
			{
				throw new DuplicateStateException(CustomException.DUPLICATE_STATE.getCode());
			}
		}
		StateMaster state = findByStateId(stateId);
		state.setStateName(stateName);
		CountryMaster country = countryService.findById(countryId);
		state.setCountry(country);
		return stateRepository.save(state);
		}
	

	@Override
	public List<StateMaster> findByStateName(String name) {

		return stateRepository.findByStateName(name);
	}

	@Override
	public StateMaster delete(Long stateId) {
		StateMaster state = findByStateId(stateId);
		state.setActive(false);
		return stateRepository.save(state);
		
	}

	@Override
	public StateMaster restore(Long stateId){
		StateMaster state = findByStateId(stateId);
		state.setActive(true);
		 return stateRepository.save(state);
	}
	
	
	@Override
	public Page<Object[]> findAllRequiredParaByActive(boolean status, int sortColNo, String sortColDir, String sSearch,
			int start, int end) {
		String sortName;
		Direction dir = "asc".equalsIgnoreCase(sortColDir) ? Direction.ASC
				: Direction.DESC;
		switch (sortColNo) {
		case 0:
			sortName = "stateName";
			break;
		case 1:
			sortName = "country.countryName";
			break;
		default:
			sortName = "stateName";
		}
		 
		return stateRepository.findAllOnlyRequiredParamByStatus(status,
							sSearch, new PageRequest(start, end, dir, sortName));
	

	}
	
	
	
}