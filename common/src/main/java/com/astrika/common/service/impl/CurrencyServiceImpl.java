package com.astrika.common.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.astrika.common.exception.DuplicateCurrencyException;
import com.astrika.common.exception.NoSuchCurrencyException;
import com.astrika.common.model.CurrencyMaster;
import com.astrika.common.repository.CurrencyRepository;
import com.astrika.common.service.CurrencyService;
import com.astrika.kernel.exception.CustomException;

@Service
public class CurrencyServiceImpl implements CurrencyService {

	@Autowired
	private CurrencyRepository currencyRepository;

	@Override
	public CurrencyMaster save(String currencyName, String currencySign,
			String currencyCode) throws DuplicateCurrencyException {
		CurrencyMaster currencyMaster = findByName(currencyName);
		if (currencyMaster != null) {
			throw new DuplicateCurrencyException(
					CustomException.DUPLICATE_CURRENCY.getCode());
		}
		currencyMaster = findByCode(currencyCode);
		if (currencyMaster != null) {
			throw new DuplicateCurrencyException(
					CustomException.DUPLICATE_CURRENCY_CODE.getCode());
		} else {
			CurrencyMaster currency = new CurrencyMaster(currencyName,
					currencySign, currencyCode);
			return currencyRepository.save(currency);
		}
	}

	@Override
	public CurrencyMaster delete(long currencyId)
			throws NoSuchCurrencyException {
		CurrencyMaster currency = findById(currencyId);
		currency.setActive(false);
		return currencyRepository.save(currency);
	}

	@Override
	public CurrencyMaster restore(long currencyId)
			throws NoSuchCurrencyException {
		CurrencyMaster currency = findById(currencyId);
		if (currency != null) {
		currency.setActive(true);
		}
		return currencyRepository.save(currency);
		
	}

	@Override
	public CurrencyMaster update(long currencyId, String currencyName,
			String currencySign, String currencyCode)
			throws NoSuchCurrencyException {
		CurrencyMaster currencyMaster = findByName(currencyName);
		if (currencyMaster != null
				&& currencyMaster.getCurrencyId() != currencyId) {
			throw new DuplicateCurrencyException(
					CustomException.DUPLICATE_CURRENCY.getCode());
		}
		currencyMaster = findByCode(currencyCode);
		if (currencyMaster != null
				&& currencyMaster.getCurrencyId() != currencyId) {
			throw new DuplicateCurrencyException(
					CustomException.DUPLICATE_CURRENCY_CODE.getCode());
		} else {
			CurrencyMaster currency = findById(currencyId);
			currency.setCurrencyName(currencyName);
			currency.setCurrencyCode(currencyCode);
			currency.setCurrencySign(currencySign);
			return currencyRepository.save(currency);
		}
	}

	@Override
	public List<CurrencyMaster> findByInActive(int start, int end) {
		Page<CurrencyMaster> currencyList = currencyRepository
				.findByActiveFalseOrderByCurrencyNameAsc(new PageRequest(start,
						end, Direction.ASC, "currencyName"));
		return currencyList.getContent();
	}

	@Override
	public List<CurrencyMaster> findByActive(int start, int end) {
		Page<CurrencyMaster> currencyList = currencyRepository
				.findByActiveTrueOrderByCurrencyNameAsc(new PageRequest(start,
						end, Direction.ASC, "currencyName"));
		return currencyList.getContent();
	}

	@Override
	public CurrencyMaster findById(long currencyId)
			throws NoSuchCurrencyException {
		CurrencyMaster currency = currencyRepository.findOne(currencyId);
		if (currency == null) {
			throw new NoSuchCurrencyException(
					CustomException.NO_SUCH_CURRENCY.getCode());
		}
		return currency;
	}

	@Override
	public List<CurrencyMaster> findAllByInActive() {
		return currencyRepository.findAllByActiveFalseOrderByCurrencyNameAsc();
	}

	@Override
	public List<CurrencyMaster> findAllByActive() {
		return currencyRepository.findAllByActiveTrueOrderByCurrencyNameAsc();
	}

	@Override
	public CurrencyMaster findByName(String currencyName) {
		return currencyRepository.findByCurrencyName(currencyName);
	}

	@Override
	public CurrencyMaster findByCode(String currencyCode) {
		return currencyRepository.findByCurrencyCode(currencyCode);
	}

}
