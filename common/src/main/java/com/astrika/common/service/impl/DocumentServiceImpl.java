package com.astrika.common.service.impl;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Date;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.astrika.common.service.DocumentService;
import com.astrika.common.util.PropsValues;
import com.astrika.kernel.util.CharPool;

@Service
public class DocumentServiceImpl implements DocumentService{

	
	
	@Autowired
	PropsValues propertyValues;
	
	FileInputStream fileInputStream;
	
	private static final String UTF8="UTF-8";

	public File save(byte[] fileData,String fileFormat,String fileName,String locationToSave) throws IOException
	{
		String locationToSaveFile=createFilesDir(locationToSave);
		
		String fileNameToPersist=fileName.substring(0,fileName.indexOf('.'));
		String fullFileName=fileNameToPersist+fileFormat;
		

		
		return copyDocumentFile(fileData,locationToSaveFile,fullFileName);
		
	}
	
	
	public File copyDocumentFile(byte[] fileData,String destinationLoc,String filename) throws IOException
	{
		Date date=new Date();
		Long dateVal=date.getTime();
		String fileNameToSave=filename.substring(0,filename.indexOf(CharPool.PERIOD)).concat(dateVal.toString()).concat(filename.substring(filename.indexOf(CharPool.PERIOD)));
		File dest=new File(destinationLoc+CharPool.FORWARD_SLASH+fileNameToSave);
		
		if(!dest.exists())
		{
			dest.createNewFile();
		}
		FileOutputStream fileOutputStream=new FileOutputStream(dest);

		fileOutputStream.write(fileData);
		fileOutputStream.close();
		
		return dest;
	}
	

	public boolean deleteFile(String pathToFile)
	{
		File fileToDelete=new File(pathToFile);
		return fileToDelete.delete();
	}
	
	
	
	public String getAbsoluteSaveLocationDir(int...locationDir) throws UnsupportedEncodingException
	{
		String fileLocation = createFilesDir("");
		for(int i:locationDir)
		{
			File fileFolder=new File(fileLocation+File.separator+i);
			if(!fileFolder.exists()){
				fileFolder.mkdir();
			}
			fileLocation=fileFolder.getPath();
		}
		return URLDecoder.decode(fileLocation,UTF8);
	}
	

	public String getAbsoluteSaveLocationDir(String...locationDir) throws UnsupportedEncodingException
	{
		String location=createFilesDir("");
		for(String i:locationDir)
		{
			File fileFolder=new File(location+File.separator+i);
			if(!fileFolder.exists()){
				fileFolder.mkdir();
			}
			location=fileFolder.getPath();
		}
		return URLDecoder.decode(location,UTF8);
	}

	public String createFilesDir(String location) throws UnsupportedEncodingException
	{

		String fileUrl=location;
		if(location==null || location.isEmpty()){
			String rootPath = this.getClass().getResource("DocumentServiceImpl.class").getPath();

			
			if(rootPath.indexOf(CharPool.FORWARD_SLASH)==-1 )
			    rootPath=rootPath.replace("\\", "/");
			fileUrl=URLDecoder.decode(rootPath,UTF8);
			
			if(fileUrl.contains("file"))
				fileUrl=fileUrl.substring(fileUrl.indexOf(CharPool.FORWARD_SLASH));
			
			if(fileUrl.indexOf(CharPool.FORWARD_SLASH+"WEB-INF"+CharPool.FORWARD_SLASH)!=-1)
				fileUrl = fileUrl.substring(0, fileUrl.indexOf(CharPool.FORWARD_SLASH+"ROOT"+CharPool.FORWARD_SLASH));
			else
				fileUrl = fileUrl.substring(0, fileUrl.indexOf(CharPool.FORWARD_SLASH+"common"+CharPool.FORWARD_SLASH+"target"+CharPool.FORWARD_SLASH+"classes"));
			if(!fileUrl.contains(propertyValues.imageSaveRootDir))
				fileUrl=fileUrl+CharPool.FORWARD_SLASH+propertyValues.imageSaveRootDir;
			File fileFolder = new File(fileUrl);
			if(!fileFolder.exists())
				fileFolder.mkdir();
			
		}

		return fileUrl;
	}
	
	public String getRelativePath(String path) throws UnsupportedEncodingException
	{
		String folderToSearch=File.separator+propertyValues.imageSaveRootDir+File.separator;
		return URLDecoder.decode(path.substring(path.lastIndexOf(folderToSearch)),UTF8);
	}
	
	
	public String getLocationToFilesDirectory() throws UnsupportedEncodingException
	{
		return createFilesDir("")+File.separator;
	}
	

	private static BufferedImage getImageFromFilePath(String url) throws IOException
	{
		return ImageIO.read(new File(url));
	}
}
