package com.astrika.common.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.astrika.common.exception.DuplicateLanguageException;
import com.astrika.common.exception.NoSuchLanguageException;
import com.astrika.common.model.LanguageMaster;
import com.astrika.common.repository.LanguageRepository;
import com.astrika.common.service.LanguageService;
import com.astrika.kernel.exception.CustomException;

@Service
public class LanguageServiceImpl implements LanguageService {

	@Autowired
	private LanguageRepository repository;

	@Override
	public LanguageMaster save(String name, String shortName)
			throws DuplicateLanguageException {
		LanguageMaster languageMaster = findByName(name);
		LanguageMaster languageByCode = findByShortName(shortName);
		if (languageMaster != null) {
			throw new DuplicateLanguageException(
					CustomException.DUPLICATE_LANGUAGE.getCode());
		}
		else if(languageByCode != null) {
			throw new DuplicateLanguageException(
					CustomException.DUPLICATE_LANGUAGE_CODE.getCode());
		}
		else {
			LanguageMaster language = new LanguageMaster(name, shortName);
			return repository.save(language);
		}
	}

	@Override
	public LanguageMaster delete(long languageId)
			throws NoSuchLanguageException {
		LanguageMaster language = findById(languageId);
		language.setActive(false);
		return repository.save(language);
	}

	@Override
	public LanguageMaster restore(long languageId)
			throws NoSuchLanguageException {
		LanguageMaster language = findById(languageId);
		language.setActive(true);
		return repository.save(language);
	}

	@Override
	public LanguageMaster findByName(String name) {
		return repository.findByLanguageName(name);
	}
	
	@Override
	public LanguageMaster findByShortName(String shortName) {
		return repository.findByShortName(shortName);
	}

	@Override
	public LanguageMaster findById(long languageId)
			throws NoSuchLanguageException {
		LanguageMaster language = repository.findOne(languageId);
		if (language == null) {
			throw new NoSuchLanguageException(
					CustomException.NO_SUCH_LANGUAGE.getCode());
		}
		return language;
	}

	@Override
	public List<LanguageMaster> findAllByActive() {
		return repository.findByActiveTrue();
	}

	@Override
	public LanguageMaster update(long languageId, String name, String shortName)
			throws DuplicateLanguageException{
		LanguageMaster languageMaster = findByName(name);
		LanguageMaster languageByCode = findByShortName(shortName);
		if (languageMaster != null
				&& languageMaster.getLanguageId() != languageId) {
			throw new DuplicateLanguageException(
					CustomException.DUPLICATE_LANGUAGE.getCode());
		}
		else if(languageByCode != null && languageByCode.getLanguageId() != languageId) {
			throw new DuplicateLanguageException(
					CustomException.DUPLICATE_LANGUAGE_CODE.getCode());
		}
		else {
			LanguageMaster language = findById(languageId);
			language.setLanguageName(name);
			language.setShortName(shortName);
			return repository.save(language);
		}
	}

	@Override
	public List<LanguageMaster> findAllByInActive() {
		return repository.findByActiveFalse();
	}

}
