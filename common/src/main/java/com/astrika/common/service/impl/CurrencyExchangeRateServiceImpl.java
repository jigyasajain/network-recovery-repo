package com.astrika.common.service.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.astrika.common.exception.NoSuchCurrencyException;
import com.astrika.common.exception.NoSuchCurrencyExchangeRateException;
import com.astrika.common.model.CurrencyExchangeRate;
import com.astrika.common.model.CurrencyMaster;
import com.astrika.common.repository.CurrencyExchangeRateRepository;
import com.astrika.common.service.CurrencyExchangeRateService;
import com.astrika.common.service.CurrencyService;
import com.astrika.kernel.exception.CustomException;

@Service
public class CurrencyExchangeRateServiceImpl implements
		CurrencyExchangeRateService {

	@Autowired
	private CurrencyExchangeRateRepository repository;

	@Autowired
	private CurrencyService currencyService;

	@Override
	public CurrencyExchangeRate save(long fromCurrencyId, long toCurrencyId,
			BigDecimal rate) throws NoSuchCurrencyException {
		CurrencyMaster currencyFrom = currencyService.findById(fromCurrencyId);
		CurrencyMaster currencyTo = currencyService.findById(toCurrencyId);
		CurrencyExchangeRate excRate = new CurrencyExchangeRate(currencyFrom,
				currencyTo, rate);
		
		repository.save(excRate);
		
		return excRate;
	}
	
	
	@Override
	public CurrencyExchangeRate activeDeactive(long toCurrencyId,boolean active) throws NoSuchCurrencyException {
		
		 CurrencyExchangeRate currencyExchangeRate = repository.findByCurrencyToCurrencyId(toCurrencyId);
		 currencyExchangeRate.setActive(active);
		repository.save(currencyExchangeRate);
		return currencyExchangeRate;
	}

	@Override
	public CurrencyExchangeRate update(long exRateId, long fromCurrencyId,
			long toCurrencyId, BigDecimal rate) {

		CurrencyExchangeRate excRate = repository.findOne(exRateId);
		CurrencyMaster currencyFrom = new CurrencyMaster();
		currencyFrom.setCurrencyId(fromCurrencyId);

		CurrencyMaster currencyTo = new CurrencyMaster();
		currencyFrom.setCurrencyId(toCurrencyId);

		excRate.setCurrencyFrom(currencyFrom);
		excRate.setCurrencyTo(currencyTo);
		excRate.setRate(rate);
		repository.save(excRate);
		return excRate;
	}

	@Override
	public CurrencyExchangeRate findCurrencyFrom(String code)
			throws NoSuchCurrencyExchangeRateException {
		return repository.findByActiveCurrencyFromCode(code);
	}

	@Override
	public void delete(long id) throws NoSuchCurrencyExchangeRateException {
		CurrencyExchangeRate exchangeRate = repository.findOne(id);
		if (exchangeRate == null) {
			throw new NoSuchCurrencyExchangeRateException(
					CustomException.NO_SUCH_EXCHANGERATE.getCode());
		}
		exchangeRate.setActive(false);
		repository.save(exchangeRate);
	}

	@Override
	public List<CurrencyExchangeRate> findActive() {
		return repository.findByActiveTrue();
	}
	
	
	@Override
	public List<CurrencyExchangeRate> findAll() {
		return repository.findAll();
	}

	@Override
	public List<CurrencyExchangeRate> findAllByActive() {
		return repository.findByActiveTrue();
	}

	@Override
	public List<CurrencyExchangeRate> update(List<CurrencyExchangeRate> list) {
		return repository.save(list);
	}

	@Override
	public CurrencyExchangeRate findByCurrencyTo(long currencyId)
			throws NoSuchCurrencyExchangeRateException {
		return repository.findByCurrencyToCurrencyId(currencyId);
	}
	
	@Override
	public CurrencyExchangeRate findByCurrencyFrom(long currencyId) throws NoSuchCurrencyExchangeRateException {
		CurrencyExchangeRate exchangeRate = repository.findByCurrencyFromCurrencyId(currencyId);
		if(exchangeRate != null){
			return exchangeRate;
		}
		else{
			throw new NoSuchCurrencyExchangeRateException();
		}
	}
	
	@Override
	public List<CurrencyMaster> findByCurrencyFromRate()
			{
		return repository.findByActiveCurrencyFromRate();
	}
	
}
