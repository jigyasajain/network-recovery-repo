package com.astrika.common.service.impl;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.astrika.common.exception.DuplicateCityException;
import com.astrika.common.exception.NoSuchCityException;
import com.astrika.common.model.location.CityMaster;
import com.astrika.common.model.location.CountryMaster;
import com.astrika.common.model.location.StateMaster;
import com.astrika.common.repository.CityRepository;
import com.astrika.common.service.CityService;
import com.astrika.common.service.CountryService;
import com.astrika.common.service.ImageService;
import com.astrika.common.service.StateService;
import com.astrika.kernel.exception.BusinessException;
import com.astrika.kernel.exception.CustomException;

/**
 * Service implementation for City.
 * 
 * @author Rohit
 */
@Service
public class CityServiceImpl implements CityService {

	@Autowired
	private CityRepository cityRepository;	

	@Autowired
	private ImageService imageService;
	
	@Autowired
	private StateService stateService;
	
	@Autowired
	private CountryService countryService;


	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	public CityMaster delete(long cityId) throws NoSuchCityException {
		CityMaster city = findById(cityId);
		city.setActive(false);
		return cityRepository.save(city);
	}

	@Override
	public CityMaster restore(long cityId) throws NoSuchCityException {
		CityMaster city = findById(cityId);
		city.setActive(true);
		return cityRepository.save(city);
	}

	@Transactional
	@Override
	public CityMaster update(long cityId, String cityName, long stateId,
			long countryId) throws IOException {
		List<CityMaster> cities = findByCityName(cityName);
		if (!cities.isEmpty()) { 
			for (CityMaster citymaster : cities) {
				if (citymaster.getCountry().getCountryId() == countryId
						&& citymaster.getState().getStateId() == stateId
						&& citymaster.getCityId() != cityId) {
					throw new DuplicateCityException(
							CustomException.DUPLICATE_CITY.getCode());
				}
			}
		}

		CityMaster city = findById(cityId);

		
		
		
		
		
		
		
		
		
		
		
		city.setCityName(cityName);
		CountryMaster country = countryService.findById(countryId);
		StateMaster state = stateService.findByStateId(stateId);	
		city.setState(state);
		city.setCountry(country);

		
		return cityRepository.save(city);
	}

	@Override
	public List<CityMaster> findByActive(boolean active) {
		return cityRepository.findByActive(active);
	}

	@Override
	public List<CityMaster> findByCountryId(long countryId) {
		return cityRepository
				.findByCountryCountryIdAndActiveTrueOrderByCityNameAsc(countryId);
	}

	@Override
	public List<CityMaster> findByIds(Long[] cities) {
		Iterable<Long> citiesId = Arrays.asList(cities);
		return cityRepository.findAll(citiesId);
	}

	@Override
	public List<CityMaster> findByCityName(String name) {
		return cityRepository.findByCityName(name);
	}

	@Override
	public CityMaster findById(long cityId) throws NoSuchCityException {
		CityMaster city = cityRepository.findByCityId(cityId);
		if (city == null) {
			throw new NoSuchCityException(
					CustomException.NO_SUCH_CITY.getCode());
		}
		return city;
	}

	@Override
	public List<CityMaster> findLatestAdded(int start, int end) {
		return Collections.emptyList();
	}

	@Override
	public CityMaster save(String cityName, Long stateId, Long countryId)
			throws BusinessException{
		List<CityMaster> cities = cityRepository.findByCityName(cityName);
		if (!cities.isEmpty()) {
			for (CityMaster citymaster : cities) {
				if ((citymaster.getState().getStateId() == stateId)
						&& (citymaster.getCountry().getCountryId() == countryId)) {
					throw new DuplicateCityException(
							CustomException.DUPLICATE_CITY.getCode());
				}
			}
		}

		CountryMaster country = countryService.findById(countryId);
		StateMaster state = stateService.findByStateId(stateId);	
		CityMaster city1 = new CityMaster(cityName, state, country);
		return cityRepository.save(city1);
	}

	@Override
	public CityMaster save(String cityName, long countryId)
			throws IOException {
		return null;
	}

	@Override
	public List<CityMaster> findByStateId(Long stateId) {
		
		return cityRepository.findByStateStateIdAndActiveTrueOrderByCityNameAsc(stateId);
	}

	@Override
	public Page<Object[]> findAllRequiredParaByActive(boolean status, int sortColNo, String sortColDir, String sSearch,
			int start, int end) {
		String sortName;
		Direction dir = "asc".equalsIgnoreCase(sortColDir) ? Direction.ASC
				: Direction.DESC;
		switch (sortColNo) {
		case 0:
			sortName = "cityName";
			break;
		case 1:
			sortName = "state.stateName";
			break;
		case 2:
			sortName = "country.countryName";
			break;
		default:
			sortName = "cityName";
		}
		
		return cityRepository.findAllOnlyRequiredParamByStatus(status,
							sSearch, new PageRequest(start, end, dir, sortName));

	}
	
}
