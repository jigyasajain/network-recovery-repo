package com.astrika.common.service;

import java.io.IOException;
import java.util.List;

import org.springframework.data.domain.Page;

import com.astrika.common.exception.DuplicateCityException;
import com.astrika.common.exception.NoSuchCityException;
import com.astrika.common.model.location.CityMaster;
import com.astrika.kernel.exception.BusinessException;

/**
 * Service interface for City.
 * 
 * @author Rohit
 */
public interface CityService {

	/**
	 * Save city
	 * 
	 * @param cityName
	 * @param showInPopup
	 * @param countryId
	 * @param timeZoneName
	 * @return
	 * @throws DuplicateCityException
	 */
	
	CityMaster save(String cityName, long countryId)
			throws IOException;

	/**
	 * Delete City
	 * 
	 * @param cityId
	 * @return
	 * @throws NoSuchCityException
	 */
	CityMaster delete(long cityId) throws NoSuchCityException;

	/**
	 * Restore City
	 * 
	 * @param cityId
	 * @return
	 * @throws NoSuchCityException
	 */
	CityMaster restore(long cityId) throws NoSuchCityException;

	/**
	 * Update
	 * 
	 * @param cityId
	 * @param cityName
	 * @param showInPopup
	 * @param countryId
	 * @param timeZoneName
	 * @return
	 * @throws NoSuchCityException
	 * @throws DuplicateCityException
	 */
	CityMaster update(long cityId, String cityName, long stateId,
			long countryId) throws IOException;

	//
	// /**
	// *
	// * @param start
	// * @param end
	// * @return
	// */

	/**
	 * 
	 * @return
	 */
	List<CityMaster> findByActive(boolean active);

	/**
	 * Fetch with country
	 * 
	 * @param cityId
	 * @return
	 * @throws NoSuchCityException
	 */
	CityMaster findById(long cityId) throws NoSuchCityException;

	/**
	 * without country
	 * 
	 * @return
	 */

	/**
	 * 
	 * @param countryId
	 * @return
	 */
	List<CityMaster> findByCountryId(long countryId);

	List<CityMaster> findByIds(Long[] cities);



	/**
	 * without country
	 * 
	 * @return
	 */
	List<CityMaster> findByCityName(String name);

	List<CityMaster> findLatestAdded(int start, int end);
	
	CityMaster save(String cityName, Long stateId, Long countryId) throws BusinessException ;
	
	List<CityMaster> findByStateId(Long stateId);

	public Page<Object[]> findAllRequiredParaByActive(boolean status, int sortColNo, String sortColDir, String sSearch,
			int start, int end);
	
	
	
}
