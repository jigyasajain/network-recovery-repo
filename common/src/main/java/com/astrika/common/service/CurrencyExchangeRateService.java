package com.astrika.common.service;

import java.math.BigDecimal;
import java.util.List;

import com.astrika.common.exception.NoSuchCurrencyException;
import com.astrika.common.exception.NoSuchCurrencyExchangeRateException;
import com.astrika.common.model.CurrencyExchangeRate;
import com.astrika.common.model.CurrencyMaster;

public interface CurrencyExchangeRateService {

	CurrencyExchangeRate save(long fromCurrencyId, long toCurrencyId,
			BigDecimal rate) throws NoSuchCurrencyException;

	CurrencyExchangeRate update(long exRateId, long fromCurrencyId,
			long toCurrencyId, BigDecimal rate);

	CurrencyExchangeRate findCurrencyFrom(String code)
			throws NoSuchCurrencyExchangeRateException;

	public List<CurrencyExchangeRate> findActive();
	
	public List<CurrencyExchangeRate> findAll();

	public void delete(long id) throws NoSuchCurrencyExchangeRateException;

	public List<CurrencyExchangeRate> findAllByActive();

	public List<CurrencyExchangeRate> update(List<CurrencyExchangeRate> list);

	CurrencyExchangeRate findByCurrencyTo(long currencyId)
			throws NoSuchCurrencyExchangeRateException;
	CurrencyExchangeRate findByCurrencyFrom(long currencyId) throws NoSuchCurrencyExchangeRateException;
	
	List<CurrencyMaster> findByCurrencyFromRate();
	
	public CurrencyExchangeRate  activeDeactive(long toCurrencyId,boolean active) throws NoSuchCurrencyException ;
}