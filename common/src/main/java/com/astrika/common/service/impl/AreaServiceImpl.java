package com.astrika.common.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.astrika.common.exception.DuplicateAreaException;
import com.astrika.common.exception.NoSuchAreaException;
import com.astrika.common.model.location.AreaMaster;
import com.astrika.common.model.location.CityMaster;
import com.astrika.common.model.location.CountryMaster;
import com.astrika.common.repository.AreaRepository;
import com.astrika.common.service.AreaService;
import com.astrika.kernel.exception.CustomException;

/**
 * Service implementation for area.
 * 
 * @author Rohit
 */
@Service
public class AreaServiceImpl implements AreaService {

	@Autowired
	public AreaRepository areaRepository;

	@Override
	public AreaMaster save(String areaName, Long cityId, Long countryId,
			double latitude, double longitude) throws DuplicateAreaException {
		List<AreaMaster> areas = findByName(areaName);
		if (!areas.isEmpty()) { 
			for (AreaMaster areamaster : areas) {
				if (areamaster.getCity().getCityId() == cityId
						&& areamaster.getCountry().getCountryId() == countryId) {
					throw new DuplicateAreaException(
							CustomException.DUPLICATE_AREA.getCode());
				}
			}
		} 
		CityMaster city = new CityMaster();
		city.setCityId(cityId);
		CountryMaster country = new CountryMaster();
		country.setCountryId(countryId);
		AreaMaster area = new AreaMaster(areaName, city, country, latitude,
				longitude);
		return areaRepository.save(area);
	}

	@Override
	public AreaMaster delete(long areaId) throws NoSuchAreaException {
		AreaMaster area = findById(areaId);
		area.setActive(false);
		return areaRepository.save(area);
	}

	@Override
	public AreaMaster restore(long areaId) throws NoSuchAreaException {
		AreaMaster area = findById(areaId);
		area.setActive(true);
		return areaRepository.save(area);
	}

	@Override
	public AreaMaster update(long areaId, String areaName, Long cityId,
			Long countryId, double latitude, double longitude)
			throws NoSuchAreaException{
		List<AreaMaster> areas = findByName(areaName);
		if (areas != null) { // area already exist
			for (AreaMaster areamaster : areas) {
				if (areamaster.getAreaId() != areaId && areamaster.getCity().getCityId() == cityId
						&& areamaster.getCountry().getCountryId() == countryId) {
					throw new DuplicateAreaException(
							CustomException.DUPLICATE_AREA.getCode());
				}
			}
		}
		AreaMaster area = findById(areaId);
		CityMaster city = new CityMaster();
		city.setCityId(cityId);
		CountryMaster country = new CountryMaster();
		country.setCountryId(countryId);
		area.setAreaName(areaName);
		area.setCity(city);
		area.setCountry(country);
		area.setLatitude(latitude);
		area.setLongitude(longitude);
		return areaRepository.save(area);
	}


	
	
	
	
	
	
	
	
	
	
	
	
	
	

	@Override
	public AreaMaster findById(long areaId) throws NoSuchAreaException {
		AreaMaster area = areaRepository.findById(areaId);
		if (area == null) {
			throw new NoSuchAreaException(
					CustomException.NO_SUCH_AREA.getCode());
		}
		return area;
	}

	@Override
	public List<AreaMaster> findByActive(boolean active) {
		return areaRepository.findByActiveOrderByAreaNameAsc(active);
	}

	@Override
	public List<AreaMaster> findByName(String areaName) {
		return areaRepository.findByAreaName(areaName);
	}

	@Override
	public List<AreaMaster> findAllByActiveByCityIdAndCountryId(long cityId,
			long countryId) {
		return areaRepository
				.findByCityCityIdAndCountryCountryIdAndActiveTrueOrderByAreaNameAsc(
						cityId, countryId);
	}

	@Override
	public List<AreaMaster> findActiveByCityId(long cityId) {
		return areaRepository.findByActiveTrueAndCityCityId(cityId);
	}

}
